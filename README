------------------------------------------------------
RealFluid README
------------------------------------------------------

For information on the RealFluid development, please contact Remi Abgrall
e-mail: remi.abgrall@math.uzh.ch

For information on the GPU based RealFluid development, please contact Tulin Kaman
e-mail: tulin.kaman@math.uzh.ch

https://git.math.uzh.ch/tkaman/RealFluid

Project Summary
----------------
The computational fluid dynamics (CFD) application code, RealFluid requires 
an adaptation to take advantage of hybrid high-performance supercomputing systems and we 
would like to extend it so that we can minimize the computationally expensive parts of the
numerical simulations by porting onto GPUs.

Statement of Project Goals
---------------------------
The goal of this project is to continue the development of accelerator-capable version of
our application code RealFluid, which we started during the EuroHack16 in Lugano between
July 4-8, 2016. During this event, we compiled our code using CRAY and PGI compiler
suites; used performance profiling on CPU to identify the part of the code that is the most
time consuming. We identify the suitable part of RealFluid to port onto GPUs. The op-
portunity to work with the experts and specifically with Michael Wolfe from NVIDIA/PGI
was extremely helpful to make a good star. This work requires the continuation of adding
the OpenACC application program interface to RealFluid for effective use of the collection
of compiler directives to loops and regions of our code to be offloaded from a host CPU to
an attached accelerator device.

INDEX
	1. Introduction
	2. How to get it
	3. Contents
	4. Configuring and Building 
	5. Running the test programs
	6. Viewing the output
	7. Parallel version of RealFluid 

---------------
1. Introduction
---------------
The computational fluid dynamics (CFD) application code, RealFluid is a multiphysics
simulation package that has been developed for over two decades and has been under 
continuous enhancement. The package supports a range of physics, including compressible 
inviscid and viscous flows, turbulence models. It is written in Fortran and parallelized 
using MPI. The current version of RealFluid is purely MPI parallelized. The version of 
RealFluid in this repository will be able to run on hybrid(CPU+GPU) high-performance supercomputing 
systems. 

----------------
2. How to get it
----------------
git@git.math.uzh.ch:tkaman/RealFluid.git

-----------
3. Contents
-----------
The RealFluid/ directory is the "root" directory of the RealFluid distribution.
The software consist of two subdirectories. It contains various files required for configuration,
and this README file. 

RealFluid/trunk            the source code subdirectory
RealFluid/TOOLS            the set of tools for configuration, documentation, running, visualization

TOOLS/CONFIG/compilers_options.py
TOOLS/CONFIG/mode.py
TOOLS/CONFIG/models.py
TOOLS/CONFIG/mymodels.py
TOOLS/Construire.py
TOOLS/DOXYGEN/Doxyfile
TOOLS/DOXYGEN/FBxLogo.png
TOOLS/DOXYGEN/README
TOOLS/DOXYGEN/myDox
TOOLS/DOXYGEN/myDox1
TOOLS/DOXYGEN/out
TOOLS/Help.py
TOOLS/InstallFBx.sh
TOOLS/Locate.py
TOOLS/MACHINES/feux.py
TOOLS/MACHINES/mab.py
TOOLS/MACHINES/mygale.py
TOOLS/Makedoc.py
TOOLS/NonRegression.py
TOOLS/REGRESSION/list_tests_cases.py
TOOLS/REGRESSION/submit.py
TOOLS/Redo.py
TOOLS/Restart.py
TOOLS/Run.py
TOOLS/TestCheck.py
TOOLS/Tex/doc.pdf
TOOLS/Tex/doc.tex
TOOLS/UTILS/FindPETSc.cmake
TOOLS/UTILS/FindPackageMultipass.cmake
TOOLS/UTILS/FindSCOTCH.cmake
TOOLS/UTILS/ResolveCompilerPaths.cmake
TOOLS/UTILS/check_args.py
TOOLS/UTILS/environment.py
TOOLS/UTILS/getsvn.py
TOOLS/Visu.py
TOOLS/lastbuild.py
TOOLS/lastrun.py
TOOLS/verstecplot.py
trunk/ALGEBRE/AssembFBox.f90
trunk/ALGEBRE/LinAlgFBox.f90
trunk/ALGEBRE/LinAlgMURGE.f90
trunk/ALGEBRE/PETSc_driver.f90
trunk/ALGEBRE/PlainLinAlg.f90
trunk/CMakeLists.txt
trunk/CMakeLists.txt.dora
trunk/COMM/MpiMPPComm.f90
trunk/COMM/MpiMURGE.f90
trunk/COMM/SeqMPPComm.f90
trunk/ELEMENTS/Elements_Class.f90
trunk/ELEMENTS/Elements_driver.f90
trunk/ELEMENTS/Face_Class.f90
trunk/ELEMENTS/Hexahedron_Class.f90
trunk/ELEMENTS/Pyramid_Class.f90
trunk/ELEMENTS/Quadrangle_Class.f90
trunk/ELEMENTS/Quadrangle_F_Class.f90
trunk/ELEMENTS/Segment_Class.f90
trunk/ELEMENTS/Tetrahedron_Class.f90
trunk/ELEMENTS/Triangle_Class.f90
trunk/ELEMENTS/Triangle_F_Class.f90
trunk/EOS/EOS.f90
trunk/EOS/FullMHDLaws.f90
trunk/EOS/IncompGas.f90
trunk/EOS/Mutation.f90
trunk/EOS/PRSV.f90
trunk/EOS/PerfectGas.f90
trunk/EOS/SW.f90
trunk/EOS/StiffGasMF.f90
trunk/EOS/StiffenedGas.f90
trunk/EOS/StiffenedGasRef.f90
trunk/EOS/VDW.f90
trunk/EOS/Visc_Laws.f90
trunk/EOS/noreg.xml
trunk/GEOM/Extract.f90
trunk/GEOM/GeomGraph.f90
trunk/GEOM/GeomParam.f90
trunk/GEOM/HJ_WallDistance.f90
trunk/GEOM/ReadMesh.f90
trunk/INOUT/Initialisation.f90
trunk/INOUT/Inputs.f90
trunk/INOUT/O2toO3.f90
trunk/INOUT/Outputs.f90
trunk/INOUT/Reprise.f90
trunk/INOUT/Visu.f90
trunk/Interf.f90
trunk/MODELS/Boucle_en_temps.f90
trunk/MODELS/GMRES_LUSGS.f90
trunk/MODELS/LUSGS_method.f90
trunk/MODELS/MF_method.f90
trunk/MODELS/Multiphase/#ModelInterfaces.f90#
trunk/MODELS/Multiphase/Advection.f90
trunk/MODELS/Multiphase/CMakeLists.txt
trunk/MODELS/Multiphase/Contrib_Eul.f90
trunk/MODELS/Multiphase/Contrib_NS.f90
trunk/MODELS/Multiphase/Contrib_b.f90
trunk/MODELS/Multiphase/Diffusion.f90
trunk/MODELS/Multiphase/Gradient_recovery.f90
trunk/MODELS/Multiphase/ModelInterfaces.f90
trunk/MODELS/Multiphase/Num_Schemes.f90
trunk/MODELS/Multiphase/Scheme_LDA.f90
trunk/MODELS/Multiphase/Scheme_LF.f90
trunk/MODELS/Multiphase/Scheme_LW.f90
trunk/MODELS/Multiphase/Scheme_SUPG.f90
trunk/MODELS/Multiphase/Shock_Sensor.f90
trunk/MODELS/Multiphase/Source.f90
trunk/MODELS/Multiphase/Space_integration.f90
trunk/MODELS/Multiphase/Turbulence/Spalart_Allmaras_model.f90
trunk/MODELS/Multiphase/Turbulence/Turbulence_models.f90
trunk/MODELS/Multiphase/new_Scheme_SUPG.f90
trunk/MODELS/Multiphase/old_Scheme_SUPG.f90
trunk/MODELS/Multiphase/stable_Scheme_LF.f90
trunk/MODELS/OrdreEleve/Advection.f90
trunk/MODELS/OrdreEleve/CMakeLists.txt
trunk/MODELS/OrdreEleve/Contrib_Eul.f90
trunk/MODELS/OrdreEleve/Contrib_NS.f90
trunk/MODELS/OrdreEleve/Contrib_b.f90
trunk/MODELS/OrdreEleve/Diffusion.f90
trunk/MODELS/OrdreEleve/Gradient_recovery.f90
trunk/MODELS/OrdreEleve/ModelInterfaces.f90
trunk/MODELS/OrdreEleve/Num_Schemes.f90
trunk/MODELS/OrdreEleve/Scheme_LDA.f90
trunk/MODELS/OrdreEleve/Scheme_LF.f90
trunk/MODELS/OrdreEleve/Scheme_LW.f90
trunk/MODELS/OrdreEleve/Scheme_SUPG.f90
trunk/MODELS/OrdreEleve/Shock_Sensor.f90
trunk/MODELS/OrdreEleve/Source.f90
trunk/MODELS/OrdreEleve/Space_integration.f90
trunk/MODELS/OrdreEleve/Turbulence/Spalart_Allmaras_model.f90
trunk/MODELS/OrdreEleve/Turbulence/Turbulence_models.f90
trunk/MODELS/OrdreEleve/new_Scheme_SUPG.f90
trunk/MODELS/OrdreEleve/old_Scheme_SUPG.f90
trunk/MODELS/OrdreEleve/stable_Scheme_LF.f90
trunk/Main.f90
trunk/Main_module.f90
trunk/MeshCubes3DenHexa.f90
trunk/O2toO3.f90
trunk/Save2Visu.f90
trunk/TYPES/LesTypes.f90
trunk/TYPES/element.f90
trunk/TYPES/element_seg.f90
trunk/UTILS/Compute_Forces.f90
trunk/UTILS/Grid_Play.f90
trunk/UTILS/LibMesh.f90
trunk/UTILS/MMS.f90
trunk/UTILS/Moulinette.f90
trunk/UTILS/PostPro_turbine.f90
trunk/UTILS/Quadrature.f90
trunk/UTILS/Ringleb_flow.f90
trunk/UTILS/Utilitaires.f90
trunk/UTILS/add.c
trunk/UTILS/libmesh4.c
trunk/UTILS/libmesh4.h
trunk/UTILS/mmg3d.f90
trunk/UTILS/mmg3d4.0
trunk/UTILS/mshmet

---------------------------
4. Configuring and Building 
---------------------------
Set the 2 environment variables: 
$RFB that points to the source directory trunk
$TFB that points to the TOOLS  directory TOOLS

The following lines are at the end of my .bashrc file
export PE_ENV=$PE_ENV 
HOMEFB=$HOME/Projects/RealFluid_gpu/RealFluid
export TFB=${HOMEFB}/TOOLS
source $TFB/InstallFBx.sh $TFB
export RFB=${HOMEFB}/trunk

To build RealFluid either type 
$ Construire.py
and choose the model, compiler, environment, compiler options  

We extend the compilation support for RealFluid so that it can be compiled using 
Cray wrapper command "ftn" using all the different programming environments 
(Cray, Intel, PGI) on Cray systems such as CSCS Piz Dora and Piz Daint.

----------------------------
5. Running the test programs
----------------------------
---------------------
6. Viewing the output
---------------------
--------------------------------	
7. Parallel version of RealFluid
--------------------------------


References:
R. Abgrall and Dante de Santis, Linear and non-linear high order accurate residual distribution schemes for the discretization of the steady compressible Navier–Stokes equations,
Journal of Computational Physics, volume 283, 2015

R. Abgrall , M,. Ricchiuto and D. de Santis, High-order preserving residual distribution schemes for advection-diffusion scalar problems on arbitrary grids. ,
SIAM J. Sci. Comput. 36, No. 3, A955-A983 (2014). 

R. Abgrall, A. Larat and M. Ricchiuto, Construction of very high order residual distribution schemes for steady inviscid flow problems on hybrid unstructured meshes,
Journal of Computtaional Physics, volume 230(11), 2011

