!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Mathématiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Université Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   PROGRAM HexaMesh
      CHARACTER(LEN = *), PARAMETER :: mod_name = "HexaMesh"

      REAL,    DIMENSION(:, : ), POINTER  :: Coor
      INTEGER, DIMENSION(:, : ), POINTER  :: Nu, NsFac
      INTEGER, DIMENSION(: ), POINTER :: LogFac

      REAL :: Lx, Ly, Lz, Dx, Dy, Dz, xi, yi, zi

      INTEGER :: ix, iy, iz, is, js, ifa, Iface, jt, Nx, Ny, Nz
      INTEGER :: Ns, Nelmt, NFac, ios
      REAL, DIMENSION(3) :: Xmin

      Nx = 21  
      Ny = 21  
      Nz = 21
      Lx = 1.2 
      Ly = 1.2  
      Lz = 1.2
      Xmin = 0.0

      Ns    = Nx * Ny * Nz
      Nelmt = (Nx -1) * (Ny -1) * (Nz -1)
      Nfac  = 2 * (Nx -1) * (Ny -1) + 2 * (Nx -1) * (Nz -1) + 2 * (Nz -1) * (Ny -1) 
      Dx    = Lx / (Nx -1)
      Dy    = Ly / (Ny -1)
      Dz    = Lz / (Nz -1)

      ALLOCATE( Coor(3, Ns), Nu(8, Nelmt), LogFac(Nfac), NsFac(4, Nfac))
      jt = 0
      ifa = 0
      DO ix = 1, Nx
         xi = (ix -1) * Dx
         DO iy = 1, Ny
            yi = (iy -1) * Dy
            DO iz = 1, Ny
               zi = (iz -1) * Dz

               is = ix + (iy -1) * Nx + (iz -1) * Nx * Ny
               js = is + Nx * Ny
               Coor(:, is) = (/ xi, yi, zi /)

               IF (ix < Nx .AND. iy < Ny .AND. iz < Nz ) THEN
                  jt = jt + 1  
                  nu(:, jt) = (/ is, is + 1, is + 1 + Nx, is + Nx, &
                  &       js, js + 1, js + 1 + Nx, js + Nx /)
                  IF ((ix == 1)   ) THEN
                     Iface = 10 
                     ifa   = ifa + 1
                     NsFac(:, ifa) = (/ is, is + Nx, js + Nx, js /)
                     LogFac(ifa)  = Iface
                  END IF
                  IF ((ix == Nx -1) ) THEN
                     Iface = 11
                     ifa   = ifa + 1
                     NsFac(:, ifa) = (/ is + 1, is + 1 + Nx, js + 1 + Nx, js + 1 /)
                     LogFac(ifa)  = Iface
                  END IF

                  IF ((iy == 1)  ) THEN
                     Iface = 20 
                     ifa = ifa + 1
                     NsFac(:, ifa) = (/ is, is + 1, js + 1, js /)               
                     LogFac(ifa)  = Iface
                  END IF
                  IF ((iy == Ny -1)  ) THEN
                     Iface = 21
                     ifa = ifa + 1
                     NsFac(:, ifa) = (/ is + Nx, is + Nx + 1, js + Nx + 1, js + Nx /)               
                     LogFac(ifa)  = Iface
                  END IF

                  IF ((iz == 1 ) ) THEN
                     Iface = 30
                     ifa = ifa + 1
                     NsFac(:, ifa) = (/ is, is + 1, is + 1 + Nx, is + Nx /)               
                     LogFac(ifa)  = Iface
                  END IF
                  IF ((iz == Nz -1 ) ) THEN
                     Iface = 31
                     ifa = ifa + 1
                     LogFac(ifa)  = Iface
                     NsFac(:, ifa) = (/ js, js + 1, js + 1 + Nx, js + Nx /)               
                  END IF
               END IF
            END DO
         END DO
      END DO
      WRITE(6, *) " Ifa = ", ifa, " Nfac = ", Nfac

      OPEN(10, FILE = "Sortie.mesh", IOSTAT = ios)

      IF (ios /= 0) THEN
         PRINT *, mod_name, " ERREUR : impossible d'ouvrir ", "Sortie.mesh", ios
         CALL delegate_stop()
      END IF
      WRITE(10, *) "MeshVersionFormatted 1"
      WRITE(10, *) "Dimension            3"
      WRITE(10, *) "Vertices ", Ns
      js = 0
      DO is = 1, Ns
         WRITE(10, '( 3(E16.5, 1X), I5 )') Coor(1: 3, is), js 
      END DO
      WRITE(10, *) "Hexahedra ", Nelmt
      js = 2
      WRITE(10, '( 8(I5, 1X), I5 )') Nu(1: 8, 1), js 
      js = 1
      DO jt = 2, Nelmt
         WRITE(10, '( 8(I5, 1X), I5 )') Nu(1: 8, jt), js 
      END DO
!!$   WRITE(10, *) "Hexahedra ", Nelmt
!!$   DO jt = 1, Nelmt
!!$      WRITE(10, '( 8(I5, 1X), I5 )') Nu(1:8, jt), js 
!!$   END DO
      WRITE(10, *) "Quadrilaterals ", Nfac
      DO jt = 1, Nfac
         WRITE(10, '( 4(I5, 1X), I5 )') NsFac(1: 4, jt), LogFac(jt) 
      END DO

      WRITE(10, *) "End "
      CLOSE(10)
   END PROGRAM HexaMesh
