!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                          */
!*               2009-2010 R. Abgrall, P. Jacq et C. Lachat                  */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                  2002-2010 INRIA                                          */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

!
MODULE LinAlg
  !
  ! Modules utilis�s
  ! ----------------
  USE Lestypes
  USE LibComm
  USE PLinAlg
  IMPLICIT NONE

  INTERFACE GaussSeidel
     MODULE PROCEDURE GaussSeidel_orig!blas
  END INTERFACE
  INTERFACE MatrxDotVecteur
     MODULE PROCEDURE MatrxDotVecteur_orig!blas
  END INTERFACE
  INTERFACE ILUMatrix
     MODULE PROCEDURE ILUMatrix_orig
  END INTERFACE
 
  LOGICAL, SAVE, PRIVATE :: debug = .FALSE.
  LOGICAL, SAVE, PRIVATE :: debug_inside_precision = .FALSE.
  LOGICAL, SAVE, PRIVATE :: debug_precision2 = .FALSE.

  INTEGER(KIND = 8), SAVE, PRIVATE :: delta = 0, d2 = 0
  INTEGER, SAVE, PRIVATE :: nb_larg = 0
  INTEGER, SAVE, PRIVATE :: nbgmres = 0
  INTEGER, SAVE, PRIVATE :: nbdiff = 0
  INTEGER, PRIVATE :: new_loc, prev_loc
  INTEGER, PRIVATE :: n2_loc, p2_loc
  INTEGER(KIND=8), PRIVATE, SAVE :: nbrelax = 0
  INTEGER, SAVE, PRIVATE :: nb_jacobi = 0
  INTEGER, SAVE, PRIVATE :: nb_gaussseidel = 0
  INTEGER, SAVE, PRIVATE :: nb_gmreslgsd = 0
  INTEGER, SAVE, PRIVATE :: nb_gmresrgsd = 0
  INTEGER, SAVE, PRIVATE :: nb_gmreslupdo = 0
  INTEGER, SAVE, PRIVATE :: nb_gmresrupdo = 0
  INTEGER, SAVE, PRIVATE :: nb_bicgs = 0
  INTEGER, SAVE, PRIVATE :: nb_bicgslgsd = 0
  INTEGER, SAVE, PRIVATE :: nb_bicgslupdo = 0
  LOGICAL, PRIVATE, SAVE :: module_debug = .FALSE.

CONTAINS

  !!-- R�solution d'un syst�me lin�aire
  !!-- Appelle preconddiagonal,
  !!-- puis au choix jacobi, gaussseidel, bicgs, bicgslgsd, ilumatrix, bicgslupdo, gmres, gmreslgsd, gmresrgsd, gmreslupdo, gmresrupdo, ssormatrix
  SUBROUTINE SolveSystem(Com, DATA, Var, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "SolveSystem"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT( IN OUT)   :: Com
    TYPE(Variables), INTENT( IN OUT) :: Var
    TYPE(Donnees), INTENT(IN)        :: DATA
    TYPE(Matrice), INTENT(INOUT)     :: Mat
    ! Variables locales tableaux
    !----------------------------
    REAL, DIMENSION(:, : ), ALLOCATABLE, SAVE :: Vecteur
    ! Variables locales scalaires
    !----------------------------
    INTEGER :: istat, zz, l
    LOGICAL, SAVE :: initialise = .FALSE.
    LOGICAL :: debug2
    CHARACTER(LEN=*), PARAMETER :: fmt1 = "(A, 1X, A, 1X, 5(F25.15, :))"
    INTEGER :: t0,t1

!#define VERIFIER
    CALL My_Timer(t0)
    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF

    debug2 = .FALSE.
    IF (debug2) THEN
       PRINT fmt1, mod_name, "IN", Var%Flux(:, :50)
    END IF

    IF (.NOT. initialise) THEN
       initialise = .TRUE.
       ALLOCATE(Vecteur(data%Nvar, Var%Ncells), STAT = istat)
       IF (istat /= 0) THEN
          PRINT *, mod_name, " ERREUR : istat=", istat
          CALL stop_run()
       END IF
       IF (see_alloc) THEN
          !OFF           CALL alloc_allocate(mod_name, "vecteur", size(Vecteur))
          CALL alloc_allocate(mod_name, "vecteur", 100000) !-- %%%%%%%%%%%%%%%%%% bug ifort : mettre une expr. en arg3 "tue" le programme
       END IF
    END IF !-- .NOT. initialise
#ifdef VERIFIER
    CALL tester_nan2(Var%Flux, mod_name // " var%flux mal calcule avant preconddiag")
#endif
#ifdef Precision
    IF (debug_precision2) THEN
       WRITE(6, fmt_prec1) mod_name, "avant precond, Sum(flux)==", SUM(Var%Flux)
       WRITE(6, fmt_prec1) mod_name, "avant precond, Sum(mat)==", SUM(Mat%Vals)
       WRITE(6, fmt_prec1) mod_name, "avant precond, MinVal(mat)==", MINVAL(Mat%Vals)
       WRITE(6, fmt_prec1) mod_name, "avant precond, MaxVal(mat)==", MAXVAL(Mat%Vals)
       WRITE(6, fmt_prec1) mod_name, "avant precond Mat%Vals(1,1,1)", Mat%Vals(1, 1, 1)
       WRITE(6, fmt_prec1) mod_name, "avant precond Mat%Vals(*,*,*)", Mat%Vals(Mat%blksize, Mat%blksize, Mat%ncoefmat)
       WRITE(6, *) mod_name, "Infos sur Mat%Vals:", SIZE(Mat%Vals)
    END IF
#endif

    IF (module_debug) THEN
       DO l = 1, Var%Ncells
          DO zz = 1, Data%Nvar
             PRINT fmt_prec2, mod_name, "Flux-avant-precond ", 10000 * zz + l, " ", Var%Flux(zz, l)
          END DO
       END DO
    END IF
    CALL PrecondDiagonal(Mat, Var%Flux)
    IF (module_debug) THEN
       DO l = 1, Var%Ncells
          DO zz = 1, Data%Nvar
             PRINT fmt_prec2, mod_name, "Flux-apres-precond ", 10000 * zz + l, " ", Var%Flux(zz, l)
          END DO
       END DO
    END IF

#ifdef Precision
    IF (debug_precision2) THEN
       WRITE(6, fmt_prec1) mod_name, "apres precond, Sum(flux)==", SUM(Var%Flux)
       WRITE(6, fmt_prec1) mod_name, "apres precond, Sum(mat)==", SUM(Mat%Vals)
    END IF
#endif

#ifdef VERIFIER
    CALL tester_nan2(Var%Flux, mod_name // " var%flux mal calcule apres preconddiag")
#endif

#ifndef SEQUENTIEL
    ! Attention: les lignes de MAT sont mal calcul�es sur les overlaps ....... %%%%%%%%%%%%%%
    ! on y fait n'importe quoi: �change obligatoire !!!!!!!!!!!! %%%%%%%%%%
    CALL EchangeSol( Com, Var%Flux )
#endif

    Vecteur = - Var%Flux !-----------------

    SELECT CASE (data%sel_relax)
    CASE(cs_relax_jacobi)
       CALL Jacobi(Com, DATA, Mat, Var, Vecteur )

    CASE(cs_relax_gauss_seid)
       CALL GaussSeidel(Com, DATA, Mat, Var, Vecteur )
    CASE(cs_relax_bicgs)
       CALL BiCGs(Com, DATA, Mat, Var, Vecteur )
    CASE(cs_relax_bicgsl_gsd)
       CALL BiCgsLGsd(Com, DATA, Mat, Var, Vecteur )
    CASE(cs_relax_bicgsl_ilu)
       CALL ILUMatrix(Mat)
       CALL BiCgsLUpDo(Com, DATA, Mat, Var, Vecteur )

    CASE(cs_relax_gmres, cs_relax_gmresl, cs_relax_gmresr)
       CALL Gmres(Com, DATA, Mat, Var, Vecteur )

    CASE(cs_relax_gmresl_gsd)
       CALL GmresLGsd(Com, DATA, Mat, Var, Vecteur )
    CASE(cs_relax_gmresr_gsd)
       CALL GmresRGsd(Com, DATA, Mat, Var, Vecteur )
    CASE(cs_relax_gmresl_ilu)
       CALL ILUMatrix(Mat)
       CALL GmresLUpDo(Com, DATA, Mat, Var, Vecteur )

    CASE(cs_relax_gmresr_ilu)
       CALL ILUMatrix(Mat)
       CALL GmresRUpDo(Com, DATA, Mat, Var, Vecteur )

    CASE(cs_relax_gmresl_ssor)
       CALL SSORMatrix(Mat )
       CALL GmresLUpDo(Com, DATA, Mat, Var, Vecteur )
    CASE(cs_relax_gmresr_ssor)
       CALL SSORMatrix(Mat )
       CALL GmresRUpDo(Com, DATA, Mat, Var, Vecteur )
    CASE DEFAULT
       WRITE( 6, *) mod_name, " ERREUR : La chaine de caracteres `", TRIM(data%Relax), "' ne correspond pas a une methode"
       CALL stop_run()
    END SELECT


#ifndef SEQUENTIEL
    CALL EchangeSolPrepSend( Com, Vecteur )
#endif

    Var%Flux(:, : Var%NCellsIn) = Vecteur(:, : Var%NCellsIn)
    IF (module_debug) THEN
       DO l = 1, Var%Ncells
          DO zz = 1, Data%Nvar
             PRINT fmt_prec2, mod_name, "Flux-apres-solve ", 10000 * zz + l, " ", Var%Flux(zz, l)
          END DO
       END DO
    END IF


    IF (debug2) THEN
       PRINT fmt1, mod_name, "OUT", Var%Flux(:, :50)
    END IF

#ifndef SEQUENTIEL
    CALL EchangeSolRecv( Com, Var%Flux )
#endif

#ifdef Precision
    IF (.FALSE. .AND. debug_precision) THEN
       WRITE(6, fmt_prec1) mod_name, "fin solvesystem, Sum(flux)==", SUM(Var%Flux)
    END IF
#endif

    CALL My_Timer(t1)
    Var%TotalSolveTime = Var%TotalSolveTime + t1 - t0
  END SUBROUTINE SolveSystem


  !!-- Produit scalaire de deux vecteurs.
  FUNCTION VecteurDotVecteur(v1, v2, Com, InDom) RESULT(pds)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "VecteurDotVecteur"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    REAL                                           :: pds
    REAL, DIMENSION(:, : ), INTENT( IN)     :: v1, v2
    TYPE(MeshCom), INTENT( IN OUT)  :: Com
    INTEGER, INTENT( IN)     :: InDom
    ! Variables locales scalaires
    INTEGER               :: is

    pds = 0.0
    DO is = 1, InDom
       pds = pds + SUM( v1(:, is) * v2(:, is))
    END DO
#ifndef SEQUENTIEL
    CALL Reduce(Com, cs_parall_sum, pds )
#endif
  END FUNCTION VecteurDotVecteur


  !!-- Multiplication matrice x vecteur par petits bouts (l� o� la matrice est non nulle)
  SUBROUTINE MatrxDotVecteur_orig(Mat, Vecteur, Com, Av)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "MatrxDotVecteur_orig"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(Matrice), INTENT( IN)     :: Mat
    REAL, DIMENSION(:, : ), INTENT( IN)     :: Vecteur
    TYPE(MeshCom), INTENT( IN OUT)  :: Com
    REAL, DIMENSION(: ,: ), INTENT(OUT) :: av
    ! Variables locales scalaires
    INTEGER               :: is, js, iv, i1, IFin
    ! Variables locales tableaux
    REAL, DIMENSION(SIZE(Vecteur, dim = 1))   :: AVi

    ! On devrait supposer qu'en entr�e, les overlaps sont � jour %%%%
    ! On qu'on les met � jour en sortie

#ifdef DEBUG_ALGO
    IF (SIZE(Vecteur, 2) /= Mat%NNtot) THEN
       PRINT *, mod_name, " ERREUR : PB de taille dans MAt vect", SIZE( Vecteur), Mat%NNtot
       CALL stop_run()
    END IF
#endif

#ifndef SEQUENTIEL
    DO is = 1, Mat%NNsnd
       AVi   = 0.0
       i1    = Mat%JPosi(is)
       !I1    = IFin + 1
       IFin  = Mat%JPosi(is + 1) -1
       DO iv = i1, IFin
          js    = Mat%JvCell(iv)
          AVi   = AVi + MATMUL(Mat%Vals(:, :, iv), Vecteur(:, js) )
       END DO
       av(:, is) = AVi
    END DO
    CALL EchangeSolPrepSend( Com, av )
#endif


    DO is = Mat%NNsnd + 1, Mat%NNin
       AVi   = 0.0
       i1    = Mat%JPosi(is)
       !I1    = IFin +1
       IFin  = Mat%JPosi(is + 1) -1
       DO iv = i1, IFin
          js    = Mat%JvCell(iv)
          AVi   = AVi + MATMUL(Mat%Vals(:, :, iv), Vecteur(:, js) )
       END DO
       av(:, is) = AVi
    END DO

#ifndef SEQUENTIEL
    CALL EchangeSolRecv( Com, av)
#endif
    ! Juste ou pas pour les lignes des overlaps, le produit matrice vecteur rend le bon r�sultat, avec des overlaps � jour
  END SUBROUTINE MatrxDotVecteur_orig

  !!-- Norme L2 d'un vecteur.
  FUNCTION Norme2(v1, Com, InDom) RESULT(norm)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Norme2"

    ! Type et vocations des Variables d'appel
    !----------------------------------------
    REAL                                 :: norm

    REAL, DIMENSION(:,:), INTENT(IN)    :: v1
    TYPE(MeshCom),        INTENT(INOUT) :: Com
    INTEGER,              INTENT(IN)    :: InDom

    ! Variables locales scalaires
    INTEGER :: is

    norm = 0.0
!$OMP PARALLEL PRIVATE(is)
!$OMP DO ORDERED SCHEDULE(STATIC) REDUCTION(+:norm)
    DO is = 1, InDom
       norm = norm + SUM( v1(:, is) * v1(:, is))
    END DO
!$OMP END DO
!$OMP END PARALLEL

#ifndef SEQUENTIEL
    CALL Reduce(Com, cs_parall_sum, norm )
#endif

    norm = SQRT(norm)

  END FUNCTION Norme2
  
  !!-- Pr�conditionnement de la matrice totale en inversant chaque bloc de la diagonale.
  SUBROUTINE PrecondDiagonal(Mat, rhs)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "PrecondDiagonal"

    ! Type et vocations des Variables d'appel
    !----------------------------------------

    TYPE(Matrice), INTENT(INOUT)                              :: Mat
    REAL, INTENT( IN OUT), DIMENSION(:, : ) :: rhs

    ! Variables locales

    INTEGER :: is, iv, i1, IFin, isd
    REAL, DIMENSION(SIZE(rhs, dim = 1))   :: Ui
    REAL, DIMENSION(:, : ), ALLOCATABLE, SAVE:: Mat2, Mat3
    LOGICAL, SAVE :: initialise = .FALSE.
    INTEGER :: istat3

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF

    IF (.NOT. initialise) THEN
       initialise = .TRUE.
       ALLOCATE(Mat2(SIZE(rhs, dim = 1), SIZE(rhs, dim = 1)), STAT = istat3)
       IF (istat3 /= 0) THEN
          PRINT *, mod_name, " ERREUR : istats(Mat2)=", istat3
          CALL stop_run()
       END IF
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "mat2", SIZE(Mat2))
       END IF
       ALLOCATE(Mat3(SIZE(rhs, dim = 1), SIZE(rhs, dim = 1)), STAT = istat3)
       IF (istat3 /= 0) THEN
          PRINT *, mod_name, " ERREUR : istats(Mat3)=", istat3
          CALL stop_run()
       END IF
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "mat3", SIZE(Mat3))
       END IF
    END IF

!!$     #ifdef VERIFIER
!!$              DO is = 1, Mat%NNin !-- c'est a dire npoint dans le contexte actuel %%
!!$                 isd           = Mat%IDiag(is)
!!$                 PRINT *, "is=", is, "isd=", isd
!!$                 PRINT *, "mat=", Mat%Vals(:, :, isd)
!!$                 PRINT *, "rhs=", rhs(:, is)
!!$print*, is
!!$call tester_nan2(Mat%Vals(:, :, isd),mod_name//" Mat%Vals")
!!$                 CALL tester_nan1(rhs(:, is), mod_name // " rhs mauvais en entree")
!!$              END DO
!!$     #endif

    DO is = 1, Mat%NNin !-- c'est � dire npoint dans le contexte actuel %%
       isd           = Mat%IDiag(is)
       !            PRINT *, "is=", is, "isd=", isd
       !            PRINT *, "Val-in==", Mat%Vals(:, :, isd)
       Mat2          = Inverse(Mat%Vals(:, :, isd))
#ifdef Precision
       IF (debug_precision2) THEN
          WRITE(6, fmt_prec2), mod_name, "isd==", isd, "Sum(Mat2)==", SUM(Mat2)
       END IF
#endif
       Ui           = rhs(:, is)
       rhs(:, is)   = MATMUL(Mat2, Ui)
#ifdef VERIFIER
       CALL tester_nan1(rhs(:, is), mod_name // " rhs mal calcule")
#endif
       i1    = Mat%JPosi(is)
       IFin  = Mat%JPosi(is + 1) -1
       !            PRINT *, "Mat2==", Mat2
       DO iv = i1, IFin
          Mat3 = Mat%Vals(:, :, iv)
          !               PRINT *, "Mat3==", Mat3
          Mat%Vals(:, :, iv) = MATMUL(Mat2, Mat3) !-- %%%%%%%%%%%%%%%%%% Fct de l'optim., inlin� ou pas, r�sultats diff�rents
#ifdef Precision
          IF (debug_precision2) THEN
             WRITE(6, fmt_prec2), mod_name, "iv==", iv, "Sum(Mat(iv))==", SUM(Mat%Vals(:, :, iv))
          END IF
#endif
          !               PRINT *, "iv=", iv
          !               PRINT *, "Vals==", Mat%Vals(:, :, iv)
       END DO
    END DO

    ! Attention: MAT est mal (pas) calcul�e sur les overlaps ....... on fait n'importe quoi %%%%%%
    ! sur les overlaps: �change obligatoire !!!!!!!!!!!! %%%%%%
    !  .... Pour le second membre %%%%%%
    !  .... Pour la matrice on se d�brouille que ca soit masqu�. %%%%%%
    ! i.e. on perd un peu de la pr�cision en //, il faudrait faire l'�change des matrices diagonales sur les bords
  END SUBROUTINE PrecondDiagonal

  !!-- M�thode de Jacobi classique, data%nrelax nombre de relaxations, lu sur fichier.
  SUBROUTINE Jacobi(Com, DATA, Mat, Var, Ukk )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Jacobi"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT( IN OUT)  :: Com
    TYPE(Donnees), INTENT( IN)     :: DATA
    TYPE(Matrice), INTENT( IN)     :: Mat
    TYPE(Variables), INTENT( IN OUT)  :: Var
    REAL, DIMENSION(:, : ), INTENT( IN OUT)  :: Ukk
    REAL, DIMENSION(:, : ), ALLOCATABLE :: Uk
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    INTEGER :: irelax, is, ies
    REAL    :: residu
    INTEGER :: ok, istat
    !----------------------------
    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
#ifdef VERIFIER
    CALL tester_nan2(Var%Flux, mod_name // " var%flux mal calcule")
#endif
    !if (.not.allocated(UK)) then
    ALLOCATE(Uk(data%Nvar, Var%Ncells), STAT = istat)
    IF (istat /= 0) THEN
       PRINT *, mod_name, " ERREUR : istat=", istat
       CALL stop_run()
    END IF
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "uk", SIZE(Uk))
    END IF
    !endif
    ok = 0
    Uk = 0.0
    nb_jacobi = nb_jacobi + 1
    DO irelax = 1, data%Nrelax
       nbrelax = nbrelax + 1


       CALL MatrxDotVecteur(Mat, Uk, Com, Ukk)
       Ukk = Uk - Var%Flux - Ukk

       residu = 0.0
       DO is = 1, Mat%NNin
          DO ies = 1, data%Nvar
             residu = residu + (Ukk(ies, is) - Uk(ies, is)) ** 2
             !             PRINT *, mod_name, " Ukk", is, ies, Ukk(ies, is), Uk(ies, is), Var%Flux(ies, is)
#ifdef VERIFIER
             IF (residu < data%Resdlin) THEN !-- pour declencher une FPE sans se servir de isnan
                ok = ok + 1
             END IF
#endif
          END DO
       END DO

#ifndef SEQUENTIEL
       CALL Reduce(Com, cs_parall_sum, residu )
#endif
       residu  =  SQRT( residu )
       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
#ifndef SEQUENTIEL
       CALL EchangeSolPrepSend( Com, Ukk )
#endif
       Uk(:, : Mat%NNin) = Ukk(:, : Mat%NNin)

#ifndef SEQUENTIEL
       CALL EchangeSolRecv( Com, Uk )
#endif
    END DO
    DEALLOCATE(Uk) 
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "uk")
    END IF
  END SUBROUTINE Jacobi



  !!-- Methode de Gauss-Seidel classique, le residu devant devenir inf�rieur � data%resdlin, lu sur fichier, nb. de relax. data%nrelax
  SUBROUTINE GaussSeidel_orig(Com, DATA, Mat, Var, Vecteur )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GaussSeidel"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT(INOUT)           :: Com
    TYPE(Donnees), INTENT(IN)              :: DATA
    TYPE(Matrice), INTENT(IN)              :: Mat
    TYPE(Variables), INTENT(IN)            :: Var
    REAL, DIMENSION(:, : ), INTENT(INOUT)  :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    INTEGER                                :: is, irelax
    INTEGER                                :: js, iv, i1, I1Fin, l, zz, jk
    REAL                                   :: residu
#ifndef NO_STAT
    INTEGER :: prev_loc
#endif
    ! Variables locales tableaux
    !
    REAL, DIMENSION(SIZE(Vecteur, dim = 1))      :: AVi
    !----------------------------

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF

    nb_gaussseidel = nb_gaussseidel + 1
    IF (module_debug) THEN
       PRINT *, mod_name, " irelax==", irelax
       DO jk = 1, SIZE(Mat%Vals, 3)
          DO l = 1, SIZE(Mat%Vals, 2)
             DO zz = 1, SIZE(Mat%Vals, 1)
                PRINT fmt_prec2, mod_name, "Mat%Vals ", 10000 * zz + l, " ", Mat%Vals(zz, l, jk)
             END DO
          END DO
       END DO
    END IF
    DO irelax = 1, data%Nrelax
       IF (module_debug) THEN
          PRINT *, mod_name, " irelax==", irelax
          DO l = 1, SIZE(vecteur, 2)
             DO zz = 1, SIZE(vecteur, 1)
                PRINT fmt_prec2, mod_name, "vecteur ", 10000 * zz + l, " ", vecteur(zz, l)
             END DO
          END DO
       END IF
       nbrelax = nbrelax + 1
       residu = 0.0


#ifndef SEQUENTIEL
       DO is = 1, Mat%NNsnd
          AVi = 0.0
          i1     =  Mat%JPosi(is)
          I1Fin  =  Mat%JPosi(is + 1) -1
          DO iv = i1, I1Fin
             js    = Mat%JvCell(iv)
             AVi   = AVi + MATMUL(Mat%Vals(:, :, iv), Vecteur(:, js) )
          END DO
          AVi  =  AVi + Var%Flux(:, is )
          residu  = residu + SUM( AVi ** 2 )
          Vecteur(:, is) = Vecteur(:, is) - AVi
       END DO
       CALL EchangeSolPrepSend( Com, Vecteur )
#endif /*-- Not SEQUENTIEL */

#ifndef NO_STAT
       prev_loc = 0
#endif
       DO is = Mat%NNsnd + 1, Mat%NNin
          AVi = 0.0
          i1     =  Mat%JPosi(is)
          I1Fin  =  Mat%JPosi(is + 1) -1
          DO iv = i1, I1Fin
             js    = Mat%JvCell(iv)
             AVi   = AVi + MATMUL(Mat%Vals(:, :, iv), Vecteur(:, js ))
#ifndef NO_STAT
             new_loc = Loc(Mat%Vals(:, :, iv))
             n2_loc = Loc(Vecteur(:, js))
             IF (prev_loc /= 0) THEN
                delta = delta + ABS(new_loc - prev_loc)
                d2 = d2 + ABS(n2_loc - p2_loc)
                nb_larg = nb_larg + 1
             END IF
             prev_loc = new_loc
             p2_loc = n2_loc
#endif
          END DO
          AVi = AVi + Var%Flux(:, is )
          IF (module_debug) THEN
             PRINT *, mod_name, " irelax==", irelax, "is==", is
             DO zz = 1, SIZE(AVi, 1)
                PRINT fmt_prec2, mod_name, "AVi ", zz, " ", AVi(zz)
             END DO
          END IF
          residu  = residu + SUM( AVi ** 2 )
          Vecteur(:, is) = Vecteur(:, is) - AVi
       END DO
#ifndef SEQUENTIEL
       CALL EchangeSolRecv( Com, Vecteur)
       CALL Reduce(Com, cs_parall_sum, residu )
#endif
       residu  = SQRT( residu )

       IF (residu < data%Resdlin) THEN
          EXIT
       END IF

    END DO

  END SUBROUTINE GaussSeidel_orig

  !!-- m�thode des sous-espaces de Krylov
  SUBROUTINE Gmres(Com, DATA, Mat, Var, Vecteur)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Gmres"
    !-------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT(INOUT)              :: Com
    TYPE(Donnees), INTENT(IN)              :: DATA
    TYPE(Matrice), INTENT(IN)              :: Mat
    TYPE(Variables), INTENT(IN)            :: Var
    REAL, DIMENSION(:, : ), INTENT(INOUT)  :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    INTEGER ::  i, j, km, is, irelax, m
    REAL    ::  hij, hjj, hbj, cosx, sinx, residu
    ! Variables locales tableaux
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: Vj, AVj
    REAL, DIMENSION(:, :, : ), ALLOCATABLE    :: v
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: h
    REAL, DIMENSION(: ), ALLOCATABLE    :: Hb, Pcos, Psin, y
    INTEGER, SAVE :: prev_sz1 = 0, prev_sz2 = 0
    !  initialisation
    !----------------------------

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    km = -2000000000 !-- pour faire taire l'analyse de flot
    m = data%MKrylov
    nbgmres = nbgmres + 1
    IF (prev_sz1 /= 0) THEN
       IF (prev_sz1 /= SIZE(Vecteur, dim = 1) .OR. prev_sz2 /= SIZE(Vecteur, dim = 2)) THEN
          nbdiff = nbdiff + 1
       END IF
    END IF
    prev_sz1 = SIZE(Vecteur, dim=1)
    prev_sz2 = SIZE(Vecteur, dim=2)
    ALLOCATE( Vj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))   ) !-- le co�t de ces allocations fixes de fa�on it�rative est sans doute faible (et peut-on le faire en allocation automatique ?)
    ALLOCATE( AVj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))  )
    ALLOCATE( v(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2), m + 1) )
    ALLOCATE( h(m + 1, m), Hb(m + 1), Pcos(m), Psin(m), y(m)) !-- m = Data%MKrylov
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "vj", SIZE(Vj))
       CALL alloc_allocate(mod_name, "avj", SIZE(AVj))
       CALL alloc_allocate(mod_name, "v", SIZE(v))
       CALL alloc_allocate(mod_name, "h", SIZE(h))
       CALL alloc_allocate(mod_name, "hb", SIZE(Hb))
       CALL alloc_allocate(mod_name, "pcos", SIZE(Pcos))
       CALL alloc_allocate(mod_name, "psin", SIZE(Psin))
       CALL alloc_allocate(mod_name, "y", SIZE(y))
    END IF
    !-------------------------------------------------!
    !       V_{1}= (b - Ax_{0})/||(b - Ax_{0})||      !
    ! ------------------------------------------------!
    CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
    Vj   = - Var%Flux - Vj
    !-------------------------------------------------!
    !          calcul de la norme de V_{1}
    ! ------------------------------------------------!
    residu = Norme2(Vj, Com, Mat%NNin)
    ! ------------------------------------------------!
    ! normalisation de V_{1}
    ! ------------------------------------------------!
    IF (residu <= data%Resdlin  ) THEN
       Vecteur = Vecteur + Vj
       DEALLOCATE(Vj, AVj, v, h, Hb, Pcos, Psin, y)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "vj")
          CALL alloc_deallocate(mod_name, "avj")
          CALL alloc_deallocate(mod_name, "v")
          CALL alloc_deallocate(mod_name, "h")
          CALL alloc_deallocate(mod_name, "hb")
          CALL alloc_deallocate(mod_name, "pcos")
          CALL alloc_deallocate(mod_name, "psin")
          CALL alloc_deallocate(mod_name, "y")
       END IF
       RETURN
    END IF
    v(:, :, 1)   = Vj / residu
    Hb(1) = residu
    Hb(2) = 0.0
    DO irelax = 1, data%Nrelax, m !-- m = Data%MKrylov

       DO j = 1, m !-- m = Data%MKrylov
          Vj       = v(:, :, j)
          ! calcul de Av_{j}
          CALL MatrxDotVecteur(Mat, Vj, Com, AVj)
          ! Othogonalisation de Gram-Schmidt modifie
          ! ----------------------------------------
          DO i = 1, j
             Vj       = v(:, :, i)
             ! H(i, j) = VecteurDotVecteur(AVj, Vj, Com, Mat%NNin)
             ! en principe, le reduce est fait dans vectDotVec, mais pour une raison de pref
             ! (latence MPI) on fait pas comme �a mais comme �a:
             h(i, j) = 0.0
             DO is = 1, Mat%NNin
                h(i, j) = h(i, j) + SUM(AVj(:, is) * Vj(:, is))
             END DO
          END DO
#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_sum, h(1: j, j) )
#endif
          Vj       =  AVj
          DO i = 1, j
             Vj    = Vj - h(i, j) * v(:, :, i)
          END DO

          h(j + 1, j) = Norme2(Vj, Com, Mat%NNin)
          residu     = h(j + 1, j)
          IF (residu <= data%Resdlin ) THEN
             km = j
             EXIT
          END IF
          !!    FACTORISATION Rk=Qk*HK-barre :
          !!         Rk est stocke dans Hk-barre        !!
          !!    et CALCUL de g_{k}=Qk*(norme de R_0)*e_1 :
          !!      composante jk           !!
          v(:, :, j + 1) = Vj / h(j + 1, j)
          !!------------------------------------------
          !!......ici on multiplie la nouvelle ligne
          !!        par les rotations precedentes
          !!------------------------------------------
          DO  i = 1, j - 1
             cosx      = Pcos(i)
             sinx      = Psin(i)
             hjj       = h(i, j)
             hij       = h(i + 1, j)
             h(i, j)   =   cosx * hjj + sinx * hij
             h(i + 1, j) = - sinx * hjj + cosx * hij
          END DO
          !!---------------------------------------
          !!       prise en compte de la rotation j
          !!---------------------------------------
          hjj     = h(j, j)
          hij     = h(j + 1, j)
          cosx    =  hjj / SQRT(hjj ** 2 + hij ** 2)
          sinx    =  hij / SQRT(hjj ** 2 + hij ** 2)
          Pcos(j) = cosx
          Psin(j) = sinx
          h(j, j)  = cosx * hjj + sinx * hij
          hbj     = Hb(j)
          Hb(j)   = cosx * Hb(j)
          Hb(j + 1) = - sinx * hbj
          !!-------------------
          !! expression du residu
          !!---------------------
          residu = ABS( Hb(j + 1) )
          km = j   ! Th�oriquement correct mais potentiellement dangereux
          ! car il peut exister des diff�rences entre les residus calcul�s ...
          ! perte de comms
          IF (residu <= data%Resdlin ) THEN
             EXIT
          END IF
       END DO
       !!----------------------------------------------------------!!
       !!    calcul de Y par la resolution du systeme H*Y= Hb      !!
       !!----------------------------------------------------------!!
       y(km) = Hb(km) / h(km, km)
       DO  i = km - 1, 1, -1
          hbj = Hb(i)
          DO  j = i + 1, km
             hbj  = hbj  - h(i, j) * y(j)
          END DO
          y(i) = hbj / h(i, i)
       END DO
       !!----------------------------------------------------------!!
       !!   la solution du syst�me lin�aire est mise dans vecteur  !!
       !!----------------------------------------------------------!!

#ifndef SEQUENTIEL
       DO  i = 1, km
          DO is = 1, Mat%NNsnd !!!!size(vecteur, 2)
             Vecteur(:, is) = Vecteur(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
       CALL EchangeSolPrepSend( Com, Vecteur )
#endif

       DO  i = 1, km
          DO is = Mat%NNsnd + 1, Mat%NNin
             Vecteur(:, is) = Vecteur(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
#ifndef SEQUENTIEL
       CALL EchangeSolRecv( Com, Vecteur)
#endif

       CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
       Vj = - Var%Flux - Vj
       !-------------------------------------------------!
       !          calcul de la norme de V_{1}
       ! ------------------------------------------------!
       residu = Norme2(Vj, Com, Mat%NNin)
       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
       ! ------------------------------------------------!
       ! normalisation de V_{1}
       ! ------------------------------------------------!
       v(:, :, 1)   = Vj / residu
       Hb(1)      = residu
       Hb(2)      = 0.0
    END DO
    DEALLOCATE( Vj, AVj, v, h, Hb, Pcos, Psin, y)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "vj")
       CALL alloc_deallocate(mod_name, "avj")
       CALL alloc_deallocate(mod_name, "v")
       CALL alloc_deallocate(mod_name, "h")
       CALL alloc_deallocate(mod_name, "hb")
       CALL alloc_deallocate(mod_name, "pcos")
       CALL alloc_deallocate(mod_name, "psin")
       CALL alloc_deallocate(mod_name, "y")
    END IF
  END SUBROUTINE Gmres

  SUBROUTINE GmresLGsd(Com, DATA, Mat, Var, Vecteur)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GmresLGsd"
    !-------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT(INOUT)              :: Com
    TYPE(Donnees), INTENT(IN)              :: DATA
    TYPE(Matrice), INTENT(IN)              :: Mat
    TYPE(Variables), INTENT(IN)            :: Var
    REAL, DIMENSION(:, : ), INTENT(INOUT)  :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    INTEGER ::  m, i, j, km, is, irelax
    REAL    ::  hij, hjj, hbj, cosx, sinx, residu, omega
    ! Variables locales tableaux
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: Vj, AVj
    REAL, DIMENSION(:, :, : ), ALLOCATABLE    :: v
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: h
    REAL, DIMENSION(: ), ALLOCATABLE    :: Hb, Pcos, Psin, y
    !  initialisation
    !----------------------------

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    km = -2000000000 !-- pour faire taire l'analyse de flot
    m = data%MKrylov
    ALLOCATE( Vj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))   )
    ALLOCATE( AVj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))  )
    ALLOCATE( v(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2), m + 1) )
    ALLOCATE( h(m + 1, m), Hb(m + 1), Pcos(m), Psin(m), y(m))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "vj", SIZE(Vj))
       CALL alloc_allocate(mod_name, "avj", SIZE(AVj))
       CALL alloc_allocate(mod_name, "v", SIZE(v))
       CALL alloc_allocate(mod_name, "h", SIZE(h))
       CALL alloc_allocate(mod_name, "hb", SIZE(Hb))
       CALL alloc_allocate(mod_name, "pcos", SIZE(Pcos))
       CALL alloc_allocate(mod_name, "psin", SIZE(Psin))
       CALL alloc_allocate(mod_name, "y", SIZE(y))
    END IF
    ! ------------------------------------------------!
    omega   = 1.0
    !-------------------------------------------------!
    !       V_{1}= (b - Ax_{0})/||(b - Ax_{0})||      !
    ! ------------------------------------------------!
    CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
    Vj = - Var%Flux - Vj
    CALL PrecondGSd(Com, Mat, Vj, omega )
    !-------------------------------------------------!
    !          calcul de la norme de V_{1}
    ! ------------------------------------------------!
    residu = Norme2(Vj, Com, Mat%NNin)
    ! ------------------------------------------------!
    ! normalisation de V_{1}
    ! ------------------------------------------------!
    IF (residu <= data%Resdlin  ) THEN
       Vecteur = Vecteur + Vj
       DEALLOCATE(Vj, AVj, v, h, Hb, Pcos, Psin, y)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "vj")
          CALL alloc_deallocate(mod_name, "avj")
          CALL alloc_deallocate(mod_name, "v")
          CALL alloc_deallocate(mod_name, "h")
          CALL alloc_deallocate(mod_name, "hb")
          CALL alloc_deallocate(mod_name, "pcos")
          CALL alloc_deallocate(mod_name, "psin")
          CALL alloc_deallocate(mod_name, "y")
       END IF
       RETURN
    END IF
    v(:, :, 1)   = Vj / residu
    Hb(1) = residu
    Hb(2) = 0.0
    nb_gmreslgsd = nb_gmreslgsd + 1
    DO irelax = 1, data%Nrelax, m
       nbrelax = nbrelax + 1
       DO j = 1, m
          Vj       = v(:, :, j)
          ! calcul de Av_{j}
          CALL MatrxDotVecteur(Mat, Vj, Com, AVj)
          CALL PrecondGSd(Com, Mat, AVj, omega )
          ! Othogonalisation de Gram-Schmidt modifie
          ! ----------------------------------------
          DO i = 1, j
             Vj       = v(:, :, i)
             ! H(i, j) = VecteurDotVecteur(AVj, Vj, Com, Mat%NNin)
             ! en principe, le reduce est fait dans vectDotVec, mais pour une raison de perf
             ! (latence MPI) on fait pas comme �a mais comme �a:
             h(i, j) = 0.0
             DO is = 1, Mat%NNin
                h(i, j) = h(i, j) + SUM(AVj(:, is) * Vj(:, is))
             END DO
          END DO
#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_sum, h(1: j, j) )
#endif
          Vj       =  AVj
          DO i = 1, j
             Vj    = Vj - h(i, j) * v(:, :, i)
          END DO

          h(j + 1, j)   =  Norme2(Vj, Com, Mat%NNin)
          residu     = h(j + 1, j)
          IF (residu <= data%Resdlin ) THEN
             km = j
             EXIT
          END IF
          !!    FACTORISATION Rk=Qk*HK-barre :
          !!         Rk est stocke dans Hk-barre        !!
          !!    et CALCUL de g_{k}=Qk*(norme de R_0)*e_1 :
          !!      composante jk           !!
          v(:, :, j + 1) = Vj / h(j + 1, j)
          !!------------------------------------------
          !!......ici on multiplie la nouvelle ligne
          !!        par les rotations precedentes
          !!------------------------------------------
          DO  i = 1, j - 1
             cosx      = Pcos(i)
             sinx      = Psin(i)
             hjj       = h(i, j)
             hij       = h(i + 1, j)
             h(i, j)   =   cosx * hjj + sinx * hij
             h(i + 1, j) = - sinx * hjj + cosx * hij
          END DO
          !!--------------
          !!       prise en compte de la rotation j
          !!---------------------------------------
          hjj     = h(j, j)
          hij     = h(j + 1, j)
          cosx    =  hjj / SQRT(hjj ** 2 + hij ** 2)
          sinx    =  hij / SQRT(hjj ** 2 + hij ** 2)
          Pcos(j) = cosx
          Psin(j) = sinx
          h(j, j)  = cosx * hjj + sinx * hij
          hbj     = Hb(j)
          Hb(j)   = cosx * Hb(j)
          Hb(j + 1) = - sinx * hbj
          !!-------------------
          !! expression du residu
          !!---------------------
          residu = ABS( Hb(j + 1) )
          km = j
          IF (residu <= data%Resdlin ) THEN
             EXIT
          END IF
       END DO
       !!----------------------------------------------------------!!
       !!    calcul de Y par la resolution du systeme H*Y= Hb      !!
       !!----------------------------------------------------------!!
       y(km) = Hb(km) / h(km, km)
       DO  i = km - 1, 1, -1
          hbj = Hb(i)
          DO  j = i + 1, km
             hbj  = hbj  - h(i, j) * y(j)
          END DO
          y(i) = hbj / h(i, i)
       END DO
       !!----------------------------------------------------------!!
       !!   la solution du syst�me lin�aire est mise dans vecteur  !!
       !!----------------------------------------------------------!!
#ifndef SEQUENTIEL
       DO  i = 1, km
          DO is = 1, Mat%NNsnd       !!!!size(vecteur, 2)
             Vecteur(:, is) = Vecteur(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
       CALL EchangeSolPrepSend( Com, Vecteur )
#endif
       DO  i = 1, km
          DO is = Mat%NNsnd + 1, Mat%NNin
             Vecteur(:, is) = Vecteur(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
#ifndef SEQUENTIEL
       CALL EchangeSolRecv( Com, Vecteur)
#endif
       CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
       Vj = - Var%Flux - Vj
       CALL PrecondGSd(Com, Mat, Vj, omega )
       !-------------------------------------------------!
       !          calcul de la norme de V_{1}
       ! ------------------------------------------------!
       residu = Norme2(Vj, Com, Mat%NNin)
       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
       ! ------------------------------------------------!
       ! normalisation de V_{1}
       ! ------------------------------------------------!
       v(:, :, 1)   = Vj / residu
       Hb(1)      = residu
       Hb(2)      = 0.0
    END DO
    DEALLOCATE( Vj, AVj, v, h, Hb, Pcos, Psin, y)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "vj")
       CALL alloc_deallocate(mod_name, "avj")
       CALL alloc_deallocate(mod_name, "v")
       CALL alloc_deallocate(mod_name, "h")
       CALL alloc_deallocate(mod_name, "hb")
       CALL alloc_deallocate(mod_name, "pcos")
       CALL alloc_deallocate(mod_name, "psin")
       CALL alloc_deallocate(mod_name, "y")
    END IF
  END SUBROUTINE GmresLGsd

  SUBROUTINE GmresRGsd(Com, DATA, Mat, Var, Vecteur)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GmresRGsd"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    INTEGER                    ::  m
    TYPE(MeshCom), INTENT(INOUT)              :: Com
    TYPE(Donnees), INTENT(IN)              :: DATA
    TYPE(Matrice), INTENT(IN)              :: Mat
    TYPE(Variables), INTENT(IN)            :: Var
    REAL, DIMENSION(:, : ), INTENT(INOUT)  :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    INTEGER ::  i, j, km, irelax, is
    REAL    ::  hij, hjj, hbj, cosx, sinx, residu, omega
    ! Variables locales tableaux
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: Vj, AVj
    REAL, DIMENSION(:, :, : ), ALLOCATABLE    :: v
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: h
    REAL, DIMENSION(: ), ALLOCATABLE    :: Hb, Pcos, Psin, y
    !----------------------------
    !  initialisation
    !----------------------------

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    km = -2000000000 !-- pour faire taire l'analyse de flot
    m = data%MKrylov
    ALLOCATE( Vj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))   )
    ALLOCATE( AVj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))  )
    ALLOCATE( v(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2), m + 1) )
    ALLOCATE( h(m + 1, m), Hb(m + 1), Pcos(m), Psin(m), y(m)) !-- %% allocate inutile si l'on sort sur le 1er RETURN
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "vj", SIZE(Vj))
       CALL alloc_allocate(mod_name, "avj", SIZE(AVj))
       CALL alloc_allocate(mod_name, "v", SIZE(v))
       CALL alloc_allocate(mod_name, "h", SIZE(h))
       CALL alloc_allocate(mod_name, "hb", SIZE(Hb))
       CALL alloc_allocate(mod_name, "pcos", SIZE(Pcos))
       CALL alloc_allocate(mod_name, "psin", SIZE(Psin))
       CALL alloc_allocate(mod_name, "y", SIZE(y))
    END IF
    omega   = 1.0
    !-------------------------------------------------!
    !       V_{1}= (b - Ax_{0})/||(b - Ax_{0})||      !
    ! ------------------------------------------------!
    CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
    Vj  = - Var%Flux - Vj
    !-------------------------------------------------!
    !          calcul de la norme de V_{1}
    ! ------------------------------------------------!
    residu = Norme2(Vj, Com, Mat%NNin)
    ! ------------------------------------------------!
    ! normalisation de V_{1}
    ! ------------------------------------------------!
    IF (residu <= data%Resdlin  ) THEN
       Vecteur = Vecteur + Vj
       DEALLOCATE(Vj, AVj, v, h, Hb, Pcos, Psin, y)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "vj")
          CALL alloc_deallocate(mod_name, "avj")
          CALL alloc_deallocate(mod_name, "v")
          CALL alloc_deallocate(mod_name, "h")
          CALL alloc_deallocate(mod_name, "hb")
          CALL alloc_deallocate(mod_name, "pcos")
          CALL alloc_deallocate(mod_name, "psin")
          CALL alloc_deallocate(mod_name, "y")
       END IF
       RETURN
    END IF
    v(:, :, 1)   = Vj / residu
    Hb(1) = residu
    Hb(2) = 0.0
    nb_gmresrgsd = nb_gmresrgsd + 1
    DO irelax = 1, data%Nrelax, m
       nbrelax = nbrelax + 1
       DO j = 1, m
          Vj(:, : ) = v(:, :, j)
          CALL PrecondGSd(Com, Mat, Vj, omega )
          ! calcul de Av_{j}
          CALL MatrxDotVecteur(Mat, Vj, Com, AVj)
          ! Othogonalisation de Gram-Schmidt modifie
          ! ----------------------------------------
          DO i = 1, j
             Vj       = v(:, :, i)
             ! H(i, j) = VecteurDotVecteur(AVj, Vj, Com, Mat%NNin)
             ! en principe, le reduce est fait dans vectDotVec, mais pour une raison de pref
             ! (latence MPI) on fait pas comme �a mais comme �a:
             h(i, j) = 0.0
             DO is = 1, Mat%NNin
                h(i, j) = h(i, j) + SUM(AVj(:, is) * Vj(:, is))
             END DO
          END DO
#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_sum, h(1: j, j) )
#endif
          Vj       =  AVj
          DO i = 1, j
             Vj    = Vj - h(i, j) * v(:, :, i)
          END DO
          h(j + 1, j)   =  Norme2(Vj, Com, Mat%NNin)
          residu     = h(j + 1, j)
          IF (residu <= data%Resdlin ) THEN
             km = j
             EXIT
          END IF
          !!    FACTORISATION Rk=Qk*HK-barre :
          !!         Rk est stocke dans Hk-barre        !!
          !!    et CALCUL de g_{k}=Qk*(norme de R_0)*e_1 :
          !!      composante jk           !!
          v(:, :, j + 1) = Vj / h(j + 1, j)
          !!------------------------------------------
          !!......ici on multiplie la nouvelle ligne
          !!        par les rotations precedentes
          !!------------------------------------------
          DO  i = 1, j - 1
             cosx      = Pcos(i)
             sinx      = Psin(i)
             hjj       = h(i, j)
             hij       = h(i + 1, j)
             h(i, j)   =   cosx * hjj + sinx * hij
             h(i + 1, j) = - sinx * hjj + cosx * hij
          END DO
          !!--------------
          !!       prise en compte de la rotation j
          !!---------------------------------------
          hjj     = h(j, j)
          hij     = h(j + 1, j)
          cosx    =  hjj / SQRT(hjj ** 2 + hij ** 2)
          sinx    =  hij / SQRT(hjj ** 2 + hij ** 2)
          Pcos(j) = cosx
          Psin(j) = sinx
          h(j, j)  = cosx * hjj + sinx * hij
          hbj     = Hb(j)
          Hb(j)   = cosx * Hb(j)
          Hb(j + 1) = - sinx * hbj
          !!-------------------
          !! expression du residu
          !!---------------------
          residu = ABS( Hb(j + 1) )
          km = j
          IF (residu <= data%Resdlin ) THEN
             EXIT
          END IF
       END DO
       !!----------------------------------------------------------!!
       !!    calcul de Y par la resolution du systeme H*Y= Hb      !!
       !!----------------------------------------------------------!!
       y(km) = Hb(km) / h(km, km)
       DO  i = km - 1, 1, -1
          hbj = Hb(i)
          DO  j = i + 1, km
             hbj  = hbj  - h(i, j) * y(j)
          END DO
          y(i) = hbj / h(i, i)
       END DO
       !!----------------------------------------------------------!!
       !!   la solution du systeme lineaire est mise dans vecteur  !!
       !!----------------------------------------------------------!!
       Vj = 0.0
#ifndef SEQUENTIEL
       DO  i = 1, km
          DO is = 1, Mat%NNsnd       !!!!size(vecteur, 2)
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
       CALL EchangeSolPrepSend( Com, Vj )
#endif
       DO  i = 1, km
          DO is = Mat%NNsnd + 1, Mat%NNin
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
#ifndef SEQUENTIEL
       CALL EchangeSolRecv( Com, Vj)
#endif
       CALL PrecondGSd(Com, Mat, Vj, omega )

#ifndef SEQUENTIEL
       Vecteur(:, : Mat%NNsnd) = Vecteur(:, : Mat%NNsnd) + Vj(:, : Mat%NNsnd)
       CALL EchangeSolPrepSend( Com, Vecteur )
       Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) = Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) + Vj(:, Mat%NNsnd+ 1: Mat%NNin)
       CALL EchangeSolRecv( Com, Vecteur )
#else
       Vecteur = Vecteur + Vj
#endif

       CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
       Vj = - Var%Flux - Vj
       !-------------------------------------------------!
       !          calcul de la norme de V_{1}
       ! ------------------------------------------------!
       residu = Norme2(Vj, Com, Mat%NNin)
       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
       ! ------------------------------------------------!
       ! normalisation de V_{1}
       ! ------------------------------------------------!
       v(:, :, 1)   = Vj / residu
       Hb(1)      = residu
       Hb(2)      = 0.0
    END DO
    DEALLOCATE( Vj, AVj, v, h, Hb, Pcos, Psin, y)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "vj")
       CALL alloc_deallocate(mod_name, "avj")
       CALL alloc_deallocate(mod_name, "v")
       CALL alloc_deallocate(mod_name, "h")
       CALL alloc_deallocate(mod_name, "hb")
       CALL alloc_deallocate(mod_name, "pcos")
       CALL alloc_deallocate(mod_name, "psin")
       CALL alloc_deallocate(mod_name, "y")
    END IF
  END SUBROUTINE GmresRGsd


  SUBROUTINE GmresLUpDo(Com, DATA, Mat, Var, Vecteur )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GmresLUpDo"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT(INOUT)              :: Com
    TYPE(Donnees), INTENT(IN)              :: DATA
    TYPE(Matrice), INTENT(IN)              :: Mat
    TYPE(Variables), INTENT(IN)            :: Var
    REAL, DIMENSION(:, : ), INTENT(INOUT)  :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    INTEGER ::  m, i, j, km, is, irelax
    REAL    ::  hij, hjj, hbj, cosx, sinx, residu, omega
    ! Variables locales tableaux
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: Vj, AVj
    REAL, DIMENSION(:, :, : ), ALLOCATABLE    :: v
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: h
    REAL, DIMENSION(: ), ALLOCATABLE    :: Hb, Pcos, Psin, y
    !  initialisation
    !----------------------------

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    km = -2000000000 !-- pour faire taire l'analyse de flot
    m = data%MKrylov
    ALLOCATE( Vj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))   )
    ALLOCATE( AVj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))  )
    ALLOCATE( v(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2), m + 1) )
    ALLOCATE( h(m + 1, m), Hb(m + 1), Pcos(m), Psin(m), y(m)) !-- %% allocate trop t�t si 1er RETURN
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "vj", SIZE(Vj))
       CALL alloc_allocate(mod_name, "avj", SIZE(AVj))
       CALL alloc_allocate(mod_name, "v", SIZE(v))
       CALL alloc_allocate(mod_name, "h", SIZE(h))
       CALL alloc_allocate(mod_name, "hb", SIZE(Hb))
       CALL alloc_allocate(mod_name, "pcos", SIZE(Pcos))
       CALL alloc_allocate(mod_name, "psin", SIZE(Psin))
       CALL alloc_allocate(mod_name, "y", SIZE(y))
    END IF
    ! ------------------------------------------------!
    omega   = 1.0
    !-------------------------------------------------!
    !       V_{1}= (b - Ax_{0})/||(b - Ax_{0})||      !
    ! ------------------------------------------------!
    CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
    Vj = - Var%Flux - Vj
    CALL PrecondUpDo(Com, Mat, Vj )
    !-------------------------------------------------!
    !          calcul de la norme de V_{1}
    ! ------------------------------------------------!
    residu = Norme2(Vj, Com, Mat%NNin)
    ! ------------------------------------------------!
    ! normalisation de V_{1}
    ! ------------------------------------------------!
    IF (residu <= data%Resdlin  ) THEN
       Vecteur = Vecteur + Vj
       DEALLOCATE(Vj, AVj, v, h, Hb, Pcos, Psin, y)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "vj")
          CALL alloc_deallocate(mod_name, "avj")
          CALL alloc_deallocate(mod_name, "v")
          CALL alloc_deallocate(mod_name, "h")
          CALL alloc_deallocate(mod_name, "hb")
          CALL alloc_deallocate(mod_name, "pcos")
          CALL alloc_deallocate(mod_name, "psin")
          CALL alloc_deallocate(mod_name, "y")
       END IF
       RETURN
    END IF
    v(:, :, 1)   = Vj / residu
    Hb(1) = residu
    Hb(2) = 0.0
    nb_gmreslupdo = nb_gmreslupdo + 1
    DO irelax = 1, data%Nrelax, m
       nbrelax = nbrelax + 1
       DO j = 1, m
          Vj       = v(:, :, j)
          ! calcul de Av_{j}
          CALL MatrxDotVecteur(Mat, Vj, Com, AVj)
          CALL PrecondUpDo(Com, Mat, AVj )
          ! Othogonalisation de Gram-Schmidt modifie
          ! ----------------------------------------
          DO i = 1, j
             Vj       = v(:, :, i)
             ! H(i, j) = VecteurDotVecteur(AVj, Vj, Com, Mat%NNin)
             ! en principe, le reduce est fait dans vectDotVec, mais pour une raison de perf
             ! (latence MPI) on fait pas comme �a mais comme �a:
             h(i, j) = 0.0
             DO is = 1, Mat%NNin
                h(i, j) = h(i, j) + SUM(AVj(:, is) * Vj(:, is))
             END DO
          END DO
#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_sum, h(1: j, j) )
#endif
          Vj       =  AVj
          DO i = 1, j
             Vj    = Vj - h(i, j) * v(:, :, i)
          END DO

          h(j + 1, j)   =  Norme2(Vj, Com, Mat%NNin)
          residu     = h(j + 1, j)
          IF (residu <= data%Resdlin ) THEN
             km = j
             EXIT
          END IF
          !!    FACTORISATION Rk=Qk*HK-barre :
          !!         Rk est stocke dans Hk-barre        !!
          !!    et CALCUL de g_{k}=Qk*(norme de R_0)*e_1 :
          !!      composante jk           !!
          v(:, :, j + 1) = Vj / h(j + 1, j)
          !!------------------------------------------
          !!......ici on multiplie la nouvelle ligne
          !!        par les rotations precedentes
          !!------------------------------------------
          DO  i = 1, j - 1
             cosx      = Pcos(i)
             sinx      = Psin(i)
             hjj       = h(i, j)
             hij       = h(i + 1, j)
             h(i, j)   =   cosx * hjj + sinx * hij
             h(i + 1, j) = - sinx * hjj + cosx * hij
          END DO
          !!--------------
          !!       prise en compte de la rotation j
          !!---------------------------------------
          hjj     = h(j, j)
          hij     = h(j + 1, j)
          cosx    =  hjj / SQRT(hjj ** 2 + hij ** 2)
          sinx    =  hij / SQRT(hjj ** 2 + hij ** 2)
          Pcos(j) = cosx
          Psin(j) = sinx
          h(j, j)  = cosx * hjj + sinx * hij
          hbj     = Hb(j)
          Hb(j)   = cosx * Hb(j)
          Hb(j + 1) = - sinx * hbj
          !!-------------------
          !! expression du residu
          !!---------------------
          residu = ABS( Hb(j + 1) )
          km = j
          IF (residu <= data%Resdlin ) THEN
             EXIT
          END IF
       END DO
       !!----------------------------------------------------------!!
       !!    calcul de Y par la resolution du systeme H*Y= Hb      !!
       !!----------------------------------------------------------!!
       y(km) = Hb(km) / h(km, km)
       DO  i = km - 1, 1, -1
          hbj = Hb(i)
          DO  j = i + 1, km
             hbj  = hbj  - h(i, j) * y(j)
          END DO
          y(i) = hbj / h(i, i)
       END DO
       !!----------------------------------------------------------!!
       !!   la solution du systeme lineaire est mise dans vecteur  !!
       !!----------------------------------------------------------!!
#ifndef SEQUENTIEL
       DO  i = 1, km
          DO is = 1, Mat%NNsnd       !!!!size(vecteur, 2)
             Vecteur(:, is) = Vecteur(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
       CALL EchangeSolPrepSend( Com, Vecteur )
#endif
       DO  i = 1, km
          DO is = Mat%NNsnd + 1, Mat%NNin
             Vecteur(:, is) = Vecteur(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
#ifndef SEQUENTIEL
       CALL EchangeSolRecv( Com, Vecteur)
#endif
       CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
       Vj = - Var%Flux - Vj
       CALL PrecondUpDo(Com, Mat, Vj)
       !-------------------------------------------------!
       !          calcul de la norme de V_{1}
       ! ------------------------------------------------!
       residu = Norme2(Vj, Com, Mat%NNin)
       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
       ! ------------------------------------------------!
       ! normalisation de V_{1}
       ! ------------------------------------------------!
       v(:, :, 1)   = Vj / residu
       Hb(1)      = residu
       Hb(2)      = 0.0
    END DO
    !write(98, *)irelax
    DEALLOCATE( Vj, AVj, v, h, Hb, Pcos, Psin, y)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "vj")
       CALL alloc_deallocate(mod_name, "avj")
       CALL alloc_deallocate(mod_name, "v")
       CALL alloc_deallocate(mod_name, "h")
       CALL alloc_deallocate(mod_name, "hb")
       CALL alloc_deallocate(mod_name, "pcos")
       CALL alloc_deallocate(mod_name, "psin")
       CALL alloc_deallocate(mod_name, "y")
    END IF
  END SUBROUTINE GmresLUpDo

  SUBROUTINE GmresRUpDo(Com, DATA, Mat, Var, Vecteur )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GmresRUpDo"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    INTEGER                    ::  m
    TYPE(MeshCom), INTENT(INOUT)           :: Com
    TYPE(Donnees), INTENT(IN)              :: DATA
    TYPE(Matrice), INTENT(IN)              :: Mat
    TYPE(Variables), INTENT(IN)            :: Var
    REAL, DIMENSION(:, : ), INTENT(INOUT)  :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    INTEGER ::  i, j, km, irelax, is
    REAL    ::  hij, hjj, hbj, cosx, sinx, residu, omega
    ! Variables locales tableaux
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: Vj, AVj
    REAL, DIMENSION(:, :, : ), ALLOCATABLE    :: v
    REAL, DIMENSION(:, : ), ALLOCATABLE    :: h
    REAL, DIMENSION(: ), ALLOCATABLE    :: Hb, Pcos, Psin, y
    !----------------------------
    !  initialisation
    !----------------------------

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    km = -2000000000 !-- pour faire taire l'analyse de flot
    m = data%MKrylov
    ALLOCATE( Vj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))   )
    ALLOCATE( AVj(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2))  )
    ALLOCATE( v(SIZE(Vecteur, dim = 1), SIZE( Vecteur, dim = 2), m + 1) )
    ALLOCATE( h(m + 1, m), Hb(m + 1), Pcos(m), Psin(m), y(m)) !-- %% allocate trop t�t si 1er RETURN
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "vj", SIZE(Vj))
       CALL alloc_allocate(mod_name, "avj", SIZE(AVj))
       CALL alloc_allocate(mod_name, "v", SIZE(v))
       CALL alloc_allocate(mod_name, "h", SIZE(h))
       CALL alloc_allocate(mod_name, "hb", SIZE(Hb))
       CALL alloc_allocate(mod_name, "pcos", SIZE(Pcos))
       CALL alloc_allocate(mod_name, "psin", SIZE(Psin))
       CALL alloc_allocate(mod_name, "y", SIZE(y))
    END IF
    omega   = 1.0
    !-------------------------------------------------!
    !       V_{1}= (b - Ax_{0})/||(b - Ax_{0})||      !
    ! ------------------------------------------------!
    CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
    Vj = - Var%Flux - Vj
    !-------------------------------------------------!
    !          calcul de la norme de V_{1}
    ! ------------------------------------------------!
    residu = Norme2(Vj, Com, Mat%NNin)
    ! ------------------------------------------------!
    ! normalisation de V_{1}
    ! ------------------------------------------------!
    IF (residu <= data%Resdlin  ) THEN
       Vecteur = Vecteur + Vj
       DEALLOCATE(Vj, AVj, v, h, Hb, Pcos, Psin, y)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "vj")
          CALL alloc_deallocate(mod_name, "avj")
          CALL alloc_deallocate(mod_name, "v")
          CALL alloc_deallocate(mod_name, "h")
          CALL alloc_deallocate(mod_name, "hb")
          CALL alloc_deallocate(mod_name, "pcos")
          CALL alloc_deallocate(mod_name, "psin")
          CALL alloc_deallocate(mod_name, "y")
       END IF
       RETURN
    END IF
    v(:, :, 1)   = Vj / residu
    Hb(1) = residu
    Hb(2) = 0.0
    nb_gmresrupdo = nb_gmresrupdo + 1
    DO irelax = 1, data%Nrelax, m
       nbrelax = nbrelax + 1
       DO j = 1, m
          Vj(:, : ) = v(:, :, j)
          CALL PrecondUpDo(Com, Mat, Vj)
          ! calcul de Av_{j}
          CALL MatrxDotVecteur(Mat, Vj, Com, AVj)
          ! Othogonalisation de Gram-Schmidt modifie
          ! ----------------------------------------
          DO i = 1, j
             Vj       = v(:, :, i)
             ! H(i, j) = VecteurDotVecteur(AVj, Vj, Com, Mat%NNin)
             ! en principe, le reduce est fait dans vectDotVec, mais pour une raison de pref
             ! (latence MPI) on fait pas comme �a mais comme �a:
             h(i, j) = 0.0
             DO is = 1, Mat%NNin
                h(i, j) = h(i, j) + SUM(AVj(:, is) * Vj(:, is))
             END DO
          END DO
#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_sum, h(1: j, j) )
#endif
          Vj       =  AVj
          DO i = 1, j
             Vj    = Vj - h(i, j) * v(:, :, i)
          END DO
          h(j + 1, j)   =  Norme2(Vj, Com, Mat%NNin)
          residu     = h(j + 1, j)
          IF (residu <= data%Resdlin ) THEN
             km = j
             EXIT
          END IF
          !!    FACTORISATION Rk=Qk*HK-barre :
          !!         Rk est stocke dans Hk-barre        !!
          !!    et CALCUL de g_{k}=Qk*(norme de R_0)*e_1 :
          !!      composante jk           !!
          v(:, :, j + 1) = Vj / h(j + 1, j)
          !!------------------------------------------
          !!......ici on multiplie la nouvelle ligne
          !!        par les rotations precedentes
          !!------------------------------------------
          DO  i = 1, j - 1
             cosx      = Pcos(i)
             sinx      = Psin(i)
             hjj       = h(i, j)
             hij       = h(i + 1, j)
             h(i, j)   =   cosx * hjj + sinx * hij
             h(i + 1, j) = - sinx * hjj + cosx * hij
          END DO
          !!--------------
          !!       prise en compte de la rotation j
          !!---------------------------------------
          hjj     = h(j, j)
          hij     = h(j + 1, j)
          cosx    =  hjj / SQRT(hjj ** 2 + hij ** 2)
          sinx    =  hij / SQRT(hjj ** 2 + hij ** 2)
          Pcos(j) = cosx
          Psin(j) = sinx
          h(j, j)  = cosx * hjj + sinx * hij
          hbj     = Hb(j)
          Hb(j)   = cosx * Hb(j)
          Hb(j + 1) = - sinx * hbj
          !!-------------------
          !! expression du residu
          !!---------------------
          residu = ABS( Hb(j + 1) )
          km = j
          IF (residu <= data%Resdlin ) THEN
             EXIT
          END IF
       END DO
       !!----------------------------------------------------------!!
       !!    calcul de Y par la resolution du systeme H*Y= Hb      !!
       !!----------------------------------------------------------!!
       y(km) = Hb(km) / h(km, km)
       DO  i = km - 1, 1, -1
          hbj = Hb(i)
          DO  j = i + 1, km
             hbj  = hbj  - h(i, j) * y(j)
          END DO
          y(i) = hbj / h(i, i)
       END DO
       !!----------------------------------------------------------!!
       !!   la solution du systeme lineaire est mise dans vecteur  !!
       !!----------------------------------------------------------!!
       Vj = 0.0
#ifndef SEQUENTIEL
       DO  i = 1, km
          DO is = 1, Mat%NNsnd       !!!!size(vecteur, 2)
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
       CALL EchangeSolPrepSend( Com, Vj )
#endif
       DO  i = 1, km
          DO is = Mat%NNsnd + 1, Mat%NNin
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO
#ifndef SEQUENTIEL
       CALL EchangeSolRecv( Com, Vj)
#endif
       CALL PrecondUpDo(Com, Mat, Vj)

#ifndef SEQUENTIEL
       Vecteur(:, : Mat%NNsnd) = Vecteur(:, : Mat%NNsnd) + Vj(:, : Mat%NNsnd)
       CALL EchangeSolPrepSend( Com, Vecteur )
       Vecteur(:, Mat%NNsnd+1: Mat%NNin) = Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) + Vj(:, Mat%NNsnd+ 1: Mat%NNin)
       CALL EchangeSolRecv( Com, Vecteur )
#else
       Vecteur = Vecteur + Vj
#endif
       CALL MatrxDotVecteur(Mat, Vecteur, Com, Vj)
       Vj = - Var%Flux - Vj
       !-------------------------------------------------!
       !          calcul de la norme de V_{1}
       ! ------------------------------------------------!
       residu = Norme2(Vj, Com, Mat%NNin)
       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
       ! ------------------------------------------------!
       ! normalisation de V_{1}
       ! ------------------------------------------------!
       v(:, :, 1)   = Vj / residu
       Hb(1)      = residu
       Hb(2)      = 0.0
    END DO
    DEALLOCATE( Vj, AVj, v, h, Hb, Pcos, Psin, y)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "vj")
       CALL alloc_deallocate(mod_name, "avj")
       CALL alloc_deallocate(mod_name, "v")
       CALL alloc_deallocate(mod_name, "h")
       CALL alloc_deallocate(mod_name, "hb")
       CALL alloc_deallocate(mod_name, "pcos")
       CALL alloc_deallocate(mod_name, "psin")
       CALL alloc_deallocate(mod_name, "y")
    END IF
  END SUBROUTINE GmresRUpDo


  !!-- Bi Conjugate Gradients Square
  !!-- Gradient conjugu� classique.
  !!-- Appelle matrxdotvecteur, vecteurdotvecteur, norme2
  !!-- Contr�l� par data%resdlin et data%nrelax.
  SUBROUTINE BiCGs(Com, DATA, Mat, Var, Vecteur )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "BiCGs"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT( IN OUT) :: Com
    TYPE(Donnees), INTENT( IN)    :: DATA
    TYPE(Matrice), INTENT( IN)    :: Mat
    TYPE(Variables), INTENT( IN OUT) :: Var
    REAL, DIMENSION(:, : ), INTENT( IN OUT) :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    REAL, DIMENSION(1: SIZE(Vecteur, 1), 1: SIZE(Vecteur, 2)) :: dx, p, r, r0, q, u, v, r1
    INTEGER :: irelax
    REAL    :: residu, rho, rhom1, beta, alpha
    !----------------------------

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    CALL MatrxDotVecteur(Mat, Vecteur, Com, r)
    r = - Var%Flux - r
    r0 = r
    q = 0.0
    p = 0.0
    rho = 1.0

    !*************************************

    !    DO irelax = 2, Data%Nrelax

    nb_bicgs = nb_bicgs + 1
    DO irelax = 1, data%Nrelax
       nbrelax = nbrelax + 1
       rhom1 = rho
       rho  = VecteurDotVecteur(r, r0, Com, Mat%NNin)
       beta = rho / rhom1
       u = r + beta * q
       p = u + beta * (q + beta * p)
       CALL MatrxDotVecteur(Mat, p, Com, v)
       alpha = rho / VecteurDotVecteur(v, r0, Com, Mat%NNin)
       q = u - alpha * v
       dx = alpha * (u + q)
#ifndef SEQUENTIEL
       Vecteur(:, : Mat%NNsnd) = Vecteur(:, : Mat%NNsnd) + dx(:, : Mat%NNsnd)
       CALL EchangeSolPrepSend( Com, Vecteur )
       Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) = Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) + dx(:, Mat%NNsnd+ 1: Mat%NNin)
       CALL EchangeSolRecv( Com, Vecteur)
#else
       Vecteur = Vecteur + dx
#endif
       CALL MatrxDotVecteur(Mat, dx, Com, r1)
       r = r - r1
       residu = Norme2(r, Com, Mat%NNin)

       IF (residu < data%Resdlin) THEN
          EXIT
       END IF

    END DO
  END SUBROUTINE BiCGs

  !!-- Bi Conjugate Gradients Square
  !!-- Gradient conjugu� pr�conditionn�. omega non pass�, donc vaut 1.0 dans precondgsd
  !!-- Appelle matrxdotvecteur, precondgsd, vecteurdotvecteur, norme2
  SUBROUTINE BiCgsLGsd(Com, DATA, Mat, Var, Vecteur )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "BiCgsLGsd"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT( IN OUT) :: Com
    TYPE(Donnees), INTENT( IN)    :: DATA
    TYPE(Matrice), INTENT( IN)    :: Mat
    TYPE(Variables), INTENT( IN OUT) :: Var
    REAL, DIMENSION(:, : ), INTENT( IN OUT) :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    REAL, DIMENSION(1: SIZE(Vecteur, 1), 1: SIZE(Vecteur, 2)) :: dx, p, r, r0, q, u, v, dx1
    INTEGER :: irelax
    REAL    :: residu, rho, rhom1, beta, alpha

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    !----------------------------
    ! Ro = So = L^-1  (b-A Xo)
    CALL MatrxDotVecteur(Mat, Vecteur, Com, r)
    r = - Var%Flux - r
    CALL PrecondGSd(Com, Mat, r)
    r0 = r
    q = 0.0
    p = 0.0
    rho = 1.0
    nb_bicgslgsd = nb_bicgslgsd + 1
    DO irelax = 1, data%Nrelax
       nbrelax = nbrelax + 1
       rhom1 = rho
       rho  = VecteurDotVecteur(r, r0, Com, Mat%NNin)
       beta = rho / rhom1
       u = r + beta * q
       p = u + beta * (q + beta * p)
       !V= L^-1 A U^-1 P
       CALL MatrxDotVecteur(Mat, p, Com, v)
       CALL PrecondGSd(Com, Mat, v)
       alpha = rho / VecteurDotVecteur(v, r0, Com, Mat%NNin)
       q = u - alpha * v ! 2 U - alpha V
       dx = alpha * (u + q) ! alpha Q
       ! X = X + U^-1 DX
#ifndef SEQUENTIEL
       Vecteur(:, : Mat%NNsnd) = Vecteur(:, : Mat%NNsnd) + dx(:, : Mat%NNsnd)
       CALL EchangeSolPrepSend( Com, Vecteur )
       Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) = Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) + dx(:, Mat%NNsnd+ 1: Mat%NNin)
       CALL EchangeSolRecv( Com, Vecteur)
#else
       Vecteur = Vecteur + dx
#endif
       ! R = R - L^-1 A U^-1 DX
       CALL MatrxDotVecteur(Mat, dx, Com, dx1)
       dx = dx1
       CALL PrecondGSd(Com, Mat, dx)
       r = r - dx
       residu = Norme2(r, Com, Mat%NNin)

       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
    END DO
  END SUBROUTINE BiCgsLGsd

  !!-- Bi Conjugate Gradients Square
  !!-- Gradient conjugu� pr�conditionn� par ILU; contr�l� par data%nrelax et data%resdlin
  SUBROUTINE BiCgsLUpDo(Com, DATA, Mat, Var, Vecteur )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "BiCgsLUpDo"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT( IN OUT) :: Com
    TYPE(Donnees), INTENT( IN)    :: DATA
    TYPE(Matrice), INTENT( IN)    :: Mat
    TYPE(Variables), INTENT( IN OUT) :: Var
    REAL, DIMENSION(:, : ), INTENT( IN OUT) :: Vecteur
    !----------------------------
    ! Variables locales scalaires
    !----------------------------
    REAL, DIMENSION(1: SIZE(Vecteur, 1), 1: SIZE(Vecteur, 2)) :: dx, p, r, r0, q, u, v, dx1
    INTEGER :: irelax
#ifdef Precision         
    INTEGER :: ii, jj, kl
#endif
    REAL    :: residu, rho, rhom1, beta, alpha

    !----------------------------
    ! Ro = So = L^-1  (b-A Xo)

#ifdef Precision
    IF (.FALSE. .AND. debug_precision) THEN
       WRITE(6, fmt_prec2) mod_name, "irelax==", 0, "Sum(flux)==", SUM(Var%Flux)
       WRITE(6, fmt_prec2) mod_name, "irelax==", 0, "Sum(Mat)==", SUM(Mat%Vals)
       WRITE(6, fmt_prec2) mod_name, "irelax==", 0, "Sum(Vecteur)==", SUM(Vecteur)
    END IF
    IF (debug_precision .AND. .FALSE.) THEN
       DO kl = 1, SIZE(Mat%Vals, 3)
          DO jj = 1, SIZE(Mat%Vals, 2)
             DO ii = 1, SIZE(Mat%Vals, 1)
                PRINT *, mod_name, " ii, jj, kl", ii, jj, kl
                WRITE(6, fmt_prec1) mod_name, "mats/init==", Mat%Vals(ii, jj, kl)
             END DO
          END DO
       END DO
    END IF
#endif
    CALL MatrxDotVecteur(Mat, Vecteur, Com, r)
    r = - Var%Flux - r
#ifdef Precision
    IF (debug_precision .AND. .FALSE.) THEN
       DO jj = 1, SIZE(u, 2)
          DO ii = 1, SIZE(u, 1)
             PRINT *, mod_name, " ii, jj", ii, jj
             WRITE(6, fmt_prec1) mod_name, "flux==", Var%Flux(ii, jj)
             WRITE(6, fmt_prec1) mod_name, "r==", r(ii, jj)
          END DO
       END DO
    END IF
#endif
    CALL PrecondUpDo(Com, Mat, r)
#ifdef Precision
    IF (debug_precision .AND. .FALSE.) THEN
       DO jj = 1, SIZE(u, 2)
          DO ii = 1, SIZE(u, 1)
             PRINT *, mod_name, " ii, jj", ii, jj
             WRITE(6, fmt_prec1) mod_name, "r/precond==", r(ii, jj)
          END DO
       END DO
    END IF
    IF (debug_precision .AND. .FALSE.) THEN
       WRITE(6, fmt_prec2) mod_name, "irelax==", 0, "Sum(r)==", SUM(r)
    END IF
#endif
    r0 = r
    q = 0.0
    p = 0.0
    rho = 1.0
    nb_bicgslupdo = nb_bicgslupdo + 1
    DO irelax = 1, data%Nrelax
       nbrelax = nbrelax + 1
       rhom1 = rho
       !            IF (irelax >= 1) THEN
       !               STOP "seen"
       !            END IF
#ifdef Precision
       IF (.FALSE. .AND. debug_precision) THEN
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "rhom1==", rhom1
       END IF
#endif
       rho  = VecteurDotVecteur(r, r0, Com, Mat%NNin)
#ifdef Precision
       IF (.FALSE. .AND. debug_precision) THEN
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "rho==", rho
       END IF
#endif
       beta = rho / rhom1
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "beta==", beta
       END IF
#endif
       u = r + beta * q !-- u, r et q sont des matrices
#ifdef VERIFIER
       CALL tester_nan2(r, mod_name // " r mal calcule en interne")
       CALL tester_nan2(q, mod_name // " q mal calcule en interne")
#endif
#ifdef Precision
       DO jj = 1, SIZE(u, 2)
          DO ii = 1, SIZE(u, 1)
             IF (debug_precision .AND. .FALSE.) THEN
                PRINT *, mod_name, " ii, jj", ii, jj
                WRITE(6, fmt_prec1) mod_name, "r==", r(ii, jj)
                WRITE(6, fmt_prec1) mod_name, "q==", q(ii, jj)
                WRITE(6, fmt_prec1) mod_name, "u==", u(ii, jj)
             END IF
          END DO
       END DO
       IF (debug_precision .AND. .FALSE.) THEN
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "MinVal(r)==", MINVAL(r)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "MinVal(q)==", MINVAL(q)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "MinVal(u)==", MINVAL(u)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Maxval(r)==", MAXVAL(r)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Maxval(q)==", MAXVAL(q)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Maxval(u)==", MAXVAL(u)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Sum(r)==", SUM(r)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Sum(q)==", SUM(q)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Sum(u)==", SUM(u)
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Sum(u/1)==", SUM(r) + beta * SUM(q)
       END IF
#endif
       p = u + beta * (q + beta * p)
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Sum(p)==", SUM(p)
       END IF
#endif
       !V= L^-1 A U^-1 P
       CALL MatrxDotVecteur(Mat, p, Com, v)
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "Sum(v)==", SUM(v)
       END IF
#endif
       CALL PrecondUpDo(Com, Mat, v)
       alpha = rho / VecteurDotVecteur(v, r0, Com, Mat%NNin)
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          WRITE(6, fmt_prec2) mod_name, "irelax==", irelax, "alpha==", alpha
       END IF
#endif
       q = u - alpha * v ! 2 U - alpha V
       dx = alpha * (u + q) ! alpha Q
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          DO jj = 1, SIZE(u, 2)
             DO ii = 1, SIZE(u, 1)
                PRINT *, mod_name, " ii, jj", ii, jj
                WRITE(6, fmt_prec1) mod_name, "dx/in==", dx(ii, jj)
                WRITE(6, fmt_prec1) mod_name, "v==", v(ii, jj)
                WRITE(6, fmt_prec1) mod_name, "u==", u(ii, jj)
                WRITE(6, fmt_prec1) mod_name, "q==", q(ii, jj)
             END DO
          END DO
       END IF
#endif
       ! X = X + U^-1 DX
#ifndef SEQUENTIEL
       Vecteur(:, : Mat%NNsnd) = Vecteur(:, : Mat%NNsnd) + dx(:, : Mat%NNsnd)
       CALL EchangeSolPrepSend( Com, Vecteur )
       Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) = Vecteur(:, Mat%NNsnd+ 1: Mat%NNin) + dx(:, Mat%NNsnd+ 1: Mat%NNin)
       CALL EchangeSolRecv( Com, Vecteur)
#else
       Vecteur = Vecteur + dx
#endif
       ! R = R - L^-1 A U^-1 DX
       CALL MatrxDotVecteur(Mat, dx, Com, dx1)
       dx = dx1
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          IF (irelax == 9) THEN
             DO kl = 1, SIZE(Mat%Vals, 3)
                DO jj = 1, SIZE(Mat%Vals, 2)
                   DO ii = 1, SIZE(Mat%Vals, 1)
                      PRINT *, mod_name, " ii, jj, kl", ii, jj, kl
                      WRITE(6, fmt_prec1) mod_name, "mats==", Mat%Vals(ii, jj, kl)
                   END DO
                END DO
             END DO
          END IF
          DO jj = 1, SIZE(u, 2)
             DO ii = 1, SIZE(u, 1)
                PRINT *, mod_name, " ii, jj", ii, jj
                WRITE(6, fmt_prec1) mod_name, "dx/mxv==", dx(ii, jj)
             END DO
          END DO
       END IF
#endif
       CALL PrecondUpDo(Com, Mat, dx)
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          DO jj = 1, SIZE(u, 2)
             DO ii = 1, SIZE(u, 1)
                PRINT *, mod_name, " ii, jj", ii, jj
                WRITE(6, fmt_prec1) mod_name, "dx/pupdo==", dx(ii, jj)
             END DO
          END DO
       END IF
#endif
       r = r - dx
#ifdef Precision
       IF (debug_precision .AND. .FALSE.) THEN
          DO jj = 1, SIZE(u, 2)
             DO ii = 1, SIZE(u, 1)
                PRINT *, mod_name, " ii, jj", ii, jj
                WRITE(6, fmt_prec1) mod_name, "dx==", dx(ii, jj)
                WRITE(6, fmt_prec1) mod_name, "r/dx==", r(ii, jj)
             END DO
          END DO
       END IF
#endif
       residu = Norme2(r, Com, Mat%NNin)
#ifdef Precision
       IF (.FALSE. .AND. debug_precision) THEN
          WRITE(6, fmt_prec2), mod_name, "irelax==", irelax, "residu==", residu
       END IF
#endif

       IF (residu < data%Resdlin) THEN
          EXIT
       END IF
    END DO
    !OFF     STOP "seen"
  END SUBROUTINE BiCgsLUpDo


  !!-- Pr�conditionnement du vecteur, omega argument optionnel
  !!-- NOTA: Gsd == SOR (version non sym de SSOR)
  SUBROUTINE PrecondGSd(Com, Mat, Vecteur, omega)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "PrecondGSd"
    ! Type et vocations des Variables d'appel
    !------------------------------ ----------
    TYPE(MeshCom), INTENT(INOUT)                      :: Com
    TYPE(Matrice), INTENT(IN)                      :: Mat
    REAL, INTENT( IN OUT), DIMENSION(:, : ) :: Vecteur
    REAL, OPTIONAL, INTENT(IN)                    :: omega
    ! Variables locales scalaires
    !----------------------------
    INTEGER                                  :: is, js
    INTEGER                                  :: iv, i1, IFin
    REAL                                     :: lomega, eps

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF

    eps = 0.0000001
    lomega = 1.0
    IF (PRESENT(omega) ) THEN
       lomega = omega
    END IF
    !    IF (lomega .LT. eps )  RETURN
    !**************
    !*  Descente  *
    !**************
#ifndef SEQUENTIEL
    DO is = 1, Mat%NNsnd
       i1   =  Mat%JPosi(is)
       IFin =  Mat%JPosi(is + 1) -1
       DO iv = i1, IFin
          js = Mat%JvCell(iv)
          IF (js >= is ) THEN
             EXIT
          END IF
          Vecteur(:, is) = Vecteur(:, is )  - lomega * MATMUL(Mat%Vals(:, :, iv), Vecteur(:, js) )
       END DO
    END DO
    CALL EchangeSolPrepSend( Com, Vecteur )
#endif
    DO is = Mat%NNsnd + 1, Mat%NNin
       i1   =  Mat%JPosi(is)
       IFin =  Mat%JPosi(is + 1) -1
       DO iv = i1, IFin
          js = Mat%JvCell(iv)
          IF (js >= is ) THEN
             EXIT
          END IF
          Vecteur(:, is) = Vecteur(:, is )  - lomega * MATMUL(Mat%Vals(:, :, iv), Vecteur(:, js) )
       END DO
    END DO
#ifndef SEQUENTIEL
    CALL EchangeSolRecv( Com, Vecteur)
#endif
    !**************
    !*  Remont�e  *
    !**************
    DO is = Mat%NNin, 1, -1
       IFin                 =  Mat%JPosi(is + 1) -1
       i1   =  Mat%JPosi(is)
       DO iv = i1, IFin
          js = Mat%JvCell(iv)
          IF (js <= is) THEN
             EXIT
          END IF
          Vecteur(:, is) = Vecteur(:, is )  - lomega * MATMUL(Mat%Vals(:, :, iv), Vecteur(:, js) )
       END DO
    END DO
#ifndef SEQUENTIEL
    CALL EchangeSol   ( Com, Vecteur)
#endif
  END SUBROUTINE PrecondGSd


  !!-- Construction de et stockage de la matrice pour un pr�conditionnement ILU(0)
  !!-- La double boucle de recherche peut-elle �tre rendue fixe ? Non
  SUBROUTINE ILUMatrix_orig(Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ILUMatrix"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(Matrice), INTENT( IN OUT)                      :: Mat
    ! Variables locales scalaires
    !----------------------------
    INTEGER :: i, j, k, l
    INTEGER :: ki, ij, kj, ik, kk
    INTEGER :: i1, IFin, j1, JFin
    ! Variables locales tableaux
    REAL, DIMENSION( SIZE(Mat%Vals, dim = 1), SIZE( Mat%Vals, dim = 1)  ) :: Akk, Aik, Akj, Aij, d
    ! L = matrice triangulaire inf � diagonale de blocs identit� (non stock�e)
    ! U = matrice triangulaire sup. Ses blocs diagonaux sont stock�s d�j� invers�s (optim) !!!!!!!!!!
    ! Stockage dans le profil initial: ILU(0) = Incompl�te L U de degr� 0.
    ! Degr� 1 = remplisage des voisins, 2 des voisins et des voisins des voisins... jusqu'au remplissage total

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF

    !        Mat%ILUVals(:, :, : ) = Mat%Vals(:, :, : ) !-- bogue ifort sur les trop gros tableaux %%%%%%
    DO i = 1, SIZE(Mat%ILUVals, dim=3)
       Mat%ILUVals(:, :, i) = Mat%Vals(:, :, i)
    END DO

    boucle_k : DO k = 1, Mat%NNin
       kk  = Mat%IDiag(k)
       Akk = Mat%ILUVals(:, :, kk)
       d   = Inverse( Akk )
       Mat%ILUVals(:, :, kk) = d ! Anticipation de l'inversion de l'�tape (des �tapes...) de descente
       IFin = Mat%JPosi(k + 1) - 1
       DO ki = kk + 1, IFin      ! Parcours de la partie inf de la colonne k
          i   =  Mat%JvCell(ki) ! Car le STOCKAGE est sym�trique
          ik  =  Mat%IvPos(ki)  ! place dans le stockage ligne des elts de la col k
          Aik = Mat%ILUVals(:, :, ik)
          Mat%ILUVals(:, :, ik) = MATMUL(Aik, d)
          i1     = kk + 1
          j1     = Mat%JPosi(i)
          JFin   = Mat%JPosi(i + 1) -1
          boucle_ij : DO ij = j1, JFin
             j = Mat%JvCell(ij)
             IF (j <= k) THEN
                CYCLE
             END IF
             !***********************
             !* RECHERCHE DE Akj
             DO kj = i1, IFin
                l = Mat%JvCell(kj) ! Akl = Mat%ILUVals(kj)
                IF (l == j) THEN
                   Akj = Mat%ILUVals(:, :, kj)
                   Aij = Mat%ILUVals(:, :, ij)
                   Mat%ILUVals(:, :, ij) = Aij - MATMUL(Aik, Akj)
                   i1 = kj + 1 ! ij croit donc j aussi, donc l aussi et kj aussi: on peut repartir du suivant!!!
                   CYCLE boucle_ij
                END IF
             END DO
             !* FIN RECHERCHE DE Akj
             !***********************
          END DO boucle_ij
       END DO !-- boucle sur ki = kk + 1, IFin
    END DO boucle_k

  END SUBROUTINE ILUMatrix_orig

  !!-- Construction de et stockage de la matrice pour un pr�conditionnement ILU(0)
  !!-- La double boucle de recherche peut-elle �tre rendue fixe ? Non
!!$  SUBROUTINE ILUMatrix_blas(Mat)
!!$    ! emploi des Blas
!!$    ! 2010 R. Abgrall
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "ILUMatrix"
!!$    ! Type et vocations des Variables d'appel
!!$    !----------------------------------------
!!$    TYPE(Matrice), INTENT( IN OUT)                      :: Mat
!!$    ! Variables locales scalaires
!!$    !----------------------------
!!$    INTEGER :: i, j, k, l
!!$    INTEGER :: ki, ij, kj, ik, kk
!!$    INTEGER :: i1, IFin, j1, JFin
!!$    INTEGER:: taille
!!$    ! Variables locales tableaux
!!$    REAL, DIMENSION( SIZE(Mat%Vals, dim = 1), SIZE( Mat%Vals, dim = 1)  ) :: Akk, Aik, Akj, Aij, d
!!$    ! L = matrice triangulaire inf � diagonale de blocs identit� (non stock�e)
!!$    ! U = matrice triangulaire sup. Ses blocs diagonaux sont stock�s d�j� invers�s (optim) !!!!!!!!!!
!!$    ! Stockage dans le profil initial: ILU(0) = Incompl�te L U de degr� 0.
!!$    ! Degr� 1 = remplisage des voisins, 2 des voisins et des voisins des voisins... jusqu'au remplissage total
!!$    REAL, DIMENSION(:),  POINTER  :: mat_a
!!$    REAL, DIMENSION(:), POINTER  :: Vect_x
!!$
!!$    taille = SIZE(mat%vals,1)
!!$    IF (debug) THEN
!!$       PRINT *, mod_name, " DEBUT"
!!$    END IF
!!$
!!$    !    ALLOCATE(mat_a(SIZE(mat%vals,1)*SIZE(mat%vals,3)*SIZE(mat%vals,2)))
!!$    !    ALLOCATE(vect_x(SIZE(mat%vals,3)*SIZE(mat%vals,1)))
!!$
!!$    !        Mat%ILUVals(:, :, : ) = Mat%Vals(:, :, : ) !-- bogue ifort sur les trop gros tableaux %%%%%%
!!$    DO i = 1, SIZE(Mat%ILUVals, dim=3)
!!$       Mat%ILUVals(:, :, i) = Mat%Vals(:, :, i)
!!$    END DO
!!$
!!$    boucle_k : DO k = 1, Mat%NNin
!!$       kk  = Mat%IDiag(k)
!!$       Akk = Mat%ILUVals(:, :, kk)
!!$       d   = Inverse( Akk )
!!$       Mat%ILUVals(:, :, kk) = d ! Anticipation de l'inversion de l'�tape (des �tapes...) de descente
!!$       IFin = Mat%JPosi(k + 1) - 1
!!$       DO ki = kk + 1, IFin      ! Parcours de la partie inf de la colonne k
!!$          i   =  Mat%JvCell(ki) ! Car le STOCKAGE est sym�trique
!!$          ik  =  Mat%IvPos(ki)  ! place dans le stockage ligne des elts de la col k
!!$          Aik = Mat%ILUVals(:, :, ik)
!!$          Mat%ILUVals(:, :, ik) = MATMUL(Aik, d)
!!$          i1     = kk + 1
!!$          j1     = Mat%JPosi(i)
!!$          JFin   = Mat%JPosi(i + 1) -1
!!$          boucle_ij : DO ij = j1, JFin
!!$             j = Mat%JvCell(ij)
!!$             IF (j <= k) THEN
!!$                CYCLE
!!$             END IF
!!$             !***********************
!!$             !* RECHERCHE DE Akj
!!$             DO kj = i1, IFin
!!$                l = Mat%JvCell(kj) ! Akl = Mat%ILUVals(kj)
!!$                IF (l == j) THEN
!!$                   Akj = Mat%ILUVals(:, :, kj)
!!$                   Aij = Mat%ILUVals(:, :, ij)
!!$                   Mat%ILUVals(:, :, ij) = Aij - MATMUL(Aik, Akj)
!!$                   i1 = kj + 1 ! ij croit donc j aussi, donc l aussi et kj aussi: on peut repartir du suivant!!!
!!$                   CYCLE boucle_ij
!!$                END IF
!!$             END DO
!!$             !* FIN RECHERCHE DE Akj
!!$             !***********************
!!$          END DO boucle_ij
!!$       END DO !-- boucle sur ki = kk + 1, IFin
!!$    END DO boucle_k
!!$
!!$  END SUBROUTINE ILUMatrix_blas
!!$


  !!-- Construction de et stockage de la matrice pour un pr�conditionnement SSOR (fa�on Ludo)
  SUBROUTINE SSORMatrix(Mat )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "SSORMatrix"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(Matrice), INTENT( IN OUT)                      :: Mat
    ! Variables locales scalaires
    !----------------------------
    INTEGER :: i, k
    INTEGER :: ki, ik, kk, ii
    INTEGER :: IFin
    ! Variables locales tableaux
    REAL, DIMENSION( SIZE(Mat%Vals, dim = 1), SIZE( Mat%Vals, dim = 1)  ) :: Akk, Aik, Dkk, invar
    ! L = matrice triangulaire inf � diagonale de blocs identit� (non stock�e)
    ! U = matrice triangulaire sup. Ses blocs diagonaux sont stock�s d�j� invers�s (optim) !!!!!!!!!!
    ! Stockage dans le profil initial: ILU(0) = Incompl�te L U de degr� 0.
    ! Degr� 1 = remplisage des voisins, 2 des voisins et des voisins des voisins... jusqu'au remplissage total
    LOGICAL, SAVE :: said = .FALSE.

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF
    IF (.NOT. said) THEN
       said = .TRUE.
       PRINT *, mod_name, " ATTENTION ATTENTION ATTENTION : verifier l'optimisation ", SIZE(Mat%Vals, 1)
    END IF

    Mat%ILUVals(:, :, : ) = Mat%Vals(:, :, : )

    boucle_k : DO k = 1, Mat%NNtot!-1
       kk  = Mat%IDiag(k)
       Akk = Mat%ILUVals(:, :, kk)
       Dkk   = Inverse( Akk )
       Mat%ILUVals(:, :, kk) = Dkk ! Anticipation de l'inversion de l'�tape (des �tapes...) de descente
       IFin = Mat%JPosi(k + 1) -1
       invar = MATMUL( Dkk, Akk) !-- Matmul(Dkk, Akk) est un invariant de boucle
       DO ki = kk + 1, IFin      ! Parcours de la partie inf de la colonne k
          i   =  Mat%JvCell(ki) ! Car le STOCKAGE est sym�trique
          ik  =  Mat%IvPos(ki)  ! place dans le stockage ligne des elts de la col k
          Aik = Mat%ILUVals(:, :, ik)
          Mat%ILUVals(:, :, ik) = MATMUL(Aik, Dkk)
          ii = Mat%IDiag(i)
          Mat%ILUVals(:, :, ii) = Mat%ILUVals(:, :, ii) - MATMUL(Aik, invar)
       END DO
    END DO boucle_k
  END SUBROUTINE SSORMatrix


  !!-- Pr�conditionnement ILU.
  !!-- Blocs diag de U d�j� invers�s !!!
  !!-- Blocs diag de L = identit�

  SUBROUTINE PrecondUpDo(Com, Mat, Vecteur )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "PrecondUpDo"

    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(MeshCom), INTENT(INOUT)                                  :: Com
    TYPE(Matrice), INTENT(IN)                                  :: Mat
    REAL, INTENT( IN OUT), DIMENSION(:, : )                   :: Vecteur

    ! Variables locales scalaires
    INTEGER :: is, js
    INTEGER :: i1, IFin, jvi

    IF (debug) THEN
       PRINT *, mod_name, " DEBUT"
    END IF

    ! Blocs diag de U d�j� invers�s !!!
    ! Blocs diag de L = identit�
    !**************
    !*  Descente  *
    !**************
#ifndef SEQUENTIEL
    Down1_is : DO is = 1, Mat%NNsnd
       i1                   =  Mat%JPosi(is)
       IFin                 =  Mat%JPosi(is + 1) - 1
       Down1_js : DO jvi = i1, IFin
          js = Mat%JvCell(jvi)
          IF (js >= is) THEN
             EXIT Down1_js
          END IF
          Vecteur(:, is )  = Vecteur(:, is )  - MATMUL(Mat%ILUVals(:, :, jvi), Vecteur(:, js ))
       END DO Down1_js
    END DO Down1_is
    CALL EchangeSolPrepSend( Com, Vecteur )
#endif
    Down2_is : DO is = Mat%NNsnd + 1, Mat%NNin
       i1                   =  Mat%JPosi(is)
       IFin                 =  Mat%JPosi(is + 1) - 1
       Down2_js : DO jvi = i1, IFin
          js = Mat%JvCell(jvi)
          IF (js >= is) THEN
             EXIT Down2_js
          END IF
          Vecteur(:, is )  = Vecteur(:, is )  - MATMUL(Mat%ILUVals(:, :, jvi), Vecteur(:, js ))
       END DO Down2_js
    END DO Down2_is
#ifndef SEQUENTIEL
    CALL EchangeSolRecv( Com, Vecteur )
#endif
    !**************
    !*  Remont�e  *
    !**************
    Up_is : DO is = Mat%NNin, 1, -1
       i1                   =  Mat%JPosi(is)
       IFin                 =  Mat%JPosi(is + 1) -1
       Up_js : DO jvi = i1, IFin
          js = Mat%JvCell(jvi)
          IF (js <= is) THEN
             EXIT Up_js
          END IF
          Vecteur(:, is )  = Vecteur(:, is )  - MATMUL(Mat%ILUVals(:, :, jvi), Vecteur(:, js ))
       END DO Up_js
       Vecteur(:, is ) = MATMUL(Mat%ILUVals(:, :, Mat%IDiag(is)), Vecteur(:, is ) )
    END DO Up_is
#ifndef SEQUENTIEL
    CALL EchangeSol   ( Com, Vecteur )
#endif
  END SUBROUTINE PrecondUpDo

  SUBROUTINE statistiques_linalg()
    CHARACTER(LEN = *), PARAMETER :: mod_name = "statistiques_linalg"
    IF (nb_larg > 0) THEN
       PRINT *, mod_name, " delta(gauss_seidel/mat)==", delta, "nb_larg==", nb_larg, "moy==", Float(delta) / Float(nb_larg)
       PRINT *, mod_name, " d2(gauss_seidel/vecteur)==", d2, "nb_larg==", nb_larg, "moy==", Float(d2) / Float(nb_larg)
    ELSE
       PRINT *, mod_name, " delta==", delta, "nb_larg==", nb_larg
    END IF
    IF (nbgmres /= 0 .AND. nbdiff == 0) THEN
       PRINT *, mod_name, " ATTENTION,", nbgmres, "appels a gmres de meme taille"
    ELSE
       PRINT *, mod_name, nbgmres, "appels a gmres,", nbdiff, "changement(s) de taille"
    END IF

    IF (nb_jacobi /= 0) THEN
       PRINT *, mod_name, "jacobi", nbrelax, nb_jacobi, Float(nbrelax) / Float(nb_jacobi)
    END IF
    IF (nb_gaussseidel /= 0) THEN
       PRINT *, mod_name, " gaussseidel nbrelax=", nbrelax, nb_gaussseidel, Float(nbrelax) / Float(nb_gaussseidel)
    END IF
    IF (nb_gmreslgsd /= 0) THEN
       PRINT *, mod_name, " gmreslgsd nbrelax=", nbrelax, nb_gmreslgsd, Float(nbrelax) / Float(nb_gmreslgsd)
    END IF
    IF (nb_gmresrgsd /= 0) THEN
       PRINT *, mod_name, " gmresrgsd nbrelax=", nbrelax, nb_gmresrgsd, Float(nbrelax) / Float(nb_gmresrgsd)
    END IF
    IF (nb_gmreslupdo /= 0) THEN
       PRINT *, mod_name, " gmreslupdo nbrelax=", nbrelax, nb_gmreslupdo, Float(nbrelax) / Float(nb_gmreslupdo)
    END IF
    IF (nb_gmresrupdo /= 0) THEN
       PRINT *, mod_name, " gmresrupdo nbrelax=", nbrelax, nb_gmresrupdo, Float(nbrelax) / Float(nb_gmresrupdo)
    END IF
    IF (nb_bicgs /= 0) THEN
       PRINT *, mod_name, " bicgs nbrelax=", nbrelax, nb_bicgs, Float(nbrelax) / Float(nb_bicgs)
    END IF
    IF (nb_bicgslgsd /= 0) THEN
       PRINT *, mod_name, " bicgslgsd nbrelax=", nbrelax, nb_bicgslgsd, Float(nbrelax) / Float(nb_bicgslgsd)
    END IF
    IF (nb_bicgslupdo /= 0) THEN
       PRINT *, mod_name, " bicgslupdo nbrelax=", nbrelax, nb_bicgslupdo, Float(nbrelax) / Float(nb_bicgslupdo)
    END IF

  END SUBROUTINE statistiques_linalg

END MODULE LinAlg

