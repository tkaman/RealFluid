!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga, M.Papin                                 */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                  2000-2008 INRIA Futur pui BSO, EPI Scalapplix            */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  !************************************************************
  !*                                                          *
  !!@author Mikael PAPIN & rev. B. Nkonga    14 novembre 2005 *
  !*                                                          *
  !************************************************************

MODULE Assemblage

  USE Lestypes
  USE LibComm

  IMPLICIT NONE

  INTEGER, PRIVATE, SAVE :: blksize
  INTEGER, SAVE, PRIVATE :: search = 0
  INTEGER, SAVE, PRIVATE :: calls = 0

  INTEGER, SAVE, PRIVATE :: prev_loc = 0
  INTEGER, SAVE, PRIVATE :: new_loc = 0
  INTEGER(KIND = 8), SAVE, PRIVATE :: delta = 0
  INTEGER, SAVE, PRIVATE :: nb_larg = 0

CONTAINS

  !*****************************************************************
  !!-- Assembler Aii, Aij, Aji, Ajj / i < j
  SUBROUTINE assemblage_addmulti( Aii, Aij, Aji, Ajj, Iin, Jin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_addmulti"
    INTEGER,                       INTENT( IN)   :: Iin, Jin
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Aii, Aij, Aji, Ajj
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is
#ifdef DEBUG_ALGO
    ! Verification des hypoth�se
    IF (Iin >= Jin ) THEN
       PRINT *, mod_name, " ERREUR : In >= Jin au lieu de < "
       CALL stop_run()
    END IF
#endif
    i1   = Mat%Idiag(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    Mat%Vals(:, :, i1) = Mat%Vals(:, :, i1) + Aii
    DO is = i1 + 1, IFin
       ! On cherche la place dans le stockage ligne de ... elt de la colone Jin
       IF (Mat%Jvcell(is) == Jin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) + Aij
          EXIT
       END IF
    END DO

    i1   = Mat%Jposi(Jin)
    IFin = Mat%Idiag(Jin) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Iin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) + Aji
          EXIT
       END IF
    END DO

    Mat%Vals(:, :, IFin + 1) = Mat%Vals(:, :, IFin + 1) + Ajj

  END SUBROUTINE assemblage_addmulti

  !!-- Ajoute des matrices blocs � la matrice principale
  SUBROUTINE assemblage_addmulticons( Aii, Aij, Iin, Jin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_addmulticons"
    ! Assembler Aii, Aij, Aji, Ajj / i < j
    !    avec      Aji = - Aii et Ajj = - Aij
    INTEGER,                       INTENT( IN)   :: Iin, Jin
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Aii, Aij
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is
#ifdef DEBUG_ALGO
    ! Verification des hypoth�se
    IF (Iin >= Jin ) THEN
       PRINT *, mod_name, " ERREUR : In >= Jin au lieu de < "
       CALL stop_run()
    END IF
#endif
    i1 = Mat%Idiag(Iin)
    IFin = Mat%Jposi(Iin + 1) -1

    Mat%Vals(:, :, i1) = Mat%Vals(:, :, i1) + Aii
    DO is = i1 + 1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) + Aij
          EXIT
       END IF
    END DO

    i1 = Mat%Jposi(Jin)
    IFin = Mat%Idiag(Jin) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Iin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) - Aii
          EXIT
       END IF
    END DO
    Mat%Vals(:, :, IFin + 1) = Mat%Vals(:, :, IFin + 1) - Aij

  END SUBROUTINE assemblage_addmulticons

  !!-- Assembler Aii, Aij, Aji, Ajj / i < j
  !!--    avec      Aji = - Aii et Ajj = - Aij
  SUBROUTINE assemblage_addmulticonsNc( Aii, Aij, Ajj, Aji, Iin, Jin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_addmulticonsNc"
    INTEGER,                       INTENT( IN)   :: Iin, Jin
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Aii, Aij, Ajj, Aji
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is
#ifdef DEBUG_ALGO
    ! Verification des hypoth�se
    IF (Iin >= Jin ) THEN
       PRINT *, mod_name, " ERREUR : In >= Jin au lieu de < "
       CALL stop_run()
    END IF
#endif
    i1 = Mat%Idiag(Iin)
    IFin = Mat%Jposi(Iin + 1) -1

    Mat%Vals(:, :, i1) = Mat%Vals(:, :, i1) + Aii
    DO is = i1 + 1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) + Aij
          EXIT
       END IF
    END DO

    i1 = Mat%Jposi(Jin)
    IFin = Mat%Idiag(Jin) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Iin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) - Aji
          EXIT
       END IF
    END DO
    Mat%Vals(:, :, IFin + 1) = Mat%Vals(:, :, IFin + 1) - Ajj

  END SUBROUTINE assemblage_addmulticonsNc

  !!-- Soustrait des matrices blocs � la matrice principale
  ! Assembler Aii, Aij, Aji, Ajj / i < j
  !    avec      Aii = - Aii et Aij = - Aij
  !    avec      Aji =   Aii et Ajj =   Aij
  SUBROUTINE assemblage_submulticons(Aii, Aij, Iin, Jin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_submulticons"

    INTEGER, INTENT( IN)   :: Iin, Jin
    REAL, DIMENSION(:, : ), INTENT( IN)   :: Aii, Aij
    TYPE(Matrice), INTENT( IN OUT) :: Mat

    INTEGER :: i1, IFin, is

#ifdef DEBUG_ALGO
    ! V�rification des hypoth�ses
    IF (Iin >= Jin ) THEN
       PRINT *, mod_name, " ERREUR : In >= Jin au lieu de < ", Iin, Jin
       CALL stop_run()
    END IF
#endif

    i1 = Mat%Idiag(Iin)
    IFin = Mat%Jposi(Iin + 1) -1

    Mat%Vals(:, :, i1) = Mat%Vals(:, :, i1) - Aii
    DO is = i1 + 1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) - Aij
          EXIT
       END IF
    END DO

    i1 = Mat%Jposi(Jin)
    IFin = Mat%Idiag(Jin) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Iin) THEN
          Mat%Vals(:, :, is) = Mat%Vals(:, :, is) + Aii
          EXIT
       END IF
    END DO
    Mat%Vals(:, :, IFin + 1) = Mat%Vals(:, :, IFin + 1) + Aij

  END SUBROUTINE assemblage_submulticons

  !!-- Addition sur un bloc diagonal
  SUBROUTINE assemblage_addDiag(Matelem, Iin, Mat, pos)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_addDiag"

    INTEGER,                INTENT( IN)   :: Iin
    REAL, DIMENSION(:, : ), INTENT( IN)   :: Matelem
    TYPE(Matrice),          INTENT( IN OUT) :: Mat
    INTEGER, INTENT(OUT), OPTIONAL :: pos

    INTEGER :: is

    is               = Mat%Idiag(Iin)
    IF (Present(pos)) THEN
       pos = is
    ELSE
       Mat%Vals(:, :, is) = Mat%Vals(:, :, is) + Matelem
    END IF

  END SUBROUTINE assemblage_addDiag


  !!-- Soustraction sur un bloc diagonal
  SUBROUTINE assemblage_subDiag(Matelem, Iin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_subDiag"

    INTEGER,                       INTENT( IN)   :: Iin
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Matelem
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: is

    is               = Mat%Idiag(Iin)
    Mat%Vals(:, :, is) = Mat%Vals(:, :, is) - Matelem

  END SUBROUTINE assemblage_subDiag

  !!-- mise � a*Id du bloc diagonal
  !!-- Initialise la matrice carr� blksize X blksize pour l'indice iin (argument), � une matrice diagonale de coefficient a (argument)
  SUBROUTINE assemblage_setidenta(a, Iin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_setidenta"
    REAL,           INTENT( IN)    :: a
    INTEGER,        INTENT( IN)    :: Iin
    TYPE(Matrice),  INTENT( IN OUT) :: Mat
    INTEGER :: is, k
    is               = Mat%Idiag(Iin)
    Mat%Vals(:, :, is) = 0.0
    DO k = 1, blksize
       Mat%Vals(k, k, is) = a
    END DO

  END SUBROUTINE assemblage_setidenta

  !!-- Soustraction d'une matrice �l�mentaire, avec une recherche lineaire d'indice qui pourrait �tre factoris�e (%%%%)
  !!-- (si le maillage, m�me d�formable, ne change pas de structure).
  SUBROUTINE assemblage_sub(Matelem, Iin, Jin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_sub"

    INTEGER,                       INTENT( IN)   :: Iin, Jin
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Matelem
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is, itrouv

    i1   = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1

    itrouv = -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          itrouv = is
          EXIT
       END IF
    END DO
    IF (itrouv == -1) THEN
       PRINT *, mod_name, " ALG-ERREUR"
       CALL stop_run()
    END IF
    Mat%Vals(:, :, itrouv) = Mat%Vals(:, :, itrouv) - Matelem

  END SUBROUTINE assemblage_sub


  !!-- mise � Id du bloc diag et � z�ro du reste de la ligne
  SUBROUTINE assemblage_setidentblokline(Iin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "assemblage_setidentblokline"
    INTEGER,        INTENT( IN)    :: Iin
    TYPE(Matrice),  INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, id, k, is
    i1 = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    id = Mat%Idiag(Iin)
    DO is = i1, IFin
       Mat%Vals(:, :, is) = 0.0
       IF (is == id) THEN
          DO k = 1, Mat%blksize !blksize !dante
             Mat%Vals(k, k, is) = 1.0
          END DO
       END IF
    END DO
  END SUBROUTINE assemblage_setidentblokline

  !!-- mise � Id du bloc diag et � z�ro du reste de la ligne pour une variable donn�e
  SUBROUTINE assemblage_setidentvarline(Iin, ivar, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_setidentvarline"
    INTEGER,        INTENT( IN)    :: Iin, ivar
    TYPE(Matrice),  INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, id, is
    i1 = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    id = Mat%Idiag(Iin)
    DO is = i1, IFin
       Mat%Vals(ivar, :, is) = 0.0
       IF (is == id) THEN
          Mat%Vals(ivar, ivar, is) = 1.0
       END IF
    END DO
  END SUBROUTINE assemblage_setidentvarline

  SUBROUTINE assemblage_replacevar(Matelem, Iin, Jin, ivar, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_replacevar"
    INTEGER,                       INTENT( IN)   :: Iin, Jin, ivar
    REAL,           DIMENSION(: ), INTENT( IN)   :: Matelem
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is, itrouv
    i1   = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    itrouv = -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          itrouv = is
          EXIT
       END IF
    END DO
    IF (itrouv == -1) THEN
       PRINT *, mod_name, " ALG-ERREUR"
       CALL stop_run()
    END IF
    Mat%Vals(ivar, :, itrouv) = Matelem
  END SUBROUTINE assemblage_replacevar

  SUBROUTINE assemblage_nullifyExtDiag(Iin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_nullifyExtDiag"
    INTEGER,                       INTENT( IN)   :: Iin
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is
    i1 = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) /= Iin) THEN
          Mat%Vals(:, :, is) = 0.0
       END IF
    END DO
  END SUBROUTINE assemblage_nullifyExtDiag

  SUBROUTINE assemblage_nullifyExtDiagVar(Iin, ivar, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_nullifyExtDiagVar"
    INTEGER,                       INTENT( IN)   :: Iin, ivar
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is
    i1 = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) /= Iin) THEN
          Mat%Vals(ivar, :, is) = 0.0
       END IF
    END DO
  END SUBROUTINE assemblage_nullifyExtDiagVar

  SUBROUTINE assemblage_nullifyExtAndReplaceDiagVar(Matelem, Iin, ivar, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_nullifyExtAndReplaceDiagVar"

    REAL,           DIMENSION(: ), INTENT( IN)   :: Matelem
    INTEGER,                       INTENT( IN)   :: Iin, ivar
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is
    i1 = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) /= Iin) THEN
          Mat%Vals(ivar, :, is) = 0.0
       ELSE
          Mat%Vals(ivar, :, is) = Matelem
       END IF
    END DO

  END SUBROUTINE assemblage_nullifyExtAndReplaceDiagVar

  SUBROUTINE assemblage_nullifyExtAndReplaceDiag(Matelem, Iin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_nullifyExtAndReplaceDiag"
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Matelem
    INTEGER,                       INTENT( IN)   :: Iin
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is

    i1 = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) /= Iin) THEN
          Mat%Vals(:, :, is) = 0.0
       ELSE
          Mat%Vals(:, :, is) = Matelem
       END IF
    END DO
  END SUBROUTINE assemblage_nullifyExtAndReplaceDiag

  !!-- Mise � ze�ro des blocs extra diag et remplacement du bloc diag de la ligne I
  !!-- sur les variables autoris�es par MASK(:)
  !!-- %%%% nom trop long (norme Fortran 90 <= 31 caract�res)
  SUBROUTINE assemblage_nullifyExtAndReplaceDiagMask(Matelem, Iin, Mask, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_nullifyExtAndReplaceDiagMask"

    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Matelem
    LOGICAL,        DIMENSION(: ), INTENT( IN)   :: Mask
    INTEGER,                       INTENT( IN)   :: Iin
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is, k
    i1 = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) /= Iin) THEN
          DO k = 1, blksize
             IF (Mask(k)) THEN
                Mat%Vals(k, :, is) = 0.0
             END IF
          END DO
       ELSE
          DO k = 1, blksize
             IF (Mask(k)) THEN
                Mat%Vals(k, :, is) = Matelem(k, : )
             END IF
          END DO
       END IF
    END DO

  END SUBROUTINE assemblage_nullifyExtAndReplaceDiagMask

  SUBROUTINE return_blocDiag(Jac, i, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/return_blocDiag"
    INTEGER,             INTENT( IN)  :: i
    REAL, DIMENSION(:, : ), INTENT( OUT) :: Jac
    TYPE(Matrice),       INTENT( IN)  :: Mat
    INTEGER                         :: id

    id  = Mat%Idiag(i)
    Jac = Mat%Vals(:, :, id)
  END SUBROUTINE return_blocDiag


  SUBROUTINE assemblage_nullifyvar(Iin, Jin, ivar, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_nullifyvar"
    INTEGER,        INTENT( IN)   :: Iin, Jin, ivar
    TYPE(Matrice),  INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is, itrouv
    i1   = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    itrouv = -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          itrouv = is
          EXIT
       END IF
    END DO
    IF (itrouv == -1) THEN
       PRINT *, mod_name, " ALG-ERREUR"
       CALL stop_run()
    END IF
    Mat%Vals(ivar, :, itrouv) = 0.0
  END SUBROUTINE assemblage_nullifyvar

  !!-- mise Id du bloc diag
  SUBROUTINE assemblage_setident(Iin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "assemblage_setident"
    INTEGER,        INTENT( IN)    :: Iin
    TYPE(Matrice),  INTENT( IN OUT) :: Mat
    INTEGER :: is, k
    is                = Mat%Idiag(Iin)
    Mat%Vals(:, :, is)  = 0.0
    DO k = 1, blksize
       Mat%Vals(k, k, is) = 1.0
    END DO

  END SUBROUTINE assemblage_setident


  SUBROUTINE assemblage_nullify(Iin, Jin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_nullify"

    INTEGER,        INTENT( IN)    :: Iin, Jin
    TYPE(Matrice),  INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is, itrouv
    i1   = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    itrouv = -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          itrouv = is
          EXIT
       END IF
    END DO
    IF (itrouv == -1) THEN
       PRINT *, mod_name, " ALG-ERREUR"
       CALL stop_run()
    END IF
    Mat%Vals(:, :, itrouv) = 0.0

  END SUBROUTINE assemblage_nullify


  !!-- Addition d'une matrice �l�mentaire, avec une recherche lineaire d'indice qui pourrait �tre factoris�e (%%%%????)
  !!-- (si le maillage, m�me d�formable, ne change pas de structure).
  SUBROUTINE assemblage_add(Matelem, Iin, Jin, Mat, position)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_add"

    INTEGER, INTENT( IN)   :: Iin, Jin
    REAL, DIMENSION(:, : ), INTENT( IN)   :: Matelem
    TYPE(Matrice), INTENT( IN OUT) :: Mat
    INTEGER, INTENT(OUT), OPTIONAL :: position

    INTEGER :: i1, IFin, is, itrouv

    i1   = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    itrouv = -1
    calls = calls + 1
    DO is = i1, IFin
       search = search + 1
       IF (Mat%Jvcell(is) == Jin) THEN
          itrouv = is
          EXIT
       END IF
    END DO
    IF (itrouv == -1) THEN
       PRINT *, mod_name, " ALG-ERREUR"
       CALL stop_run()
    END IF

    IF (Present(position)) THEN
       position = itrouv
    ELSE
       Mat%Vals(:, :, itrouv) = Mat%Vals(:, :, itrouv) + Matelem

#ifndef NO_STAT
       new_loc = Loc(Mat%Vals(1, 1, itrouv))
       IF (prev_loc > 0) THEN
          delta = delta + Abs(new_loc - prev_loc)
          nb_larg = nb_larg + 1
       END IF
       prev_loc = new_loc
#endif
    END IF

  END SUBROUTINE assemblage_add

  !!-- Addition d'une matrice �l�mentaire multipli�e par un scalaire, avec une recherche lineaire d'indice qui pourrait �tre factoris�e (%%%%????)
  !!-- (si le maillage, m�me d�formable, ne change pas de structure).
  SUBROUTINE assemblage_addmult(Matelem, scalaire, Iin, Jin, Mat, inverse)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_addmult"

    INTEGER,                       INTENT( IN)   :: Iin, Jin
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Matelem
    REAL,                          INTENT( IN)   :: scalaire
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    LOGICAL, INTENT(IN), OPTIONAL :: inverse

    INTEGER :: i1, IFin, is, itrouv

    i1   = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    itrouv = -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          itrouv = is
          EXIT
       END IF
    END DO
    IF (itrouv == -1) THEN
       PRINT *, mod_name, " ALG-ERREUR"
       CALL stop_run()
    END IF
    IF (Present(inverse) .AND. inverse) THEN
       Mat%Vals(:, :, itrouv) = Mat%Vals(:, :, itrouv) + Matelem / scalaire
    ELSE
       Mat%Vals(:, :, itrouv) = Mat%Vals(:, :, itrouv) + Matelem * scalaire
    END IF

  END SUBROUTINE assemblage_addmult


  SUBROUTINE assemblage_replace(Matelem, Iin, Jin, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/assemblage_replace"

    INTEGER,                       INTENT( IN)   :: Iin, Jin
    REAL,           DIMENSION(:, : ), INTENT( IN)   :: Matelem
    TYPE(Matrice),                 INTENT( IN OUT) :: Mat
    INTEGER :: i1, IFin, is, itrouv
    i1   = Mat%Jposi(Iin)
    IFin = Mat%Jposi(Iin + 1) -1
    itrouv = -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == Jin) THEN
          itrouv = is
          EXIT
       END IF
    END DO
    IF (itrouv == -1) THEN
       PRINT *, mod_name, " ALG-ERREUR"
       CALL stop_run()
    END IF
    Mat%Vals(:, :, itrouv) = Matelem

  END SUBROUTINE assemblage_replace

  SUBROUTINE return_bloc(Jac, i, j, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/return_bloc"
    INTEGER,             INTENT( IN)  :: i, j
    REAL, DIMENSION(:, : ), INTENT( OUT) :: Jac
    TYPE(Matrice),       INTENT( IN)  :: Mat
    INTEGER                         :: i1, IFin, is
    i1   = Mat%Jposi(i)
    IFin = Mat%Jposi(i + 1) -1
    DO is = i1, IFin
       IF (Mat%Jvcell(is) == j) THEN
          EXIT
       END IF
    END DO
    Jac = Mat%Vals(:, :, is)

  END SUBROUTINE return_bloc


  !!-- Initialisation de Mat%vals � 0
  SUBROUTINE initcoeftab(Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/initcoeftab"
    TYPE(Matrice), INTENT( IN OUT) :: Mat

    blksize = Size(Mat%Vals, 1) !-- variable r�manente
    Mat%Vals(:, :, : ) = 0.0
  END SUBROUTINE initcoeftab


  !!-- annule la matrice
  SUBROUTINE resetcoeftab(Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/resetcoeftab"
    TYPE(Matrice),   INTENT( IN OUT) :: Mat
    Mat%Vals(:, :, : ) = 0.0
  END SUBROUTINE resetcoeftab

  !!-- statistiques sur la recherche par listes (et non par h�chage)
  SUBROUTINE statistiques()
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/statistiques"
    IF (calls > 0) THEN
       PRINT *, mod_name, " searches==", search, "calls(assemblage_add)==", calls, " -> len==", Float(search) / Float(calls)
    END IF
    IF (nb_larg > 0) THEN
       PRINT *, mod_name, " nb_larg==", nb_larg, "delta(assemblage_add)==", delta, " -> len==", Float(delta) / Float(nb_larg)
    ELSE
       PRINT *, mod_name, " nb_larg==", nb_larg, "delta(assemblage_add)==", delta
    END IF
  END SUBROUTINE statistiques

  !***********************************************************************
  !                                               -- schema_acces_mat
  !***********************************************************************
  !-- UMR 5251 IMB   C.N.R.S.  2007/05/03

  !-- Summary : pr�definir un schema d'acc�s � la matrice
  !-- End Summary

  SUBROUTINE schema_acces_mat(Mat, Nt, accum, nb_chunk, degres, nulocs, appels, sze_ap, back) !-- Interface

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOM/schema_acces_mat"

    !-- arguments

    TYPE(Matrice), INTENT(IN OUT) :: Mat
    INTEGER, INTENT(IN) :: Nt, accum, nb_chunk
    INTEGER, DIMENSION(:), INTENT(IN) :: degres
    INTEGER, DIMENSION(:, :), INTENT(IN) :: nulocs
    TYPE(doublet), DIMENSION(:, :), INTENT(OUT) :: appels
    INTEGER, DIMENSION(:), INTENT(OUT) :: sze_ap
    INTEGER, DIMENSION(:, :), INTENT(OUT) :: back

    !-- locales

    INTEGER :: jt, iacc, i_chunk, Nsmplx, napp, j, i, cnt, pos, ii, inbuf
    REAL, DIMENSION(1, 1) :: Coeff_Matrice
    TYPE(doublet) :: dbl

    !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    !                                               -- schema_acces_mat.1
    !-----------------------------------------------------------------------

    Coeff_Matrice = 0.0

    DO jt = 1, Nt

       iacc = 1 + Modulo(jt - 1, accum)
       i_chunk = 1 + (jt - 1) / accum

       IF (i_chunk > nb_chunk) THEN
          PRINT *, mod_name, " ALG-ERREUR : i_chunk, nb_chunk", i_chunk, nb_chunk
          CALL delegate_stop()
       END IF

       Nsmplx = degres(jt)

       IF (Modulo(jt - 1, accum) == 0) THEN
          napp = 0
       END IF

       DO j = 1, Nsmplx
          DO i = 1, Nsmplx

             cnt = i + Nsmplx * (j - 1)

             CALL assemblage_add(Coeff_Matrice, nulocs(i, jt), nulocs(j, jt), Mat, pos)  !-- `pos' argument OUT

             dbl%n0_ds_buf = iacc + accum * (cnt - 1)
             dbl%n0_ds_mat = pos
             dbl%jt = jt

             CALL straight_insertion(napp, appels(:, i_chunk), dbl)

          END DO
       END DO

       !-----------------------------------------------------------------------
       !                                               -- schema_acces_mat.2
       !------------------------------------------------ taille du bout courant

       IF (jt == Nt .OR. iacc == accum) THEN

          sze_ap(i_chunk) = napp

          IF (.FALSE.) THEN
             PRINT *, mod_name, " napp==", napp, "Size(appels)==", Size(appels, 1), appels(:, i_chunk)
          END IF

       END IF

    END DO !-- pour pour jt de 1 � Nt

    !-----------------------------------------------------------------------
    !                                               -- schema_acces_mat.3
    !--------------------------------------------------- indirection inverse

    back = -1

    DO i_chunk = 1, nb_chunk

       DO ii = 1, sze_ap(i_chunk)
          inbuf = appels(ii, i_chunk)%n0_ds_buf !-- dans 1 .. nbuf
          back(inbuf, i_chunk) = ii
       END DO

    END DO

  END SUBROUTINE schema_acces_mat

END MODULE Assemblage
