MODULE Extract
  USE LesTypes
  IMPLICIT NONE

CONTAINS

  !*********************************************************************
  !> \brief Calculate the communications between
  !! the partitions of the graph
  !!
  !! A partir du maillage original et d'une partition de scotch
  !! chaque proc identifie ses r�ceptions : recv(2, reception, idproc)
  !! Num�rote ses inconnues
  !! 
  !! \author Pascal JACQ, INRIA Bordeaux Sud-Ouest 
  !*********************************************************************
  SUBROUTINE CalNewMesh(Mesh, Ndom, PointPart, NbElts, NbPts, Glob2Loc, NbRecv, Recv, OvSeg, ordre_eleve)
    CHARACTER(LEN=*), PARAMETER :: mod_name = "CalNewMesh"
    ! variables d'appel
    TYPE(MeshDef)          , INTENT(IN) :: Mesh
    INTEGER                , INTENT(IN) :: Ndom
    INTEGER, DIMENSION(:)  , INTENT(IN) :: PointPart

    INTEGER, DIMENSION(:)     , POINTER :: NbElts
    INTEGER, DIMENSION(:,:)   , POINTER :: Glob2Loc
    INTEGER, DIMENSION(:,:,:) , POINTER :: Recv
    INTEGER, DIMENSION(:)     , POINTER :: NbRecv
    INTEGER, DIMENSION(:)     , POINTER :: NbPts     ! Donne le nombre de points de chaque maillage 
    ! variables locales
    INTEGER :: jt, is, js, pt, NbOverlaps , degre , dom , pt2, dom2
    LOGICAL, DIMENSION(:)     , POINTER :: Contient  ! Ajoute un element de l'overlap au compte des elements
    INTEGER, DIMENSION(:)     , POINTER :: ElemtPart ! Donne le num�ro de la partition dans lequel est l'element : 0=overlap

    TYPE(SegList), DIMENSION(:), POINTER :: OvSeg
    LOGICAL                 , INTENT(IN) :: ordre_eleve

    INTEGER :: Seg, ntyp,k
    INTEGER , DIMENSION(4) :: Pts, domaine   

    nbOverlaps = 0
    ALLOCATE( NbElts(Ndom) )
    ALLOCATE( ElemtPart(Mesh%Nelemt) )
    ! 1�re passe pour pr�parer les allocations
    NbElts = 0
    WRITE(6, *) mod_name, ": Comptage du nombre d'elements dans l'overlap"
    DO jt = 1, Mesh%Nelemt
       degre = Mesh%Ndegre(jt)
       IF( MINVAL(PointPart(Mesh%Nu(1:degre,jt))) /= MAXVAL(PointPart(Mesh%Nu(1:degre,jt))) ) THEN
          NbOverlaps = NbOverlaps + 1
          ElemtPart(jt) = 0
       ELSE
          ! Attention pour �viter une recopie les numeros de PointPart vont de 0 � Ndom-1
          dom = PointPart( Mesh%Nu(1,jt) ) + 1
          NbElts( dom ) = NbElts( dom ) + 1
          ElemtPart(jt) = dom
       END IF
    END DO
    WRITE(6, *) mod_name, ": il y a", NbOverlaps," elements dans l'overlap"

    ! Allocations
    ALLOCATE( Glob2Loc(Mesh%Npoint, Ndom) )   ! car on utilise Glob2Loc(:, domaine) lors du savemesh final
    ALLOCATE( Recv(2, NbOverlaps*(Mesh%NdegreMax-1), Ndom) ) ! meilleur pour le calcul des envois (sinon?)
    ALLOCATE( NbRecv(Ndom) )
    ALLOCATE( NbPts(Ndom) )

    ALLOCATE( Contient(Ndom) )   ! Permet d'ajouter les elements d'overlap

    ! 2�me passe, num�rotation et calcul des recv
    NbRecv = 0
    NbPts  = 0
    Glob2Loc = 0

    DO jt = 1, Mesh%Nelemt

! dante
!#ifdef P2P2
       degre = Mesh%NSommets(jt)
!#else
!       degre = Mesh%Ndegre(jt)
!#endif

       dom   = ElemtPart(jt)

       IF ( dom > 0 ) THEN
          ! On est dans un element interne � un maillage, On num�rote les inconnues
          DO is = 1, degre
             pt = Mesh%Nu(is,jt)
             IF ( Glob2Loc(pt, dom) == 0 ) THEN
                NbPts(dom) = NbPts(dom) + 1
                Glob2Loc(pt, dom) = NbPts(dom)
             END IF
          END DO
       ELSE
          Contient = .FALSE.
          ! On est dans un element overlap
          DO is = 1, degre
             pt  = Mesh%Nu(is,jt)
             dom = PointPart(pt) + 1
             Contient(dom) = .TRUE.
             DO js = 1, degre
                pt2  = Mesh%Nu(js, jt)
                ! On renumerote pour le maillage "dom"
                IF ( Glob2Loc(pt2, dom) == 0 ) THEN
                   NbPts(dom) = NbPts(dom) + 1
                   Glob2Loc(pt2, dom) = NbPts(dom)

                   dom2 = PointPart(pt2) + 1
                   ! Si le point est dans un domaine different alors il faudra le recuperer
                   IF ( dom /= dom2 ) THEN
                      NbRecv(dom) = NbRecv(dom) + 1
                      Recv(1, NbRecv(dom), dom) = dom2
                      Recv(2, NbRecv(dom), dom) = pt2
                   END IF
                END IF

             END DO
          END DO

          ! On ajoute 1 element par sous domaine concern�
          DO is = 1, Ndom
             IF ( Contient(is) ) THEN
                NbElts(is) = NbElts(is) + 1
             END IF
          END DO

       END IF

    END DO

    ! Nettoyage
    DEALLOCATE( Contient )

    ALLOCATE( OvSeg(1:Ndom) )
    DO is = 1, Ndom
       NULLIFY(OvSeg(is)%suivant)
    END DO
    IF (ordre_eleve) THEN
       IF (Mesh%Ndim == 2) THEN
          ntyp  = SIZE(Mesh%NuSeg,1 )
          DO jt = 1, Mesh%Nelemt
             IF( ElemtPart(jt) /= 0 ) CYCLE
             ! parcours des segments de l'�l�ment
             DO k = 1,  ntyp 
                ! On cherche le numero du k�me segment de Jt
                seg = Mesh%NuSeg( k, jt )
                IF (seg <= 0) CYCLE
                ! Les 2 pts du segment
                Pts(1:2) = Mesh%Nubo( 1:2, seg )
                ! On cherche les Points voisins de seg
                CALL NotInSeg(Mesh, jt, Pts(1:3) )

                domaine(1:3) = PointPart( Pts(1:3) ) + 1

                is = 3
                IF (domaine(is) /= MINVAL(domaine(1:2))) THEN
                   CALL EchangeSeg( Seg,jt,Pts(1),Pts(2),domaine(is), MINVAL(domaine(1:2)), OvSeg)
                ENDIF

                IF (domaine(1) /= domaine(2)) THEN
                   CALL EchangeSeg( Seg,jt,Pts(1),Pts(2),MAXVAL(domaine(1:2)), MINVAL(domaine(1:2)), OvSeg)
                   ! print *, "Ajout de com entre :" ,maxval(domaine(1:2)), minval(domaine(1:2)),Seg,jt,Pts(1),Pts(2)
                END IF

             END DO
          END DO
       ELSE
          ntyp  = SIZE(Mesh%NuSeg,1 )  
          DO jt = 1, Mesh%Nelemt
             IF( ElemtPart(jt) /= 0 ) CYCLE
             ! parcours des segments de l'�l�ment
             DO k = 1,  ntyp 
                ! On cherche le numero du k�me segment de Jt
                seg = Mesh%NuSeg( k, jt )
                IF (seg <= 0) CYCLE

                ! Les 2 pts du segment
                Pts(1:2) = Mesh%Nubo( 1:2, seg )
                ! On cherche les Points voisins de seg
                CALL NotInSeg(Mesh, jt, Pts(1:4) )

                domaine(1:4) = PointPart( Pts(1:4) ) + 1

                DO is = 3,4
                   IF (domaine(is) /= MINVAL(domaine(1:2))) THEN
                      CALL EchangeSeg( Seg,jt,Pts(1),Pts(2),domaine(is), MINVAL(domaine(1:2)), OvSeg)
                   ENDIF

                   IF (domaine(1) /= domaine(2)) THEN
                      CALL EchangeSeg( Seg,jt,Pts(1),Pts(2),MAXVAL(domaine(1:2)), MINVAL(domaine(1:2)), OvSeg)
                   END IF
                END DO
             END DO
          END DO
       END IF
    END IF

  CONTAINS
    SUBROUTINE NotInSeg(Mesh, jt, Pts )
      TYPE(MeshDef)         , INTENT(IN)    :: Mesh
      INTEGER, DIMENSION(:) , INTENT(INOUT) :: Pts
      INTEGER               , INTENT(IN)    :: jt

      INTEGER :: i,k,pt
      k = 3

      DO i = 1,SIZE(Pts)
         pt = Mesh%Nu(i,jt)
         IF (pt /= Pts(1) .AND. pt /= Pts(2)) THEN
            Pts(k) = pt
            k = k + 1
         END IF
      END DO

    END SUBROUTINE NotInSeg


    SUBROUTINE EchangeSeg( Seg,Elt, Pt1, Pt2, DomRcv, DomSnd, OvSeg )
      INTEGER                      , INTENT(IN)    :: Seg, Elt, Pt1, Pt2, DomRcv, DomSnd
      TYPE(SegList) , DIMENSION(:) , INTENT(INOUT) :: OvSeg
      TYPE(SegCom)  , POINTER  :: nouveau
      TYPE(SegList)  , POINTER :: Ptr, OldPtr, newPtr

      INTEGER ::  Flag

      ! On s'occupe de la com du receive
      NULLIFY(nouveau)
      CALL CreeSegCom(Seg,Elt,Pt1,Pt2,nouveau)

      Ptr => OvSeg(DomRcv)%suivant
      IF (.NOT. ASSOCIATED(Ptr)) THEN
         CALL CreeSegList(DomSnd,NewPtr)
         OvSeg(DomRcv)%suivant => NewPtr
         NewPtr%rcv => nouveau
         NewPtr%NbRcv = 1
      ELSE
         NULLIFY(OldPtr)
         DO 
            IF (Ptr%Domv == DomSnd) THEN
               CALL InsereSegCom(nouveau,Ptr%rcv, Flag)
               Ptr%NbRcv = Ptr%NbRcv + Flag
               EXIT
            ELSE IF (Ptr%Domv > DomSnd) THEN
               CALL CreeSegList(DomSnd,NewPtr)
               NewPtr%suivant => Ptr
               IF (ASSOCIATED(OldPtr)) THEN
                  OldPtr%suivant => NewPtr
               ELSE
                  OvSeg(DomRcv)%suivant => NewPtr
               ENDIF
               NewPtr%rcv => nouveau
               NewPtr%NbRcv = 1
               EXIT       
            ELSE
               OldPtr => Ptr
               Ptr => Ptr%suivant         
            ENDIF
            IF (.NOT. ASSOCIATED(Ptr)) THEN
               CALL CreeSegList(DomSnd,NewPtr)
               OldPtr%suivant => NewPtr
               NewPtr%rcv => nouveau
               NewPtr%NbRcv = 1
               EXIT
            ENDIF
         END DO
      END IF

      ! On s'occupe de la com du send
      NULLIFY(nouveau)
      CALL CreeSegCom(Seg,Elt,Pt1,Pt2,nouveau)

      Ptr => OvSeg(DomSnd)%suivant
      IF (.NOT. ASSOCIATED(Ptr)) THEN
         CALL CreeSegList(DomRcv,NewPtr)
         OvSeg(DomSnd)%suivant => NewPtr
         NewPtr%snd => nouveau
         NewPtr%NbSnd = 1
      ELSE
         NULLIFY(OldPtr)
         DO 
            IF (Ptr%Domv == DomRcv) THEN
               CALL InsereSegCom(nouveau,Ptr%snd, Flag)
               Ptr%NbSnd = Ptr%NbSnd + Flag
               EXIT
            ELSE IF (Ptr%Domv > DomRcv) THEN
               CALL CreeSegList(DomRcv,NewPtr)
               NewPtr%suivant => Ptr
               IF (ASSOCIATED(OldPtr)) THEN
                  OldPtr%suivant => NewPtr
               ELSE
                  OvSeg(DomSnd)%suivant => NewPtr
               ENDIF
               NewPtr%snd => nouveau
               NewPtr%NbSnd = 1
               EXIT       
            ELSE
               OldPtr => Ptr
               Ptr => Ptr%suivant         
            ENDIF

            IF (.NOT. ASSOCIATED(Ptr)) THEN
               CALL CreeSegList(DomRcv,NewPtr)
               OldPtr%suivant => NewPtr
               NewPtr%snd => nouveau
               NewPtr%NbSnd = 1
               EXIT
            ENDIF
         END DO
      END IF

    END SUBROUTINE EchangeSeg

    SUBROUTINE CreeSegList(DomV,List)
      INTEGER , INTENT(IN) :: DomV
      TYPE(SegList), POINTER, INTENT(OUT) :: List

      ALLOCATE(List)
      List%DomV = DomV
      List%NbSnd = 0
      List%NbRcv = 0
      NULLIFY(List%snd,List%rcv,List%suivant)          

    END SUBROUTINE CreeSegList

    SUBROUTINE CreeSegCom(Seg,Elt,Pt1,Pt2,SegC)
      INTEGER , INTENT(IN) :: Seg,Elt,Pt1,Pt2
      TYPE(SegCom), POINTER, INTENT(OUT) :: SegC

      ALLOCATE(SegC)
      SegC%Seg = Seg
      SegC%Elt = Elt

      SegC%Pt1 = Pt1
      SegC%Pt2 = Pt2
      NULLIFY(SegC%suivant)          

    END SUBROUTINE CreeSegCom

    SUBROUTINE InsereSegCom(seg,liste, Flag)
      TYPE(SegCom), POINTER, INTENT(INOUT) :: seg
      TYPE(SegCom), POINTER, INTENT(INOUT) :: liste
      INTEGER , INTENT(OUT) :: Flag

      TYPE(SegCom), POINTER :: Ptr,OldPtr

      Flag = 0
      Ptr => liste
      NULLIFY(OldPtr)
      IF (.NOT. ASSOCIATED(liste)) THEN
         Flag = 1
         liste => seg
      ELSE
         DO
            IF (seg%Seg == Ptr%Seg) THEN
               EXIT
            ELSE IF (seg%Seg < Ptr%Seg) THEN
               Seg%suivant=>Ptr
               IF (ASSOCIATED(OldPtr)) THEN
                  OldPtr%suivant => Seg
               ELSE
                  liste => Seg
               ENDIF
               Flag = 1
               EXIT            
            ELSE
               OldPtr => Ptr
               Ptr => Ptr%suivant
            END IF

            IF (.NOT. ASSOCIATED(Ptr)) THEN
               OldPtr%Suivant => Seg
               Flag = 1
               EXIT
            END IF
         END DO
      ENDIF
    END SUBROUTINE InsereSegCom

  END SUBROUTINE CalNewMesh



  !****************************************************************
  !> Calcule et stoke toutes les receptions ainsi que le nb de domV
  !! \author Pascal JACQ, INRIA Bordeaux Sud-Ouest 
  !****************************************************************
  SUBROUTINE CalComRecv( isd, Ndom, NbRecv, Recv, Comm)
    CHARACTER(LEN=*), PARAMETER :: mod_name = "CalComRecv"
    ! variables d'appel
    INTEGER                  , INTENT(IN)  :: isd
    INTEGER                  , INTENT(IN)  :: Ndom
    INTEGER, DIMENSION(:)    , INTENT(IN)  :: NbRecv
    INTEGER, DIMENSION(:,:,:), INTENT(IN)  :: Recv

    TYPE(MeshCom)            , INTENT(OUT) :: Comm


    ! variables locales
    INTEGER , DIMENSION(Ndom)  :: NbRcv
    INTEGER , DIMENSION(Ndom)  :: MapInv    ! Donne le num�ro local du proc global 

    INTEGER            :: NdomV, Nrcv2Max

    INTEGER            :: is, id, pt

    ! On considere pour le moment que si un recv existe alors un snd existe, 
    ! mais pas l'inverse.
    ! En clair on se base uniquement sur les recv pour calculer le nombre 
    ! de procs voisins : il se peut que �a d�conne

    ! Calcul des donn�es simples : les recv
    NdomV = 0
    NbRcv = 0
    DO is= 1, NbRecv(isd)
       id = Recv(1, is, isd)
       NbRcv( id ) = NbRcv( id ) + 1
    END DO

    ! On connait donc le nombre de voisins 
    ! (Attention en recv uniquement....si plantage regarder par l�): 
    ! apr�s courte reflexion �a doit �tre ok
    NdomV = COUNT(NbRcv /= 0)
    Nrcv2Max = MAXVAL(NbRcv)

    Comm%NdomV = NdomV
    Comm%Nrcv2Max = Nrcv2Max
    Comm%NPrcv2tot = NbRecv(isd)

    ! Allocation des donnees connues
    ALLOCATE(Comm%JDomV(NdomV), Comm%NPrcv2(NdomV))
    ALLOCATE(Comm%NPrcv2(NdomV))
    ALLOCATE(Comm%PTrcv2(Nrcv2Max, NdomV))

    ALLOCATE(Comm%InvMap(Ndom))

    ! Numerotation des domaines voisins
    id = 1
    MapInv = 0
    DO is = 1, Ndom
       IF (NbRcv(is) /= 0) THEN
          Comm%JDomV(id) = is
          MapInv(is)     = id
          id             = id + 1
       END IF
    END DO

    Comm%InvMap = MapInv
    ! Attribution des recv
    Comm%NPrcv2(:) = NbRcv( Comm%JDomV )
    ! On les ordonne
    NbRcv = 0
    DO is= 1, NbRecv(isd)
       id = MapInv( Recv(1, is, isd) )  ! Numero du proc local de qui on receptionne
       pt = Recv(2, is, isd)            ! Numero global du point qu'on receptionne

       NbRcv( id ) = NbRcv( id ) + 1
       Comm%PTrcv2( NbRcv( id ), id ) = pt
    END DO

  END SUBROUTINE CalComRecv

  !***********************************************************
  !> Calcule les sends et enregistre tout le maillage
  !! \author Pascal JACQ, INRIA Bordeaux Sud-Ouest 
  !***********************************************************
  SUBROUTINE SaveNewMesh( PbName, Ndom, Mesh, NbEltsTot, NbPtsTot, &
                          Glob2LocTot, Comms, PointPart, OvSeg, ordre_eleve, &
                          MeshP2, Map_P2P1 )

    CHARACTER(LEN=*), PARAMETER :: mod_name = "SaveNewMesh"

    ! variables d'appel
    CHARACTER(len=*)                  , INTENT(IN) :: PbName
    INTEGER                           , INTENT(IN) :: Ndom
    TYPE(MeshDef)                     , INTENT(IN) :: Mesh
    INTEGER, DIMENSION(:),TARGET      , INTENT(IN) :: NbEltsTot
    INTEGER, DIMENSION(:),TARGET      , INTENT(IN) :: NbPtstot    
    INTEGER, DIMENSION(:,:)    ,TARGET, INTENT(IN) :: Glob2LocTot
    TYPE(MeshCom), DIMENSION(:),TARGET, INTENT(IN) :: Comms
    INTEGER, DIMENSION(:)             , INTENT(IN) :: PointPart
    TYPE(SegList), DIMENSION(:)       , INTENT(IN) :: OvSeg
    LOGICAL                           , INTENT(IN) :: ordre_eleve
    TYPE(MeshDef), OPTIONAL           , INTENT(IN) :: MeshP2
    INTEGER, OPTIONAL, DIMENSION(:)   , INTENT(IN) :: Map_P2P1
    !----------------------------------------------------------

    ! variables locales
    CHARACTER(LEN=255) :: MeshFile
    INTEGER            :: lensd, MyUnit

    INTEGER            :: NdomV , isd
    INTEGER            :: is, js,id, Nfr, iv, k, ifac, Ndim, jt, nbPtIn, voisin

    INTEGER, DIMENSION(:), POINTER :: OrdCoor
    INTEGER, DIMENSION(:), POINTER :: IdSnd2
    TYPE(MeshCom),         POINTER :: Comm, comm2

    INTEGER              , POINTER :: NbElts
    INTEGER              , POINTER :: NbPts    
    INTEGER, DIMENSION(:), POINTER :: Glob2Loc, Glo2LocElt

    TYPE(SegList) , POINTER       :: ListPtr

    INTEGER , DIMENSION(Ndom)     :: NbRcv,NbSnd
    TYPE(SegCom) , POINTER        :: SegPtr
    INTEGER :: DomV
    INTEGER :: degFr

    LOGICAL :: P2_mesh
    INTEGER :: i_m, j_m, Nu_i, Nu_j, Nu_k, i_s, i_s_f, je

!#ifdef P2P2
    REAL, DIMENSION(:,:)   , ALLOCATABLE :: SegCoor
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: SegPts
    INTEGER                              :: iseg, NbSeg , is1, is2
!#endif

    ALLOCATE( Glo2LocElt(Mesh%Nelemt) )

!#ifdef P2P2
!!$    ! Allocation pire des cas
!!$    ALLOCATE( SegCoor(Mesh%Ndim, Mesh%Nsegmt))
!!$    ALLOCATE( SegPts(2,Mesh%Nsegmt))
!#endif
    
    IF ( PRESENT(MeshP2) .AND. PRESENT(Map_P2P1) ) THEN
       P2_mesh = .TRUE.
    ELSE
       P2_mesh = .FALSE.
    ENDIF

    IF( P2_mesh ) THEN
       ALLOCATE(SegCoor(Mesh%Ndim, Mesh%Nsegmt))
       ALLOCATE( SegPts(2,         Mesh%Nsegmt))
    ENDIF

    DO isd = 1, Ndom
       ! Les donn�es sont fig�es cette boucle est parall�lisable (OpenMP)
       ! Maintenant on sauve tout

       Ndim  = Mesh%Ndim
       NdomV = comms(isd)%NdomV

       NbElts   => NbEltsTot(isd)
       NbPts    => NbPtsTot(isd)
       Glob2Loc => Glob2LocTot(:,isd)

       ! Calcul des Sends
       Comm => Comms(isd)
       ALLOCATE(  Comm%NPsnd2(NdomV) )
       ALLOCATE(  IdSnd2(NdomV) )
       DO is = 1, NdomV
          id = 0
          voisin = Comm%JdomV(is)
          comm2 => Comms(voisin)
          ! On recherche l'id du proc en cour dans les recv de ses voisins
          DO js = 1, comm2%NdomV
             IF (comm2%JdomV(js) == isd) THEN
                id = js
                IdSnd2(is) = id
             END IF
          END DO

          IF (id == 0) THEN
             WRITE(6,*) mod_name, ": Plantage!!! Id introuvable chez le voisin"
             CALL Abort()
          ELSE
             Comm%NPsnd2(is) = Comm2%NPrcv2(id)
          END IF
       END DO
       Comm%Nsnd2Max = MAXVAL(Comm%NPsnd2)

       ! Calcul du nombre de faces fronti�res
       Nfr = 0
       DO ifac = 1, Mesh%NfacFr
!#ifdef P2P2
          ! Attention valable uniquement pour le 2D
          !is = COUNT( PointPart(Mesh%NsFacFr(1:Ndim,ifac)) == (isd -1))
!#else
          is = COUNT( PointPart(Mesh%NsFacFr(:,ifac)) == (isd -1))
!#endif
          IF (is > 0) THEN
             Nfr = Nfr + 1
          END IF
       END DO


       ! Concatenation du nom du fichier de sauvegarde, et ouverture du fichier
       MeshFile = TRIM(ADJUSTL(PbName))//"-"
       lensd = INDEX(MeshFile," ")
       WRITE(MeshFile(lensd:lensd+2),'(i3.3)') isd
       MeshFile = TRIM(ADJUSTL(MeshFile))//".pmsh"

       WRITE(6,*) mod_name, ": Ecriture du fichier ", TRIM(Meshfile)
       MyUnit  = 1000 + isd
       OPEN(MyUnit, FILE=TRIM(MeshFile), FORM="formatted" )

       ! Enregistrement au format pmsh

       WRITE( MyUnit, *)  Comm%Ndomv, Ndom
       !                  NdomV =  Nombre de sous-domaines
       !                           voisins du domaine courant 

       WRITE( MyUnit, *)  Comm%Nsnd2Max, Comm%Nrcv2Max

       DO iv = 1, Comm%Ndomv
          WRITE( MyUnit, *) Comm%Jdomv(iv)
          ! Sommets sur lesquels il y a des communications de niveau 2
          ! ---------------------------------------------------------
          WRITE( MyUnit, *)    Comm%NPsnd2(iv),  Comm%NPrcv2(iv) 
          !! Changement pour les snd + Glob2Loc
          !! On cherche chez le voisin les rcvs qui correspondent � nos snd
          WRITE( MyUnit, *) (   Glob2Loc( Comms( Comm%Jdomv(iv) )%PTrcv2(js,IdSnd2(iv)) ) , js=1,Comm%NPsnd2(iv))
          WRITE( MyUnit, *) (   Glob2Loc( Comm%PTrcv2(js,iv)  ), js=1,Comm%NPrcv2(iv))
       END DO

       WRITE(6, *) " Fin des donnees de communication "

       degFr = SIZE(Mesh%NsFacFr, DIM=1)
       ! nombre de sommet, elements et de segments frontieres
       WRITE( MyUnit, *) Mesh%Ndim, NbPts, NbElts, Nfr, Mesh%NDegreMax, degFr

       ! coordonnees des sommets
       ALLOCATE(OrdCoor(NbPts))
       id = 0
       DO js=1, Mesh%Npoint
          is = Glob2Loc(js)
          IF (is > 0) THEN
             OrdCoor(is) = js
             id = id + 1
          END IF
       END DO

       DO js=1, NbPts
          is = OrdCoor(js)
          DO k = 1, Ndim
             WRITE(MyUnit, '(E24.16)',  ADVANCE='NO') Mesh%Coor(k, is)
          ENDDO
          WRITE(MyUnit, *)
       END DO
       PRINT *, mod_name, ": Npts", NbPts,id
       DEALLOCATE(OrdCoor)

       ! Connectivites des elements locaux
       id = 0
       Glo2LocElt = 0
       DO jt=1,Mesh%Nelemt
          is = Glob2Loc(Mesh%Nu(1,jt))
          IF (is > 0) THEN
             ! En fait l'�l�ment n'est interne que si TOUT les numeros sont non nuls => 
             ! On approfondi la recherche
             ! En fait m�me pas : on est oblig� d'avoir recours au pointpart!
             nbPtIn= COUNT( PointPart(Mesh%Nu(1:Mesh%Ndegre(jt), jt)) == (isd-1))
             IF (nbPtIn > 0) THEN
                WRITE(MyUnit,*) Mesh%Ndegre(jt), Mesh%LogElt(jt)
                WRITE(MyUnit,'(50(I15,X))') ( Glob2Loc( Mesh%Nu(k,jt) ), k=1, Mesh%Ndegre(jt) )
                id = id + 1
                Glo2LocElt(jt) = id
             END IF
          END IF
       END DO
       PRINT *, mod_name, ": NElts", NbElts, id

       id = 0
       ! Segments frontieres locaux
       DO ifac = 1, Mesh%NFacFr
!#ifdef P2P2
          ! Attention valable uniquement pour le 2D
          !is = COUNT( PointPart(Mesh%NsFacFr(1:Ndim,ifac)) == (isd -1))
!#else
          is = COUNT( PointPart(Mesh%NsFacFr(:,ifac)) == (isd -1))
!#endif
          IF (is > 0) THEN
             !!!! Pour lire du vrai hybride (3D) il faudrait un Mesh%degreFr(:)
             WRITE(MyUnit,*) Mesh%LogFr(ifac), degFr
             WRITE(MyUnit,*) ( Glob2Loc( Mesh%NsFacFr(k,ifac) ), k=1, degFr )
             id = id + 1
          END IF
       END DO
       PRINT *, mod_name, ": NFr", NFr,id

       WRITE(6, *) " Fin des donnees du maillage  "
       CLOSE(MyUnit)

       ! Ecriture dans le fichier stat.dat
       WRITE(77,'(10(I9,2X))') isd, Nbpts, Nbelts , NdomV, MINVAL(Comm%NPsnd2), &
            & MAXVAL(Comm%NPsnd2), MINVAL(Comm%NPrcv2), MAXVAL(Comm%NPrcv2)
       ! Nettoyage
       DEALLOCATE(  IdSnd2 )


       ! P. JACQ
       ! On va parcourir toutes les com de type segment et associer 
       ! les numeros locaux des elts et des pts (avant on avait les numeros globaux)
       ListPtr => OvSeg(isd)%suivant
       IF (ordre_eleve) THEN
          NbRcv = 0
          NbSnd = 0

          DO WHILE ( ASSOCIATED(ListPtr) )
             DomV = ListPtr%DomV
             NbRcv(DomV) = ListPtr%NbRcv
             NbSnd(DomV) = ListPtr%NbSnd
             ListPtr => ListPtr%suivant
          END DO

          MeshFile = TRIM(ADJUSTL(PbName))//"-"
          lensd = INDEX(MeshFile," ")
          WRITE(MeshFile(lensd:lensd+2),'(i3.3)') isd
          MeshFile = TRIM(ADJUSTL(MeshFile))//".pseg"

          OPEN(UNIT=MyUnit , FILE = TRIM(MeshFile) )
          ! Nombre de voisins , max de receptions, max d'envois pour 1 voisin
          WRITE(MyUnit,*) COUNT( (NbRcv+NbSnd) > 0 ), MAXVAL( NbRcv ),MAXVAL( NbSnd )
          ListPtr => OvSeg(isd)%suivant
          DO WHILE ( ASSOCIATED(ListPtr) )
             WRITE(MyUnit,*) ListPtr%DomV, ListPtr%NbRcv, ListPtr%NbSnd

             SegPtr => ListPtr%rcv
             DO WHILE( ASSOCIATED(SegPtr))

                WRITE(MyUnit, *) Glo2LocElt(SegPtr%Elt), &
                                 Glob2loc(SegPtr%Pt1),   &
                                 Glob2loc(SegPtr%Pt2)

                IF( P2_mesh ) THEN

!!$                   WRITE(*,*)
!!$                   WRITE(*, '("[", I2, "]",I10, "|", 2I10)' ) isd, SegPtr%Seg, SegPtr%Pt1, SegPtr%Pt2
!!$                   WRITE(*, '(10I10)') MeshP2%NU(:, SegPtr%Elt) 
!!$                   WRITE(*, '(10I10, "  Map")') Map_P2P1(MeshP2%NU(:, SegPtr%Elt))
!!$                   WRITE(*, '(4I10)')  Mesh%Nu(:, SegPtr%Elt)
!!$
!!$                   DO i_s = 1, SIZE( MeshP2%Nuseg(:, SegPtr%Elt) )           
!!$
!!$                      write(*, '(2I10, "|", 2I10)') Mesh%NuBo(:,   Mesh%Nuseg(i_s, SegPtr%Elt) ), &
!!$                                      Map_P2P1( MeshP2%NuBo(:, MeshP2%Nuseg(i_s, SegPtr%Elt)) )
!!$
!!$                   ENDDO


                   DO i_s = 1, SIZE( MeshP2%Nuseg(:, SegPtr%Elt) )

                      IF( MeshP2%Nuseg(i_s, SegPtr%Elt) /= 0 ) THEN
                         
                         Nu_i = Map_P2P1( MeshP2%NuBo(1, MeshP2%Nuseg(i_s, SegPtr%Elt)) )
                         Nu_j = Map_P2P1( MeshP2%NuBo(2, MeshP2%Nuseg(i_s, SegPtr%Elt)) )

                         IF( (Nu_i == SegPtr%Pt1 .AND. Nu_j == SegPtr%Pt2) .OR. &
                             (Nu_i == SegPtr%Pt2 .AND. Nu_j == SegPtr%Pt1) ) THEN

                            i_s_f = i_s
                            EXIT

                         ENDIF

                      ENDIF

                   ENDDO

                   ! Order extra-Dof: N_verts + i_seg
                   Nu_k = MeshP2%Nsommets(SegPtr%Elt) + i_s_f

                   IF( MeshP2%NDim == 3 ) THEN
                      ! For P2 tetrahedrons the numeration of the last
                      ! two extra-Dof is inverted
                      IF( i_s_f == 5 ) THEN
                         Nu_k = MeshP2%Nsommets(SegPtr%Elt) + i_s_f + 1
                      ELSEIF( i_s_f == 6 ) THEN
                         Nu_k = MeshP2%Nsommets(SegPtr%Elt) + i_s_f - 1
                      ENDIF
                   ENDIF

                   DO k = 1, Ndim
                      WRITE(MyUnit, '(E24.16)', ADVANCE='NO') MeshP2%Coor(k, MeshP2%Nu(Nu_k, SegPtr%Elt) )
                   ENDDO
                   WRITE(MyUnit, *)


!!$                   WRITE(*, '(I4)') Nu_k
!!$                   WRITE(*, '(3F24.16, "  C")')  MeshP2%Coor(:, MeshP2%Nu(Nu_k, SegPtr%Elt) )
!!$                   WRITE(*, '(3F24.16, "  L")')  0.5d0*(Mesh%Coor(:, SegPtr%Pt1) +  Mesh%Coor(:, SegPtr%Pt2) )
!!$                   WRITE(*,*) '----------------------------------------'
!!$                   WRITE(*,*)

                ELSE

                   DO k = 1, Ndim
                      WRITE(MyUnit, '(E24.16)', ADVANCE='NO') 0.5d0*(Mesh%Coor(k, SegPtr%Pt1) +  Mesh%Coor(k, SegPtr%Pt2) )
                   ENDDO
                   WRITE(MyUnit, *)

                ENDIF

                SegPtr => SegPtr%suivant

             END DO

             SegPtr => ListPtr%snd
             DO WHILE( ASSOCIATED(SegPtr))

                WRITE(MyUnit,*) Glo2LocElt(SegPtr%Elt), &
                                Glob2loc(SegPtr%Pt1),   &
                                Glob2loc(SegPtr%Pt2)

                SegPtr => SegPtr%suivant

             ENDDO

             ListPtr => ListPtr%suivant

          END DO

       ENDIF


       IF( P2_mesh ) THEN

          SegCoor = 0.0
          SegPts  = 0

          NbSeg = 0

          ! Parcours des segments
          DO iseg = 1, MeshP2%Nsegmt

             ! Recuperation du domaine
             is1 = PointPart( Map_P2P1(MeshP2%Nubo(1, iseg)) ) + 1
             is2 = PointPart( Map_P2P1(MeshP2%Nubo(2, iseg)) ) + 1

             ! Si le segment est entierement dans le domaine on le sauve
             IF( ((is1 == isd) .OR. (is2 == isd)) ) THEN
                
                NbSeg = NbSeg + 1
                
                SegPts(:, NbSeg) = Glob2Loc( Map_P2P1(MeshP2%Nubo(:, iseg)) )

                CALL Seg2Ele(iseg, i_s_f, je)

                ! Order extra-Dof: N_verts + i_seg
                Nu_k = MeshP2%NSommets(je) + i_s_f

                ! For P2 tetrahedrons the numeration of the last
                ! two extra-Dof is inverted
                IF( MeshP2%NDim == 3) THEN
                   IF( i_s_f == 5 ) THEN
                      Nu_k = MeshP2%NSommets(je) + i_s_f + 1
                   ELSEIF( i_s_f == 6 ) THEN
                      Nu_k = MeshP2%NSommets(je) + i_s_f - 1
                   ENDIF
                ENDIF

                SegCoor(:, NbSeg) = MeshP2%Coor(:, MeshP2%Nu(Nu_k, je))

!!$write(*,*)                
!!$write(*, '("I_seg "  I4)') iseg
!!$write(*, '("NUBO  ", 2I4, "|" 2I4)') MeshP2%Nubo(:, iseg), Map_P2P1(MeshP2%Nubo(:, iseg))
!!$write(*, '("JE IS ", 2I4)') je, i_s_f
!!$write(*, '("NU P2 ", 10I4)') MeshP2%Nu(:, je)
!!$write(*, '("NU_k ",  I4)') MeshP2%Nu(Nu_k, je)
!!$write(*,*) '-------------------------'

             END IF

          ENDDO

          WRITE(MyUnit,*) NbSeg

          DO iseg = 1, NbSeg

             WRITE(MyUnit, '(2I10)', ADVANCE='NO') SegPts(:, iseg) 
             DO k = 1, Ndim
                WRITE(MyUnit, '(E24.16)', ADVANCE='NO') SegCoor(k, iseg)
             ENDDO
             WRITE(MyUnit, *)

          END DO

       ENDIF 

       CLOSE(MyUnit)

    ENDDO

    DEALLOCATE(Glo2LocElt)

    IF( ALLOCATED(SegCoor) ) DEALLOCATE(SegCoor)
    IF( ALLOCATED(SegPts)  ) DEALLOCATE(SegPts)

CONTAINS

  !==================================
  SUBROUTINE Seg2Ele(iseg, i_s_f, je)
  !==================================
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: iseg
    INTEGER, INTENT(OUT) :: i_s_f
    INTEGER, INTENT(OUT) :: je
    !---------------------------

    INTEGER :: m, k
    !---------------------------

    DO m = 1, MeshP2%Nelemt
       
       DO k = 1, SIZE( MeshP2%NuSeg(:, m) )

          IF( MeshP2%NuSeg(k, m) == iseg ) THEN

             i_s_f = k
                je = m

                RETURN

          ENDIF
                       
       ENDDO

    ENDDO

  END SUBROUTINE Seg2Ele
  !=====================

  END SUBROUTINE SaveNewMesh

END MODULE Extract

