!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga, M.Papin                                 */
!*               2006-2011 R. Abgrall and P. Jacq                            */
!*                  2000-2006 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                  2002-2011 INRIA Bordeaux Sud Ouest                       */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

!> \brief A geometry and graph manipulation library
!!
!! This library contains all the routines related to geometry and graph manipulation
!! - Reading Meshes
!! - Manipulating graphs
!! - Computing geometry : 
!!   - Computing normals
!!   - Computing volumes
MODULE GeomGraph
  USE LesTypes

 USE GeomParam

  IMPLICIT NONE
  !==============================================================
  !! Ndegre(jt)    ::  type de l'element
  !! Nu(k, jt)     ::  numero du Keme point de l'element jt
  !! Nubo(k, js)   ::  numero du Keme point du segment js
  !! NsfacFr(k, is)::  numero du Keme point du segment frontiere is
  !! Nuseg(k, jt)  ::  numero du Keme segment de l'element jt
  !! Nusv(k, js)   ::  numero du Keme sommet tel qu'avec le
  !!                  segment js, ils forment un element
  !! Nutv(k, js)   ::  numero du Keme element contenant le segment js
  !! NbVois(k)     ::  numero du voisin du sommet k
  !! LogFr(k)      ::  contrainte de la face fronti�re k
  !!
  !!      MATRICE CSR : Jvcell, Jposi, Vals (, ILUVals), Diag, IvPos
  !! Jvcell(k)           ::  indice de colonne du bloc k dans le stockage CSR
  !! IvPos(k)            ::  acces direct aux colonnes
  !! JPosi(i)            ::  indice dans le stockage CSR du premier bloc non nul de la ieme ligne.
  !! IDiag               ::  indice dans le stockage CSR du bloc diag de la ieme ligne.
  !! (ILU)Vals(:, :, ij) ::  bloc B(i, j) avec ij dans [Jposi(i), Jposi(i+1)-1] et j=Jvcell(ij)
  !!
  !! Remarque 1: Les structures de nos matrices sont SYMETRIQUES.
  !! Remarque 2: Preconditionneur ILU (0) !!!!!!!!!!!!!!!!!!!!!!!
  !==============================================================

  INTEGER, PARAMETER, PRIVATE :: NONE = -100

  TYPE, PRIVATE :: quad
     INTEGER :: kmin, kmed, kmax, ifac
  END TYPE quad

  INTEGER, SAVE, PRIVATE :: search_cnt = 0
  INTEGER, SAVE, PRIVATE :: nb_search = 0
  INTEGER, SAVE, PRIVATE :: old_search = 0
  INTEGER, SAVE, PRIVATE :: hdebug = 0

  TYPE(quad), DIMENSION(: ), PRIVATE, SAVE, ALLOCATABLE :: t_ifac

CONTAINS

  !> \author Mikael PAPIN
  !!
  !! \brief D�-allocation de la matrice logique
  SUBROUTINE FreeMatGraphVc(Mat) ! semble non employ�e (RA18/12/10)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "FreeMatGraphVc"
    TYPE(Matrice), INTENT(INOUT)  :: Mat
    DEALLOCATE(Mat%JvCell, Mat%JPosi)
    IF (see_alloc) THEN
       CALL alloc_deallocate(".global.", "mat%jvcell")
       CALL alloc_deallocate(".global.", "mat%jposi")
    END IF
    IF (ASSOCIATED(Mat%IvPos)) THEN
       DEALLOCATE(Mat%IvPos) !-- il semblerait que Ivpos => NULL() manque %%%%
       IF (see_alloc) THEN
          CALL alloc_deallocate(".global.", "mat%ivpos")
       END IF
    END IF
  END SUBROUTINE FreeMatGraphVc

  !> \author Mikael PAPIN
  !!
  !! \brief D�-allocation de la matrice logique
  SUBROUTINE FreeMatVc(Mat)! semble non employ�e (RA18/12/10)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "FreeMatVc"
    TYPE(Matrice), INTENT(INOUT)  :: Mat
    DEALLOCATE(Mat%JvCell, Mat%JPosi, Mat%IvPos, Mat%IDiag)
    IF (see_alloc) THEN
       CALL alloc_deallocate(".global.", "mat%jvcell")
       CALL alloc_deallocate(".global.", "mat%jposi")
       CALL alloc_deallocate(".global.", "mat%ivpos")
       CALL alloc_deallocate(".global.", "mat%idiag")
    END IF
    IF (ASSOCIATED(Mat%Vals)) THEN
       DEALLOCATE(Mat%Vals)
       IF (see_alloc) THEN
          CALL alloc_deallocate(".global.", "mat%vals")
       END IF
    END IF
    IF (ASSOCIATED(Mat%ILUVals)) THEN
       DEALLOCATE(Mat%ILUVals)
       IF (see_alloc) THEN
          CALL alloc_deallocate(".global.", "mat%iluvals")
       END IF
    END IF
  END SUBROUTINE FreeMatVc

  !> \brief Modifies the mesh in order to match with the order of the scheme
  !!
  !! Update the fields of Mesh : EltType, Ndegre, Nu, NsFacFr, Coor
  !!
  !! \param Mesh : The mesh to update
  !! \param Data : We just need Data%Nordre to get the order of the scheme

  !#####################################
  SUBROUTINE MeshOrderUpdate(Mesh, Data)!
  !#####################################

    CHARACTER(LEN = *), PARAMETER :: mod_name = "MeshOrderUpdate"

    TYPE(MeshDef), INTENT(INOUT)              :: Mesh
    TYPE(Donnees), INTENT(IN)                 :: Data

    INTEGER :: jt, eltType, jq, jseg, is1, is2, Np
    INTEGER :: Nddl, nTypFr , oldTypFr, nHexa

    IF (Mesh%Nordre > Data%Nordre) THEN
       WRITE(*,*) 'ERROR: mesh order higher than the data order'
       STOP
    ENDIF

#ifdef SEQUENTIEL

#ifndef MOULINETTE

    ! No need to update the mesh
    ! Computation procedure
    IF (Mesh%Nordre == Data%Nordre) RETURN

#else

    ! No need to update the mesh
    ! Parallel post-pro procedure
    IF(Mesh%Nordre == 2 .AND. Data%Nordre == 2) RETURN

#endif

#else

    ! No need to update the mesh
    ! Computation procedure
    IF(Mesh%Nordre == 2 .AND. Data%Nordre == 2) RETURN
    
    ! In parallel only linear meshes are read.
    ! Higher order grids are handles like linear ones
    ! and at the end the coordinates of the extra DOF
    ! are corrected. 
    
#endif 

    ! Pour l'instant le seul truc qu'on sait faire c'est 
    ! de passer un maillage P1 et maillage P2
    IF (Data%Nordre == 3) THEN

       ! Comptage des ddls + modification des degre
       Nddl = Mesh%Npoint + Mesh%Nsegmt

         nTypFr = 3
       oldTypFr = 2

       nHexa  = 0

       DO jt = 1, Mesh%Nelemt

          eltType = Mesh%EltType(jt)

          SELECT CASE(eltType)

          CASE(et_triaP1)

             Mesh%Ndegre(jt)  = 6
             Mesh%EltType(jt) = et_triaP2

          CASE(et_QuadP1)

             Mesh%Ndegre(jt)  = 9
             Mesh%EltType(jt) = et_QuadP2

             ! On ajoute 1 ddl au milieu
             Nddl = Nddl + 1

          CASE(et_tetraP1)

               nTypFr = MAX(6,   nTypFr)
             oldTypFr = MAX(3, oldTypFr)

             Mesh%Ndegre(jt)  = 10
             Mesh%EltType(jt) = et_tetraP2

          CASE(et_HexaP1)

               nTypFr = MAX(9,   nTypFr)
             oldTypFr = MAX(4, oldTypFr)

             Mesh%Ndegre(jt)  = 27
             Mesh%EltType(jt) = et_HexaP2

             ! On ajoute 1 ddl au milieu
              Nddl = Nddl + 1
             nHexa = nHexa + 1 

          CASE DEFAULT

             PRINT *, mod_name, " Invalid element : ",Mesh%EltType(jt), " degre=", Mesh%Ndegre(jt) 
             CALL ABORT()

          END SELECT

       END DO

       Mesh%NDegreMax = MAXVAL( Mesh%Ndegre(:) )

       ! Sous-entend maillage entierement HEXA
       IF (Mesh%Ndim == 3 .AND. nHexa > 0 ) THEN
          ! Au final :  Mesh%Npoint + Mesh%Nsegmt + Mesh%Nfac + nHexa
          Nddl = Nddl + Mesh%Nfac
       END IF
       Mesh%NCells = Nddl

       ! Re-allocations
       CALL RE_ALLOCATE(Mesh%Coor, (/Mesh%Ndim     , Nddl        /))
       CALL RE_ALLOCATE(Mesh%Nu  , (/Mesh%NDegreMax, Mesh%Nelemt /))
       IF (Mesh%NFacFr > 0 ) THEN
          CALL RE_ALLOCATE(Mesh%NsFacFr, (/ nTypFr, Mesh%NFacFr/))
       END IF

       ! Remplissage de Nu
       jq = 0
       DO jt = 1, Mesh%Nelemt

          SELECT CASE(Mesh%Ndegre(jt))

          CASE(6) !-- triangle ordre 3

             Mesh%Nu(4:6, jt) = Mesh%Nuseg(1:3, jt) + Mesh%Npoint

          CASE(9) !-- quadrangle ordre 3

             jq = jq + 1
             Mesh%Nu(5: 8, jt) = Mesh%Nuseg(1: 4, jt) + Mesh%Npoint
             Mesh%Nu(9, jt) = jq + Mesh%Npoint + Mesh%Nsegmt

          CASE(10) !-- tetra ordre 3

             ! Ajoute les numeros des segments
             !Mesh%Nu(5:10, jt) = Mesh%Nuseg(1:6, jt) + Mesh%Npoint

             ! Change of numeration: gmsh ordering adopted
             ! the local nodes 9 and 10 are swiched --- dante
             Mesh%Nu(5:8, jt) = Mesh%Nuseg(1:4, jt) + Mesh%Npoint
             Mesh%Nu(9,   jt) = Mesh%Nuseg(6,   jt) + Mesh%Npoint
             Mesh%Nu(10,  jt) = Mesh%Nuseg(5,   jt) + Mesh%Npoint

          CASE(27) !-- hexa ordre 3

             jq = jq + 1

             Mesh%Nu(9:20, jt)  = Mesh%Nuseg(1:12, jt) + Mesh%Npoint

             !Mesh%Nu(21: 26, jt) = Mesh%NuFacette(1: 6, jt) + Mesh%Npoint + Mesh%Nsegmt
             ! Change of numeration: gmsh ordering adopted
             ! the local nodes 20 and 26 are swiched --- dante
             Mesh%Nu(21, jt) = Mesh%NuFacette(1, jt) + Mesh%Npoint + Mesh%Nsegmt
             Mesh%Nu(22, jt) = Mesh%NuFacette(3, jt) + Mesh%Npoint + Mesh%Nsegmt
             Mesh%Nu(23, jt) = Mesh%NuFacette(6, jt) + Mesh%Npoint + Mesh%Nsegmt
             Mesh%Nu(24, jt) = Mesh%NuFacette(4, jt) + Mesh%Npoint + Mesh%Nsegmt
             Mesh%Nu(25, jt) = Mesh%NuFacette(5, jt) + Mesh%Npoint + Mesh%Nsegmt
             Mesh%Nu(26, jt) = Mesh%NuFacette(2, jt) + Mesh%Npoint + Mesh%Nsegmt

             Mesh%Nu(27, jt) = jq + Mesh%Npoint + Mesh%Nsegmt + Mesh%NFac

          END SELECT
       END DO

       ! Remplissage de Coor
       DO jseg = 1, Mesh%Nsegmt

          is1 = Mesh%Nubo(1, jseg)
          is2 = Mesh%Nubo(2, jseg)

          Mesh%Coor(:, Mesh%Npoint + jseg) = 0.5d0 * ( Mesh%Coor(:, is1) +  Mesh%Coor(:, is2) )

       END DO

       IF (jq /= 0) THEN ! Cas des quadrangles ou hexas

          IF (Mesh%Ndim == 2) THEN ! quad

             jq = Mesh%Npoint + Mesh%Nsegmt + 1  ! 1er numero possible pour un ddl centre de gravite

             DO jt = 1, Mesh%Nelemt

                IF (Mesh%EltType(jt) == et_quadP2) THEN

                   Mesh%Coor(1, jq)= 0.25d0* SUM(Mesh%Coor(1, Mesh%Nu(1:4,jt)) )
                   Mesh%Coor(2, jq)= 0.25d0* SUM(Mesh%Coor(2, Mesh%Nu(1:4,jt)) )

                   jq = jq + 1

                END IF

             END DO

          ELSE ! hexa

             ! DDLs Faces
             jq = Mesh%Npoint + Mesh%Nsegmt + 1   ! 1er numero possible pour un ddl centre de gravite d'une face

             DO jt = 1, Mesh%NFac

                Mesh%Coor(1, jq)= 0.25d0* SUM(Mesh%Coor(1, Mesh%NuFac(1:4,jt)) )
                Mesh%Coor(2, jq)= 0.25d0* SUM(Mesh%Coor(2, Mesh%NuFac(1:4,jt)) )
                Mesh%Coor(3, jq)= 0.25d0* SUM(Mesh%Coor(3, Mesh%NuFac(1:4,jt)) )

                jq = jq + 1

             END DO

             ! DDLs Elements
             jq = Mesh%Npoint + Mesh%Nsegmt + Mesh%NFac + 1  ! 1er numero possible pour un ddl centre de gravite

             DO jt = 1, Mesh%Nelemt

                Mesh%Coor(1, jq)= 0.125* SUM(Mesh%Coor(1, Mesh%Nu(1:8,jt)) )
                Mesh%Coor(2, jq)= 0.125* SUM(Mesh%Coor(2, Mesh%Nu(1:8,jt)) )
                Mesh%Coor(3, jq)= 0.125* SUM(Mesh%Coor(3, Mesh%Nu(1:8,jt)) )

                jq = jq + 1

             END DO

          END IF

       END IF

#ifndef SEQUENTIEL
       ! Zero initialization of the coordinates
       ! of the extra dofs. It should prevents
       ! errors in the overwriting of the 
       ! coordinates for curved meshes in parallel
       ! computations
       IF(Mesh%NOrdre == 3) THEN
          Np = SIZE(Mesh%Coor, 2)
          Mesh%Coor(:, Mesh%NPoint+1:Np) = 0.d0
       ENDIF
#endif

       IF (Mesh%NFacFr > 0) THEN

          ! Remplissage de NsFacFr
          IF (Mesh%Ndim == 2) THEN

             Mesh%NsfacFr(3, 1:Mesh%NFacFr) = Mesh%NumFac(1:Mesh%NFacFr) + Mesh%Npoint

          ELSE

             CALL FindSegFr3D(Mesh)

             IF (jq /= 0) THEN 
                
                CALL FindFacFr3D(Mesh)

             ENDIF

          END IF

       END IF

       NULLIFY(Mesh%Coon)
       Mesh%Coon => Mesh%Coor

#ifdef SEQUENTIEL
#ifndef MOULINETTE
       Mesh%Nordre = 3
#endif
#endif

    ELSE

       PRINT *, mod_name, ": We don't know how to deal with Mesh%Nordre =" , Mesh%Nordre
       PRINT *, " and Data%Nordre =", Data%Nordre, " for dimension =" , Mesh%Ndim
       CALL ABORT()

    END IF

  CONTAINS

    !! \brief Finds the segments of the boundary faces in order to set the good numerotation of P2 dof
    SUBROUTINE FindSegFr3D(Mesh)
      TYPE(Meshdef), INTENT(INOUT) :: Mesh

      INTEGER                      :: jseg
      INTEGER :: ifr, eltfr , i, j, typSeg, typFr
      INTEGER, DIMENSION(2) :: sommetsFr, sommetsSeg

      DO ifr = 1, Mesh%NFacFr

         eltfr = Mesh%NumElemtFr(ifr)

         typSeg = COUNT(Mesh%Nuseg(:, eltfr) /= 0)

         typFr = COUNT(Mesh%NsFacFr(:, ifr) /= 0)

         DO i = 1, typFr

            sommetsFr(1) = Mesh%Nsfacfr(i, ifr)
            sommetsFr(2) = Mesh%Nsfacfr(MOD(i,typFr) + 1, ifr)

            DO j = 1, typSeg

               jseg = Mesh%Nuseg(j, eltfr)

               sommetsSeg(:) = Mesh%Nubo(1:2, jseg)

               IF (MINVAL(sommetsFr) == MINVAL(sommetsSeg) .AND. &
                   MAXVAL(sommetsFr) == MAXVAL(sommetsSeg) ) THEN

                  Mesh%NsfacFr(typFr + i, ifr) = jseg + Mesh%Npoint
                  
                  EXIT

               END IF

               IF (j == typSeg) THEN

                  PRINT *, "FindSegFr PAS TROUVE Frontiere : ", ifr

                  CALL ABORT()

               END IF

            END DO

         END DO

      END DO

    END SUBROUTINE FindSegFr3D

    SUBROUTINE FindFacFr3D(Mesh)

      TYPE(Meshdef), INTENT(INOUT) :: Mesh

      INTEGER :: i_b, N_faces, Np_f, Np_f_, &
                 jf, k, N_found, ele_b, Np

      LOGICAL :: Found
                       
      INTEGER, DIMENSION(6, 4), PARAMETER :: F_Hexa = &
               RESHAPE( (/ 1, 5, 1, 2, 3, 1, &
                           4, 6, 2, 3, 4, 5, &
                           3, 7, 6, 7, 8, 8, &
                           2, 8, 5, 6, 7, 4 /), (/6, 4/))

      INTEGER, DIMENSION(6), PARAMETER :: G_hexa = (/ 1, 6, 2, 4, 5, 3 /)
      !--------------------------------------------------

      DO i_b = 1, Mesh%NFacFr

         ele_b = Mesh%NumElemtFr(i_b)

         N_faces = COUNT( Mesh%NuFacette(:, ele_b) /= 0 )

         Np_f = COUNT( Mesh%NsfacFr(:, i_b) /= 0 )

         Np_f_ = Np_f/2

         Np = COUNT( Mesh%Nu(:, ele_b) /= 0 )

         found = .FALSE.

         DO jf = 1, N_faces
            
            N_found = 0

            DO k = 1, Np_f_

               IF( Mesh%NsfacFr(k, i_b) == Mesh%NU(F_Hexa(jf, k), ele_b)) THEN
                  
                  N_found = N_found + 1

               ENDIF

            ENDDO

            IF( N_found ==  Np_f_ ) THEN
               
               Mesh%NsfacFr(Np_f+1, i_b) = Mesh%Npoint + Mesh%Nsegmt + &
                                           Mesh%NuFacette(jf, ele_b)

               found = .TRUE.
               
               EXIT

            ENDIF
          
         ENDDO

         IF( .NOT. found ) THEN

            WRITE(*,*) 'ERROR: boundary face not found'
            STOP

         ENDIF

      ENDDO

    END SUBROUTINE FindFacFr3D
    
  END SUBROUTINE MeshOrderUpdate
  !#############################


  !#############################################
  SUBROUTINE  SetModMesh( DATA, Mesh, Mesh_new )
  !#############################################

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "SetModMesh"

    TYPE(Donnees),  INTENT(IN)  :: DATA
    TYPE(MeshDef),  INTENT(IN)  :: Mesh
    TYPE(Maillage), INTENT(OUT) :: Mesh_new
    !--------------------------------------

    INTEGER :: eltType, Nsommets, Ndegre
    INTEGER :: jt, k, i, ifr
    !--------------------------------------
    
    Mesh_new%Npoint   = Mesh%Npoint
    Mesh_new%Ndofs    = Mesh%NCells ! already updated
    Mesh_new%Nelement = Mesh%Nelemt

    SELECT CASE(DATA%Nordre)

    CASE(2)
             
       ALLOCATE( Mesh_new%maill(Mesh_new%Nelement) )

       DO jt = 1, Mesh%Nelemt
          
          eltType = Mesh%EltType(jt)

          Mesh_new%maill(jt)%EltType = eltType

          SELECT CASE(eltType)

          CASE(et_triaP1)

             Mesh_new%maill(jt)%Ndegre   = 3
             Mesh_new%maill(jt)%NSommets = 3

          CASE(et_QuadP1)

             Mesh_new%maill(jt)%Ndegre   = 4
             Mesh_new%maill(jt)%NSommets = 4

          CASE(et_tetraP1)

             Mesh_new%maill(jt)%NSommets = 4
             Mesh_new%maill(jt)%Ndegre   = 4

          CASE(et_HexaP1)

             Mesh_new%maill(jt)%Ndegre   = 9
             Mesh_new%maill(jt)%NSommets = 9

          CASE DEFAULT

             PRINT *, mod_name, " Invalid element : ",Mesh%EltType(jt), " degre=", Mesh%Ndegre(jt) 
             CALL ABORT()

          END SELECT

          Nsommets = Mesh_New%Maill(jt)%Nsommets

          ALLOCATE( Mesh_New%Maill(jt)%Nu(Nsommets))

          DO k = 1, Mesh_new%Maill(jt)%Nsommets
             Mesh_New%Maill(jt)%Nu(k) = Mesh%Nu(k, jt)
          ENDDO

       END DO

    CASE(3)

       ! Pour l'instant le seul truc qu'on sait faire
       ! c'est de passer un maillage P1 et maillage P2
       ! Comptage des ddls + modification des degre

       ALLOCATE( Mesh_new%maill(Mesh%Nelemt) )

       DO jt = 1, Mesh%Nelemt

          eltType = Mesh%EltType(jt)

          SELECT CASE(eltType)

          CASE(et_triaP2)

             Mesh_new%maill(jt)%NSommets = 6
             Mesh_new%maill(jt)%Ndegre   = 3
             Mesh_new%maill(jt)%EltType  = et_triaP2

          CASE(et_QuadP2)

             Mesh_new%maill(jt)%NSommets = 9
             Mesh_new%maill(jt)%Ndegre   = 4
             Mesh_new%maill(jt)%EltType  = et_QuadP2

          CASE(et_tetraP2)

             Mesh_new%maill(jt)%NSommets = 10
             Mesh_new%maill(jt)%Ndegre   = 4
             Mesh_new%maill(jt)%EltType  = et_tetraP2

          CASE(et_HexaP2)

             Mesh_new%maill(jt)%NSommets = 27
             Mesh_new%maill(jt)%Ndegre   = 8
             Mesh_new%maill(jt)%EltType  = et_HexaP2

          CASE DEFAULT

             PRINT *, mod_name, " Invalid element : ",Mesh%EltType(jt), " degre=", Mesh%Ndegre(jt) 
             CALL ABORT()

          END SELECT

       END DO

    CASE default

       PRINT *, mod_name, ": We don't know how to deal with Mesh%Nordre =" , Mesh%Nordre
       PRINT *," and Data%Nordre =", Data%Nordre, " for dimension =" , Mesh%Ndim
       CALL ABORT()

    END SELECT

    ! On cree maintenant la structure Mesh_new%Maill%coor
    DO jt = 1, Mesh_New%Nelement

       Nsommets = Mesh_New%Maill(jt)%Nsommets
       Ndegre   = Mesh_New%Maill(jt)%Ndegre

       ALLOCATE( Mesh_New%Maill(jt)%Nu(Nsommets) )
       ALLOCATE( Mesh_New%Maill(jt)%coor(Mesh%Ndim, Nsommets) )
       ALLOCATE( Mesh_New%Maill(jt)%n(Mesh%ndim, Nsommets) )

       Mesh_New%Maill(jt)%Nu(:) = Mesh%Nu(:, jt)
       
       DO k = 1, Nsommets
          Mesh_New%Maill(jt)%coor(:, k) = Mesh%coor(:, Mesh_New%Maill(jt)%Nu(k) )
       ENDDO

       DO k = 1, Nsommets
          Mesh_New%Maill(jt)%n(:,k) = Mesh_New%Maill(jt)%normale(k)
       ENDDO

       Mesh_New%Maill(jt)%volume = Mesh_New%Maill(jt)%aire()
       
    ENDDO

    Mesh_New%Ndim   = Mesh%Ndim
    Mesh_New%Nfac   = Mesh%Nfac
    Mesh_New%NfacFr = Mesh%NfacFr 

    Mesh_New%NdegreMax   = MAXVAL(Mesh_new%Maill(:)%Ndegre)
    Mesh_New%NSommetsMax = MAXVAL(Mesh_new%Maill(:)%NSommets)

    ! Netoyage. 
    ! On fait les frontieres maintenant
    IF (Mesh%NFacFr > 0) THEN
       
       ALLOCATE( Mesh_New%fr(Mesh%NfacFr) )
       
       DO ifr = 1, Mesh%NfacFr

          IF (Mesh%Ndim == 2) THEN

             Mesh_New%fr(ifr)%Ndegre = 2 ! ce sont des segments

          ELSEIF (Mesh%Ndim == 3) THEN

             IF (SIZE(Mesh%NsfacFr, 1) == 3 .OR. &
             &   SIZE(Mesh%NsfacFr, 1) == 6) THEN
                
                Mesh_New%fr(ifr)%Ndegre = 3 ! triangles

             ELSEIF (SIZE(Mesh%NsfacFr, 1) == 4 .OR. &
             &       SIZE(Mesh%NsfacFr, 1) == 9) THEN

                Mesh_New%fr(ifr)%Ndegre = 4 ! quadrangles

             ENDIF
             
          ENDIF
         
          Mesh_New%fr(ifr)%Nsommets = SIZE(Mesh%NsfacFr, 1)

          ! cela suppose que tous les faces frontieres sont identiques
          ALLOCATE( Mesh_New%fr(ifr)%Nu(1:SIZE(Mesh%NsfacFr, 1)) ) 

          ! ici je suppose que l'arete est plane. On ne met pas les coordonnees
          ALLOCATE( Mesh_New%fr(ifr)%n(Mesh%Ndim, 1) )           

         !>\bug GeomGraph MeshOrderUpdate L 371 : We assume that the boundary faces are planar. 
          ALLOCATE( Mesh_New%fr(ifr)%coor(Mesh%Ndim, Mesh_New%fr(ifr)%Nsommets) )

          Mesh_New%fr(ifr)%n(:,1) = Mesh%VnoFr(:, ifr) !? strano

          DO i = 1, Mesh_New%fr(ifr)%Nsommets

             Mesh_New%fr(ifr)%Nu(i)      = Mesh%NsfacFr(i, ifr)
             Mesh_New%fr(ifr)%inum       = Mesh%LogFr(ifr)
             Mesh_New%fr(ifr)%coor(:, i) = Mesh%coor(:, Mesh%NsfacFr(i, ifr))

          ENDDO

       ENDDO

    END IF  

    PRINT*, "points", Mesh%Npoint,Mesh_New%Npoint, Mesh%Ncells,Mesh_New%Ndofs
    print*, "segments max", maxval(mesh_new%fr(:)%Ndegre), maxval(mesh_new%fr(:)%Nsommets)
    print*, "segments min", minval(mesh_new%fr(:)%Ndegre), minval(mesh_new%fr(:)%Nsommets)
    PRINT*, mod_name," FIN"

  END SUBROUTINE  SetModMesh
  !#########################

  !========================================
  SUBROUTINE Find_Wall_Overlapas(Com, Mesh)
  !========================================
  !
  ! For each wall-boundary element, find the
  ! the number of domain overlaps. 
  ! This is used to reduce correctly the 
  ! boundary integrals during the computation 
  ! of the areodynamic forces in parallel.
  ! 
  ! b_ovlp = 1, default
  ! b_ovlp = # ovelaps for the boundary 
  !          elements on the overlap
  !
    IMPLICIT NONE

    TYPE(MeshCom), INTENT(IN)    :: Com
    TYPE(MeshDef), INTENT(INOUT) :: Mesh
    !-----------------------------------

    INTEGER :: bc_type, NCellsIn, NU_max

    INTEGER :: jf, j_dom, k
    !-----------------------------------

    ALLOCATE( Mesh%b_ovlp(Mesh%NfacFr) )

    Mesh%b_ovlp = 1
    
    ! # of nodes without 'ghost' nodes
    NCellsIn = Mesh%NCells - Com%NPrcv2tot

    DO jf = 1, Mesh%NfacFr

       bc_type = Data%FacMap( Mesh%LogFr(jf) )

       IF( bc_type == lgc_paroi_adh_adiab_flux_nul .OR. &
           bc_type == lgc_paroi_misc               .OR. &
           bc_type == lgc_paroi_glissante ) THEN

          NU_max = MAXVAL(Mesh%NsfacFr(:, jf))
          
          IF( NU_max > NCellsIn ) THEN

             DO j_dom = 1, Com%Ndomv

                DO k = 1, Com%NPrcv2(j_dom)

                   IF( Com%PTrcv2(k, j_dom) == NU_max ) THEN

                      Mesh%b_ovlp(jf) = Mesh%b_ovlp(jf) + 1

                   ENDIF

                ENDDO ! receiving nodes loop

             ENDDO ! overlaps loop

          ENDIF ! if nu > nu_max

       ENDIF ! if wall

    ENDDO ! boundary elements loop

  END SUBROUTINE Find_Wall_Overlapas
  !=================================  

  !=================================
  SUBROUTINE NodeTOele_connect(Mesh)
  !=================================

    IMPLICIT NONE

    TYPE(MeshDef), INTENT(IN) :: Mesh
    !---------------------------------

    INTEGER, DIMENSION(:), ALLOCATABLE :: temp_ele
    INTEGER, DIMENSION(:), ALLOCATABLE :: Nu

    INTEGER :: je, jf, k, N_, n_v
    !----------------------------------------------

    ALLOCATE( Connect(Mesh%NCells) )

    !--------------------------------------
    ! Node-to-domain elements connectivity
    !-----------------------------------------------
    DO je = 1, Mesh%Nelemt

       ! # DOFs of the element
       n_v = COUNT( Mesh%Nu(:, je) /= 0 )
       
       ALLOCATE( NU(n_v) )

       NU = Mesh%Nu(1:n_v, je)

       DO k = 1, n_v
          
          IF(.NOT. ALLOCATED(Connect(NU(k))%cn_ele)) THEN

             ALLOCATE( Connect(NU(k))%cn_ele(1) )

             Connect(NU(k))%cn_ele = je

          ELSE
      
             N_ = SIZE(Connect(NU(k))%cn_ele)
   
             ALLOCATE( temp_ele(N_+1) )

             temp_ele(1:N_) = Connect(NU(k))%cn_ele
             temp_ele(N_+1) = je

             DEALLOCATE( Connect(NU(k))%cn_ele )
             ALLOCATE( Connect(NU(k))%cn_ele(N_+1) )

             Connect(NU(k))%cn_ele = temp_ele

             DEALLOCATE(temp_ele)

          ENDIF

       ENDDO

       DEALLOCATE(NU)

    ENDDO

    !----------------------------------------
    ! Node-to-boundary elements connectivity
    !-----------------------------------------------
    DO jf = 1, Mesh%NfacFr

       ! # DOFs of the element
       n_v = COUNT( Mesh%NsfacFr(:, jf) /= 0 )

       ALLOCATE( NU(n_v) )

       NU = Mesh%NsfacFr(1:n_v, jf)

          DO k = 1, n_v
          
          IF(.NOT. ALLOCATED(Connect(NU(k))%cn_bele)) THEN

             ALLOCATE( Connect(NU(k))%cn_bele(1) )

             Connect(NU(k))%cn_bele = jf

          ELSE
      
             N_ = SIZE(Connect(NU(k))%cn_bele)
   
             ALLOCATE( temp_ele(N_+1) )

             temp_ele(1:N_) = Connect(NU(k))%cn_bele
             temp_ele(N_+1) = jf

             DEALLOCATE( Connect(NU(k))%cn_bele )
             ALLOCATE( Connect(NU(k))%cn_bele(N_+1) )

             Connect(NU(k))%cn_bele = temp_ele

             DEALLOCATE(temp_ele)

          ENDIF

       ENDDO

       DEALLOCATE(NU)

    ENDDO

  END SUBROUTINE NodeTOele_connect
  !===============================  

  !=================================
  SUBROUTINE Periodic_connect(N_dof)
  !=================================

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: N_dof
    !--------------------------------

    LOGICAL, DIMENSION(:),   ALLOCATABLE :: seen
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: per_list

    REAL,    DIMENSION(:,:), ALLOCATABLE :: per_coor

    INTEGER :: bc_idx, bc_type, N_tot_p, N_c
    INTEGER :: jf, k, Nu, n, i, j
    !-------------------------------

    ALLOCATE( seen(N_dof) )
    seen = .FALSE.

    DO jf = 1, SIZE(b_element)

       bc_idx  = b_element(jf)%b_f%bc_type
       bc_type = Data%FacMap(bc_idx)

       IF(bc_type == lgc_periodique) THEN

          DO k = 1, b_element(jf)%b_f%N_points

!if( b_element(jf)%b_f%Coords(1, k) > -0.8 .AND. &
!    b_element(jf)%b_f%Coords(1, k) < 2.0 ) then

             Nu = b_element(jf)%b_f%Nu(k)

             IF( .NOT. seen(Nu) ) seen(Nu) = .TRUE.

!endif

          ENDDO

       ENDIF

    ENDDO

    N_tot_p = COUNT( seen )

    ALLOCATE( per_list(N_tot_p, 2) )
    ALLOCATE( per_coor(Data%NDim, N_tot_p) )
    
    seen = .FALSE.; n = 0
    DO jf = 1, SIZE(b_element)

       bc_idx  = b_element(jf)%b_f%bc_type
       bc_type = Data%FacMap(bc_idx)

       IF(bc_type == lgc_periodique) THEN

          DO k = 1, b_element(jf)%b_f%N_points

!if( b_element(jf)%b_f%Coords(1, k) > -0.8 .AND. &
!    b_element(jf)%b_f%Coords(1, k) < 2.0 ) then

             Nu = b_element(jf)%b_f%Nu(k)

             IF( .NOT. seen(Nu) ) THEN

                seen(NU) = .TRUE.

                n = n + 1

                per_list(n, :) = (/ Nu, bc_idx /)
                per_coor(:, n) = b_element(jf)%b_f%Coords(:, k)

             ENDIF
             
!endif

          ENDDO

       ENDIF

    ENDDO

    ALLOCATE( per_conn(N_tot_p/2, 2) )

    n = 0; seen = .FALSE.; per_conn = 0
    DO i = 1, N_tot_p

       IF( seen(per_list(i, 1)) ) CYCLE

       DO j = 1, N_tot_p

          IF( j == i  ) CYCLE

          IF( seen(per_list(j, 1)) ) CYCLE

          IF( per_list(j, 2) == per_list(i, 2) ) CYCLE

          N_c = COUNT( ABS(per_coor(:, i) - per_coor(:, j)) < 1.0e-5 ) !***

          IF( N_c == Data%NDim - 1) THEN

             n = n + 1
             per_conn(n, :) = (/ per_list(i,1), per_list(j,1) /)

             seen( per_list(j,1) ) = .TRUE.

          ENDIF

       ENDDO

    ENDDO

    DEALLOCATE( seen, per_list, per_coor )

  END SUBROUTINE Periodic_connect
  !==============================

  SUBROUTINE GraphMatVertexCentered( DATA, Mesh, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GraphMatVertexCentered"
    ! ***************
    !   declarations
    ! ***************
    ! variables d'appel
    TYPE(Matrice), INTENT(OUT)                :: Mat
    TYPE(MeshDef), INTENT(INOUT)              :: Mesh
    TYPE(Donnees), INTENT(IN)                 :: DATA
    ! variables locales
    INTEGER                          :: Npoint, Nelement
    INTEGER                          :: Info
    INTEGER                          :: is, iv, jt, k, is1, is2, start
    INTEGER                          :: next, ntyp
    INTEGER        :: jv, NCoefMat
    REAL(KIND = 4) :: Dist
    REAL(KIND = 4), DIMENSION(:, : ), ALLOCATABLE :: prof
    ! variables locales
    INTEGER, DIMENSION(: ),   POINTER, SAVE         :: NbVois !-- CCE CCE
    TYPE(cellule), POINTER                          :: NewCell, PtCell, PtCellPred
    TYPE(cellule), DIMENSION(: ), POINTER           :: Voisins

    ! ***************************
    !   initialisations
    ! ***************************

    Npoint   = Mesh%NCells
    Nelement = Mesh%Nelemt

    IF (Npoint == 0) THEN
       PRINT *, mod_name, " ERREUR : Npoint==0"
       CALL delegate_stop()
    END IF

    ALLOCATE(Voisins(1: Npoint), STAT = Info)
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "voisins", SIZE(Voisins))
    END IF
    IF (Info /= 0) THEN
       PRINT *, mod_name, " ERREUR : Impossible d'allouer Voisins", Npoint
       CALL delegate_stop()
    END IF

    DO is = 1, Npoint
       Voisins(is)%val = 0
       NULLIFY(Voisins(is)%suiv)
    END DO

    ! ***************************************
    !   construction de Voisins
    ! ***************************************

    IF ( data%Impre > 2 ) THEN
!#ifdef DEBUG
       WRITE(6, *) mod_name, " Construction de la table des points voisins ... au sens elements et pas segments"
!#endif
    END IF

    ! construction de la table des voisins
    NULLIFY(PtCell, PtCellPred )

    DO jt = 1, Nelement 
       ntyp = Mesh%Ndegre(jt)
       DO k = 1, ntyp ! Parcours les sommets des �l�ments
          is1  = Mesh%Nu(   k, jt) !-- num�ro de degr� de libert�, 1..nCells

          ! Commencer start � k+1 ne change rien et fait donc gagner du temps; Adam 26.02.08

          start = k + 1

          DO next = start, ntyp
             is2  = Mesh%Nu(next, jt)  !-- num�ro de degr� de libert�, 1..nCells
             ! Voisins de is1
             PtCell     => Voisins(is1)%suiv
             PtCellPred => Voisins(is1)

             ! on recherche la place du nouveau voisin
             DO
                IF (.NOT. ASSOCIATED(PtCell)) THEN
                   EXIT
                END IF
                IF (PtCell%val > is2) THEN
                   EXIT ! on a trouve la place, on arr�te
                END IF
                PtCellPred => PtCell
                PtCell     => PtCell%suiv
             END DO

             IF (PtCellPred%val < is2) THEN ! on teste si le sommet n'est pas d�j� l�
                ! cr�ation de la nouvelle cellule
                ALLOCATE(NewCell)
                IF (see_alloc) THEN
                   CALL alloc_allocate(mod_name, "newcell.a", 1)
                END IF
                NewCell%val  = is2
                ! insertion de la nouvelle cellule
                NewCell%suiv    => PtCell
                PtCellPred%suiv => NewCell
             END IF

             ! Voisins de is2
             PtCell     => Voisins(is2)%suiv
             PtCellPred => Voisins(is2)
             ! on recherche la place du nouveau voisin
             DO
                IF (.NOT. ASSOCIATED(PtCell)) THEN
                   EXIT
                END IF
                IF (PtCell%val > is1) THEN
                   EXIT ! on a trouv� la place, on arr�te
                END IF
                PtCellPred => PtCell
                PtCell     => PtCell%suiv
             END DO
             ! on teste si le sommet n'est pas deja la
             IF (PtCellPred%val < is1) THEN
                ! creation de la nouvelle cellule
                ALLOCATE(NewCell)
                IF (see_alloc) THEN
                   CALL alloc_allocate(mod_name, "newcell.b", 1)
                END IF
                NewCell%val  = is1
                ! insertion de la nouvelle cellule
                NewCell%suiv    => PtCell
                PtCellPred%suiv => NewCell
             END IF
          END DO !-- next = start, ntyp
       END DO !-- k = 1, ntyp ! Parcours les sommets des �l�ments
    END DO !-- jt = 1, Nelemt ! Parcours des �l�ments


    !    PRINT *, mod_name, ' STOP, pour voir : le tableau temporaire Voisins est constitue'
    ! CALL delegate_stop()


    ! allocation memoire
    NULLIFY(NbVois)
    ALLOCATE(NbVois(1: Npoint) )


    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nbvois", Npoint)
    END IF


    NbVois(1: Npoint) = 0


    DO is = 1, Npoint
       PtCell => Voisins(is)%suiv
       ! on prend tous les voisins du sommet is
       DO
          IF (.NOT. ASSOCIATED(PtCell)) THEN
             EXIT
          END IF
          NbVois(is)    = NbVois(is) + 1
          ! on pointe sur le voisin suivant
          PtCell => PtCell%suiv
       END DO
    END DO


    Mat%NCoefMat =  SUM( NbVois )
    NCoefMat     =  Mat%NCoefMat

    NULLIFY(Mat%JvCell, Mat%JPosi)
    ALLOCATE(Mat%JvCell(1: NCoefMat))
    ALLOCATE(Mat%JPosi(1: Npoint + 1))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mat%jvcell", NCoefMat)
       CALL alloc_allocate(mod_name, "mat%jposi", Npoint + 1)
    END IF
    k = 1
    DO is = 1, Npoint
       PtCell => Voisins(is)%suiv
       DO iv = 1, NbVois(is)
          Mat%JvCell(k) = PtCell%val !-- num�ro de degr� de libert�, 1..nCells
          k = k + 1
          PtCell => PtCell%suiv
       END DO
    END DO

    ! Nettoyage memoire !!
    DO is = 1, Npoint
       PtCellPred => Voisins(is)%suiv
       PtCell => PtCellPred%suiv
       DO
          DEALLOCATE(PtCellPred)

          IF (see_alloc) THEN
             CALL alloc_deallocate(".dummy.", "ptcellpred")
          END IF
          IF (.NOT.ASSOCIATED(PtCell)) THEN
             EXIT
          END IF
          PtCellPred => PtCell
          PtCell => PtCell%suiv
       END DO
    END DO

#ifdef DEBUG
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Construction du tableau JPosi"
    END IF
#endif

    ! Calcul de la matrice logique, priv�e de sa diagonale

    Mat%JPosi(1) = 1 ! D�but des coeffs non nuls de la ligne is de la matrice pleine dans le stockage ligne
    DO is = 2, Npoint + 1
       Mat%JPosi(is) = Mat%JPosi(is -1)  + NbVois(is -1)
    END DO

#define DEBUG
#ifdef DEBUG
    DO is = 2, Npoint + 1
       DO jv =  Mat%JPosi(is -1), Mat%JPosi(is) -2
          IF ( Mat%JvCell(jv) >=  Mat%JvCell(jv + 1) ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!! Mat%JvCell(jv) >=  Mat%JvCell(jv+1)"
             CALL delegate_stop()
          END IF
          IF ( Mat%JvCell(jv) == is -1 ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!! Mat%JvCell(jv) == is-1"
             CALL delegate_stop()
          END IF
       END DO
    END DO
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " OK OK OK !!!!!!!"
    END IF
#endif


#define INFO
#ifdef INFO
    ! EVALUATION SUR LE PROFIL DE LA MATRICE
    ALLOCATE(prof(1: 2, Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "prof", SIZE(prof))
    END IF
    DO is = 1, Npoint
       prof(:, is) = 0.0
       DO jv =  Mat%JPosi(is), Mat%JPosi(is + 1) -1
          IF (Mat%JvCell(jv) > is) THEN
             CYCLE ! On ne consid�re que la matrice triangulaire inf�rieure
          END IF
          Dist = is - Mat%JvCell(jv)
          prof(1, is) = MAX(prof(1, is), Dist)
          prof(2, is) = prof(2, is) + Dist
       END DO
       prof(2, is) = prof(2, is) / NbVois(is)
    END DO
    IF (Data%impre > 0) THEN
       PRINT *, mod_name, " EVALUATION DU PROFIL DE LA MATRICE:"
       PRINT *, '  --> Ecart maximal       =', MAXVAL(prof(1, : ))
       PRINT *, '  --> Ecart moyen maximal =', MAXVAL(prof(2, : ))
       PRINT *, '  --> Ecart moyen         =', SUM(prof(2, : )) / Npoint
    END IF
    DEALLOCATE(prof)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "prof")
    END IF
#endif
  END SUBROUTINE GraphMatVertexCentered

  !> \author     Boniface Nkonga      Universite de Bordeaux 1
  !!
  !! \author     Mikael Papin         INRIA 
  !! \author  R�mi Abgrall INRIA (1 modif)
  !!
  !! Objet : Entree: matrice logique (points voisins) sans la diagonale
  !!         Sortie: matrice logique avec la diagonale.
  !!         Format CSR
  !! Reconstruit mat%JvCell,
  !! puis construit Mat%IvPos
  !! puis v�rifie les r�sultats ainsi calcul�s (les permutations de points et la position de la diagonale)

  SUBROUTINE GraphMatAddDiag( DATA, Mesh, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GraphMatAddDiag"
    ! ***************
    !   declarations
    ! ***************
    ! variables d'appel
    TYPE(Matrice), INTENT(INOUT)          :: Mat !-- CCE R.B. 2008/12/10 Mat%JvCell, Mat%Jposi sont IN
    TYPE(MeshDef), INTENT(IN)             :: Mesh ! On utilise que Mesh%Ndofs
    TYPE(Donnees), INTENT(IN)             :: DATA

    ! variables locales
    INTEGER                          :: Npoint, Deb,  Fin
    INTEGER                          :: is, js, iv, jv
    INTEGER                          :: is1, is2, k
    INTEGER,  DIMENSION(: ), ALLOCATABLE           :: JvCell
    LOGICAL                          :: diag
    INTEGER, DIMENSION(:),ALLOCATABLE      :: NPtv
    ALLOCATE(NPtv( 1: Mesh%NCells))
    Npoint = SIZE(Mat%JPosi) -1

    ALLOCATE(JvCell(1: Mat%NCoefMat))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "jvcell", SIZE(JvCell))
    END IF
    JvCell = Mat%JvCell
    DEALLOCATE(Mat%JvCell)
    IF (see_alloc) THEN
       CALL alloc_deallocate(".global.", "mat%jvcell")
    END IF
    Mat%NCoefMat = Mat%NCoefMat + Npoint
    ALLOCATE(Mat%JvCell(1: Mat%NCoefMat))
    ALLOCATE(Mat%IDiag(1: Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mat%jvcell", SIZE(Mat%JvCell))
       CALL alloc_allocate(mod_name, "mat%idiag", SIZE(Mat%IDiag))
    END IF

    iv = 1
    DO is = 1, Npoint  ! Boucles sur les lignes de MAt
       ! Parcours de la ligne is
       Deb = Mat%JPosi(is)
       Fin = Mat%JPosi(is + 1) -1
       diag = .FALSE.
       DO js = Deb, Fin
          jv = JvCell(js)
          IF (jv < is .AND. js /= Fin) THEN ! DIAG = .F.
             Mat%JvCell(iv) = jv
             iv = iv + 1
          ELSE IF ( diag ) THEN ! js > is et DIAG =.T.
             Mat%JvCell(iv) = jv
             iv = iv + 1
          ELSE IF (jv > is) THEN
             diag = .TRUE.
             Mat%JvCell(iv) = is
             Mat%JvCell(iv + 1) = jv
             Mat%IDiag(is) = iv
             iv = iv + 2
          ELSE IF (js == Fin) THEN
             diag = .TRUE.
             Mat%JvCell(iv) = jv
             Mat%JvCell(iv + 1) = is
             Mat%IDiag(is) = iv + 1
             iv = iv + 2
          ELSE
             WRITE(6, *) mod_name, " ERREUR : CAS NON PREVU"
             CALL delegate_stop()
          END IF
       END DO
       Mat%JPosi(is) = Mat%JPosi(is) + is -1
    END DO
    Mat%JPosi(Npoint + 1) = Mat%NCoefMat + 1
    DEALLOCATE(JvCell)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "jvcell")
    END IF

    DO is = 1, Npoint
       DO jv =  Mat%JPosi(is), Mat%JPosi(is + 1) -2
          IF ( Mat%JvCell(jv) >=  Mat%JvCell(jv + 1) ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!! Mat%JvCell(jv) >=  Mat%JvCell(jv+1)"
             CALL delegate_stop()
          END IF
       END DO
    END DO
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " OK OK OK !!!!!!!"
    END IF

    ! IvPos(jposi(is2):jposi(is2+1)-1) :
    ! place dans le stockage ligne  de  la colonne (:, is2),
    NPtv(1: Npoint) = Mat%JPosi(1: Npoint)

    NULLIFY(Mat%IvPos)
    ALLOCATE(Mat%IvPos(1: Mat%NCoefMat))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mat%ivpos", SIZE(Mat%IvPos))
    END IF

    DO  is1 = 1, Npoint
       boucle_k : DO k = Mat%JPosi(is1), Mat%JPosi(is1 + 1) -1
          is2  = Mat%JvCell(k) !-- un voisin de is1, 1..ncells
          IF (is1 > is2) THEN
             Mat%IvPos(NPtv(is2)) = NPtv(is1)
             Mat%IvPos(NPtv(is1)) = NPtv(is2)
             NPtv(is2) = NPtv(is2) + 1
             NPtv(is1) = NPtv(is1) + 1
          ELSE IF (is1 == is2) THEN
             Mat%IvPos(NPtv(is1)) = NPtv(is1)
             NPtv(is1) = NPtv(is1) + 1
          END IF
       END DO boucle_k
    END DO

    DO is = 1, Npoint
       IF ( NPtv(is) /=  Mat%JPosi(is + 1) ) THEN
          PRINT *, is, NPtv(is), Mat%JPosi(is + 1)
          WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!! PtV(is) /=  Mat%JPosi(is+1)"
          CALL delegate_stop()
       END IF
    END DO

    Mat%NNtot = Npoint

    toto: DO is1 = 1, Npoint
       Deb = Mat%JPosi(is1)
       Fin = Mat%JPosi(is1 + 1) -1
       DO k = Deb, Fin
          is2 = Mat%JvCell(k)
          IF (is1 == is2) THEN
             IF (k - Mat%IDiag(is1) /= 0) THEN
                PRINT *, mod_name, " ERREUR : Pb dans Mat%IDiag"
                CALL delegate_stop()
             END IF
             CYCLE toto
          END IF
       END DO
       PRINT *, mod_name, " ERREUR : Terme diag pas trouve: ", is1
       CALL delegate_stop()
    END DO toto
    DEALLOCATE(NPtv)
  END SUBROUTINE GraphMatAddDiag


  !> Objet: Construction du Graphe de la matrice (sans la diag, donc)
  !! Bas� sur les voisins au sens "�l�ment",
  !! avec conditions aux limites periodiques.
  !! Cas Vertex Centered.
  SUBROUTINE GraphMatVertexCenteredPer( DATA, Mesh, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GraphMatVertexCenteredPer"
    ! ***************
    !   declarations
    ! ***************
    ! variables d'appel
    TYPE(Matrice), INTENT(OUT)                :: Mat
    TYPE(MeshDef), INTENT(INOUT)             :: Mesh
    TYPE(Donnees), INTENT(IN)                 :: DATA
    ! variables locales
    INTEGER                          :: Npoint, Nelemt
    INTEGER                          :: Info
    INTEGER                          :: is, iv, jt, k, is1, is2
    INTEGER                          :: next, ntyp
    INTEGER        :: jv, NCoefMat
    REAL(KIND = 4) :: Dist
    REAL(KIND = 4), DIMENSION(:, : ), ALLOCATABLE :: prof
    ! variables locales
    INTEGER, DIMENSION(: ),  POINTER, SAVE          :: NbVois !-- CCE CCE
    TYPE(cellule), POINTER                          :: NewCell, PtCell, PtCellPred
    TYPE(cellule), DIMENSION(: ), POINTER           :: Voisins
    ! ***************************
    !   initialisations
    ! ***************************

    Npoint   = Mesh%Npoint
    Nelemt   = Mesh%Nelemt

    ALLOCATE(Voisins(1: Npoint), STAT = Info)
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "voisins", SIZE(Voisins))
    END IF

    DO is = 1, Npoint
       Voisins(is)%val = 0
       NULLIFY(Voisins(is)%suiv)
    END DO

    ! ***************************************
    !   construction de Voisins
    ! ***************************************

    IF ( data%Impre > 2 ) THEN
#ifdef DEBUG
       WRITE(6, *) mod_name, " Construction de la table des points voisins ... au sens elements et pas segments"
#endif
    END IF

    ! construction de la table des voisins
    NULLIFY(PtCell, PtCellPred )
    DO jt = 1, Nelemt ! Parcours des elements
       ntyp = Mesh%Ndegre(jt)
       DO k = 1, ntyp ! Parcours les sommets des �l�ments
          is1  =  Mesh%LogPer( Mesh%Nu(   k, jt) )
          DO next = 1, ntyp
             IF (k == next) THEN
                CYCLE
             END IF
             is2  =  Mesh%LogPer( Mesh%Nu(next, jt))
             ! Voisins de is1
             PtCell     => Voisins(is1)%suiv
             PtCellPred => Voisins(is1)
             ! on recherche la place du nouveau voisin
             DO
                IF (.NOT. ASSOCIATED(PtCell)) THEN
                   EXIT
                END IF
                IF (PtCell%val > is2) THEN
                   EXIT
                END IF
                ! on a trouve la place, on arrete
                PtCellPred => PtCell
                PtCell     => PtCell%suiv
             END DO
             ! on teste si le sommet n'est pas deja la
             IF (PtCellPred%val < is2) THEN
                ! creation de la nouvelle cellule
                ALLOCATE(NewCell)
                IF (see_alloc) THEN
                   CALL alloc_allocate(mod_name, "newcell.z", 1)
                END IF
                NewCell%val  = is2
                ! insertion de la nouvelle cellule
                NewCell%suiv    => PtCell
                PtCellPred%suiv => NewCell
             END IF

             ! Voisins de is2
             PtCell     => Voisins(is2)%suiv
             PtCellPred => Voisins(is2)
             ! on recherche la place du nouveau voisin
             DO
                IF (.NOT. ASSOCIATED(PtCell)) THEN
                   EXIT
                END IF
                IF (PtCell%val > is1) THEN
                   EXIT
                END IF
                ! on a trouve la place, on arrete
                PtCellPred => PtCell
                PtCell     => PtCell%suiv
             END DO
             ! on teste si le sommet n'est pas deja la
             IF (PtCellPred%val < is1) THEN
                ! creation de la nouvelle cellule
                ALLOCATE(NewCell)
                IF (see_alloc) THEN
                   CALL alloc_allocate(mod_name, "newcell.y", 1)
                END IF
                NewCell%val  = is1
                ! insertion de la nouvelle cellule
                NewCell%suiv    => PtCell
                PtCellPred%suiv => NewCell
             END IF
          END DO
       END DO
    END DO


    !PRINT *, mod_name, ' ERREUR : le tableau temporaire Voisins est constitue'
    ! CALL delegate_stop()

    ! allocation memoire
    NULLIFY(NbVois)
    ALLOCATE(NbVois(1: Npoint) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nbvois", SIZE(NbVois))
    END IF
    NbVois(1: Npoint)   = 0

    DO is = 1, Npoint
       IF ( Mesh%LogPer(is) /= is ) THEN
          CYCLE
       END IF
       PtCell => Voisins(is)%suiv
       ! on prend tous les voisins du sommet is
       DO
          IF (.NOT. ASSOCIATED(PtCell)) THEN
             EXIT
          END IF
          NbVois(is)    = NbVois(is) + 1
          ! on pointe sur le voisin suivant
          PtCell => PtCell%suiv
       END DO
    END DO

    NULLIFY(Mesh%NbVois)
    Mesh%NbVois  => NbVois(1: Npoint)
    Mat%NCoefMat      =  SUM( NbVois )
    NCoefMat          =  Mat%NCoefMat

    NULLIFY(Mat%JvCell)
    ALLOCATE(Mat%JvCell(1: NCoefMat))
    ALLOCATE(Mat%JPosi(1: Npoint + 1))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mat%jvcell", SIZE(Mat%JvCell))
       CALL alloc_allocate(mod_name, "mat%jposi", SIZE(Mat%JPosi))
    END IF


    k = 1
    DO is = 1, Npoint
       IF ( Mesh%LogPer(is) /= is ) THEN
          CYCLE
       END IF
       PtCell => Voisins(is)%suiv
       DO iv = 1, NbVois(is)
          Mat%JvCell(k) = PtCell%val
          k = k + 1
          PtCell => PtCell%suiv
       END DO
    END DO

    ! Nettoyage memoire !!
    DO is = 1, Npoint
       IF ( Mesh%LogPer(is) /= is ) THEN
          CYCLE
       END IF
       PtCellPred => Voisins(is)%suiv
       PtCell => PtCellPred%suiv
       DO
          DEALLOCATE(PtCellPred)
          IF (see_alloc) THEN
             CALL alloc_deallocate(".dummy.", "ptcellpred")
          END IF
          IF (.NOT.ASSOCIATED(PtCell)) THEN
             EXIT
          END IF
          PtCellPred => PtCell
          PtCell => PtCell%suiv
       END DO
    END DO

#ifdef DEBUG
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Construction du tableau JPosi"
    END IF
#endif

    ! Calcul de la matrice logique, priv�e de sa diagonale

    Mat%JPosi(1) = 1 ! D�but des coeffs non nuls de la ligne is de la matrice pleine dans le stockage ligne
    DO is = 2, Npoint + 1
       Mat%JPosi(is) = Mat%JPosi(is -1)  + Mesh%NbVois(is -1)
    END DO

#ifdef DEBUG
    DO is = 2, Npoint + 1
       DO jv =  Mat%JPosi(is -1), Mat%JPosi(is) -2
          IF ( Mat%JvCell(jv) >=  Mat%JvCell(jv + 1) ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!! Mat%JvCell(jv) >=  Mat%JvCell(jv+1)"
             CALL delegate_stop()
          END IF
          IF ( Mat%JvCell(jv) == is -1 ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!! Mat%JvCell(jv) == is"
             CALL delegate_stop()
          END IF
       END DO
    END DO
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " OK OK OK !!!!!!!"
    END IF
#endif


#define INFO
#ifdef INFO
    ! EVALUATION SUR LE PROFIL DE LA MATRICE
    ALLOCATE(prof(1: 2, Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "prof", SIZE(prof))
    END IF
    DO is = 1, Npoint
       IF ( Mesh%LogPer(is) /= is ) THEN
          CYCLE
       END IF
       prof(:, is) = 0.0
       DO jv =  Mat%JPosi(is), Mat%JPosi(is + 1) -1
          IF (Mat%JvCell(jv) > is) THEN
             CYCLE ! On ne consid�re que la matrice triangulaire inf�rieure
          END IF
          Dist = is - Mat%JvCell(jv)
          prof(1, is) = MAX(prof(1, is), Dist)
          prof(2, is) = prof(2, is) + Dist
       END DO
       prof(2, is) = prof(2, is) / Mesh%NbVois(is)
    END DO
    PRINT *, mod_name, " EVALUATION DU PROFIL DE LA MATRICE:"
    PRINT *, '  --> Ecart maximal       =', MAXVAL(prof(1, : ))
    PRINT *, '  --> Ecart moyen maximal =', MAXVAL(prof(2, : ))
    PRINT *, '  --> Ecart moyen         =', SUM(prof(2, : )) / Npoint
    DEALLOCATE(prof)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "prof")
    END IF
#endif


  END SUBROUTINE GraphMatVertexCenteredPer


  !> \author     Boniface Nkonga      Universite de Bordeaux 1
  !!
  !! Objet : Entree: matrice logique (points voisins) sans la diagonale
  !!         Sortie: matrice logique avec la diagonale.
  !!         Format CSR, avec conditions aux limites periodiques.
  SUBROUTINE GraphMatAddDiagPer( DATA, Mesh, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GraphMatAddDiagPer"
    ! ***************
    !   declarations
    ! ***************
    ! variables d'appel
    TYPE(Matrice), INTENT(IN OUT)                :: Mat !-- CCE R.B. 2008/12/10 Mat%Jposi est IN
    TYPE(MeshDef), INTENT(INOUT)             :: Mesh
    TYPE(Donnees), INTENT(IN)                 :: DATA

    ! variables locales
    INTEGER                          :: Npoint, Deb,  Fin
    INTEGER                          :: is, js, iv, jv
    INTEGER                          :: is1, is2, k
    INTEGER,  DIMENSION(: ), ALLOCATABLE           :: JvCell
    LOGICAL                          :: diag
    INTEGER, DIMENSION(1: Mesh%Npoint)      :: NPtv

    Npoint = SIZE(Mat%JPosi) -1

    ALLOCATE(JvCell(1: Mat%NCoefMat))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "jvcell", SIZE(JvCell))
    END IF
    JvCell = Mat%JvCell
    DEALLOCATE(Mat%JvCell)
    IF (see_alloc) THEN
       CALL alloc_deallocate(".global.", "mat%jvcell")
    END IF
    Mat%NCoefMat = Mat%NCoefMat + Npoint
    ALLOCATE(Mat%JvCell(1: Mat%NCoefMat))
    ALLOCATE(Mat%IDiag(1: Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mat%jvcell", SIZE(Mat%JvCell))
       CALL alloc_allocate(mod_name, "mat%idiag", SIZE(Mat%IDiag))
    END IF

    iv = 1
    DO is = 1, Npoint  ! Boucles sur les lignes de MAt
       IF ( Mesh%LogPer(is) /= is ) THEN
          Mat%JvCell(iv) = is
          Mat%IDiag(is)  = iv
          iv = iv + 1
          Mat%JPosi(is) = Mat%JPosi(is) + is -1
          CYCLE
       END IF
       ! Parcours de la ligne is
       Deb = Mat%JPosi(is)
       Fin = Mat%JPosi(is + 1) -1
       diag = .FALSE.
       DO js = Deb, Fin
          jv = JvCell(js)
          IF (jv < is .AND. js /= Fin) THEN ! DIAG = .F.
             Mat%JvCell(iv) = jv
             iv = iv + 1
          ELSE IF ( diag ) THEN ! js > is et DIAG =.T.
             Mat%JvCell(iv) = jv
             iv = iv + 1
          ELSE IF (jv > is) THEN
             diag = .TRUE.
             Mat%JvCell(iv) = is
             Mat%JvCell(iv + 1) = jv
             Mat%IDiag(is) = iv
             iv = iv + 2
          ELSE IF (js == Fin) THEN
             diag = .TRUE.
             Mat%JvCell(iv) = jv
             Mat%JvCell(iv + 1) = is
             Mat%IDiag(is) = iv + 1
             iv = iv + 2
          END IF
       END DO
       Mat%JPosi(is) = Mat%JPosi(is) + is -1
    END DO

    Mat%JPosi(Npoint + 1) = Mat%NCoefMat + 1
    DEALLOCATE(JvCell)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "jvcell")
    END IF

    DO is = 1, Npoint
       IF ( Mesh%LogPer(is) /= is ) THEN
          CYCLE
       END IF
       DO jv =  Mat%JPosi(is), Mat%JPosi(is + 1) -2
          IF ( Mat%JvCell(jv) >=  Mat%JvCell(jv + 1) ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !! Mat%JvCell(jv) >  Mat%JvCell(jv+1)"
             WRITE(6, *) is, iv, Mat%JvCell(jv), Mat%JvCell(jv + 1)
             CALL delegate_stop()
          END IF
       END DO
    END DO
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " OK OK OK !!!!!!!"
    END IF



    ! IvPos(jposi(is2):jposi(is2+1)-1) :
    ! place dans le stockage ligne  de  la colonne (:, is2),
    NPtv(1: Npoint) = Mat%JPosi(1: Npoint)

    NULLIFY(Mat%IvPos)
    ALLOCATE(Mat%IvPos(1: Mat%NCoefMat))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mat%ivpos", SIZE(Mat%IvPos))
    END IF

    DO  is1 = 1, Npoint
       boucle_k : DO k = Mat%JPosi(is1), Mat%JPosi(is1 + 1) -1
          is2  = Mat%JvCell(k)
          IF (is1 > is2) THEN
             Mat%IvPos(NPtv(is2)) = NPtv(is1)
             Mat%IvPos(NPtv(is1)) = NPtv(is2)
             NPtv(is2) = NPtv(is2) + 1
             NPtv(is1) = NPtv(is1) + 1
          ELSE IF (is1 == is2) THEN
             Mat%IvPos(NPtv(is1)) = NPtv(is1)
             NPtv(is1) = NPtv(is1) + 1
          END IF
       END DO boucle_k
    END DO

    DO is = 1, Npoint
       IF ( Mesh%LogPer(is) /= is ) THEN
          CYCLE
       END IF
       IF ( NPtv(is) /=  Mat%JPosi(is + 1) ) THEN
          PRINT *, is, NPtv(is), Mat%JPosi(is + 1)
          WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!! PtV(is) /=  Mat%JPosi(is+1)"
          CALL delegate_stop()
       END IF
    END DO

    Mat%NNtot = Npoint

    toto: DO is1 = 1, Npoint
       IF ( Mesh%LogPer(is1) /= is1 ) THEN
          CYCLE
       END IF
       Deb = Mat%JPosi(is1)
       Fin = Mat%JPosi(is1 + 1) -1
       DO k = Deb, Fin
          is2 = Mat%JvCell(k)
          IF (is1 == is2) THEN
             IF (k - Mat%IDiag(is1) /= 0) THEN
                PRINT *, mod_name, " ERREUR : Pb dans Mat%IDiag"
                CALL delegate_stop()
             END IF
             CYCLE toto
          END IF
       END DO
       PRINT *, mod_name, " ERREUR : Terme diag pas trouve: ", is1
       CALL delegate_stop()
    END DO toto
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " OK OK OK !!!!!!! GraphMatAddDiagPer"
    END IF

  END SUBROUTINE GraphMatAddDiagPer







  !> \author Mikael PAPIN/modif Nkonga
  !!
  !! Objet : Entree: matrice logique (points voisins) avec la diagonale
  !!         Sortie: matrice logique + allocation des termes non nuls sous forme de blocs matriciels
  !!         Format CSR
  !! Allocation de Mat%vals
  SUBROUTINE GraphMatAllocTab(blksize, Mat)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GraphMatAllocTab"
    TYPE(Matrice), INTENT(IN OUT)                :: Mat !-- CCE JPosi est IN (ainsi que bien d'autres cps sans doute)
    INTEGER,       INTENT(IN)                 :: blksize
    INTEGER :: istat
    ALLOCATE(Mat%Vals(1: blksize, 1: blksize, 1: Mat%NCoefMat), STAT = istat )
    IF (istat /= 0) THEN
       PRINT *, mod_name, " ERREUR : la matrice ne peut etre allouee", blksize, Mat%NCoefMat
       CALL delegate_stop()
    END IF
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mat%vals", SIZE(Mat%Vals))
    END IF
    Mat%Vals(1, 1, 1) = 0.0 !-- pour faire taire l'analyse de flot
  END SUBROUTINE GraphMatAllocTab


  !-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !> \brief Recherche des segments du maillage
  !! une m�thode qui ressemble � SegmentsSinusFr2D de loin; mais l� on part des structures (mesh)
  SUBROUTINE GeomSegments2d( Mesh, Impre)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GeomSegments2d"
    ! ****************
    !   declarations
    ! ***************
    ! variables d'appel
    TYPE(MeshDef), INTENT(INOUT)              :: Mesh
    INTEGER,       INTENT(IN)                 :: Impre

    ! variables locales
    INTEGER                          :: Npoint, Nelemt, NFacFr
    INTEGER                          :: Info
    INTEGER                          :: is, js, iv, jt, k, is1, is2
    INTEGER                          :: is_min, is_max, jseg
    INTEGER                          :: next, noth, ntyp

    INTEGER, DIMENSION(: ),   POINTER                :: NewLogfac
    INTEGER, DIMENSION(: ),   POINTER, SAVE          :: NumFac !-- CCE CCE
    INTEGER, DIMENSION(:, : ), POINTER                :: NewNsfacFr, NsfacFr
    INTEGER, DIMENSION(: ),   POINTER                :: Logfac
    TYPE(cellule), POINTER                          :: NewCell, PtCell, PtCellPred
    TYPE(cellule), DIMENSION(: ), POINTER            :: hashtable
    INTEGER, DIMENSION(: ), ALLOCATABLE, SAVE :: NSupVois
    REAL, DIMENSION(:, : ), ALLOCATABLE, SAVE         :: Coor
    LOGICAL, SAVE                                   :: initialise = .FALSE.

    ! ***************************
    !   initialisations
    ! ***************************


    Npoint = Mesh%Npoint

    WRITE(*,*) mod_name, ' Npoint ', Npoint, ' NCells = ', mesh%NCells 

    IF (.NOT. initialise) THEN

#ifndef MOULINETTE
       initialise = .TRUE.
#else
       IF (ALLOCATED(NSupVois)) THEN
          DEALLOCATE(NSupVois)
       END IF
       IF (ALLOCATED(Coor)) THEN
          DEALLOCATE(Coor)
       END IF
#endif

       ALLOCATE(NSupVois(SIZE(Mesh%Coor, dim = 2)), STAT = Info)
       IF (Info /= 0) THEN
          WRITE(6, *) mod_name, " ERREUR : Apres Allocation NSupVois, Info =", Info
          CALL delegate_stop()
       END IF

       ALLOCATE(Coor(Mesh%Ndim, Mesh%Npoint), STAT = Info)
       IF (Info /= 0) THEN
          WRITE(6, *) mod_name, " ERREUR : Apres Allocation Coor, Info =", Info
          CALL delegate_stop()
       END IF

    END IF

    Nelemt = Mesh%Nelemt
    NFacFr = Mesh%NFacFr

    IF ( NFacFr > 0 ) THEN
       NULLIFY( NsfacFr, Logfac )
       NsfacFr => Mesh%NsfacFr !Mesh%NsfacFr(1: 2, 1: NFacFr)
       Logfac  => Mesh%LogFr   !Mesh%LogFr(1: NFacFr)
    END IF

    WRITE(*,*) " Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr", Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr 

    ALLOCATE(hashtable(1: Npoint), STAT = Info)
    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Apres Allocation Hashtable, Info =", Info
    END IF

    IF (Info /= 0) THEN
       WRITE(*,*) mod_name, " ERREUR : Apres Allocation Hashtable, Info =", Info
       CALL delegate_stop()
    END IF

    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "hashtable", SIZE(hashtable))
    END IF

    DO is = 1, Npoint
       hashtable(is)%val = is
       NULLIFY(hashtable(is)%suiv)
    END DO

    NSupVois(1:Npoint) = 0
    Mesh%Nsegmt = 0

    ! OK OK OK
    ! *******************************************
    !   construction de hashtable, NSupVois, Nsegmt
    ! *******************************************

    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " size(NSupVois) == ", SIZE(NSupVois)
       WRITE(6, *) mod_name, " Construction de la table des segments hashtable ..."
    END IF

    ! construction de la table des segments

    NULLIFY(PtCell, PtCellPred )

    DO jt = 1, Nelemt

#ifdef ORDRE_ELEVE
       ntyp = Mesh%Nsommets(jt)
#else
       ntyp = Mesh%Ndegre(jt)
#endif

       DO k = 1, ntyp

          next = MOD(k, ntyp) + 1
          is1  = Mesh%Nu(   k, jt)
          is2  = Mesh%Nu(next, jt)

          is_min = MIN(is1, is2)
          is_max = MAX(is1, is2)

          ! recherche de la position d'insertion de is_max dans
          ! le tableau hashtable(is_min). on utilise une
          ! recherche sequentielle simple.
          ! initialisation des pointeurs
          PtCell     => hashtable(is_min)%suiv
          PtCellPred => hashtable(is_min)

          ! on recherche la place du nouveau voisin
          DO
             IF (.NOT. ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             IF (PtCell%val > is_max) THEN
                EXIT
             END IF
             ! on a trouve la place, on arrete
             PtCellPred => PtCell
             PtCell     => PtCell%suiv
          END DO

          ! on teste si le sommet n'est pas deja la
          IF (PtCellPred%val < is_max) THEN
             ! creation de la nouvelle cellule
             ALLOCATE(NewCell)
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "newcell.A", 1)
             END IF
             NewCell%val  = is_max

             ! insertion de la nouvelle cellule
             NewCell%suiv    => PtCell
             PtCellPred%suiv => NewCell

             ! on a rajoute un voisin, donc on incremente
             ! NSupVois(is_min) et Nsegmt
             NSupVois(is_min) = NSupVois(is_min) + 1
             Mesh%Nsegmt = Mesh%Nsegmt + 1 !-- 2D

          END IF
       END DO

    END DO

    ! le tableau temporaire hashtable est constitue,
    ! on le recopie dans le tableau Nubo
    ! CALL delegate_stop()

    ! ****************************
    !   creation du tableau Nubo
    ! ****************************

    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Remplissage du tableau Nubo ... Nsegmt = ", Mesh%Nsegmt
    END IF

    ! allocation memoire
    ALLOCATE(Mesh%Nubo(2, Mesh%Nsegmt))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%nubo", SIZE(Mesh%Nubo))
    END IF

    ! compteur pour les segments frontieres
    next = 0

    ! allocation memoire pour le tableau NewLogfac = LogFac trie
    IF ( NFacFr > 0 ) THEN
       NULLIFY(NewLogfac, NewNsfacFr, NumFac )
       ALLOCATE(NewLogfac(1: NFacFr), NewNsfacFr(1: 2, 1: NFacFr), NumFac(1: NFacFr) )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "newlogfac", SIZE(NewLogfac))
          CALL alloc_allocate(mod_name, "newnsfacfr", SIZE(NewNsfacFr))
          CALL alloc_allocate(mod_name, "numfac", SIZE(NumFac))
       END IF
    END IF
    !OK OK OK

    k = 0
    DO is = 1, Npoint
       ! si le sommet is a un(des) voisin(s)
       IF (NSupVois(is) /= 0) THEN
          PtCell => hashtable(is)%suiv
          ! on prend tous les voisins du sommet is
          DO iv = 1, NSupVois(is)
             js            = PtCell%val
             k             = k + 1
             IF ( NFacFr > 0 ) THEN

                IF (SegmentFr(is, js, NFacFr, NsfacFr, jseg)) THEN
                   next                 = next + 1
                   NewNsfacFr(1: 2, next) = NsfacFr(1: 2, jseg)
                   NumFac(next)         = k
                   NewLogfac(next)      = Mesh%LogFr(jseg)
                END IF
             END IF
             Mesh%Nubo(1, k) = is
             Mesh%Nubo(2, k) = PtCell%val
             ! on pointe sur le voisin suivant
             PtCell => PtCell%suiv
          END DO
       END IF
    END DO

    ! Mesh%LogFr     : OK
    ! NewLogfac       : OK
    ! ici, next doit etre egal a NFacFr
    ! et noth doit etre egal a Nsegmt
    IF ( next /= NFacFr ) THEN
       WRITE(6, *) mod_name, " Probleme !!!!!  !!!!!NFacFr_trouve= ", next
       WRITE(6, *) mod_name, " ERREUR : Probleme !!!!!  !!!!!NFacFr_real  = ", NFacFr
#ifndef MOULINETTE
       CALL delegate_stop()
#endif
    ELSE
       IF ( Impre > 2 ) THEN
          WRITE(6, *) mod_name, " Ok OK OK !!!!!  NFacFr_trouve= ", next
       END IF
       IF ( Impre > 2 ) THEN
          WRITE(6, *) mod_name, " Ok OK OK !!!!!  NFacFr_real  = ", NFacFr
       END IF
    END IF

    !maintenant que la numerotation dans Nubo est correcte,
    !on construit les tableaux Nuseg, Nusv, Nutv

    ! ***************************************
    !   construction de Nuseg, Nusv et Nutv
    ! ***************************************

    IF ( Impre > 2 ) THEN
       WRITE(6, *)
       WRITE(6, *) mod_name, " Stockage des tableaux Nuseg, Nusv et Nutv .."
    END IF

    ! initialisation des tableaux Nuseg, Nusv et Nutv
    ALLOCATE(Mesh%Nuseg(1: 4, 1: Nelemt))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%nuseg", SIZE(Mesh%Nuseg))
    END IF
    Mesh%Nuseg(1: 4, 1: Nelemt) = 0
    ALLOCATE(Mesh%Nusv(1: 2, 1: Mesh%Nsegmt))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%nusv", SIZE(Mesh%Nusv))
    END IF
    Mesh%Nusv(1: 2, 1: Mesh%Nsegmt) = 0
    ALLOCATE(Mesh%Nutv(1: 2, 1: Mesh%Nsegmt))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%nutv", SIZE(Mesh%Nutv))
    END IF
    Mesh%Nutv(1: 2, 1: Mesh%Nsegmt) = 0
    ! OK OK OK

    DO jt = 1, Nelemt
#ifdef ORDRE_ELEVE
       ntyp = Mesh%Nsommets(jt)
#else
       ntyp = Mesh%Ndegre(jt)
#endif
       DO k = 1, ntyp
          next = MOD(   k, ntyp) + 1
          noth = MOD(next, ntyp) + 1
          is1 = Mesh%Nu(   k, jt)
          is2 = Mesh%Nu(next, jt)
          jseg = WhichSeg(1, Mesh%Nsegmt, Mesh%Nubo, is1, is2)
          IF ( jseg == 0 ) THEN
             CYCLE
          END IF
          Mesh%Nuseg(k, jt) = jseg
          IF (Mesh%Nutv(1, Mesh%Nuseg(k, jt)) == 0) THEN
             Mesh%Nutv(1, Mesh%Nuseg(k, jt)) = jt
             Mesh%Nusv(1, Mesh%Nuseg(k, jt)) = Mesh%Nu(noth, jt)
          ELSE
             IF (Mesh%Nutv(2, Mesh%Nuseg(k, jt)) == 0) THEN
                Mesh%Nutv(2, Mesh%Nuseg(k, jt)) = jt
                Mesh%Nusv(2, Mesh%Nuseg(k, jt)) = Mesh%Nu(noth, jt)
             ELSE
                WRITE(6, *) mod_name, " ERREUR : Probleme dans la construction des Nutv"
                CALL delegate_stop()
             END IF
          END IF
       END DO
    END DO

    ! Nettoyage memoire !!
    DO is = 1, Npoint
       IF (NSupVois(is) /= 0) THEN
          PtCellPred => hashtable(is)%suiv
          PtCell => PtCellPred%suiv
          DO
             DEALLOCATE(PtCellPred)
             IF (see_alloc) THEN
                CALL alloc_deallocate(".dummy.", "ptcellpred.A")
             END IF
             IF (.NOT.ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             PtCellPred => PtCell
             PtCell => PtCell%suiv
          END DO
       END IF
    END DO

    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Stockage des tableaux Nuseg, Nusv et Nutv ..."
    END IF

#ifdef SEQUENTIEL

    ! for HO(>=3) meshes there is no need to
    ! re-construct Logfac, NsfacFr, NumFac
    IF(NFacFr > 0 .AND. Mesh%Nordre < 3) THEN

       Mesh%LogFr(1: NFacFr) = NewLogfac(1: NFacFr)
       DEALLOCATE(NewLogfac)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "newlogfac")
       END IF

       Mesh%NsfacFr(1: 2, 1: NFacFr) =  NewNsfacFr(1: 2, 1: NFacFr)
       DEALLOCATE(NewNsfacFr)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "newnsfacfr")
       END IF

       Mesh%NumFac => NumFac(1: NFacFr)

    ELSE IF(NFacFr > 0 .AND. Mesh%Nordre >= 3) THEN

#ifdef MOULINETTE

       ! For Post-pro only
       Mesh%LogFr(1: NFacFr) = NewLogfac(1: NFacFr)
       Mesh%NsfacFr(1: 2, 1: NFacFr) =  NewNsfacFr(1: 2, 1: NFacFr)
       Mesh%NumFac => NumFac(1: NFacFr)

#endif

       DEALLOCATE(NewLogfac)       
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "newlogfac")
       END IF

       DEALLOCATE(NewNsfacFr)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "newnsfacfr")
       END IF

    END IF

#else

    ! In parallel HO mehes are always treated as 
    ! linear meshes for the preliminary stuff
    IF(NFacFr > 0 ) THEN

       Mesh%LogFr(1: NFacFr) = NewLogfac(1: NFacFr)
       DEALLOCATE(NewLogfac)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "newlogfac")
       END IF

       Mesh%NsfacFr(1: 2, 1: NFacFr) =  NewNsfacFr(1: 2, 1: NFacFr)
       DEALLOCATE(NewNsfacFr)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "newnsfacfr")
       END IF

       Mesh%NumFac => NumFac(1: NFacFr)

       PRINT *, mod_name, " HWA NumFac initialise/1"

    ENDIF

#endif

    WRITE(6, *) mod_name, " Sortie de la procedure"

  CONTAINS

    !> \brief Recherche d'un num�ro de segment 2D �tant donn� les num�ros des 2 sommets
    !! Fonction � double r�sultat (donc effet de bord %%%%) :: trouv� et le num�ro de segment
    FUNCTION SegmentFr(is1, is2, NFacFr, NsfacFr, jseg) RESULT(is_seg_fr)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "SegmentFr"
      ! variables d'appel
      INTEGER, INTENT(IN)          :: is1, is2, NFacFr
      INTEGER, INTENT(OUT)         :: jseg !-- %%%%%%%%%%%%%%%%%%%%%%%%%%%% Argument OUT d'une fonction
      INTEGER, DIMENSION(:, : ), POINTER :: NsfacFr
      LOGICAL :: is_seg_fr
      ! variables locales
      INTEGER              :: k, is_min, is_max

      is_min = MIN(is1, is2)
      is_max = MAX(is1, is2)
      jseg = 0

      DO k = 1, NFacFr
         !IF ((NsfacFr(1, k) == is_min) .AND. (NsfacFr(2, k) == is_max)) THEN ! dante ---
         IF( ( NsfacFr(1, k) == is1 .AND.NsfacFr(2, k) == is2 ) .OR. &
             ( NsfacFr(2, k) == is1 .AND.NsfacFr(1, k) == is2 ) ) THEN
            jseg = k
            EXIT
         END IF
      END DO

      is_seg_fr = jseg /=0

    END FUNCTION SegmentFr

    !> \brief Recherche dichotomique d'un segment entre 2 sommets, la recherche ayant lieu dans le tableau nubo
    !! Ensuite recherche dichotomique croissante ou d�croissante du 2�me sommet (on parcourt les segments qui bordent l'�l�ment)
    FUNCTION WhichSeg(idebut, ifin, Nubo, is1, is2) RESULT(i_seg)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "WhichSeg"

      ! variables d'appel
      INTEGER, INTENT(IN)              :: is1, is2, idebut, ifin
      INTEGER, DIMENSION(:, : ), POINTER :: Nubo
      INTEGER :: i_seg

      ! variables locales
      INTEGER              :: jseg2,  debut, Fin, incr, is_min, is_max 

      ! initialisation des variables
      i_seg = -2000000000 !-- pour faire taire l'analyse de flot
      is_min = MIN(is1, is2)
      is_max = MAX(is1, is2)
      debut = idebut
      Fin   = ifin
      jseg  = 0
      !-- WhichSeg = 0 ici ???? %%%%
      IF ( ifin == 0 ) THEN
         RETURN
      END IF

      ! recherche du premier point is1 dans Nubo
      DO
         jseg2 = jseg
         jseg  = (debut + Fin) / 2

         ! fin de la recherche, on sort
         IF ((Nubo(1, jseg) == is_min) .OR. (jseg == jseg2)) THEN
            EXIT
         END IF

         IF (Nubo(1, jseg) < is_min) THEN
            debut = jseg + 1
         ELSE
            Fin = jseg
         END IF

      END DO

      ! si is_min est dans Nubo, on cherche is_max
      IF (Nubo(1, jseg) == is_min) THEN

         ! ruse pour forcer jseg a diminuer ou augmenter
         IF (Nubo(2, jseg) > is_max) THEN
            incr = - 1
         ELSE
            incr = 1
         END IF

         ! recherche de is_max
         DO
            IF (  Nubo(2, jseg) == is_max ) THEN
               i_seg = jseg
               EXIT
            END IF

            IF (  Nubo(1, jseg) /= is_min ) THEN
               i_seg = 0
               PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/1"
               CALL delegate_stop()
            END IF

            jseg = jseg + incr
            IF ( jseg < idebut .OR. jseg > ifin ) THEN
               i_seg = 0
               PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/2"
               CALL delegate_stop()
            END IF
         END DO
      ELSE
         i_seg = 0
         PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/3"
         CALL delegate_stop()
      END IF

    END FUNCTION WhichSeg

  END SUBROUTINE GeomSegments2d



  !> \author     Boniface Nkonga      Universite de Bordeaux 1
  !!
  !! \brief construction de la liste des segments et d'autres tableaux utiles
  !!
  !! Calcul sur des �l�ments cubiques (ntyp=6, 6 sommets par �l�ment)
  !!
  !! une m�thode qui ressemble � SegmentsSinusFr2D de loin; on travaille sur mesh% cette fois ci, via des alias par POINTER
  SUBROUTINE GeomSegments3d(DATA, Mesh )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GeomSegments3d"
    !
    !==============================================================
    ! Ndegre(jt) ::  type de l'element
    ! Nu(k, jt)   ::  numero du Keme point de l'element jt
    ! Nubo(k, js) ::  numero du Keme point du segment js
    ! Nsfac(k, is)::  numero du Keme point du segment frontiere is
    ! Logfac(k)  ::  contrainte du segment k
    !==============================================================
    !
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(Donnees), INTENT(IN)    :: DATA
    TYPE(MeshDef), INTENT(INOUT) :: Mesh

    ! Variables Locales Scalaires
    ! -------------------------------------
    INTEGER :: Npoint, Nelemt, Nordre
    INTEGER :: Nsegmt
    INTEGER :: is, js, iv, jt, k, is1, is2
    INTEGER :: is_min, is_max
    INTEGER :: next, N_seg, i1, i2, N_seg_MAX
    INTEGER :: jseg, nnz

    ! Variables Locales Tableaux et pointeurs
    ! -------------------------------------

    INTEGER, DIMENSION(: ), POINTER :: n1, n2

    INTEGER, SAVE, DIMENSION(6),  TARGET :: nTetra1 = (/ 1, 2, 1, 1, 2, 3 /)
    INTEGER, SAVE, DIMENSION(6),  TARGET :: nTetra2 = (/ 2, 3, 3, 4, 4, 4 /)
    INTEGER, SAVE, DIMENSION(12), TARGET :: nHexa1  = (/ 1, 1, 1, 3, 2, 3, 3, 4, 6, 5, 6, 8 /)
    INTEGER, SAVE, DIMENSION(12), TARGET :: nHexa2  = (/ 2, 4, 5, 2, 6, 4, 7, 8, 5, 8, 7, 7 /)
    INTEGER, SAVE, DIMENSION(8),  TARGET :: nPyram1 = (/ 1, 2, 3, 4, 1, 2, 3, 4 /)
    INTEGER, SAVE, DIMENSION(8),  TARGET :: nPyram2 = (/ 2, 3, 4, 1, 5, 5, 5, 5 /)

    TYPE(cellule), DIMENSION(:),   ALLOCATABLE, TARGET :: hashtable
    INTEGER,       DIMENSION(:),   POINTER             :: Ndegre
    INTEGER,       DIMENSION(:,:), POINTER             :: Nu, Nsfac
    INTEGER,       DIMENSION(:,:), POINTER             :: Nubo !-- DOIT �TRE UN POINTER ! CCE CCE
    INTEGER,       DIMENSION(:),   POINTER             :: NbVois !-- DOIT �TRE UN POINTER, 
    INTEGER,       DIMENSION(: ),  POINTER             :: NSupVois, Logfac
    TYPE(cellule),                 POINTER             :: NewCell, PtCell, PtCellPred

!!$    LOGICAL, SAVE :: firstpass = .TRUE.
!!$
!!$    IF (firstpass) THEN
!!$       firstpass = .FALSE.
!!$    ELSE
!!$       DEALLOCATE(Nubo)
!!$       DEALLOCATE(NbVois)
!!$    END IF

    IF( ASSOCIATED(NuBo) )   NULLIFY(NuBo)
    IF( ASSOCIATED(NbVois) ) NULLIFY(NbVois)

    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
    NULLIFY( Ndegre, Nsfac, Nu, Logfac)

    Nordre   = Data%Nordre
    Npoint   = Mesh%Npoint
    Nelemt   = Mesh%Nelemt
    Ndegre  => Mesh%Ndegre
    Nu      => Mesh%Nu
    Logfac  => Mesh%LogFr

    IF (Data%Impre > 0) THEN
       WRITE(6, *) "Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr", Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr 
    END IF

    ALLOCATE(hashtable(1: Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "hashtable", SIZE(hashtable))
    END IF

    DO is = 1, Npoint
       hashtable(is)%val = is
       NULLIFY(hashtable(is)%suiv)
    END DO

    ALLOCATE(NSupVois(Npoint) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nsupvois", SIZE(NSupVois))
    END IF
    NSupVois(: ) = 0

    ! *******************************************
    !   construction de hashtable, NSupVois, Nsegmt
    ! *******************************************

    IF (Data%Impre > 0) THEN
       WRITE(6, *)
       PRINT *, mod_name, ' Construction de la table des segments hashtable ...'
    END IF

    NSupVois = 0

    N_seg_MAX = 0

    DO jt = 1, Nelemt
       
       nnz = COUNT( NU(:, jt) /= 0)

       SELECT CASE( nnz )

       CASE(4, 10) ! Thetrahedron

          n1 => NTetra1; n2 => NTetra2
          N_seg = 6

       CASE(8, 27) ! Hexahedron
          
          n1 => NHexa1; n2 => NHexa2
          N_seg = 12

       CASE(5, 14) ! Pyramid

          n1 => NPyram1; n2 => NPyram2
          N_seg = 8

       CASE DEFAULT

          WRITE(*,*) 'ERROR: unknown element with ', nnz, ' DOFs'
          STOP
             
       END SELECT

       N_seg_MAX = MAX(N_seg_MAX, N_seg)

       DO k = 1, N_seg

          i1 = n1(k)
          i2 = n2(k)

          is1 = Nu(i1, jt)
          is2 = Nu(i2, jt)

          is_min = MIN(is1, is2)
          is_max = MAX(is1, is2)

          PtCell     => hashtable(is_min)%suiv
          PtCellPred => hashtable(is_min)

          ! on recherche la place du nouveau voisin
          DO
             IF (.NOT. ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             IF (PtCell%val > is_max) THEN
                EXIT
             END IF

             PtCellPred => PtCell
             PtCell     => PtCell%suiv
          END DO

          ! on teste si le sommet n'est pas deja la
          IF (PtCellPred%val < is_max) THEN
             ALLOCATE(NewCell)
             NULLIFY(NewCell%suiv)
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "newcell.Z", 1)
             END IF
             NewCell%val     = is_max
             NewCell%suiv    => PtCell
             PtCellPred%suiv => NewCell

             NSupVois(is_min)  = NSupVois(is_min) + 1

          END IF
       END DO
    
       NULLIFY( n1, n2 )
  
    END DO

    ! le tableau temporaire hashtable est constitu�,
    ! on le recopie dans le tableau Nubo

    ! ****************************
    !   creation du tableau Nubo
    ! ****************************

    Nsegmt = SUM(NSupVois) !-- 3D
    IF (Data%Impre > 0) THEN
       WRITE(6, *)
       PRINT *, mod_name, ' Remplissage du tableau Nubo ... Nsegmt = ', Nsegmt
    END IF

    ! allocation memoire
    ALLOCATE(Nubo(2, Nsegmt), NbVois(Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nubo", SIZE(Nubo))
       CALL alloc_allocate(mod_name, "nbvois", SIZE(NbVois))
    END IF

    NuBo = 0;  NbVois = 0

    next      = 0
    DO is = 1, Npoint

       IF (NSupVois(is) /= 0) THEN
          PtCell => hashtable(is)%suiv

          DO iv = 1, NSupVois(is)
             next         = next + 1
             js           = PtCell%val
             Nubo(1, next) = is
             Nubo(2, next) = js

             PtCell       => PtCell%suiv
             NbVois(is)    = NbVois(is) + 1
             NbVois(js)    = NbVois(js) + 1
          END DO

       END IF

    END DO


    ! ici, next doit �tre �gal � Nsegmt
    IF ( next /= Nsegmt ) THEN
       WRITE(6, *) mod_name, '  Probleme !!!!!  !!!!!Nsegmt_trouve= ', next
       WRITE(6, *) mod_name, '  Probleme !!!!!  !!!!!Nsegmt_real  = ', Nsegmt
    ELSE
       IF (Data%Impre > 0) THEN
          WRITE(6, *) mod_name, '  Ok OK OK !!!!!  Nsegmt_trouve= ', next
          WRITE(6, *) mod_name, '  Ok OK OK !!!!!  Nsegmt_real  = ', Nsegmt
       END IF
    END IF

    !maintenant que la numerotation dans Nubo est correcte,
    !on construit les tableaux Nusv
    ! ***************************************
    !   construction de Nuseg, Nusv
    ! *****************
    ALLOCATE(Mesh%Nuseg(N_seg_Max, Nelemt))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%nuseg", SIZE(Mesh%Nuseg))
    END IF
    Mesh%Nuseg = 0

    DO jt = 1, Nelemt

       nnz = COUNT( NU(:, jt) /= 0)

       SELECT CASE( nnz )

       CASE(4, 10) ! Thetrahedron

          n1 => NTetra1; n2 => NTetra2
          N_seg = 6

       CASE(8, 27) ! Hexahedron
          
          n1 => NHexa1; n2 => NHexa2
          N_seg = 12

       CASE(5, 14) ! Pyramid

          n1 => NPyram1; n2 => NPyram2
          N_seg = 8

       CASE DEFAULT

          WRITE(*,*) 'ERROR: unknown element with ', nnz, ' DOFs'
          STOP
             
       END SELECT

       DO k = 1, N_seg

          is1 = Mesh%Nu(n1(k), jt)
          is2 = Mesh%Nu(n2(k), jt)

          CALL WhichSeg(1, Nsegmt, Nubo, is1, is2, jseg)

          IF ( jseg == 0 ) THEN
             PRINT *, mod_name, " ERREUR : jseg == 0, ici... GEOM/GeomGraph.f90 L1566"
             CALL delegate_stop()
          END IF

          Mesh%Nuseg(k, jt) = jseg

       END DO

       NULLIFY( n1, n2 )

    END DO

    ! Nettoyage memoire !!
    DO is = 1, Npoint

       IF (NSupVois(is) /= 0) THEN

          PtCellPred => hashtable(is)%suiv
          PtCell => PtCellPred%suiv

          DO

             DEALLOCATE(PtCellPred)
             IF (see_alloc) THEN
                CALL alloc_deallocate(".dummy.", "ptcellpred.Z")
             END IF

             IF (.NOT.ASSOCIATED(PtCell)) THEN
                EXIT
             END IF

             PtCellPred => PtCell
             PtCell => PtCell%suiv

          END DO

       END IF

    END DO
    DEALLOCATE(hashtable)

    DEALLOCATE(NSupVois)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "hashtable")
       CALL alloc_deallocate(mod_name, "nsupvois")
    END IF
    NULLIFY(NSupVois)


    NULLIFY(Mesh%Nubo, Mesh%NbVois)
    Mesh%Nsegmt  =  Nsegmt
    Mesh%Nubo    => Nubo
    Mesh%NbVois  => NbVois

!    NULLIFY( Nubo, NbVois )

    Mesh%NCoefMatEx = SUM( Mesh%NbVois )
    ALLOCATE(Mesh%JvPnt(Mesh%NCoefMatEx), Mesh%IvPos(Mesh%NCoefMatEx) )
    ALLOCATE(Mesh%JvSeg(Mesh%NCoefMatEx))
    ALLOCATE(Mesh%JPosi(Mesh%Npoint + 1))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%jvpnt", SIZE(Mesh%JvPnt))
       CALL alloc_allocate(mod_name, "mesh%ivpos", SIZE(Mesh%IvPos))
       CALL alloc_allocate(mod_name, "mesh%jvseg", SIZE(Mesh%JvSeg))
       CALL alloc_allocate(mod_name, "mesh%jposi", SIZE(Mesh%JPosi))
    END IF

  CONTAINS
    !> \brief Recherche dichotomique d'un segment entre 2 sommets, la recherche ayant lieu dans le tableau nubo
    !! Ensuite recherche dichotomique croissante ou d�croissante du 2�me sommet (on parcourt les segments qui bordent l'�l�ment)
    SUBROUTINE WhichSeg(idebut, ifin, Nubo, is1, is2, Num_Seg)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "WhichSeg"

      ! variables d'appel
      INTEGER, INTENT(IN)              :: is1, is2, idebut, ifin
      INTEGER, INTENT(OUT)             :: Num_Seg
      INTEGER, DIMENSION(:, : ), POINTER :: Nubo !-- pas de INTENT pour un POINTER en Fortran 90

      ! variables locales
      INTEGER              :: TheSeg, jseg, jseg2,  debut, Fin, incr, is_min, is_max !-- %%%% TheSeg est inutile

      ! initialisation des variables
      is_min = MIN(is1, is2)
      is_max = MAX(is1, is2)
      debut = idebut
      Fin   = ifin
      jseg  = 0

      Num_Seg = 0
      IF ( ifin == 0 ) THEN
         RETURN
      END IF

      ! recherche du premier point is1 dans Nubo
      DO
         jseg2 = jseg
         jseg  = (debut + Fin) / 2

         ! fin de la recherche, on sort
         IF ((Nubo(1, jseg) == is_min) .OR. (jseg == jseg2)) THEN
            TheSeg = jseg
            EXIT
         END IF

         IF (Nubo(1, jseg) < is_min) THEN
            debut = jseg + 1
         ELSE
            Fin = jseg
         END IF

      END DO

      ! si is_min est dans Nubo, on cherche is_max
      IF (Nubo(1, jseg) == is_min) THEN

         ! ruse pour forcer jseg a diminuer ou augmenter
         IF (Nubo(2, jseg) > is_max) THEN
            incr = - 1
         ELSE
            incr = 1
         END IF

         ! recherche de is_max
         DO
            IF (  Nubo(2, jseg) == is_max ) THEN
               Num_Seg = jseg
               EXIT
            END IF

            IF (  Nubo(1, jseg) /= is_min ) THEN
               Num_Seg = 0
               PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/1"
               CALL delegate_stop()
            END IF

            jseg = jseg + incr
            IF ( jseg < idebut .OR. jseg > ifin ) THEN
               Num_Seg =  0
               PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/2"
               CALL delegate_stop()
            END IF
         END DO
      ELSE
         Num_Seg = 0
         PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/3"
         CALL delegate_stop()
      END IF

    END SUBROUTINE WhichSeg

  END SUBROUTINE GeomSegments3d


  !     -----------------------------------------------
  !> \author Boniface Nkonga      Universite de Bordeaux 1
  !!
  !! \brief construction de la liste des facettes
  !!
  !! Cas des tetra�dres (ntyp=4)
  SUBROUTINE GeomFacettes3d(DATA, Mesh)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GeomFacettes3d"
    !
    !
    !==============================================================
    !
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    TYPE(Donnees), INTENT(IN)                :: DATA
    TYPE(MeshDef), INTENT(INOUT)             :: Mesh

    ! Variables Locales Scalaires
    ! -------------------------------------
    INTEGER                   :: Npoint, Nelemt
    INTEGER                   :: NFacFr
    INTEGER                   :: is, iv, jt, k, is1, is2, is3, ifr
    INTEGER                   :: is_min, ismil, is_max
    INTEGER                   :: next, ntyp, i1, i2, i3

    ! Variables Locales Tableaux et pointeurs
    ! -------------------------------------
    INTEGER, DIMENSION(4), PARAMETER :: n1 = (/ 1, 1, 1, 2 /)
    INTEGER, DIMENSION(4), PARAMETER :: n2 = (/ 2, 3, 4, 3 /)
    INTEGER, DIMENSION(4), PARAMETER :: n3 = (/ 3, 4, 2, 4 /)

    TYPE(cellule2d), DIMENSION (: ), ALLOCATABLE, TARGET :: hashtable3d
    TYPE(cellule2d),               POINTER              :: NewCell2d, PtCell2d
    TYPE(cellule2d),               POINTER              :: PtCellPred2d
    INTEGER,       DIMENSION(: ), ALLOCATABLE          :: NSupVois
    INTEGER,       DIMENSION(: ), ALLOCATABLE, TARGET, SAVE  :: NumTetFr !-- CCE CCE
    INTEGER,       DIMENSION(:, : ), POINTER              :: Nu
    INTEGER,       DIMENSION(: ), POINTER, SAVE         :: NewLogfac !-- CCE CCE
    INTEGER,       DIMENSION(: ), ALLOCATABLE, TARGET, SAVE  :: NewLogfacReel !-- CCE CCE
    INTEGER, DIMENSION(:, : ), POINTER, SAVE              :: NewNsfacFr !-- CCE CCE
    INTEGER,       DIMENSION(: ), ALLOCATABLE, TARGET, SAVE    :: NumFac !-- CCE CCE
    LOGICAL :: test
    LOGICAL, PARAMETER :: debug = .FALSE.
    LOGICAL, SAVE      :: firstpass = .TRUE.

    IF (firstpass) THEN
       firstpass = .FALSE.
    ELSE
       DEALLOCATE( NumTetFr )          
       !DEALLOCATE( NewLogfac )          
       IF (ALLOCATED(NewLogfacReel) ) THEN
          DEALLOCATE( NewLogfacReel )          
       END IF
       ! DEALLOCATE( NewNsfacFr )          
       !DEALLOCATE( NsfacFr )          
       IF (ALLOCATED( NumFac) ) THEN
          DEALLOCATE( NumFac )          
       END IF
    END IF
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------

    NULLIFY( NewCell2d, PtCell2d, PtCellPred2d, Nu)

    Npoint   = Mesh%Npoint
    Nelemt   = Mesh%Nelemt
    NFacFr   = Mesh%NFacFr
    Nu       => Mesh%Nu

    IF (Data%Impre > 0) THEN
       WRITE(6, *) "Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr", Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr
    END IF
    ALLOCATE(hashtable3d(1: Npoint)) !-- hashtable3d hache en liste chainee toutes les facettes (triangulaires),
    !-- de nom (numero-sommet-min, numero-sommet-milieu, numero-sommet-max),
    !-- le hachage implicite �tant le num�ro de sommet-min;
    !-- la longueur de la liste chain�e finale NSupVois sera le nombre de sommets
    !-- voisins de sommet entr�e ayant un num�ro plus grand que sommet entr�e
    !-- La 2�me contrainte est que la liste chain�e soit ordonn�e par l'ordre lexicographique
    !-- (Face1 <= Face2) <=> (Face1.ismil < Face2.ismil
    !--                       ou Face1.ismin == Face2.ismil et Face1.ismax < Face2.ismax
    !-- ismil est nomm� is2 dans le Type, ismax est nomm� is3
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "hashtable3d", SIZE(hashtable3d))
    END IF
    DO is = 1, Npoint
       hashtable3d(is)%is2 = is
       hashtable3d(is)%is3 = is !-- les (is, is, is) �tant des noms invalides de facette, les cellule2d de hashtable3d ne seront jamais actives,
       hashtable3d(is)%jt1 = 0 !-- donc en fait cette initialisation est inutile, en sera jamais employ�e
       hashtable3d(is)%jt2 = 0
       NULLIFY(hashtable3d(is)%suiv)
    END DO

    ALLOCATE(NSupVois(Npoint) ) !-- nombre de sommets voisins ayant des num�ros sup�rieurs
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nsupvois", SIZE(NSupVois))
    END IF
    NSupVois(: ) = 0

    IF (Data%Impre > 0) THEN
       WRITE(6, *)
       PRINT *, mod_name, ' Construction de la table des Faces hashtable 3D ...', "DegreMax Nu = ", SIZE(Nu, 1), SIZE(Nu, 2)
    END IF

    ! construction de la table des faces
    SELECT CASE(MAXVAL(Mesh%Ndegre))
    CASE(4) !-- 10 => ORDRE_ELEVE
       ntyp = 4
    CASE DEFAULT
       ntyp = 0
       PRINT *, mod_name, " ERREUR : Cas impossible" , MAXVAL(Mesh%Ndegre)
       CALL delegate_stop()
    END SELECT

    DO jt = 1, Nelemt !-- �l�ments
       DO k = 1, ntyp !-- facette dans l'�l�ment

          i1    = n1(k) !-- num�ro du 1er sommet de la facette courante
          i2    = n2(k)
          i3    = n3(k)

          is1   = Nu( i1, jt) !-- num�ro global du 1er sommet de la facette courante
          is2   = Nu( i2, jt)
          is3   = Nu( i3, jt)

          is_min = MIN(is1, is2, is3)
          is_max = MAX(is1, is2, is3)

          !-- ismil est le point au milieu, {is1, is2, is3} ordonn� donne {is_min, ismil, is_max}

          ismil = is1 + is2 + is3 - is_min - is_max

          PtCellPred2d => hashtable3d(is_min)
          PtCell2d     => hashtable3d(is_min)%suiv

          ! on recherche la place du nouveau voisin
          bouclext : DO
             IF (.NOT. ASSOCIATED(PtCell2d)) THEN
                EXIT !-- pas de suivant dans la liste chain�e
             END IF
             IF ( PtCell2d%is2 > ismil  ) THEN
                EXIT !-- on a d�pass� ismil, il faut ins�rer avant
             END IF

             IF ( PtCell2d%is2 ==  ismil )  THEN !-- ismil �gaux, il faut regarder sur la 3�me composante du nom
                DO
                   IF (.NOT. ASSOCIATED(PtCell2d)) THEN
                      EXIT bouclext
                   END IF
                   IF (PtCell2d%is3 > is_max .OR. PtCell2d%is2 >  ismil  ) THEN
                      EXIT bouclext !-- on a d�pass� is_max ou ismil
                   END IF

                   PtCellPred2d => PtCell2d
                   PtCell2d     => PtCell2d%suiv
                END DO
             END IF

             PtCellPred2d => PtCell2d
             PtCell2d     => PtCell2d%suiv
          END DO bouclext

          ! on teste si la face n'est pas d�j� la

          IF (PtCellPred2d%is2 < ismil ) THEN !-- il faut ins�rer entre PtCellPred2d et PtCell2d
             ALLOCATE(NewCell2d)
             NULLIFY(NewCell2d%suiv)
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "newcell2d.A", 1)
             END IF

             NewCell2d%is2     = ismil
             NewCell2d%is3     = is_max
             NewCell2d%jt1     = jt !-- num�ro de l'�l�ment
             NewCell2d%jt2     = NONE
             NewCell2d%suiv    => PtCell2d !-- insertion entre PtCellPred2d et PtCell2d
             PtCellPred2d%suiv => NewCell2d

             IF (debug) THEN 
                PRINT *, mod_name, " Nouvelle facette : ", is_min, ismil, is_max, "element", jt
             END IF

             NSupVois(is_min)  = NSupVois(is_min) + 1

          ELSE IF (PtCellPred2d%is3  < is_max ) THEN !-- il faut ins�rer entre PtCellPred2d et PtCell2d
             ALLOCATE(NewCell2d)
             NULLIFY(NewCell2d%suiv)
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "newcell2d.B", 1)
             END IF

             NewCell2d%is2     = ismil
             NewCell2d%is3     = is_max
             NewCell2d%jt1     = jt
             NewCell2d%jt2     = NONE
             NewCell2d%suiv    => PtCell2d
             PtCellPred2d%suiv => NewCell2d

             IF (debug) THEN 
                PRINT *, mod_name, " Nouvelle facette : ", is_min, ismil, is_max, "element", jt
             END IF

             NSupVois(is_min)  = NSupVois(is_min) + 1

          ELSE IF (PtCellPred2d%is2  == ismil .AND. PtCellPred2d%is3  == is_max ) THEN !-- facette vue pour la 2�me fois
             !-- une facette ne peut �tre vue que 2 fois au maximum, puisqu'elle ne s�pare que 2 �l�ments
             PtCellPred2d%jt2 = jt

             IF (debug) THEN  
                PRINT *, mod_name, " Facette completee : ", is_min, ismil, is_max, "element", jt
             END IF
          ELSE
             PRINT *, mod_name, " NOM DE LA FACETTE : 1) jt==", jt
             PRINT *, "                    2) PtCellPred2d%is2", PtCellPred2d%is2
             PRINT *, "                    3) PtCellPred2d%is3", PtCellPred2d%is3
             PRINT *, "is_min", is_min
             PRINT *, "ismil", ismil
             PRINT *, "is_max", is_max
             WRITE(6, *) mod_name, " : ERREUR : probleme dans la construction des Faces"
             CALL delegate_stop()
          END IF
       END DO !-- k = 1, ntyp
    END DO !--  jt = 1, Nelemt

    NFacFr = Mesh%NFacFr
    IF ( NFacFr > 0 ) THEN
       ALLOCATE( NumFac(1: NFacFr) )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "numfac", SIZE(NumFac))
       END IF
    END IF

    Mesh%NFac = SUM( NSupVois) !-- nombre total de facettes, somme des taille de toutes les listes
    ALLOCATE(Mesh%NuFac(3, Mesh%NFac), Mesh%EleVois(2, Mesh%NFac))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%nufac", SIZE(Mesh%NuFac))
       CALL alloc_allocate(mod_name, "mesh%elevois", SIZE(Mesh%EleVois))
    END IF
    ! allocation m�moire pour le tableau NewLogfac = LogFac tri�
    IF ( NFacFr > 0 ) THEN
       NULLIFY(NewLogfac, NewNsfacFr)
       ALLOCATE(NewLogfac(1: NFacFr), NewLogfacReel(1: NFacFr))
       ALLOCATE(NewNsfacFr(1: 3, 1: NFacFr) ) !-- ici, on se sert explicitement du fait que ndim==3==taille du simplexe
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "newlogfac", SIZE(NewLogfac))
          CALL alloc_allocate(mod_name, "newnsfacfr", SIZE(NewNsfacFr))
          CALL alloc_allocate(mod_name, "newlogfacreel", SIZE(NewLogfacReel))
       END IF
    END IF

    !----------------------------------------------------------------------
    !                                                       -- GeomFacettes3D
    !---------------------------------------- constitution des num�rotations

    is1  = 0 !-- nombre de facettes associ�es seulement � un �l�ment (dans le cas //, cela est sup�rieur � NFacFr
    !OK OK OK
    k = 0 !-- num�ro de la facette courante
    next = 0 !-- num�ro de la facette FRONTIERE courante
    IF (Data%Impre > 0) THEN
       WRITE(6, *) mod_name, " Nombre de Faces == ", Mesh%NFac, SIZE(Mesh%NsfacFr, 1)
    END IF
    ALLOCATE(NumTetFr(NFacFr))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "numtetfr", SIZE(NumTetFr))
    END IF

    DO is = 1, Npoint
       IF (NSupVois(is) /= 0) THEN
          PtCell2d => hashtable3d(is)%suiv

          DO iv = 1, NSupVois(is) !-- boucle sur la liste chain�e partant de la racine is dans hashtable3d
             k         = k + 1

             IF ( NFacFr > 0 .AND. PtCell2d%jt2 <= 0 ) THEN !-- jt2==None => facette associ�e � un seul domaine

                IF (FaceFr(is, PtCell2d%is2, PtCell2d%is3, Mesh%NsfacFr, ifr)) THEN !-- d�termine si la face est fronti�re, retourne dans ifr le num�ro de face fronti�re
                   next                = next + 1
                   NewNsfacFr(:, next) = Mesh%NsfacFr(:, ifr) !-- Ici, Size(Mesh%NsFacFr, 1) === 3
                   NumFac(next)        = k
                   NewLogfac(next)     = Mesh%LogFr(ifr)
#ifdef ADAPT
                   NewLogfacReel(next) = Mesh%LogFrReel(ifr)
#endif
                   NumTetFr(next)      = PtCell2d%jt1
                ELSE !-- la face est vue comme face du bord (jt2 == None ou jt2 == 0)

#ifdef SEQUENTIEL /* en parallele il y a des faces du bord qui ne sont pas des faces fronti�res */
#ifndef MOULINETTE
                   PRINT *, mod_name, " Liste is==", is
                   PRINT *, "Element de la Liste iv==", iv
                   PRINT *, "Informations de la liste : NOM==", is, PtCell2d%is2, PtCell2d%is3
                   PRINT *, "Informations de la liste : ELEMENT==", PtCell2d%jt1
                   PRINT *, "HINT : le fichier de maillage est il correct (pbs avec gmsh par ex.)"
                   PRINT *, mod_name, " ERREUR : Face du bord non detectee", NFacFr, PtCell2d%jt2
                   CALL delegate_stop()
#endif
#endif
                END IF
             END IF !-- face fronti�re
             !
             Mesh%NuFac(1, k) = is
             Mesh%NuFac(2, k) = PtCell2d%is2
             Mesh%NuFac(3, k) = PtCell2d%is3

             Mesh%EleVois(1, k) = PtCell2d%jt1
             Mesh%EleVois(2, k) = PtCell2d%jt2

             IF ( PtCell2d%jt2 == NONE ) THEN
                is1 = is1 + 1
             END IF

             PtCell2d       => PtCell2d%suiv
          END DO
       END IF
    END DO
    IF (Data%Impre > 0) THEN
       PRINT *, mod_name, " Fin des appels a FaceFr"
    END IF

    Mesh%NFacFr = NFacFr
    ! ici, k doit etre egal a Nfac
    IF ( k /= Mesh%NFac  ) THEN
       WRITE(6, *) mod_name, '  Probleme !!!!!  !!!!!NFaces retrouve= ', k
       WRITE(6, *) mod_name, '  Probleme !!!!!  !!!!!NFaces real  = ', Mesh%NFac
    ELSE
       IF (Data%Impre > 0) THEN
          WRITE(6, *) mod_name, '  Ok OK OK !!!!!  NFaces retrouve  = ', k
          WRITE(6, *) mod_name, '  Ok OK OK !!!!!  NFaces real    = ', Mesh%NFac
          WRITE(6, *) mod_name, '  Ok OK OK !!!!!  NFacFr Maillage  = ', Mesh%NFacFr
          WRITE(6, *) mod_name, '  Ok OK OK !!!!!  NFacFr trouve    = ', is1
       END IF
    END IF
    IF (Data%Impre > 0) THEN
       WRITE(6, *) mod_name, " Nombre de Faces Fr           == ",  Mesh%NFacFr
       WRITE(6, *) mod_name, " Nombre de Faces Fr identifie == ", next
    END IF
    IF ( Mesh%NFacFr /= next ) THEN
       PRINT *, mod_name, " ERREUR : Probleme dans la construction geometrique"
#ifdef MURGE 
       ! // Le maillage n'est pas calcule proprement en 3D
       PRINT *, mod_name, "Mais on utilise MURGE donc c'est permis (verifier qu'on est bien en seconde passe)"
       Mesh%NFacFr = next
#else
       CALL delegate_stop()
#endif
    END IF


    IF (NFacFr /= 0) THEN
       Mesh%NumElemtFr  => NumTetFr(1:Mesh%NFacFr)

       jt = MINVAL(Mesh%NumElemtFr)

       IF (jt <= 0 .OR. jt > SIZE(Mesh%nu, 2)) THEN
          PRINT *, mod_name, " ERREUR : jt min invalide", jt, SIZE(Mesh%nu, 2), MAXVAL(Mesh%NumElemtFr)
          CALL delegate_stop()
       END IF
       jt = MAXVAL(Mesh%NumElemtFr)
       IF (jt <= 0 .OR. jt > SIZE(Mesh%nu, 2)) THEN
          PRINT *, mod_name, " ERREUR : jt max invalide", jt, SIZE(Mesh%nu, 2)
          CALL delegate_stop()
       END IF
    END IF
    ! Nettoyage m�moire !!

    DO is = 1, Npoint
       IF (NSupVois(is) /= 0) THEN
          PtCellPred2d => hashtable3d(is)%suiv
          PtCell2d => PtCellPred2d%suiv
          DO
             DEALLOCATE(PtCellPred2d)
             IF (see_alloc) THEN
                CALL alloc_deallocate(".dummy.", "ptcellpred2")
             END IF
             IF (.NOT.ASSOCIATED(PtCell2d)) THEN
                EXIT
             END IF
             PtCellPred2d => PtCell2d
             PtCell2d => PtCell2d%suiv
          END DO
       END IF
    END DO
    DEALLOCATE(hashtable3d)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "hashtable3d")
    END IF
    IF ( NFacFr > 0 ) THEN
       DEALLOCATE(Mesh%Logfr)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "mesh%logfr")
       END IF
       NULLIFY(Mesh%LogFr)
       Mesh%LogFr =>  NewLogfac

#ifdef ADAPT
       DEALLOCATE(Mesh%LogfrReel)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "mesh%logfrreel")
       END IF
       NULLIFY(Mesh%LogFrReel)
       Mesh%LogFrReel =>  NewLogfacReel
#endif

       DEALLOCATE(Mesh%NsfacFr)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "mesh%nsfacfr")
       END IF
       NULLIFY(Mesh%NsfacFr)
       Mesh%NsfacFr =>  NewNsfacFr !-- NsFacFr passe ainsi de taille 4 pour les hexa en triangles NewNsfacFr de taille 3 (RB et AL, 2008/03/20)
       Mesh%NumFac              => NumFac
       IF (Data%Impre > 0) THEN
          PRINT *, mod_name, " HWA NumFac initialise/2"

          PRINT *, mod_name, " statistiques Ancienne recherche longueur moyenne", REAL(old_search) / REAL(nb_search)
          PRINT *, mod_name, " statistiques Nouvelle recherche longueur moyenne", REAL(search_cnt) / REAL(nb_search)
       END IF

       CALL HFaceFr(0, 0, 0, 0, Mesh%NsFacFr, ifr, test)
    ENDIF
    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Sortie de la procedure"
    END IF

  CONTAINS

    !> d�termine si la face est face fronti�re, par un parcours global de toutes les faces fronti�re
    FUNCTION FaceFr(is1, is2, is3, NsfacFr, iell) RESULT(is_face_fr)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "FaceFr"
      ! variables d'appel
      INTEGER, INTENT(IN)          :: is1, is2, is3
      INTEGER, INTENT(OUT)         :: iell !-- %%%%%%%%%%%%%%%%%%%%%% Argument OUT pour une FONCTION
      INTEGER, DIMENSION(:, : ), POINTER :: NsfacFr
      LOGICAL :: is_face_fr
      ! variables locales
      INTEGER              :: ifac, is_min, is_max, ism, NFacFr
      INTEGER              :: js1, js2, js3, jsmin, jsmax, jsm
      LOGICAL, SAVE :: seen = .TRUE. !-- .FALSE. pour d�boguer
      INTEGER :: itest
      LOGICAL :: iface

      nb_search = nb_search + 1

      IF (.FALSE.) THEN !-- ancienne fa�on de faire la recherche, en O(N^2)

         is_min = MIN(is1, is2, is3)
         is_max = MAX(is1, is2, is3)
         ism   = is1 + is2 + is3 - is_min - is_max

         NFacFr = SIZE(NsfacFr, 2)
         iell   = 0

         DO ifac = 1, NFacFr
            js1   = NsfacFr(1, ifac)
            js2   = NsfacFr(2, ifac)
            js3   = NsfacFr(3, ifac)

            jsmin = MIN(js1, js2, js3)
            jsmax = MAX(js1, js2, js3)
            jsm   = js1 + js2 + js3 - jsmin - jsmax

            IF (.NOT. seen) THEN
               PRINT *, mod_name, " Facette frontiere", jsmin, jsm, jsmax
            END IF

            old_search = old_search + 1

            IF ((jsmin == is_min) .AND. (jsm == ism) .AND. (jsmax == is_max)) THEN
               iell = ifac
               EXIT
            END IF
         END DO

         seen = .TRUE.
         is_face_fr = iell /= 0
         CALL HFaceFr(is1, is2, is3, Mesh%Nfac, NsfacFr, itest, iface)
         IF (itest /= iell .OR. iface .NEQV. is_face_fr) THEN
            PRINT *, mod_name, " ALG-ERREUR is*=", is1, is2, is3, "iell==", iell, itest, "FaceFr==", &
                 & is_face_fr, iface
         END IF

      ELSE

         CALL HFaceFr(is1, is2, is3, Mesh%Nfac, NsfacFr, iell, iface)
         is_face_fr = iface

      END IF

    END FUNCTION FaceFr

    !***********************************************************************
    !                                                          -- recherche
    !***********************************************************************
    !> \author R.Butel 2008/11/07 
    !!
    !! \brief hachage interne pour accel�rer tr�s fortement FaceFr
    !!
    !!       r�le : recherche une combinaison
    FUNCTION recherche(intro, is1, is2, is3, nnn, premier) RESULT(ires)

      CHARACTER(LEN = *), PARAMETER :: mod_name = "recherche"

      LOGICAL, INTENT(IN) :: intro
      INTEGER, INTENT(IN) :: is1, is2, is3, nnn, premier
      INTEGER :: ires

      INTEGER :: ipos, ocnt, ii
      INTEGER(KIND = 8) :: key1, kincr, reprem

      !-----------------------------------------------------------------------
      !                                                         -- recherche.1
      !------------------------------------------------------------------ cl�s

      reprem = premier

      key1 = INT(is1, 8) - 1 + nnn * (INT(is2, 8) - 1 + nnn * (INT(is3, 8) - 1))
      kincr = INT(is3, 8) - 1 + nnn * (INT(is2, 8) - 1 + nnn * (INT(is1, 8) - 1))

      IF (hdebug >= 2) THEN
         PRINT *, mod_name, " DEBUT, intro==", intro, is1, is2, is3, nnn, premier
         PRINT *, mod_name, " premier==", premier
         PRINT *, mod_name, " key1-init==", key1
         PRINT *, mod_name, " kincr-init==", kincr
         PRINT *, mod_name, " key1-mod==", MODULO(key1, reprem)
         PRINT *, mod_name, " kincr-mod==", 1 + MODULO(kincr, reprem - 1)
      END IF

      key1 = MODULO(key1, reprem)
      IF (hdebug >= 2) THEN
         PRINT *, mod_name, " key1/1", key1
      END IF
      IF (key1 < 0) THEN !-- il y a eu un d�bordement d'arithmetique enti�re
         key1 = key1 + reprem
         IF (hdebug >= 2) THEN
            PRINT *, mod_name, " key1/2", key1
         END IF
      END IF

      kincr = MODULO(kincr, reprem - 1)
      IF (hdebug >= 2) THEN
         PRINT *, mod_name, " kincr/1(sans le +1)", kincr
      END IF
      IF (kincr < 0) THEN !-- il y a eu un d�bordement d'arithmetique enti�re
         kincr = kincr + reprem - 1
         IF (hdebug >= 2) THEN
            PRINT *, mod_name, " kincr/2(sans le +1)", kincr
         END IF
      END IF

      kincr = kincr + 1

      IF (hdebug >= 2) THEN
         PRINT *, mod_name, " key1==", key1, " kincr==", kincr
      END IF

      !-----------------------------------------------------------------------
      !                                                         -- recherche.2
      !-----------------------------------------------------------------------

      ipos = key1
      ires = -1

      ocnt = 0

      DO ii = 1, SIZE(t_ifac) !-- on peut limiter la recherche

         search_cnt = search_cnt + 1
         ocnt = ocnt + 1

         IF (intro) THEN

            IF (t_ifac(ipos)%ifac == -1) THEN
               ires = ipos
               EXIT
            END IF

         ELSE !-- .NOT. intro

            IF (hdebug >= 2) THEN
               PRINT *, mod_name, " HWA recherche/ipos==", ipos, " -> ", t_ifac(ipos)
            END IF

            IF (t_ifac(ipos)%kmin == is1 .AND. t_ifac(ipos)%kmed == is2 .AND. t_ifac(ipos)%kmax == is3) THEN !-- trouv�
               ires = ipos
               EXIT
            END IF
            IF (t_ifac(ipos)%ifac == -1) THEN !-- pas trouv�
               EXIT
            END IF

         END IF !-- .NOT. intro

         ipos = MODULO(INT(ipos + kincr, 8), reprem)
      END DO

      !-----------------------------------------------------------------------
      !                                                         -- recherche.3
      !-------------------------------------------------------------- �pilogue

      IF (intro .AND. ires < 0) THEN
         PRINT *, mod_name, " ERREUR : la table est pleine", SIZE(t_ifac)
         CALL delegate_stop()
      END IF

      IF (hdebug >= 2) THEN
         PRINT *, mod_name, " is*=", is1, is2, is3, "key1, kincr, premier=", key1, kincr, premier, "ires=", ires, &
              & "ocnt=", ocnt
      END IF

    END FUNCTION recherche

    !***********************************************************************
    !                                                          -- HFaceFr
    !***********************************************************************
    !> \author R.Butel 2008/11/07
    !!
    !! \brief hachage interne pour accel�rer tr�s fortement FaceFr
    !!
    !! r�le : recherche de facette fronti�re par hachage interne
    SUBROUTINE HFaceFr(is1, is2, is3, Nfac, NsfacFr, iell, isFaceFr)

      CHARACTER(LEN = *), PARAMETER :: mod_name = "HFaceFr"

      INTEGER, INTENT(IN)          :: is1, is2, is3, Nfac
      INTEGER, DIMENSION(:, : ), INTENT(IN) :: NsfacFr !-- [NO-DUMP]
      INTEGER, INTENT(OUT)         :: iell
      LOGICAL, INTENT(OUT) :: isFaceFr

      INTEGER              :: ifac, is_min, is_max, is_med, NFacFr
      INTEGER              :: js1, js2, js3, js_min, js_max, js_med
      INTEGER              :: istat, nnn, ii, ipos
      INTEGER, DIMENSION(31), PARAMETER :: premiers = (/ 131, &
           &       257, &
           &       521, &
           &      1031, &
           &      2053, &
           &      4099, &
           &      8209, &
           &     12289, &
           &     16381, &
           &     20479, &
           &     24571, &
           &     28669, &
           &     64091, &
           &    131101, &
           &    262147, &
           &    524309, &
           &   1048583, &
           &   2015557, & !-- ~150000 �me nombre premier
           &   4103629, & !-- 290000 �me nombre premier
           &   8242261, & !-- 555000 �me nombre premier
           &  16480501, & !-- 1060000 �me nombre premier
           &  33386479, & !-- 2080000 �me nombre premier
           &  64000031, & !-- site http://www.prime-numbers.org
           &  96000011, &
           & 128000003, &
           & 160000003, &
           & 192000043, &
           & 224000047, &
           & 256000001, &
           & 384000031, &
           & 512000009 /)

      INTEGER, SAVE :: premier = 0
      INTEGER, SAVE :: max_nsfacfr = 0

      LOGICAL, SAVE :: initialise = .FALSE.
      LOGICAL, SAVE :: termine = .FALSE.

      !-----------------------------------------------------------------------
      !                                                        -- HFaceFr.1
      !--------------------------------------- desinitialisation de la m�thode

      IF (termine) THEN
         PRINT *, mod_name, " ERREUR : cette methode ne peut plus etre appelee, elle a ete deinitialisee"
         CALL delegate_stop()
      END IF

      IF (is1 == 0 .AND. is2 == 0 .AND. is3 == 0) THEN
#ifndef MURGE
#ifndef MOULINETTE
         termine = .TRUE.
#endif
#endif
         IF (initialise) THEN
            DEALLOCATE(t_ifac)
            IF (see_alloc) THEN
               CALL alloc_deallocate(mod_name, "t_ifac")
            END IF
#if ((defined MURGE) || (defined MOULINETTE))
            initialise = .FALSE.
#endif
         END IF
         RETURN
      END IF

      !-----------------------------------------------------------------------
      !                                                        -- HFaceFr.2
      !---------- initialisation de la m�thode : taille de la table de hachage

      IF (.NOT. initialise) THEN

         initialise = .TRUE.

         nnn = CEILING(2.5 * NFac)

         max_nsfacfr = MAXVAL(NsFacFr)

         premier = 0
         !               PRINT *, mod_name, " Size(premiers)==", Size(premiers)
         DO ii = 1, SIZE(premiers)
            !                  PRINT *, mod_name, " ii", ii, premiers(ii), nnn, "test", premiers(ii) >= nnn
            IF (premiers(ii) >= nnn) THEN
               premier = premiers(ii)
               EXIT
            END IF
         END DO
         IF (premier == 0) THEN
            PRINT *, mod_name, " ERREUR : il n'y a pas de Nombre Premier assez grand pour passer nnn==", nnn
            CALL delegate_stop()
         END IF

         !-----------------------------------------------------------------------
         !                                                        -- HFaceFr.3
         !------ initialisation de la m�thode : allocation de la table de hachage

         ALLOCATE(t_ifac(0: premier -1), STAT = istat)

         IF (istat /= 0) THEN
            PRINT *, mod_name, " ERREUR : on ne peut allouer t_ifac, nnn==", nnn, "istat==", istat
            CALL delegate_stop()
         END IF
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "t_ifac", SIZE(t_ifac))
         END IF

         t_ifac = quad( -1, -1, -1, -1)

         !-----------------------------------------------------------------------
         !                                                        -- HFaceFr.4
         !----- initialisation de la m�thode : remplissage de la table de hachage

         search_cnt = 0

         NFacFr = SIZE(NsfacFr, 2)

         DO ifac = 1, NFacFr

            js1   = NsfacFr(1, ifac)
            js2   = NsfacFr(2, ifac)
            js3   = NsfacFr(3, ifac)

            js_min = MIN(js1, js2, js3)
            js_max = MAX(js1, js2, js3)
            js_med   = js1 + js2 + js3 - js_min - js_max

            ipos = recherche(.TRUE., js_min, js_med, js_max, max_nsfacfr, premier)

            IF (ipos < 0) THEN
               hdebug = 3
               ipos = recherche(.TRUE., js_min, js_med, js_max, max_nsfacfr, premier)
               PRINT *, mod_name, " ERREUR : Facette frontiere", js_min, js_med, js_max, "ne peut etre inseree"
               CALL delegate_stop()
            END IF

            t_ifac(ipos) = quad(js_min, js_med, js_max, ifac)

            IF (hdebug >= 2) THEN
               PRINT *, mod_name, " intro/ipos==", ipos, " -> ", t_ifac(ipos)
            END IF

         END DO
         IF (Data%Impre > 0) THEN
            PRINT *, mod_name, " longueur moyenne de la recherche a l'introduction :", REAL(search_cnt) / REAL(NFacFr)
         END IF

         search_cnt = 0

      END IF !-- initialisation

      !-----------------------------------------------------------------------
      !                                                        -- HFaceFr.5
      !-----------------------------------------------------------------------

      is_min = MIN(is1, is2, is3)
      is_max = MAX(is1, is2, is3)
      is_med   = is1 + is2 + is3 - is_min - is_max

      ipos = recherche(.FALSE., is_min, is_med, is_max, max_nsfacfr, premier)

      IF (hdebug >= 2) THEN
         PRINT *, mod_name, " is*=", is_min, is_med, is_max, "->ipos==", ipos
      END IF

      IF (ipos >= 0) THEN
         isFaceFr = .TRUE.
         iell = t_ifac(ipos)%ifac
      ELSE
         isFaceFr = .FALSE.
         iell = 0
      END IF

    END SUBROUTINE HFaceFr

  END SUBROUTINE GeomFacettes3d

  !     -----------------------------------------------
  !> \author     Boniface Nkonga      Universite de Bordeaux 1
  !!
  !! \brief Construction de la liste des sommets voisins � un point
  !!
  !! Construit, sur chaque ar�te, la relation entre les 2 sommets reli�s. JvPnt, IvPos
  !! Construit la permutation entre cette relation et la num�rotation initiale des sommets.
  SUBROUTINE GrapheDesVois( DATA, Mesh)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GrapheDesVois"

    !--------------------------------------------------------------------
    !                 Specifications et Declarations
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
    !
    !
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    ! variables d'appel
    TYPE(Donnees), INTENT(IN)                :: DATA
    TYPE(MeshDef), INTENT(INOUT)             :: Mesh

    ! Variables Locales Scalaires
    ! -------------------------------------
    INTEGER        :: is, jv, is1, is2, iseg
    INTEGER        :: Npos, NCoefMatEx

    LOGICAL, DIMENSION(: ), ALLOCATABLE      :: UnMarked
    INTEGER, DIMENSION(: ), ALLOCATABLE      :: NPtv, Rang, NewRang, Ensb
    REAL,    DIMENSION(: ), ALLOCATABLE      :: Critere
    !
    ! Fonction itreseques
    !--------------------

    !
    !--------------------------------------------------------------------
    !                Partie executable de la procedure
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------

    ALLOCATE(UnMarked(Mesh%Npoint), Critere(Mesh%Npoint) ) 
    ALLOCATE(    NPtv(Mesh%Npoint), Rang(Mesh%Npoint) )
    ALLOCATE( NewRang(Mesh%Npoint), Ensb(Mesh%Npoint) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "unmarked", SIZE(UnMarked))
       CALL alloc_allocate(mod_name, "critere", SIZE(Critere))
       CALL alloc_allocate(mod_name, "nptv", SIZE(NPtv))
       CALL alloc_allocate(mod_name, "rang", SIZE(Rang))
       CALL alloc_allocate(mod_name, "newrang", SIZE(NewRang))
       CALL alloc_allocate(mod_name, "ends", SIZE(Ensb))
    END IF


    NCoefMatEx  =  Mesh%NCoefMatEx

    IF ( data%Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Construction du tableau JPosi"
    END IF
    Npos = 0

    Mesh%JPosi(1) = 1
    DO is = 2, Mesh%Npoint + 1
       Mesh%JPosi(is) = Mesh%JPosi(is -1)  + Mesh%NbVois(is -1)
    END DO
    IF (Data%Impre > 0) THEN
       WRITE(6, *) mod_name, " Npos,  Mesh%NCoefMatEx == ", Npos,  Mesh%NCoefMatEx
       !
       !
       !______________________________________________________
       !  On construit la liste des sommets voisins a un point


       WRITE(6, *) mod_name, " Construction de la liste des points voisins d'un sommet"
    END IF
    NPtv = Mesh%JPosi(1: Mesh%Npoint)
    IF (Data%Impre > 0) THEN
       WRITE(6, *) mod_name, " NPtV(Mesh%Npoint) = ", NPtv(Mesh%Npoint), " + ", Mesh%NbVois(Mesh%Npoint)
    END IF

    DO  iseg = 1, Mesh%Nsegmt
       is1                     = Mesh%Nubo(1, iseg)
       is2                     = Mesh%Nubo(2, iseg)
       IF ( is1 == is2) THEN
          WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!!"
          CALL delegate_stop()
       END IF
       IF ( is1 <= 0 .OR. is1 > SIZE(NPtv)) THEN
          WRITE(6, *) mod_name, " debordement de tableau NPtv sur is1==", is1
          CALL delegate_stop()
       END IF
       IF ( is2 <= 0 .OR. is2 > SIZE(NPtv)) THEN
          WRITE(6, *) mod_name, " debordement de tableau NPtv sur is2==", is2
          CALL delegate_stop()
       END IF
       IF ( NPtv(is1) <= 0 .OR. NPtv(is1) > SIZE(Mesh%JvPnt)) THEN
          WRITE(6, *) mod_name, " debordement de tableau JvPnt sur NPtv(is1)==", NPtv(is1), SIZE(Mesh%JvPnt)
          CALL delegate_stop()
       END IF
       IF ( NPtv(is2) <= 0 .OR. NPtv(is2) > SIZE(Mesh%JvPnt)) THEN
          WRITE(6, *) mod_name, " debordement de tableau JvPnt sur NPtv(is2)==", NPtv(is2), SIZE(Mesh%JvPnt)
          CALL delegate_stop()
       END IF

       Mesh%JvPnt( NPtv(is1) ) = is2
       Mesh%JvPnt( NPtv(is2) ) = is1

       Mesh%IvPos( NPtv(is1) ) = NPtv(is2)
       Mesh%IvPos( NPtv(is2) ) = NPtv(is1)

       Mesh%JvSeg( NPtv(is1) ) = iseg
       Mesh%JvSeg( NPtv(is2) ) = iseg

       NPtv(is1)               = NPtv(is1)  + 1
       NPtv(is2)               = NPtv(is2)  + 1
    END DO

    DO is = 1, Mesh%Npoint
       IF ( NPtv(is) /=  Mesh%JPosi(is + 1) ) THEN
          WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!!"
          CALL delegate_stop()
       END IF
    END DO

    DO is = 1, Mesh%Npoint
       DO jv =  Mesh%JPosi(is), Mesh%JPosi(is + 1) -2
          IF ( Mesh%JvPnt(jv) >=  Mesh%JvPnt(jv + 1) ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!!"
             CALL delegate_stop()
          END IF
          IF ( Mesh%JvPnt(jv) == is  ) THEN
             WRITE(6, *) mod_name, " ERREUR : PB PB PB !!!!!!!"
             CALL delegate_stop()
          END IF
       END DO
    END DO
    IF (Data%Impre > 0) THEN
       WRITE(6, *) mod_name, " OK OK OK !!!!!!!"
    END IF
  END SUBROUTINE GrapheDesVois
  
  !!
  !! - builds the list of internal and boundary faces
  !! - finds the neighbouring hexaedrons
  !! - builds the arrays : NuFacette, NumFac, TetVois, NuFac, NumElemtFr
  !! - Modifies the array NsFacFr
  !!
  !! \param Mesh : The mesh you want to update
  !==========================
  SUBROUTINE GeomFace3D(Mesh)
  !==========================

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GeomFace3D"

    TYPE(MeshDef), INTENT(INOUT) :: Mesh
    !------------------------------------

    TYPE FaceFinder
       INTEGER, DIMENSION(:), ALLOCATABLE :: Face
       INTEGER, DIMENSION(:), ALLOCATABLE :: FaceOrd

       INTEGER :: EltId
       INTEGER :: FaceId
       LOGICAL :: Found

       TYPE(FaceFinder), POINTER :: Next
    END TYPE FaceFinder
    TYPE(FaceFinder), DIMENSION(:), POINTER :: FacesList

    TYPE FaceFinderPtr
       TYPE(FaceFinder), POINTER :: Next
    END TYPE FaceFinderPtr
    TYPE(FaceFinderPtr), DIMENSION(:), POINTER :: First 

    TYPE FaceType
       INTEGER, DIMENSION(:), ALLOCATABLE :: f
    END type FaceType
    TYPE(FaceType), DIMENSION(:), ALLOCATABLE :: f3D
    TYPE(FaceType), DIMENSION(:), ALLOCATABLE :: FacesFr

    !-------------------------------------------------

    INTEGER :: N_ele, NFacFr, NFace
    INTEGER :: N_VERTS_MAX, N_FACE_ELE_MAX, N_VERTS_FACE_MAX
    INTEGER :: is, jt, jf, ipos, iFr, mini
    INTEGER :: N_faces_ele, N_verts_face,  N_faces_tot 
    INTEGER :: CurFace, LastFace, LastFaceFr
    LOGICAL :: isFr
    !---------------------------------------------------

    INTEGER, DIMENSION(:,:), ALLOCATABLE :: NuEv, NewFaces

    INTEGER, DIMENSION(:)  , ALLOCATABLE :: LogFr

    INTEGER, DIMENSION(:), ALLOCATABLE :: Face
    !-----------------------------------------------------

    N_ele = Mesh%Nelemt

    ALLOCATE( First(Mesh%Npoint) )
    DO is = 1, Mesh%Npoint
       NULLIFY(First(is)%Next)
    END DO

    N_VERTS_MAX = SIZE(mesh%NU, 1)
    
    IF(N_VERTS_MAX == 4) THEN

       ! Tetrahedons P1
       N_FACE_ELE_MAX   = 4
       N_VERTS_FACE_MAX = 3

    ELSEIF(N_VERTS_MAX == 10) THEN

       ! Tetrahedons P2
       N_FACE_ELE_MAX   = 4
       N_VERTS_FACE_MAX = 6

   ELSEIF(N_VERTS_MAX == 5) THEN
   
      ! Pyramids P1
      N_FACE_ELE_MAX   = 5
      N_VERTS_FACE_MAX = 4

   ELSEIF(N_VERTS_MAX == 14) THEN
   
      ! Pyramids P2
      N_FACE_ELE_MAX   = 5
      N_VERTS_FACE_MAX = 9

   !ELSEIF(N_VERTS_MAX == 6) THEN
   !
   !   ! Prisms
   !   N_FACE_ELE_MAX   = 5     
   !   N_VERTS_FACE_MAX = 4

    ELSEIF(N_VERTS_MAX == 8) THEN

       ! Hexaedrons Q1
       N_FACE_ELE_MAX   = 6
       N_VERTS_FACE_MAX = 4

   ELSEIF(N_VERTS_MAX == 27) THEN

       ! Hexaedrons Q2
       N_FACE_ELE_MAX   = 6
       N_VERTS_FACE_MAX = 9

    ELSE

       WRITE(*,*) 'ERROR: unknown element'
       STOP

    ENDIF

    ALLOCATE( FacesList(N_FACE_ELE_MAX*N_ele) )
    
    ALLOCATE( NewFaces(N_VERTS_FACE_MAX,     &
                       N_FACE_ELE_MAX*N_ele) ) 

    NewFaces = 0
    
    ! numero global des facettes locale a l'element
    ALLOCATE( Mesh%NuFacette(N_FACE_ELE_MAX, N_ele) )

    Mesh%NuFacette = 0
       
    ipos = 0

    ! Creation de l'ensemble des faces (on ordonne par numero croissant de sommet)
    DO jt = 1, N_ele

       IF( Mesh%EltType(jt) == et_tetraP1 ) THEN

          N_faces_ele  = 4
          N_verts_face = 3

          ALLOCATE( f3D(N_faces_ele) )

          ALLOCATE( f3D(1)%f(N_verts_face) )
          ALLOCATE( f3D(2)%f(N_verts_face) )
          ALLOCATE( f3D(3)%f(N_verts_face) )
          ALLOCATE( f3D(4)%f(N_verts_face) )

!!$       f3D(1)%f = (/ 4, 2, 3 /)
          f3D(1)%f = (/ 2, 3, 4 /)
          f3D(2)%f = (/ 1, 4, 3 /)
          f3D(3)%f = (/ 4, 1, 2 /)
          f3D(4)%f = (/ 2, 1, 3 /)

       ELSEIF( Mesh%EltType(jt) == et_HexaP1 ) THEN

          N_faces_ele  = 6
          N_verts_face = 4

          ALLOCATE( f3D(N_faces_ele) )

          ALLOCATE( f3D(1)%f(N_verts_face) )
          ALLOCATE( f3D(2)%f(N_verts_face) )
          ALLOCATE( f3D(3)%f(N_verts_face) )
          ALLOCATE( f3D(4)%f(N_verts_face) )
          ALLOCATE( f3D(5)%f(N_verts_face) )
          ALLOCATE( f3D(6)%f(N_verts_face) )

          f3D(1)%f = (/ 1, 4, 3, 2 /)
          f3D(2)%f = (/ 5, 6, 7, 8 /)
          f3D(3)%f = (/ 1, 2, 6, 5 /)
          f3D(4)%f = (/ 2, 3, 7, 6 /)
          f3D(5)%f = (/ 3, 4, 8, 7 /)
          f3D(6)%f = (/ 1, 5, 8, 4 /)
                   
       ELSEIF( Mesh%EltType(jt) == et_PyramP1 ) THEN

          N_faces_ele  = 5
          ALLOCATE( f3D(N_faces_ele) )

          N_verts_face = 3
          ALLOCATE( f3D(1)%f(N_verts_face) )
          ALLOCATE( f3D(2)%f(N_verts_face) )
          ALLOCATE( f3D(3)%f(N_verts_face) )
          ALLOCATE( f3D(4)%f(N_verts_face) )

          N_verts_face = 4
          ALLOCATE( f3D(5)%f(N_verts_face) )

          f3D(1)%f = (/ 4, 5, 3    /)
          f3D(2)%f = (/ 1, 5, 4    /)
          f3D(3)%f = (/ 1, 2, 5    /)
          f3D(4)%f = (/ 2, 3, 5    /)
          f3D(5)%f = (/ 1, 4, 3, 2 /)

       ELSEIF( Mesh%EltType(jt) == et_tetraP2 ) THEN

          N_faces_ele  = 4
          N_verts_face = 6

          ALLOCATE( f3D(N_faces_ele) )

          ALLOCATE( f3D(1)%f(N_verts_face) )
          ALLOCATE( f3D(2)%f(N_verts_face) )
          ALLOCATE( f3D(3)%f(N_verts_face) )
          ALLOCATE( f3D(4)%f(N_verts_face) )

          f3D(1)%f = (/ 2, 3, 4, 6, 9, 10 /)
          f3D(2)%f = (/ 1, 4, 3, 8, 9, 7  /)
          f3D(3)%f = (/ 4, 1, 2, 8, 5, 10 /)
          f3D(4)%f = (/ 2, 1, 3, 5, 7, 6  /)

       ELSEIF( Mesh%EltType(jt) ==  et_hexap2 ) THEN

          N_faces_ele  = 6
          N_verts_face = 9
          
          ALLOCATE( f3D(N_faces_ele) )

          ALLOCATE( f3D(1)%f(N_verts_face) )
          ALLOCATE( f3D(2)%f(N_verts_face) )
          ALLOCATE( f3D(3)%f(N_verts_face) )
          ALLOCATE( f3D(4)%f(N_verts_face) )
          ALLOCATE( f3D(5)%f(N_verts_face) )
          ALLOCATE( f3D(6)%f(N_verts_face) )

          f3D(1)%f = (/ 1, 4, 3, 2, 10, 14, 12, 9,  21 /)
          f3D(2)%f = (/ 5, 6, 7, 8, 17, 19, 20, 18, 26 /)
          f3D(3)%f = (/ 1, 2, 6, 5, 9,  13, 17, 11, 22 /)
          f3D(4)%f = (/ 2, 3, 7, 6, 12, 15, 19, 13, 24 /)
          f3D(5)%f = (/ 3, 4, 8, 7, 14, 16, 20, 15, 25 /)
          f3D(6)%f = (/ 1, 5, 8, 4, 11, 18, 16, 10, 23 /)

       ELSE

          WRITE(*,*) 'ERROR: 3D element not defined!'
          STOP

       ENDIF

       DO jf = 1, N_faces_ele

          ipos = ipos + 1

          N_verts_face = SIZE(f3D(jf)%f)

          ! On stocke la face
          ALLOCATE( FacesList(ipos)%Face(N_verts_face) )
          FacesList(ipos)%Face = Mesh%Nu(f3D(jf)%f, jt)

          ! On stocke la face par sommets ordonnes
          ALLOCATE( FacesList(ipos)%FaceOrd(N_verts_face) )
          FacesList(ipos)%FaceOrd = FacesList(ipos)%Face

          CALL ORDER( FacesList(ipos)%FaceOrd )

          ! Creation du chainage de la liste pour accelerer la recherche
          mini = FacesList(ipos)%FaceOrd(1)

          FacesList(ipos)%Next => First(mini)%Next
          First(mini)%Next     => FacesList(ipos)

          ! on stocke des infos utiles
          FacesList(ipos)%EltId  = jt
          FacesList(ipos)%FaceId = jf
          FacesList(ipos)%Found = .FALSE.

       END DO

       DEALLOCATE(f3D)

    END DO

    N_faces_tot = ipos

    !$
    IF( Mesh%NFacFr > 0 ) THEN

       ALLOCATE( FacesFr(Mesh%NFacFr) )

       DO jf = 1, Mesh%NFacFr

          N_verts_face = COUNT( Mesh%NsFacFr(:, jf) /= 0 )

          ALLOCATE( FacesFr(jf)%f(N_verts_face+1) )

          FacesFr(jf)%f(1:N_verts_face) = Mesh%NsFacFr(1:N_verts_face, jf)

          FacesFr(jf)%f(N_verts_face+1) = jf

          CALL ORDER( FacesFr(jf)%f(1:N_verts_face) )

       END DO

    ENDIF
    !$

    ! Liste les elements voisins d'une face : allocation pire des cas
    ALLOCATE( NuEv(2, N_faces_tot) )

    NuEv = 0! -1

    ALLOCATE( Mesh%NumFac(Mesh%NFacFr), &
                    LogFr(Mesh%NFacFr) )

    ! Face Interne ou face frontiere ?
    NfacFr = 0
    Nface  = 0

    CurFace = 1

    LastFace   = N_faces_tot
    LastFaceFr = Mesh%NFacFr

    DO CurFace = 1, LastFace

       ! Si ce test est faux on a deja trouve la face
       IF (.NOT. FacesList(CurFace)%Found) THEN
 
          Nface = Nface + 1

          N_verts_face = SIZE( FacesList(CurFace)%FaceOrd )

          ALLOCATE( Face(N_verts_face) )

          Face = FacesList(CurFace)%FaceOrd
          
          ! On stocke la face ordonnee
          NewFaces(1:N_verts_face, Nface) = FacesList(CurFace)%Face(1:N_verts_face)

          CALL SearchFaces(First, FacesList(CurFace), isFr, NuEv(:, Nface))

          ! Face Frontiere on cherche a l'identifier
          IF (isFr) THEN

             CALL FindFr(Face, FacesFr, LastFaceFr, iFr)

             ! Cas inverse possible qu'en parallele
             IF (iFr > 0) THEN 

                NfacFr = NfacFr + 1

                Mesh%NumFac(NfacFr) = Nface

                LogFr(NfacFr) = Mesh%LogFr(iFr)

             END IF

          END IF

          DEALLOCATE( Face )

       END IF

    END DO

    ! Suppression des donnees utilisees
    DEALLOCATE(FacesList, First)

    ! Recopie des logFr
    IF( NfacFr > 0 ) THEN
       Mesh%LogFr(:) = LogFr(:)
       DEALLOCATE(LogFr)
    ENDIF

    Mesh%NFac = NFace
    PRINT *, mod_name, " On a trouve ", Nface, "Faces"
    PRINT *, mod_name, " On a trouve ", NfacFr, "Faces Frontieres"

    ! Allocation a la bonne taille de tetvois
    ALLOCATE(Mesh%EleVois(2, Nface))

    Mesh%EleVois(:,1:Nface) = NuEv(:,1:Nface)

    DEALLOCATE(NuEv)

    ! Allocation a la bonne taille de nufac
    ALLOCATE(Mesh%NuFac(N_VERTS_FACE_MAX, Nface))

!   Mesh%NuFac = 0
    Mesh%NuFac = NewFaces

    DEALLOCATE(NewFaces)

    ! Reallocation de NsFacFr
    IF (NfacFr > 0) THEN

       DEALLOCATE(Mesh%NsFacFr)

       ALLOCATE( Mesh%NsFacFr(N_VERTS_FACE_MAX, NfacFr) )

       Mesh%NsFacFr = 0

       Mesh%NsFacFr = Mesh%NuFac(:, Mesh%NumFac)

       ! Allocation du numero des element frontieres
       ALLOCATE(Mesh%NumElemtFr(NfacFr))

       Mesh%NumElemtFr = Mesh%EleVois(1, Mesh%NumFac)

       DEALLOCATE( FacesFr )

    END IF

  CONTAINS

    !> \brief Find a face in the array of boundary faces
    SUBROUTINE FindFr(Face, FacesFr, LastFaceFr, iFr)

      INTEGER,        DIMENSION(:), INTENT(IN)    :: Face
      TYPE(FaceType), DIMENSION(:), INTENT(INOUT) :: FacesFr
      INTEGER,                      INTENT(INOUT) :: LastFaceFr
      INTEGER,                      INTENT(OUT)   :: iFr

      INTEGER :: jf, N_v, nn

      N_v = SIZE(FACE)
      
      iFr = -1
      ! On parcours les faces
      DO jf = 1, LastFaceFr

         ! Si on a trouve la face que l'on cherchait
         IF ( COUNT(FacesFr(jf)%f(1:N_v) == Face) == N_v ) THEN

            ! On supprime cette face de la liste globale
            ! on va remplacer la face trouvee par la derniere face
            nn = SIZE(FacesFr(jf)%f)
            iFr = FacesFr(jf)%f(nn)

            DEALLOCATE( FacesFr(jf)%f )
            nn = SIZE(FacesFr(LastFaceFr)%f)
            ALLOCATE( FacesFr(jf)%f(nn) )

            FacesFr(jf)%f = FacesFr(LastFaceFr)%f
            LastFaceFr = LastFaceFr - 1

            ! Le travail pour cette face est finie on peut quitter cette routine 
            RETURN

         END IF

      END DO

#ifndef MOULINETTE
#ifdef SEQUENTIEL

      ! On a pas trouve la face, il y a donc un probleme dans le maillage
      PRINT *, "Face frontiere introuvable:", Face
      !CALL Abort()

#endif
#endif

    END SUBROUTINE FindFr
    

    !> \brief Subroutine to find an hexahedron face
    !!
    !! - If the face is not found it's a boundary (or a parallel mesh)
    !! - If the face is found we can return the 2 neighbouring hexaedrons
    SUBROUTINE SearchFaces(First, FaceBase, isFr, EltVois)
      TYPE(FaceFinderPtr), DIMENSION(:), POINTER :: First
      TYPE(FaceFinder)   , TARGET, INTENT(INOUT) :: FaceBase

      LOGICAL                , INTENT(OUT)   :: isFr
      INTEGER, DIMENSION(:)  , INTENT(OUT)   :: EltVois
      INTEGER, DIMENSION(SIZE(FaceBase%FaceOrd)) :: base

      TYPE(FaceFinder), POINTER :: iterator
      INTEGER :: N_v

      isFr = .TRUE.
      EltVois = 0!-1

      N_v = SIZE(FaceBase%FaceOrd)
     
      ! Element a trouver dans le reste du tableau

      base = facebase%FaceOrd

      iterator => First( base(1) )%Next 

      ! On parcours les faces, ayant le meme plus petit numero de sommet
      DO WHILE (ASSOCIATED(iterator) .AND. .NOT. ASSOCIATED(iterator, facebase))

         ! Si on a trouve la face que l'on cherchait
         IF (COUNT(iterator%FaceOrd == base) == N_v) THEN

            ! Ce n'est pas une face frontiere
            isFr = .FALSE.
            ! On connait les numeros des voisins
            EltVois(1) = iterator%EltId
            EltVois(2) = facebase%EltId

            ! Remplissage de NuFacette
            Mesh%NuFacette(iterator%faceId, iterator%EltId) = Nface
            Mesh%NuFacette(facebase%faceId, facebase%EltId) = Nface

            ! On supprime ces 2 faces de la liste globale
            iterator%Found = .TRUE.
            facebase%Found = .TRUE.
            ! Le travail pour cette face est fini on peut quitter cette routine 
            RETURN

         END IF

         iterator => iterator%Next

      END DO

      ! On tombe ici quand on travaille sur une face frontiere
      ! On a alors qu'un element voisin
      EltVois(1) = facebase%EltId
      ! On supprime cette face de la liste globale
      facebase%Found = .TRUE.

      ! Remplissage de NuFacette
      Mesh%NuFacette(iterator%faceId, iterator%EltId) = Nface

    END SUBROUTINE SearchFaces


    !> \brief A simple sorting algorithm (for small arrays)
    SUBROUTINE ORDER( List )
      INTEGER, DIMENSION(:) , INTENT(INOUT) :: List

      INTEGER :: n,i
      INTEGER :: posm, mini

      n = SIZE(List)
      DO i = 1, n-1
         posm = MINLOC(List(i:n), DIM=1) + i - 1 
         mini = List(posm)
         List(posm) = List(i)
         List(i) = mini
      END DO
    END SUBROUTINE ORDER


  END SUBROUTINE GeomFace3D
  !========================

  SUBROUTINE Fbx2ScotchArcs(Npoint, Nelemt, Ndegre, Nu,  &
       &   BaseVal, VertNbr, EdgeNbr, VeLoTab, VertTab, EdgeTab, EdLoTab )
    !==============================================================
    ! Ndegre(jt) ::  type de l'element
    ! Nu(k, jt)   ::  numero du Keme point de l'element jt
    !==============================================================
    ! ****************
    !   declarations
    ! ***************


    ! variables d'appel
    CHARACTER(LEN = *), PARAMETER      :: mod_name = "Fbx2ScotchArcs"
    INTEGER, INTENT(IN)              :: Npoint, Nelemt
    INTEGER, INTENT(OUT)             :: BaseVal
    INTEGER, INTENT(OUT)             :: VertNbr, EdgeNbr
    INTEGER, DIMENSION(: ),   POINTER :: Ndegre
    INTEGER, DIMENSION(:, : ), POINTER :: Nu
    INTEGER, DIMENSION(: ),  POINTER  :: VeLoTab, VertTab, EdgeTab, EdLoTab


    ! variables locales
    INTEGER                   :: is, iv, jt, k, is1, is2, iseg
    INTEGER                   :: ismin, ismax
    INTEGER                   :: next, ntyp, Nsegmt
    TYPE(cellule), POINTER    :: NewCell, PtCell, PtCellPred
    TYPE(cellule), DIMENSION (: ), TARGET, ALLOCATABLE :: hashtable
    INTEGER, DIMENSION(: ),   ALLOCATABLE :: NbVois
    INTEGER :: istat


    ! ***************************
    !   initialisations
    ! ***************************
    ALLOCATE(hashtable(Npoint), STAT = istat)
    IF (istat /= 0) THEN
       PRINT *, mod_name, " ERREUR : impossible d'allouer hashtable", Npoint
       CALL delegate_stop()
    END IF
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "hashtable", SIZE(hashtable))
    END IF

    DO is = 1, Npoint
       hashtable(is)%val = 0
       NULLIFY(hashtable(is)%suiv)
    END DO

    ALLOCATE(NbVois(Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nbvois", SIZE(NbVois))
    END IF
    NbVois(1: Npoint) = 0

    Nsegmt = 0


    ! *******************************************
    !   construction de hashtable, nbvois, Nsegmt
    ! *******************************************

    WRITE(6, *)
    PRINT *, mod_name, ' Construction de la table des segments hashtable ...'

    ! construction de la table des segments

    DO jt = 1, Nelemt

       !write(*, *) ' jt = ', jt, ' Ndegre(jt) = ', Ndegre(jt), '*'

       ntyp = Ndegre(jt)
       DO k = 1, ntyp

          next = MOD(k, ntyp) + 1

          is1 = Nu(   k, jt)
          is2 = Nu(next, jt)
          !
          ismin = MIN(is1, is2)
          ismax = MAX(is1, is2)

          ! recherche de la position d'insertion de ismax dans
          ! le tableau hashtable(ismin).
          PtCell     => hashtable(ismin)%suiv
          PtCellPred => hashtable(ismin)

          ! on recherche la place du nouveau voisin
          DO
             IF (.NOT. ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             IF (PtCell%val > ismax) THEN
                EXIT ! on a trouv� la place, on arr�te
             END IF

             PtCellPred => PtCell
             PtCell     => PtCell%suiv
          END DO

          ! on teste si le sommet n'est pas deja la
          IF (PtCellPred%val < ismax) THEN

             ! creation de la nouvelle cellule
             ALLOCATE(NewCell)
             IF (see_alloc) THEN
                CALL alloc_allocate(".global.", "newcell.10", 1)
             END IF
             NewCell%val  = ismax

             ! insertion de la nouvelle cellule
             NewCell%suiv    => PtCell
             PtCellPred%suiv => NewCell

             ! on a rajoute un voisin, donc on incremente
             ! NbVois(ismin) et Nsegmt
             NbVois(ismin) = NbVois(ismin) + 1
             Nsegmt = Nsegmt + 1

          END IF
          ! ######################################################
          ismin = MAX(is1, is2)
          ismax = MIN(is1, is2)

          ! recherche de la position d'insertion de ismax dans
          ! le tableau hashtable(ismin).
          PtCell     => hashtable(ismin)%suiv
          PtCellPred => hashtable(ismin)

          ! on recherche la place du nouveau voisin
          DO
             IF (.NOT. ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             IF (PtCell%val > ismax) THEN
                EXIT ! on a trouve la place, on arr�te
             END IF

             PtCellPred => PtCell
             PtCell     => PtCell%suiv
          END DO

          ! on teste si le sommet n'est pas deja la
          IF (PtCellPred%val < ismax) THEN

             ! creation de la nouvelle cellule
             ALLOCATE(NewCell)
             IF (see_alloc) THEN
                CALL alloc_allocate(".global.", "newcell.20", 1)
             END IF
             NewCell%val  = ismax

             ! insertion de la nouvelle cellule
             NewCell%suiv    => PtCell
             PtCellPred%suiv => NewCell

             ! on a rajoute un voisin, donc on incremente
             ! NbVois(ismin) et Nsegmt
             NbVois(ismin) = NbVois(ismin) + 1
             Nsegmt = Nsegmt + 1

          END IF
       END DO

    END DO


    ! le tableau temporaire hashtable est constitue,
    ! on le recopie dans le tableau Nubo


    ! ****************************
    !   creation du tableau Nubo
    ! ****************************

    BaseVal = 1
    VertNbr = Npoint
    EdgeNbr = Nsegmt

    WRITE(6, *) mod_name, ' Remplissage du Graphe'
    WRITE(6, *) mod_name, " VertNbr = ", VertNbr
    WRITE(6, *) mod_name, " EdgeNbr = ", EdgeNbr


    ALLOCATE(VeLoTab(VertNbr), VertTab(VertNbr + 1) )
    ALLOCATE(EdgeTab(EdgeNbr), EdLoTab(EdgeNbr)  )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "velotab", SIZE(VeLoTab))
       CALL alloc_allocate(mod_name, "verttab", SIZE(VertTab))
       CALL alloc_allocate(mod_name, "edgetab", SIZE(EdgeTab))
       CALL alloc_allocate(mod_name, "edlotab", SIZE(EdLoTab))
    END IF

    VeLoTab(: ) = 1 !-- LogFr(:)
    VertTab(1) = 1
    EdLoTab(: ) = 0
    iseg       = 0

    DO is = 1, Npoint
       ! si le sommet is a un(des) voisin(s)
       IF (NbVois(is) /= 0) THEN
          VertTab(is + 1) = VertTab(is) + NbVois(is)

          PtCell => hashtable(is)%suiv

          ! on prend tous les voisins du sommet is
          DO iv = 1, NbVois(is)
             iseg          = iseg + 1
             EdgeTab(iseg) = PtCell%val
             ! on pointe sur le voisin suivant
             PtCell => PtCell%suiv
          END DO

       END IF

    END DO

    ! Nettoyage memoire !!
    DO is = 1, Npoint
       IF (NbVois(is) /= 0) THEN
          PtCellPred => hashtable(is)%suiv
          PtCell => PtCellPred%suiv
          DO
             DEALLOCATE(PtCellPred)
             IF (see_alloc) THEN
                CALL alloc_deallocate(".global.", "newcell.30")
             END IF
             IF (.NOT.ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             PtCellPred => PtCell
             PtCell => PtCell%suiv
          END DO
       END IF
    END DO

    DEALLOCATE(NbVois)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "nbvois")
    END IF
    DEALLOCATE(hashtable)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "hashtable")
    END IF

  END SUBROUTINE Fbx2ScotchArcs
  
  !=================================
  SUBROUTINE Greddy_coloring(N_DOFS)
  !=================================

    IMPLICIT NONE
    
    INTEGER, INTENT(IN) :: N_DOFS
    !-----------------------------

    LOGICAL, DIMENSION(N_DOFS) :: colorable
    INTEGER, DIMENSION(N_DOFS) :: color2node

    INTEGER, DIMENSION(:), ALLOCATABLE :: idx

    INTEGER :: N_color, N_colored
    INTEGER :: l, k, je, j
    !-----------------------------

    N_color = 0
    N_colored = 0
    color2node = 0

    DO

       ! Increment the colors
       N_color = N_color + 1

       colorable = .TRUE.

       ! Node already colored cannot colored
       DO k = 1, N_DOFS
          IF( color2node(k) > 0 ) THEN
             colorable(k) = .FALSE.
          ENDIF
       ENDDO
      
       ! Look for nodes that can be colored
       ! with the new color
       DO l = 1, N_DOFS
           
          IF( colorable(l) ) THEN

             color2node(l) = N_color

             N_colored = N_colored + 1

             ! The neighbors nodes cannot be colored
             ! with the current color
             DO j = 1, SIZE(Connect(l)%cn_ele)

                je = Connect(l)%cn_ele(j)

                DO k = 1, d_element(je)%e%N_points
                   colorable( d_element(je)%e%NU(k) ) = .FALSE.
                ENDDO                

             ENDDO

          ENDIF
          
       ENDDO

       IF(N_colored == N_dofs) EXIT

       IF( N_color > 8 ) STOP

    ENDDO

    WRITE(*, '("   Found ", I2, " Colors")') N_color
    
    ALLOCATE(ColorMap(N_color))

    DO j = 1, N_color

       k = COUNT(color2node == j)
       ALLOCATE( ColorMap(j)%NU(k) )

    ENDDO

    ALLOCATE( idx(N_color) )
    idx = 0

    DO j = 1, N_DOFS
       
       k = color2node(j)

       idx(k) = idx(k) + 1
       
       ColorMap(k)%NU(idx(k)) = j

    ENDDO

    DEALLOCATE(idx)
    
  END SUBROUTINE Greddy_coloring
  !=============================

  !===============================================
  SUBROUTINE Mesh_P2toP1(MeshP2, MeshP1, Map_P2P1)
  !===============================================

    IMPLICIT NONE

    TYPE(MeshDef), INTENT(IN)  :: MeshP2
    TYPE(MeshDef), INTENT(OUT) :: MeshP1
    INTEGER, DIMENSION(:), ALLOCATABLE, &
                   INTENT(OUT) :: Map_P2P1
    !--------------------------------------

    LOGICAL, DIMENSION(:), ALLOCATABLE :: seen

    INTEGER :: Nu, nnz, nnz_max, je, k
    !----------------------------------------------

    MeshP1%NOrdre = 2

    MeshP1%NDim = MeshP2%NDim

    MeshP1%NElemt = MeshP2%NElemt

    ALLOCATE( MeshP1%Nsommets(MeshP1%NElemt), &
              MeshP1%Ndegre(MeshP1%NElemt)    )

    ALLOCATE( MeshP1%LogElt(MeshP1%NElemt) )
    MeshP1%LogElt = 0

    ALLOCATE( seen(MeshP2%Npoint) )
    seen = .FALSE.

    ALLOCATE( Map_P2P1(MeshP2%Npoint) )
    Map_P2P1 = 0

    Nu = 0

    DO je = 1, MeshP1%Nelemt

       MeshP1%Nsommets(je) = MeshP2%Nsommets(je)
       MeshP1%Ndegre(je)   = MeshP2%Nsommets(je)       

       DO k = 1, MeshP1%Ndegre(je) ! P2 -> P1

          IF(.NOT. seen(MeshP2%Nu(k, je)) ) THEN

             seen(MeshP2%Nu(k, je)) = .TRUE.

             Nu = Nu + 1

             Map_P2P1(MeshP2%Nu(k, je)) = Nu
            
          ENDIF

       ENDDO

    ENDDO

    MeshP1%Npoint = Nu

    ALLOCATE( MeshP1%LogPt(MeshP1%Npoint) )
    MeshP1%LogPt = 0

    ALLOCATE( MeshP1%Coor(MeshP1%Ndim, MeshP1%Npoint) )

    MeshP1%NdegreMax   = MAXVAL(MeshP1%Ndegre)
    MeshP1%NSommetsMax = MAXVAL(MeshP1%NSommets)

    ALLOCATE( MeshP1%Nu(MeshP1%NdegreMax, MeshP1%Nelemt) )
    MeshP1%Nu = 0

    ALLOCATE( MeshP1%LogPt(MeshP1%Npoint) )
    MeshP1%LogPt = 0

    ALLOCATE( MeshP1%LogElt(MeshP1%Nelemt) )
    MeshP1%LogElt = 0

    DO je = 1, MeshP1%Nelemt

       DO k = 1, MeshP1%Ndegre(je)

          MeshP1%Nu(k, je) = Map_P2P1( MeshP2%Nu(k, je) )

          MeshP1%Coor(:, MeshP1%Nu(k, je)) = MeshP2%Coor(:, meshP2%Nu(k, je))

       ENDDO

    ENDDO

    DEALLOCATE( seen )

    MeshP1%NFacFr = MeshP2%NFacFr

    nnz_max = 0

    DO je = 1, MeshP1%NFacFr

       nnz = COUNT(MeshP2%NsFacFr(:, je) /= 0)
     
       nnz = FLOOR( REAL(nnz)/REAL(MeshP1%Ndim) )

       ! # of vertces of the P1 face
       nnz = nnz + 1

       nnz_max = MAX(nnz_max, nnz)

    ENDDO

    ALLOCATE( MeshP1%LogFr(MeshP1%NFacFr) )
    ALLOCATE( MeshP1%NsFacFr(nnz_max, MeshP1%NFacFr) )

    MeshP1%NsFacFr = 0

    DO je = 1, MeshP1%NFacFr

       nnz = COUNT(MeshP2%NsFacFr(:, je) /= 0)

       nnz = FLOOR( REAL(nnz)/REAL(MeshP1%Ndim) )

       nnz = nnz + 1

       MeshP1%NsFacFr(1:nnz, je) = Map_P2P1( MeshP2%NsFacFr(1:nnz, je) )
       MeshP1%LogFr(je)          = MeshP2%LogFr(je)

    ENDDO

    MeshP1%Ncells = MeshP1%Npoint

    SELECT CASE(MeshP1%Ndim)
    CASE(2)
       CALL GeomSegments2d(MeshP1, Data%impre)
    CASE(3)
       CALL GeomSegments3d(Data, MeshP1)
    END SELECT

  END SUBROUTINE Mesh_P2toP1
  !=========================

  ! ----------- TEST ------------
  !==============================
  SUBROUTINE check_P1Mehs(MeshP1)
  !==============================

    IMPLICIT NONE

     TYPE(MeshDef), INTENT(IN) :: MeshP1
     !----------------------------------

     LOGICAL, DIMENSION(:), ALLOCATABLE :: seen

     INTEGER :: je, Nu, k
     !----------------------------------
     ALLOCATE ( seen(MeshP1%Npoint) )

     seen = .FALSE.; Nu = 0

     write(2, '("$MeshFormat")')
     write(2, '("2.2 0 8")')
     write(2, '("$EndMeshFormat")')

     write(2, '("$Nodes")')
     write(2,'(I9)') MeshP1%Npoint
     DO je = 1, MeshP1%Nelemt
        DO k = 1, MeshP1%Ndegre(je)
           IF( .NOT. seen(MeshP1%Nu(k, je)) ) THEN
              seen(MeshP1%Nu(k, je)) = .TRUE.
              Nu = NU + 1
              write(2, '(I9, 3F24.16)') Nu, MeshP1%Coor(:, (MeshP1%Nu(k, je)))
           ENDIF
        ENDDO
     ENDDO
     write(2, '("$EndNodes")')
     
     Nu = 0

     write(2, '("$Elements")')
     write(2, '(I9)') MeshP1%Nelemt+MeshP1%NFacFr
     DO je = 1, MeshP1%Nelemt
        write(2, '(9I9)') je, 4, 2, 0, 2, MeshP1%Nu(:, je)
     ENDDO
     DO je = 1, MeshP1%NFacFr
        write(2, '(8I9)') je+MeshP1%Nelemt, 2, 2, MeshP1%LogFr(je), 0, MeshP1%NsFacFr(:, je)
     ENDDO
     write(2,'("$EndElements")')

     DEALLOCATE(seen) 

  END SUBROUTINE check_P1Mehs
  !==========================

END MODULE GeomGraph
