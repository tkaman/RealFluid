!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 A.Larat, B.Nkonga, M.Papin, C.Tav�                */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   MODULE GeomParam
      USE Lestypes
      IMPLICIT NONE

   CONTAINS


    !---------------------------------------------
    !!@author     B. Nkonga & Modifs Mikael Papin INRIA / Univ. de Bordeaux 1
    !----------------------------------------------
    !!     role: Calcul des aires des cellules 2D et de leur hauteurs
    !!           Fausse hauteur sur les quadrangles sauf si rectangles.
!!
!!-- Calcul, pour chaque �l�ment,
!!-- du centre de gravit� (local G),
!!-- puis ajout � l'aire totale de l'aire d'un triangle form� par 2 points cons�cutifs de la fronti�re et le centre de gravit�
!!-- h (locale) est la plus grande longueur de segment au carr�, puis
!!-- h = aire / sqrt(h) devient un rayon approxim�, vraie hauteur pour les triangles et les rectangles
!!-- puis remplit Mesh%VolEl, Mesh%H et Mesh%VolS
      SUBROUTINE GEOMVol2DGeneric(Mesh) ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOMVol2DGeneric"
    !-----------------------------------------------
    !     debut des declarations
    !-----------------------------------------------
    !.. Formal Arguments ..
         TYPE(MeshDef),  INTENT(INOUT)            :: Mesh
         INTEGER       :: k, kp1, jt, Degre
         REAL          :: h, aire, xG1, xG2, yG1, yG2
         REAL,    DIMENSION(1: Mesh%Ndim, 1: Mesh%NdegreMax) :: Coor
         INTEGER :: indice
#ifdef ORDRE_ELEVE
         INTEGER, DIMENSION(1: Mesh%NSommetsMax)           :: NULoc
#else
         INTEGER, DIMENSION(1: Mesh%NdegreMax)             :: NULoc
#endif
         REAL,    DIMENSION(1: Mesh%Ndim)                  :: g

#ifdef ORDRE_ELEVE
         IF (Mesh%Ncells == 0) THEN
            PRINT *, mod_name, " ERREUR : Mesh%Ncells==0"
            CALL delegate_stop()
         END IF
         IF (Size(mesh%h) == 0) THEN
            PRINT *, mod_name, " ERREUR : Size(Mesh%h)==0"
            CALL delegate_stop()
         END IF
         ALLOCATE(Mesh%Vols(Mesh%NCells))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "mesh%vols", Size(Mesh%Vols))
         END IF
#endif
         Mesh%Vols = 0.0
         Mesh%CentrEl = 0.0
         Mesh%h    = 1.0d+12

         DO jt = 1, Mesh%Nelemt
            h     = 0.0
            aire  = 0.0
            Coor  = 0.0
#ifdef ORDRE_ELEVE
            Degre = Mesh%NSommets(jt)
#else
            Degre = Mesh%Ndegre(jt)
#endif
!            PRINT *, mod_name, " Degre==", Degre, "Ndegre==", Mesh%Ndegre(jt), Size(NULoc), Size(Coor, 2), Size(Mesh%nu, 1) !-- DO KEEP IT
            DO k = 1, Degre
               NULoc(k)  = Mesh%nu(k, jt)
               Coor(:, k) = Mesh%Coon(:, NULoc(k))
            END DO

            ! Centre de gravite du triangle
            DO k = 1, Mesh%Ndim
               g(k) = Sum(Coor(k, 1: Degre)) / Degre
            END DO
!-- anciennement, mais coor pour ordre=3 a 6 composantes !            g = Sum(coor, 2)/Degre

            DO k = 1, Degre
               kp1 = Mod(k, Degre) + 1 !-- %%%% Modulo
               xG1 = Coor(1, k)   - g(1)
               yG1 = Coor(2, k)   - g(2)
               xG2 = Coor(1, kp1) - g(1)
               yG2 = Coor(2, kp1) - g(2)
               aire = aire + xG1 * yG2 - xG2 * yG1
               h    = Max(h, (xG1 - xG2) ** 2 + (yG1 - yG2) ** 2)
            END DO
            h    = aire / Sqrt(h) ! Vraie hauteur pour triangles et rectangles
            aire = aire * 0.5
            Mesh%VolEl(jt) = aire
            DO k = 1, Degre
               Mesh%h(NULoc(k)) = Min(Mesh%h(NULoc(k)), h)
            END DO
            SELECT CASE(Mesh%Ndegre(jt))
               CASE(3, 4) !-- triangle, quadrangle ordre 1 ou 2
                  aire = aire / Degre
                  DO k = 1, Degre
                     Mesh%Vols(NULoc(k)) = Mesh%Vols(NULoc(k)) + aire
                     Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) + Coor(:, k)
                  END DO
                  Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) / Real(Degre)
               CASE(6) !-- triangle ordre 3
                  DO k = 1, 3
                     Mesh%Vols(NULoc(k)) = Mesh%Vols(NULoc(k)) + aire / Real(6)
                     Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) + Coor(:, k)
                  END DO
                  Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) / Real(3)
                  DO k = 4, 6
                     indice  = Mesh%nu(k, jt)
!                     PRINT *, mod_name, " k==", k, "size==", Size(Mesh%Vols), "indice==", indice, "ncells==", Mesh%Ncells !-- DO KEEP IT !
                     Mesh%Vols(indice) = Mesh%Vols(indice) + aire / Real(4)
                  END DO

               CASE(9) !-- Quadrangle ordre 3
                  DO k=1,4
                     Mesh%Vols(NULoc(k)) = Mesh%Vols(NULoc(k)) + aire/16.0
                  END DO
                  DO k=5,8
                     Mesh%Vols(Mesh%nu(k,jt)) = Mesh%Vols(Mesh%nu(k,jt)) + aire/8.0
                  END DO
                  Mesh%Vols(Mesh%nu(9,jt))    = aire/4.0 ! Normalement le dofs du milieu du quadrangle n'appartient qu'� un seul �l�ment.

               CASE(10) !-- triangle ordre 4
                  DO k = 1, 3
                     Mesh%Vols(NULoc(k)) = Mesh%Vols(NULoc(k)) + aire / Real(10)
                     Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) + Coor(:, k)
                  END DO
                  Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) / Real(3)
                  DO k = 4, 10
                     indice  = Mesh%nu(k, jt)
!                     PRINT *, mod_name, " k==", k, "size==", Size(Mesh%Vols), "indice==", indice, "ncells==", Mesh%Ncells !-- DO KEEP IT !
                     Mesh%Vols(indice) = Mesh%Vols(indice) + aire / Real(10)
                  END DO
               CASE DEFAULT
                  PRINT *, mod_name, " ERREUR : degre NIP", Mesh%Ndegre(jt)
                  CALL delegate_stop()
            END SELECT

         END DO
      END SUBROUTINE GEOMVol2DGeneric

      SUBROUTINE GEOMVol2d(Mesh)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOMVol2d"
    !---------------------------------------------
    !!     role: Calcul des aires des cellules 2D et des triangles.
    !----------------------------------------------
    !!@author     Boniface Nkonga      Universite de Bordeaux 1
    !!@author     Mikael Papin         INRIA / Univ. de Bordeaux 1
    !-----------------------------------------------
    !     debut des declarations
    !-----------------------------------------------
    !.. Formal Arguments ..
         TYPE(MeshDef),  INTENT(INOUT)            :: Mesh
    !.. Local variables ..
         INTEGER, DIMENSION(: ), POINTER   :: Ndegre
         REAL,    DIMENSION(:, : ), POINTER   :: Coor
         REAL,    DIMENSION(: ), POINTER   :: airt, airs
    !.. Local Scalars ..
         INTEGER       :: is1, is2, is3, jt
         INTEGER       :: is4
         REAL          :: h, aire, x12, x13, x14, y12, y13, y14, x23, y23, x34, y34
    !
         NULLIFY( Ndegre, Coor, airt, airs)

         Ndegre   => Mesh%Ndegre
         Coor     => Mesh%Coon
         airt     => Mesh%VolEl
         airs     => Mesh%Vols
         Mesh%CentrEl(:, : ) = 0.0

         airs = 0.0

         h = 0.0
         is4 = -1

         DO jt = 1, Mesh%Nelemt
            is1 = Mesh%nu(1, jt)
            is2 = Mesh%nu(2, jt)
            is3 =  Mesh%nu(3, jt)
       !
            x12 = Coor(1, is2) - Coor(1, is1)
            y12 = Coor(2, is2) - Coor(2, is1)
       !
            x23 = Coor(1, is3) - Coor(1, is2)
            y23 = Coor(2, is3) - Coor(2, is2)

            aire = 0.5 * Abs( x12 * y23 - y12 * x23 )
            IF (Ndegre(jt) == 3) THEN
               x13 = Coor(1, is3) - Coor(1, is1)
               y13 = Coor(2, is3) - Coor(2, is1)
            ELSE IF (Ndegre(jt) == 4) THEN
               is4 = Mesh%nu(4, jt)
               x14 = Coor(1, is4) - Coor(1, is1)
               y14 = Coor(2, is4) - Coor(2, is1)
               x34 = Coor(1, is4) - Coor(1, is3)
               y34 = Coor(2, is4) - Coor(2, is3)

               aire = aire + 0.5 * Abs( x14 * y34 - y14 * x34 )
               h  = Max(h, x34 * x34 + y34 * y34 )
               h  = Max(h, x14 * x14 + y14 * y14 )
            END IF
       !
            airt(jt) = aire

            aire = aire / Real(Ndegre(jt))
            airs(is1) = airs(is1) + aire
            airs(is2) = airs(is2) + aire
            airs(is3) = airs(is3) + aire
            IF (Ndegre(jt) == 4) THEN
               airs(is4) = airs(is4) + aire
            END IF
         END DO
      END SUBROUTINE GEOMVol2d

    !!@author     Boniface Nkonga      Universite de Bordeaux 1
    !!@author     Mikael Papin         INRIA / Univ. de Bordeaux 1
    !---------------------------------------------
    !!     role: Calcul des volumes des t�trahedres et des cellules.
    !----------------------------------------------
!!-- volume des t�tra�dres; appelle VolTet dans une boucle sur les �l�ments
      SUBROUTINE GEOMvol3d(Mesh) ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOMvol3d"
    !-----------------------------------------------
    !     debut des declarations
    !-----------------------------------------------
         TYPE(MeshDef),    INTENT(INOUT)  :: Mesh
    !
    ! Variables Locales Scalaires
    ! -------------------------------------
         INTEGER, SAVE, DIMENSION(6), TARGET   :: nHexa1 = (/ 1, 2, 3, 1, 1, 5 /)
         INTEGER, SAVE, DIMENSION(6), TARGET   :: nHexa2 = (/ 2, 3, 4, 5, 4, 6 /)
         INTEGER, SAVE, DIMENSION(6), TARGET   :: nHexa3 = (/ 6, 7, 8, 8, 3, 7 /)
         INTEGER, SAVE, DIMENSION(6), TARGET   :: nHexa4 = (/ 5, 6, 7, 4, 2, 8 /)
         INTEGER :: is1, is2, is3, is4, isf1, isf2, isf3, isf4, is_min
         INTEGER ::  ic, Ncut, Ntyp, Nface
         INTEGER                 :: k, is, jt
         REAL                    :: Volt
         REAL,    DIMENSION(3)   :: Xg
         REAL,    DIMENSION(3, 4) :: VPTet

         isf1 = -2000000000 !-- pour faire taire l'analyse de flot
         isf2 = -2000000000
         isf3 = -2000000000
         isf4 = -2000000000

#ifdef ORDRE_ELEVE
         ALLOCATE(Mesh%Vols(Mesh%NCells))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "mesh%vols", Size(Mesh%Vols))
         END IF
#endif

         Mesh%Vols(: ) = 0.0
         Mesh%CentrEl(:, : ) = 0.0
         Ntyp         = Mesh%Ndegre(1)
         ! On suppose ici que le maillage n'est pas hybride
         IF (Maxval(Mesh%Ndegre) /= Minval(Mesh%Ndegre) ) THEN
            PRINT *, mod_name, " ERREUR :  Maillage hybride non prevu"
            CALL delegate_stop()
         END IF
    ! boucle sur les �l�ments
         SELECT CASE(Ntyp)
            CASE(4) !-- tetraedres P1
               DO  jt = 1,  Mesh%Nelemt
                  DO  k = 1,  Ntyp
                     is           = Mesh%nu(k, jt)
                     VpTet(:, k)  = Mesh%Coon(:, is)
                     Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) + Mesh%Coon(:, is)
                  END DO
                  Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) / Real(Ntyp)
                  Volt            = VolTet(VpTet)
                  Mesh%VolEl(jt)  = Volt
                  DO  k = 1,  Ntyp
                     is            = Mesh%nu(k, jt)
                     Mesh%Vols(is) = Mesh%Vols(is)  +  Volt / Ntyp
                  END DO
               END DO
            CASE(10) !-- tetraedres P2
               DO  jt = 1,  Mesh%Nelemt
                  DO  k = 1,  Mesh%NSommets(jt)
                     is           = Mesh%nu(k, jt)
                     VpTet(:, k)  = Mesh%Coon(:, is)
                     Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) + Mesh%Coon(:, is)
                  END DO
                  Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) / Real(4)
                  Volt            = VolTet(VpTet)
                  Mesh%VolEl(jt)  = Volt
                  DO  k = 1,  Ntyp
                     is            = Mesh%nu(k, jt)
                     IF (k <= 4) THEN
                        Mesh%Vols(is) = Mesh%Vols(is)  +  Volt / 32.0
                     ELSE
                        Mesh%Vols(is) = Mesh%Vols(is)  +  7.0 * Volt / 48.0
                     END IF
                  END DO
               END DO

            CASE(8) !-- hexaeadres P1
               Nface = 6
               Ncut = 2
               DO  jt = 1,  Mesh%Nelemt
                  Xg = 0.0
                  DO  k = 1,  Ntyp
                     is   = Mesh%nu(k, jt)
                     Xg   = Xg + Mesh%Coon(:, is)
                  END DO
                  Xg  = Xg / Ntyp
                  Mesh%CentrEl(:, jt) = Xg
                  Volt = 0.0
                  DO  k = 1,  Nface
                     is1    = Mesh%Nu( nHexa1(k), jt)
                     is2    = Mesh%Nu( nHexa2(k), jt)
                     is3    = Mesh%Nu( nHexa3(k), jt)
                     is4    = Mesh%Nu( nHexa4(k), jt)

                     is_min = Min(is1, is2, is3, is4)

                     IF (is_min ==  is1 ) THEN
                        isf1 = is1
                        isf2 = is2
                        isf3 = is3
                        isf4 = is4
                     END IF
                     IF (is_min ==  is2 ) THEN
                        isf1 = is2
                        isf2 = is3
                        isf3 = is4
                        isf4 = is1
                     END IF
                     IF (is_min ==  is3 ) THEN
                        isf1 = is3
                        isf2 = is4
                        isf3 = is1
                        isf4 = is2
                     END IF
                     IF (is_min ==  is4 ) THEN
                        isf1 = is4
                        isf2 = is1
                        isf3 = is2
                        isf4 = is3
                     END IF
                     DO ic = 1, Ncut
                        IF (ic == 1 ) THEN
                           VpTet(:, 1) = Mesh%Coon(:, isf1)
                           VpTet(:, 2) = Mesh%Coon(:, isf2)
                           VpTet(:, 3) = Mesh%Coon(:, isf3)
                           VpTet(:, 4) = Xg
                        END IF
                        IF (ic == 2 ) THEN
                           VpTet(:, 1) = Mesh%Coon(:, isf1)
                           VpTet(:, 2) = Mesh%Coon(:, isf3)
                           VpTet(:, 3) = Mesh%Coon(:, isf4)
                           VpTet(:, 4) = Xg
                        END IF
                        Volt            = Volt + Abs(VolTet(VpTet))
                     END DO
                  END DO

                  Mesh%VolEl(jt)  = Volt

                  DO  k = 1,  Ntyp
                     is            = Mesh%nu(k, jt)
                     Mesh%Vols(is) = Mesh%Vols(is)  +  Volt / Ntyp
                  END DO
               END DO
            CASE DEFAULT
               PRINT *, mod_name, " ERREUR : Cas bizzare dans GeomParam.f90/GEOMvol3d, Ntyp : ", Ntyp
               CALL delegate_stop()

         END SELECT
      END SUBROUTINE GEOMvol3d




      SUBROUTINE  Hauteurs3d(Mesh)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "Hauteurs3d"
         TYPE(MeshDef), INTENT(INOUT)             :: Mesh
         INTEGER :: jt, is, is1, is2, is3,  k
         INTEGER, PARAMETER, DIMENSION(4) :: s1 = (/ 2, 1, 1, 1 /)
         INTEGER, PARAMETER, DIMENSION(4) :: s2 = (/ 3, 3, 2, 2 /)
         INTEGER, PARAMETER, DIMENSION(4) :: s3 = (/ 4, 4, 4, 3 /)
         REAL               :: h
         REAL, DIMENSION(3) :: x, x1, x2, x3, v, v1, v2, Vt


         Mesh%h = 1.0d+12

         DO jt = 1, Mesh%Nelemt

            DO k = 1, 4
               is  = Mesh%nu(k, jt)
               is1 = Mesh%nu(s1(k), jt)
               is2 = Mesh%nu(s2(k), jt)
               is3 = Mesh%nu(s3(k), jt)

               x   = Mesh%Coor(:, is)
               x1  = Mesh%Coor(:, is1)
               x2  = Mesh%Coor(:, is2)
               x3  = Mesh%Coor(:, is3)

               v   = x  - x1
               v1  = (x2 - x1) / Sqrt(Sum((x2 - x1) ** 2))
               v2  = (x3 - x1) / Sqrt(Sum((x3 - x1) ** 2))

               Vt(1) =   v1(2) * v2(3) - v1(3) * v2(2)
               Vt(2) =   v1(3) * v2(1) - v1(1) * v2(3)
               Vt(3) =   v1(1) * v2(2) - v1(2) * v2(1)


               h   = Abs( Sum( Vt * v ) )
          !
               Mesh%h(is) = Min( Mesh%h(is), h )
               Mesh%h(is1) = Min( Mesh%h(is1), h )
               Mesh%h(is2) = Min( Mesh%h(is2), h )
               Mesh%h(is3) = Min( Mesh%h(is3), h )
            END DO

         END DO

      END SUBROUTINE Hauteurs3d
      SUBROUTINE  HauteursCelCentered3d(Mesh)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "HauteursCelCentered3d"
         TYPE(MeshDef), INTENT(INOUT)             :: Mesh
         INTEGER :: jt, is, is1, is2, is3,  k, Ntyp
         INTEGER, DIMENSION(: ), POINTER  :: s0, s1, s2, s3

         INTEGER, SAVE, DIMENSION(4), TARGET :: Tets0 = (/ 1, 2, 3, 4 /)
         INTEGER, SAVE, DIMENSION(4), TARGET :: Tets1 = (/ 2, 1, 1, 1 /)
         INTEGER, SAVE, DIMENSION(4), TARGET :: Tets2 = (/ 3, 3, 2, 2 /)
         INTEGER, SAVE, DIMENSION(4), TARGET :: Tets3 = (/ 4, 4, 4, 3 /)

         INTEGER, SAVE, DIMENSION(4), TARGET :: Hexs0 = (/ 1, 3, 5, 8 /)
         INTEGER, SAVE, DIMENSION(4), TARGET :: Hexs1 = (/ 5, 5, 1, 1 /)
         INTEGER, SAVE, DIMENSION(4), TARGET :: Hexs2 = (/ 6, 6, 2, 2 /)
         INTEGER, SAVE, DIMENSION(4), TARGET :: Hexs3 = (/ 7, 7, 4, 3 /)
         REAL               :: h
         REAL, DIMENSION(3) :: x, x1, x2, x3, v, v1, v2, Vt

         Mesh%h = 1.0d+12
         IF (Mesh%Ndegre(1) == 8 ) THEN
            s0 => Hexs0
            s1 => Hexs1
            s2 => Hexs2
            s3 => Hexs3
            Ntyp = 4 !-- Size(Hexs0)
         ELSE
            s0 => Tets0
            s1 => Tets1
            s2 => Tets2
            s3 => Tets3
            Ntyp = 4 !-- Size(Tets0)
         END IF

         DO jt = 1, Mesh%Nelemt

            DO k = 1, Ntyp
               is  = Mesh%nu(s0(k), jt)
               is1 = Mesh%nu(s1(k), jt)
               is2 = Mesh%nu(s2(k), jt)
               is3 = Mesh%nu(s3(k), jt)

               x   = Mesh%Coor(:, is)
               x1  = Mesh%Coor(:, is1)
               x2  = Mesh%Coor(:, is2)
               x3  = Mesh%Coor(:, is3)

               v   = x  - x1
               v1  = (x2 - x1) / Sqrt(Sum((x2 - x1) ** 2))
               v2  = (x3 - x1) / Sqrt(Sum((x3 - x1) ** 2))

               Vt(1) =   v1(2) * v2(3) - v1(3) * v2(2)
               Vt(2) =   v1(3) * v2(1) - v1(1) * v2(3)
               Vt(3) =   v1(1) * v2(2) - v1(2) * v2(1)


               h   =  Abs( Sum( Vt * v ) )
           !
               IF (h > 1.0e-09) THEN
                  Mesh%h(jt) = Min( Mesh%h(jt), h )
               END IF
            END DO
            Mesh%h(jt) = 0.5 * (  0.5 * Mesh%h(jt) + 0.1 * (Mesh%VolEl(jt) ** (1.0 / 3.0)) )
         END DO


      END SUBROUTINE HauteursCelCentered3d


!!-- role : calcul du volume d'un tetraedre

    !!-- Par convention VPt(1:3, i) = X_{i}
    !!-- et la suite ( X_{1}, X_{2}, X_{3}, X_{4} ) est ORIENTEE!!!
      FUNCTION VolTet(VPt) RESULT(volume)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "VolTet"
         REAL, DIMENSION(:, : ), INTENT(IN) :: VPt
         REAL :: volume

         REAL :: x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4
         REAL :: z12, z13, z14, z23, z24, z34, b1, b2, b3, b4

         x1 = VPt(1, 1)
         x2 = VPt(1, 2)
         x3 = VPt(1, 3)
         x4 = VPt(1, 4)
         y1 = VPt(2, 1)
         y2 = VPt(2, 2)
         y3 = VPt(2, 3)
         y4 = VPt(2, 4)
         z1 = VPt(3, 1)
         z2 = VPt(3, 2)
         z3 = VPt(3, 3)
         z4 = VPt(3, 4)

         z12  = z2 - z1
         z13  = z3 - z1
         z14  = z4 - z1
         z23  = z3 - z2
         z24  = z4 - z2
         z34  = z4 - z3
    !
         b1 = + y2 * z34 - y3 * z24 + y4 * z23
         b2 = - y1 * z34 + y3 * z14 - y4 * z13
         b3 = + y1 * z24 - y2 * z14 + y4 * z12
         b4 = - y1 * z23 + y2 * z13 - y3 * z12

         volume = ( x1 * b1 + x2 * b2 + x3 * b3 + x4 * b4 ) / 6.0

      END FUNCTION VolTet

!!-- Calcul des barycentres des mailles Vertex Centered
      SUBROUTINE Geom2DAxisym(Mesh, IAxi)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "Geom2DAxisym"
    ! ****************
    !   declarations
    ! ***************
    ! variables d'appel
         TYPE(MeshDef), INTENT(INOUT)              :: Mesh
         INTEGER, INTENT(IN), OPTIONAL :: IAxi
         REAL,    DIMENSION(1: Mesh%Ndim, 1: Mesh%NdegreMax) :: Coor
         INTEGER, DIMENSION(1: Mesh%NdegreMax)             :: NULoc
         REAL, DIMENSION(1: Mesh%Ndim) :: g
         REAL :: y0, yp, ym
         INTEGER :: is, jt, ism, isp, Degre, idir
    ! Ce qui suit suppose que l'axe de sym�trie est l'axe y=0.0
    ! Mesh%Rbar(is) est la composante en y du barycentre des cellules.

         idir = 2
         IF (Present( IAxi) ) THEN
            idir = IAxi
         END IF

         Mesh%Gs = 0.0
    ! Calcul des barycentres des mailles Vertex Centered
         DO jt = 1, Mesh%Nelemt
            Coor    = 0.0
            Degre = Mesh%Ndegre(jt)
       ! Barycentre de la cellule
            g = 0.0
            DO is = 1, Degre
               NULoc(is)  = Mesh%nu(is, jt)
               Coor(:, is) = Mesh%Coon(:, NULoc(is))
               Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) + Coor(:, is)
            END DO
            Mesh%CentrEl(:, jt) = Mesh%CentrEl(:, jt) / Real(Degre)
            g = Sum(Coor, 2) / Degre

            DO is = 1, Degre
          ! (M(is) + M(is-1) + G + S) / 4 *V/Mesh%Ndegre
               isp = Mod(is, Degre) + 1 !-- %%%% Modulo
               ism = Mod(is + Degre-2, Degre) + 1 !-- %%%% Modulo
               ym  = Coor(idir, ism)
               y0  = Coor(idir, is)
               yp  = Coor(idir, isp)
               ym  = (ym + y0) * 0.5
               yp  = (yp + y0) * 0.5

               Mesh%Gs(NULoc(is)) = Mesh%Gs(NULoc(is)) + &
               & Mesh%VolEl(jt) / Mesh%Ndegre(jt) / Mesh%Vols(NULoc(is)) * (g(idir) + y0 + yp + ym ) * 0.25
            END DO
         END DO
      END SUBROUTINE Geom2DAxisym


    !     -----------------------------------------------
    !!@author     Boniface Nkonga      Universite de Bordeaux 1
    !     -----------------------------------------------

!!-- role: Construction des normales associ�es aux segments Vertex Centered. Maillage mobile.

!!-- Pour chaque segment
!!-- On commence par calculer le centre du segment
!!--   Pour les 2 �l�ments qui bordent le segment courant
!!--     On calcule le centre de gravit� de l'�l�ment;
!!-- si cela est dans la boucle principale, cela n'est � faire qu'une fois si le maillage n'est pas mobile %%
!!--     On calcule le vecteur Nij diff�rence entre le C.G. et le milieu du segment courant
!!--     La somme des Nij pour les deux �l�ments donne le vecteur normal
      SUBROUTINE GEOMMvvnoVertexCentered2d(Don, Var, Mesh, Dt)  ! OUT

         CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOMMvvnoVertexCentered2d"
    !     -----------------------------------------------
    !
    !--------------------------------------------------------------------
    !                 Specifications et Declarations
    !--------------------------------------------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(Donnees),   INTENT(IN) :: Don
         TYPE(MeshDef),   INTENT(INOUT) :: Mesh
         TYPE(Variables), INTENT(IN)    :: Var
         REAL, OPTIONAL, INTENT(IN) :: Dt
    ! Variables locales scalaires
    !----------------------------
         INTEGER :: j, is1, is2, jdeg
         INTEGER :: js1, js2, js3, js4, iseg, ifr,  jt, k, k1
         INTEGER :: Nseg, NsegFr
         REAL    :: xija, yija, xijn, yijn, Dtl
         REAL    :: xGta, yGta, xGtn, yGtn
    ! Variables locales tableaux
    !----------------------------
         REAL,    DIMENSION(2)   :: Nij
         REAL,    DIMENSION(2)   :: KapGt, Kapij, kapGij
         INTEGER, DIMENSION(: ), POINTER :: Ndegre, NumFac
         INTEGER, DIMENSION(:, : ), POINTER :: nu, nubo, nutv, NsfacFr
         REAL,    DIMENSION(:, : ), POINTER :: Coor, Coon, Vno
         REAL,    DIMENSION(: ), POINTER :: sigma
         REAL,    DIMENSION(:, : ), POINTER :: Alpha
         REAL,    DIMENSION(:, :, : ), POINTER :: Beta

         xGta = 0.0
         yGta = 0.0
         xGtn = 0.0
         yGtn = 0.0

#ifdef DEBUG
         IF (.NOT. Present(Dt)) THEN
            WRITE(6, *) " passage initial dans ", mod_name
         END IF
#endif

#ifdef ORDRE_ELEVE
         Ndegre   => Mesh%NSommets
#else
         Ndegre   => Mesh%Ndegre
#endif
         nu       => Mesh%nu
         Coor     => Mesh%Coor
         Coon     => Mesh%Coon
         NsfacFr  => Mesh%NsfacFr
         NumFac   => Mesh%NumFac
         NsegFr   = Mesh%NFacFr

         Nseg     = Mesh%Nsegmt
         nutv     => Mesh%nutv
         nubo     => Mesh%nubo
         Vno      => Mesh%Vno
         Vno      = 0.0

         IF (.NOT. Present(Dt) ) THEN
            WRITE(6, *) ' Dans ', mod_name
            WRITE(6, *) ' Nseg ==  ',  Nseg, Size(nutv, 2)
            Dtl  = Var%Dt !-- CCE 2007/10/08
         ELSE
            Dtl  = Dt
         END IF

         IF (Don%Deformable) THEN
            sigma    => Mesh%sigma
            sigma    = 0.0
            Mesh%SigmaFr = 0.0
         END IF

         DO iseg = 1, Nseg
            is1 = nubo(1, iseg)
            is2 = nubo(2, iseg)

            xija = (Coor(1, is1) + Coor(1, is2) ) / 2.0
            yija = (Coor(2, is1) + Coor(2, is2) ) / 2.0

            xijn = (Coon(1, is1) + Coon(1, is2) ) / 2.0
            yijn = (Coon(2, is1) + Coon(2, is2) ) / 2.0

            DO j = 1, 2
               jt  = nutv(j, iseg)
               IF ( jt == 0 ) THEN
                  CYCLE
               END IF
               js1 = nu(1, jt)
               js2 = nu(2, jt)
               js3 = nu(3, jt)

               jdeg = Ndegre(jt)
               SELECT CASE (jdeg)
                  CASE(4) !-- jdeg==4
                     js4 = nu(4, jt)
                     xGta  = (   Coor(1, js1) + Coor(1, js2)&
                     &    + Coor(1, js3) + Coor(1, js4)  ) / 4.0
                     yGta  = (   Coor(2, js1) + Coor(2, js2)&
                     &    + Coor(2, js3) + Coor(2, js4) ) / 4.0
                     xGtn  = (   Coon(1, js1) + Coon(1, js2)&
                     &    + Coon(1, js3) + Coon(1, js4)  ) / 4.0
                     yGtn  = (   Coon(2, js1) + Coon(2, js2)&
                     &    + Coon(2, js3) + Coon(2, js4) ) / 4.0
             !
                  CASE(3) !-- jdeg==3
                     xGta  = (   Coor(1, js1) + Coor(1, js2)&
                     &    + Coor(1, js3) ) / 3.0
                     yGta  = (   Coor(2, js1) + Coor(2, js2)&
                     &    + Coor(2, js3) ) / 3.0

                     xGtn  = (   Coon(1, js1) + Coon(1, js2)&
                     &    + Coon(1, js3)  ) / 3.0
                     yGtn  = (   Coon(2, js1) + Coon(2, js2)&
                     &    + Coon(2, js3)  ) / 3.0
                  CASE DEFAULT
                     PRINT *, mod_name, " ERREUR : CAS non prevu jdeg==", jdeg
                     CALL delegate_stop()
               END SELECT

               Nij  =    0.5 * (/ (yGta - yija), - (xGta - xija) /) &
               &  + 0.5 * (/ (yGtn - yijn), - (xGtn - xijn) /)


               DO k = 1, jdeg
                  js1   = nu(k, jt)
                  k1    = Mod(k, jdeg) + 1 !-- %%%% Modulo
                  js2   = nu(k1, jt)
                  IF (js1 == is2 .AND. js2 == is1) THEN
                     Nij(1: 2)   = - Nij(1: 2)
                     EXIT
                  END IF
               END DO

               IF (Don%Deformable) THEN
                  KapGt   = (/ (xGtn - xGta), (yGtn - yGta)  /) / Dtl
                  Kapij   = (/ (xijn - xija), (yijn - yija)  /) / Dtl
                  kapGij  =  0.5 * ( KapGt + Kapij )

                  sigma(iseg)      = sigma(iseg)     + Sum(Nij * kapGij)
               END IF

               Vno(1: 2, iseg)    = Vno(1: 2, iseg)   + Nij(1: 2)
               Mesh%norme(iseg) = Sqrt(Sum(Vno(:, iseg) ** 2)) !-- VertexCentered2D
!               PRINT *, iseg, "VC2D/norme==", Mesh%Norme(iseg), Mesh%Vno(:, iseg)

            END DO
         END DO


         DO ifr = 1, NsegFr
            iseg   = NumFac(ifr)
            jt     = nutv(2, iseg)
            IF (jt /= 0 ) THEN
               CYCLE
            END IF

            is1    = NsfacFr(1, ifr)
            is2    = NsfacFr(2, ifr)
       !
            xija   = Coor(1, is1)
            yija   = Coor(2, is1)

            xijn   = Coon(1, is1)
            yijn   = Coon(2, is1)

            xGta   = Coor(1, is2)
            yGta   = Coor(2, is2)

            xGtn   = Coon(1, is2)
            yGtn   = Coon(2, is2)
       !

            Nij  =    0.5 * (/ yGta - yija, - xGta + xija /) &
            &  + 0.5 * (/ yGtn - yijn, - xGtn + xijn /)


            jt     = nutv(1, iseg)
            jdeg   = Ndegre(jt)
       !
            DO k = 1, jdeg
               js1 = nu(k, jt)
               k1  = Mod(k, jdeg) + 1 !-- %%%% Modulo
               js2 = nu(k1, jt)
               IF (js1 == is2 .AND. js2 == is1 ) THEN
                  Nij(1: 2)   = - Nij(1: 2)
                  EXIT
               END IF
            END DO
       !

            IF (Don%Deformable) THEN
               KapGt   = (/ xGtn - xGta, yGtn - yGta  /) / Dtl
               Kapij   = (/ xijn - xija, yijn - yija  /) / Dtl
               kapGij  =  0.5 * ( KapGt + Kapij )

               Mesh%SigmaFr(ifr)      = Sum(Nij * kapGij)
            END IF
            Mesh%vnoFr(1: 2, ifr)    = Nij(1: 2)
       !
         END DO

    !
#ifdef DEBUG
         WRITE(6, *) ' Fin de  CellVertexMvvno '
         IF (NsegFr > 0) THEN
            WRITE(6, *) " Int Contour x = ", Sum( Mesh%vnoFr(1, : ))
            WRITE(6, *) " Int Contour y = ", Sum( Mesh%vnoFr(2, : ))
         END IF
         CALL verif()
#endif

         NULLIFY( Ndegre, nu, Coor, Coon, nubo, nutv, Vno, sigma )
         NULLIFY( Alpha, Beta )

      CONTAINS
!!-- Sert � la mise au point, calcule SNvol (comme somme de Mesh%vno + 0.5 * Mesh%vnoFr) et l'imprime
         SUBROUTINE verif()
            CHARACTER(LEN = *), PARAMETER :: mod_name = "verif"
            !-- REAL, DIMENSION( 2, SIZE(Mesh%COOR, DIM=2) ) :: SNvol
            REAL, DIMENSION(:, : ), ALLOCATABLE             :: SNvol
            INTEGER :: iseg, ifr

            ALLOCATE( SNvol( 1: 2, Size(Mesh%Coor, dim = 2)))
            IF (see_alloc) THEN
               CALL alloc_allocate(mod_name, "snvol", Size(SNvol))
            END IF

            SNvol = 0.0
            DO iseg = 1, Nseg
               is1 = nubo(1, iseg)
               is2 = nubo(2, iseg)
               SNvol(1: 2, is1) = SNvol(1: 2, is1) + Vno(1: 2, iseg)
               SNvol(1: 2, is2) = SNvol(1: 2, is2) - Vno(1: 2, iseg)
            END DO

            DO ifr = 1, NsegFr
         !
               iseg   = NumFac(ifr)
               jt     = nutv(2, iseg)
               IF (jt /= 0 ) THEN
                  CYCLE
               END IF
               is1    = NsfacFr(1, ifr)
               is2    = NsfacFr(2, ifr)
               SNvol(1: 2, is1) = SNvol(1: 2, is1) + 0.5 * Mesh%vnoFr(1: 2, ifr)
               SNvol(1: 2, is2) = SNvol(1: 2, is2) + 0.5 * Mesh%vnoFr(1: 2, ifr)
            END DO

#ifdef DEBUG
            WRITE(6, * ) mod_name, " Min val sur les cellules ", Minval(SNvol)
            WRITE(6, * ) mod_name, " Max val sur les cellules ", Maxval(SNvol)
#endif

            DEALLOCATE( SNvol )
            IF (see_alloc) THEN
               CALL alloc_deallocate(mod_name, "snvol")
            END IF
         END SUBROUTINE verif

      END SUBROUTINE GEOMMvvnoVertexCentered2d


    !     -----------------------------------------------
    !!@author     Boniface Nkonga      Universite de Bordeaux 1
    !     -----------------------------------------------
!!-- role : calcul des normales aux segments et des deplacements: cas deformable
      SUBROUTINE GEOMMvvnoVertexCentered3d(Don, Var, Mesh, Dt) ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "GEOMMvvnoVertexCentered3d"

    !--------------------------------------------------------------------
    !                 Specifications et Declarations
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
    !
    !
    ! Modules utilises
    ! ----------------

    !
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    ! variables d'appel
         TYPE(Donnees), INTENT(IN)    :: Don
         TYPE(MeshDef), INTENT(INOUT)   :: Mesh
         TYPE(Variables), INTENT(IN)    :: Var
         REAL, INTENT(IN), OPTIONAL    :: Dt

    ! Variables Locales Scalaires
    ! -------------------------------------
         LOGICAL     :: Terr
         INTEGER     :: iseg, jt, is1, is2, is3, is4
         INTEGER     :: nub1, nub2, k, kv, ic3, in, ic, Nt, Npt
         REAL        ::  eps, Dtl

    ! Variables Locales tableaux
    ! -------------------------------------
         INTEGER, PARAMETER, DIMENSION(6)     :: s1 = (/ 1, 1, 1, 2, 2, 3 /)
         INTEGER, PARAMETER, DIMENSION(6)     :: s2 = (/ 2, 3, 4, 3, 4, 4 /)
         INTEGER, PARAMETER, DIMENSION(6)     :: s3 = (/ 3, 4, 2, 1, 3, 1 /)
         INTEGER, PARAMETER, DIMENSION(6)     :: s4 = (/ 4, 2, 3, 4, 1, 2 /)

         REAL,    DIMENSION(3)     :: Kappag, eta
         REAL,    DIMENSION(3, 2)   :: v12, v13, v14, v23, v24, v34
         REAL,    DIMENSION(3, 4, 6) :: GraPhi
         REAL,    DIMENSION(3, 4, 2) :: VPt
         REAL                      :: norme
    !
    ! Fonction itreseques
    !--------------------

    !
    !--------------------------------------------------------------------
    !                Partie executable de la procedure
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
#ifdef DEBUG
         IF (.NOT. Present(Dt)) THEN
            WRITE(6, *) mod_name, " : passage initial, Size(vno)==", Size(Mesh%Vno)
         END IF
#endif
         eps        = 1.0 / 24.0
         Nt         = Mesh%Nelemt
         Mesh%Vno   = 0.0

         IF (.NOT. Present(Dt) ) THEN
            Dtl  = Var%Dt !-- CCE 2007/10/08
         ELSE
            Dtl  = Dt
         END IF
    !
    !     -- boucle / aretes de chaque tetraedre
         DO  jt = 1, Nt
       !
       ! calcul des derivees de fonction de base
       !
            is1 = Mesh%nu(1, jt)
            is2 = Mesh%nu(2, jt)
            is3 = Mesh%nu(3, jt)
            is4 = Mesh%nu(4, jt)
       !
            VPt(:, 1, 1) = Mesh%Coor(:, is1)
            VPt(:, 2, 1) = Mesh%Coor(:, is2)
            VPt(:, 3, 1) = Mesh%Coor(:, is3)
            VPt(:, 4, 1) = Mesh%Coor(:, is4)
       !
            VPt(:, 1, 2) = Mesh%Coon(:, is1)
            VPt(:, 2, 2) = Mesh%Coon(:, is2)
            VPt(:, 3, 2) = Mesh%Coon(:, is3)
            VPt(:, 4, 2) = Mesh%Coon(:, is4)

            DO  ic = 1, 2
               v12(:, ic)   = VPt(:, 2, ic) - VPt(:, 1, ic)
               v13(:, ic)   = VPt(:, 3, ic) - VPt(:, 1, ic)
               v14(:, ic)   = VPt(:, 4, ic) - VPt(:, 1, ic)
               v23(:, ic)   = VPt(:, 3, ic) - VPt(:, 2, ic)
               v24(:, ic)   = VPt(:, 4, ic) - VPt(:, 2, ic)
               v34(:, ic)   = VPt(:, 4, ic) - VPt(:, 3, ic)
            END DO !-- ic = 1, 2

            ic3 = -1

            DO  in = 1, 4
               SELECT CASE(in)
                  CASE(1)
                     ic  = 1
                     ic3 = 1
                  CASE(2)
                     ic  = 1
                     ic3 = 2
                  CASE(3)
                     ic  = 2
                     ic3 = 1
                  CASE(4)
                     ic  = 2
                     ic3 = 2
               END SELECT

               GraPhi(1, in, 1) =   (  v14(3, ic3) + v24(3, ic3) ) * v34(2, ic)&
               &           - (  v14(2, ic3) + v24(2, ic3) ) * v34(3, ic)

               GraPhi(1, in, 2) =   ( - v12(3, ic3) + v23(3, ic3) ) * v24(2, ic)&
               &           - ( - v12(2, ic3) + v23(2, ic3) ) * v24(3, ic)

               GraPhi(1, in, 3) = - ( - v13(3, ic3) + v34(3, ic3) ) * v23(2, ic)&
               &           + ( - v13(2, ic3) + v34(2, ic3) ) * v23(3, ic)

               GraPhi(1, in, 4) = + (  v24(3, ic3) + v34(3, ic3) ) * v14(2, ic)&
               &           - (  v24(2, ic3) + v34(2, ic3) ) * v14(3, ic)

               GraPhi(1, in, 5) =   (  v12(3, ic3) + v14(3, ic3) ) * v13(2, ic)&
               &           - (  v12(2, ic3) + v14(2, ic3) ) * v13(3, ic)

               GraPhi(1, in, 6) = - (  v23(3, ic3) + v24(3, ic3) ) * v12(2, ic)&
               &           + (  v23(2, ic3) + v24(2, ic3) ) * v12(3, ic)

               GraPhi(2, in, 1) =   (   v14(1, ic3) + v24(1, ic3) ) * v34(3, ic)&
               &           - (   v14(3, ic3) + v24(3, ic3) ) * v34(1, ic)

               GraPhi(2, in, 2) =   ( - v12(1, ic3) + v23(1, ic3) ) * v24(3, ic)&
               &           - ( - v12(3, ic3) + v23(3, ic3) ) * v24(1, ic)

               GraPhi(2, in, 3) = - ( - v13(1, ic3) + v34(1, ic3) ) * v23(3, ic)&
               &           + ( - v13(3, ic3) + v34(3, ic3) ) * v23(1, ic)

               GraPhi(2, in, 4) =   (   v24(1, ic3) + v34(1, ic3) ) * v14(3, ic)&
               &           - (   v24(3, ic3) + v34(3, ic3) ) * v14(1, ic)

               GraPhi(2, in, 5) =   (   v12(1, ic3) + v14(1, ic3) ) * v13(3, ic)&
               &           - (   v12(3, ic3) + v14(3, ic3) ) * v13(1, ic)

               GraPhi(2, in, 6) = - (   v23(1, ic3) + v24(1, ic3) ) * v12(3, ic)&
               &           + (   v23(3, ic3) + v24(3, ic3) ) * v12(1, ic)

               GraPhi(3, in, 1) =   (   v14(2, ic3) + v24(2, ic3) ) * v34(1, ic)&
               &           - (   v14(1, ic3) + v24(1, ic3) ) * v34(2, ic)

               GraPhi(3, in, 2) =   ( - v12(2, ic3) + v23(2, ic3) ) * v24(1, ic)&
               &           - ( - v12(1, ic3) + v23(1, ic3) ) * v24(2, ic)

               GraPhi(3, in, 3) = - ( - v13(2, ic3) + v34(2, ic3) ) * v23(1, ic)&
               &           + ( - v13(1, ic3) + v34(1, ic3) ) * v23(2, ic)

               GraPhi(3, in, 4) =   (   v24(2, ic3) + v34(2, ic3) ) * v14(1, ic)&
               &           - (   v24(1, ic3) + v34(1, ic3) ) * v14(2, ic)

               GraPhi(3, in, 5) =   (   v12(2, ic3) + v14(2, ic3) ) * v13(1, ic)&
               &           - (   v12(1, ic3) + v14(1, ic3) ) * v13(2, ic)

               GraPhi(3, in, 6) = - (   v23(2, ic3) + v24(2, ic3) ) * v12(1, ic)&
               &           + (   v23(1, ic3) + v24(1, ic3) ) * v12(2, ic)

            END DO !-- in = 1, 4

            DO k = 1, 6
               is1 = Mesh%nu(s1(k), jt)
               is2 = Mesh%nu(s2(k), jt)

          ! Vitesse de l'interface
          ! associee au segment [is1, is2] et a l'element jt
               Kappag(: ) = ( (  VPt(:, s1(k), 2) - VPt(:, s1(k), 1) &
               &        + VPt(:, s2(k), 2) - VPt(:, s2(k), 1) ) * 13.0  &
               &     + (  VPt(:, s3(k), 2) - VPt(:, s3(k), 1) &
               &        + VPt(:, s4(k), 2) - VPt(:, s4(k), 1) ) * 5.0 &
               &              ) / (36.0)
          !
          !     numero segment [is1, is2] ?
               Terr   = .TRUE.
               Npt    = 0
!!-- recherche de la position du segment dans la liste
               iseg = -1
               DO  kv = Mesh%JPosi(is1), Mesh%JPosi(is1 + 1) - 1
                  iseg = Mesh%JvSeg(kv)
                  nub1 = Mesh%nubo(1, iseg)
                  nub2 = Mesh%nubo(2, iseg)
                  IF (is1 == nub1 .AND. is2 == nub2) THEN
                     Terr    = .FALSE.
                     eps     = Abs(eps)
                     EXIT
                  ELSE
                     IF (is1 == nub2 .AND. is2 == nub1) THEN
                        Terr    = .FALSE.
                        eps     = - Abs(eps)
                        Npt     =  Npt + 1
                        EXIT
                     END IF
                  END IF
               END DO !-- kv
               IF (iseg < 0) THEN
                  PRINT *, mod_name, " : ERREUR : segment non trouve"
                  CALL delegate_stop()
               END IF

               IF (Terr) THEN
                  DO kv = 1, 5
                     WRITE(message_log, *) mod_name, ' ERREUR ', is1, is2, nub1, nub2
                     CALL log_record(message_log)
                  END DO !-- kv
               END IF

               eta(: ) = eps * ( GraPhi(:, 1, k) + GraPhi(:, 4, k)&
               &        + 0.5 * ( GraPhi(:, 2, k) + GraPhi(:, 3, k)) ) / 3.0
          !
               Mesh%Vno(:, iseg)  = Mesh%Vno(:, iseg)   - eta(: )!-- accumulation, on ne calcule pas encore la norme
               IF (Don%Deformable) THEN
                  Mesh%sigma(iseg)  = Mesh%sigma(iseg)   - Sum(eta * Kappag )
               END IF

            END DO !-- k = 1, 6
         END DO !-- boucle sur jt = 1, Nelemt
         eps = 1.0e-16
         DO iseg = 1, Mesh%Nsegmt
            IF (Abs(Mesh%Vno(1, iseg))  <  eps .AND. &
            &  Abs(Mesh%Vno(2, iseg))  <  eps .AND. &
               &  Abs(Mesh%Vno(3, iseg))  <  eps        ) THEN
               Mesh%Vno(:, iseg) = 1.0e-9
            END IF
            norme = Sqrt(Sum(Mesh%Vno(:, iseg) ** 2))
            Mesh%norme(iseg) = norme !-- VertexCentered3D
!            PRINT *, iseg, "VC3D/norme==", Mesh%Norme(iseg), Mesh%Vno(:, iseg)
            IF (Don%Deformable) THEN
               Mesh%sigma(iseg)   = Mesh%sigma(iseg) / (Dtl * norme)
            END IF
         END DO !-- iseg = 1, Nsegmt
         IF (.NOT. Present(Dt)) THEN
            IF (don%Impre > 0) THEN
               WRITE(6, *) mod_name, " Fin"
            END IF
         END IF

      END SUBROUTINE GEOMMvvnoVertexCentered3d


  !
  !--------------------------------------------------------------------
       !     -----------------------------------------------
       !!@author     Boniface Nkonga      Universite de Bordeaux 1
       !     -----------------------------------------------
       !!  r�le :  calcul des normales et des d�placements aux points fronti�res
      SUBROUTINE CalVnoFr3d(Don, Mesh, Dt)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "CalVnoFr3d"

       !--------------------------------------------------------------------
       !                 Specifications et Declarations
       !--------------------------------------------------------------------
       !
       ! Type et vocations des Variables d'appel
       !----------------------------------------
       !
         TYPE(Donnees), INTENT(IN)             :: Don
         TYPE(MeshDef), INTENT(INOUT)             :: Mesh
         REAL, INTENT(IN), OPTIONAL               :: Dt

       ! Variables Locales Scalaires
       ! -------------------------------------
         INTEGER               :: ifac, k, is, in, ic, ic3, jt
         INTEGER               :: NFacFr, Nsplex

       ! Variables Locales Tableaux
       ! -------------------------------------
         REAL, DIMENSION(3)    :: Vno,  Kappag, xg, xgt
         REAL, DIMENSION(3, 2)  :: v12,  v13
         REAL, DIMENSION(3, 3)  :: VPto, VPtn
         REAL, DIMENSION(3, 4)  :: GraPhi

       !--------------------------------------------------------------------
       !---------------------------------------------------------------------

         NFacFr   = Mesh%NFacFr
         Nsplex   = Mesh%Ndim
         IF (NFacFr  == 0 ) THEN
            RETURN
         END IF
         jt = Minval(Mesh%NumElemtFr)
         IF (jt <= 0 .OR. jt > Size(Mesh%nu, 2)) THEN
            PRINT *, mod_name, " ERREUR : jt min invalide", jt, Size(Mesh%nu, 2), Maxval(Mesh%NumElemtFr)
            CALL delegate_stop()
         END IF
         jt = Maxval(Mesh%NumElemtFr)
         IF (jt <= 0 .OR. jt > Size(Mesh%nu, 2)) THEN
            PRINT *, mod_name, " ERREUR : jt max invalide", jt, Size(Mesh%nu, 2), Minval(Mesh%NumElemtFr)
            CALL delegate_stop()
         END IF

         IF (Nsplex + 1 > Size(Mesh%nu, 1)) THEN
            PRINT *, mod_name, " ERREUR : Nsplex invalide", Nsplex, Size(Mesh%nu, 1)
            CALL delegate_stop()
         END IF

         DO ifac = 1,  NFacFr
            xg = 0.0
            DO  k = 1,  Nsplex
               is           = Mesh%NsfacFr(k, ifac)
!               PRINT *, mod_name, " ifac", ifac, "k", k, " -> is==", is
               VPto(:, k)   = Mesh%Coor(:, is)
               VPtn(:, k)   = Mesh%Coon(:, is)
               xg          = xg + Mesh%Coor(:, is)
            END DO
            xg = xg / 3.0

            jt = Mesh%NumElemtFr(ifac)
            xgt = 0.0
            DO  k = 1,  Nsplex + 1
               is           = Mesh%nu(k, jt)
               IF (is <= 0 .OR. is > Size(Mesh%Coor, 2)) THEN
                  PRINT *, mod_name, " ERREUR : is invalide", is, Size(Mesh%Coor, 2), k, jt, "Nsplex==", Nsplex
                  CALL delegate_stop()
               END IF
               xgt = xgt + Mesh%Coor(:, is)
            END DO
            xgt = xgt / 4.0
          !
          ! Vecteur normal a la face de longeur egale a la surface
          !
            Kappag(: ) =  ( Sum(VPtn, dim=2) - Sum(VPto, dim=2) ) / Real(Nsplex)
          !
            v12(:, 1)   = VPto(:, 2) - VPto(:, 1)
            v13(:, 1)   = VPto(:, 3) - VPto(:, 1)

            v12(:, 2)   = VPtn(:, 2) - VPtn(:, 1)
            v13(:, 2)   = VPtn(:, 3) - VPtn(:, 1)
          !
            ic = -1
            ic3 = -1
            DO  in = 1, 4
               SELECT CASE(in)
                  CASE(1)
                     ic  = 1
                     ic3 = 1
                  CASE(2)
                     ic  = 1
                     ic3 = 2
                  CASE(3)
                     ic  = 2
                     ic3 = 1
                  CASE(4)
                     ic  = 2
                     ic3 = 2
               END SELECT

               GraPhi(1, in) =    v13(2, ic) * v12(3, ic3) - v13(3, ic) * v12(2, ic3)
               GraPhi(2, in) =  - v13(1, ic) * v12(3, ic3) + v13(3, ic) * v12(1, ic3)
               GraPhi(3, in) =    v13(1, ic) * v12(2, ic3) - v13(2, ic) * v12(1, ic3)

            END DO

          !
            Vno(: ) =     (       GraPhi(:, 1) + GraPhi(:, 4) &
            &        + 0.5 * (GraPhi(:, 2) + GraPhi(:, 3))   ) / 6.0
          !
            Mesh%vnoFr(:, ifac) = Vno(: )
          ! Ce test r�gle le probl�me des maillages mal orient�s.
          ! -----------------------------------------------------
            IF (Sum(Vno * (xg - xgt)) < 0 ) THEN
               Mesh%vnoFr(:, ifac) = - Vno(: )
            END IF

!-- Dt doit etre �gal a Var%Dt si MAILLAGE_MOBILE %%%%%%
            IF (Don%Deformable) THEN
               IF (.NOT. Present(Dt)) THEN
                  PRINT *, mod_name, " en MAILLAGE_MOBILE : ERREUR : Var%Dt doit etre passe en argument"
                  CALL delegate_stop()
               ELSE
                  Mesh%SigmaFr(ifac)   = Sum( Vno * Kappag ) / (Dt * Mesh%norme(ifac))
               END IF
            END IF
         END DO

      END SUBROUTINE CalVnoFr3d


       !     -----------------------------------------------
       !!@author     Pascal JACQ
       !     -----------------------------------------------
       !!  r�le :  calcul des normales aux faces fronti�res
      SUBROUTINE CalVnoFrHexa(Mesh)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "CalVnoFrHexa"

       !--------------------------------------------------------------------
       !                 Specifications et Declarations
       !--------------------------------------------------------------------
       !
       ! Type et vocations des Variables d'appel
       !----------------------------------------
       !
         TYPE(MeshDef), INTENT(INOUT)             :: Mesh

       ! Variables Locales Scalaires
       ! -------------------------------------
         INTEGER               :: ifac, k, is, jt, is1, is2
         INTEGER               :: NFacFr, Nsplex

       ! Variables Locales Tableaux
       ! -------------------------------------
         REAL, DIMENSION(3)    :: Vno, xg, xgt
         REAL, DIMENSION(3)    :: v1,  v2

       !--------------------------------------------------------------------
       !---------------------------------------------------------------------

         NFacFr   = Mesh%NFacFr
         Nsplex   = 4 ! SIZE(Mesh%NsfacFr(:,:), DIM=1)
         IF (NFacFr == 0 ) THEN
            RETURN
         END IF
         jt = Minval(Mesh%NumElemtFr)
         IF (jt <= 0 .OR. jt > Size(Mesh%nu, 2)) THEN
            PRINT *, mod_name, " ERREUR : jt min invalide", jt, Size(Mesh%nu, 2), Maxval(Mesh%NumElemtFr)
            CALL delegate_stop()
         END IF
         jt = Maxval(Mesh%NumElemtFr)
         IF (jt <= 0 .OR. jt > Size(Mesh%nu, 2)) THEN
            PRINT *, mod_name, " ERREUR : jt max invalide", jt, Size(Mesh%nu, 2), Minval(Mesh%NumElemtFr)
            CALL delegate_stop()
         END IF
         IF (Nsplex /= 4) THEN
            PRINT *, mod_name, " ERREUR : Nsplex invalide attendu 4 recu:", Nsplex
            CALL delegate_stop()
         END IF

 
         DO ifac = 1,  NFacFr
            ! Calcul du centre de gravite de la face frontiere
            xg = 0.0
            Nsplex = 4
            DO  k = 1,  Nsplex
               is           = Mesh%NsfacFr(k, ifac)
               xg          = xg + Mesh%Coor(:, is)
            END DO
            xg = xg / REAL(Nsplex)

            ! Calcul du centre de gravite de l'element
            jt = Mesh%NumElemtFr(ifac)
            xgt = 0.0
            Nsplex = Mesh%NSommets(jt)
            DO  k = 1,  Nsplex
               is           = Mesh%nu(k, jt)
               xgt = xgt + Mesh%Coor(:, is)
            END DO
            xgt = xgt / Real(Nsplex)

            ! On decompose la face en 4 triangles avec pour sommets les sommets du quadrangle et le centre de gravite
            Vno(:) = 0.0
            DO k = 1, 4
               is1 = Mesh%NsfacFr(k, ifac)
               is2 = Mesh%NsfacFr(mod(k,4) + 1, ifac)
                              
               V1(:) = Mesh%Coor(:, is1) - xg(:)
               V2(:) = Mesh%Coor(:, is2) - xg(:)
               
               ! Produit vectoriel = Aire triangle* normale *2
               Vno(1) = Vno(1) + (V1(2)*V2(3) - V1(3)*V2(2))*0.5
               Vno(2) = Vno(2) + (V1(3)*V2(1) - V1(1)*V2(3))*0.5
               Vno(3) = Vno(3) + (V1(1)*V2(2) - V1(2)*V2(1))*0.5
            END DO
            Mesh%vnoFr(:, ifac) = Vno(: )
          ! Ce test r�gle le probl�me des maillages mal orient�s.
          ! -----------------------------------------------------
            IF (Sum(Vno * (xg - xgt)) < 0 ) THEN
               Mesh%vnoFr(:, ifac) = - Vno(: )
            END IF
         END DO
       END SUBROUTINE CalVnoFrHexa


  ! ===============================================================================
  !!    Param�tres G�ometriques POUR Fluctuation Spliting et Semblables (SUPG)
  ! ===============================================================================
  !         !


       !!  role : verifications et sorties
!!-- Methode d'impressions de mise au point (par ex. l'int�grale des normales exterieures sur la fronti�re)
       !     -----------------------------------------------
       !!@author     Boniface Nkonga      Universite de Bordeaux 1
       !     -----------------------------------------------
      SUBROUTINE GeomVerif(Don , Mesh)  ! OUT
         CHARACTER(LEN = *), PARAMETER :: mod_name = "GeomVerif"
       !
       !--------------------------------------------------------------------
       !                 Specifications et Declarations
       !--------------------------------------------------------------------
       !--------------------------------------------------------------------
       !
       !
       ! Type et vocations des Variables d'appel
       !----------------------------------------
       !
         TYPE(Donnees), INTENT(IN)   :: Don
         TYPE(MeshDef), INTENT(INOUT)   :: Mesh
       ! Variables Locales Scalaires
       ! -------------------------------------
         INTEGER               :: ifac, i, is, is1, is2, iseg
         INTEGER               :: Nsplex
#ifdef MAILLAGE_MOBILE
         REAL                  :: DeltaVol
#endif

       ! Variables Locales Tableaux
       ! -------------------------------------
         REAL, DIMENSION(3)    :: Vno
         REAL, DIMENSION(:, : ), ALLOCATABLE   :: VcNorm

         Nsplex = 3
         IF (Mesh%NFacFr  == 0 ) THEN
            RETURN
         END IF

       !--------------------------------------------------------------------
       !
       !---------------------------------------------------------------------
         WRITE(6, *) mod_name, " Geom Verif  == ",  Size(Mesh%NsfacFr), Size(Mesh%vnoFr)

#ifdef MAILLAGE_MOBILE
         IF (Don%Deformable) THEN
            WRITE(6, *) mod_name, " MIN des Sigma = ", Minval(Mesh%sigma)
            WRITE(6, *) mod_name, " MAX des Sigma = ", Maxval(Mesh%sigma)
            DeltaVol = 0.0
            DO ifac = 1, Mesh%NFacFr
               Vno(: ) = Mesh%vnoFr(:, ifac)
               DeltaVol  = DeltaVol + Mesh%SigmaFr(ifac) * Sqrt( Sum( Vno ** 2 ) )
            END DO
            WRITE(6, *) mod_name, " Variation de volume  == ",  DeltaVol
         END IF
#endif

         SELECT CASE(Don%sel_approche)
            CASE(cs_approche_vertexcentered)
               ALLOCATE( VcNorm(3, Mesh%Npoint) )
               IF (see_alloc) THEN
                  CALL alloc_allocate(mod_name, "vcnorm", Size(VcNorm))
               END IF

               VcNorm = 0.0
               DO iseg = 1, Mesh%Nsegmt
                  is1 = Mesh%nubo(1, iseg)
                  is2 = Mesh%nubo(2, iseg)

                  VcNorm (:, is1) = VcNorm (:, is1)  + Mesh%Vno(:, iseg)
                  VcNorm (:, is2) = VcNorm (:, is2)  - Mesh%Vno(:, iseg)
               END DO

               DO ifac = 1, Mesh%NFacFr
                  DO i = 1, Nsplex
                     is             = Mesh%NsfacFr(i, ifac)
                     VcNorm (:, is) = VcNorm (:, is)  + Mesh%vnoFr(:, ifac) / Real(Nsplex)
                  END DO
               END DO
               WRITE(6, 103) " Int Vno Cells Min = ", Minval(VcNorm, dim = 2)
               WRITE(6, 103) " Int Vno Cells Max = ", Maxval(VcNorm, dim = 2)

            CASE DEFAULT
               WRITE(6, *) mod_name, " :: ERROR : RDS does work with cell centered"
               CALL delegate_stop()
         END SELECT
         WRITE(6, 103) " Vno Min Val = ", Minval(Mesh%Vno)
         WRITE(6, 103) " Vno Max Val = ", Maxval(Mesh%Vno)
         WRITE(6, 103) " Int Vno Min = ", Minval(VcNorm, dim = 2)
         WRITE(6, 103) " Int Vno Max = ", Maxval(VcNorm, dim = 2)

         Vno      = Sum( Mesh%vnoFr, dim=2)
         WRITE(6, *)' Int. des normales ext. : ', Vno,  Maxval( Mesh%vnoFr)
#ifdef SEQUENTIEL
         IF (Maxval( Abs(Vno)) >= 1.0e-05  ) THEN
            WRITE(6, *) mod_name, ' :: ERREUR : domaine non ferme en sequentiel'
            CALL delegate_stop()  ! Sauf pour le calcul //
         END IF
#endif /*-- SEQUENTIEL */

103 FORMAT( a15, 3(2x, e15.8) )
       END SUBROUTINE GeomVerif


       !==================================
       SUBROUTINE Near_Wall_Distance_seq()
       !==================================

         IMPLICIT NONE

         REAL, DIMENSION(Data%Ndim) :: xx, Pb_1, Pb_2, &
                                       l, r, nn, tt

         REAL :: d, d_min

         INTEGER :: N_dofs, bc_idx, bc_type
         INTEGER :: je, jb, id, k
         !------------------------------------

         DO je = 1, SIZE(d_element)

            N_dofs = d_element(je)%e%N_points

            ALLOCATE( d_element(je)%e%d_min(N_dofs) )

            DO k = 1, N_dofs

               xx = d_element(je)%e%Coords(:, k)

               d_min = HUGE(1.d0)
               
               DO jb = 1, SIZE(b_element)
                  
                  bc_idx  = b_element(jb)%b_f%bc_type
                  bc_type = Data%FacMap(bc_idx)
                  
                  IF ( bc_type == lgc_paroi_adh_adiab_flux_nul ) THEN
                     
                     ! boundary segment supposed not curved
                     Pb_1 = b_element(jb)%b_f%Coords(:, 1)
                     Pb_2 = b_element(jb)%b_f%Coords(:, 2)
                     
                     ! Distance vector from the boundary element's vertices
                     ! to the selected point
                     l = xx - Pb_1
                     r = xx - Pb_2
                     
                     ! Normal and tangential versor to the boundary element
                     nn = b_element(jb)%b_f%n_b(:, 1)
                     
                     nn = nn / ( SQRT(SUM(nn*nn)) )
                     
                     tt = (/ nn(2), -nn(1) /)
                     
                     ! Minumum distance of the selected point
                     ! to the boundary element
!!$                     IF ( ABS(SUM(l*tt)) < ABS(SUM(r*tt)) ) THEN
!!$                        d = SQRT(SUM(l*l))
!!$                     ELSEIF( ABS(SUM(r*tt)) < ABS(SUM(l*tt)) ) THEN
!!$                        d = SQRT(SUM(r*r))
!!$                     ENDIF
!!$                  
                     IF( DOT_PRODUCT(l, tt) > 0.d0 ) THEN
                        d = DSQRT(SUM(l*l))
                     ELSEIF( DOT_PRODUCT(r, tt) < 0.d0 ) THEN
                        d = DSQRT(SUM(r*r))
                     ELSEIF(  DOT_PRODUCT(l, tt) <= 0.d0 .AND. &
                              DOT_PRODUCT(r, tt) >= 0.d0 ) THEN
                        d = ABS(DOT_PRODUCT(l, nn))
                     ELSE
                        write(*,*) 'ehhh... gia'
                        stop
                     ENDIF                       

                     d_min = MIN(d_min, d)
                     
                  ENDIF
                                                         
               ENDDO

               d_min = MAX(1.0E-16, d_min) ! To avoid division by 0
            
               d_element(je)%e%d_min(k) = d_min
               
            END DO
                        
         ENDDO
                  
       END SUBROUTINE Near_Wall_Distance_seq
       !====================================

       !==================================
       SUBROUTINE Near_Wall_Distance_par()
       !==================================

         IMPLICIT NONE

         REAL, DIMENSION(Data%Ndim) :: xx, Pb_1, Pb_2, &
                                       l, r, nn, tt, &
                                       nn_1, nn_2

         REAL :: d, d_min

         INTEGER :: N_dofs, bc_idx, bc_type, N_P
         INTEGER :: je, jb, id, k, statut
         LOGICAL :: existe
         !------------------------------------

         INQUIRE(FILE = TRIM(ADJUSTL(Data%rootname))//"-wall", EXIST = existe)

         IF(.NOT. existe) THEN
            WRITE(*,*) 'ERROR: no wall-points file'
            STOP
         ENDIF

         OPEN(1, FILE = TRIM(ADJUSTL(Data%rootname))//"-wall", IOSTAT = statut)

         DO je = 1, SIZE(d_element)

            N_dofs = d_element(je)%e%N_points

            ALLOCATE( d_element(je)%e%d_min(N_dofs) )

            DO k = 1, N_dofs

               xx = d_element(je)%e%Coords(:, k)

               d_min = HUGE(1.d0)
                                
               DO

                  READ(1, *, IOSTAT = statut) N_p
                  IF( statut /= 0 ) EXIT

                  READ(1, *) Pb_1, nn_1
                  READ(1, *) Pb_2, nn_2
       
                  ! Distance vector from the boundary element's vertices
                  ! to the selected point
                  l = xx - Pb_1
                  r = xx - Pb_2
                     
                  ! Normal and tangential versor to the boundary element
                  nn = nn_1
                     
                  nn = nn / ( SQRT(SUM(nn*nn)) )
                     
                  tt = (/ nn(2), -nn(1) /)
                     
!!$                  ! Minumum distance of the selected point
!!$                  ! to the boundary element
!!$                  IF ( ABS(SUM(l*tt)) <= ABS(SUM(r*tt)) ) THEN
!!$                     d = SQRT(SUM(l*l))
!!$                  ELSEIF( ABS(SUM(r*tt)) <= ABS(SUM(l*tt)) ) THEN
!!$                     d = SQRT(SUM(r*r))
!!$                  ENDIF

                  IF( DOT_PRODUCT(l, tt) > 0.d0 ) THEN
                     d = DSQRT(SUM(l*l))
                  ELSEIF( DOT_PRODUCT(r, tt) < 0.d0 ) THEN
                     d = DSQRT(SUM(r*r))
                  ELSEIF(  DOT_PRODUCT(l, tt) <= 0.d0 .AND. &
                       DOT_PRODUCT(r, tt) >= 0.d0 ) THEN
                     d = ABS(DOT_PRODUCT(l, nn))
                  ELSE
                     write(*,*) 'ehhh... gia'
                     STOP
                  ENDIF

                  d_min = MIN(d_min, d)
                           
            ENDDO

            d_min = MAX(1.0E-16, d_min) ! To avoid division by 0
            
            d_element(je)%e%d_min(k) = d_min

            REWIND(1)
               
         END DO
                        
      ENDDO

      CLOSE(1)
                  
    END SUBROUTINE Near_Wall_Distance_par
    !====================================
    
    !===============================================
    SUBROUTINE Near_Wall_Distance_parPtoP(N_DOF_tot)
    !===============================================

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: N_DOF_tot
      !-------------------------------

      REAL, DIMENSION(:,:), ALLOCATABLE :: Coords

      REAL, DIMENSION(:),   ALLOCATABLE :: list_dmin

      REAL, DIMENSION(Data%Ndim) :: xx, xx_b, nn_b

      REAL :: d, d_min

      INTEGER :: N_dofs, bc_idx, bc_type, N_P
      INTEGER :: je, jb, id, k, i, status
      INTEGER :: step, progres
      LOGICAL :: existe
      !------------------------------------

      INQUIRE(FILE = TRIM(ADJUSTL(Data%PbName))//".distance", EXIST = existe)
      
      IF(.NOT. existe) THEN

         WRITE(*,*) 'No previous wall-distance file.'
         WRITE(*,*) 'Distance must be computed' 

         ! Check if the file with the list of the wall points exist
         INQUIRE(FILE = TRIM(ADJUSTL(Data%rootname))//"-wall", EXIST = existe)
         IF(.NOT. existe) THEN
            WRITE(*,*) 'ERROR: no wall-points file. STOP!'
            STOP
         ENDIF
         
         OPEN(1, FILE = TRIM(ADJUSTL(Data%rootname))//"-wall", IOSTAT = status)

         ALLOCATE( Coords(Data%NDim, N_DOF_tot) )
         ALLOCATE( list_dmin(N_DOF_tot) )

         DO je = 1, SIZE(d_element)
            Coords(:, d_element(je)%e%NU) = d_element(je)%e%Coords
         ENDDO

         step = N_DOF_tot/10
         progres = 0

         ! File for storing the wall-distance
         OPEN(15, FILE = TRIM(ADJUSTL(Data%PbName))//".distance", ACTION = 'WRITE', IOSTAT = status)  
         IF(status /= 0) THEN
            WRITE(*,*) 'ERROR: cannnot open the distance file'
            STOP
         ENDIF
      
         DO i = 1, N_DOF_tot

            xx = Coords(:, i)

            d_min = HUGE(1.d0)

            DO

               READ(1, *, IOSTAT = status) N_p
               IF( status /= 0 ) EXIT ! End of File

               DO  k = 1, N_p

                  READ(1, *) xx_b, nn_b

                  d = SQRT(SUM((xx - xx_b)**2))

                  d_min = MIN(d_min, d)

               ENDDO

            ENDDO

            d_min = MAX(1.0E-16, d_min) ! To avoid division by 

            list_dmin(i) = d_min

            IF( MOD(i, step) == 0 ) THEN
               progres = progres + 10
               WRITE(*, '("    ...", I3, "% computed" )') progres
            ENDIF
            
            ! Write the distace in the file
            WRITE(15, '(I10, E24.16)') i, d_min

            REWIND(1)

         ENDDO
         
         CLOSE(1)
         CLOSE(15)
         
         DEALLOCATE( Coords )

      ! A file with the distance exist
      ! Just read it
      ELSE 

         WRITE(*,*) 'Reading the wall distance from the file'
      
         OPEN(15, FILE = TRIM(ADJUSTL(Data%PbName))//".distance", ACTION = 'READ', IOSTAT = status)  
         IF(status /= 0) THEN
            WRITE(*,*) 'ERROR: cannnot open the distance file'
            STOP
         ENDIF
         
         ALLOCATE( list_dmin(N_DOF_tot) )

         DO i = 1, N_DOF_tot            
            READ(15, *) k, list_dmin(i)
         ENDDO

      ENDIF

      ! Store the distance into the element structure
      DO je = 1, SIZE(d_element)

         N_dofs = d_element(je)%e%N_points

         ALLOCATE( d_element(je)%e%d_min(N_dofs) )

         d_element(je)%e%d_min = list_dmin(d_element(je)%e%NU)

      ENDDO

      DEALLOCATE( list_dmin )
             
    END SUBROUTINE Near_Wall_Distance_parPtoP    
    !========================================

  END MODULE GeomParam

