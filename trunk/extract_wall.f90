PROGRAM extract

  USE ReadMesh
  USE GeomGraph
  USE LesTypes
  USE Inputs
  USE Segment_Class
  USE Triangle_F_Class
  USE Quadrangle_F_Class


  CHARACTER(LEN = 64) :: rootname

  INTEGER, DIMENSION(:), ALLOCATABLE :: wall_flag

  INTEGER :: N_flag
  !---------------------------------

  TYPE(MeshDef) :: Mesh
  TYPE(MeshCom) :: Com 
  !--------------------------------

  TYPE(segment),      POINTER :: seg_pt
  TYPE(triangle_F),   POINTER :: tri_F_pt
  TYPE(quadrangle_F), POINTER :: qua_F_pt

  TYPE(b_faces_ptr) :: b_ele
  !----------------------------------

  INTEGER, DIMENSION(:), ALLOCATABLE :: Nu

  REAL, DIMENSION(:,:), ALLOCATABLE :: xx_coords  

  INTEGER, DIMENSION(2,2) :: dummy

  INTEGER :: i, j, k, log, N_p,  ierror
  !--------------------------------

  IF (command_argument_count() < 1) THEN
     WRITE(*,*) 'STOP!'          
     STOP
  ENDIF

  CALL get_command_argument(1, rootname)

  WRITE(*,*) 'N flag:'
  READ(*,*) N_flag

  ALLOCATE( wall_flag(N_flag) )

  WRITE(*,*) 'flag:'
  DO j = 1, N_flag
     READ(*,*) wall_flag(j)
  ENDDO
   
  Data%rootname = TRIM(rootname)
  Com%Ntasks = 1

  dummy = 0

  CALL DonNumMeth(Com, Data)
  CALL DonMesh(Com, Mesh, Data)

  !-------------------------------
  SELECT CASE(Data%sel_geotype)

  CASE(cs_geotype_2dplan)

     CALL GeomSegments2d(Mesh, Data%Impre)

  CASE(cs_geotype_3d)

     CALL GeomSegments3d(Data, Mesh)

     CALL GeomFace3D(Mesh)

  END SELECT

  CALL MeshOrderUpdate(Mesh, Data)
  !-------------------------------

  CALL MeshOrderUpdate(Mesh, Data)

  OPEN(1, FILE = TRIM(ADJUSTL(rootname))//"-wall", ACTION = 'WRITE', IOSTAT = ierror)

  DO i = 1, Mesh%NFacFr

     log = Mesh%LogFr(i)

     DO j = 1, N_flag

        IF( log == wall_flag(j) ) THEN

           N_p = COUNT(Mesh%NsFacFr(:, i) /= 0)

           ALLOCATE( NU(N_p) )

           ALLOCATE( xx_coords(Mesh%Ndim, N_p) )

           Nu = Mesh%NsFacFr(:, i)

           WRITE(1, '(I4)') N_p

           IF( Mesh%Ndim == 2) THEN
              
              ! Segment face
              ALLOCATE(seg_pt)
              CALL seg_pt%initialize( "BoundaryFace", &
                                       NU, Mesh%coor(:, Nu), 0, dummy, 0)
              b_ele%b_f => seg_pt

           ELSE

              IF(N_P == 3 .OR. N_P == 6) THEN

                 ! Triangular face
                 ALLOCATE(tri_F_pt)
                 CALL tri_F_pt%initialize( "BoundaryFace", &
                                            NU, Mesh%coor(:, Nu), 0, dummy, 0)
                 b_ele%b_f => tri_F_pt

              ELSEIF(N_P == 4 .OR. N_P == 9) THEN

                 ! Quadrangular face
                 ALLOCATE(qua_F_pt)
                 CALL qua_F_pt%initialize( "BoundaryFace", &
                                            NU, Mesh%coor(:, Nu), 0, dummy, 0)
                 b_ele%b_f => qua_F_pt

              ENDIF

           ENDIF

           DO k = 1, N_p

              xx_coords(:, k) = Mesh%coor(:, Nu(k))
              IF( Mesh%Ndim == 2) THEN
                 WRITE(1, '(4F24.16)') xx_coords(:,k), b_ele%b_f%n_b(:, k)
              ELSE
                 WRITE(1, '(6F24.16)') xx_coords(:,k), b_ele%b_f%n_b(:, k)
              ENDIF

           ENDDO

           DEALLOCATE( Nu, xx_coords )
           
        ENDIF

     ENDDO    

  ENDDO

  CLOSE(1)

END PROGRAM extract

