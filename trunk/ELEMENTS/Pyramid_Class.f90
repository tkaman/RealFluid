MODULE Pyramid_Class

  USE Elements_Class
  USE Triangle_F_Class
  USE Quadrangle_F_Class
  USE PlinAlg

  IMPLICIT NONE
  !================================================
  TYPE, PUBLIC, EXTENDS(element_str) :: pyramid

     ! < >

   CONTAINS
     
     GENERIC, PUBLIC :: initialize => initialize_pyr_d

     ! Doamain element
     PROCEDURE, PRIVATE, PASS :: initialize_pyr_d

  END TYPE pyramid
  !================================================

  PRIVATE :: init_faces
  PRIVATE :: init_faces_PYR_P1
!  PRIVATE :: init_faces_PYR_P2

  PRIVATE :: volume_quadrature

  PRIVATE :: basis_function
  PRIVATE :: basis_function_PYR_P1
!  PRIVATE :: basis_function_PYR_P2
  
  PRIVATE :: gradient

  PRIVATE :: gradient_ref
  PRIVATE :: gradient_ref_PYR_P1
!  PRIVATE :: gradient_ref_PYR_P2

  PRIVATE :: DOFs_ref
  PRIVATE :: DOFs_ref_PYR_P1
!  PRIVATE :: DOFs_ref_PYR_P2
  
 !PRIVATE :: gradient_trace
 !PRIVATE :: face_trace
  PRIVATE :: face_trace_PYR_P1
!  PRIVATE :: face_trace_PYR_P2  

  PRIVATE :: init_quadrature_PYR_P1
!  PRIVATE :: init_quadrature_PYR_P2

  PRIVATE :: rd_normal

  PRIVATE :: Compute_Jacobian

CONTAINS

  !===========================================================================
  SUBROUTINE initialize_pyr_d(el, mode, Nodes, Coords, Nu_face, n_ele, cn_ele)
  !===========================================================================

    IMPLICIT NONE

    CLASS(pyramid)                           :: el
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    INTEGER,      DIMENSION(:,:), INTENT(IN) :: cn_ele
    !-------------------------------------------------

    INTEGER :: i, id, ne, k
    !-------------------------------------------------

    IF ( mode /= "Element" ) THEN       
       WRITE(*,*) 'ERROR: elment must be initialize in element mode'
       STOP
    ENDIF

    !---------------------------
    ! Attach data to the element
    !---------------------------------------------------
    ALLOCATE( el%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
    ALLOCATE( el%NU(SIZE(Nodes)) )
     
    DO id = 1, SIZE(Coords, 1)
       el%Coords(id, :) = Coords(id, :)
    ENDDO

    el%NU = Nodes

    el%N_dim = SIZE(Coords, 1)

    SELECT CASE( SIZE(Nodes) )

    CASE(5)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = PYR_P1 ! Element type
       el%N_verts  = 5      ! # Vertices 
       el%N_points = 5      ! # DoFs       

    CASE(14)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = PYR_P2 ! Element type
       el%N_verts  = 8      ! # Vertices 
       el%N_points = 14     ! # DoFs

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Pyramid type'
        STOP

     END SELECT

     ! Itialize the faces of the element
     CALL init_faces(el, Nodes, Coords, NU_face, n_ele)

     ! Store the informations at the quadrature points
     CALL volume_quadrature(el)
        
     !--------------------------
     ! Normals for the RD scheme
     !----------------------------------------
     ALLOCATE( el%rd_n(el%N_dim, el%N_verts) )

     DO i = 1, el%N_verts
        el%rd_n(:, i) = rd_normal(el, i)
     ENDDO

     ALLOCATE( el%b_e_con(el%N_points) )

     DO i = 1, el%N_points

         ne = COUNT( cn_ele(i, :) /= 0 ) 

         ALLOCATE( el%b_e_con(i)%cn_ele(ne) )

         el%b_e_con(i)%cn_ele = cn_ele(i, 1:ne)

     ENDDO

     CALL nodal_gradients(el)

   END SUBROUTINE initialize_pyr_d
   !==============================

   !======================================================
   SUBROUTINE init_faces(el, Nodes, Coords, NU_seg, n_ele)
   !======================================================

     IMPLICIT NONE
    
     CLASS(pyramid)                           :: el
     INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
     REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
     INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_seg
     INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
     !-------------------------------------------------

     SELECT CASE(el%Type)
              
     CASE(PYR_P1)

        CALL init_faces_PYR_P1(el, Nodes, Coords, NU_seg, n_ele)
       
!!$     CASE(PYR_P2)
!!$
!!$        CALL init_faces_PYR_P2(el, Nodes, Coords, NU_seg, n_ele)
       
     CASE DEFAULT

        WRITE(*,*) 'Unknown Pyramid type for face initialization'
        WRITE(*,*) 'STOP'
        
     END SELECT

   END SUBROUTINE init_faces
   !========================

   !===============================
   SUBROUTINE volume_quadrature(el)
   !===============================
   !
   ! Store the following  quantities at the quadrature points
   !    - value of the basis function
   !    - normal versor
   !    - weight of the quadrature formula (multipplied by 
   !      the jacobian of the transformation)
   !    - value of the trace of the gradient of basis functions
   !    - physical coordiantes of the quadrature point
   !
   ! Compute the lenght of the segment
   !
     IMPLICIT NONE

     CLASS(pyramid) :: el
     !-----------------------------------------------

     REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ

     INTEGER :: iq, k
     !-----------------------------------------------

     SELECT CASE(el%Type)

     CASE(PYR_P1)

        CALL init_quadrature_PYR_P1(el)
       
!!$     CASE(PYR_P2)
!!$
!!$        CALL init_quadrature_PYR_P2(el)
       
     CASE DEFAULT

        WRITE(*,*) 'Unknown Pyramid type for quadrature'
        WRITE(*,*) 'STOP'

     END SELECT

     !-------------------------------------    
     ! Attach data to the quadrature points
     !---------------------------------------------------
     ALLOCATE( JJ(el%N_dim, el%N_dim) )

     DO iq = 1, el%N_quad

        DO k = 1, el%N_points

           el%phi_q(k, iq) = basis_function( el, k, el%x_q(iq, :) )

           el%xx_q(:, iq) = el%xx_q(:, iq) + &               
                            basis_function( el, k, el%x_q(iq, :) ) * el%Coords(:, k)
 
           el%D_phi_q(:, k, iq) = gradient( el, k, el%x_q(iq, :) )

        ENDDO

        ! remark: not optimized, jacobian computed twice       
        JJ = Compute_Jacobian( el, el%x_q(iq, :) )

        el%w_q(iq) = el%w_q(iq) * determinant(JJ)
      
     ENDDO

     DEALLOCATE( JJ )

     ! Volume of the element
     el%volume = SUM( el%w_q )

     IF( el%volume < 0.d0 ) THEN
        WRITE(*,*) 'ERROR: Wrong node ordering, volume negative'
        STOP
     ENDIF

  END SUBROUTINE volume_quadrature
  !===============================

  !=============================
  SUBROUTINE nodal_gradients(el)
  !=============================
  !
  ! Compute the gradient of the basis functions
  ! at the DOFs of the element
  !
    IMPLICIT NONE

    CLASS(pyramid) :: el
    !-----------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof

    INTEGER :: i, k, id
    !-----------------------------------------------

    ALLOCATE( x_dof(el%N_points, 3) )
    
    x_dof = DOFs_ref(el)
    
    ALLOCATE( el%D_phi_k(el%N_dim, el%N_points, el%N_points) )

    el%D_phi_k = 0.d0

    DO i = 1, el%N_points

       DO k = 1, el%N_points       

          el%D_phi_k(:, i, k) = gradient( el, i, x_dof(k, :) )                 

       ENDDO

    ENDDO
    
    DEALLOCATE(x_dof)

  END SUBROUTINE nodal_gradients
  !=============================

!*******************************************************************************
!*******************************************************************************
!                         COMPLEMENTARY FUNCTIONS                              !
!*******************************************************************************
!*******************************************************************************

  !===============================================
  FUNCTION basis_function(el, i, xi) RESULT(psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(pyramid)                         :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(PYR_P1)

       psi_i = basis_function_PYR_P1(el, i, xi)
       
!!$    CASE(PYR_P2)
!!$
!!$       psi_i = basis_function_PYR_P2(el, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Pyramid type for basis functions'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION basis_function
  !==========================

  !===============================================
  FUNCTION gradient_ref(el, i, xi) RESULT(D_psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(pyramid)                          :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(PYR_P1)

       D_psi_i = gradient_ref_PYR_P1(el, i, xi)
       
!!$    CASE(PYR_P2)
!!$
!!$       D_psi_i = gradient_ref_PYR_P2(el, i, xi)
     
    CASE DEFAULT

       WRITE(*,*) 'Unknown Pyramid type for gradients'
       WRITE(*,*) 'STOP'

    END SELECT    

  END FUNCTION gradient_ref
  !========================

  !===========================================
  FUNCTION gradient(el, i, xi) RESULT(D_psi_i)
  !===========================================
  !
  ! Compute the gradient of the shape function i
  ! on the actual pyramid at the point of 
  ! coordinates xi
  !  
    IMPLICIT NONE

    CLASS(pyramid)                          :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !--------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ, inv_J
    !---------------------------------------------

    ALLOCATE(    JJ(el%N_dim, el%N_dim) )
    ALLOCATE( inv_J(el%N_dim, el%N_dim) )
        
    JJ = compute_Jacobian( el, xi )
    
    inv_J = inverse_easy(JJ)

    D_Psi_i = MATMUL( inv_J, gradient_ref(el, i, xi) )

    DEALLOCATE( JJ, inv_J )

  END FUNCTION gradient
  !====================
   
  !=====================================
  FUNCTION rd_normal(el, l) RESULT(nn_l)
  !=====================================

    IMPLICIT NONE

    CLASS(pyramid)      :: el
    INTEGER, INTENT(IN) :: l

    REAL(KIND=8), DIMENSION(el%N_dim) :: nn_l
    !-----------------------------------------   

    INTEGER :: iq
    !-----------------------------------------

    nn_l = 0.d0
    
    DO iq = 1, el%N_quad
       
       nn_l = nn_l + el%w_q(iq) * el%D_phi_q(:, l, iq)

    ENDDO

  END FUNCTION rd_normal
  !=====================

!!$  !=========================================
!!$  SUBROUTINE gradient_trace(el, jf, p_D_phi)
!!$  !=========================================
!!$  !
!!$  ! Compute the trace of the gradients of all shape
!!$  ! functions on the face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(pyramid)                           :: el
!!$    INTEGER,                        INTENT(IN)  :: jf
!!$    REAL(KIND=8), DIMENSION(:,:,:), INTENT(OUT) :: p_D_phi
!!$    !-----------------------------------------------------3
!!$
!!$    REAL(kind=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    INTEGER :: N_points_ele, N_points_face, k, i
!!$    !-----------------------------------------------------
!!$
!!$    N_points_ele  = e%N_points
!!$    N_points_face = SIZE(p_D_phi, 2)
!!$
!!$    ALLOCATE( xi_f(N_points_face, 3) )
!!$
!!$    xi_f = face_trace(e, jf)
!!$
!!$    DO k = 1, N_points_face      
!!$
!!$       DO i = 1, N_points_ele
!!$
!!$          p_D_phi(:, k, i) = gradient( e, i, xi_f(k,:) )
!!$
!!$       ENDDO
!!$
!!$    ENDDO
!!$    
!!$    DEALLOCATE(xi_f)    
!!$
!!$  END SUBROUTINE gradient_trace
!!$  !============================

!!$  !=======================================
!!$  FUNCTION face_trace(el, jf) RESULT(xi_f)
!!$  !=======================================
!!$  !
!!$  ! Compute the baricentric coordinates on the
!!$  ! face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(pyramid)        :: el
!!$    INTEGER,      INTENT(IN) :: jf
!!$    
!!$    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    !---------------------------------------------
!!$     
!!$    SELECT CASE(el%Type)
!!$       
!!$    CASE(PYR_P1)
!!$
!!$       ALLOCATE( xi_f(4, 3) )
!!$
!!$       xi_f = face_trace_PYR_P1(e, jf)
!!$       
!!$    CASE(PYR_P2)
!!$
!!$       ALLOCATE( xi_f(9, 3) )
!!$
!!$       xi_f = face_trace_PYR_P2(e, jf)
!!$       
!!$    CASE DEFAULT
!!$
!!$       WRITE(*,*) 'Unknown Pyramid type for face trace'
!!$       WRITE(*,*) 'STOP'
!!$
!!$    END SELECT 
!!$
!!$  END FUNCTION face_trace
!!$  !======================

  !==================================
  FUNCTION DOFs_ref(el) RESULT(x_dof)
  !==================================
  !
  ! Compute the coordinates of the DOFs
  ! on the reference element
  !
    IMPLICIT NONE

    CLASS(pyramid) :: el
    
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof
    !-------------------------------------------------
     
    SELECT CASE(el%Type)
       
    CASE(PYR_P1)

       ALLOCATE( x_dof(el%N_points, 3) )

       x_dof = DOFs_ref_PYR_P1(el)
       
!!$    CASE(PYR_P2)
!!$
!!$       ALLOCATE( x_dof(el%N_points, 3) )
!!$
!!$       x_dof = DOFs_ref__P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Pyramid type for DOFs'
       WRITE(*,*) 'STOP'

    END SELECT 

  END FUNCTION DOFs_ref
  !====================

  !===========================================
  FUNCTION Compute_Jacobian(el, xi) RESULT(JJ)
  !===========================================

    IMPLICIT NONE

    CLASS(pyramid)                          :: el
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8),  DIMENSION(el%N_dim, el%N_dim) :: JJ
    !------------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: d

    INTEGER :: k, l, m
    !------------------------------------------------

    ALLOCATE( d(el%N_dim, el%N_points) )

    DO m = 1, el%N_points
       d(:, m) = gradient_ref(el, m, xi)
    ENDDO

    ! Construction of the Jacobian matrix
    DO k = 1, el%N_dim

       DO l = 1, el%N_dim

          JJ(l, k) = SUM( el%Coords(k, :) * d(l, :) )

       ENDDO

    ENDDO

    DEALLOCATE( d )
    
  END FUNCTION Compute_Jacobian
  !============================  

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%     PYRAMID P1     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


   !                                 5
   !                               ,/|\
   !                             ,/ .'|\
   !                           ,/   | | \
   !                         ,/    .' | `.
   !                       ,/      |  '.  \
   !                     ,/       .' z |   \
   !                   ,/         |  ^ |    \
   !                  1----------.'--|-4    `.
   !                   `\        |   |  `\    \ 
   !                     `\     .'   +----`\ - \ -> y
   !                       `\   |    `\     `\  \
   !                         `\.'      `\     `\` 
   !                            2----------------3
   !                                      `\
   !                                         x

   !==============================================================
   SUBROUTINE init_faces_PYR_P1(el, Nodes, Coords, NU_face, n_ele)
   !==============================================================

     IMPLICIT NONE

     CLASS(pyramid)                           :: el
     INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
     REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
     INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
     INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
     !-------------------------------------------------

     TYPE(triangle_F),   POINTER :: tri_F_pt
     TYPE(quadrangle_F), POINTER :: quad_F_pt

     INTEGER,      DIMENSION(:,:), ALLOCATABLE :: loc
     INTEGER,      DIMENSION(:),   ALLOCATABLE :: VV
     REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: RR

     INTEGER :: id, jf, N_ln, nnz, istat
     !-------------------------------------------------

     el%N_faces = 5 ! # Faces

     ALLOCATE( el%faces(el%N_faces) )

     ! # of local nodes
     N_ln = 4
    
     ! DoFs on the faces
     ALLOCATE(loc(el%N_faces, N_ln) )

     ! loc(jf, :) Local ordering of
     ! the nodes on the face jf
     loc(1, :) = (/ 4, 5, 3, 0 /)
     loc(2, :) = (/ 1, 5, 4, 0 /)
     loc(3, :) = (/ 1, 2, 5, 0 /)
     loc(4, :) = (/ 2, 3, 5, 0 /)
     loc(5, :) = (/ 1, 4, 3, 2 /)

     DO jf = 1, el%N_faces

        nnz = COUNT( loc(jf, :) /= 0 )

        ALLOCATE( VV(nnz) )
        ALLOCATE( RR(SIZE(Coords, 1), nnz) )

        VV = Nodes(    loc(jf, 1:nnz) )
        RR = Coords(:, loc(jf, 1:nnz) )

        IF( nnz == 3 ) THEN

           ALLOCATE(tri_F_pt, STAT=istat)
           IF(istat /=0) THEN
              WRITE(*,*) 'ERROR: failed Pyramid faces allocation'
           ENDIF
       
           CALL tri_F_pt%initialize( "InternalFace", &
                                     loc(jf,:), VV, RR, NU_face(jf), n_ele(jf) )

           CALL tri_F_pt%face_quadrature()

           el%faces(jf)%f => tri_F_pt
          
        ELSE

           ALLOCATE(quad_F_pt, STAT=istat)
           IF(istat /=0) THEN
              WRITE(*,*) 'ERROR: failed Pyramid faces allocation'
           ENDIF
       
           CALL quad_F_pt%initialize( "InternalFace", &
                                      loc(jf,:), VV, RR, NU_face(jf), n_ele(jf) )

           CALL quad_F_pt%face_quadrature()

           el%faces(jf)%f => quad_F_pt

        END IF
     
        DEALLOCATE( VV, RR )
          
     ENDDO

     DEALLOCATE( loc )

   END SUBROUTINE init_faces_PYR_P1
   !===============================

   !======================================================
   FUNCTION basis_function_PYR_P1(el, i, xi) RESULT(psi_i)
   !======================================================

     IMPLICIT NONE

     CLASS(pyramid)                         :: el
     INTEGER,                    INTENT(IN) :: i
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

     REAL(KIND=8) :: psi_i
     !-------------------------------------------

     REAL(KIND=8) :: x, y, z
     !-------------------------------------------

     x = xi(1); y = xi(2); z = xi(3)

     SELECT CASE (i)
       
     CASE(1)

        psi_i = (1.d0 - x)*(1.d0 - y)*(1.d0 - z)/8.d0

     CASE(2)

        psi_i = (1.d0 + x)*(1.d0 - y)*(1.d0 - z)/8.d0

     CASE(3)

        psi_i = (1.d0 + x)*(1.d0 + y)*(1.d0 - z)/8.d0

     CASE(4)

        psi_i = (1.d0 - x)*(1.d0 + y)*(1.d0 - z)/8.d0

     CASE(5)

        psi_i = 0.5d0*(1.d0 + z)

     CASE DEFAULT

        WRITE(*,*) 'ERROR: not supported Dof in Pyramid baisis function'
        STOP
       
     END SELECT

   END FUNCTION basis_function_PYR_P1
   !=================================

   !======================================================
   FUNCTION gradient_ref_PYR_P1(el, i, xi) RESULT(D_psi_i)
   !======================================================

    IMPLICIT NONE

    CLASS(pyramid)                         :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !---------------------------------------------

    REAL(KIND=8) :: x, y, z
    !-------------------------------------------

    x = xi(1); y = xi(2); z = xi(3)

    SELECT CASE(i)

    CASE(1)

       D_psi_i(1) = -(1.d0 - y)*(1.d0 - z)/8.d0
       D_psi_i(2) = -(1.d0 - x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 - x)*(1.d0 - y)/8.d0
       
    CASE(2)

       D_psi_i(1) =  (1.d0 - y)*(1.d0 - z)/8.d0
       D_psi_i(2) = -(1.d0 + x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 + x)*(1.d0 - y)/8.d0

    CASE(3)

       D_psi_i(1) =  (1.d0 + y)*(1.d0 - z)/8.d0
       D_psi_i(2) =  (1.d0 + x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 + x)*(1.d0 + y)/8.d0

    CASE(4)

       D_psi_i(1) = -(1.d0 + y)*(1.d0 - z)/8.d0 
       D_psi_i(2) =  (1.d0 - x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 - x)*(1.d0 + y)/8.d0

    CASE(5)

       D_psi_i(1) = 0.d0
       D_psi_i(2) = 0.d0
       D_psi_i(3) = 0.5d0

    CASE DEFAULT
       
       WRITE(*,*) 'ERROR: not supported Dof in Pyramid gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_PYR_P1
  !===============================

  !=========================================
  FUNCTION DOFs_ref_PYR_P1(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(pyramid) :: el

    REAL(KIND=8), DIMENSION(8, 3) :: x_dof
    !--------------------------------------

    REAL(KIND=8), PARAMETER :: eps = 1.0E-16
    !--------------------------------------

    x_dof(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
    x_dof(2, :) = (/  1.d0, -1.d0, -1.d0 /)
    x_dof(3, :) = (/  1.d0,  1.d0, -1.d0 /)
    x_dof(4, :) = (/ -1.d0,  1.d0, -1.d0 /)
    x_dof(5, :) = (/  0.d0,  0.d0,  1.d0-eps /)

  END FUNCTION DOFs_ref_PYR_P1
  !===========================

  !==============================================
  FUNCTION face_trace_PYR_P1(el, jf) RESULT(xi_f)
  !==============================================

    IMPLICIT NONE

    CLASS(pyramid)           :: el
    INTEGER,      INTENT(IN) :: jf

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
    !----------------------------------------------

    SELECT CASE(jf)

    CASE(1)

       ALLOCATE( xi_f(3, 3) )

       xi_f(1, :) = (/ -1.d0,  1.d0, -1.d0 /)
       xi_f(2, :) = (/  0.d0,  0.d0,  1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0, -1.d0 /)

    CASE(2)

       ALLOCATE( xi_f(3, 3) )

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/  0.d0,  0.d0,  1.d0 /)
       xi_f(3, :) = (/ -1.d0,  1.d0, -1.d0 /)

    CASE(3)

       ALLOCATE( xi_f(3, 3) )

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/  1.d0, -1.d0, -1.d0 /)
       xi_f(3, :) = (/  0.d0,  0.d0,  1.d0 /)

    CASE(4)

       ALLOCATE( xi_f(3, 3) )

       xi_f(1, :) = (/  1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/  1.d0,  1.d0, -1.d0 /)
       xi_f(3, :) = (/  0.d0,  0.d0,  1.d0 /)

    CASE(5)

       ALLOCATE( xi_f(4, 3) )

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/ -1.d0,  1.d0, -1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0, -1.d0 /)
       xi_f(4, :) = (/  1.d0, -1.d0, -1.d0 /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong Pyramid face in face trace P1'
       STOP

    END SELECT    
    
  END FUNCTION face_trace_PYR_P1
  !=============================

  !====================================
  SUBROUTINE init_quadrature_Pyr_P1(el)
  !====================================

    IMPLICIT NONE

    CLASS(Pyramid) :: el
    !----------------------------------------------
    
    REAL(KIND=8) :: g1, g2, g3
    !----------------------------------------------

    el%N_quad = 5

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%D_phi_q(el%N_dim, el%N_points, el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 3) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )

    !-------------------
    ! Quadrature formula
    !--------------------------------------
    
    g1 = (8.d0/5.d0)*DSQRT(2.d0/15.d0)
    g2 = -2.d0/3.d0
    g3 =  2.d0/5.d0

    el%x_q(1,:) = (/ -g1, -g1, g2 /) 
    el%x_q(2,:) = (/  g1, -g1, g2 /)
    el%x_q(3,:) = (/  g1,  g1, g2 /)
    el%x_q(4,:) = (/ -g1,  g1, g2 /)

    el%w_q(1:4) = 81.d0/100.d0

    el%x_q(5,:) = (/ 0.d0, 0.d0, g3 /)

    el%w_q(5) = 125.d0/27.d0
    !--------------------------------------

    el%xx_q = 0.d0

  END SUBROUTINE init_quadrature_PYR_P1
  !====================================

END MODULE Pyramid_Class
