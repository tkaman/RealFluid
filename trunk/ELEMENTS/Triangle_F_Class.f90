MODULE Triangle_F_Class

  USE Face_Class
  USE PLinAlg

  IMPLICIT NONE

  !============================================
  TYPE, PUBLIC, EXTENDS(face_str) ::triangle_F

     ! < >

   CONTAINS

     GENERIC, PUBLIC :: initialize => initialize_tf, &
                                      initialize_tb

     ! Internal Face
     PROCEDURE, PRIVATE, PASS :: initialize_tf
     ! Boundary face
     PROCEDURE, PRIVATE, PASS :: initialize_tb
     
     PROCEDURE, PUBLIC :: face_quadrature => face_quadrature_sub
     
  END TYPE triangle_F
  !============================================
    
  PRIVATE :: face_quadrature_sub
  PRIVATE :: init_quadrature_TRI_P1
  PRIVATE :: init_quadrature_TRI_P2

  PRIVATE :: normal
  
  PRIVATE :: basis_function
  PRIVATE :: basis_function_TRI_P1
  PRIVATE :: basis_function_TRI_P2

  PRIVATE :: gradient_ref
  PRIVATE :: gradient_ref_TRI_P1
  PRIVATE :: gradient_ref_TRI_P2

  PRIVATE :: DOFs_ref
  PRIVATE :: DOFs_ref_TRI_P1
  PRIVATE :: DOFs_ref_TRI_P2

CONTAINS

  !====================================================================
  SUBROUTINE initialize_tf(el, mode, loc, Nodes, Coords, Nu_face, n_ele)
  !====================================================================

    IMPLICIT NONE

    CLASS(triangle_F)                        :: el
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: loc
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,                      INTENT(IN) :: NU_face
    INTEGER,                      INTENT(IN) :: n_ele
    !---------------------------------------------------

    INTEGER :: i, id
    !---------------------------------------------------
    
    IF ( mode /= "InternalFace" ) THEN
       WRITE(*,*) 'ERROR: elment must be initialize in face mode'
       STOP
    ENDIF

    SELECT CASE( SIZE(Nodes) )
       
    CASE(3)
        
       el%Type     = TRI_F_P1
       el%N_verts  = 3
       el%N_points = 3
              
    CASE(6)

       el%Type     = TRI_F_P2
       el%N_verts  = 3
       el%N_points = 6

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unsupported Triangle type'
       STOP

    END SELECT

    !---------------------------
    ! Attach data to the element
    !---------------------------------------------------
    ALLOCATE( el%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
    ALLOCATE( el%NU(SIZE(Nodes)) )
    ALLOCATE( el%l_NU(SIZE(loc)) )
          
    DO id = 1, SIZE(Coords,1)
       el%Coords(id, :) = Coords(id, :)
    ENDDO

    el%N_dim = SIZE(Coords,1)

    el%NU = Nodes

    el%l_NU = loc

    el%g_face = NU_face

    el%c_ele = n_ele

  END SUBROUTINE initialize_tf
  !==========================

  !========================================================================
  SUBROUTINE initialize_tb(el, mode, Nodes, Coords, bc_type, cn_ele, b_ovlp)
  !========================================================================

    IMPLICIT NONE

    CLASS(triangle_F)                        :: el
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,                      INTENT(IN) :: bc_type
    INTEGER,      DIMENSION(:,:), INTENT(IN) :: cn_ele
    INTEGER,                      INTENT(IN) :: b_ovlp
    !-------------------------------------------------

    REAL, DIMENSION(:,:), ALLOCATABLE :: x_dof
    REAL :: mod
    INTEGER :: j, id, ne
    !-------------------------------------------------

    IF ( mode /= "BoundaryFace" ) THEN
       WRITE(*,*) 'ERROR: elment must be initialize in boundary mode'
       STOP
    ENDIF
    
    SELECT CASE( SIZE(Nodes) )

    CASE(3)

       el%Type     = TRI_P1
       el%N_verts  = 3
       el%N_points = 3
              
    CASE(6)

       el%Type     = TRI_P2
       el%N_verts  = 3
       el%N_points = 6

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unsupported Triangle type'
       STOP

    END SELECT

    !---------------------------
    ! Attach data to the element
    !---------------------------------------------------
    ALLOCATE( el%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
    ALLOCATE( el%NU(SIZE(Nodes)) )
    
    DO id = 1, SIZE(Coords,1)
       el%Coords(id, :) = Coords(id, :)
    ENDDO

    el%N_dim = SIZE(Coords,1)
    
    el%NU = Nodes

    el%bc_type = bc_type

    el%b_ovlp = b_ovlp

    CALL el%face_quadrature()

    !----------------------
    ! Normal versor at Dofs
    !--------------------------------------
    ALLOCATE( el%n_b(el%N_dim, el%N_points) )
    ALLOCATE( x_dof(el%N_points, 3) )
    ALLOCATE( el%b_e_con(el%N_points) )

    x_dof = DOFs_ref(el)
     
    DO j = 1, el%N_points

       el%n_b(:, j) = normal( el, x_dof(j, :) )
        
       mod = DSQRT( SUM(el%n_b(:, j)**2) )        

       el%n_b(:, j) = el%n_b(:, j)/mod

       ne = COUNT( cn_ele(j, :) /= 0 )

       ALLOCATE( el%b_e_con(j)%cn_ele(ne) )

       el%b_e_con(j)%cn_ele = cn_ele(j, 1:ne)
        
        
    ENDDO

    DEALLOCATE(x_dof)
         
  END SUBROUTINE initialize_tb
  !==========================

  !=================================
  SUBROUTINE face_quadrature_sub(el)
  !=================================
  !
  ! Store the following  quantities at the quadrature points
  !    - value of the basis function
  !    - normal versor
  !    - weight of the quadrature formula (multipplied by 
  !      the jacobian of the transformation)
  !    - value of the trace of the gradient of basis functions
  !    - physical coordiantes of the quadrature point
  !
  ! Compute the lenght of the segment
  !
    IMPLICIT NONE

    CLASS(triangle_F)  :: el
    !-----------------------

    REAL(KIND=8) :: Jac    
    INTEGER :: iq, k, l
    !--------------------

    SELECT CASE(el%Type)

    CASE(TRI_P1)

       CALL init_quadrature_TRI_P1(el)
       
    CASE(TRI_P2)
       
       CALL init_quadrature_TRI_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Segment type for quadrature'
       WRITE(*,*) 'STOP'

    END SELECT

    !-------------------------------------
    ! Attach data to the quadrature points
    !---------------------------------------------------
    DO iq = 1, el%N_quad

       DO k = 1, el%N_points

          el%phi_q(k, iq) = basis_function( el, k, el%x_q(iq, :) )

          el%xx_q(:, iq) = el%xx_q(:, iq) + &
                           basis_function( el, k, el%x_q(iq, :) ) * el%Coords(:, k)

       ENDDO
       
       el%n_q(:, iq) = normal(el, el%x_q(iq, :) )

       Jac = DSQRT( SUM(el%n_q(:, iq)**2) )

       el%n_q(:, iq) = el%n_q(:, iq)/Jac

       el%w_q(iq) = el%w_q(iq) * Jac


!!$       DO k = 1, SIZE(p_D_phi,3)
!!$
!!$          DO l = 1, e%N_points
!!$             
!!$             e%p_Dphi_1_q(:, k, j) = e%p_Dphi_1_q(:, k, j) + &
!!$                                     p_D_phi(:, l, k)*e%phi_q(l, j)
!!$ 
!!$          ENDDO          
!!$          
!!$       ENDDO

    ENDDO

    ! Area of the element
    el%area = SUM(el%w_q)

    IF( el%area < 0.d0 ) THEN
       WRITE(*,*) 'ERROR: Wrong node ordering, area negative'
       STOP
    ENDIF

  END SUBROUTINE face_quadrature_sub  
  !=================================


!*******************************************************************************
!*******************************************************************************
!                         COMPLEMENTARY FUNCTIONS                              !
!*******************************************************************************
!*******************************************************************************

  !===============================================
  FUNCTION basis_function(el, i, xi) RESULT(psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(triangle_F)                      :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(TRI_P1)

       psi_i = basis_function_TRI_P1(el, i, xi)
       
    CASE(TRI_P2)

       psi_i = basis_function_TRI_P2(el, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tiangle type for basis functions'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION basis_function
  !==========================

  !===============================================
  FUNCTION gradient_ref(el, i, xi) RESULT(D_psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(triangle_F)                       :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(TRI_P1)

       D_psi_i = gradient_ref_TRI_P1(el, i, xi)
       
    CASE(TRI_P2)

       D_psi_i = gradient_ref_TRI_P2(el, i, xi)
     
    CASE DEFAULT

       WRITE(*,*) 'Unknown Triangle type for gradients'
       WRITE(*,*) 'STOP'

    END SELECT    

  END FUNCTION gradient_ref
  !========================

  !==================================
  FUNCTION DOFs_ref(el) RESULT(x_dof)
  !==================================
  !
  ! Compute the coordinates of the DOFs
  ! on the reference element
  !
    IMPLICIT NONE

    CLASS(triangle_F) :: el
    
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof
    !-------------------------------------------------
     
    SELECT CASE(el%Type)
       
    CASE(TRI_P1)

       ALLOCATE( x_dof(3, 3) )

       x_dof = DOFs_ref_TRI_P1(el)
       
    CASE(TRI_P2)

       ALLOCATE( x_dof(6, 3) )

       x_dof = DOFs_ref_TRI_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Triangle type for DOFs'
       WRITE(*,*) 'STOP'

    END SELECT 

  END FUNCTION DOFs_ref
  !====================

  !==================================
  FUNCTION normal(el, xi) RESULT(n_i)
  !==================================
  !
  ! Compute the normal versor to the face 
  ! on the node with coordiantes xi
  !
    IMPLICIT NONE

    CLASS(triangle_F)                       :: el
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: n_i
    !---------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: d
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: Jb

    INTEGER :: i, k, l
    !---------------------------------------------

    ALLOCATE( d(el%N_dim, el%N_points) )

    DO i = 1, el%N_points
       d(:, i) = gradient_ref(el, i, xi)
    ENDDO    

    ALLOCATE( Jb(el%N_dim-1, el%N_dim) )

    DO k = 1, el%N_dim

       DO l = 1, el%N_dim - 1

          Jb(l, k) = SUM( el%Coords(k, :) * d(l, :) )

       ENDDO

    ENDDO

    n_i = Cross_product(Jb)

    DEALLOCATE( d, Jb )

  END FUNCTION normal
  !==================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%     TRIANGLE P1    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !
  !                           (3)
  !                            o\
  !                            |  \
  !                            |    \
  !                            |      \
  !                            |        \
  !                            o---------o
  !                           (1)       (2)
  !

  !======================================================
  FUNCTION basis_function_TRI_P1(el, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle_F)                      :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    SELECT CASE (i)
       
    CASE(1, 2, 3)
       
       psi_i = xi(i)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle  baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_TRI_P1
  !=================================

  !======================================================
  FUNCTION gradient_ref_TRI_P1(el, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle_F)                      :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = (/ -1.d0, -1.d0 /)
       
    CASE(2)

       D_psi_i = (/ 1.d0, 0.d0 /)
       
    CASE(3)

       D_psi_i = (/ 0.d0, 1.d0 /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_TRI_P1
  !===============================

  !=========================================
  FUNCTION DOFs_ref_TRI_P1(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(triangle_F) :: el

    REAL(KIND=8), DIMENSION(3, 3) :: x_dof
    !-------------------------------------

    x_dof(1, :) = (/ 1.d0,  0.d0,  0.d0  /)
    x_dof(2, :) = (/ 0.d0,  1.d0,  0.d0  /)
    x_dof(3, :) = (/ 0.d0,  0.d0,  1.d0  /)

  END FUNCTION DOFs_ref_TRI_P1
  !===========================

  !====================================
  SUBROUTINE init_quadrature_TRI_P1(el)
  !====================================

    IMPLICIT NONE

    CLASS(triangle_F) :: el
    !----------------------------------------------

    REAL(KIND=8) :: alpha_1, beta_1
    !-----------------------------------------------

    el%N_quad = 6

    ALLOCATE(   el%n_q(el%N_dim,    el%N_quad) )
    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 3) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !--------------------------------------
    alpha_1 = 0.816847572980459d0
    beta_1  = 0.091576213509771d0

    el%x_q(1,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(2,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(3,:) = (/ beta_1 , beta_1 , alpha_1 /)
    el%w_q(1:3) = 0.109951743655322d0
    
    alpha_1 = 0.108103018168070d0
    beta_1  = 0.445948490915965d0
    
    el%x_q(4,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(5,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(6,:) = (/ beta_1 , beta_1 , alpha_1 /)    
    el%w_q(4:6) = 0.223381589678011d0
    !--------------------------------------
    
    el%w_q = el%w_q*0.5d0 ! J -> 0.5*det|J|  
    
    el%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_TRI_P1
  !====================================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%     TRIANGLE P2    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !
  !                           (3)
  !                            o\
  !                            |  \
  !                            |    \
  !                            |      \
  !                        (6) o        o (5)
  !                            |          \
  !                            |            \
  !                            o------o------o
  !                           (1)    (4)    (2)
  !

  !======================================================
  FUNCTION basis_function_TRI_P2(el, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle_F)                      :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    SELECT CASE (i)

    CASE(1, 2, 3)
       
       psi_i = (2.d0*xi(i) - 1.d0) * xi(i)
                     
    CASE(4)

       psi_i  = 4.d0*xi(1)*xi(2)
              
    CASE(5)

       psi_i  = 4.d0*xi(2)*xi(3)
              
    CASE(6)

       psi_i = 4.d0*xi(3)*xi(1)
       
    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle  baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_TRI_P2
  !=================================

  !======================================================
  FUNCTION gradient_ref_TRI_P2(el, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle_F)                      :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = -(/ 4.d0*xi(1) - 1.d0, 4.d0*xi(1) - 1.d0 /)
       
    CASE(2)

       D_psi_i = (/ 4.d0*xi(2) - 1.d0, 0.d0 /)
       
    CASE(3)

       D_psi_i = (/ 0.d0, 4.d0*xi(3) - 1.d0 /)

    CASE(4)

       D_psi_i = (/ 4.d0*(xi(1)-xi(2)), -4.d0*xi(2) /)

    CASE(5)

       D_psi_i = (/ 4.d0*xi(3), 4.d0*xi(2) /)

    CASE(6)

       D_psi_i = (/ -4.d0*xi(3), 4.d0*(xi(1) - xi(3)) /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_TRI_P2
  !===============================

  !=========================================
  FUNCTION DOFs_ref_TRI_P2(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(triangle_F) :: el

    REAL(KIND=8), DIMENSION(6,3) :: x_dof
    !-------------------------------------

    x_dof(1, :) = (/ 1.d0,  0.d0,  0.d0  /)
    x_dof(2, :) = (/ 0.d0,  1.d0,  0.d0  /)
    x_dof(3, :) = (/ 0.d0,  0.d0,  1.d0  /)
    x_dof(4, :) = (/ 0.5d0, 0.5d0, 0.d0  /)
    x_dof(5, :) = (/ 0.d0,  0.5d0, 0.5d0 /)
    x_dof(6, :) = (/ 0.5d0, 0.d0,  0.5d0 /)

  END FUNCTION DOFs_ref_TRI_P2
  !===========================

  !====================================
  SUBROUTINE init_quadrature_TRI_P2(el)
  !====================================

    IMPLICIT NONE

    CLASS(triangle_F) :: el
    !----------------------------------------------------

    REAL(KIND=8) :: alpha_1, beta_1, gamma_1
    !----------------------------------------------------

    el%N_quad = 7

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )
    ALLOCATE(   el%n_q(el%N_dim,    el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 3) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !-----------------------------------------------
    alpha_1 = 1.d0/3.d0

    el%x_q(1,:) = (/ alpha_1, alpha_1 , alpha_1 /)
    el%w_q(1)   = 9.d0/40.d0
    
    beta_1 = ( 6.d0 + DSQRT(15.d0) ) /21.d0
    alpha_1 = 1.d0 - 2.d0*beta_1
    
    el%x_q(2,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(3,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(4,:) = (/ beta_1 , beta_1 , alpha_1 /)
    el%w_q(2:4) = ( 155.d0 + DSQRT(15.d0) ) / 1200.d0

    beta_1 = ( 6.d0 - DSQRT(15.d0) ) /21.d0
    alpha_1 = 1.d0 - 2.d0*beta_1
    
    el%x_q(5,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(6,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(7,:) = (/ beta_1 , beta_1 , alpha_1 /)    
    el%w_q(5:7) = ( 155.d0 - DSQRT(15.d0) ) / 1200.d0
    !-------------------------------------------------

    el%w_q = el%w_q*0.5d0 ! J -> 0.5*det|J|
    
    el%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_TRI_P2
  !====================================

END MODULE Triangle_F_Class
