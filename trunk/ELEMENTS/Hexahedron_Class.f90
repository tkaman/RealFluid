MODULE Hexahedron_Class

  USE Elements_Class
  USE Quadrangle_F_Class
  USE PlinAlg

  IMPLICIT NONE
  
  !================================================
  TYPE, PUBLIC, EXTENDS(element_str) :: hexahedron

     ! < >

   CONTAINS
     
     GENERIC, PUBLIC :: initialize => initialize_hex_d

     ! Doamain element
     PROCEDURE, PRIVATE, PASS :: initialize_hex_d

  END TYPE hexahedron
  !================================================

  PRIVATE :: init_faces
  PRIVATE :: init_faces_HEX_Q1
  PRIVATE :: init_faces_HEX_Q2

  PRIVATE :: volume_quadrature

  PRIVATE :: basis_function
  PRIVATE :: basis_function_HEX_Q1
  PRIVATE :: basis_function_HEX_Q2
  
  PRIVATE :: gradient

  PRIVATE :: gradient_ref
  PRIVATE :: gradient_ref_HEX_Q1
  PRIVATE :: gradient_ref_HEX_Q2

  PRIVATE :: DOFs_ref
  PRIVATE :: DOFs_ref_HEX_Q1
  PRIVATE :: DOFs_ref_HEX_Q2
  
 !PRIVATE :: gradient_trace
 !PRIVATE :: face_trace
  PRIVATE :: face_trace_HEX_Q1
  PRIVATE :: face_trace_HEX_Q2  

  PRIVATE :: init_quadrature_HEX_Q1
  PRIVATE :: init_quadrature_HEX_Q2

  PRIVATE :: rd_normal

  PRIVATE :: Compute_Jacobian

CONTAINS

  !===========================================================================
  SUBROUTINE initialize_hex_d(el, mode, Nodes, Coords, Nu_face, n_ele, cn_ele)
  !===========================================================================

    IMPLICIT NONE

    CLASS(hexahedron)                        :: el
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    INTEGER,      DIMENSION(:,:), INTENT(IN) :: cn_ele
    !-------------------------------------------------

    INTEGER :: i, id, ne, k
    !-------------------------------------------------

    IF ( mode /= "Element" ) THEN       
       WRITE(*,*) 'ERROR: elment must be initialize in element mode'
       STOP
    ENDIF

    !---------------------------
    ! Attach data to the element
    !---------------------------------------------------
    ALLOCATE( el%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
    ALLOCATE( el%NU(SIZE(Nodes)) )
     
    DO id = 1, SIZE(Coords, 1)
       el%Coords(id, :) = Coords(id, :)
    ENDDO

    el%NU = Nodes

    el%N_dim = SIZE(Coords, 1)

    SELECT CASE( SIZE(Nodes) )

    CASE(8)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = HEX_Q1 ! Element type
       el%N_verts  = 8      ! # Vertices 
       el%N_points = 8      ! # DoFs       

    CASE(27)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = HEX_Q2 ! Element type
       el%N_verts  = 8      ! # Vertices 
       el%N_points = 27     ! # DoFs

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Hexahedon type'
        STOP

     END SELECT

     ! Itialize the faces of the element
     CALL init_faces(el, Nodes, Coords, NU_face, n_ele)

     ! Store the informations at the quadrature points
     CALL volume_quadrature(el)
        
     !--------------------------
     ! Normals for the RD scheme
     !----------------------------------------
     ALLOCATE( el%rd_n(el%N_dim, el%N_verts) )

     DO i = 1, el%N_verts
        el%rd_n(:, i) = rd_normal(el, i)
     ENDDO

     ALLOCATE( el%b_e_con(el%N_points) )

     DO i = 1, el%N_points

         ne = COUNT( cn_ele(i, :) /= 0 ) 

         ALLOCATE( el%b_e_con(i)%cn_ele(ne) )

         el%b_e_con(i)%cn_ele = cn_ele(i, 1:ne)

     ENDDO

     CALL nodal_gradients(el)

   END SUBROUTINE initialize_hex_d
   !==============================

   !======================================================
   SUBROUTINE init_faces(el, Nodes, Coords, NU_seg, n_ele)
   !======================================================

     IMPLICIT NONE
    
     CLASS(hexahedron)                        :: el
     INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
     REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
     INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_seg
     INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
     !-------------------------------------------------

     SELECT CASE(el%Type)
              
     CASE(HEX_Q1)

        CALL init_faces_HEX_Q1(el, Nodes, Coords, NU_seg, n_ele)
       
     CASE(HEX_Q2)

        CALL init_faces_HEX_Q2(el, Nodes, Coords, NU_seg, n_ele)
       
     CASE DEFAULT

        WRITE(*,*) 'Unknown Hexahedron type for face initialization'
        WRITE(*,*) 'STOP'
        
     END SELECT

   END SUBROUTINE init_faces
   !========================

   !===============================
   SUBROUTINE volume_quadrature(el)
   !===============================
   !
   ! Store the following  quantities at the quadrature points
   !    - value of the basis function
   !    - normal versor
   !    - weight of the quadrature formula (multipplied by 
   !      the jacobian of the transformation)
   !    - value of the trace of the gradient of basis functions
   !    - physical coordiantes of the quadrature point
   !
   ! Compute the lenght of the segment
   !
     IMPLICIT NONE

     CLASS(hexahedron) :: el
     !-----------------------------------------------

     REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ

     INTEGER :: iq, k
     !-----------------------------------------------

     SELECT CASE(el%Type)

     CASE(HEX_Q1)

        CALL init_quadrature_HEX_Q1(el)
       
     CASE(HEX_Q2)

        CALL init_quadrature_HEX_Q2(el)
       
     CASE DEFAULT

        WRITE(*,*) 'Unknown Hexahedron type for quadrature'
        WRITE(*,*) 'STOP'

     END SELECT

     !-------------------------------------    
     ! Attach data to the quadrature points
     !---------------------------------------------------
     ALLOCATE( JJ(el%N_dim, el%N_dim) )

     DO iq = 1, el%N_quad

        DO k = 1, el%N_points

           el%phi_q(k, iq) = basis_function( el, k, el%x_q(iq, :) )

           el%xx_q(:, iq) = el%xx_q(:, iq) + &               
                            basis_function( el, k, el%x_q(iq, :) ) * el%Coords(:, k)
 
           el%D_phi_q(:, k, iq) = gradient( el, k, el%x_q(iq, :) )

        ENDDO

        ! remark: not optimized, jacobian computed twice       
        JJ = Compute_Jacobian( el, el%x_q(iq, :) )

        el%w_q(iq) = el%w_q(iq) * determinant(JJ)
      
     ENDDO

     DEALLOCATE( JJ )

     ! Volume of the element
     el%volume = SUM( el%w_q )

     IF( el%volume < 0.d0 ) THEN
        WRITE(*,*) 'ERROR: Wrong node ordering, volume negative'
        STOP
     ENDIF

  END SUBROUTINE volume_quadrature
  !===============================

  !=============================
  SUBROUTINE nodal_gradients(el)
  !=============================
  !
  ! Compute the gradient of the basis functions
  ! at the DOFs of the element
  !
    IMPLICIT NONE

    CLASS(hexahedron) :: el
    !-----------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof

    INTEGER :: i, k
    !-----------------------------------------------

    ALLOCATE( x_dof(el%N_points, 3) )
    
    x_dof = DOFs_ref(el)
    
    ALLOCATE( el%D_phi_k(el%N_dim, el%N_points, el%N_points) )

    DO i = 1, el%N_points

       DO k = 1, el%N_points

          el%D_phi_k(:, i, k) = gradient( el, i, x_dof(k, :) )

       ENDDO

    ENDDO
    
    DEALLOCATE(x_dof)

  END SUBROUTINE nodal_gradients
  !=============================

!*******************************************************************************
!*******************************************************************************
!                         COMPLEMENTARY FUNCTIONS                              !
!*******************************************************************************
!*******************************************************************************

  !===============================================
  FUNCTION basis_function(el, i, xi) RESULT(psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(hexahedron)                      :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(HEX_Q1)

       psi_i = basis_function_HEX_Q1(el, i, xi)
       
    CASE(HEX_Q2)

       psi_i = basis_function_HEX_Q2(el, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Hexahedron type for basis functions'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION basis_function
  !==========================

  !===============================================
  FUNCTION gradient_ref(el, i, xi) RESULT(D_psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(hexahedron)                       :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(HEX_Q1)

       D_psi_i = gradient_ref_HEX_Q1(el, i, xi)
       
    CASE(HEX_Q2)

       D_psi_i = gradient_ref_HEX_Q2(el, i, xi)
     
    CASE DEFAULT

       WRITE(*,*) 'Unknown Hexahedron type for gradients'
       WRITE(*,*) 'STOP'

    END SELECT    

  END FUNCTION gradient_ref
  !========================

  !===========================================
  FUNCTION gradient(el, i, xi) RESULT(D_psi_i)
  !===========================================
  !
  ! Compute the gradient of the shape function i
  ! on the actual hexahedron at the point of 
  ! coordinates xi
  !  
    IMPLICIT NONE

    CLASS(hexahedron)                       :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !--------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ, inv_J
    !---------------------------------------------

    ALLOCATE(    JJ(el%N_dim, el%N_dim) )
    ALLOCATE( inv_J(el%N_dim, el%N_dim) )
        
    JJ = compute_Jacobian( el, xi )
    
    inv_J = inverse_easy(JJ)

    D_Psi_i = MATMUL( inv_J, gradient_ref(el, i, xi) )

    DEALLOCATE( JJ, inv_J )

  END FUNCTION gradient
  !====================
   
  !=====================================
  FUNCTION rd_normal(el, l) RESULT(nn_l)
  !=====================================

    IMPLICIT NONE

    CLASS(hexahedron)   :: el
    INTEGER, INTENT(IN) :: l

    REAL(KIND=8), DIMENSION(el%N_dim) :: nn_l
    !-----------------------------------------   

    INTEGER :: iq
    !-----------------------------------------

    nn_l = 0.d0
    
    DO iq = 1, el%N_quad
       
       nn_l = nn_l + el%w_q(iq) * el%D_phi_q(:, l, iq)

    ENDDO

  END FUNCTION rd_normal
  !=====================

!!$  !=========================================
!!$  SUBROUTINE gradient_trace(el, jf, p_D_phi)
!!$  !=========================================
!!$  !
!!$  ! Compute the trace of the gradients of all shape
!!$  ! functions on the face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(hexahedron)                           :: el
!!$    INTEGER,                        INTENT(IN)  :: jf
!!$    REAL(KIND=8), DIMENSION(:,:,:), INTENT(OUT) :: p_D_phi
!!$    !-----------------------------------------------------3
!!$
!!$    REAL(kind=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    INTEGER :: N_points_ele, N_points_face, k, i
!!$    !-----------------------------------------------------
!!$
!!$    N_points_ele  = e%N_points
!!$    N_points_face = SIZE(p_D_phi, 2)
!!$
!!$    ALLOCATE( xi_f(N_points_face, 3) )
!!$
!!$    xi_f = face_trace(e, jf)
!!$
!!$    DO k = 1, N_points_face      
!!$
!!$       DO i = 1, N_points_ele
!!$
!!$          p_D_phi(:, k, i) = gradient( e, i, xi_f(k,:) )
!!$
!!$       ENDDO
!!$
!!$    ENDDO
!!$    
!!$    DEALLOCATE(xi_f)    
!!$
!!$  END SUBROUTINE gradient_trace
!!$  !============================

!!$  !=======================================
!!$  FUNCTION face_trace(el, jf) RESULT(xi_f)
!!$  !=======================================
!!$  !
!!$  ! Compute the baricentric coordinates on the
!!$  ! face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(hexahedron)        :: el
!!$    INTEGER,      INTENT(IN) :: jf
!!$    
!!$    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    !---------------------------------------------
!!$     
!!$    SELECT CASE(el%Type)
!!$       
!!$    CASE(HEX_Q1)
!!$
!!$       ALLOCATE( xi_f(4, 3) )
!!$
!!$       xi_f = face_trace_HEX_Q1(e, jf)
!!$       
!!$    CASE(HEX_Q2)
!!$
!!$       ALLOCATE( xi_f(9, 3) )
!!$
!!$       xi_f = face_trace_HEX_Q2(e, jf)
!!$       
!!$    CASE DEFAULT
!!$
!!$       WRITE(*,*) 'Unknown Hexahedron type for face trace'
!!$       WRITE(*,*) 'STOP'
!!$
!!$    END SELECT 
!!$
!!$  END FUNCTION face_trace
!!$  !======================

  !==================================
  FUNCTION DOFs_ref(el) RESULT(x_dof)
  !==================================
  !
  ! Compute the coordinates of the DOFs
  ! on the reference element
  !
    IMPLICIT NONE

    CLASS(hexahedron) :: el
    
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof
    !-------------------------------------------------
     
    SELECT CASE(el%Type)
       
    CASE(HEX_Q1)

       ALLOCATE( x_dof(el%N_points, 3) )

       x_dof = DOFs_ref_HEX_Q1(el)
       
    CASE(HEX_Q2)

       ALLOCATE( x_dof(el%N_points, 3) )

       x_dof = DOFs_ref_HEX_Q2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Hexaedron type for DOFs'
       WRITE(*,*) 'STOP'

    END SELECT 

  END FUNCTION DOFs_ref
  !====================

  !===========================================
  FUNCTION Compute_Jacobian(el, xi) RESULT(JJ)
  !===========================================

    IMPLICIT NONE

    CLASS(hexahedron)                       :: el
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8),  DIMENSION(el%N_dim, el%N_dim) :: JJ
    !------------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: d

    INTEGER :: k, l, m
    !------------------------------------------------

    ALLOCATE( d(el%N_dim, el%N_points) )

    DO m = 1, el%N_points
       d(:, m) = gradient_ref(el, m, xi)
    ENDDO

    ! Construction of the Jacobian matrix
    DO k = 1, el%N_dim

       DO l = 1, el%N_dim

          JJ(l, k) = SUM( el%Coords(k, :) * d(l, :) )

       ENDDO

    ENDDO

    DEALLOCATE( d )
    
  END FUNCTION Compute_Jacobian
  !============================  

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%    HEXAHEDRON Q1   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   !                                  y
   !                            4----------3
   !                            |\     ^   |\
   !                            | \    |   | \
   !                            |  \   |   |  \
   !                            |   8------+---7
   !                            |   |  +-- |-- | -> x
   !                            1---+---\--2   |
   !                             \  |    \  \  |
   !                              \ |     \  \ |
   !                               \|      z  \|
   !                                5----------6

   !==============================================================
   SUBROUTINE init_faces_HEX_Q1(el, Nodes, Coords, NU_face, n_ele)
   !==============================================================

     IMPLICIT NONE

     CLASS(hexahedron)                        :: el
     INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
     REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
     INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
     INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
     !-------------------------------------------------

     TYPE(quadrangle_F), POINTER :: quad_F_pt

     INTEGER,      DIMENSION(:,:), ALLOCATABLE :: loc
     INTEGER,      DIMENSION(:),   ALLOCATABLE :: VV
     REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: RR

     INTEGER :: id, jf, N_ln, istat
     !-------------------------------------------------

     el%N_faces = 6 ! # Faces

     ALLOCATE( el%faces(el%N_faces) )

     ! # of local nodes
     N_ln = 4
    
     ! DoFs on the faces
     ALLOCATE( VV(N_ln), loc(el%N_faces, N_ln) )

     ! Coordinates on the faces
     ALLOCATE( RR(SIZE(Coords, 1), N_ln) )

     ! loc(jf, :) Local ordering of
     ! the nodes on the face jf
     loc(1, :) = (/ 1, 4, 3, 2 /)
     loc(2, :) = (/ 5, 6, 7, 8 /)
     loc(3, :) = (/ 1, 2, 6, 5 /)
     loc(4, :) = (/ 2, 3, 7, 6 /)
     loc(5, :) = (/ 3, 4, 8, 7 /)
     loc(6, :) = (/ 1, 5, 8, 4 /)

     DO jf = 1, el%N_faces

        VV = Nodes(    loc(jf, :) )
        RR = Coords(:, loc(jf, :) )

        ALLOCATE(quad_F_pt, STAT=istat)
        IF(istat /=0) THEN
           WRITE(*,*) 'ERROR: failed Hexahedron faces allocation'
        ENDIF
       
        CALL quad_F_pt%initialize( "InternalFace", &
                                   loc(jf,:), VV, RR, NU_face(jf), n_ele(jf) )

        CALL quad_F_pt%face_quadrature()

        el%faces(jf)%f => quad_F_pt
          
     ENDDO

     DEALLOCATE( VV, RR, loc )

   END SUBROUTINE init_faces_HEX_Q1
   !===============================

   !======================================================
   FUNCTION basis_function_HEX_Q1(el, i, xi) RESULT(psi_i)
   !======================================================

     IMPLICIT NONE

     CLASS(hexahedron)                      :: el
     INTEGER,                    INTENT(IN) :: i
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

     REAL(KIND=8) :: psi_i
     !-------------------------------------------

     REAL(KIND=8) :: x, y, z
     !-------------------------------------------

     x = xi(1); y = xi(2); z = xi(3)

     SELECT CASE (i)
       
     CASE(1)

        psi_i = (1.d0 - x)*(1.d0 - y)*(1.d0 - z)/8.d0

     CASE(2)

        psi_i = (1.d0 + x)*(1.d0 - y)*(1.d0 - z)/8.d0

     CASE(3)

        psi_i = (1.d0 + x)*(1.d0 + y)*(1.d0 - z)/8.d0

     CASE(4)

        psi_i = (1.d0 - x)*(1.d0 + y)*(1.d0 - z)/8.d0

     CASE(5)

        psi_i = (1.d0 - x)*(1.d0 - y)*(1.d0 + z)/8.d0

     CASE(6)

        psi_i = (1.d0 + x)*(1.d0 - y)*(1.d0 + z)/8.d0

     CASE(7)

        psi_i = (1.d0 + x)*(1.d0 + y)*(1.d0 + z)/8.d0

     CASE(8)

        psi_i = (1.d0 - x)*(1.d0 + y)*(1.d0 + z)/8.d0


     CASE DEFAULT

        WRITE(*,*) 'ERROR: not supported Dof in Hexahedron baisis function'
        STOP
       
     END SELECT

   END FUNCTION basis_function_HEX_Q1   
   !=================================

   !======================================================
   FUNCTION gradient_ref_HEX_Q1(el, i, xi) RESULT(D_psi_i)
   !======================================================

    IMPLICIT NONE

    CLASS(hexahedron)                      :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !---------------------------------------------

    REAL(KIND=8) :: x, y, z
    !-------------------------------------------

    x = xi(1); y = xi(2); z = xi(3)

    SELECT CASE(i)

    CASE(1)

       D_psi_i(1) = -(1.d0 - y)*(1.d0 - z)/8.d0
       D_psi_i(2) = -(1.d0 - x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 - x)*(1.d0 - y)/8.d0
       
    CASE(2)

       D_psi_i(1) =  (1.d0 - y)*(1.d0 - z)/8.d0
       D_psi_i(2) = -(1.d0 + x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 + x)*(1.d0 - y)/8.d0

    CASE(3)

       D_psi_i(1) =  (1.d0 + y)*(1.d0 - z)/8.d0
       D_psi_i(2) =  (1.d0 + x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 + x)*(1.d0 + y)/8.d0

    CASE(4)

       D_psi_i(1) = -(1.d0 + y)*(1.d0 - z)/8.d0 
       D_psi_i(2) =  (1.d0 - x)*(1.d0 - z)/8.d0
       D_psi_i(3) = -(1.d0 - x)*(1.d0 + y)/8.d0

    CASE(5)

       D_psi_i(1) = -(1.d0 - y)*(1.d0 + z)/8.d0 
       D_psi_i(2) = -(1.d0 - x)*(1.d0 + z)/8.d0
       D_psi_i(3) =  (1.d0 - x)*(1.d0 - y)/8.d0

    CASE(6)

       D_psi_i(1) =  (1.d0 - y)*(1.d0 + z)/8.d0 
       D_psi_i(2) = -(1.d0 + x)*(1.d0 + z)/8.d0
       D_psi_i(3) =  (1.d0 + x)*(1.d0 - y)/8.d0

    CASE(7)

       D_psi_i(1) = (1.d0 + y)*(1.d0 + z)/8.d0 
       D_psi_i(2) = (1.d0 + x)*(1.d0 + z)/8.d0
       D_psi_i(3) = (1.d0 + x)*(1.d0 + y)/8.d0

    CASE(8)

       D_psi_i(1) = -(1.d0 + y)*(1.d0 + z)/8.d0 
       D_psi_i(2) =  (1.d0 - x)*(1.d0 + z)/8.d0
       D_psi_i(3) =  (1.d0 - x)*(1.d0 + y)/8.d0

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Hexaedron gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_HEX_Q1  
  !===============================

  !=========================================
  FUNCTION DOFs_ref_HEX_Q1(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(hexahedron) :: el

    REAL(KIND=8), DIMENSION(8, 3) :: x_dof
    !--------------------------------------

    x_dof(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
    x_dof(2, :) = (/  1.d0, -1.d0, -1.d0 /)
    x_dof(3, :) = (/  1.d0,  1.d0, -1.d0 /)
    x_dof(4, :) = (/ -1.d0,  1.d0, -1.d0 /)
    x_dof(5, :) = (/ -1.d0, -1.d0,  1.d0 /)
    x_dof(6, :) = (/  1.d0, -1.d0,  1.d0 /)
    x_dof(7, :) = (/  1.d0,  1.d0,  1.d0 /)
    x_dof(8, :) = (/ -1.d0,  1.d0,  1.d0 /)

  END FUNCTION DOFs_ref_HEX_Q1
  !===========================
  
  !==============================================
  FUNCTION face_trace_HEX_Q1(el, jf) RESULT(xi_f)
  !==============================================

    IMPLICIT NONE

    CLASS(hexahedron)        :: el
    INTEGER,      INTENT(IN) :: jf

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
    !----------------------------------------------

    ALLOCATE( xi_f(4, 3) )

    SELECT CASE(jf)

    CASE(1)

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/  1.d0, -1.d0, -1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0, -1.d0 /)
       xi_f(4, :) = (/ -1.d0,  1.d0, -1.d0 /)

    CASE(2)

       xi_f(1, :) = (/ -1.d0, -1.d0,  1.d0 /)
       xi_f(2, :) = (/  1.d0, -1.d0,  1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/ -1.d0,  1.d0,  1.d0 /)

    CASE(3)

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/  1.d0, -1.d0, -1.d0 /)
       xi_f(3, :) = (/  1.d0, -1.d0,  1.d0 /)
       xi_f(4, :) = (/ -1.d0, -1.d0,  1.d0 /)

    CASE(4)

       xi_f(1, :) = (/  1.d0, -1.d0, -1.d0 /) 
       xi_f(2, :) = (/  1.d0,  1.d0, -1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/  1.d0, -1.d0,  1.d0 /)

    CASE(5)

       xi_f(1, :) = (/  1.d0,  1.d0, -1.d0 /) 
       xi_f(2, :) = (/ -1.d0,  1.d0, -1.d0 /) 
       xi_f(3, :) = (/ -1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/  1.d0,  1.d0,  1.d0 /)

    CASE(6)

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /) 
       xi_f(2, :) = (/ -1.d0, -1.d0,  1.d0 /)
       xi_f(3, :) = (/ -1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/ -1.d0,  1.d0, -1.d0 /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong Hexahedron face in face trace Q1'
       STOP

    END SELECT    
    
  END FUNCTION face_trace_HEX_Q1
  !=============================

  !====================================
  SUBROUTINE init_quadrature_HEX_Q1(el)
  !====================================

    IMPLICIT NONE

    CLASS(hexahedron) :: el
    !----------------------------------------------

    el%N_quad = 8

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%D_phi_q(el%N_dim, el%N_points, el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 3) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )

    !-------------------
    ! Quadrature formula
    !--------------------------------------
    el%x_q(1,:) = (/ -1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0) /)
    el%x_q(2,:) = (/  1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0) /)
    el%x_q(3,:) = (/  1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0) /)
    el%x_q(4,:) = (/ -1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0) /)
    el%x_q(5,:) = (/ -1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0) /)
    el%x_q(6,:) = (/  1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0) /)
    el%x_q(7,:) = (/  1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0) /)
    el%x_q(8,:) = (/ -1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0) /)

    el%w_q = 1.d0
    !--------------------------------------

    el%xx_q = 0.d0

  END SUBROUTINE init_quadrature_HEX_Q1
  !====================================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%    HEXAHEDRON Q2   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !                           4----14----3
  !                           |\         |\
  !                           |16    25  | 15
  !                          10  \ 21    12 \
  !                           |   8----20+---7
  !                           |23 |  27  | 24|
  !                           1---+-9----2   |
  !                            \ 18    26 \  19
  !                            11 |  22    13|
  !                              \|         \|
  !                               5----17----6
  !                                  

  !==============================================================
  SUBROUTINE init_faces_HEX_Q2(el, Nodes, Coords, NU_face, n_ele)
  !==============================================================

    IMPLICIT NONE

    CLASS(hexahedron)                        :: el
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    !-------------------------------------------------

    TYPE(quadrangle_F), POINTER :: quad_F_pt

    INTEGER,      DIMENSION(:,:), ALLOCATABLE :: loc
    INTEGER,      DIMENSION(:),   ALLOCATABLE :: VV
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: RR
    
    INTEGER :: id, jf, N_ln, istat
    !-------------------------------------------------

    el%N_faces = 6 ! # Faces
    ALLOCATE( el%faces(el%N_faces) )

    ! # of local nodes
    N_ln = 9

    ! DoFs on the faces
    ALLOCATE( VV(N_ln), loc(el%N_faces, N_ln) )

    ! Coordinates on the faces
    ALLOCATE( RR(SIZE(Coords, 1), N_ln) )

    ! loc(jf, :) Local ordering of
    ! the nodes on the face jf
    loc(1, :) = (/ 1, 4, 3, 2, 10, 14, 12, 9,  21 /)
    loc(2, :) = (/ 5, 6, 7, 8, 17, 19, 20, 18, 26 /)
    loc(3, :) = (/ 1, 2, 6, 5, 9,  13, 17, 11, 22 /)
    loc(4, :) = (/ 2, 3, 7, 6, 12, 15, 19, 13, 24 /)
    loc(5, :) = (/ 3, 4, 8, 7, 14, 16, 20, 15, 25 /)
    loc(6, :) = (/ 1, 5, 8, 4, 11, 18, 16, 10, 23 /)
  
    DO jf = 1, el%N_faces

       VV = Nodes(    loc(jf, :) )
       RR = Coords(:, loc(jf, :) )

       ALLOCATE(quad_F_pt, STAT=istat)
       IF(istat /=0) THEN
          WRITE(*,*) 'ERROR: failed Hexahedron faces allocation'
       ENDIF
       
       CALL quad_F_pt%initialize( "InternalFace", &
                                   loc(jf,:), VV, RR, NU_face(jf), n_ele(jf) )

        CALL quad_F_pt%face_quadrature()

        el%faces(jf)%f => quad_F_pt
          
     ENDDO

     DEALLOCATE( VV, RR, loc )

  END SUBROUTINE init_faces_HEX_Q2
  !===============================

  !======================================================
  FUNCTION basis_function_HEX_Q2(el, i, xi) RESULT(psi_i)
  !======================================================

     IMPLICIT NONE

     CLASS(hexahedron)                      :: el
     INTEGER,                    INTENT(IN) :: i
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

     REAL(KIND=8) :: psi_i
     !-------------------------------------------

     REAL(KIND=8) :: x, y, z, x2, y2, z2
     !-------------------------------------------

     x = xi(1); y = xi(2); z = xi(3)

     x2 = x*x; y2 = y*y; z2 = z*z

     SELECT CASE (i)
       
     CASE(1)

        psi_i = (x2 - x)*(y2 - y)*(z2 - z)/8.d0

     CASE(2)
        
        psi_i = (x2 + x)*(y2 - y)*(z2 - z)/8.d0

     CASE(3)

        psi_i = (x2 + x)*(y2 + y)*(z2 - z)/8.d0

     CASE(4)

        psi_i = (x2 - x)*(y2 + y)*(z2 - z)/8.d0

     CASE(5)

        psi_i = (x2 - x)*(y2 - y)*(z2 + z)/8.d0

     CASE(6)

        psi_i = (x2 + x)*(y2 - y)*(z2 + z)/8.d0

     CASE(7)

        psi_i = (x2 + x)*(y2 + y)*(z2 + z)/8.d0

     CASE(8)

        psi_i = (x2 - x)*(y2 + y)*(z2 + z)/8.d0

     CASE(9)

        psi_i = (1.d0 - x2)*(y2 - y)*(z2 - z)/4.d0

     CASE(10)

        psi_i = (x2 - x)*(1.d0 - y2)*(z2 -z)/4.d0

     CASE(11)

        psi_i = (x2 - x)*(y2 - y)*(1.d0 -z2)/4.d0

     CASE(12)

        psi_i = (x2 + x)*(1.d0 - y2)*(z2 - z)/4.d0

     CASE(13)

        psi_i = (x2 + x)*(y2 - y)*(1.d0 - z2)/4.d0

     CASE(14)

        psi_i = (1.d0 - x2)*(y2 + y)*(z2 - z)/4.d0

     CASE(15)

        psi_i = (x2 + x)*(y2 + y)*(1.d0 - z2)/4.d0

     CASE(16)

        psi_i = (x2 - x)*(y2 + y)*(1.d0 - z2)/4.d0

     CASE(17)

        psi_i = (1.d0 - x2)*(y2 - y)*(z2 + z)/4.d0

     CASE(18)

        psi_i = (x2 - x)*(1.d0 - y2)*(z2 + z)/4.d0

     CASE(19)

        psi_i = (x2 + x)*(1.d0 - y2)*(z + z2)/4.d0

     CASE(20)

        psi_i = (1.d0 - x2)*(y2 + y)*(z2 + z)/4.d0

     CASE(21)

        psi_i = (1.d0 - x2)*(1.d0 - y2)*(z2 - z)/2.d0

     CASE(22)

        psi_i = (1.d0 - x2)*(y2 - y)*(1.d0 - z2)/2.d0

     CASE(23)

        psi_i = (x2 - x)*(1.d0 - y2)*(1.d0 - z2)/2.d0

     CASE(24)

        psi_i = (x2 + x)*(1.d0 - y2)*(1.d0 - z2)/2.d0

     CASE(25)

        psi_i = (1.d0 - x2)*(y2 + y)*(1.d0 - z2)/2.d0

     CASE(26)

        psi_i = (1.d0 - x2)*(1.d0 - y2)*(z2 + z)/2.d0

     CASE(27)

        psi_i = (1.d0 - x2)*(1.d0 - y2)*(1.d0 - z2)
        
     CASE DEFAULT

        WRITE(*,*) 'ERROR: not supported Dof in Hexahedron baisis function'
        STOP
       
     END SELECT

   END FUNCTION basis_function_HEX_Q2
   !=================================

   !======================================================
   FUNCTION gradient_ref_HEX_Q2(el, i, xi) RESULT(D_psi_i)
   !======================================================

     IMPLICIT NONE

     CLASS(hexahedron)                      :: el
     INTEGER,                    INTENT(IN) :: i
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi
     
     REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
     !---------------------------------------------

     REAL(KIND=8) :: x, y, z, x2, y2, z2
     !-------------------------------------------
     
     x = xi(1); y = xi(2); z = xi(3)

     x2 = x*x; y2 = y*y; z2 = z*z

     SELECT CASE(i)

     CASE(1)

        D_psi_i(1) = (2.d0*x - 1.d0)*(y2 - y)*(z2 - z)/8.d0
        D_psi_i(2) = (x2 - x)*(2.d0*y - 1.d0)*(z2 - z)/8.d0
        D_psi_i(3) = (x2 - x)*(y2 - y)*(2.d0*z - 1.d0)/8.d0
        
     CASE(2)

        D_psi_i(1) = (2.d0*x + 1.d0)*(y2 - y)*(z2 - z)/8.d0
        D_psi_i(2) = (x2 + x)*(2.d0*y - 1.d0)*(z2 - z)/8.d0
        D_psi_i(3) = (x2 + x)*(y2 - y)*(2.d0*z - 1.d0)/8.d0

     CASE(3)

        D_psi_i(1) = (2.d0*x + 1.d0)*(y2 + y)*(z2 - z)/8.d0
        D_psi_i(2) = (x2 + x)*(2.d0*y + 1.d0)*(z2 - z)/8.d0
        D_psi_i(3) = (x2 + x)*(y2 + y)*(2.d0*z - 1.d0)/8.d0

     CASE(4)

        D_psi_i(1) = (2.d0*x - 1.d0)*(y2 + y)*(z2 - z)/8.d0
        D_psi_i(2) = (x2 - x)*(2.d0*y + 1.d0)*(z2 - z)/8.d0
        D_psi_i(3) = (x2 - x)*(y2 + y)*(2.d0*z - 1.d0)/8.d0

     CASE(5)

        D_psi_i(1) = (2.d0*x - 1.d0)*(y2 - y)*(z2 + z)/8.d0
        D_psi_i(2) = (x2 - x)*(2.d0*y - 1.d0)*(z2 + z)/8.d0
        D_psi_i(3) = (x2 - x)*(y2 - y)*(2.d0*z + 1.d0)/8.d0

     CASE(6)

        D_psi_i(1) = (2.d0*x + 1.d0)*(y2 - y)*(z2 + z)/8.d0
        D_psi_i(2) = (x2 + x)*(2.d0*y - 1.d0)*(z2 + z)/8.d0
        D_psi_i(3) = (x2 + x)*(y2 - y)*(2.d0*z + 1.d0)/8.d0

     CASE(7)

        D_psi_i(1) = (2.d0*x + 1.d0)*(y2 + y)*(z2 + z)/8.d0
        D_psi_i(2) = (x2 + x)*(2.d0*y + 1.d0)*(z2 + z)/8.d0
        D_psi_i(3) = (x2 + x)*(y2 + y)*(2.d0*z + 1.d0)/8.d0

     CASE(8)

        D_psi_i(1) = (2.d0*x - 1.d0)*(y2 + y)*(z2 + z)/8.d0
        D_psi_i(2) = (x2 - x)*(2.d0*y + 1.d0)*(z2 + z)/8.d0
        D_psi_i(3) = (x2 - x)*(y2 + y)*(2.d0*z + 1.d0)/8.d0

     CASE(9)

        D_psi_i(1) = (-2.d0*x)*(y2 - y)*(z2 - z)/4.d0
        D_psi_i(2) = (1.d0 - x2)*(2.d0*y - 1.d0)*(z2 - z)/4.d0
        D_psi_i(3) = (1.d0 - x2)*(y2 - y)*(2.d0*z - 1.d0)/4.d0

     CASE(10)

        D_psi_i(1) = (2.d0*x - 1.d0)*(1.d0 - y2)*(z2 -z)/4.d0
        D_psi_i(2) = (x2 - x)*(-2.d0*y)*(z2 -z)/4.d0
        D_psi_i(3) = (x2 - x)*(1.d0 - y2)*(2.d0*z - 1.d0)/4.d0

     CASE(11)

        D_psi_i(1) = (2.d0*x - 1.d0)*(y2 - y)*(1.d0 - z2)/4.d0
        D_psi_i(2) = (x2 - x)*(2.d0*y - 1.d0)*(1.d0 - z2)/4.d0
        D_psi_i(3) = (x2 - x)*(y2 - y)*(-2.d0*z)/4.d0

     CASE(12)

        D_psi_i(1) = (2.d0*x + 1.d0)*(1.d0 - y2)*(z2 - z)/4.d0
        D_psi_i(2) = (x2 + x)*(-2.d0*y)*(z2 - z)/4.d0
        D_psi_i(3) = (x2 + x)*(1.d0 - y2)*(2.d0*z - 1.d0)/4.d0

     CASE(13)

        D_psi_i(1) = (2.d0*x + 1.d0)*(y2 - y)*(1.d0 - z2)/4.d0
        D_psi_i(2) = (x2 + x)*(2.d0*y - 1.d0)*(1.d0 - z2)/4.d0
        D_psi_i(3) = (x2 + x)*(y2 - y)*(-2.d0*z)/4.d0

     CASE(14)

        D_psi_i(1) = (-2.d0*x)*(y2 + y)*(z2 - z)/4.d0
        D_psi_i(2) = (1.d0 - x2)*(2.d0*y + 1.d0)*(z2 - z)/4.d0
        D_psi_i(3) = (1.d0 - x2)*(y2 + y)*(2.d0*z - 1.d0)/4.d0
        
     CASE(15)

        D_psi_i(1) = (2.d0*x + 1.d0)*(y2 + y)*(1.d0 - z2)/4.d0
        D_psi_i(2) = (x2 + x)*(2.d0*y + 1.d0)*(1.d0 - z2)/4.d0
        D_psi_i(3) = (x2 + x)*(y2 + y)*(-2.d0*z)/4.d0

     CASE(16)

        D_psi_i(1) = (2.d0*x - 1.d0)*(y2 + y)*(1.d0 - z2)/4.d0
        D_psi_i(2) = (x2 - x)*(2.d0*y + 1.d0)*(1.d0 - z2)/4.d0
        D_psi_i(3) = (x2 - x)*(y2 + y)*(-2.d0*z)/4.d0

     CASE(17)
        
        D_psi_i(1) = (-2.d0*x)*(y2 - y)*(z2 + z)/4.d0
        D_psi_i(2) = (1.d0 - x2)*(2.d0*y - 1.d0)*(z2 + z)/4.d0
        D_psi_i(3) = (1.d0 - x2)*(y2 - y)*(2.d0*z + 1.d0)/4.d0

     CASE(18)

        D_psi_i(1) = (2.d0*x - 1.d0)*(1.d0 - y2)*(z2 + z)/4.d0
        D_psi_i(2) = (x2 - x)*(-2.d0*y)*(z2 + z)/4.d0
        D_psi_i(3) = (x2 - x)*(1.d0 - y2)*(2.d0*z + 1.d0)/4.d0

     CASE(19)

        D_psi_i(1) = (2.d0*x + 1.d0)*(1.d0 - y2)*(z + z2)/4.d0
        D_psi_i(2) = (x2 + x)*(-2.d0*y)*(z + z2)/4.d0
        D_psi_i(3) = (x2 + x)*(1.d0 - y2)*(2.d0*z + 1.d0)/4.d0

     CASE(20)

         D_psi_i(1) = (-2.d0*x)*(y2 + y)*(z2 + z)/4.d0
         D_psi_i(2) = (1.d0 - x2)*(2.d0*y + 1.d0)*(z2 + z)/4.d0
         D_psi_i(3) = (1.d0 - x2)*(y2 + y)*(2.d0*z + 1.d0)/4.d0

      CASE(21)

         D_psi_i(1) = (-2.d0*x)*(1.d0 - y2)*(z2 - z)/2.d0
         D_psi_i(2) = (1.d0 - x2)*(-2.d0*y)*(z2 - z)/2.d0
         D_psi_i(3) = (1.d0 - x2)*(1.d0 - y2)*(2.d0*z - 1.d0)/2.d0

      CASE(22)

         D_psi_i(1) = (-2.d0*x)*(y2 - y)*(1.d0 - z2)/2.d0
         D_psi_i(2) = (1.d0 - x2)*(2.d0*y - 1.d0)*(1.d0 - z2)/2.d0
         D_psi_i(3) = (1.d0 - x2)*(y2 - y)*(-2.d0*z)/2.d0

      CASE(23)

         D_psi_i(1) = (2.d0*x - 1.d0)*(1.d0 - y2)*(1.d0 - z2)/2.d0
         D_psi_i(2) = (x2 - x)*(-2.d0*y)*(1.d0 - z2)/2.d0
         D_psi_i(3) = (x2 - x)*(1.d0 - y2)*(-2.d0*z)/2.d0

      CASE(24)

         D_psi_i(1) = (2.d0*x + 1.d0)*(1.d0 - y2)*(1.d0 - z2)/2.d0
         D_psi_i(2) = (x2 + x)*(-2.d0*y)*(1.d0 - z2)/2.d0
         D_psi_i(3) = (x2 + x)*(1.d0 - y2)*(-2.d0*z)/2.d0

      CASE(25)

         D_psi_i(1) = (-2.d0*x)*(y2 + y)*(1.d0 - z2)/2.d0
         D_psi_i(2) = (1.d0 - x2)*(2.d0*y + 1.d0)*(1.d0 - z2)/2.d0
         D_psi_i(3) = (1.d0 - x2)*(y2 + y)*(-2.d0*z)/2.d0

      CASE(26)

         D_psi_i(1) = (-2.d0*x)*(1.d0 - y2)*(z2 + z)/2.d0
         D_psi_i(2) = (1.d0 - x2)*(-2.d0*y)*(z2 + z)/2.d0
         D_psi_i(3) = (1.d0 - x2)*(1.d0 - y2)*(2.d0*z + 1.d0)/2.d0

      CASE(27)

         D_psi_i(1) = (-2.d0*x)*(1.d0 - y2)*(1.d0 - z2)
         D_psi_i(2) = (1.d0 - x2)*(-2.d0*y)*(1.d0 - z2)
         D_psi_i(3) = (1.d0 - x2)*(1.d0 - y2)*(-2.d0*z)

     CASE DEFAULT

        WRITE(*,*) 'ERROR: not supported Dof in Quadrangle gradient'
        STOP

     END SELECT
    
   END FUNCTION gradient_ref_HEX_Q2
   !===============================

   !=========================================
   FUNCTION DOFs_ref_HEX_Q2(el) RESULT(x_dof)
   !=========================================

    IMPLICIT NONE

    CLASS(hexahedron) :: el

    REAL(KIND=8), DIMENSION(27, 3) :: x_dof
    !--------------------------------------

    x_dof(1,  :) = (/ -1.d0, -1.d0, -1.d0 /)
    x_dof(2,  :) = (/  1.d0, -1.d0, -1.d0 /)
    x_dof(3,  :) = (/  1.d0,  1.d0, -1.d0 /)
    x_dof(4,  :) = (/ -1.d0,  1.d0, -1.d0 /)
    x_dof(5,  :) = (/ -1.d0, -1.d0,  1.d0 /)
    x_dof(6,  :) = (/  1.d0, -1.d0,  1.d0 /)
    x_dof(7,  :) = (/  1.d0,  1.d0,  1.d0 /)
    x_dof(8,  :) = (/ -1.d0,  1.d0,  1.d0 /)
    x_dof(9,  :) = (/  0.d0, -1.d0, -1.d0 /)
    x_dof(10, :) = (/ -1.d0,  0.d0, -1.d0 /)
    x_dof(11, :) = (/ -1.d0, -1.d0,  0.d0 /)
    x_dof(12, :) = (/  1.d0,  0.d0, -1.d0 /)
    x_dof(13, :) = (/  1.d0, -1.d0,  0.d0 /)
    x_dof(14, :) = (/  0.d0,  1.d0, -1.d0 /)
    x_dof(15, :) = (/  1.d0,  1.d0,  0.d0 /)
    x_dof(16, :) = (/ -1.d0,  1.d0,  0.d0 /)
    x_dof(17, :) = (/  0.d0, -1.d0,  1.d0 /) 
    x_dof(18, :) = (/ -1.d0,  0.d0,  1.d0 /)
    x_dof(19, :) = (/  1.d0,  0.d0,  1.d0 /)
    x_dof(20, :) = (/  0.d0,  1.d0,  1.d0 /)
    x_dof(21, :) = (/  0.d0,  0.d0, -1.d0 /)
    x_dof(22, :) = (/  0.d0, -1.d0,  0.d0 /)
    x_dof(23, :) = (/ -1.d0,  0.d0,  0.d0 /)
    x_dof(24, :) = (/  1.d0,  0.d0,  0.d0 /)
    x_dof(25, :) = (/  0.d0,  1.d0,  0.d0 /)
    x_dof(26, :) = (/  0.d0,  0.d0,  1.d0 /)
    x_dof(27, :) = (/  0.d0,  0.d0,  0.d0 /)  
    
  END FUNCTION DOFs_ref_HEX_Q2
  !===========================

  !==============================================
  FUNCTION face_trace_HEX_Q2(el, jf) RESULT(xi_f)
  !==============================================

    IMPLICIT NONE

    CLASS(hexahedron)        :: el
    INTEGER,      INTENT(IN) :: jf

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
    !----------------------------------------------

    ALLOCATE( xi_f(9, 3) )

    SELECT CASE(jf)

    CASE(1)

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/  1.d0, -1.d0, -1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0, -1.d0 /)
       xi_f(4, :) = (/ -1.d0,  1.d0, -1.d0 /)
       xi_f(5, :) = (/  0.d0, -1.d0, -1.d0 /)
       xi_f(6, :) = (/  1.d0,  0.d0, -1.d0 /)
       xi_f(7, :) = (/  0.d0,  1.d0, -1.d0 /)
       xi_f(8, :) = (/ -1.d0,  0.d0, -1.d0 /)
       xi_f(9, :) = (/  0.d0,  0.d0, -1.d0 /)


    CASE(2)

       xi_f(1, :) = (/ -1.d0, -1.d0,  1.d0 /)
       xi_f(2, :) = (/  1.d0, -1.d0,  1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/ -1.d0,  1.d0,  1.d0 /)
       xi_f(5, :) = (/  0.d0, -1.d0,  1.d0 /)
       xi_f(6, :) = (/  1.d0,  0.d0,  1.d0 /)
       xi_f(7, :) = (/  0.d0,  1.d0,  1.d0 /)
       xi_f(8, :) = (/ -1.d0,  0.d0,  1.d0 /)
       xi_f(9, :) = (/  0.d0,  0.d0,  1.d0 /)

    CASE(3)

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /)
       xi_f(2, :) = (/  1.d0, -1.d0, -1.d0 /)
       xi_f(3, :) = (/  1.d0, -1.d0,  1.d0 /)
       xi_f(4, :) = (/ -1.d0, -1.d0,  1.d0 /)
       xi_f(5, :) = (/  0.d0, -1.d0, -1.d0 /)
       xi_f(6, :) = (/  1.d0, -1.d0,  0.d0 /)
       xi_f(7, :) = (/  0.d0, -1.d0,  1.d0 /)
       xi_f(8, :) = (/ -1.d0, -1.d0,  0.d0 /)
       xi_f(9, :) = (/  0.d0, -1.d0,  0.d0 /)

    CASE(4)

       xi_f(1, :) = (/  1.d0, -1.d0, -1.d0 /) 
       xi_f(2, :) = (/  1.d0,  1.d0, -1.d0 /)
       xi_f(3, :) = (/  1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/  1.d0, -1.d0,  1.d0 /)
       xi_f(5, :) = (/  1.d0,  0.d0, -1.d0 /) 
       xi_f(6, :) = (/  1.d0,  1.d0,  0.d0 /)
       xi_f(7, :) = (/  1.d0,  0.d0,  1.d0 /)
       xi_f(8, :) = (/  1.d0, -1.d0,  0.d0 /)
       xi_f(9, :) = (/  1.d0,  0.d0,  0.d0 /)

    CASE(5)

       xi_f(1, :) = (/  1.d0,  1.d0, -1.d0 /) 
       xi_f(2, :) = (/ -1.d0,  1.d0, -1.d0 /) 
       xi_f(3, :) = (/ -1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/  1.d0,  1.d0,  1.d0 /)
       xi_f(5, :) = (/  0.d0,  1.d0, -1.d0 /) 
       xi_f(6, :) = (/ -1.d0,  1.d0,  0.d0 /) 
       xi_f(7, :) = (/  0.d0,  1.d0,  1.d0 /)
       xi_f(8, :) = (/  1.d0,  1.d0,  0.d0 /)
       xi_f(9, :) = (/  0.d0,  1.d0,  0.d0 /)

    CASE(6)

       xi_f(1, :) = (/ -1.d0, -1.d0, -1.d0 /) 
       xi_f(2, :) = (/ -1.d0, -1.d0,  1.d0 /)
       xi_f(3, :) = (/ -1.d0,  1.d0,  1.d0 /)
       xi_f(4, :) = (/ -1.d0,  1.d0, -1.d0 /)
       xi_f(5, :) = (/ -1.d0, -1.d0,  0.d0 /) 
       xi_f(6, :) = (/ -1.d0,  0.d0,  1.d0 /)
       xi_f(7, :) = (/ -1.d0,  1.d0,  0.d0 /)
       xi_f(8, :) = (/ -1.d0,  0.d0, -1.d0 /)
       xi_f(9, :) = (/ -1.d0,  0.d0,  0.d0 /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong Hexahedron face in face'
       STOP

    END SELECT    
    
  END FUNCTION face_trace_HEX_Q2
  !=============================

  !====================================
  SUBROUTINE init_quadrature_HEX_Q2(el)
  !====================================

    IMPLICIT NONE

    CLASS(hexahedron) :: el
    !----------------------------------------------

    el%N_quad = 27

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%D_phi_q(el%N_dim, el%N_points, el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 3) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )

    !-------------------
    ! Quadrature formula
    !--------------------------------------
    el%x_q(1 ,:) = (/ -DSQRT(0.6d0),  -DSQRT(0.6d0),  -DSQRT(0.6d0) /)
    el%x_q(2 ,:) = (/          0.d0,  -DSQRT(0.6d0),  -DSQRT(0.6d0) /)
    el%x_q(3 ,:) = (/  DSQRT(0.6d0),  -DSQRT(0.6d0),  -DSQRT(0.6d0) /)
    el%x_q(4 ,:) = (/ -DSQRT(0.6d0),           0.d0,  -DSQRT(0.6d0) /)
    el%x_q(5 ,:) = (/          0.d0,           0.d0,  -DSQRT(0.6d0) /)
    el%x_q(6 ,:) = (/  DSQRT(0.6d0),           0.d0,  -DSQRT(0.6d0) /)
    el%x_q(7 ,:) = (/ -DSQRT(0.6d0),   DSQRT(0.6d0),  -DSQRT(0.6d0) /)
    el%x_q(8 ,:) = (/          0.d0,   DSQRT(0.6d0),  -DSQRT(0.6d0) /)
    el%x_q(9 ,:) = (/  DSQRT(0.6d0),   DSQRT(0.6d0),  -DSQRT(0.6d0) /)
    el%x_q(10,:) = (/ -DSQRT(0.6d0),  -DSQRT(0.6d0),           0.d0 /)
    el%x_q(11,:) = (/          0.d0,  -DSQRT(0.6d0),           0.d0 /)
    el%x_q(12,:) = (/  DSQRT(0.6d0),  -DSQRT(0.6d0),           0.d0 /)
    el%x_q(13,:) = (/ -DSQRT(0.6d0),           0.d0,           0.d0 /)
    el%x_q(14,:) = (/          0.d0,           0.d0,           0.d0 /)
    el%x_q(15,:) = (/  DSQRT(0.6d0),           0.d0,           0.d0 /)
    el%x_q(16,:) = (/ -DSQRT(0.6d0),   DSQRT(0.6d0),           0.d0 /)
    el%x_q(17,:) = (/          0.d0,   DSQRT(0.6d0),           0.d0 /)
    el%x_q(18,:) = (/  DSQRT(0.6d0),   DSQRT(0.6d0),           0.d0 /)
    el%x_q(19,:) = (/ -DSQRT(0.6d0),  -DSQRT(0.6d0),   DSQRT(0.6d0) /)
    el%x_q(20,:) = (/          0.d0,  -DSQRT(0.6d0),   DSQRT(0.6d0) /)
    el%x_q(21,:) = (/  DSQRT(0.6d0),  -DSQRT(0.6d0),   DSQRT(0.6d0) /)
    el%x_q(22,:) = (/ -DSQRT(0.6d0),           0.d0,   DSQRT(0.6d0) /)
    el%x_q(23,:) = (/          0.d0,           0.d0,   DSQRT(0.6d0) /)
    el%x_q(24,:) = (/  DSQRT(0.6d0),           0.d0,   DSQRT(0.6d0) /)
    el%x_q(25,:) = (/ -DSQRT(0.6d0),   DSQRT(0.6d0),   DSQRT(0.6d0) /)
    el%x_q(26,:) = (/          0.d0,   DSQRT(0.6d0),   DSQRT(0.6d0) /)
    el%x_q(27,:) = (/   DSQRT(0.6d0),  DSQRT(0.6d0),   DSQRT(0.6d0) /)
   
    el%w_q(1)  = 125.d0/729.d0 
    el%w_q(2)  = 200.d0/729.d0 
    el%w_q(3)  = 125.d0/729.d0 
    el%w_q(4)  = 200.d0/729.d0 
    el%w_q(5)  = 320.d0/729.d0 
    el%w_q(6)  = 200.d0/729.d0 
    el%w_q(7)  = 125.d0/729.d0 
    el%w_q(8)  = 200.d0/729.d0 
    el%w_q(9)  = 125.d0/729.d0 
    el%w_q(10) = 200.d0/729.d0 
    el%w_q(11) = 320.d0/729.d0 
    el%w_q(12) = 200.d0/729.d0 
    el%w_q(13) = 320.d0/729.d0 
    el%w_q(14) = 512.d0/729.d0
    el%w_q(15) = 320.d0/729.d0 
    el%w_q(16) = 200.d0/729.d0 
    el%w_q(17) = 320.d0/729.d0 
    el%w_q(18) = 200.d0/729.d0 
    el%w_q(19) = 125.d0/729.d0 
    el%w_q(20) = 200.d0/729.d0
    el%w_q(21) = 125.d0/729.d0
    el%w_q(22) = 200.d0/729.d0
    el%w_q(23) = 320.d0/729.d0
    el%w_q(24) = 200.d0/729.d0 
    el%w_q(25) = 125.d0/729.d0 
    el%w_q(26) = 200.d0/729.d0
    el%w_q(27) = 125.d0/729.d0
    !--------------------------------------

    el%xx_q = 0.d0

  END SUBROUTINE init_quadrature_HEX_Q2
  !====================================

END MODULE Hexahedron_Class
