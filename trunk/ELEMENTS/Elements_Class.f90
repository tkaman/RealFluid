MODULE Elements_Class

  USE Face_Class

  IMPLICIT NONE

  !==================================
  INTEGER, PARAMETER :: TRI_P1 = 10
  INTEGER, PARAMETER :: TRI_P2 = 11

  INTEGER, PARAMETER :: QUA_Q1 = 20
  INTEGER, PARAMETER :: QUA_Q2 = 21

  INTEGER, PARAMETER :: TET_P1 = 30
  INTEGER, PARAMETER :: TET_P2 = 31

  INTEGER, PARAMETER :: HEX_Q1 = 40
  INTEGER, PARAMETER :: HEX_Q2 = 41

  INTEGER, PARAMETER :: PYR_P1 = 50
  INTEGER, PARAMETER :: PYR_P2 = 51
  !==================================

  !----------------------------------
  TYPE :: faces_ptr     
     CLASS(face_str), POINTER :: f     
  END TYPE faces_ptr
  !----------------------------------

  !-------------------------------------------
  !TYPE b_e_con_str, defined in face_class
  !-------------------------------------------

  !==================================
  TYPE, PUBLIC :: element_str

     INTEGER :: Type     = 0 ! Element type
     INTEGER :: N_dim    = 0 ! # spatial dimension
     INTEGER :: N_verts  = 0 ! # vertices
     INTEGER :: N_points = 0 ! # DoFs
     INTEGER :: N_Faces  = 0 ! # faces

     !------------------
     ! Element geometry
     !-------------------------------------------------
     ! Vertices coordinates
     ! Coords(id, j): id = 1, N_dim, j = 1, N_Points
     REAL(KIND=8), DIMENSION(:,:), POINTER :: Coords
     !-------------------------------------------------
     !
     ! Distance from the nearest wall
     ! d_min(j): j = 1, N_Points
     !-------------------------------------------------
     !
     REAL(KIND=8), DIMENSION(:), POINTER :: d_min
     !-------------------------------------------------
     !
     ! Volume of the face
     REAL(KIND=8) :: Volume = 0.d0

     !--------------
     ! Element Topology
     !-------------------------------------------------
     ! Nodes (global numeration of the mesh)
     ! NU(j), j = 1, N_points
     INTEGER,      DIMENSION(:),   POINTER :: NU => NULL()
     !-------------------------------------------------
     !
     ! Connectivity between the node and the elements
     ! which share the same node
     ! b_e_con(j)%je, j = 1, N_points, je = 1, # ele(bubble)
     TYPE(b_e_con_str), DIMENSION(:), POINTER :: b_e_con => NULL()
     !-------------------------------------------------
     !     
     ! Normals for the RD scheme
     ! rd_n(id, j), id = 1, N_dim, j = 1, N_points
     !-------------------------------------------------
     REAL(KIND=8), DIMENSION(:,:), POINTER :: rd_n => NULL()

     !------------
     ! Quadrature
     !------------------------------------------------------------
     ! # quadrature points
     INTEGER :: N_quad = 0
     !------------------------------------------------------------
     !
     ! Quadrature weighs (multiplied by the jacobian)
     ! w_q(iq), iq = 1, N_quad
     !------------------------------------------------------------
     REAL(KIND=8), DIMENSION(:),     POINTER :: w_q => NULL()
     !
     ! Reference Coordinates of the quadrature points
     ! x_q(ik, iq), ik = 1, N_dim_ele, iq = 1, N_quad
     !------------------------------------------------------------
     REAL(kind=8), DIMENSION(:,:),   POINTER :: x_q => NULL()
     !
     ! Physical coordinates of the quadrature points
     ! xx_q(id, iq), , id = 1, N_dim, iq = 1, N_quad
     REAL(kind=8), DIMENSION(:,:),   POINTER :: xx_q => NULL()
     !------------------------------------------------------------
     !
     ! Value of basis functions at the quadrature point
     ! phi_q(i, iq), i = 1, N_points,  iq = 1, N_quad
     REAL(KIND=8), DIMENSION(:,:),   POINTER :: phi_q => NULL()
     !------------------------------------------------------------
     !
     ! Gradient of basis functions at the quadrature point
     ! D_phi_q(id, i, iq), 
     ! id =1, N_dim, i = 1, N_points,  iq = 1, N_quad
     REAL(KIND=8), DIMENSION(:,:,:), POINTER :: D_phi_q => NULL()
     !
     ! Gradient of the basis functions at the DOFs
     !  D_phi_k(id, i, k)
     ! id =1, N_dim, i = 1, N_points,  k = 1, N_points
     REAL(KIND=8), DIMENSION(:,:,:), POINTER :: D_phi_k => NULL()

     !------------------
     ! Gradient recovery
     !------------------------------------------------------------
     ! # sampling points
     INTEGER :: N_sampl
     ! Reference Coordinates of the recovery points
     ! xx_q(ik, iq), , ik = 1, N_dim_ele, iq = 1, N_sampling_pt
     !------------------------------------------------------------
     REAL(kind=8), DIMENSION(:,:),   POINTER :: x_R   => NULL()
     ! Physical coordinates of the recovery points
     ! xx_q(id, iq), , id = 1, N_dim, iq = 1, N_sampling_pt
     REAL(kind=8), DIMENSION(:,:),   POINTER :: xx_R  => NULL()
     !------------------------------------------------------------
     ! Gradient of basis functions at the recovery point
     ! D_phi_q(id, i, iq), 
     ! id =1, N_dim, i = 1, N_points,  iq = 1, N_sampling_pt
     REAL(KIND=8), DIMENSION(:,:,:), POINTER :: D_phi_R => NULL()

     !---------------
     !Faces structure
     !---------------------------------------------------
     TYPE(faces_ptr), DIMENSION(:), ALLOCATABLE :: faces




CONTAINS

  GENERIC  , PUBLIC :: interpolation => mat_interpolation,      &
       &                                vec_interpolation,      &
       &                                scal_interpolation,     &
       &                                mat_grad_interpolation, &
       &                                vec_grad_interpolation, &
       &                                scal_grad_interpolation

  PROCEDURE, PUBLIC :: phys2ctrl     => phys2ctrl_element

  PROCEDURE, PUBLIC :: printf        => print_element

  PROCEDURE, PRIVATE, PASS :: mat_interpolation       => &
       &                      mat_interpolation_element
  PROCEDURE, PRIVATE, PASS :: vec_interpolation       => &
       &                      vec_interpolation_element
  PROCEDURE, PRIVATE, PASS :: scal_interpolation      => &
       &                      scal_interpolation_element
  PROCEDURE, PRIVATE, PASS :: mat_grad_interpolation  => &
       &                      mat_grad_interpolation_element
  PROCEDURE, PRIVATE, PASS :: vec_grad_interpolation  => &
       &                      vec_grad_interpolation_element
  PROCEDURE, PRIVATE, PASS :: scal_grad_interpolation => &
       &                      scal_grad_interpolation_element

END TYPE element_str
!==================================

PRIVATE :: phys2ctrl_element
PRIVATE :: print_element

CONTAINS

  !---------------------------------------------------------------------
  !!> \brief ne fait rien.. mais en nurbs remplace var par ses vals de ctrl
  !!\author Algiane 2012
  !---------------------------------------------------------------------
  !===================================================
FUNCTION phys2ctrl_element(this,Var) RESULT(VarCtrl)
  !=================================================
 IMPLICIT NONE

 CLASS(element_str) , INTENT(IN) :: this
 REAL,DIMENSION(:,:), INTENT(IN) :: Var

 REAL,DIMENSION(SIZE(Var,1),SIZE(Var,2)) :: VarCtrl
 TYPE(element_str) :: toto ! pour le warning
 ! Evite un warning unused
 toto%TYPE = this%TYPE

 VarCtrl(:,:) =Var(:,:)

END FUNCTION phys2ctrl_element

!===============================
!!>\brief Interpolation des valeurs de la matrice f aux
!! noeuds de  l'element sur les fonctions de bases
!! evaluees au point de quadrature iq
!! \author Algiane 2012
!===========================================================
FUNCTION mat_interpolation_element(this, f, iq) RESULT( f_i)
  !=========================================================
 IMPLICIT NONE

 CLASS(element_str)    , INTENT(IN) :: this
 REAL, DIMENSION(:,:,:), INTENT(IN) :: f
 INTEGER,                INTENT(IN) :: iq

 REAL, DIMENSION(SIZE(f,1),SIZE(f,2)):: f_i
 !---------------------------------------
 INTEGER ::k

 IF(SIZE(f,3)/=this%N_points)THEN
    PRINT*," Mat_grad_interpolation_element : ERREUR "
    PRINT*," La matrice n'est pas au bon format. "
    STOP
 ENDIF

 f_i = 0.0
 DO k = 1, this%N_points
    f_i = f_i + f(:,:, k) * this%phi_q(k,iq)
 ENDDO

END FUNCTION mat_interpolation_element
!=====================================

!!>\brief Interpolation des valeurs du vecteur f aux
!! noeuds de  l'element sur les fonctions de bases
!! evaluees au point de quadrature iq
!! \author Algiane 2012
!===========================================================
FUNCTION vec_interpolation_element(this, f, iq) RESULT( f_i)
  !=========================================================
 IMPLICIT NONE

 CLASS(element_str)  , INTENT(IN) :: this
 REAL, DIMENSION(:,:), INTENT(IN) :: f
 INTEGER,              INTENT(IN) :: iq
 REAL, DIMENSION(SIZE(f,1))       :: f_i
 !---------------------------------------
 INTEGER ::k



 f_i = 0.0
 DO k = 1, this%N_points
    f_i = f_i + f(:, k) * this%phi_q(k,iq)
 ENDDO

END FUNCTION vec_interpolation_element
!=====================================

!!>\brief Interpolation des valeurs du scalaire f aux
!! noeuds de  l'element sur les fonctions de bases
!! evaluees au point de quadrature iq
!! \author Algiane 2012
!============================================================
FUNCTION scal_interpolation_element(this, f, iq) RESULT( f_i)
  !==========================================================
 IMPLICIT NONE

 CLASS(element_str)  , INTENT(IN) :: this
 REAL, DIMENSION(:)  , INTENT(IN) :: f
 INTEGER,              INTENT(IN) :: iq
 REAL                             :: f_i
 !---------------------------------------
 INTEGER ::k



 f_i = 0.0
 DO k = 1, this%N_points
    f_i = f_i + f( k) * this%phi_q(k,iq)
 ENDDO

END FUNCTION scal_interpolation_element
!======================================

!!>\brief Interpolation des valeurs d'une matrice f aux noeuds
!!        de l'element sur la derivee partielle suivant idim des
!!        fonctions de bases evaluees
!!        au point de quadrature iq
!! \author Algiane 2012
!============================================================================
FUNCTION mat_grad_interpolation_element(this, f, iq, pt, idim) RESULT( f_i)
  !==========================================================================
 IMPLICIT NONE

 CLASS(element_str)    , INTENT(IN) :: this
 REAL, DIMENSION(:,:,:), INTENT(IN) :: f
 INTEGER,                INTENT(IN) :: iq
 INTEGER,                INTENT(IN) :: pt
 INTEGER,                INTENT(IN) :: idim

 REAL, DIMENSION(SIZE(f,1),SIZE(f,2)):: f_i
 !---------------------------------------
 REAL, DIMENSION(:,:,:),     POINTER :: p
 INTEGER ::k

 IF(SIZE(f,3)/=this%N_points)THEN
    PRINT*," Mat_grad_interpolation_element : ERREUR "
    PRINT*," La matrice n'est pas au bon format. "
    STOP
 ENDIF

 f_i = 0.0

 SELECT CASE(pt)
 CASE(DOF)
    p => this%D_phi_k
 CASE(PT_QUAD)
    p => this%D_phi_q
 CASE DEFAULT
    PRINT*, " mat_grad_interpolation_element : "
    PRINT*, " Erreur de point de calcul : pt=", pt
    STOP
 END SELECT

 DO k = 1, this%N_points
    f_i = f_i + f(:,:, k) * p(idim, k, iq)
 ENDDO

END FUNCTION  mat_grad_interpolation_element
!===========================================

!!>\brief Interpolation des valeurs d'un vecteur f aux noeuds
!!        de l'element sur la derivee partielle suivant idim des
!!        fonctions de bases evaluees
!!         au point de quadrature iq
!! \author Algiane 2012
!============================================================================
FUNCTION vec_grad_interpolation_element(this, f, iq, pt, idim)  RESULT( f_i)
  !==========================================================================
 IMPLICIT NONE

 CLASS(element_str)  , INTENT(IN) :: this
 REAL, DIMENSION(:,:), INTENT(IN) :: f
 INTEGER,              INTENT(IN) :: iq
 INTEGER,              INTENT(IN) :: pt
 INTEGER,              INTENT(IN) :: idim

 REAL, DIMENSION(SIZE(f,1))          :: f_i
 !---------------------------------------
 REAL, DIMENSION(:,:,:),     POINTER :: p
 INTEGER ::k

 f_i = 0.0

 SELECT CASE(pt)
 CASE(DOF)
    p => this%D_phi_k
 CASE(PT_QUAD)
    p => this%D_phi_q
 CASE DEFAULT
    PRINT*, " Vec_grad_interpolation_element : "
    PRINT*, " Erreur de point de calcul : pt=", pt
    STOP
 END SELECT

 DO k = 1, this%N_points
    f_i = f_i + f(:, k) * p(idim, k, iq)
 ENDDO

END FUNCTION  vec_grad_interpolation_element
!============================================


!!>\brief Interpolation des valeurs d'un scalaire f aux noeuds
!!        de l'element sur la derivee partielle suivant idim des
!!        fonctions de bases evaluees
!!        au point de quadrature iq
!! \author Algiane 2012
!=============================================================================
FUNCTION scal_grad_interpolation_element(this, f, iq, pt, idim) RESULT( f_i)
  !===========================================================================
 IMPLICIT NONE

 CLASS(element_str)  , INTENT(IN) :: this
 REAL, DIMENSION(:)  , INTENT(IN) :: f
 INTEGER,              INTENT(IN) :: iq
 INTEGER,              INTENT(IN) :: pt
 INTEGER,              INTENT(IN) :: idim

 REAL                             :: f_i
 !--------------------------------------------
 REAL, DIMENSION(:,:,:),  POINTER :: p
 INTEGER ::k

 f_i = 0.0

 SELECT CASE(pt)
 CASE(DOF)
    p => this%D_phi_k
 CASE(PT_QUAD)
    p => this%D_phi_q
 CASE DEFAULT
    PRINT*, " Vec_grad_interpolation_element : "
    PRINT*, " Erreur de point de calcul : pt=", pt
    STOP
 END SELECT

 DO k = 1, this%N_points
    f_i = f_i + f(k) * p(idim, k,iq)
 ENDDO

END FUNCTION  scal_grad_interpolation_element
!============================================


!!>\brief Ecriture d'un element dans le fichier MyUnit
!! \author Algiane 2012
!===================================
SUBROUTINE print_element(this, MyUnit)
  !=================================
 IMPLICIT NONE

 CLASS(element_str), INTENT(IN) :: this
 INTEGER           , INTENT(IN) :: MyUnit

 INTEGER :: stat1, stat2, stat3, i, j

 WRITE(MyUnit, IOSTAT = stat1) this%N_Verts,  &
      this%N_points, &
      this%Type

 WRITE(MyUnit, IOSTAT = stat2) ( (this%Coords(i, j),  &
      i = 1, this%N_dim), &
      j = 1, this%N_points )

 WRITE(MyUnit, IOSTAT = stat3) (  this%NU(i), &
      i = 1,this%N_points )

END SUBROUTINE print_element
!=========================

END MODULE Elements_Class
