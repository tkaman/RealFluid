!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

PROGRAM O2toO3
  USE LesTypes 
  USE Reprise 
  USE Visu
  USE Inputs
  USE ReadMEsh
  IMPLICIT NONE
  CHARACTER(LEN = *), PARAMETER :: mod_name = "O2toO3"
  TYPE(MeshCom)       :: ComO2,ComO3
  TYPE(MeshDef)       :: MeshO2,MeshO3
  TYPE(Variables)     :: VarO2,VarO3
  TYPE(Donnees)       :: data
  INTEGER             :: Stat, ind, iargc, i
  CHARACTER(LEN = 70) :: FileToConvert, type, exec, num ! sans l'extension .save
  CHARACTER(LEN = 2), DIMENSION(: ), ALLOCATABLE :: tab

  CALL info_ifdef()

  Stat = iargc()
  CALL getarg(0, exec)
  IF (Stat == 0) THEN
     PRINT *, Trim(exec), " prend 2 arguments OU PLUS (pour medit) : le type de visualisation, le fichier a convertir"
     PRINT *, "Le nom du fichier a convertir doit se terminer par .save (ou par .<qqch faisant 4 caracteres>)"
     PRINT *, "    et doit contenir un -, separateur du type de run et du pas de temps"
     PRINT *, "Les types de visualisation disponibles sont :"
     PRINT *, "  ParaviewVTU"
     PRINT *, "  ParaviewVTP"
     PRINT *, "  DX"
     PRINT *, "  GNUplot / XMGR"
     PRINT *, "  VigieVC_HDB"
     PRINT *, "  VigieVC_Ascii"
     PRINT *, "  Tecplot, tecplot, TECPLOT"
     PRINT *, "  MEDITwall"
     PRINT *, "  MEDIT, medit : preciser en 3eme argument ++ le ou les champs selectionnes : 1, '2 3' ou '2 3 4', ..."
     PRINT *, "  FlPar : flux parietaux"
     PRINT *, "  NetCDF (ASCII)"
#ifdef VISIT
     PRINT *, "  visit, Visit, VisIt (format silo pour visit)"
#endif
     CALL delegate_stop()
  END IF
  IF (Stat < 2) THEN
     GO TO 999 
  END IF!-- va imprimer exec
  CALL getarg(1, type)
  CALL getarg(2, FileToConvert)

  ind = Index(FileToConvert, " ")
  FileToConvert(ind-5: ind-1) = Repeat(" ", 5)
  ind = Index(FileToConvert, "-")
  data%RootName = FileToConvert(1: ind-1)

  CALL Reprise_visu(data, MeshO2, VarO2, trim(FileToConvert)//"O2" ) !-- lecture des donn�es, en Fortran, code O2
  CALL Reprise_visu(data, MeshO3, VarO3, trim(FileToConvert)//"O3" ) !-- lecture des donn�es, en Fortran, code O3
  !On merge les donnees du O2 au mailalge du O3

  call Reprise_out(data,MeshO3,VarO2,"O3")
  STOP "Fin du programme principal"
999 CONTINUE
  PRINT *, "ERREUR : donner 1) le type de visu en argument, 2) le nom du fichier a convertir."
  PRINT *, "  Ce nom doit se terminer par .save (ou par .<qqch faisant 4 caracteres>)"
  PRINT *, "    et doit contenir un -, separateur du type de run et du pas de temps"
  PRINT *, "ex: ", Trim(exec), " type fichier.save"
  PRINT *, "     type={ParaviewVTK, DX, GNUplot, XMGR, VigieVC_HDB, VigieVC_Ascii2D, MEDIT, medit,"
  PRINT *, "           MEDIT2, medit2, MEDIT3, medit3, NetCDF}"

END PROGRAM O2toO3
