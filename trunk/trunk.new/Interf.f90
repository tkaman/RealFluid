
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !> \brief        Programme de decomposition de domaine
  !!
  !!         pour les maillages non structur�s, construction des
  !!   sous-dommaines a partir du fichier sortie du programme interf
  !!\author B.Nkonga, modifi� par M. Papin, �lagu� par P. Jacq
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

PROGRAM INTERf3d
  USE LesTypes
  USE ReadMesh
  USE GeomGraph
  USE Extract

  IMPLICIT NONE
  INCLUDE "scotchf.h"
  CHARACTER(LEN=*), PARAMETER :: mod_name = "INTERF"

  ! declarations pour scotch
  ! ------------------------------------------------------
  INTEGER                          :: BaseVal, VertNbr, EdgeNbr
  INTEGER, DIMENSION(:),  POINTER  :: VeLoTab, VertTab
  INTEGER, DIMENSION(:),  POINTER  :: EdgeTab, EdLoTab
  INTEGER, DIMENSION(:),  POINTER  :: PartTab
  DOUBLE PRECISION                 :: Graph(Scotch_GraphDim), StratDat(Scotch_StratDim)
  INTEGER                          :: StatInfo

  ! Lecture Du maillage initial
  TYPE(MeshDef)     :: MeshOri, MeshP1
  TYPE(MeshCom)     :: Comm
  CHARACTER(LEN=80) :: PbName , MeshFile
  CHARACTER(LEN=80) ::  ArgString

  ! variables locales
  INTEGER                                :: Num, npr, Ndom, nb_args
  TYPE(MeshCom), DIMENSION(:),  POINTER  :: Comms

  ! Mesures de temps
  INTEGER :: t0, t1, dt, i, jt

  ! Tableaux de construction des maillages/Comm : A passer sous forme de structure une fois finalis�
  INTEGER, DIMENSION(:,:)  , POINTER :: Glob2Loc       !< [Npoint,Nproc] Numerotation Globale vers Maillage local
  INTEGER, DIMENSION(:,:,:), POINTER :: Recv           !< [2, TailleOverlap*DegreMax, Nprocs] Contient pour chaque proc les r�ceptions (id proc, id point)
  INTEGER, DIMENSION(:)    , POINTER :: NbRecv         !< [Nprocs] Contient le nombre de Recv par procs
  INTEGER, DIMENSION(:)    , POINTER :: NbElts, NbPts  !< [Nprocs] Contient le nombre d'�l�ments/points par procs
  ! ORDRE_ELEVE
  TYPE(SegList) , DIMENSION(:) , POINTER :: OvSeg
  LOGICAL                                :: ordre_eleve

  INTEGER, DIMENSION(:), ALLOCATABLE :: Map_P2P1

  WRITE(6,*)
  WRITE(6,*) ' *****************************************************'
  WRITE(6,*) ' *      Programme de decomposition de domaines       *'
  WRITE(6,*) ' *        pour des maillages non structurees         *'
  WRITE(6,*) ' *****************************************************'
  WRITE(6,*)

  ! On lance le timer
  CALL My_Timer(t0)

  CALL don2d()
  nb_args = iargc()

  IF (nb_args < 1) THEN
     PRINT *, mod_name, " ERROR : You didn't gave the number of domain"
     CALL getarg(0, ArgString)
     PRINT *, "USAGE      :: ", TRIM(ArgString), " 8"
     PRINT *, " OR for P2 :: ", TRIM(ArgString), " 8 p2"
     CALL Abort()
  END IF

  CALL getarg(1, ArgString)
  READ(ArgString, FMT = *, ERR=666) npr

  ordre_eleve = .FALSE.
  IF (nb_args > 1) THEN
     CALL getarg(2, ArgString)
     CALL en_minuscules(ArgString, ArgString)
     IF (TRIM(ArgString)=="p2") THEN
        ordre_eleve = .TRUE.
     END IF
  END IF

  IF( Data%MeshOrder == 3 .AND. &
     (.NOT. ordre_eleve) ) THEN

     WRITE(*,*) 'ERROR: mismatch Mesh order and solution order'
     STOP

  ENDIF

  Data%Nordre = 2
  IF (ordre_eleve) THEN

     Data%Nordre = 3
     WRITE(*,*) 'Computing for P2 mesh'

  END IF

  
  !*******************************************************************************************
  !************    Cas un domaine : D�bile donc non g�r�                                ******
  IF( npr == 1 ) THEN
     WRITE(6,*) "On d�compose sur 2 domaines minimum!"
     CALL Abort()
  END IF
  !************ Fin de Cas un domaine                                                   ******
  !*******************************************************************************************

  IF (Data%impre > 5 ) THEN
     WRITE(6,*)  ' Lecture du fichier Maillage  ...  DEB !!!!'
  END IF
   
  CALL decoder_data_meshtype(Data%MeshType, Data%sel_MeshType)
  Data%PbName  = MeshFile
  CALL DonMesh(Comm, MeshOri, Data)

  CALL My_Timer(t1)
  t0 = t1
  WRITE(6,'(2A,F10.3,A)') mod_name, ": Temps lecture maillage : ", t1 *0.001, "s"

  IF (Data%impre > 5 ) THEN
     WRITE(6,*)  ' Construction de la liste des segments  ...'
  END IF

  MeshOri%Ncells = MeshOri%Npoint
  SELECT CASE(MeshOri%ndim)
  CASE(2)
     CALL GeomSegments2d(MeshOri, Data%impre)
  CASE(3)
     CALL GeomSegments3d(Data,  MeshOri)
  END SELECT

  IF (Data%impre > 5 ) THEN
     WRITE(6,*) ' Construction de la liste des segments ...  FIN !!!!'
  END IF
  CALL My_Timer(t1)
  dt = t1 - t0
  WRITE(6,'(2A,F10.3,A)') mod_name, ": Temps Construction segments : ", dt *0.001, "s"
  t0 = t1

  IF( Data%MeshOrder == 3 ) THEN

     ! Construct an equivalent P1 mesh
     ! to partition the graph 
     CALL Mesh_P2toP1(MeshOri, MeshP1, Map_P2P1)

  ENDIF

  IF (Data%impre > 5 ) THEN
     WRITE(6,*) " Decomposition du graphe du maillage ...  DEB!!!!"
  END IF
  IF (Data%impre > 5 ) THEN
     WRITE(6,*) " Mesh type = ", TRIM(Data%MeshType)
  END IF

  ! Dans cette version on n'utilise que SCOTCH !
  IF( Data%MeshOrder == 3 ) THEN
     CALL ScotchDecomp(MeshP1)
  ELSE
     CALL ScotchDecomp(MeshOri)
  ENDIF

  IF (Data%impre > 5 ) THEN
     WRITE(6,*) " Decomposition du graphe du maillage ...  FIN !!!!"
  END IF
  CALL My_Timer(t1)
  dt = t1 - t0
  WRITE(6,'(2A,F10.3,A)') mod_name, ": Temps Scotch : ", dt *0.001, "s"
  t0 = t1

  WRITE(6, *) " Debut de construction des structures de communication "

  IF( Data%MeshOrder == 3 ) THEN
     CALL CalNewMesh(MeshP1, npr, PartTab, NbElts, NbPts, &
                     Glob2Loc, NbRecv, Recv, OvSeg, ordre_eleve)
  ELSE
     CALL CalNewMesh(MeshOri, npr, PartTab, NbElts, NbPts, &
                     Glob2Loc, NbRecv, Recv, OvSeg, ordre_eleve)
  ENDIF

  ! Ici on sait pour chaque sous domaines
  ! le nombre d'elements, de points, 
  ! la correspondance numero global<->numero local, et les r�ceptions
  ! on a tout en main pour construire les sous domaines

  OPEN(77, FILE="stat.dat", FORM="formatted")
  WRITE(77,*) "   Dom   Npoint   Nelemt    NdomV Nsnd2min Nsnd2max Nrcv2min Nrcv2max"

  ! on cr�e les structures de com
  ALLOCATE( Comms(npr) )
  DO Num = 1, npr
     ! Les donn�es sont fig�es cette boucle est parall�lisable (OpenMP)
     ! On calcule tout les recv     
     CALL CalComRecv( Num, npr, NbRecv, Recv, Comms(Num))
  END DO

  CALL My_Timer(t1)
  dt = t1 - t0
  WRITE(6,'(2A,F10.3,A)') mod_name, ": Temps Calcul maillages", dt *0.001, "s"
  t0 = t1

  ! Maintenant on sauve tout
  IF( Data%MeshOrder == 3 ) THEN
     CALL SaveNewMesh(PbName, npr, MeshP1, NbElts, NbPts, &
                      Glob2Loc, Comms, PartTab, OvSeg, ordre_eleve, &
                      MeshOri, Map_P2P1)
  ELSE
     CALL SaveNewMesh(PbName, npr, MeshOri, NbElts, NbPts, &
                      Glob2Loc, Comms, PartTab, OvSeg, ordre_eleve)
  ENDIF

  ! Les donn�es sont fig�es cette routine est parall�lisable (OpenMP)

  CLOSE(77)

  CALL My_Timer(t1)
  dt = t1 - t0
  WRITE(6,'(2A,F10.3,A)') mod_name, ": Temps Ecriture Maillages : ", dt *0.001, "s"
  WRITE(6,'(2A,F10.3,A)') mod_name, ": Temps final : ", t1*0.001 , "s"


  STOP

666 PRINT * , mod_name, " ERROR:" , ArgString, " is not a valid integer"

CONTAINS

  !> Convert our graph into Scotch format
  !! and partition it
  SUBROUTINE ScotchDecomp(Mesh)
    
    TYPE(MeshDef), INTENT(IN) :: Mesh
    !--------------------------------

    BaseVal = 1

    CALL Fbx2ScotchArcs(Mesh%Npoint, Mesh%Nelemt,     & 
                        Mesh%Ndegre, Mesh%Nu,         & 
                        BaseVal, VertNbr, EdgeNbr, VeLoTab, & 
                        VertTab, EdgeTab, EdLoTab )

    CALL ScotchFGraphInit( Graph, StatInfo )

    WRITE(6,*) " Apres ScotchFGraphInit :: StatInfo=", StatInfo
    IF (StatInfo /= 0) THEN
       WRITE(6,*) "Pensez � v�rifier la compatibilit� de Scotch et de FBx en terme de norme 32bits/64bits"
    END IF

    WRITE(6,*) " BaseVal :: ", BaseVal
    WRITE(6,*) " VertNbr :: ", VertNbr
    WRITE(6,*) " EdgeNbr :: ", EdgeNbr

    Ndom = npr

    CALL ScotchFGraphBuild( Graph, BaseVal, &
                   VertNbr, VertTab, VertTab(2), VertTab, VertTab, &
                   EdgeNbr, EdgeTab, EdgeTab,  StatInfo )

    CALL ScotchFGraphCheck(Graph, StatInfo )
    WRITE(6,*) " Apres ScotchFGraphCheck  :: StatInfo=",StatInfo

    CALL ScotchFStratInit(StratDat, StatInfo )
    WRITE(6,*) " Apres ScotchFStratInit  :: StatInfo=",StatInfo

    ALLOCATE( PartTab(VertNbr) )
    CALL ScotchFGraphPart(Graph, Ndom, StratDat, PartTab, StatInfo )
    WRITE(6,*) " Apres ScotchFGraphPart  :: StatInfo=",StatInfo

    WRITE(6,*) " Sortie de Scotch"
    DEALLOCATE(VeLoTab, EdgeTab, EdLoTab, VertTab)
  END SUBROUTINE ScotchDecomp


  !> Open and reads a file containing decomposition data
  SUBROUTINE don2d()
    IMPLICIT NONE
    INTEGER :: MyUnit = 12
    LOGICAL :: existe
    INQUIRE(FILE="Decomp.data", EXIST=existe)
    IF (.NOT. existe) THEN
       PRINT *, mod_name," :: ERREUR : il faudrait fournir un fichier Decomp.data"
       PRINT *, "Exemple:"
       PRINT *, '6                        !!! Verbose level'
       PRINT *, '"MTC3"                   !!! Test case name'
       PRINT *, '"MTC3-000" "meshemc2" 2  !!! Mesh name (without extension), meshtype, mesh_order, N_dim'
       CALL Abort()
    END IF
    OPEN(UNIT=MyUnit, FILE='Decomp.data', FORM='formatted')
    READ(MyUnit,*) Data%impre
    READ(MyUnit,*) PbName
    READ(MyUnit,*) MeshFile, Data%MeshType, Data%MeshOrder, Data%Ndim
    CLOSE(MyUnit)

  END SUBROUTINE don2d

END PROGRAM INTERf3d

