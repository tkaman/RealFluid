MODULE Tetrahedron_Class

  USE Elements_Class
  USE Triangle_F_Class
  USE PlinAlg

  IMPLICIT NONE

  !================================================
  TYPE, PUBLIC, EXTENDS(element_str) :: tetrahedron

     ! < >

   CONTAINS
     
     GENERIC, PUBLIC :: initialize => initialize_tet_d

     ! Doamain element
     PROCEDURE, PRIVATE, PASS :: initialize_tet_d

  END TYPE tetrahedron
  !================================================

  PRIVATE :: init_faces
  PRIVATE :: init_faces_TET_P1
  PRIVATE :: init_faces_TET_P2

  PRIVATE :: volume_quadrature

  PRIVATE :: basis_function
  PRIVATE :: basis_function_TET_P1
  PRIVATE :: basis_function_TET_P2
  
  PRIVATE :: gradient

  PRIVATE :: gradient_ref
  PRIVATE :: gradient_ref_TET_P1
  PRIVATE :: gradient_ref_TET_P2

  PRIVATE :: DOFs_ref
  PRIVATE :: DOFs_ref_TET_P1
  PRIVATE :: DOFs_ref_TET_P2
  
 !PRIVATE :: gradient_trace
 !PRIVATE :: face_trace
  PRIVATE :: face_trace_TET_P1
  PRIVATE :: face_trace_TET_P2  

  PRIVATE :: rd_normal

  PRIVATE :: Compute_Jacobian

  PRIVATE :: recovery_procedure
  PRIVATE :: init_Gradiet_Recovery_TET_P1
  PRIVATE :: init_Gradiet_Recovery_TET_P2

CONTAINS
  
  !===========================================================================
  SUBROUTINE initialize_tet_d(el, mode, Nodes, Coords, Nu_face, n_ele, cn_ele)
  !===========================================================================

    IMPLICIT NONE

    CLASS(tetrahedron)                       :: el
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    INTEGER,      DIMENSION(:,:), INTENT(IN) :: cn_ele
    !-------------------------------------------------

    INTEGER :: i, id, ne
    !-------------------------------------------------

    IF ( mode /= "Element" ) THEN       
       WRITE(*,*) 'ERROR: elment must be initialize in element mode'
       STOP
    ENDIF

    !---------------------------
    ! Attach data to the element
    !---------------------------------------------------
    ALLOCATE( el%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
    ALLOCATE( el%NU(SIZE(Nodes)) )
     
    DO id = 1, SIZE(Coords, 1)
       el%Coords(id, :) = Coords(id, :)
    ENDDO

    el%NU = Nodes

    el%N_dim = SIZE(Coords, 1)

    SELECT CASE( SIZE(Nodes) )

    CASE(4)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = TET_P1 ! Element type
       el%N_verts  = 4      ! # Vertices 
       el%N_points = 4      ! # DoFs       

    CASE(10)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = TET_P2 ! Element type
       el%N_verts  = 4      ! # Vertices 
       el%N_points = 10     ! # DoFs

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Tetrahedon type'
        STOP

     END SELECT

     ! Itialize the faces of the element
     CALL init_faces(el, Nodes, Coords, NU_face, n_ele)

     ! Store the informations at the quadrature points
     CALL volume_quadrature(el)
        
     !--------------------------
     ! Normals for the RD scheme
     !----------------------------------------
     ALLOCATE( el%rd_n(el%N_dim, el%N_verts) )

     DO i = 1, el%N_verts! warning... not sure?!
        el%rd_n(:, i) = rd_normal(el, i)
     ENDDO

     ALLOCATE( el%b_e_con(el%N_points) )

     DO i = 1, el%N_points

         ne = COUNT( cn_ele(i, :) /= 0 ) 

         ALLOCATE( el%b_e_con(i)%cn_ele(ne) )

         el%b_e_con(i)%cn_ele = cn_ele(i, 1:ne)

     ENDDO

     CALL nodal_gradients(el)

     ! Store the information at the recovery points
     CALL recovery_procedure(el)

   END SUBROUTINE initialize_tet_d
   !==============================

   !======================================================
   SUBROUTINE init_faces(el, Nodes, Coords, NU_seg, n_ele)
   !======================================================

     IMPLICIT NONE
    
     CLASS(tetrahedron)                       :: el
     INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
     REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
     INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_seg
     INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
     !-------------------------------------------------

     SELECT CASE(el%Type)
              
     CASE(TET_P1)

        CALL init_faces_TET_P1(el, Nodes, Coords, NU_seg, n_ele)
       
     CASE(TET_P2)

        CALL init_faces_TET_P2(el, Nodes, Coords, NU_seg, n_ele)
       
     CASE DEFAULT

        WRITE(*,*) 'Unknown Tetrahedron type for face initialization'
        WRITE(*,*) 'STOP'
        
     END SELECT

   END SUBROUTINE init_faces
   !========================

  !===============================
  SUBROUTINE volume_quadrature(el)
  !===============================
  !
  ! Store the following  quantities at the quadrature points
  !    - value of the basis function
  !    - normal versor
  !    - weight of the quadrature formula (multipplied by 
  !      the jacobian of the transformation)
  !    - value of the trace of the gradient of basis functions
  !    - physical coordiantes of the quadrature point
  !
  ! Compute the lenght of the segment
  !
    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    !-----------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ

    INTEGER :: iq, k
    !-----------------------------------------------

    SELECT CASE(el%Type)

    CASE(TET_P1)

       CALL init_quadrature_TET_P1(el)
       
    CASE(TET_P2)

      CALL init_quadrature_TET_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tetrahedron type for quadrature'
       WRITE(*,*) 'STOP'

    END SELECT

    !-------------------------------------    
    ! Attach data to the quadrature points
    !---------------------------------------------------
    ALLOCATE( JJ(el%N_dim, el%N_dim) )

    DO iq = 1, el%N_quad

       DO k = 1, el%N_points

          el%phi_q(k, iq) = basis_function( el, k, el%x_q(iq, :) )

          el%xx_q(:, iq) = el%xx_q(:, iq) + &               
                           basis_function( el, k, el%x_q(iq, :) ) * el%Coords(:, k)
 
          el%D_phi_q(:, k, iq) = gradient( el, k, el%x_q(iq, :) )

       ENDDO

       ! remark: not optimized, jacobian computed twice       
       JJ = Compute_Jacobian( el, el%x_q(iq, :) )

       el%w_q(iq) = el%w_q(iq) * determinant(JJ)
      
    ENDDO

    DEALLOCATE( JJ )

    ! Volume of the element
    el%volume = SUM( el%w_q )

     IF( el%volume < 0.d0 ) THEN
        WRITE(*,*) 'ERROR: Wrong node ordering, volume negative'
        WRITE(*,*) el%volume
        STOP
     ENDIF

  END SUBROUTINE volume_quadrature
  !===============================

  !=============================
  SUBROUTINE nodal_gradients(el)
  !=============================
  !
  ! Compute the gradient of the basis functions
  ! at the DOFs of the element
  !
    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    !-----------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof

    INTEGER :: i, k
    !-----------------------------------------------

    ! Baricentric coordinates
    ALLOCATE( x_dof(el%N_points, 4) )
    
    x_dof = DOFs_ref(el)
    
    ALLOCATE( el%D_phi_k(el%N_dim, el%N_points, el%N_points) )

    DO i = 1, el%N_points

       DO k = 1, el%N_points

          el%D_phi_k(:, i, k) = gradient( el, i, x_dof(k, :) )

       ENDDO

    ENDDO
    
    DEALLOCATE(x_dof)

  END SUBROUTINE nodal_gradients
  !=============================

  !================================
  SUBROUTINE recovery_procedure(el)
  !================================

    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    !-----------------------------------------------

    INTEGER :: iq, k
    !-----------------------------------------------

    SELECT CASE(el%Type)

    CASE(TET_P1)

       CALL init_Gradiet_Recovery_TET_P1(el)
       
    CASE(TET_P2)

       CALL init_Gradiet_Recovery_TET_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tetrahedron type for recovery'
       WRITE(*,*) 'STOP'

    END SELECT

    !-------------------------------------    
    ! Attach data to the recovey points
    !---------------------------------------------------
    DO iq = 1, el%N_sampl

       DO k = 1, el%N_points
         
          el%xx_R(:, iq) = el%xx_R(:, iq) + &               
                           basis_function( el, k, el%x_R(iq, :) ) * el%Coords(:, k)

          el%D_phi_R(:, k, iq) = gradient( el, k, el%x_R(iq, :) )

       ENDDO
     
    ENDDO

  END SUBROUTINE recovery_procedure
  !================================

!*******************************************************************************
!*******************************************************************************
!                         COMPLEMENTARY FUNCTIONS                              !
!*******************************************************************************
!*******************************************************************************

  !===============================================
  FUNCTION basis_function(el, i, xi) RESULT(psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(tetrahedron)                     :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(TET_P1)

       psi_i = basis_function_TET_P1(el, i, xi)
       
    CASE(TET_P2)

       psi_i = basis_function_TET_P2(el, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tetrahedron type for basis functions'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION basis_function
  !==========================

  !===============================================
  FUNCTION gradient_ref(el, i, xi) RESULT(D_psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(tetrahedron)                      :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(TET_P1)

       D_psi_i = gradient_ref_TET_P1(el, i, xi)
       
    CASE(TET_P2)

       D_psi_i = gradient_ref_TET_P2(el, i, xi)
     
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tetrahedron type for gradients'
       WRITE(*,*) 'STOP'

    END SELECT    

  END FUNCTION gradient_ref
  !========================

  !===========================================
  FUNCTION gradient(el, i, xi) RESULT(D_psi_i)
  !===========================================
  !
  ! Compute the gradient of the shape function i
  ! on the actual tetrahedron at the point of baricentric
  ! coordinates xi
  !  
    IMPLICIT NONE

    CLASS(tetrahedron)                      :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !--------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ, inv_J
    !---------------------------------------------

    ALLOCATE(    JJ(el%N_dim, el%N_dim) )
    ALLOCATE( inv_J(el%N_dim, el%N_dim) )
        
    JJ = compute_Jacobian( el, xi )
    
    inv_J = inverse_easy(JJ)

    D_Psi_i = MATMUL( inv_J, gradient_ref(el, i, xi) )

    DEALLOCATE( JJ, inv_J )

  END FUNCTION gradient
  !====================
  
  !=====================================
  FUNCTION rd_normal(el, l) RESULT(nn_l)
  !=====================================

    IMPLICIT NONE

    CLASS(tetrahedron)  :: el
    INTEGER, INTENT(IN) :: l

    REAL(KIND=8), DIMENSION(el%N_dim) :: nn_l
    !-----------------------------------------   

    REAL(KIND=8), DIMENSION(el%N_dim) :: UU, VV

    INTEGER :: i, j, k
    !-----------------------------------------

    i = el%faces(l)%f%l_NU(1)
    j = el%faces(l)%f%l_NU(2)
    k = el%faces(l)%f%l_NU(3)

    UU = el%Coords(:, j) - el%Coords(:, i)
    VV = el%Coords(:, k) - el%Coords(:, i)

    nn_l(1) = UU(2)*VV(3) - UU(3)*VV(2) 
    nn_l(2) = UU(3)*VV(1) - UU(1)*VV(3)
    nn_l(3) = UU(1)*VV(2) - UU(2)*VV(1)

    nn_l = -nn_l / 2.d0
    
  END FUNCTION rd_normal
  !=====================

!!$  !=========================================
!!$  SUBROUTINE gradient_trace(el, jf, p_D_phi)
!!$  !=========================================
!!$  !
!!$  ! Compute the trace of the gradients of all shape
!!$  ! functions on the face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(tetrahedron)                          :: el
!!$    INTEGER,                        INTENT(IN)  :: jf
!!$    REAL(KIND=8), DIMENSION(:,:,:), INTENT(OUT) :: p_D_phi
!!$    !-----------------------------------------------------
!!$
!!$    REAL(kind=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    INTEGER :: N_points_ele, N_points_face, k, i
!!$    !-----------------------------------------------------
!!$
!!$    N_points_ele  = e%N_points
!!$    N_points_face = SIZE(p_D_phi, 2)
!!$
!!$    ALLOCATE( xi_f(N_points_face, 4) )
!!$
!!$    xi_f = face_trace(e, jf)
!!$
!!$    DO k = 1, N_points_face      
!!$
!!$       DO i = 1, N_points_ele
!!$
!!$          p_D_phi(:, k, i) = gradient( e, i, xi_f(k,:) )
!!$
!!$       ENDDO
!!$
!!$    ENDDO
!!$    
!!$    DEALLOCATE(xi_f)    
!!$
!!$  END SUBROUTINE gradient_trace
!!$  !============================

!!$  !=======================================
!!$  FUNCTION face_trace(el, jf) RESULT(xi_f)
!!$  !=======================================
!!$  !
!!$  ! Compute the baricentric coordinates on the
!!$  ! face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(tetrahedron)       :: el
!!$    INTEGER,      INTENT(IN) :: jf
!!$    
!!$    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    !---------------------------------------------
!!$     
!!$    SELECT CASE(e%Type)
!!$       
!!$    CASE(TET_P1)
!!$
!!$       ALLOCATE( xi_f(3, 4) )
!!$
!!$       xi_f = face_trace_TET_P1(e, jf)
!!$       
!!$    CASE(TET_P2)
!!$
!!$       ALLOCATE( xi_f(6, 4) )
!!$
!!$       xi_f = face_trace_TET_P2(e, jf)
!!$       
!!$    CASE DEFAULT
!!$
!!$       WRITE(*,*) 'Unknown Tetrahedron type for face trace'
!!$       WRITE(*,*) 'STOP'
!!$
!!$    END SELECT 
!!$
!!$  END FUNCTION face_trace
!!$  !======================

  !==================================
  FUNCTION DOFs_ref(el) RESULT(x_dof)
  !==================================
  !
  ! Compute the coordinates of the DOFs
  ! on the reference element
  !
    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof
    !-------------------------------------------------
     
    SELECT CASE(el%Type)
       
    CASE(TET_P1)

       ALLOCATE( x_dof(el%N_points, 4) )

       x_dof = DOFs_ref_TET_P1(el)
       
    CASE(TET_P2)

       ALLOCATE( x_dof(el%N_points, 4) )

       x_dof = DOFs_ref_TET_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tetrahedron type for DOFs'
       WRITE(*,*) 'STOP'

    END SELECT 

  END FUNCTION DOFs_ref
  !====================

  !===========================================
  FUNCTION Compute_Jacobian(el, xi) RESULT(JJ)
  !===========================================

    IMPLICIT NONE

    CLASS(tetrahedron)                      :: el
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8),  DIMENSION(el%N_dim, el%N_dim) :: JJ
    !------------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: d

    REAL(KIND=8) :: det

    INTEGER :: k, l, m
    !------------------------------------------------

    ALLOCATE( d(el%N_dim, el%N_points) )

    DO m = 1, el%N_points
       d(:, m) = gradient_ref(el, m, xi)
    ENDDO

    ! Construction of the Jacobian matrix
    DO k = 1, el%N_dim

       DO l = 1, el%N_dim

          JJ(l, k) = SUM( el%Coords(k, :) * d(l, :) )

       ENDDO

    ENDDO

    DEALLOCATE( d )

    det = determinant(JJ)

    IF( det <= 0.d0 ) THEN
       WRITE(*,*) 'Negative tetrahedron Jacobian'
       WRITE(*,*) 'STOP'
       STOP
    ENDIF
    
  END FUNCTION Compute_Jacobian
  !============================  

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%    TETRAHEDRON P1  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   !
   !                                             y
   !                                           .
   !                                         ,/
   !                                        /
   !                                     3  
   !                                   ,/|`\ 
   !                                 ,/  |  `\
   !                               ,/    '.   `\
   !                             ,/       |     `\
   !                           ,/         |       `\
   !                         1-----------'.--------2 --> x
   !                           `\.         |      ,/ 
   !                              `\.      |    ,/
   !                                 `\.   '. ,/
   !                                    `\. |/
   !                                       `4
   !                                          `\.
   !                                             ` z

   !==============================================================
   SUBROUTINE init_faces_TET_P1(el, Nodes, Coords, NU_face, n_ele)
   !==============================================================

     IMPLICIT NONE

     CLASS(tetrahedron)                       :: el
     INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
     REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
     INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
     INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
     !-------------------------------------------------

     TYPE(triangle_F), POINTER :: tri_F_pt

     INTEGER,      DIMENSION(:,:), ALLOCATABLE :: loc
     INTEGER,      DIMENSION(:),   ALLOCATABLE :: VV
     REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: RR

     INTEGER :: id, jf, N_ln, istat
     !-------------------------------------------------

      el%N_faces = 4 ! # Faces
             
      ALLOCATE( el%faces(el%N_faces) )

      ! # of local nodes
      N_ln = 3
    
      ! DoFs on the faces
      ALLOCATE( VV(N_ln), loc(el%N_faces, N_ln) )

      ! Coordinates on the faces
      ALLOCATE( RR(SIZE(Coords, 1), N_ln) )

      ! loc(jf, :) Local ordering of
      ! the nodes on the face jf
      loc(1, :) = (/ 2, 3, 4 /)
      loc(2, :) = (/ 1, 4, 3 /)
      loc(3, :) = (/ 4, 1, 2 /)
      loc(4, :) = (/ 2, 1, 3 /)

      DO jf = 1, el%N_faces

         VV = Nodes(    loc(jf, :) )
         RR = Coords(:, loc(jf, :) )

         ALLOCATE(tri_F_pt, STAT=istat)
         IF(istat /=0) THEN
            WRITE(*,*) 'ERROR: failed Tetrahedron faces allocation'
         ENDIF
       
         CALL tri_F_pt%initialize( "InternalFace", &
                                   loc(jf,:), VV, RR, NU_face(jf), n_ele(jf) )

         CALL tri_F_pt%face_quadrature()

         el%faces(jf)%f => tri_F_pt
          
      ENDDO

    DEALLOCATE( VV, RR, loc )

  END SUBROUTINE init_faces_TET_P1
  !===============================

  !======================================================
  FUNCTION basis_function_TET_P1(el, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(tetrahedron)                     :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    SELECT CASE (i)
       
    CASE(1, 2, 3, 4)
       
       psi_i = xi(i)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Tetrahedron baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_TET_P1
  !=================================
  
  !======================================================
  FUNCTION gradient_ref_TET_P1(el, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(tetrahedron)                     :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !--------------------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = (/ -1.d0, -1.d0, -1.d0 /)
       
    CASE(2)

       D_psi_i = (/ 0.d0, 1.d0, 0.d0 /)
       
    CASE(3)

       D_psi_i = (/ 0.d0, 0.d0, 1.d0 /)

    CASE(4)

       D_psi_i = (/ 1.d0, 0.d0, 0.d0 /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Tetrahedron gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_TET_P1
  !===============================

  !=========================================
  FUNCTION DOFs_ref_TET_P1(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(tetrahedron) :: el

    REAL(KIND=8), DIMENSION(4, 4) :: x_dof
    !--------------------------------------

    x_dof(1, :) = (/ 1.d0, 0.d0, 0.d0, 0.d0 /)
    x_dof(2, :) = (/ 0.d0, 1.d0, 0.d0, 0.d0 /)
    x_dof(3, :) = (/ 0.d0, 0.d0, 1.d0, 0.d0 /)
    x_dof(4, :) = (/ 0.d0, 0.d0, 0.d0, 1.d0 /)

  END FUNCTION DOFs_ref_TET_P1
  !===========================

  !==============================================
  FUNCTION face_trace_TET_P1(el, jf) RESULT(xi_f)
  !==============================================

    IMPLICIT NONE

    CLASS(tetrahedron)       :: el
    INTEGER,      INTENT(IN) :: jf

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
    !----------------------------------------------

    ALLOCATE( xi_f(3, 4) )

    SELECT CASE(jf)

    CASE(1)

       xi_f(1, :) = (/ 0.d0, 1.d0, 0.d0, 0.d0 /)
       xi_f(2, :) = (/ 0.d0, 0.d0, 1.d0, 0.d0 /)
       xi_f(3, :) = (/ 0.d0, 0.d0, 0.d0, 1.d0 /)

    CASE(2)

       xi_f(1, :) = (/ 1.d0, 0.d0, 0.d0, 0.d0 /)
       xi_f(2, :) = (/ 0.d0, 0.d0, 0.d0, 1.d0 /)
       xi_f(3, :) = (/ 0.d0, 0.d0, 1.d0, 0.d0 /)

    CASE(3)
  
       xi_f(1, :) = (/ 0.d0, 0.d0, 0.d0, 1.d0 /)
       xi_f(2, :) = (/ 1.d0, 0.d0, 0.d0, 0.d0 /)
       xi_f(3, :) = (/ 0.d0, 1.d0, 0.d0, 0.d0 /)
            
    CASE(4)
  
       xi_f(1, :) = (/ 0.d0, 1.d0, 0.d0, 0.d0 /)
       xi_f(2, :) = (/ 1.d0, 0.d0, 0.d0, 0.d0 /)
       xi_f(3, :) = (/ 0.d0, 0.d0, 1.d0, 0.d0 /)
            
    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong Tetrahedron face in face trace P1'
       STOP

    END SELECT    
    
  END FUNCTION face_trace_TET_P1
  !=============================

  !====================================
  SUBROUTINE init_quadrature_TET_P1(el)
  !====================================

    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    !----------------------------------------------

    REAL(KIND=8) :: alpha_1, beta_1
    !-----------------------------------------------

    el%N_quad = 4

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%D_phi_q(el%N_dim, el%N_points, el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 4) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !--------------------------------------
    beta_1 = ( 5.d0 - SQRT(5.d0) ) / 20.d0
    alpha_1  = 1.d0 - 3.d0*beta_1

    el%x_q(1,:) = (/ alpha_1, beta_1 , beta_1 , beta_1  /)
    el%x_q(2,:) = (/ beta_1 , alpha_1, beta_1 , beta_1  /)
    el%x_q(3,:) = (/ beta_1 , beta_1 , alpha_1, beta_1  /)
    el%x_q(4,:) = (/ beta_1 , beta_1 , beta_1,  alpha_1 /)
    el%w_q(:)    = 1.d0/4.d0
    !--------------------------------------
    
    el%w_q = el%w_q / 6.d0 ! V = (1/6)*det|J|
    
    el%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_TET_P1
  !====================================

  !==========================================
  SUBROUTINE init_Gradiet_Recovery_TET_P1(el)
  !==========================================

    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    !------------------------

    el%N_sampl = 1

    ALLOCATE( el%D_phi_R(el%N_dim, el%N_points, el%N_sampl) )
    
    ALLOCATE( el%x_R(el%N_sampl, 4) )

    ALLOCATE( el%xx_R(el%N_dim, el%N_sampl) )

    !----------------
    ! Recovery points
    !---------------------------------------------------------
    el%x_R(1,:) = (/ 1.d0/4.d0, 1.d0/4.d0, 1.d0/4.d0, 1.d0/4.d0 /)
   
    el%xx_R = 0.d0

  END SUBROUTINE init_Gradiet_Recovery_TET_P1
  !==========================================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%    TETRAHEDON P2   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                
  !                                             y
  !                                           .
  !                                         ,/
  !                                        / 
  !                        	         3 
  !                        	       ,/|`\
  !                           	     ,/  |  `\
  !                               ,7    '.   `6
  !                             ,/       9     `\
  !                           ,/         |       `\
  !                          1--------5--'.--------2---> x
  !                           `\.         |      ,/
  !                              `\.      |    ,10
  !                        	     `8.   '. ,/
  !                        	    `   \. |/
  !                        	       `   4
  !                                          `\.
  !                                             ` z

  !==============================================================
  SUBROUTINE init_faces_TET_P2(el, Nodes, Coords, NU_face, n_ele)
  !==============================================================

    IMPLICIT NONE

    CLASS(tetrahedron)                       :: el
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_face
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    !-------------------------------------------------

    TYPE(triangle_F), POINTER :: tri_F_pt

    INTEGER,      DIMENSION(:,:), ALLOCATABLE :: loc
    INTEGER,      DIMENSION(:),   ALLOCATABLE :: VV
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: RR
    
    INTEGER :: id, jf, N_ln, istat
    !-------------------------------------------------

    el%N_faces = 4 ! # Faces
             
    ALLOCATE( el%faces(el%N_faces) )

    ! # of local nodes
    N_ln = 6
      
    ! DoFs on the faces
    ALLOCATE( VV(N_ln), loc(el%N_faces, N_ln) )

    ! Coordinates on the faces
    ALLOCATE( RR(SIZE(Coords, 1), N_ln) )

    ! loc(jf, :) Local ordering of
    ! the nodes on the face jf
    loc(1, :) = (/ 2, 3, 4, 6, 9, 10 /)
    loc(2, :) = (/ 1, 4, 3, 8, 9, 7  /)
    loc(3, :) = (/ 4, 1, 2, 8, 5, 10 /)
    loc(4, :) = (/ 2, 1, 3, 5, 7, 6  /)

    DO jf = 1, el%N_faces

       VV = Nodes(    loc(jf, :) )
       RR = Coords(:, loc(jf, :) )

       ALLOCATE(tri_F_pt, STAT=istat)
       IF(istat /=0) THEN
          WRITE(*,*) 'ERROR: failed Tetrahedon faces allocation'
       ENDIF
       
       CALL tri_F_pt%initialize( "InternalFace", &
                                 loc(jf,:), VV, RR, NU_face(jf), n_ele(jf) )
       
       CALL tri_F_pt%face_quadrature()
       
       el%faces(jf)%f => tri_F_pt
          
    ENDDO

    DEALLOCATE( VV, RR, loc )

  END SUBROUTINE init_faces_TET_P2
  !===============================

  !======================================================
  FUNCTION basis_function_TET_P2(el, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(tetrahedron)                     :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    SELECT CASE (i)
       
    CASE(1, 2, 3, 4)
       
       psi_i = (2.d0*xi(i) - 1.d0) * xi(i)

    CASE(5)

       psi_i  = 4.d0*xi(1)*xi(2)

    CASE(6)

       psi_i  = 4.d0*xi(2)*xi(3)

    CASE(7)

       psi_i  = 4.d0*xi(3)*xi(1)
       
    CASE(8)

       psi_i  = 4.d0*xi(1)*xi(4)

    CASE(9)

       psi_i  = 4.d0*xi(3)*xi(4)

    CASE(10)

       psi_i  = 4.d0*xi(2)*xi(4)
  
    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Tetrahedron baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_TET_P2
  !=================================

  !======================================================
  FUNCTION gradient_ref_TET_P2(el, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(tetrahedron)                     :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !--------------------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = -(/ 4.d0*xi(1) - 1.d0, &
                     4.d0*xi(1) - 1.d0, &
                     4.d0*xi(1) - 1.d0  /)
       
    CASE(2)

       D_psi_i = (/ 0.d0,               &
                    4.d0*xi(2) - 1.d0,  &
                    0.d0                /)
       
    CASE(3)

       D_psi_i = (/ 0.d0,              &
                    0.d0,              &
                    4.d0*xi(3) - 1.d0  /)

    CASE(4)

       D_psi_i = (/ 4.d0*xi(4) - 1.d0, &
                    0.d0,              &
                    0.d0               /)

    CASE(5)

       D_psi_i = (/ -4.d0*xi(2),           &
                    -4.d0*xi(2) + 4*xi(1), &
                    -4.d0*xi(2)            /)
            
    CASE(6)

       D_psi_i = (/ 0.d0,       &
                    4.d0*xi(3), &
                    4.d0*xi(2)  /)

    CASE(7)

       D_psi_i = (/ -4.d0*xi(3),              &
                    -4.d0*xi(3),              &
                    -4.d0*xi(3) + 4.d0*xi(1)  /)

    CASE(8)

       D_psi_i = (/ -4.d0*xi(4) + 4.d0*xi(1), &
                    -4.d0*xi(4),              &
                    -4.d0*xi(4)               /)

    CASE(9)

       D_psi_i = (/ 4.d0*xi(3), &
                    0.d0,       &
                    4.d0*xi(4)  /)
    CASE(10)

       D_psi_i = (/ 4.d0*xi(2), &
                    4.d0*xi(4), &
                    0.d0        /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Tetrahedron gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_TET_P2
  !===============================

  !=========================================
  FUNCTION DOFs_ref_TET_P2(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(tetrahedron) :: el

    REAL(KIND=8), DIMENSION(10, 4) :: x_dof
    !--------------------------------------

    x_dof(1, :) = (/ 1.d0,  0.d0,  0.d0,  0.d0  /)
    x_dof(2, :) = (/ 0.d0,  1.d0,  0.d0,  0.d0  /)
    x_dof(3, :) = (/ 0.d0,  0.d0,  1.d0,  0.d0  /)
    x_dof(4, :) = (/ 0.d0,  0.d0,  0.d0,  1.d0  /)
    x_dof(5, :) = (/ 0.5d0, 0.5d0, 0.d0,  0.d0  /)
    x_dof(6, :) = (/ 0.d0,  0.5d0, 0.5d0, 0.d0  /)
    x_dof(7, :) = (/ 0.5d0, 0.d0,  0.5d0, 0.0d0 /)
    x_dof(8, :) = (/ 0.5d0, 0.d0,  0.d0,  0.5d0 /)
    x_dof(9, :) = (/ 0.d0,  0.d0,  0.5d0, 0.5d0 /)
    x_dof(10,:) = (/ 0.0d0, 0.5d0, 0.d0,  0.5d0 /)

  END FUNCTION DOFs_ref_TET_P2
  !===========================

  !==============================================
  FUNCTION face_trace_TET_P2(el, jf) RESULT(xi_f)
  !==============================================

    IMPLICIT NONE

    CLASS(tetrahedron)       :: el
    INTEGER,      INTENT(IN) :: jf

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
    !----------------------------------------------

    ALLOCATE( xi_f(6, 4) )

    SELECT CASE(jf)

    CASE(1)

       xi_f(1, :) = (/ 0.d0,  1.d0,  0.d0,  0.d0  /)
       xi_f(2, :) = (/ 0.d0,  0.d0,  1.d0,  0.d0  /)
       xi_f(3, :) = (/ 0.d0,  0.d0,  0.d0,  1.d0  /)
       xi_f(4, :) = (/ 0.0d0, 0.5d0, 0.d0,  0.5d0 /)
       xi_f(5, :) = (/ 0.d0,  0.5d0, 0.5d0, 0.d0  /)
       xi_f(6, :) = (/ 0.d0,  0.d0,  0.5d0, 0.5d0 /)

    CASE(2)

       xi_f(1, :) = (/ 1.d0,  0.d0,  0.d0,  0.d0  /)
       xi_f(2, :) = (/ 0.d0,  0.d0,  0.d0,  1.d0  /)
       xi_f(3, :) = (/ 0.d0,  0.d0,  1.d0,  0.d0  /)
       xi_f(4, :) = (/ 0.5d0, 0.d0,  0.d0,  0.5d0 /)
       xi_f(5, :) = (/ 0.d0,  0.d0,  0.5d0, 0.5d0 /)
       xi_f(6, :) = (/ 0.5d0, 0.d0,  0.5d0, 0.5d0 /)

    CASE(3)
  
       xi_f(1, :) = (/ 0.d0,  0.d0,  0.d0,  1.d0  /)
       xi_f(2, :) = (/ 1.d0,  0.d0,  0.d0,  0.d0  /)
       xi_f(3, :) = (/ 0.d0,  1.d0,  0.d0,  0.d0  /)
       xi_f(4, :) = (/ 0.5d0, 0.d0,  0.d0,  0.5d0 /)
       xi_f(5, :) = (/ 0.5d0, 0.5d0, 0.d0,  0.d0  /)
       xi_f(6, :) = (/ 0.0d0, 0.5d0, 0.d0,  0.5d0 /)

    CASE(4)
  
       xi_f(1, :) = (/ 0.d0,  1.d0,  0.d0,  0.d0  /)
       xi_f(2, :) = (/ 1.d0,  0.d0,  0.d0,  0.d0  /)
       xi_f(3, :) = (/ 0.d0,  0.d0,  1.d0,  0.d0  /)
       xi_f(5, :) = (/ 0.5d0, 0.5d0, 0.d0,  0.d0  /)
       xi_f(6, :) = (/ 0.5d0, 0.d0,  0.5d0, 0.5d0 /)
       xi_f(7, :) = (/ 0.d0,  0.5d0, 0.5d0, 0.d0  /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong Tetrahedron face in face trace P2'
       STOP       

    END SELECT    
    
  END FUNCTION face_trace_TET_P2
  !=============================

  !====================================
  SUBROUTINE init_quadrature_TET_P2(el)
  !====================================

    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    !----------------------------------------------

    REAL(KIND=8) :: alpha_1, beta_1
    !-----------------------------------------------

    el%N_quad = 14

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%D_phi_q(el%N_dim, el%N_points, el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 4) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !--------------------------------------
    ! Walkington's fifth-order 14-point rule from
    ! "Quadrature on Simplices of Arbitrary Dimension"
     
    el%x_q(1, :) = (/ 0.0673422422100981d0, 0.3108859192633010d0, 0.3108859192633010d0, 0.3108859192633010d0 /)
    el%x_q(2, :) = (/ 0.3108859192633010d0, 0.3108859192633010d0, 0.0673422422100982d0, 0.3108859192633010d0 /)
    el%x_q(3, :) = (/ 0.3108859192633010d0, 0.0673422422100982d0, 0.3108859192633010d0, 0.3108859192633010d0 /)
    el%x_q(4, :) = (/ 0.3108859192633010d0, 0.3108859192633010d0, 0.3108859192633010d0, 0.0673422422100982d0 /)
    el%w_q(1:4)  = 0.112687925718016d0

    el%x_q(5, :) = (/ 0.7217942490673260d0, 0.0927352503108912d0, 0.0927352503108912d0, 0.0927352503108912d0 /)
    el%x_q(6, :) = (/ 0.0927352503108911d0, 0.0927352503108912d0, 0.7217942490673260d0, 0.0927352503108912d0 /)
    el%x_q(7, :) = (/ 0.0927352503108911d0, 0.7217942490673260d0, 0.0927352503108912d0, 0.0927352503108913d0 /)
    el%x_q(8, :) = (/ 0.0927352503108912d0, 0.0927352503108912d0, 0.0927352503108912d0, 0.7217942490673260d0 /)
    el%w_q(5:8)  = 0.073493043116362d0

    el%x_q(9, :) = (/ 0.0455037041256496d0, 0.4544962958743500d0, 0.4544962958743500d0, 0.0455037041256496d0 /)
    el%x_q(10,:) = (/ 0.4544962958743500d0, 0.4544962958743500d0, 0.0455037041256496d0, 0.0455037041256496d0 /)
    el%x_q(11,:) = (/ 0.4544962958743500d0, 0.0455037041256496d0, 0.0455037041256496d0, 0.4544962958743500d0 /)
    el%x_q(12,:) = (/ 0.4544962958743500d0, 0.0455037041256496d0, 0.4544962958743500d0, 0.0455037041256496d0 /)
    el%x_q(13,:) = (/ 0.0455037041256496d0, 0.4544962958743500d0, 0.0455037041256496d0, 0.4544962958743500d0 /)
    el%x_q(14,:) = (/ 0.0455037041256496d0, 0.0455037041256496d0, 0.4544962958743500d0, 0.4544962958743500d0 /)
    el%w_q(9:14) = 0.0425460207770815d0
    !--------------------------------------
    
    el%w_q = el%w_q / 6.d0 ! V = (1/6)*det|J|
    
    el%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_TET_P2
  !====================================

  !==========================================
  SUBROUTINE init_Gradiet_Recovery_TET_P2(el)
  !==========================================

    IMPLICIT NONE

    CLASS(tetrahedron) :: el
    !-----------------------

    REAL :: alpha, beta
    !------------------------

    el%N_sampl = 4

    ALLOCATE( el%D_phi_R(el%N_dim, el%N_points, el%N_sampl) )
    
    ALLOCATE( el%x_R(el%N_sampl, 4) )

    ALLOCATE( el%xx_R(el%N_dim, el%N_sampl) )

    !----------------
    ! Recovery points
    !---------------------------------------------------------
    alpha = 0.585410196624969d0
     beta = 0.138196601125011d0

     el%x_R(1,:) = (/ alpha, beta,  beta,  beta  /)
     el%x_R(2,:) = (/ beta,  alpha, beta,  beta  /)
     el%x_R(3,:) = (/ beta,  beta,  alpha, beta  /)
     el%x_R(4,:) = (/ beta,  beta,  beta,  alpha /)

    el%xx_R = 0.d0

  END SUBROUTINE init_Gradiet_Recovery_TET_P2
  !==========================================

END MODULE Tetrahedron_Class
