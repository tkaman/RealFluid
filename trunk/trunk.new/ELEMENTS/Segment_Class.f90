MODULE Segment_Class

  USE Face_Class
  USE PLinAlg
  
  IMPLICIT NONE
  
  !=========================================
  TYPE, PUBLIC, EXTENDS(face_str) :: segment

     ! < >

   CONTAINS

     GENERIC, PUBLIC :: initialize => initialize_f, &
                                      initialize_b

     ! Internal Face
     PROCEDURE, PRIVATE, PASS :: initialize_f
     ! Boundary face
     PROCEDURE, PRIVATE, PASS :: initialize_b
     
     PROCEDURE, PUBLIC :: face_quadrature => face_quadrature_sub
     
  END TYPE segment
  !=========================================
    
  PRIVATE :: face_quadrature_sub
  PRIVATE :: init_quadrature_SEG_P1
  PRIVATE :: init_quadrature_SEG_P2

  PRIVATE :: normal
  
  PRIVATE :: basis_function
  PRIVATE :: basis_function_SEG_P1
  PRIVATE :: basis_function_SEG_P2

  PRIVATE :: gradient_ref
  PRIVATE :: gradient_ref_SEG_P1
  PRIVATE :: gradient_ref_SEG_P2

  PRIVATE :: DOFs_ref
  PRIVATE :: DOFs_ref_SEG_P1
  PRIVATE :: DOFs_ref_SEG_P2

CONTAINS

  !===================================================================
  SUBROUTINE initialize_f(es, mode, loc, Nodes, Coords, NU_seg, n_ele)
  !===================================================================

    IMPLICIT NONE

    CLASS(segment)                           :: es
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: loc
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,                      INTENT(IN) :: NU_seg
    INTEGER,                      INTENT(IN) :: n_ele
    !-------------------------------------------------

    INTEGER :: id
    !-------------------------------------------------

    IF ( mode /= "InternalFace" ) THEN
       WRITE(*,*) 'ERROR: elment must be initialize in face mode'
       STOP
    ENDIF
    
    SELECT CASE( SIZE(Nodes) )

    CASE(2)

       es%Type     = SEG_P1
       es%N_verts  = 2
       es%N_points = 2
              
    CASE(3)

       es%Type     = SEG_P2
       es%N_verts  = 2
       es%N_points = 3

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Segment type'
        STOP

     END SELECT

     !---------------------------
     ! Attach data to the element
     !---------------------------------------------------
     ALLOCATE( es%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
     ALLOCATE( es%NU(SIZE(Nodes)) )
     ALLOCATE( es%l_NU(SIZE(loc)) )
          
     DO id = 1, SIZE(Coords,1)
        es%Coords(id, :) = Coords(id, :)
     ENDDO

     es%N_dim = SIZE(Coords,1)

     es%NU = Nodes

     es%l_NU = loc

     es%g_face = NU_seg

     es%c_ele = n_ele
     
   END SUBROUTINE initialize_f   
   !==========================
   
  !========================================================================
  SUBROUTINE initialize_b(es, mode, Nodes, Coords, bc_type, cn_ele, b_ovlp)
  !========================================================================

    IMPLICIT NONE

    CLASS(segment)                           :: es
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,                      INTENT(IN) :: bc_type
    INTEGER,      DIMENSION(:,:), INTENT(IN) :: cn_ele
    INTEGER,                      INTENT(IN) :: b_ovlp
    !-------------------------------------------------

    REAL, DIMENSION(:), ALLOCATABLE :: x_dof
    REAL :: mod
    INTEGER :: j, id, ne
    !-------------------------------------------------

    IF ( mode /= "BoundaryFace" ) THEN
       WRITE(*,*) 'ERROR: elment must be initialize in boundary mode'
       STOP
    ENDIF
    
    SELECT CASE( SIZE(Nodes) )

    CASE(2)

       es%Type     = SEG_P1
       es%N_verts  = 2
       es%N_points = 2
              
    CASE(3)

       es%Type     = SEG_P2
       es%N_verts  = 2
       es%N_points = 3

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Segment type'
        STOP

     END SELECT

     !---------------------------
     ! Attach data to the element
     !---------------------------------------------------
     ALLOCATE( es%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
     ALLOCATE( es%NU(SIZE(Nodes)) )
               
     DO id = 1, SIZE(Coords,1)
        es%Coords(id, :) = Coords(id, :)
     ENDDO

     es%N_dim = SIZE(Coords,1)

     es%NU = Nodes

     es%bc_type = bc_type

     es%b_ovlp = b_ovlp

     CALL es%face_quadrature()

     !----------------------
     ! Normal versor at Dofs
     !--------------------------------------
     ALLOCATE( es%n_b(es%N_dim, es%N_points) )
     ALLOCATE( x_dof(es%N_points) )
     ALLOCATE( es%b_e_con(es%N_points) )

     x_dof = DOFs_ref(es)
     
     DO j = 1, es%N_points

        es%n_b(:, j) = normal(es, x_dof(j) )

        mod = DSQRT( SUM(es%n_b(:, j)**2) )        

        es%n_b(:, j) = es%n_b(:, j)/mod

        ne = COUNT( cn_ele(j, :) /= 0 )

        ALLOCATE( es%b_e_con(j)%cn_ele(ne) )

        es%b_e_con(j)%cn_ele = cn_ele(j, 1:ne)
        
     ENDDO

     DEALLOCATE(x_dof)
         
   END SUBROUTINE initialize_b  
   !==========================
   
  !=================================
  SUBROUTINE face_quadrature_sub(es)
  !=================================
  !
  ! Store the following  quantities at the quadrature points
  !    - value of the basis function
  !    - normal versor
  !    - weight of the quadrature formula (multipplied by 
  !      the jacobian of the transformation)
  !    - value of the trace of the gradient of basis functions
  !    - physical coordiantes of the quadrature point
  !
  ! Compute the lenght of the segment
  !
    IMPLICIT NONE

    CLASS(segment) :: es
    !--------------------

    REAL(KIND=8) :: Jac    
    INTEGER :: j, k, l
    !--------------------

    SELECT CASE(es%Type)

    CASE(SEG_P1)

       CALL init_quadrature_SEG_P1(es)     
       
    CASE(SEG_P2)
       
       CALL init_quadrature_SEG_P2(es)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Segment type for quadrature'
       WRITE(*,*) 'STOP'

    END SELECT

    !-------------------------------------
    ! Attach data to the quadrature points
    !---------------------------------------------------
    DO j = 1, es%N_quad

       DO k = 1, es%N_points

          es%phi_q(k, j) = basis_function( es, k, es%x_q(j, 1) )

          es%xx_q(:, j) = es%xx_q(:, j) + &
                          basis_function( es, k, es%x_q(j, 1) ) * es%Coords(:, k)

       ENDDO

       es%n_q(:, j) = normal(es, es%x_q(j, 1) )

       Jac = DSQRT( SUM(es%n_q(:, j)**2) )

       es%n_q(:, j) = es%n_q(:, j)/Jac

       es%w_q(j) = es%w_q(j) * Jac


!!$       DO k = 1, SIZE(p_D_phi,3)
!!$
!!$          DO l = 1, e%N_points
!!$             
!!$             e%p_Dphi_1_q(:, k, j) = e%p_Dphi_1_q(:, k, j) + &
!!$                                     p_D_phi(:, l, k)*e%phi_q(l, j)
!!$ 
!!$          ENDDO          
!!$          
!!$       ENDDO

    ENDDO

    ! Area of the element
    es%area = SUM(es%w_q)

    IF( es%area < 0.d0 ) THEN
       WRITE(*,*) 'ERROR: Wrong node ordering, area negative'
       STOP
    ENDIF

  END SUBROUTINE face_quadrature_sub  
  !=================================


!*******************************************************************************
!*******************************************************************************
!                         COMPLEMENTARY FUNCTIONS                              !
!*******************************************************************************
!*******************************************************************************

  !===============================================
  FUNCTION basis_function(es, i, xi) RESULT(psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(segment)            :: es
    INTEGER,       INTENT(IN) :: i
    REAL(KIND=8),  INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------
      
    SELECT CASE(es%Type)
       
    CASE(SEG_P1)

       psi_i = basis_function_SEG_P1(es, i, xi)
       
    CASE(SEG_P2)

       psi_i = basis_function_SEG_P2(es, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Segment type for basis functions'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION basis_function
  !==========================

  !===============================================
  FUNCTION gradient_ref(es, i, xi) RESULT(D_psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(segment)            :: es
    INTEGER,       INTENT(IN) :: i
    REAL(KIND=8),  INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(es%N_dim) :: D_psi_i
    !-------------------------------------------
      
    SELECT CASE(es%Type)
       
    CASE(SEG_P1)

       D_psi_i = gradient_ref_SEG_P1(es, i, xi)
       
    CASE(SEG_P2)

       D_psi_i = gradient_ref_SEG_P2(es, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Segment type for gradients'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION gradient_ref
  !========================

  !==================================
  FUNCTION DOFs_ref(es) RESULT(x_dof)
  !==================================
  !
  ! Compute the coordinates of the DOFs
  ! on the reference element
  !
    IMPLICIT NONE

    CLASS(segment) :: es
    
    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: x_dof
    !------------------------------------------------
     
    SELECT CASE(es%Type)
       
    CASE(SEG_P1)

       ALLOCATE( x_dof(2) )

       x_dof = DOFs_ref_SEG_P1(es)
       
    CASE(SEG_P2)

       ALLOCATE( x_dof(3) )

       x_dof = DOFs_ref_SEG_P2(es)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Segment type for DOFs'
       WRITE(*,*) 'STOP'

    END SELECT 

  END FUNCTION DOFs_ref
  !====================

  !==================================
  FUNCTION normal(es, xi) RESULT(n_i)
  !==================================
  !
  ! Compute the normal versor to the face 
  ! on the node with coordiantes xi
  !
    IMPLICIT NONE

    CLASS(segment)            :: es
    REAL(KIND=8),  INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(es%N_dim) :: n_i
    !---------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: d
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: Jb

    INTEGER :: i, k, l
    !---------------------------------------------

    ALLOCATE( d(es%N_dim, es%N_points) )

    DO i = 1, es%N_points
       d(:, i) = gradient_ref(es, i, xi)
    ENDDO    

    ALLOCATE( Jb(es%N_dim-1, es%N_dim) )

    DO k = 1, es%N_dim

       DO l = 1, es%N_dim - 1

          Jb(l, k) = SUM( es%Coords(k, :) * d(l, :) )

       ENDDO

    ENDDO

    n_i = Cross_product(Jb)

    DEALLOCATE( d, Jb )

  END FUNCTION normal
  !==================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%     SEGMENT P1     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !                        
  !                          o------------o
  !                         (1)          (2)
  !                       

  !======================================================
  FUNCTION basis_function_SEG_P1(es, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(segment)            :: es
    INTEGER,       INTENT(IN) :: i
    REAL(KIND=8),  INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !------------------------------

    SELECT CASE(i)

    CASE(1)

       psi_i = 1.d0 - xi

    CASE(2)

       psi_i = xi

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Segment basis function'
       STOP

    END SELECT    

  END FUNCTION basis_function_SEG_P1
  !=================================

  !======================================================
  FUNCTION gradient_ref_SEG_P1(es, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(segment)            :: es
    INTEGER,       INTENT(IN) :: i
    REAL(KIND=8),  INTENT(IN) :: xi

    REAL(KIND=8) :: D_psi_i
    !------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = -1.d0

    CASE(2)

       D_psi_i = 1.d0

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Segment gradient'
       STOP

    END SELECT    

  END FUNCTION gradient_ref_SEG_P1
  !===============================

  !=========================================
  FUNCTION DOFs_ref_SEG_P1(es) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(segment):: es

    REAL(KIND=8), DIMENSION(2) :: x_dof
    !-----------------------------------

    x_dof = (/ 0.d0,  1.d0 /)

  END FUNCTION DOFs_ref_SEG_P1
  !===========================
  
  !====================================
  SUBROUTINE init_quadrature_SEG_P1(es)
  !====================================

    IMPLICIT NONE

    CLASS(segment) :: es
    !-----------------------

    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: x_q
!   REAL(KIND=8) :: Jac
    INTEGER :: j, k, l
    !----------------------------------------------------

    es%N_quad = 2

    ALLOCATE(   es%n_q(es%N_dim,    es%N_quad) )
    ALLOCATE( es%phi_q(es%N_points, es%N_quad) )

    ALLOCATE( es%w_q(es%N_quad   ) )
    ALLOCATE( es%x_q(es%N_quad, 1) )

    ALLOCATE( es%xx_q(es%N_dim, es%N_quad) )

    !-------------------
    ! Quadrature formula
    !-----------------------------
    es%x_q(1, 1) = 0.5d0*( 1.d0 - SQRT(1.d0/3.d0) )
    es%x_q(2, 1) = 0.5d0*( 1.d0 + SQRT(1.d0/3.d0) )

    es%w_q(1) = 0.5d0
    es%w_q(2) = 0.5d0
    !-----------------------------

    es%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_SEG_P1
  !====================================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%     SEGMENT P2     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !                        
  !                          o------o------o
  !                         (1)    (3)    (2)
  !  

  !======================================================
  FUNCTION basis_function_SEG_P2(es, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(segment)            :: es
    INTEGER,       INTENT(IN) :: i
    REAL(KIND=8),  INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !------------------------------

    SELECT CASE(i)

    CASE(1)

       psi_i = (1.d0 - xi)*(1.d0 - 2.d0*xi)

    CASE(2)

       psi_i = xi*(2.d0*xi - 1.d0)

    CASE(3)       

       psi_i = 4.d0*(1.d0 - xi)*xi 

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Segment baisis function'
       STOP

    END SELECT    

  END FUNCTION basis_function_SEG_P2
  !=================================

  !======================================================
  FUNCTION gradient_ref_SEG_P2(es, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(segment)            :: es
    INTEGER,       INTENT(IN) :: i
    REAL(KIND=8),  INTENT(IN) :: xi

    REAL(KIND=8) :: D_psi_i
    !------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = 4.d0*xi - 3.d0

    CASE(2)

       D_psi_i = 4.d0*xi - 1.d0

    CASE(3)

       D_psi_i = 4.d0 - 8.d0*xi

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Segment gradient'
       STOP

    END SELECT    
    
  END FUNCTION gradient_ref_SEG_P2
  !================================  

  !=========================================
  FUNCTION DOFs_ref_SEG_P2(es) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(segment):: es

    REAL(KIND=8), DIMENSION(3) :: x_dof
    !-----------------------------------

    x_dof = (/ 0.d0,  1.d0, 0.5d0 /)

  END FUNCTION DOFs_ref_SEG_P2
  !===========================

  !====================================
  SUBROUTINE init_quadrature_SEG_P2(es)
  !====================================

    IMPLICIT NONE

    CLASS(segment) :: es
    !--------------------

    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: x_q
    REAL(KIND=8) :: Jac
    INTEGER :: j, k, l
    !-------------------------------------------------

    es%N_quad = 4 !3

    ALLOCATE( es%phi_q(es%N_points, es%N_quad) )
    ALLOCATE(   es%n_q(es%N_dim,    es%N_quad) )


    ALLOCATE( es%w_q(es%N_quad   ) )
    ALLOCATE( es%x_q(es%N_quad, 1) )
    
    ALLOCATE( es%xx_q(es%N_dim, es%N_quad) )

    !-------------------
    ! Quadrature formula
    !----------------------------------------------
!!$    es%x_q(1, 1) = 0.5d0 * (1.d0 - SQRT(3.d0/5.d0) )
!!$    es%x_q(2, 1) = 0.5d0 * (1.d0 + SQRT(3.d0/5.d0) )
!!$    es%x_q(3, 1) = 0.5d0
!!$
!!$    es%w_q(1) = 5.d0/18.d0
!!$    es%w_q(2) = 5.d0/18.d0
!!$    es%w_q(3) = 8.d0/18.d0

    es%x_q(1, 1) = (1.d0 - SQRT(525.d0 + 70.d0*SQRT(30.d0))/35.d0)*0.5d0
    es%x_q(2, 1) = (1.d0 + SQRT(525.d0 + 70.d0*SQRT(30.d0))/35.d0)*0.5d0
    es%x_q(3, 1) = (1.d0 + SQRT(525.d0 - 70.d0*SQRT(30.d0))/35.d0)*0.5d0
    es%x_q(4, 1) = (1.d0 - SQRT(525.d0 - 70.d0*SQRT(30.d0))/35.d0)*0.5d0

    es%w_q(1:2) = (18.d0 - SQRT(30.d0))/72.d0 
    es%w_q(3:4) = (18.d0 + SQRT(30.d0))/72.d0

!!    es%x_q(1, 1) = 0.d0
!!    es%x_q(2, 1) = 1.d0
!!    es%x_q(3, 1) = 0.5d0
!!
!!    es%w_q(1) = 1.d0/6.d0
!!    es%w_q(2) = 1.d0/6.d0
!!    es%w_q(3) = 4.d0/6.d0
    !-----------------------------------------------
    
    es%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_SEG_P2
  !====================================
 
END MODULE Segment_Class

