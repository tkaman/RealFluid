MODULE Elements_driver
   
  USE LesTypes

  USE Segment_Class

  USE Trianlge_Class
  USE Triangle_F_Class

  USE Quadrangle_Class
  USE Quadrangle_F_Class

  USE Tetrahedron_Class
  USE Hexahedron_Class
  USE Pyramid_Class

  IMPLICIT NONE

CONTAINS

  !===================================
  SUBROUTINE Init_Elements(Data, Mesh)
  !===================================

    IMPLICIT NONE

    TYPE(Donnees), INTENT(IN) :: Data
    TYPE(MeshDef), INTENT(IN) :: Mesh
    !---------------------------------

    TYPE(segment),      POINTER :: seg_pt
    TYPE(triangle),     POINTER :: tri_pt
    TYPE(triangle_F),   POINTER :: tri_F_pt
    TYPE(quadrangle) ,  POINTER :: quad_pt
    TYPE(quadrangle_F), POINTER :: quad_F_pt
    TYPE(tetrahedron),  POINTER :: tetr_pt
    TYPE(hexahedron),   POINTER :: hexa_pt
    TYPE(pyramid),      POINTER :: pyram_pt
    !-------------------------------------

    REAL,    DIMENSION(:,:), ALLOCATABLE :: Coords
    INTEGER, DIMENSION(:),   ALLOCATABLE :: NU    
    INTEGER, DIMENSION(:),   ALLOCATABLE :: nu_seg, nu_face
    INTEGER, DIMENSION(:),   ALLOCATABLE :: c_ele
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: cn_ele
    
    INTEGER :: je, jf, k, n_v, b_ele
    INTEGER :: b_ovlp, CN_MAX, cn, istat
    !----------------------------------------------

    !************************************************************
    !************************************************************
    ! Domain Elements
    !************************************************************
    !************************************************************
    ALLOCATE( d_element(Mesh%Nelemt) )

    DO je = 1, Mesh%Nelemt

       ! # DOFs of the element
       n_v = COUNT( Mesh%Nu(:, je) /= 0 )
       ALLOCATE( NU(n_v) )

       ! Physical coordinates of the DOFs
       ALLOCATE( Coords(SIZE(Mesh%coor, 1), n_v) )

       NU = Mesh%NU(1:n_v, je)

       Coords = Mesh%coor(:, NU)

       ! Nodes to element connectivity
       CN_MAX = 0
       DO k = 1, n_v
          CN_MAX = MAX( CN_MAX, SIZE(Connect(NU(k))%cn_ele) )
       ENDDO

       ALLOCATE( cn_ele(n_v, CN_MAX) )

       cn_ele = 0

       DO k = 1, n_v

          cn = SIZE(Connect(NU(k))%cn_ele)

          cn_ele(k, 1:cn) = Connect(NU(k))%cn_ele      

       ENDDO
     
       SELECT CASE( Mesh%EltType(je) )

       !-------------------------
       CASE(et_TRIAP1, et_TRIAP2)
       !----------------------------------------------------------

          ! # Faces of the element
          ALLOCATE( nu_seg(3), c_ele(3) )
          
          ! Global numeration of the segment
          ! ordering: face i | opposed to the node i
          nu_seg = Mesh%Nuseg( (/2,3,1/), je)

          DO k = 1, SIZE(nu_seg)

             ! Adjacent element with share the same face             
             IF( Mesh%Nutv(1, nu_seg(k)) == je ) THEN
                c_ele(k) = Mesh%Nutv(2, nu_seg(k))
             ELSE
                c_ele(k) = Mesh%Nutv(1, nu_seg(k))
             ENDIF

          ENDDO

          ALLOCATE(tri_pt, STAT=istat)
          IF(istat /=0) THEN             
             WRITE(*,*) 'ERROR: failed trianlge allocation'
          ENDIF
          CALL tri_pt%initialize( "Element", NU, Coords, nu_seg, c_ele, cn_ele )

          d_element(je)%e => tri_pt

          DEALLOCATE( nu_seg, c_ele )

       !-------------------------
       CASE(et_QUADP1, et_QUADP2)
       !----------------------------------------------------------
       
          ! # Faces of the element          
          ALLOCATE( nu_seg(4), c_ele(4) )

          !Global numeration of the face
          ! ordering: face i | opposed to the node i
          nu_seg = Mesh%Nuseg( (/2,3,4,1/), je)

          DO k = 1, SIZE(nu_seg)

             ! Adjacent element with share the same face             
             IF( Mesh%Nutv(1, nu_seg(k)) == je ) THEN
                c_ele(k) = Mesh%Nutv(2, nu_seg(k))
             ELSE
                c_ele(k) = Mesh%Nutv(1, nu_seg(k))
             ENDIF

          ENDDO

          ALLOCATE(quad_pt, STAT=istat)
          IF(istat /=0) THEN             
             WRITE(*,*) 'ERROR: failed quadranlge allocation'
          ENDIF
          CALL quad_pt%initialize( "Element", NU, Coords, nu_seg, c_ele, cn_ele )

          d_element(je)%e => quad_pt

          DEALLOCATE( nu_seg, c_ele )

       !---------------------------
       CASE(et_tetraP1, et_tetraP2)
       !----------------------------------------------------------
          
          ! # Faces of the element          
          ALLOCATE( nu_face(4), c_ele(4) )

          !Global numeration of the face 
          ! ordering: face i | opposed to the node i
          nu_face = Mesh%NuFacette(:, je)

          DO k = 1, SIZE(nu_face)

             ! Adjacent element with share the same face             
             IF( Mesh%EleVois(1, nu_face(k)) == je ) THEN
                c_ele(k) = Mesh%EleVois(2, nu_face(k))
             ELSE
                c_ele(k) = Mesh%EleVois(1, nu_face(k))
             ENDIF
             
          ENDDO
          
          ALLOCATE(tetr_pt, STAT=istat)
          IF(istat /=0) THEN             
             WRITE(*,*) 'ERROR: failed tetrahedron allocation'
          ENDIF
          CALL tetr_pt%initialize( "Element", NU, Coords, nu_face, c_ele, cn_ele )

          d_element(je)%e => tetr_pt

          DEALLOCATE( nu_face, c_ele )

       !-------------------------
       CASE(et_hexaP1, et_hexaP2)
       !----------------------------------------------------------

          ! # Faces of the element          
          ALLOCATE( nu_face(6), c_ele(6) )

          !Global numeration of the face
          nu_face = Mesh%NuFacette(:, je)

          DO k = 1, SIZE(nu_face)

             ! Adjacent element with share the same face             
             IF( Mesh%EleVois(1, nu_face(k)) == je ) THEN
                c_ele(k) = Mesh%EleVois(2, nu_face(k))
             ELSE
                c_ele(k) = Mesh%EleVois(1, nu_face(k))
             ENDIF
             
          ENDDO
          
          ALLOCATE(hexa_pt, STAT=istat)
          IF(istat /=0) THEN             
             WRITE(*,*) 'ERROR: failed hexahedron allocation'
          ENDIF
          CALL hexa_pt%initialize( "Element", NU, Coords, nu_face, c_ele, cn_ele )

          d_element(je)%e => hexa_pt

          DEALLOCATE( nu_face, c_ele )

       !---------------------------
       CASE(et_PyramP1, et_PyramP2)
       !----------------------------------------------------------

          ! # Faces of the element          
          ALLOCATE( nu_face(5), c_ele(5) )

          !Global numeration of the face
          nu_face = Mesh%NuFacette(:, je)

          DO k = 1, SIZE(nu_face)

             ! Adjacent element with share the same face             
             IF( Mesh%EleVois(1, nu_face(k)) == je ) THEN
                c_ele(k) = Mesh%EleVois(2, nu_face(k))
             ELSE
                c_ele(k) = Mesh%EleVois(1, nu_face(k))
             ENDIF
             
          ENDDO
          
          ALLOCATE(pyram_pt, STAT=istat)
          IF(istat /=0) THEN             
             WRITE(*,*) 'ERROR: failed hexahedron allocation'
          ENDIF
          CALL pyram_pt%initialize( "Element", NU, Coords, nu_face, c_ele, cn_ele )

          d_element(je)%e => pyram_pt

          DEALLOCATE( nu_face, c_ele )

       CASE DEFAULT

          WRITE(*,*) 'ERROR: Unknown domain element type'
          STOP

       END SELECT

       DEALLOCATE( Coords, NU, cn_ele )

    ENDDO

    !************************************************************
    !************************************************************
    ! Boundary Elements
    !************************************************************
    !************************************************************
    IF (Mesh%NFacFr > 0) THEN

       ALLOCATE( b_element(Mesh%NfacFr) )

       DO jf = 1, Mesh%NfacFr

          n_v = COUNT(Mesh%NsfacFr(:, jf) /= 0)

          IF(Mesh%Ndim == 2) THEN
             IF(n_v == 2) THEN
                b_ele = et_segP1
             ELSEIF(n_v == 3) THEN
                b_ele = et_segP2
             ENDIF
          ELSE
             IF(n_v == 3) THEN
                b_ele = et_TRIAP1
             ELSEIF(n_v == 4) THEN
                b_ele = et_QUADP1
             ELSEIF(n_v == 6) THEN
                b_ele = et_TRIAP2
             ELSEIF(n_v == 9) THEN
                b_ele = et_QUADP2
             ENDIF
          ENDIF
          
          ALLOCATE( Nu(n_v) )          
          NU = Mesh%NsfacFr(1:n_v, jf)
          
          ALLOCATE( Coords(SIZE(Mesh%coor, 1), n_v) )          
          Coords = Mesh%coor(:, Nu)

          IF( ASSOCIATED(Mesh%b_ovlp) ) THEN
             b_ovlp = Mesh%b_ovlp(jf)
          ELSE
             b_ovlp = 0
          ENDIF

          CN_MAX = 0
          DO k = 1, n_v
             CN_MAX = MAX( CN_MAX, SIZE(Connect(NU(k))%cn_ele) )
          ENDDO

          ALLOCATE( cn_ele(n_v, CN_MAX) )

          cn_ele = 0

          DO k = 1, n_v

             cn = SIZE(Connect(NU(k))%cn_ele)

             cn_ele(k, 1:cn) = Connect(NU(k))%cn_ele      

          ENDDO

          SELECT CASE(b_ele)

          !-----------------------
          CASE(et_SEGP1, et_SEGP2)
          !----------------------------------------------------------

             ALLOCATE(seg_pt, STAT=istat)
             IF(istat /=0) THEN
                WRITE(*,*) 'ERROR: failed face Segment allocation'
             ENDIF
             CALL seg_pt%initialize( "BoundaryFace", &
                                    NU, Coords, Mesh%LogFr(jf), cn_ele, b_ovlp)

             b_element(jf)%b_f => seg_pt

             DEALLOCATE( Coords, NU )

          !-------------------------
          CASE(et_TRIAP1, et_TRIAP2)
          !----------------------------------------------------------

             ALLOCATE(tri_F_pt, STAT=istat)
             IF(istat /=0) THEN
                WRITE(*,*) 'ERROR: failed face Triangle allocation'
             ENDIF
             CALL tri_F_pt%initialize( "BoundaryFace", &
                                      NU, Coords, Mesh%LogFr(jf), cn_ele, b_ovlp )

             b_element(jf)%b_f => tri_F_pt

             DEALLOCATE( Coords, NU )

          !-------------------------
          CASE(et_QUADP1, et_QUADP2)
          !----------------------------------------------------------

             ALLOCATE(quad_F_pt, STAT=istat)
             IF(istat /=0) THEN
                WRITE(*,*) 'ERROR: failed face Qudrangle allocation'
             ENDIF
             CALL quad_F_pt%initialize( "BoundaryFace", &
                                      NU, Coords, Mesh%LogFr(jf), cn_ele, b_ovlp )

             b_element(jf)%b_f => quad_F_pt

             DEALLOCATE( Coords, NU )

          CASE DEFAULT

             WRITE(*,*) 'ERROR: Unknown boundary element type'
             STOP             

          END SELECT

          DEALLOCATE(cn_ele)

       ENDDO

    ENDIF

!    DEALLOCATE( Connect )

  END SUBROUTINE Init_Elements
  !===========================

END MODULE Elements_driver
