MODULE Face_Class

  IMPLICIT NONE

  !==================================
  INTEGER, PARAMETER :: SEG_P1 = 0
  INTEGER, PARAMETER :: SEG_P2 = 1

  INTEGER, PARAMETER :: TRI_F_P1 = 10
  INTEGER, PARAMETER :: TRI_F_P2 = 11

  INTEGER, PARAMETER :: QUA_F_Q1 = 20
  INTEGER, PARAMETER :: QUA_F_Q2 = 21

  INTEGER, PARAMETER :: DOF    =0
  INTEGER, PARAMETER :: PT_QUAD=1
  !==================================

  !-------------------------------------------
  TYPE :: b_e_con_str
     INTEGER, DIMENSION(:), POINTER :: cn_ele
  END TYPE b_e_con_str
  !-------------------------------------------

  !==================================
  TYPE, PUBLIC :: face_str

     INTEGER :: Type     = 0 ! Element type
     INTEGER :: N_dim    = 0 ! # spatial dimension
     INTEGER :: N_verts  = 0 ! # vertices
     INTEGER :: N_points = 0 ! # DoFs

     !--------------
     ! Face geometry
     !------------------------------------------------------------
     !
     ! Vertices coordinates
     ! Coords(id, j): id = 1, N_dim, j = 1, N_verts
     REAL(KIND=8), DIMENSION(:,:), POINTER :: Coords => NULL()
     !------------------------------------------------------------
     !
     ! Area of the face
     REAL(KIND=8) :: Area = 0.d0

     !--------------------
     ! Boundary conditions
     !------------------------------------------------------------
     !
     ! Flag of the boundary condition
     INTEGER :: bc_type
     !------------------------------------------------------------   
     ! Number of domain's overlaps on the wall (for parallel only)
     INTEGER :: b_ovlp
     !------------------------------------------------------------
     ! Normal versor at the DoFs on the boundary domain
     ! n_b(id, j), id = 1, N_dim, j = 1, N_points
     REAL(KIND=8), DIMENSION(:,:), POINTER :: n_b => NULL()
     !
     ! Connectivity between the boundary node and the domain elements
     ! which share the same boundary node
     ! b_e_con(j)%je, j = 1, N_points, je = 1, # ele(bubble)
     TYPE(b_e_con_str), DIMENSION(:), POINTER :: b_e_con


     !--------------
     ! Face Topology
     !------------------------------------------------------------
     !
     ! Nodes (global numeration of the mesh)
     ! NU(j), j = 1, N_points
     INTEGER,      DIMENSION(:),   POINTER :: NU => NULL()
     !------------------------------------------------------------
     !
     ! Nodes (local numeration of the element)
     ! l_nu(j), j = 1, N_points
     INTEGER,      DIMENSION(:),   POINTER :: l_nu => NULL()
     !------------------------------------------------------------
     ! Segment (global numeration)
     !
     INTEGER                               :: g_face
     !------------------------------------------------------------
     ! Element wich share the same face
     !
     INTEGER                               :: c_ele

     !------------
     ! Quadrature
     !------------------------------------------------------------
     !
     ! # quadrature points
     INTEGER :: N_quad = 0
     !------------------------------------------------------------
     !
     ! Quadrature weighs (multiplied by the jacobian)
     ! w_q(iq), iq = 1, N_quad
     REAL(KIND=8), DIMENSION(:),     POINTER :: w_q => NULL()
     !------------------------------------------------------------
     !
     ! Reference Coordinates of the quadrature points
     ! xx_q(ik, iq), , ik = 1, N_dim_ele, iq = 1, N_quad
     REAL(KIND=8), DIMENSION(:,:),   POINTER :: x_q => NULL()
     !------------------------------------------------------------
     !
     ! Physical coordinates of the quadrature points
     ! xx_q(id, iq), , id = 1, N_dim, iq = 1, N_quad
     REAL(KIND=8), DIMENSION(:,:),   POINTER :: xx_q => NULL()
     !------------------------------------------------------------
     !
     ! Normal versor at quadrature points
     ! n_q(id, iq), id = 1, N_dim, iq = 1, N_quad
     REAL(KIND=8), DIMENSION(:,:),   POINTER :: n_q => NULL()
     !------------------------------------------------------------
     !
     ! Value of basis functions at the quadrature point
     ! phi_q(k, iq), k = 1, N_points,  iq = 1, N_quad
     REAL(KIND=8), DIMENSION(:,:),   POINTER :: phi_q => NULL()

!!$     !--------------
!!$     ! CIP structure
!!$     !------------------------------------------------------------
!!$     !
!!$     ! 1 -> element to which the face belong to 
!!$     ! 2 -> adiancen element which share the same face
!!$     !
!!$     ! Gradient of element basis functions at the quadrature point
!!$     ! p_Dphi_x_q(id, k, iq)
!!$     ! id = 1, N_dim, k = 1, N_points (ele), iq = 1, N_quad
!!$     REAL(KIND=8), DIMENSION(:,:,:), POINTER :: p_Dphi_1_q
!!$     REAL(KIND=8), DIMENSION(:,:,:), POINTER :: p_Dphi_2_q
!!$     !------------------------------------------------------------
!!$     !
!!$     ! Gradient of the solution at the quadrature point
!!$     ! p_u_x_q(id, iq),  id = 1, N_dim, iq = 1, N_quad
!!$     REAL(KIND=8), DIMENSION(:,:), POINTER :: p_Du_1_q
!!$     REAL(KIND=8), DIMENSION(:,:), POINTER :: p_Du_2_q
!!$     !------------------------------------------------------------
!!$     !
!!$     ! Correspondence between the local numeration of two elements
!!$     ! with share the same face
!!$     ! loc_con(k), k = 1, N_points (ele)
!!$     INTEGER, DIMENSION(:), POINTER :: loc_con

     !   CONTAINS

  END TYPE face_str
  !=================================

CONTAINS

END MODULE Face_Class
