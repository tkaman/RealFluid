MODULE Quadrangle_F_Class

  USE Face_Class
  USE PLinAlg

  IMPLICIT NONE

  !==============================================
  TYPE, PUBLIC, EXTENDS(face_str) :: quadrangle_F

     ! < >

   CONTAINS

     GENERIC, PUBLIC :: initialize => initialize_f, &
                                      initialize_b

     ! Internal Face
     PROCEDURE, PRIVATE, PASS :: initialize_f
     ! Boundary face
     PROCEDURE, PRIVATE, PASS :: initialize_b
     
     PROCEDURE, PUBLIC :: face_quadrature => face_quadrature_sub
     
  END TYPE quadrangle_F  
  !============================================
    
  PRIVATE :: face_quadrature_sub
  PRIVATE :: init_quadrature_QUA_Q1
  PRIVATE :: init_quadrature_QUA_Q2

  PRIVATE :: normal
  
  PRIVATE :: basis_function
  PRIVATE :: basis_function_QUA_Q1
  PRIVATE :: basis_function_QUA_Q2

  PRIVATE :: gradient_ref
  PRIVATE :: gradient_ref_QUA_Q1
  PRIVATE :: gradient_ref_QUA_Q2

  PRIVATE :: DOFs_ref
  PRIVATE :: DOFs_ref_QUA_Q1
  PRIVATE :: DOFs_ref_QUA_Q2

CONTAINS

  !===================================================================
  SUBROUTINE initialize_f(e, mode, loc, Nodes, Coords, Nu_face, n_ele)
  !===================================================================
  
    IMPLICIT NONE

    
    CLASS(quadrangle_F)                      :: e
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: loc
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,                      INTENT(IN) :: NU_face
    INTEGER,                      INTENT(IN) :: n_ele
    !---------------------------------------------------

    INTEGER :: i, id
    !---------------------------------------------------
    
    IF ( mode /= "InternalFace" ) THEN
       WRITE(*,*) 'ERROR: elment must be initialize in face mode'
       STOP
    ENDIF

    SELECT CASE( SIZE(Nodes) )

    CASE(4)

       e%Type     = QUA_F_Q1 ! Element type
       e%N_verts  = 4        ! # Vertices
       e%N_points = 4        ! # DoFs

    CASE(9)

       e%Type     = QUA_F_Q2 ! Element type
       e%N_verts  = 4        ! # Vertices
       e%N_points = 9        ! # DoFs

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Quadrangle type'
        STOP

     END SELECT

     !---------------------------
     ! Attach data to the element
     !---------------------------------------------------
     ALLOCATE( e%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
     ALLOCATE( e%NU(SIZE(Nodes)) )
     ALLOCATE( e%l_NU(SIZE(loc)) )
          
     DO id = 1, SIZE(Coords,1)
        e%Coords(id, :) = Coords(id, :)
     ENDDO

     e%N_dim = SIZE(Coords,1)

     e%NU = Nodes

     e%l_NU = loc

     e%g_face = NU_face

     e%c_ele = n_ele

  END SUBROUTINE initialize_f
  !==========================

  !=======================================================================
  SUBROUTINE initialize_b(e, mode, Nodes, Coords, bc_type, cn_ele, b_ovlp)
  !=======================================================================

    IMPLICIT NONE

    CLASS(quadrangle_F)                      :: e
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,                      INTENT(IN) :: bc_type
    INTEGER,      DIMENSION(:,:), INTENT(IN) :: cn_ele
    INTEGER,                      INTENT(IN) :: b_ovlp
    !-------------------------------------------------

    REAL, DIMENSION(:,:), ALLOCATABLE :: x_dof
    REAL :: mod
    INTEGER :: j, id, ne
    !-------------------------------------------------

    IF ( mode /= "BoundaryFace" ) THEN
       WRITE(*,*) 'ERROR: elment must be initialize in boundary mode'
       STOP
    ENDIF

    SELECT CASE( SIZE(Nodes) )

    CASE(4)

       e%Type     = QUA_F_Q1 ! Element type
       e%N_verts  = 4        ! # Vertices
       e%N_points = 4        ! # DoFs

    CASE(9)

       e%Type     = QUA_F_Q2 ! Element type
       e%N_verts  = 4        ! # Vertices
       e%N_points = 9        ! # DoFs

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Quadrangle type'
        STOP

     END SELECT

     !---------------------------
     ! Attach data to the element
     !---------------------------------------------------
     ALLOCATE( e%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
     ALLOCATE( e%NU(SIZE(Nodes)) )
              
     DO id = 1, SIZE(Coords,1)
        e%Coords(id, :) = Coords(id, :)
     ENDDO

     e%N_dim = SIZE(Coords,1)

     e%NU = Nodes

     e%bc_type = bc_type

     e%b_ovlp = b_ovlp
  
     CALL e%face_quadrature()

     !----------------------
     ! Normal versor at Dofs
     !--------------------------------------
     ALLOCATE( e%n_b(e%N_dim, e%N_points) )
     ALLOCATE( x_dof(e%N_points, 2) )
     ALLOCATE( e%b_e_con(e%N_points) )

     x_dof = DOFs_ref(e)

     DO j = 1, e%N_points

        e%n_b(:, j) = normal( e, x_dof(j, :) )
        
        mod = DSQRT( SUM(e%n_b(:, j)**2) )        

        e%n_b(:, j) = e%n_b(:, j)/mod

        ne = COUNT( cn_ele(j, :) /= 0 )

        ALLOCATE( e%b_e_con(j)%cn_ele(ne) )

        e%b_e_con(j)%cn_ele = cn_ele(j, 1:ne)
        
     ENDDO

   DEALLOCATE(x_dof)

   END SUBROUTINE initialize_b
   !==========================

   !================================
   SUBROUTINE face_quadrature_sub(e)
   !================================
   !
   ! Store the following  quantities at the quadrature points
   !    - value of the basis function
   !    - normal versor
   !    - weight of the quadrature formula (multipplied by 
   !      the jacobian of the transformation)
   !    - value of the trace of the gradient of basis functions
   !    - physical coordiantes of the quadrature point
   !
   ! Compute the lenght of the segment
   !
    IMPLICIT NONE

    CLASS(quadrangle_F)  :: e
    !--------------------------

    REAL(KIND=8) :: Jac    
    INTEGER :: iq, k, l
    !--------------------

    SELECT CASE(e%Type)

    CASE(QUA_Q1)

       CALL init_quadrature_QUA_Q1(e)
       
    CASE(QUA_Q2)
       
       CALL init_quadrature_QUA_Q2(e)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Segment type for quadrature'
       WRITE(*,*) 'STOP'

    END SELECT

    !-------------------------------------
    ! Attach data to the quadrature points
    !---------------------------------------------------
    DO iq = 1, e%N_quad

       DO k = 1, e%N_points

          e%phi_q(k, iq) = basis_function( e, k, e%x_q(iq, :) )

          e%xx_q(:, iq) = e%xx_q(:, iq) + &
                          basis_function( e, k, e%x_q(iq, :) ) * e%Coords(:, k)

       ENDDO
       
       e%n_q(:, iq) = normal(e, e%x_q(iq, :) )

       Jac = DSQRT( SUM(e%n_q(:, iq)**2) )

       e%n_q(:, iq) = e%n_q(:, iq)/Jac

       e%w_q(iq) = e%w_q(iq) * Jac


!!$       DO k = 1, SIZE(p_D_phi,3)
!!$
!!$          DO l = 1, e%N_points
!!$             
!!$             e%p_Dphi_1_q(:, k, j) = e%p_Dphi_1_q(:, k, j) + &
!!$                                     p_D_phi(:, l, k)*e%phi_q(l, j)
!!$ 
!!$          ENDDO          
!!$          
!!$       ENDDO

    ENDDO

    ! Area of the element
    e%area = SUM(e%w_q)
    
    IF( e%area < 0.d0 ) THEN
       WRITE(*,*) 'ERROR: Wrong node ordering, area negative'
       STOP
    ENDIF

  END SUBROUTINE face_quadrature_sub  
  !=================================

!*******************************************************************************
!*******************************************************************************
!                         COMPLEMENTARY FUNCTIONS                              !
!*******************************************************************************
!*******************************************************************************

  !==============================================
  FUNCTION basis_function(e, i, xi) RESULT(psi_i)
  !==============================================

    IMPLICIT NONE

    CLASS(quadrangle_F)                    :: e
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------
      
    SELECT CASE(e%Type)
       
    CASE(QUA_Q1)

       psi_i = basis_function_QUA_Q1(e, i, xi)
       
    CASE(QUA_Q2)

       psi_i = basis_function_QUA_Q2(e, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Quadrangle type for basis functions'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION basis_function
  !========================== 

  !==============================================
  FUNCTION gradient_ref(e, i, xi) RESULT(D_psi_i)
  !==============================================

    IMPLICIT NONE

    CLASS(quadrangle_F)                     :: e
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------
      
    SELECT CASE(e%Type)
       
    CASE(QUA_Q1)

       D_psi_i = gradient_ref_QUA_Q1(e, i, xi)
       
    CASE(QUA_Q2)

       D_psi_i = gradient_ref_QUA_Q2(e, i, xi)
     
    CASE DEFAULT

       WRITE(*,*) 'Unknown Quadrangle type for gradients'
       WRITE(*,*) 'STOP'

    END SELECT    

  END FUNCTION gradient_ref
  !========================
     
  !=================================
  FUNCTION DOFs_ref(e) RESULT(x_dof)
  !=================================
  !
  ! Compute the coordinates of the DOFs
  ! on the reference element
  !
    IMPLICIT NONE

    CLASS(quadrangle_F) :: e
    
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof
    !-------------------------------------------------
     
    SELECT CASE(e%Type)
       
    CASE(QUA_Q1)

       ALLOCATE( x_dof(4, 2) )

       x_dof = DOFs_ref_QUA_Q1(e)
       
    CASE(QUA_Q2)

       ALLOCATE( x_dof(9, 2) )

       x_dof = DOFs_ref_QUA_Q2(e)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Quadrangle type for DOFs'
       WRITE(*,*) 'STOP'

    END SELECT 

  END FUNCTION DOFs_ref
  !====================

  !=================================
  FUNCTION normal(e, xi) RESULT(n_i)
  !=================================
  !
  ! Compute the normal versor to the face 
  ! on the node with coordiantes xi
  !
    IMPLICIT NONE

    CLASS(quadrangle_F)                     :: e
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(e%N_dim) :: n_i
    !---------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: d
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: Jb

    INTEGER :: i, k, l
    !---------------------------------------------

    ALLOCATE( d(e%N_dim, e%N_points) )

    DO i = 1, e%N_points
       d(:, i) = gradient_ref(e, i, xi)
    ENDDO    

    ALLOCATE( Jb(e%N_dim-1, e%N_dim) )

    DO k = 1, e%N_dim

       DO l = 1, e%N_dim - 1

          Jb(l, k) = SUM( e%Coords(k, :) * d(l, :) )

       ENDDO

    ENDDO

    n_i = Cross_product(Jb)

    DEALLOCATE( d, Jb )

  END FUNCTION normal
  !==================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%    QUADRANGLE Q1   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  !
  !                                  ^ y
  !                                  :
  !                           (4)    :    (3)
  !                            o-----:-----o
  !                            |     :     |
  !                            |     :.....|.....> x
  !                            |           |
  !                            |           |
  !                            o-----------o
  !                           (1)         (2)
  !

  !=====================================================
  FUNCTION basis_function_QUA_Q1(e, i, xi) RESULT(psi_i)
  !=====================================================

    IMPLICIT NONE

    CLASS(quadrangle_F)                    :: e
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    REAL(KIND=8) :: x, y
    !-------------------------------------------

    x = xi(1); y = xi(2)

    SELECT CASE (i)
       
    CASE(1)

       psi_i = (1.d0 - x)*(1.d0 - y)/4.d0

    CASE(2)

       psi_i = (1.d0 + x)*(1.d0 - y)/4.d0

    CASE(3)
       psi_i = (1.d0 + x)*(1.d0 + y)/4.d0
       
    CASE(4)       
       
       psi_i = (1.d0 - x)*(1.d0 + y)/4.d0

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Quadrangle baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_QUA_Q1
  !=================================

  !=====================================================
  FUNCTION gradient_ref_QUA_Q1(e, i, xi) RESULT(D_psi_i)
  !=====================================================

    IMPLICIT NONE

    CLASS(quadrangle_F)                    :: e
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------

    REAL(KIND=8) :: x, y
    !-------------------------------------------

    x = xi(1); y = xi(2)

    SELECT CASE(i)

    CASE(1)

       D_psi_i(1) = (-1.d0 + y)/4.d0
       D_psi_i(2) = (-1.d0 + x)/4.d0

    CASE(2)

       D_psi_i(1) = ( 1.d0 - y)/4.d0       
       D_psi_i(2) = (-1.d0 - x)/4.d0

    CASE(3)

       D_psi_i(1) = ( 1.d0 + y)/4.d0       
       D_psi_i(2) = ( 1.d0 + x)/4.d0
 
    CASE(4)

       D_psi_i(1) = (-1.d0 - y)/4.d0       
       D_psi_i(2) = ( 1.d0 - x)/4.d0

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Quadrangle gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_QUA_Q1
  !===============================  

  !========================================
  FUNCTION DOFs_ref_QUA_Q1(e) RESULT(x_dof)
  !========================================

    IMPLICIT NONE

    CLASS(quadrangle_F) :: e

    REAL(KIND=8), DIMENSION(4,2) :: x_dof
    !------------------------------------

    x_dof(1, :) = (/ -1.d0,  -1.d0  /)
    x_dof(2, :) = (/  1.d0,  -1.d0  /)
    x_dof(3, :) = (/  1.d0,   1.d0  /)
    x_dof(4, :) = (/ -1.d0,   1.d0  /)
    
  END FUNCTION DOFs_ref_QUA_Q1
  !===========================

  !===================================
  SUBROUTINE init_quadrature_QUA_Q1(e)
  !===================================

    IMPLICIT NONE

    CLASS(quadrangle_F) :: e
    !----------------------------------------------

    e%N_quad = 4

    ALLOCATE( e%n_q(e%N_dim,      e%N_quad) )
    ALLOCATE( e%phi_q(e%N_points, e%N_quad) )

    ALLOCATE( e%w_q(e%N_quad   ) )
    ALLOCATE( e%x_q(e%N_quad, 2) )

    ALLOCATE( e%xx_q(e%N_dim, e%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !--------------------------------------
    e%x_q(1,:) = (/ -1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0) /)
    e%x_q(2,:) = (/  1.d0/DSQRT(3.d0), -1.d0/DSQRT(3.d0) /)
    e%x_q(3,:) = (/  1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0) /)
    e%x_q(4,:) = (/ -1.d0/DSQRT(3.d0),  1.d0/DSQRT(3.d0) /)
    
    e%w_q = 1.d0
    !--------------------------------------
        
    e%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_QUA_Q1
  !====================================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%    QUADRANGLE Q2   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !
  !                                   ^ y
  !                                   :
  !                           (4)  (7):    (3)
  !                            o------o-----o
  !                            |      :     |
  !                            |      :     |
  !                         (8)o      o.....o.....> x
  !                            |    (9)     |(6)
  !                            |            |
  !                            o------o-----o
  !                           (1)    (5)    (2)
  !

  !=====================================================
  FUNCTION basis_function_QUA_Q2(e, i, xi) RESULT(psi_i)
  !=====================================================

    IMPLICIT NONE

    CLASS(quadrangle_F)                    :: e
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    REAL(KIND=8) :: x, y, t1, t3
    !-------------------------------------------

    x = xi(1); y = xi(2)

    t1 = x*x; t3 = y*y
     
    SELECT CASE (i)
       
    CASE(1)
 
      psi_i = (t1 - x)*(t3 - y)/4.d0

    CASE(2)

      psi_i = (t1 + x)*(t3 - y)/4.d0

    CASE(3)
 
      psi_i = (t1 + x)*(t3 + y)/4.d0

    CASE(4)       

      psi_i = (t1 - x)*(t3 + y)/4.d0

   CASE(5)

      psi_i = (-t1 + 1.d0)*(t3 - y)/2.d0

   CASE(6)

      psi_i = (t1 + x)*(-t3 + 1.d0)/2.d0

   CASE(7)

      psi_i = (-t1 + 1.d0)*(t3 + y)/2.d0

   CASE(8)

      psi_i = (t1 - x)*(-t3 + 1.d0)/2.d0

   CASE(9)

      psi_i = (-t1 + 1.d0)*(-t3 + 1.d0)
            
   CASE DEFAULT
      
       WRITE(*,*) 'ERROR: not supported Dof in Quadrangle  baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_QUA_Q2
  !=================================

  !=====================================================
  FUNCTION gradient_ref_QUA_Q2(e, i, xi) RESULT(D_psi_i)
  !=====================================================

    IMPLICIT NONE

    CLASS(quadrangle_F)                    :: e
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------

    REAL(KIND=8) :: x, y, t1, t2
    !-------------------------------------------

    x = xi(1); y = xi(2)

    t1 = x*x; t2 = y*y
    
    SELECT CASE(i)

    CASE(1)
     
       D_psi_i(1) = (x - 1.d0/2.d0)*(t2 - y)/2.d0
       D_psi_i(2) = (t1 - x)*(y - 1.d0/2.d0)/2.d0

      
    CASE(2)

      D_psi_i(1) = (x + 1.d0/2.d0)*(t2-y)/2.d0
      D_psi_i(2) = (t1 + x)*(y - 1.d0/2.d0)/2.d0
             
    CASE(3)

      D_psi_i(1) = (x + 1.d0/2.d0)*(t2 + y)/2.d0
      D_psi_i(2) = (t1 + x)*(y + 1.d0/2.d0)/2.d0

    CASE(4)

      D_psi_i(1) = (x - 1.d0/2.d0)*(t2 + y)/2.d0
      D_psi_i(2) = (t1 - x)*(y + 1.d0/2.d0)/2.d0

     CASE(5)

      D_psi_i(1) = -x*(t2 - y)
      D_psi_i(2) = (-t1 + 1)*(y - 1.d0/2.d0)

     CASE(6)

      D_psi_i(1) = (x + 1.d0/2.d0)*(-t2 + 1)
      D_psi_i(2) = -(t1 + x)*y

     CASE(7)

      D_psi_i(1) = -x*(t2 + y)
      D_psi_i(2) = (-t1 + 1)*(y + 1.d0/2.d0)

     CASE(8)

      D_psi_i(1) = (x - 1.d0/2.d0)*(-t2 + 1)
      D_psi_i(2) = -(t1 - x)*y

     CASE(9)

      D_psi_i(1) = -2*x*(-t2 + 1)
      D_psi_i(2) = -2*(-t1 + 1)*y
      
    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Quadrangle gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_QUA_Q2
  !===============================

  !========================================
  FUNCTION DOFs_ref_QUA_Q2(e) RESULT(x_dof)
  !========================================

    IMPLICIT NONE

    CLASS(quadrangle_F) :: e

    REAL(KIND=8), DIMENSION(9,2) :: x_dof
    !------------------------------------

    x_dof(1, :) = (/ -1.d0,  -1.d0  /)
    x_dof(2, :) = (/  1.d0,  -1.d0  /)
    x_dof(3, :) = (/  1.d0,   1.d0  /)
    x_dof(4, :) = (/ -1.d0,   1.d0  /)
    x_dof(5, :) = (/  0.d0,  -1.d0  /)
    x_dof(6, :) = (/  1.d0,   0.d0  /)
    x_dof(7, :) = (/  0.d0,   1.d0  /)
    x_dof(8, :) = (/ -1.d0,   0.d0  /)
    x_dof(9, :) = (/  0.d0,   0.d0  /)    
    
  END FUNCTION DOFs_ref_QUA_Q2
  !===========================

  !===================================
  SUBROUTINE init_quadrature_QUA_Q2(e)
  !===================================

    IMPLICIT NONE

    CLASS(quadrangle_F) :: e
    !----------------------------------------------

    e%N_quad = 9

    ALLOCATE( e%phi_q(e%N_points, e%N_quad) )
    ALLOCATE(   e%n_q(e%N_dim,    e%N_quad) )

    ALLOCATE( e%w_q(e%N_quad   ) )
    ALLOCATE( e%x_q(e%N_quad, 2) )

    ALLOCATE( e%xx_q(e%N_dim, e%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !--------------------------------------
    e%x_q(1,:) = (/ -DSQRT(0.6d0), -DSQRT(0.6d0) /)
    e%x_q(2,:) = (/        0.d0 ,  -DSQRT(0.6d0) /)
    e%x_q(3,:) = (/  DSQRT(0.6d0), -DSQRT(0.6d0) /)
    e%x_q(4,:) = (/ -DSQRT(0.6d0),         0.d0  /)
    e%x_q(5,:) = (/        0.d0 ,          0.d0  /)
    e%x_q(6,:) = (/  DSQRT(0.6d0),         0.d0  /)
    e%x_q(7,:) = (/ -DSQRT(0.6d0),  DSQRT(0.6d0) /)
    e%x_q(8,:) = (/        0.d0 ,   DSQRT(0.6d0) /)
    e%x_q(9,:) = (/  DSQRT(0.6d0),  DSQRT(0.6d0) /)
    
    e%w_q(1) = 25.d0/81.d0
    e%w_q(2) = 40.d0/81.d0
    e%w_q(3) = 25.d0/81.d0
    e%w_q(4) = 40.d0/81.d0
    e%w_q(5) = 64.d0/81.d0
    e%w_q(6) = 40.d0/81.d0
    e%w_q(7) = 25.d0/81.d0
    e%w_q(8) = 40.d0/81.d0
    e%w_q(9) = 25.d0/81.d0
    !--------------------------------------
        
    e%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_QUA_Q2
  !====================================

END MODULE Quadrangle_F_Class
