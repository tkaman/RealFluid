MODULE Trianlge_Class

  USE Elements_Class
  USE Segment_Class
  USE PLinAlg

  IMPLICIT NONE

  !=============================================
  TYPE, PUBLIC, EXTENDS(element_str) :: triangle

     ! < >

   CONTAINS
     
     GENERIC, PUBLIC :: initialize => initialize_tri_d

     ! Doamain element
     PROCEDURE, PRIVATE, PASS :: initialize_tri_d

  END TYPE triangle
  !=============================================

  PRIVATE :: init_faces
  PRIVATE :: init_faces_TRI_P1
  PRIVATE :: init_faces_TRI_P2

  PRIVATE :: volume_quadrature

  PRIVATE :: basis_function
  PRIVATE :: basis_function_TRI_P1
  PRIVATE :: basis_function_TRI_P2
  
  PRIVATE :: gradient
  
  PRIVATE :: gradient_ref
  PRIVATE :: gradient_ref_TRI_P1
  PRIVATE :: gradient_ref_TRI_P2

  PRIVATE :: DOFs_ref
  PRIVATE :: DOFs_ref_TRI_P1
  PRIVATE :: DOFs_ref_TRI_P2
  
  !PRIVATE :: gradient_trace
  !PRIVATE :: face_trace
  PRIVATE :: face_trace_TRI_P1
  PRIVATE :: face_trace_TRI_P2  

  PRIVATE :: rd_normal
  PRIVATE :: rd_normal_TRI_P1
  PRIVATE :: rd_normal_TRI_P2

  PRIVATE :: Compute_Jacobian
      
  PRIVATE :: recovery_procedure
  PRIVATE :: init_Gradiet_Recovery_TRI_P1
  PRIVATE :: init_Gradiet_Recovery_TRI_P2

CONTAINS

  !==========================================================================
  SUBROUTINE initialize_tri_d(el, mode, Nodes, Coords, Nu_seg, n_ele, cn_ele)
  !==========================================================================

    IMPLICIT NONE

    CLASS(triangle)                          :: el
    CHARACTER(*),                 INTENT(IN) :: mode
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_seg
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    INTEGER,      DIMENSION(:,:), INTENT(IN) :: cn_ele
    !-------------------------------------------------

    INTEGER :: i, id, ne
    !-------------------------------------------------

    IF ( mode /= "Element" ) THEN       
       WRITE(*,*) 'ERROR: elment must be initialize in element mode'
       STOP
    ENDIF

    !---------------------------
    ! Attach data to the element
    !---------------------------------------------------
    ALLOCATE( el%Coords(SIZE(Coords,1), SIZE(Coords,2)) )
    ALLOCATE( el%NU(SIZE(Nodes)) )
     
    DO id = 1, SIZE(Coords, 1)
       el%Coords(id, :) = Coords(id, :)
    ENDDO

    el%NU = Nodes

    el%N_dim = SIZE(Coords, 1)

    SELECT CASE( SIZE(Nodes) )

    CASE(3)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = TRI_P1 ! Element type
       el%N_verts  = 3      ! # Vertices 
       el%N_points = 3      ! # DoFs       

    CASE(6)

       !---------
       ! ELELMENT
       !----------------------------------
       el%Type     = TRI_P2 ! Element type
       el%N_verts  = 3      ! # Vertices 
       el%N_points = 6      ! # DoFs

     CASE DEFAULT

        WRITE(*,*) 'ERROR: Unsupported Triangle type'
        STOP

     END SELECT

     ! Itialize the faces of the element
     CALL init_faces(el, Nodes, Coords, NU_seg, n_ele)

     ! Store the informations at the quadrature points
     CALL volume_quadrature(el)
        
     !--------------------------
     ! Normals for the RD scheme
     !--------------------------------------
     ALLOCATE( el%rd_n(el%N_dim, el%N_points) )

     DO i = 1, el%N_points
        el%rd_n(:, i) = rd_normal(el, i)
     ENDDO

     ALLOCATE( el%b_e_con(el%N_points) )

     DO i = 1, el%N_points

         ne = COUNT( cn_ele(i, :) /= 0 ) 

         ALLOCATE( el%b_e_con(i)%cn_ele(ne) )

         el%b_e_con(i)%cn_ele = cn_ele(i, 1:ne)

     ENDDO

     CALL nodal_gradients(el)

     ! Store the information at the recovery points
     CALL recovery_procedure(el)

   END SUBROUTINE initialize_tri_d
   !==============================

  !======================================================
  SUBROUTINE init_faces(el, Nodes, Coords, NU_seg, n_ele)
  !======================================================

    IMPLICIT NONE
    
    CLASS(triangle)                          :: el
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_seg
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    !-------------------------------------------------

    SELECT CASE(el%Type)
              
    CASE(TRI_P1)

       CALL init_faces_TRI_P1(el, Nodes, Coords, NU_seg, n_ele)
       
    CASE(TRI_P2)

       CALL init_faces_TRI_P2(el, Nodes, Coords, NU_seg, n_ele)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tiangle type for face initialization'
       WRITE(*,*) 'STOP'

    END SELECT 

  END SUBROUTINE init_faces
  !========================

  !===============================
  SUBROUTINE volume_quadrature(el)
  !===============================
  !
  ! Store the following  quantities at the quadrature points
  !    - value of the basis function
  !    - normal versor
  !    - weight of the quadrature formula (multipplied by 
  !      the jacobian of the transformation)
  !    - value of the trace of the gradient of basis functions
  !    - physical coordiantes of the quadrature point
  !
  ! Compute the lenght of the segment
  !
    IMPLICIT NONE

    CLASS(triangle) :: el
    !-----------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ

    INTEGER :: iq, k
    !-----------------------------------------------

    SELECT CASE(el%Type)

    CASE(TRI_P1)

       CALL init_quadrature_TRI_P1(el)
       
    CASE(TRI_P2)

       CALL init_quadrature_TRI_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Triangle type for quadrature'
       WRITE(*,*) 'STOP'

    END SELECT

    !-------------------------------------    
    ! Attach data to the quadrature points
    !---------------------------------------------------
    ALLOCATE( JJ(el%N_dim, el%N_dim) )

    DO iq = 1, el%N_quad

       DO k = 1, el%N_points

          el%phi_q(k, iq) = basis_function( el, k, el%x_q(iq, :) )

          el%xx_q(:, iq) = el%xx_q(:, iq) + &               
                           basis_function( el, k, el%x_q(iq, :) ) * el%Coords(:, k)
 
          el%D_phi_q(:, k, iq) = gradient( el, k, el%x_q(iq, :) )

       ENDDO

       ! remark: not optimized, jacobian computed twice       
       JJ = Compute_Jacobian( el, el%x_q(iq, :) )

       el%w_q(iq) = el%w_q(iq) * determinant(JJ)
      
    ENDDO

    DEALLOCATE( JJ )

    ! Area of the element
    el%volume = SUM( el%w_q )    

    IF( el%volume < 0.d0 ) THEN
        WRITE(*,*) 'ERROR: Wrong node ordering, volume negative'
        STOP
     ENDIF

  END SUBROUTINE volume_quadrature
  !===============================

  !=============================
  SUBROUTINE nodal_gradients(el)
  !=============================
  !
  ! Compute the gradient of the basis functions
  ! at the DOFs of the element
  !
    IMPLICIT NONE

    CLASS(triangle) :: el
    !-----------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof

    INTEGER :: i, k
    !-----------------------------------------------

    ALLOCATE( x_dof(el%N_points, 3) )
    
    x_dof = DOFs_ref(el)
    
    ALLOCATE( el%D_phi_k(el%N_dim, el%N_points, el%N_points) )

    DO i = 1, el%N_points

       DO k = 1, el%N_points

          el%D_phi_k(:, i, k) = gradient( el, i, x_dof(k, :) )

       ENDDO

    ENDDO
    
    DEALLOCATE(x_dof)

  END SUBROUTINE nodal_gradients
  !=============================

  !================================
  SUBROUTINE recovery_procedure(el)
  !================================

    IMPLICIT NONE

    CLASS(triangle) :: el
    !-----------------------------------------------

    INTEGER :: iq, k
    !-----------------------------------------------

    SELECT CASE(el%Type)

    CASE(TRI_P1)

       CALL init_Gradiet_Recovery_TRI_P1(el)
       
    CASE(TRI_P2)

       CALL init_Gradiet_Recovery_TRI_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Triangle type for recovery'
       WRITE(*,*) 'STOP'

    END SELECT

    !-------------------------------------    
    ! Attach data to the recovey points
    !---------------------------------------------------
    DO iq = 1, el%N_sampl

       DO k = 1, el%N_points
         
          el%xx_R(:, iq) = el%xx_R(:, iq) + &               
                           basis_function( el, k, el%x_R(iq, :) ) * el%Coords(:, k)

          el%D_phi_R(:, k, iq) = gradient( el, k, el%x_R(iq, :) )

       ENDDO
     
    ENDDO

  END SUBROUTINE recovery_procedure
  !================================

!*******************************************************************************
!*******************************************************************************
!                         COMPLEMENTARY FUNCTIONS                              !
!*******************************************************************************
!*******************************************************************************

  !===============================================
  FUNCTION basis_function(el, i, xi) RESULT(psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(triangle)                        :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(TRI_P1)

       psi_i = basis_function_TRI_P1(el, i, xi)
       
    CASE(TRI_P2)

       psi_i = basis_function_TRI_P2(el, i, xi)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tiangle type for basis functions'
       WRITE(*,*) 'STOP'

    END SELECT

  END FUNCTION basis_function
  !==========================

  !===============================================
  FUNCTION gradient_ref(el, i, xi) RESULT(D_psi_i)
  !===============================================

    IMPLICIT NONE

    CLASS(triangle)                         :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------
      
    SELECT CASE(el%Type)
       
    CASE(TRI_P1)

       D_psi_i = gradient_ref_TRI_P1(el, i, xi)
       
    CASE(TRI_P2)

       D_psi_i = gradient_ref_TRI_P2(el, i, xi)
     
    CASE DEFAULT

       WRITE(*,*) 'Unknown Triangle type for gradients'
       WRITE(*,*) 'STOP'

    END SELECT    

  END FUNCTION gradient_ref
  !========================

  !===========================================
  FUNCTION gradient(el, i, xi) RESULT(D_psi_i)
  !===========================================
  !
  ! Compute the gradient of the shape function i
  ! on the actual triangle at the point of baricentric
  ! coordinates xi
  !  
    IMPLICIT NONE

    CLASS(triangle)                         :: el
    INTEGER,                     INTENT(IN) :: i    
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(el%N_dim) :: D_psi_i
    !-------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: JJ, inv_J
    !---------------------------------------------

    ALLOCATE(    JJ(el%N_dim, el%N_dim) )
    ALLOCATE( inv_J(el%N_dim, el%N_dim) )
        
    JJ = compute_Jacobian( el, xi )
    
    inv_J = inverse_easy(JJ)

    D_Psi_i = MATMUL( inv_J, gradient_ref(el, i, xi) )

    DEALLOCATE( JJ, inv_J )

  END FUNCTION gradient
  !====================
  
  !=====================================
  FUNCTION rd_normal(el, k) RESULT(nn_k)
  !=====================================

    IMPLICIT NONE

    CLASS(triangle)     :: el
    INTEGER, INTENT(IN) :: k

    REAL(KIND=8), DIMENSION(el%N_dim) :: nn_k
    !----------------------------------------   

    INTEGER :: i, j
    !----------------------------------------

    SELECT CASE(el%Type)
       
    CASE(TRI_P1)

       nn_k = rd_normal_TRI_P1(el, k)
       
    CASE(TRI_P2)

       nn_k = rd_normal_TRI_P2(el, k)
     
    CASE DEFAULT

       WRITE(*,*) 'Unknown Triangle type for rd_normal'
       WRITE(*,*) 'STOP'

    END SELECT    

!    i = el%faces(k)%f%l_NU(1)
!    j = el%faces(k)%f%l_NU(2)

!    nn_k(1) = el%Coords(2,i) - el%Coords(2,j)
!    nn_k(2) = el%Coords(1,j) - el%Coords(1,i)
    
  END FUNCTION rd_normal
  !=====================

!!$  !=========================================
!!$  SUBROUTINE gradient_trace(el, jf, p_D_phi)
!!$  !=========================================
!!$  !
!!$  ! Compute the trace of the gradients of all shape
!!$  ! functions on the face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(triangle)                             :: el
!!$    INTEGER,                        INTENT(IN)  :: jf
!!$    REAL(KIND=8), DIMENSION(:,:,:), INTENT(OUT) :: p_D_phi
!!$    !-----------------------------------------------------
!!$
!!$    REAL(kind=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    INTEGER :: N_points_ele, N_points_face, k, i
!!$    !-----------------------------------------------------
!!$
!!$    N_points_ele  = e%N_points
!!$    N_points_face = SIZE(p_D_phi, 2)
!!$
!!$    ALLOCATE( xi_f(N_points_face, 3) )
!!$
!!$    xi_f = face_trace(e, jf)
!!$
!!$    DO k = 1, N_points_face      
!!$
!!$       DO i = 1, N_points_ele
!!$
!!$          p_D_phi(:, k, i) = gradient( e, i, xi_f(k,:) )
!!$
!!$       ENDDO
!!$
!!$    ENDDO
!!$    
!!$    DEALLOCATE(xi_f)    
!!$
!!$  END SUBROUTINE gradient_trace
!!$  !============================

!!$  !=======================================
!!$  FUNCTION face_trace(el, jf) RESULT(xi_f)
!!$  !=======================================
!!$  !
!!$  ! Compute the baricentric coordinates on the
!!$  ! face jf
!!$  !
!!$    IMPLICIT NONE
!!$
!!$    CLASS(triangle)          :: el
!!$    INTEGER,      INTENT(IN) :: jf
!!$    
!!$    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
!!$    !---------------------------------------------
!!$     
!!$    SELECT CASE(e%Type)
!!$       
!!$    CASE(TRI_P1)
!!$
!!$       ALLOCATE( xi_f(2, 3) )
!!$
!!$       xi_f = face_trace_TRI_P1(e, jf)
!!$       
!!$    CASE(TRI_P2)
!!$
!!$       ALLOCATE( xi_f(3, 3) )
!!$
!!$       xi_f = face_trace_TRI_P2(e, jf)
!!$       
!!$    CASE DEFAULT
!!$
!!$       WRITE(*,*) 'Unknown Tiangle type for face trace'
!!$       WRITE(*,*) 'STOP'
!!$
!!$    END SELECT 
!!$
!!$  END FUNCTION face_trace
!!$  !======================

  !==================================
  FUNCTION DOFs_ref(el) RESULT(x_dof)
  !==================================
  !
  ! Compute the coordinates of the DOFs
  ! on the reference element
  !
    IMPLICIT NONE

    CLASS(triangle) :: el
    
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_dof
    !-------------------------------------------------
     
    SELECT CASE(el%Type)
       
    CASE(TRI_P1)

       ALLOCATE( x_dof(3, 3) )

       x_dof = DOFs_ref_TRI_P1(el)
       
    CASE(TRI_P2)

       ALLOCATE( x_dof(6, 3) )

       x_dof = DOFs_ref_TRI_P2(el)
       
    CASE DEFAULT

       WRITE(*,*) 'Unknown Tiangle type for DOFs'
       WRITE(*,*) 'STOP'

    END SELECT 

  END FUNCTION DOFs_ref
  !====================

  !===========================================
  FUNCTION Compute_Jacobian(el, xi) RESULT(JJ)
  !===========================================

    IMPLICIT NONE

    CLASS(triangle)                         :: el
    REAL(KIND=8),  DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8),  DIMENSION(el%N_dim, el%N_dim) :: JJ
    !------------------------------------------------

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: d

    REAL(KIND=8) :: det

    INTEGER :: k, l, m
    !------------------------------------------------

    ALLOCATE( d(el%N_dim, el%N_points) )

    DO m = 1, el%N_points
       d(:, m) = gradient_ref(el, m, xi)
    ENDDO

    ! Construction of the Jacobian matrix
    DO k = 1, el%N_dim

       DO l = 1, el%N_dim

          JJ(l, k) = SUM( el%Coords(k, :) * d(l, :) )

       ENDDO

    ENDDO

    DEALLOCATE( d )

    det = determinant(JJ)

    IF( det <= 0.d0 ) THEN
       WRITE(*,*) 'Negative tetrahedron Jacobian'
       WRITE(*,*) 'STOP'
       STOP
    ENDIF
    
  END FUNCTION Compute_Jacobian
  !============================  

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%     TRIANGLE P1    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !
  !                           (3)
  !                            o\
  !                            |  \
  !                            |    \
  !                            |      \
  !                            |        \
  !                            o---------o
  !                           (1)       (2)
  !

  !=============================================================
  SUBROUTINE init_faces_TRI_P1(el, Nodes, Coords, NU_seg, n_ele)
  !=============================================================

    IMPLICIT NONE

    CLASS(triangle)                          :: el
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_seg
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    !-------------------------------------------------

    TYPE(segment), POINTER :: seg_pt

    INTEGER,      DIMENSION(:),   ALLOCATABLE :: loc
    INTEGER,      DIMENSION(:),   ALLOCATABLE :: VV
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: RR

    INTEGER :: id, jf, i, j, k, N_ln, istat
    !-------------------------------------------------

    el%N_faces = 3 ! # Faces
       
    ALLOCATE( el%faces(el%N_faces) )

    ! # of local nodes    
    N_ln = 2
    
    ! DoFs on the faces
    ALLOCATE( VV(N_ln), loc(N_ln) )

    ! Coordinates on the faces
    ALLOCATE( RR(SIZE(Coords, 1), N_ln) )

    DO jf = 1, el%N_faces

       i = MOD(jf, MIN(jf+1, el%N_faces)) + 1
       j = MOD(i,  MIN(jf+2, el%N_faces)) + 1

       loc = (/ i, j /)

       VV = Nodes(loc)
       RR = Coords(:, loc)

       ALLOCATE(seg_pt, STAT=istat)          
       IF(istat /=0) THEN
          WRITE(*,*) 'ERROR: failed trianlge faces allocation'
       ENDIF
       
       CALL seg_pt%initialize( "InternalFace", &
                             loc, VV, RR, NU_seg(jf), n_ele(jf) )

       CALL seg_pt%face_quadrature()

       el%faces(jf)%f => seg_pt
          
    ENDDO

    DEALLOCATE( VV, RR, loc )

  END SUBROUTINE init_faces_TRI_P1
  !===============================

  !======================================================
  FUNCTION basis_function_TRI_P1(el, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle)                        :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    SELECT CASE (i)
       
    CASE(1,2,3)
       
       psi_i = xi(i)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle  baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_TRI_P1
  !=================================

  !======================================================
  FUNCTION gradient_ref_TRI_P1(el, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle)                        :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = (/ -1.d0, -1.d0 /)
       
    CASE(2)

       D_psi_i = (/ 1.d0, 0.d0 /)
       
    CASE(3)

       D_psi_i = (/ 0.d0, 1.d0 /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_TRI_P1
  !===============================

  !=========================================
  FUNCTION DOFs_ref_TRI_P1(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(triangle):: el

    REAL(KIND=8), DIMENSION(3, 3) :: x_dof
    !--------------------------------------

    x_dof(1, :) = (/ 1.d0, 0.d0, 0.d0  /)
    x_dof(2, :) = (/ 0.d0, 1.d0, 0.d0  /)
    x_dof(3, :) = (/ 0.d0, 0.d0, 1.d0  /)

  END FUNCTION DOFs_ref_TRI_P1
  !===========================
  
  !============================================
  FUNCTION rd_normal_TRI_P1(el, k) RESULT(nn_k)
  !============================================

    IMPLICIT NONE

    CLASS(triangle)     :: el
    INTEGER, INTENT(IN) :: k

    REAL(KIND=8), DIMENSION(el%N_dim) :: nn_k
    !---------------------------------------

    INTEGER :: i, j
    !---------------------------------------

    i = el%faces(k)%f%l_NU(1)
    j = el%faces(k)%f%l_NU(2)

    nn_k(1) = el%Coords(2,i) - el%Coords(2,j)
    nn_k(2) = el%Coords(1,j) - el%Coords(1,i)

  END FUNCTION rd_normal_TRI_P1
  !============================

  !==============================================
  FUNCTION face_trace_TRI_P1(el, jf) RESULT(xi_f)
  !==============================================

    IMPLICIT NONE

    CLASS(triangle)          :: el
    INTEGER,      INTENT(IN) :: jf

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
    !----------------------------------------------

    ALLOCATE( xi_f(2, 3) )

    SELECT CASE(jf)

    CASE(1)

       xi_f(1, :) = (/ 0.d0, 1.d0, 0.d0 /)
       xi_f(2, :) = (/ 0.d0, 0.d0, 1.d0 /)

    CASE(2)

       xi_f(1, :) = (/ 0.d0, 0.d0, 1.d0 /)
       xi_f(2, :) = (/ 1.d0, 0.d0, 0.d0 /)       
       
    CASE(3)
  
       xi_f(1, :) = (/ 1.d0, 0.d0, 0.d0 /)
       xi_f(2, :) = (/ 0.d0, 1.d0, 0.d0 /)       
            
    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong Triangle face in face trace P1'
       STOP

    END SELECT    
    
  END FUNCTION face_trace_TRI_P1
  !=============================

  !====================================
  SUBROUTINE init_quadrature_TRI_P1(el)
  !====================================

    IMPLICIT NONE

    CLASS(triangle) :: el
    !----------------------------------------------

    REAL(KIND=8) :: alpha_1, beta_1
    !-----------------------------------------------

    el%N_quad = 6

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%D_phi_q(el%N_dim, el%N_points, el%N_quad) )

    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 3) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !--------------------------------------
    alpha_1 = 0.816847572980459d0
    beta_1  = 0.091576213509771d0

    el%x_q(1,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(2,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(3,:) = (/ beta_1 , beta_1 , alpha_1 /)
    el%w_q(1:3) = 0.109951743655322d0
    
    alpha_1 = 0.108103018168070d0
    beta_1  = 0.445948490915965d0
    
    el%x_q(4,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(5,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(6,:) = (/ beta_1 , beta_1 , alpha_1 /)    
    el%w_q(4:6) = 0.223381589678011d0
    !--------------------------------------
    
    el%w_q = el%w_q*0.5d0 ! V = 0.5*det|J|  
    
    el%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_TRI_P1
  !====================================

  !==========================================
  SUBROUTINE init_Gradiet_Recovery_TRI_P1(el)
  !==========================================

    IMPLICIT NONE

    CLASS(triangle) :: el
    !---------------------

    el%N_sampl = 1

    ALLOCATE( el%D_phi_R(el%N_dim, el%N_points, el%N_sampl) )
    
    ALLOCATE( el%x_R(el%N_sampl, 3) )

    ALLOCATE( el%xx_R(el%N_dim, el%N_sampl) )

    !----------------
    ! Recovery points
    !---------------------------------------------------------
    el%x_R(1,:) = (/ 1.d0/3.d0, 1.d0/3.d0, 1.d0/3.d0 /)
   
    el%xx_R = 0.d0

  END SUBROUTINE init_Gradiet_Recovery_TRI_P1
  !==========================================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%% SPECIFIC FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%     TRIANGLE P2    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !
  !                           (3)
  !                            o\
  !                            |  \
  !                            |    \
  !                            |      \
  !                        (6) o        o (5)
  !                            |          \
  !                            |            \
  !                            o------o------o
  !                           (1)    (4)    (2)
  !

  !=============================================================
  SUBROUTINE init_faces_TRI_P2(el, Nodes, Coords, NU_seg, n_ele)
  !=============================================================

    IMPLICIT NONE

    CLASS(triangle)                          :: el
    INTEGER,      DIMENSION(:),   INTENT(IN) :: Nodes
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: Coords
    INTEGER,      DIMENSION(:),   INTENT(IN) :: NU_seg
    INTEGER,      DIMENSION(:),   INTENT(IN) :: n_ele
    !-------------------------------------------------

    TYPE(segment), POINTER :: seg_pt

    INTEGER,      DIMENSION(:),   ALLOCATABLE :: loc
    INTEGER,      DIMENSION(:),   ALLOCATABLE :: VV
    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: RR

    INTEGER :: id, jf, i, j, k, N_ln, istat
    !-------------------------------------------------

    el%N_faces  = 3 ! # Faces

    ALLOCATE( el%faces(el%N_faces) )

    ! # of local nodes
    N_ln = 3

    ! Dofs on the faces
    ALLOCATE( VV(N_ln), loc(N_ln) )

    ! Coordinates on the faces
    ALLOCATE( RR(SIZE(Coords, 1), N_ln) )
    
    DO jf = 1, el%N_faces

       i = MOD(jf, MIN(jf+1, el%N_faces)) + 1          
       j = MOD(i,  MIN(jf+2, el%N_faces)) + 1

       k = i + 3

       loc = (/ i, j, k /)

       VV = Nodes(loc)
       RR = Coords(:, loc)

       ALLOCATE(seg_pt, STAT=istat)          
       IF(istat /=0) THEN
          WRITE(*,*) 'ERROR: failed trianlge allocation'
       ENDIF

       CALL seg_pt%initialize( "InternalFace", &
                               loc, VV, RR, NU_seg(jf), n_ele(jf) )

       CALL seg_pt%face_quadrature()
         
       el%faces(jf)%f => seg_pt
          
    ENDDO

    DEALLOCATE( VV, RR, loc )

  END SUBROUTINE init_faces_TRI_P2
  !===============================

  !======================================================
  FUNCTION basis_function_TRI_P2(el, i, xi) RESULT(psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle)                        :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8) :: psi_i
    !-------------------------------------------

    SELECT CASE (i)

    CASE(1, 2, 3)
       
       psi_i = (2.d0*xi(i) - 1.d0) * xi(i)
                     
    CASE(4)

       psi_i  = 4.d0*xi(1)*xi(2)
              
    CASE(5)

       psi_i  = 4.d0*xi(2)*xi(3)
              
    CASE(6)

       psi_i = 4.d0*xi(3)*xi(1)
       
    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle  baisis function'
       STOP
       
    END SELECT

  END FUNCTION basis_function_TRI_P2
  !=================================

  !======================================================
  FUNCTION gradient_ref_TRI_P2(el, i, xi) RESULT(D_psi_i)
  !======================================================

    IMPLICIT NONE

    CLASS(triangle)                        :: el
    INTEGER,                    INTENT(IN) :: i
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xi

    REAL(KIND=8), DIMENSION(2) :: D_psi_i
    !-------------------------------------------

    SELECT CASE(i)

    CASE(1)

       D_psi_i = -(/ 4.d0*xi(1) - 1.d0, 4.d0*xi(1) - 1.d0 /)
       
    CASE(2)

       D_psi_i = (/ 4.d0*xi(2) - 1.d0, 0.d0 /)
       
    CASE(3)

       D_psi_i = (/ 0.d0, 4.d0*xi(3) - 1.d0 /)

    CASE(4)

       D_psi_i = (/ 4.d0*(xi(1)-xi(2)), -4.d0*xi(2) /)

    CASE(5)

       D_psi_i = (/ 4.d0*xi(3), 4.d0*xi(2) /)

    CASE(6)

       D_psi_i = (/ -4.d0*xi(3), 4.d0*(xi(1) - xi(3)) /)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: not supported Dof in Triangle gradient'
       STOP

    END SELECT
    
  END FUNCTION gradient_ref_TRI_P2
  !===============================

  !=========================================
  FUNCTION DOFs_ref_TRI_P2(el) RESULT(x_dof)
  !=========================================

    IMPLICIT NONE

    CLASS(triangle) :: el

    REAL(KIND=8), DIMENSION(6,3) :: x_dof
    !-------------------------------------

    x_dof(1, :) = (/ 1.d0,  0.d0,  0.d0  /)
    x_dof(2, :) = (/ 0.d0,  1.d0,  0.d0  /)
    x_dof(3, :) = (/ 0.d0,  0.d0,  1.d0  /)
    x_dof(4, :) = (/ 0.5d0, 0.5d0, 0.d0  /)
    x_dof(5, :) = (/ 0.d0,  0.5d0, 0.5d0 /)
    x_dof(6, :) = (/ 0.5d0, 0.d0,  0.5d0 /)

  END FUNCTION DOFs_ref_TRI_P2
  !===========================  

  !============================================
  FUNCTION rd_normal_TRI_P2(el, k) RESULT(nn_k)
  !============================================

    IMPLICIT NONE

    CLASS(triangle)     :: el
    INTEGER, INTENT(IN) :: k

    REAL(KIND=8), DIMENSION(el%N_dim) :: nn_k
    !---------------------------------------

    INTEGER :: i, j, kk
    !---------------------------------------

    SELECT CASE(k)

    CASE(1, 2, 3)

       i = el%faces(k)%f%l_NU(1)
       j = el%faces(k)%f%l_NU(2)

       nn_k(1) = el%Coords(2,i) - el%Coords(2,j)
       nn_k(2) = el%Coords(1,j) - el%Coords(1,i)       

    CASE(4, 5, 6)

       kk = MOD(k+1, MIN(k+2, el%N_faces)) + 1

       i = el%faces(kk)%f%l_NU(1)      
       j = el%faces(kk)%f%l_NU(2)
       
       nn_k(1) = el%Coords(2,i) - el%Coords(2,j)       
       nn_k(2) = el%Coords(1,j) - el%Coords(1,i)

       nn_k = -nn_k

    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong nodes for rd_normal'
       STOP

    END SELECT
       
  END FUNCTION rd_normal_TRI_P2
  !============================

  !==============================================
  FUNCTION face_trace_TRI_P2(el, jf) RESULT(xi_f)
  !==============================================

    IMPLICIT NONE

    CLASS(triangle)          :: el
    INTEGER,      INTENT(IN) :: jf

    REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xi_f
    !----------------------------------------------

    ALLOCATE( xi_f(3, 3) )

    SELECT CASE(jf)

    CASE(1)

       xi_f(1, :) = (/ 0.d0, 1.d0,  0.d0  /)
       xi_f(2, :) = (/ 0.d0, 0.d0,  1.d0  /)
       xi_f(3, :) = (/ 0.d0, 0.5d0, 0.5d0 /)

    CASE(2)

       xi_f(1, :) = (/ 0.d0,  0.d0, 1.d0  /)
       xi_f(2, :) = (/ 1.d0,  0.d0, 0.d0  /)       
       xi_f(3, :) = (/ 0.5d0, 0.d0, 0.5d0 /)

    CASE(3)
  
       xi_f(1, :) = (/ 1.d0,  0.d0,  0.d0 /)
       xi_f(2, :) = (/ 0.d0,  1.d0,  0.d0 /)
       xi_f(3, :) = (/ 0.5d0, 0.5d0, 0.d0 /)
            
    CASE DEFAULT

       WRITE(*,*) 'ERROR: wrong Triangle face in face trace P2'
       STOP

    END SELECT    
    
  END FUNCTION face_trace_TRI_P2
  !=============================

  !====================================
  SUBROUTINE init_quadrature_TRI_P2(el)
  !====================================

    IMPLICIT NONE

    CLASS(triangle) :: el
    !----------------------------------------------------

    REAL(KIND=8) :: alpha_1, beta_1, gamma_1
    !----------------------------------------------------

    el%N_quad = 7

    ALLOCATE( el%phi_q(el%N_points, el%N_quad) )

    ALLOCATE( el%D_phi_q(el%N_dim, el%N_points, el%N_quad) )
    
    ALLOCATE( el%w_q(el%N_quad   ) )
    ALLOCATE( el%x_q(el%N_quad, 3) )

    ALLOCATE( el%xx_q(el%N_dim, el%N_quad) )
    
    !-------------------
    ! Quadrature formula
    !--------------------------------------
!!$    alpha_1 = 0.873821971016996d0
!!$    beta_1  = 0.063089014491502d0
!!$
!!$    el%x_q(1,:) = (/ alpha_1, beta_1,  beta_1  /)
!!$    el%x_q(2,:) = (/ beta_1,  alpha_1, beta_1  /)
!!$    el%x_q(3,:) = (/ beta_1,  beta_1,  alpha_1 /)
!!$    el%w_q(1:3) = 0.050844906370207d0
!!$    
!!$    alpha_1 = 0.501426509658179d0
!!$    beta_1  = 0.249286745170910d0
!!$
!!$    el%x_q(4,:) = (/ beta_1,  beta_1,  alpha_1 /)
!!$    el%x_q(5,:) = (/ beta_1,  alpha_1, beta_1  /)
!!$    el%x_q(6,:) = (/ alpha_1, beta_1,  beta_1  /)
!!$    el%w_q(4:6) = 0.116786275726379d0
!!$
!!$    alpha_1 = 0.636502499121399d0 
!!$    beta_1  = 0.310352451033785d0
!!$    gamma_1 = 0.053145049844816d0 
!!$
!!$    el%x_q(7,:)  = (/ alpha_1, beta_1,  gamma_1 /)
!!$    el%x_q(8,:)  = (/ alpha_1, gamma_1, beta_1  /)
!!$    el%x_q(9,:)  = (/ beta_1,  alpha_1, gamma_1 /)
!!$    el%x_q(10,:) = (/ gamma_1, alpha_1, beta_1  /)
!!$    el%x_q(11,:) = (/ beta_1,  gamma_1, alpha_1 /)
!!$    el%x_q(12,:) = (/ gamma_1, beta_1,  alpha_1 /)    
!!$    el%w_q(7:12) = 0.082851075618374d0

    alpha_1 = 1.d0/3.d0 

    el%x_q(1,:) = (/ alpha_1, alpha_1 , alpha_1 /)
    el%w_q(1)   = 9.d0/40.d0
    
    beta_1 = ( 6.d0 + DSQRT(15.d0) ) /21.d0
    alpha_1 = 1.d0 - 2.d0*beta_1
    
    el%x_q(2,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(3,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(4,:) = (/ beta_1 , beta_1 , alpha_1 /)
    el%w_q(2:4) = ( 155.d0 + DSQRT(15.d0) ) / 1200.d0

    beta_1 = ( 6.d0 - DSQRT(15.d0) ) /21.d0
    alpha_1 = 1.d0 - 2.d0*beta_1
    
    el%x_q(5,:) = (/ alpha_1, beta_1 , beta_1  /)
    el%x_q(6,:) = (/ beta_1 , alpha_1, beta_1  /)
    el%x_q(7,:) = (/ beta_1 , beta_1 , alpha_1 /)    
    el%w_q(5:7) = ( 155.d0 - DSQRT(15.d0) ) / 1200.d0
    !--------------------------------------

    el%w_q = el%w_q*0.5d0 ! V = 0.5*det|J|
    
    el%xx_q = 0.d0
    
  END SUBROUTINE init_quadrature_TRI_P2
  !====================================

  !==========================================
  SUBROUTINE init_Gradiet_Recovery_TRI_P2(el)
  !==========================================

    IMPLICIT NONE

    CLASS(triangle) :: el
    !---------------------

    el%N_sampl = 3

    ALLOCATE( el%D_phi_R(el%N_dim, el%N_points, el%N_sampl) )
    
    ALLOCATE( el%x_R(el%N_sampl, 3) )

    ALLOCATE( el%xx_R(el%N_dim, el%N_sampl) )

    !----------------
    ! Recovery points
    !---------------------------------------------------------
    el%x_R(1,:) = (/ 0.66666667d0, 0.16666667d0, 0.16666667d0 /)
    el%x_R(2,:) = (/ 0.16666667d0, 0.66666667d0, 0.16666667d0 /)
    el%x_R(3,:) = (/ 0.16666667d0, 0.16666667d0, 0.66666667d0 /)

    el%xx_R = 0.d0

  END SUBROUTINE init_Gradiet_Recovery_TRI_P2
  !==========================================

END MODULE Trianlge_Class
