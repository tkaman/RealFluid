MODULE Reprise

  USE LesTypes
  USE LibMesh
  USE O2toO3
  
  IMPLICIT NONE

  !====================================
  INTERFACE Reprise_in
     MODULE PROCEDURE Reprise_inBin
  END INTERFACE
  
  INTERFACE Reprise_Visu
     MODULE PROCEDURE Reprise_visuBin
  END INTERFACE

  INTERFACE Reprise_Out
     MODULE PROCEDURE Reprise_OutBin
  END INTERFACE

  INTERFACE
     SUBROUTINE errno(mssg) !-- BIND(C, NAME="errno_")
       CHARACTER(LEN = *), PARAMETER :: mod_name = "errno"
       CHARACTER(LEN = *), INTENT(IN) :: mssg
     END SUBROUTINE errno
  END INTERFACE
  !====================================

CONTAINS

  !==================================================
  SUBROUTINE Reprise_visuBin(Data, Mesh, Var, PbName)
  !==================================================  

    TYPE(Donnees),      INTENT(INOUT) :: DATA
    TYPE(Maillage),     INTENT(INOUT) :: Mesh
    TYPE(Variables),    INTENT(INOUT) :: Var
    CHARACTER(LEN = *), INTENT(IN)    :: PbName
    !-------------------------------------------

    INTEGER :: is,  ies, jt, i, j, ifr, stat12, stat22, stat32
    INTEGER :: MyUnit, Len, ios, stat, stat1, stat2, stat3

    CHARACTER(LEN = 70) :: Hdbfile

    LOGICAL :: existe
    !-----------------------------------

    MyUnit = 7

    Hdbfile = TRIM(PbName) // ".save"

    Len     = INDEX(Hdbfile, ' ') - 1

    INQUIRE(FILE = Hdbfile, EXIST = existe)
    IF (.NOT. existe) THEN
       WRITE(*, *) 'ERROR: File ', TRIM(Hdbfile), ' not found'
       CALL stop_run()
    END IF

    OPEN( UNIT = MyUnit, FILE = TRIM(Hdbfile), STATUS = 'old', &
          FORM = 'unformatted', CONVERT = 'BIG_ENDIAN', IOSTAT = ios)

    READ(MyUnit, IOSTAT = stat) Data%sel_modele

    READ (MyUnit) Data%Ndim,     &
                  Mesh%Npoint,   &
                  Mesh%Nelement, &                  
                  Data%Nvar,     &
                  Data%NvarPhy,  &
                  Data%Nordre

    Var%NCells = Mesh%Npoint
    
    ALLOCATE( Data%VarNames(Data%NvarPhy) )

    ALLOCATE( Var%Vp(Data%NvarPhy, Var%Ncells) )

    ALLOCATE( Data%VarPhyInit(Data%NvarPhy, 1) )

    ALLOCATE( Data%Residus0(Data%Nvar), &
                Var%Residus(Data%Nvar) )

    READ(MyUnit, IOSTAT = stat) Var%Temps, Data%kt0, Var%CFL,  &
                                Data%Residus0(:), Var%Residus, &
                                Var%Resid_1, Var%Resid_2

    !----------------
    ! Domain elements
    !-----------------------------------
    Mesh%Ndofs = Var%Ncells

    IF (Data%sel_modele == cs_modele_eulerns) THEN
       ALLOCATE( Var%D_Ua(Data%Ndim, Data%Nvar, Var%Ncells) )
    ENDIF
    
    Var%kt = Data%kt0

    Mesh%Ndim = Data%Ndim

    ALLOCATE( Mesh%Maill(Mesh%Nelement) )

    DO jt = 1, Mesh%nelement

       READ(MyUnit, IOSTAT = stat1) Mesh%Maill(jt)%Nsommets, &
                                    Mesh%Maill(jt)%Ndegre,   &
                                    Mesh%Maill(jt)%EltType

       ALLOCATE( Mesh%Maill(jt)%coor(Data%Ndim, Mesh%Maill(jt)%Ndegre))
       ALLOCATE( Mesh%Maill(jt)%nu  (           Mesh%Maill(jt)%Ndegre))

       READ(MyUnit, IOSTAT = stat2) ( (Mesh%Maill(jt)%coor(i,j), &
                                       i = 1, Data%Ndim), &
                                       j = 1, Mesh%Maill(jt)%Ndegre )

       READ(MyUnit, IOSTAT = stat3) ( Mesh%Maill(jt)%Nu(i), &
                                      i = 1, Mesh%Maill(jt)%Ndegre )

    ENDDO

    READ (MyUnit, IOSTAT=ios) ( (Var%Vp(ies, is),         &
                                  ies = 1, Data%NvarPhy), &
                                   is = 1, Var%Ncells     )
    
    IF (Data%sel_modele == cs_modele_eulerns) THEN

       ! IF(turbulent) mut_s --> mu_s (eddy viscosity)
       
       READ (MyUnit, IOSTAT=ios) ( (Var%D_Ua(:, ies, is), &
                                     ies = 1, Data%Nvar), &
                                      is = 1, Var%Ncells  )
    ENDIF

    !------------------
    ! Boundary elements
    !---------------------------------
    READ(MyUnit) Mesh%NfacFr
    
    IF( Mesh%NfacFr > 0 ) THEN

       READ(MyUnit) Data%N_wall_points

       ALLOCATE( Mesh%fr(Mesh%NfacFr) )
       
       DO ifr = 1, Mesh%NFacFr

          READ(MyUnit, IOSTAT=stat12) Mesh%fr(ifr)%Nsommets, &
                                      Mesh%fr(ifr)%Ndegre,   &
                                      Mesh%fr(ifr)%EltType,  &
                                      Mesh%fr(ifr)%inum

          ALLOCATE( Mesh%fr(ifr)%coor(Data%Ndim, Mesh%fr(ifr)%Ndegre) )
          ALLOCATE( Mesh%fr(ifr)%nu(             Mesh%fr(ifr)%Ndegre) )

          READ(MyUnit, IOSTAT = stat22) ( (Mesh%fr(ifr)%coor(i,j),     &
                                          i = 1, Data%Ndim),           &
                                          j = 1, Mesh%fr(ifr)%Ndegre )

          READ(MyUnit, IOSTAT = stat32)( Mesh%fr(ifr)%Nu(i),          &
                                         i = 1, Mesh%fr(ifr)%Ndegre )

          READ(MyUnit, IOSTAT = stat32) Mesh%fr(ifr)%inum !*
          
       ENDDO

       ALLOCATE( Var%Cp(Data%N_wall_points), &
                 Var%Cf(Data%N_wall_points) )

       DO i = 1, Data%N_wall_points

          READ(MyUnit, IOSTAT = stat32) Var%Cp(i), Var%Cf(i)
                         
       ENDDO

    ENDIF

    READ(MyUnit) ( Data%VarNames(ies), &
                   ies = 1, data%NvarPhy)
   
    READ(MyUnit) Data%LoiEtat,     &
                 Data%sel_loivisq, &
                 Data%LoiTh

    ! rho_oo, V_oo, T_oo, P_oo
    READ(MyUnit) Data%VarPhyInit(1,             :), &
                 Data%VarPhyInit(2:Data%Ndim+1, :), & 
                 Data%VarPhyInit(Data%Ndim+5,   :), &
                 Data%VarPhyInit(Data%Ndim+3,   :)
                 
    READ(MyUnit) Data%RhoRef, &
                 Data%VRef,   &
                 Data%LRef,   &
                 Data%TRef

    READ(MyUnit) Var%CL, Var%CD_P, Var%CD_V

    READ(MyUnit, IOSTAT=stat) Data%N_wall_Faces

    IF(stat == 0) THEN ! For compatibility

       IF( Data%N_wall_Faces > 0 ) THEN

          ALLOCATE( Var%C_loc(3, Data%N_wall_Faces) )
          READ(MyUnit) ( Var%C_loc(:, i), &
                         i = 1, Data%N_wall_Faces )

       ENDIF

    ENDIF

    CLOSE(MyUnit)
    
  END SUBROUTINE Reprise_visuBin
  !=============================

  !===============================================
  SUBROUTINE Reprise_InBin(Com, Data, Var, PbName)
  !===============================================

    TYPE(MeshCom),      INTENT(IN)    :: Com
    TYPE(Donnees),      INTENT(INOUT) :: Data
    TYPE(Variables),    INTENT(INOUT) :: Var
    CHARACTER(LEN = *), INTENT(IN)    :: PbName
    !-----------------------------------------

    INTEGER :: is, ies, ios, jt

    INTEGER :: Ndim, Ncells, N_order
    INTEGER :: Nvar, NvarPhy, N_ele, N_P, N_B_P
    INTEGEr :: sel_modele, b_faces

    INTEGER :: stat1, stat2, stat3, stat, &
               stat12, stat22, stat32

    REAL    :: dummy
    INTEGER :: i_dummy

    REAL, PARAMETER :: toll = 1.0E-10

    REAL, DIMENSION(Data%NDim) :: GC

    INTEGER :: i, j, ll, ifr
    
    INTEGER ::  MyUnit

    CHARACTER(LEN=70) :: Hdbfile
    !------------------------------------------

    MyUnit = 7

    Hdbfile = TRIM(PbName) // ".save"

    OPEN(UNIT = MyUnit, FILE = TRIM(Hdbfile), STATUS = 'old', &
         FORM = 'unformatted', CONVERT = 'BIG_ENDIAN', IOSTAT = ios)

    IF (ios /= 0) THEN
       WRITE(*,*) 'ERROR: file ', TRIM(PbName), '.save'
       CALL stop_run()
    END IF

    REWIND(MyUnit)

    WRITE(*, *) 'Reading on Hdbfile ', Hdbfile
    
    READ(MyUnit, IOSTAT = stat) sel_modele

    IF( sel_modele /= Data%sel_modele ) THEN
       WRITE(*,*) 'ERROR: mismatch in the model'
       WRITE(*,*) 'model ', sel_modele
       WRITE(*,*) 'Data model ', Data%sel_modele
       STOP
    ENDIF

    READ(MyUnit) Ndim,     &
                 NCells,   &
                 N_ele,    & 
                 Nvar,     &
                 NvarPhy,  &
                 N_order

    IF( Ndim /= Data%Ndim ) THEN
       WRITE(*,*) 'ERROR: mismatch in spatial dimensions'
       WRITE(*,*) 'N_dim ', Ndim
       WRITE(*,*) 'Data N_dim ', Data%Ndim
       STOP
    ENDIF

    IF( Nvar /= Data%Nvar .OR. NvarPhy /= Data%NvarPhy) THEN
       WRITE(*,*) 'ERROR: mismatch in the number of variables'
       WRITE(*,*) 'Nvar ', Nvar, 'Data Nvar',  Data%Nvar
       WRITE(*,*) 'NvarPhy ', NvarPhy, 'Data NvarPhy',  Data%NvarPhy 
       STOP
    ENDIF
                                           !--- kt >>
    READ(MyUnit, IOSTAT = stat) Var%Temps, Var%kt, Var%CFL,  &
                                Data%Residus0, Var%Residus,  &
                                Var%Resid_1, Var%Resid_2

    Data%kt0 = Var%kt

    Var%Res_oo = 1.d0

    !----------------
    ! Domain elements
    !-----------------------------------
    IF( N_Order == 2 .AND. Data%Nordre == 3 ) THEN

       CALL interp_O2_to_O3(Myunit, NCells, N_ele, Var)

    ELSE

       DO jt = 1, N_ele

! dummy
          READ(MyUnit, IOSTAT = stat1) i_dummy, & !   Mesh%Maill(jt)%Nsommets
                                       N_P,     & !   Mesh%Maill(jt)%Ndegre 
                                       i_dummy    !   Mesh%Maill(jt)%EltType
! dummy
          READ(MyUnit, IOSTAT = stat2) ( (dummy,        & ! Mesh%Maill(jt)%coor(i,j)
                                          i = 1, Ndim), &
                                          j = 1, N_P )  
! dummy
          READ(MyUnit, IOSTAT = stat3) ( i_dummy, &    ! Mesh%Maill(jt)%Nu(i)
                                         i = 1, N_P )

       ENDDO

       IF( N_Order == Data%Nordre ) THEN

          READ(MyUnit, IOSTAT=ios) ( (Var%Vp(ies, is),   &
                                      ies = 1, NvarPhy), &
                                      is = 1, Ncells    )
       ELSE

          WRITE(*,*) 'ERROR: mismatch order solution'
          STOP

       ENDIF

    ENDIF

    CLOSE(MyUnit)
   
  END SUBROUTINE Reprise_InBin
  !===========================

  !===========================================
  SUBROUTINE Reprise_OutBin(Data, Var, PbName)
  !===========================================

    CHARACTER(LEN = *), PARAMETER :: mod_name = "Reprise_OutBin"
    
    TYPE(Donnees),      INTENT(IN)    :: Data
    TYPE(Variables),    INTENT(INOUT) :: Var
    CHARACTER(LEN = *), INTENT(IN)    :: PbName
    !-----------------------------------------

    REAL, DIMENSION(:), ALLOCATABLE :: Cp, Cf

    INTEGER :: is, ies, ios, je, jf, id, bc_type

    INTEGER :: stat1, stat2, stat3, stat, &
               stat12, stat22, stat32

    INTEGER :: i, j, ifr, c_b

    INTEGER             ::  MyUnit
    CHARACTER(LEN = 70) :: Hdbfile
    !------------------------------------------

    MyUnit = 7

    Hdbfile = TRIM(PbName) // ".save"

    OPEN(UNIT = MyUnit, FILE = TRIM(Hdbfile), STATUS = 'unknown', &
         FORM = 'unformatted', CONVERT = 'BIG_ENDIAN', IOSTAT = ios)

    IF (ios /= 0) THEN
       WRITE(*,*) 'ERROR: file ', TRIM(PbName), '.save'
       CALL stop_run()
    END IF

    REWIND(MyUnit)

    WRITE(*, *) 'Writing on Hdbfile ', TRIM(ADJUSTL(Hdbfile))

    
    WRITE(MyUnit, IOSTAT = stat) Data%sel_modele

    WRITE (MyUnit) Data%Ndim,       &
                   Var%NCells,      &
                   SIZE(d_element), &
                   Data%Nvar,       &
                   Data%NvarPhy,    &
                   Data%Nordre

    WRITE(MyUnit, IOSTAT = stat) Var%Temps, Var%kt, Var%CFL,    &
                                 Data%Residus0(:), Var%Residus, &
                                 Var%Resid_1, Var%Resid_2
    !----------------
    ! Domain elements
    !-----------------------------------
    DO je = 1, SIZE(d_element)

       WRITE(MyUnit, IOSTAT = stat1) d_element(je)%e%N_Verts,  &
                                     d_element(je)%e%N_points, &
                                     d_element(je)%e%Type

       WRITE(MyUnit, IOSTAT = stat2) ( (d_element(je)%e%Coords(i, j),  &
                                        i = 1, d_element(je)%e%N_dim), &
                                        j = 1, d_element(je)%e%N_points )

       WRITE(MyUnit, IOSTAT = stat3) (  d_element(je)%e%NU(i), &
                                        i = 1,d_element(je)%e%N_points )

    ENDDO

    WRITE(MyUnit, IOSTAT=ios) ( (Var%Vp(ies, is),         &
                                  ies = 1, Data%NvarPhy), &
                                   is = 1, Var%Ncells     )

!!$write(MyUnit, IOSTAT=ios) ( Var%Re_loc(is), (Var%Vp(ies, is), ies = 2, Data%NvarPhy),&
!!$                              is = 1, Var%Ncells     )

    IF (Data%sel_modele == cs_modele_eulerns) THEN
       
       WRITE(MyUnit, IOSTAT=ios) ( (Var%D_Ua(:, ies, is),  &            
                                     ies = 1, Data%Nvar),  &
                                      is = 1, Var%Ncells   )

    ENDIF
    
    !------------------
    ! Boundary elements
    !---------------------------------
    IF( .NOT. (ALLOCATED(b_element)) ) THEN

       c_b = 0

       ! No boundary
       WRITE(MyUnit) c_b

    ELSE

       WRITE(MyUnit) SIZE(b_element)

       WRITE(MyUnit) Data%N_wall_points

       DO jf = 1, SIZE(b_element)

          WRITE(MyUnit, IOSTAT = stat12) b_element(jf)%b_f%N_Verts,  &
                                         b_element(jf)%b_f%N_Points, &
                                         b_element(jf)%b_f%Type,     &
                                         b_element(jf)%b_f%bc_type

          WRITE(MyUnit, IOSTAT = stat22) ( (b_element(jf)%b_f%Coords(i, j), &
                                            i = 1, b_element(jf)%b_f%N_dim), &
                                            j = 1, b_element(jf)%b_f%N_points )

          WRITE(MyUnit, IOSTAT = stat32) (  b_element(jf)%b_f%NU(i), &
                                            i = 1, b_element(jf)%b_f%N_points )

          WRITE(MyUnit, IOSTAT = stat32)  Data%FacMap(b_element(jf)%b_f%bc_type)

          bc_type = Data%FacMap(b_element(jf)%b_f%bc_type)

       ENDDO
       
       DO j = 1, Data%N_wall_points

          WRITE(MyUnit, IOSTAT = stat32) Var%Cp(j), Var%Cf(j)
          
       ENDDO

    ENDIF

    WRITE(MyUnit) ( Data%VarNames(ies),  &
                   ies = 1, Data%NvarPhy )
    
    WRITE(MyUnit) Data%LoiEtat,     &
                  Data%sel_loivisq, &
                  Data%LoiTh

    WRITE(MyUnit) Data%VarPhyInit(i_rho_vp, 1),          &
                  Data%VarPhyInit(i_vi1_vp:i_vid_vp, 1), &    
                  Data%VarPhyInit(i_tem_vp, 1),          &
                  Data%VarPhyInit(i_pre_vp, 1)

    WRITE(MyUnit) Data%RhoRef, &
                  Data%VRef,   &
                  Data%LRef,   &
                  Data%TRef

    WRITE(MyUnit) Var%CL, Var%CD_P, Var%CD_V

    WRITE(MyUnit) Data%N_wall_Faces

    IF( Data%N_wall_Faces > 0 ) THEN
       WRITE(MyUnit) ( Var%C_loc(:, is),  &
                       is = 1, Data%N_wall_Faces )
    ENDIF
      
    CLOSE(MyUnit)

  END SUBROUTINE Reprise_OutBin
  !============================

  !==============================================
  SUBROUTINE Reprise_Merge(Data, Mesh, Var, file)
  !==============================================

    CHARACTER(LEN = *), PARAMETER :: mod_name = "Reprise_Merge"
    
    TYPE(Donnees),      INTENT(IN)    :: Data
    TYPE(Maillage),     INTENT(INOUT) :: Mesh
    TYPE(Variables),    INTENT(INOUT) :: Var
    CHARACTER(LEN = *), INTENT(IN)    :: file
    !-----------------------------------------

    REAL, DIMENSION(:), ALLOCATABLE :: Cp, Cf

    INTEGER :: is, ies, ios, je, jf, id, bc_type

    INTEGER :: stat1, stat2, stat3, stat, &
               stat12, stat22, stat32

    INTEGER :: jt, i, j, ifr, c_b, pos_end

    INTEGER             ::  MyUnit
    CHARACTER(LEN = 70) :: Hdbfile
    !------------------------------------------

    MyUnit = 7

    pos_end = INDEX(file, "-") - 1

    Hdbfile(1 :) = file(1:pos_end)// "-master.save"

    OPEN(UNIT = MyUnit, FILE = TRIM(Hdbfile), STATUS = 'unknown', &
         FORM = 'unformatted', CONVERT = 'BIG_ENDIAN', IOSTAT = ios)

    IF (ios /= 0) THEN
       WRITE(*,*) 'ERROR: file ', TRIM(Hdbfile)
       CALL stop_run()
    END IF

    REWIND(MyUnit)

    WRITE(*, *) 'Writing on Hdbfile ', TRIM(ADJUSTL(Hdbfile))

    WRITE(MyUnit, IOSTAT = stat) Data%sel_modele

    WRITE (MyUnit) Data%Ndim,     &
                   Mesh%Npoint,   &
                   Mesh%Nelement, &                  
                   Data%Nvar,     &
                   Data%NvarPhy,  &
                   Data%Nordre

    Var%NCells = Mesh%Npoint
    
    WRITE(MyUnit, IOSTAT = stat) Var%Temps, Data%kt0, Var%CFL,  &
                                 Data%Residus0(:), Var%Residus, &
                                 Var%Resid_1, Var%Resid_2

    !----------------
    ! Domain elements
    !-----------------------------------
    Mesh%Ndofs = Var%Ncells
    
    Var%kt = Data%kt0

    Mesh%Ndim = Data%Ndim

    DO jt = 1, Mesh%nelement

       WRITE(MyUnit, IOSTAT = stat1) Mesh%Maill(jt)%Nsommets, &
                                     Mesh%Maill(jt)%Ndegre,   &
                                     Mesh%Maill(jt)%EltType

       WRITE(MyUnit, IOSTAT = stat2) ( (Mesh%Maill(jt)%coor(i,j), &
                                        i = 1, Data%Ndim),        &
                                        j = 1, Mesh%Maill(jt)%Ndegre )

       WRITE(MyUnit, IOSTAT = stat3) ( Mesh%Maill(jt)%Nu(i), &
                                       i = 1, Mesh%Maill(jt)%Ndegre )

    ENDDO

    WRITE (MyUnit, IOSTAT=ios) ( (Var%Vp(ies, is),        &
                                  ies = 1, Data%NvarPhy), &
                                   is = 1, Var%Ncells     )
    
    IF (Data%sel_modele == cs_modele_eulerns) THEN

       ! IF(turbulent) mut_s --> mu_s (eddy viscosity)
       
       WRITE (MyUnit, IOSTAT=ios) ( (Var%D_Ua(:, ies, is), &
                                     ies = 1, Data%Nvar), &
                                      is = 1, Var%Ncells  )
    ENDIF

    !------------------
    ! Boundary elements
    !---------------------------------
    WRITE(MyUnit) Mesh%NfacFr
    
    IF( Mesh%NfacFr > 0 ) THEN

       WRITE(MyUnit) Data%N_wall_points
       
       DO ifr = 1, Mesh%NFacFr

          WRITE(MyUnit, IOSTAT=stat12) Mesh%fr(ifr)%Nsommets, &
                                       Mesh%fr(ifr)%Ndegre,   &
                                       Mesh%fr(ifr)%EltType,  &
                                       Mesh%fr(ifr)%inum

          WRITE(MyUnit, IOSTAT = stat22) ( (Mesh%fr(ifr)%coor(i,j),     &
                                           i = 1, Data%Ndim),           &
                                           j = 1, Mesh%fr(ifr)%Ndegre )

          WRITE(MyUnit, IOSTAT = stat32)( Mesh%fr(ifr)%Nu(i), &
                                          i = 1, Mesh%fr(ifr)%Ndegre )

          WRITE(MyUnit, IOSTAT = stat32) Mesh%fr(ifr)%inum !*
          
       ENDDO

       DO i = 1, Data%N_wall_points

          WRITE(MyUnit, IOSTAT = stat32) Var%Cp(i), Var%Cf(i)
  
       ENDDO

    ENDIF

    WRITE(MyUnit) ( Data%VarNames(ies), &
                   ies = 1, data%NvarPhy)
   
    WRITE(MyUnit) Data%LoiEtat,     &
                  Data%sel_loivisq, &
                  Data%LoiTh

    ! rho_oo, V_oo, T_oo, P_oo
    WRITE(MyUnit) Data%VarPhyInit(1,             :), &
                  Data%VarPhyInit(2:Data%Ndim+1, :), & 
                  Data%VarPhyInit(Data%Ndim+5,   :), &
                  Data%VarPhyInit(Data%Ndim+3,   :)
                 
    WRITE(MyUnit) Data%RhoRef, &
                  Data%VRef,   &
                  Data%LRef,   &
                  Data%TRef

    WRITE(MyUnit) Var%CL, Var%CD_P, Var%CD_V

    CLOSE(MyUnit)

  END SUBROUTINE Reprise_Merge
  !===========================


END MODULE Reprise

