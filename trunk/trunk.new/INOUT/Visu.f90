MODULE Visu

  USE LesTypes
  USE Visc_Laws
  USE EOSLaws
  USE Utils
  USE GeomParam
  USE GeomGraph
  USE svnversion
  USE ReadMesh
  IMPLICIT NONE

  REAL, PARAMETER, PRIVATE :: cteus3 = 1.0 / 3.0
  REAL, PARAMETER, PRIVATE :: cte05 = 0.5

CONTAINS

  !===================================================
  SUBROUTINE VisuTecplotAscii(Data, Mesh, Var, PbName)
  !===================================================

    IMPLICIT NONE
    
    CHARACTER(LEN = *) , PARAMETER  :: mod_name = "VisuTecplotAscii"
    
    TYPE(Donnees),       INTENT(IN)    :: Data
    TYPE(Maillage),      INTENT(INOUT) :: Mesh
    TYPE(Variables),     INTENT(IN)    :: Var
    CHARACTER(LEN = 70), INTENT(IN)    :: PbName
    !----------------------------------------------

    CHARACTER(LEN = 75)  :: Ascfile, nombre
    CHARACTER(LEN = 200) :: VarList
    INTEGER              :: Nordre, istat, ivar
    INTEGER              :: MyUnit, is, jt, k , js
    !----------------------------------------------
    REAL   , DIMENSION(:,:), POINTER :: Vp
    REAL   , DIMENSION(:,:), POINTER :: Coor
    INTEGER, DIMENSION(:)  , POINTER :: Nu
    !----------------------------------------------
    
    INTEGER :: nt, ns, nn, polyType, N_Pyr, N_dofs
    INTEGER, DIMENSION(2) :: NbOfPolys, Degre
    
    INTEGER, DIMENSION(:), ALLOCATABLE :: Nv

    INTEGER, DIMENSION(:), POINTER :: Glob2Loc
    !-----------------------------------------------

    Ascfile = " "
    nombre = " "
    VarList = " "

    Nordre = Data%Nordre

    WRITE(*,*) 'ORDER: ', Data%Nordre

!!$    ALLOCATE(      Glob2Loc(  Var%Ncells) )
!!$    ALLOCATE( coor(Data%Ndim, Var%Ncells) )
!!$    DO jt = 1, Mesh%Nelement
!!$       DO k = 1,Mesh%Maill(jt)%Nsommets
!!$          coor(:, Mesh%Maill(jt)%Nu(k)) = Mesh%Maill(jt)%coor(:,k)
!!$       ENDDO
!!$    ENDDO

    MyUnit = 97

!!$    ns = Var%Ncells
    nt = Mesh%Nelement

    Ascfile(1: ) = TRIM(ADJUSTL(PbName)) // ".plt"

    OPEN( UNIT = MyUnit, FILE = TRIM(Ascfile), IOSTAT = istat, STATUS = "REPLACE" ) !, FORM='formatted')
    IF (istat /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : ouverture de ", TRIM(Ascfile), " -> ", istat
       CALL delegate_stop()
    END IF
    REWIND(MyUnit)

    VarList = 'TITLE = "Fbx T='
    WRITE(nombre, '(ES10.4)') Var%Temps
    VarList = TRIM(VarList) // TRIM(ADJUSTL(nombre))
    WRITE(nombre, '(I8)') Data%kt0
    VarList = TRIM(VarList) // ', Kt=' // TRIM(ADJUSTL(nombre)) // '"'

    WRITE(MyUnit, *) TRIM(VarList) ! ecriture du titre du fichier

    !creation de la liste des variables
    VarList(1: ) = 'VARIABLES = "x","y'
    IF (Data%Ndim == 3 ) THEN
       VarList = TRIM(VarList) // '","z'
    END IF
    DO ivar = 1, Data%NvarPhy
       VarList = TRIM(VarList) // '","'
       VarList = TRIM(VarList) // TRIM(ADJUSTL(Data%VarNames(ivar)))
    END DO
    VarList(1: ) =  TRIM(VarList) // '"'

    WRITE(MyUnit, *)  TRIM(VarList) ! ecriture du nom des variables

    Degre(1) = MINVAL(Mesh%maill(:)%NSommets)
    Degre(2) = MAXVAL(Mesh%maill(:)%NSommets)

    ! NbOfPolys(1) == nb of triangles/Tetra
    ! NbOfPolys(2) == nb of quadrangles/Hexa
    NbOfPolys = 0
    N_Pyr = 0

    DO jt = 1, Mesh%Nelement

       SELECT CASE(Data%Ndim)
       CASE(2)

          SELECT CASE(Mesh%Maill(jt)%Ndegre)
          CASE(3) ! Triangles P1
             NbOfPolys(1) = NbOfPolys(1) + 1
          CASE(4) ! Quadrangles P1
             NbOfPolys(2) = NbOfPolys(2) + 1
          CASE(6) ! Triangles P2
             NbOfPolys(1) = NbOfPolys(1) + 4
          CASE(9) ! Quadrangles P2
             NbOfPolys(2) = NbOfPolys(2) + 4
          CASE DEFAULT
             PRINT *, mod_name, " ERREUR : cas non prevu/7 degre==", Mesh%Maill(jt)%Ndegre
             CALL delegate_stop()
          END SELECT

       CASE(3)
          
          SELECT CASE(Mesh%Maill(jt)%Ndegre)
          CASE(4) ! T�trah�dres P1
             NbOfPolys(1) = NbOfPolys(1) + 1
          CASE(8) ! Hexahedres P1
             NbOfPolys(2) = NbOfPolys(2) + 1
          CASE(5) ! Pyramid P1
             NbOfPolys(2) = NbOfPolys(2) + 1
             N_Pyr = N_Pyr + 1
          CASE(10) ! Tetrahedre P2
             NbOfPolys(1) = NbOfPolys(1) + 8
          CASE(27) ! Hexahedre P2
             NbOfPolys(2) = NbOfPolys(2) + 8  
          CASE(14) ! Pyramid P2
             NbOfPolys(2) = NbOfPolys(2) + 8
          CASE DEFAULT
             PRINT *, mod_name, " ERREUR : cas non prevu/7 degre==", Mesh%Maill(jt)%Ndegre
             CALL delegate_stop()
          END SELECT

       END SELECT
       

    END DO

    PRINT *, mod_name, " nt", nt, "Npolys", NbOfPolys

    N_dofs =  Var%Ncells + 3*N_pyr

    ALLOCATE(        Glob2Loc(N_dofs) )
    ALLOCATE( coor(Data%Ndim, N_dofs) )

    ALLOCATE( Vp(SIZE(Var%Vp,1), N_dofs) )

    N_pyr = 0
    nn = Var%Ncells

    DO jt = 1, Mesh%Nelement

       DO k = 1, Mesh%Maill(jt)%Ndegre

          coor(:, Mesh%Maill(jt)%Nu(k)) = Mesh%Maill(jt)%coor(:,k)
            Vp(:, Mesh%Maill(jt)%Nu(k)) = Var%Vp(:, Mesh%Maill(jt)%Nu(k))

!Vp(1:2,  Mesh%Maill(jt)%Nu(k)) = Var%D_Ua(:, 2, Mesh%Maill(jt)%Nu(k))

!!$Vp(1,  Mesh%Maill(jt)%Nu(k)) = abs(DSQRT( SUM(Var%D_Ua(:, 2, Mesh%Maill(jt)%Nu(k))**2) ) - 1.d0)
!!$Vp(3:4,  Mesh%Maill(jt)%Nu(k)) = Var%D_Ua(:, 2, Mesh%Maill(jt)%Nu(k))

       ENDDO       

       ! Fake Hexa nodes for the pyramids
       IF( Data%NDim == 3 .AND. &
           Mesh%Maill(jt)%NSommets == 5 ) THEN

          ALLOCATE( Nv(Mesh%Maill(jt)%Ndegre) )
          Nv = Mesh%Maill(jt)%Nu

          ns = Mesh%Maill(jt)%Ndegre
          
          DEALLOCATE(Mesh%Maill(jt)%Nu)
          ALLOCATE( Mesh%Maill(jt)%Nu(ns+3) )

          Mesh%Maill(jt)%Nu(1:ns) = Nv
          Mesh%Maill(jt)%Nu(ns+1 : ns+3) = (/ (is, is = nn+1, nn+3) /)
          Mesh%Maill(jt)%Ndegre = ns+3

          DO is = 1,3
             coor(:, Mesh%Maill(jt)%Nu(ns+is)) = Mesh%Maill(jt)%coor(:, ns)
               Vp(:, Mesh%Maill(jt)%Nu(ns+is)) = Vp(:, Mesh%Maill(jt)%Nu(ns))
          ENDDO

          nn = nn + 3

          !Mesh%Maill(jt)%Ndegre = (Data%Nordre)**3

          DEALLOCATE(Nv)

       ENDIF

    ENDDO
   

    DO polyType = 1, 2

       IF (NbOfPolys(polyType) > 0) THEN

          SELECT CASE(polyType)
          CASE(1)
             IF (Data%Ndim==2) THEN
                PRINT *, mod_name, " traitement des triangles"
             ELSE
                PRINT *, mod_name, " traitement des tetrahedres"
             END IF
          CASE(2)
             IF (Data%Ndim==2) THEN
                PRINT *, mod_name, " traitement des quadrangles"
             ELSE
                PRINT *, mod_name, " traitement des hexahedres"
             END IF
          CASE DEFAULT
             PRINT *, mod_name, " Element inconnu"
             CALL delegate_stop()
          END SELECT


          ! Compte le nombre de sommets locaux (les sommets des tri/tetra) et les ordonne
          Glob2Loc = 0; ns = 0

          DO jt = 1, Mesh%Nelement

             ! Identifie les elements qui nous interessent
             IF ( Mesh%Maill(jt)%NSommets == Degre(polyType)) THEN

                ! Parcours des sommets de l'element
                DO k = 1,  Mesh%Maill(jt)%NDegre

                   js = Mesh%Maill(jt)%Nu(k)

                   ! Si on ne connait pas le sommet on l'ajoute a notre tableau
                   IF ( Glob2Loc( js ) == 0 ) THEN
                      ns = ns + 1 
                      Glob2Loc( js ) = ns
                   END IF

                END DO

             END IF

          END DO

          VarList = 'ZONE T="P1", F=FEPOINT, N='
          WRITE(nombre, '(I8)') ns
          VarList = TRIM(VarList) // TRIM(ADJUSTL(nombre))
          VarList = TRIM(VarList) // ', E='
          WRITE(nombre, '(I10)') NbOfPolys(polyType)
          VarList = TRIM(VarList) // TRIM(ADJUSTL(nombre))


          ! Ecriture du type d'element
          SELECT CASE(polyType)
          CASE(1)
             SELECT CASE( Data%Ndim )
             CASE(2)
                VarList = TRIM(VarList) // ', ET=TRIANGLE'
             CASE(3)
                VarList = TRIM(VarList) // ', ET=TETRAHEDRON'
             CASE DEFAULT
                PRINT *, mod_name, ' ERREUR : conversion save 2 tecplot non valide pour Ndim=', Data%Ndim
                CALL delegate_stop()
             END SELECT
          CASE(2)
             SELECT CASE( Data%Ndim )
             CASE(2)
                VarList = TRIM(VarList) // ', ET=QUADRILATERAL'
             CASE(3)
                VarList = TRIM(VarList) // ', ET=BRICK'
             CASE DEFAULT
                PRINT *, mod_name, ' ERREUR : conversion save 2 tecplot non valide pour Ndim=', Data%Ndim
                CALL delegate_stop()
             END SELECT
          CASE DEFAULT
             CALL delegate_stop()
          END SELECT

          WRITE(MyUnit, *) TRIM( VarList )

          ns = 0 
          DO is = 1, N_dofs ! ecriture coord des noeuds et valeurs de variables
             IF ( Glob2Loc(is) > 0) THEN
                ns = ns + 1
                Glob2Loc(is) = ns
                WRITE (MyUnit, '( 26(ES16.8) )') Coor(:, is), Vp(:,is)
             END IF
          END DO

          DO jt = 1,  Mesh%Nelement! ecriture connectivites
             IF (Mesh%Maill(jt)%NSommets == Degre(polyType)) THEN

                Nu => Mesh%Maill(jt)%Nu(:)

                nn = SIZE(Nu)

                IF (polyType == 1) THEN

                   SELECT CASE(Nordre)
                   CASE(2) ! 2D Triangles P1; 3D tetra�dres P1
                      WRITE(MyUnit, *) ( Glob2Loc(Nu(k)) , k = 1, nn)

                   CASE(3)
                      SELECT CASE(Data%Ndim)
                      CASE(2)! 2D Triangles P2
                         WRITE(MyUnit, *) Glob2Loc( Nu(1) ), Glob2Loc( Nu(4) ), Glob2Loc( Nu(6) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(2) ), Glob2Loc( Nu(5) ), Glob2Loc( Nu(4) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(3) ), Glob2Loc( Nu(6) ), Glob2Loc( Nu(5) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(6) ), Glob2Loc( Nu(4) ), Glob2Loc( Nu(5) )
                      CASE(3)!  3D tetra�dres P2
                         WRITE(MyUnit, *) Glob2Loc( Nu(1) ), Glob2Loc( Nu(5) ), Glob2Loc( Nu(7) ), Glob2Loc( Nu(8) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(2) ), Glob2Loc( Nu(6) ), Glob2Loc( Nu(5) ), Glob2Loc( Nu(10))
                         WRITE(MyUnit, *) Glob2Loc( Nu(3) ), Glob2Loc( Nu(7) ), Glob2Loc( Nu(6) ), Glob2Loc( Nu(9) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(8) ), Glob2Loc( Nu(10) ), Glob2Loc( Nu(9)), Glob2Loc( Nu(4) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(7) ), Glob2Loc( Nu(8) ), Glob2Loc( Nu(5) ), Glob2Loc( Nu(10) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(7) ), Glob2Loc( Nu(5) ), Glob2Loc( Nu(6) ), Glob2Loc( Nu(10) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(7) ), Glob2Loc( Nu(6) ), Glob2Loc( Nu(9)), Glob2Loc( Nu(10) )
                         WRITE(MyUnit, *) Glob2Loc( Nu(7) ), Glob2Loc( Nu(9)), Glob2Loc( Nu(8) ), Glob2Loc( Nu(10) )
                      END SELECT
                   END SELECT

                ELSE

                   SELECT CASE(Nordre)
                   CASE(2) ! 2D Quadrangles P1; 3D Hexa P1
                      WRITE(MyUnit, '(8(I8,X))') (Glob2Loc(Nu(k)), k = 1, nn)
                   CASE(3)
                      SELECT CASE(Data%Ndim)
                      CASE(2)! 2D Quadrangles P2
                         WRITE(MyUnit,'(4(I8,X))') Glob2Loc( Nu(1) ),Glob2Loc( Nu(5) ),Glob2Loc( Nu(9) ),Glob2Loc( Nu(8) )
                         WRITE(MyUnit,'(4(I8,X))') Glob2Loc( Nu(5) ),Glob2Loc( Nu(2) ),Glob2Loc( Nu(6) ),Glob2Loc( Nu(9) )
                         WRITE(MyUnit,'(4(I8,X))') Glob2Loc( Nu(9) ),Glob2Loc( Nu(6) ),Glob2Loc( Nu(3) ),Glob2Loc( Nu(7) )
                         WRITE(MyUnit,'(4(I8,X))') Glob2Loc( Nu(8) ),Glob2Loc( Nu(9) ),Glob2Loc( Nu(7) ),Glob2Loc( Nu(4) )
                      CASE(3)!  3D Hexa P2
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 1 , 9 , 21, 10, 11, 22, 27, 23 /) ))
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 11, 22, 27, 23, 5 , 17, 26, 18 /) ))
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 22, 13, 24, 27, 17, 6 , 19, 26 /) ))
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 9 , 2 , 12, 21, 22, 13, 24, 27 /) ))
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 10, 21, 14, 4 , 23, 27, 25, 16 /) ))
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 23, 27, 25, 16, 18, 26, 20, 8  /) ))
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 27, 24, 15, 25, 26, 19, 7 , 20 /) ))
                         WRITE(MyUnit, '(8(I8,X))') Glob2Loc( Nu( (/ 21, 12, 3 , 14, 27, 24, 15, 25 /) ))
                      END SELECT
                   END SELECT

                END IF
             END IF
          END DO
       END IF
    END DO

    DEALLOCATE(Glob2Loc, coor)

    CLOSE(MyUnit)

  END SUBROUTINE VisuTecplotAscii
  !==============================

  !=========================================
  SUBROUTINE VisuCx(DATA, Mesh, Var, PbName)
  !=========================================

    IMPLICIT NONE
    
    CHARACTER(LEN = *), PARAMETER   :: mod_name = "VisuCp"

    TYPE(Donnees),       INTENT(IN) :: DATA
    TYPE(Maillage),      INTENT(IN) :: Mesh
    TYPE(Variables),     INTENT(IN) :: Var
    CHARACTER(LEN = 70), INTENT(IN) :: PbName
    !-----------------------------------------

    CHARACTER(LEN=75) :: file
    INTEGER :: MyUnit1, MyUnit2, istat
    INTEGER :: ilog, ifr, i_wall, k
    !-----------------------------------------

    MyUnit1 = 46
    MyUnit2 = 47

    file(1:)= "Cp_"//TRIM(Adjustl(PbName))//".plt"

    OPEN( UNIT=MyUnit1, FILE=TRIM(file), IOSTAT=istat , STATUS="REPLACE")

    IF (istat /= 0) THEN
       
       PRINT *, "Calcul_Cp, ERREUR : ouverture de ", Trim(file), " -> ", istat       
       CALL Abort()
       
    ENDIF

    WRITE(MyUnit1, 100)
    WRITE(MyUnit1, 110)

    i_wall = 0

    ! Write Cp for wall points
    DO ifr = 1, Mesh%NfacFr

       ilog  = Mesh%fr(ifr)%inum

       IF (ilog == lgc_paroi_adh_adiab_flux_nul .OR. &
         & ilog == lgc_paroi_glissante ) THEN

          DO k = 1, SIZE(Mesh%fr(ifr)%Nu)

             i_wall = i_wall + 1

             WRITE(MyUnit1, 115) Mesh%fr(ifr)%coor(:, k), Var%Cp(i_wall), Var%Cf(i_wall)

          ENDDO

       ENDIF    
       
    ENDDO

    CLOSE(MyUnit1)

    WRITE(*,*)
    WRITE(*,*) 'Aerodynamic coefficients. Unit reference lenght/surface.'
    WRITE(*,*)
    WRITE(*,'("   CL   = " E13.6)') Var%CL
    WRITE(*,'("   CD_P = " E13.6)') Var%CD_P
    WRITE(*,'("   CD_V = " E13.6)') Var%CD_V
    WRITE(*,*)

100 FORMAT('TITLE = "Cp and Cf on the wall"')
110 FORMAT('VARIABLES = "X", "Y", "Cp", "Cf"')
115 FORMAT(4(1X, F24.10))

  END SUBROUTINE VisuCx
  !====================

  !===========================================
  SUBROUTINE VisuGmsh(Data, Mesh, Var, PbName)
  !===========================================

    IMPLICIT NONE

    TYPE(Donnees),       INTENT(IN)    :: Data
    TYPE(Maillage),      INTENT(INOUT) :: Mesh
    TYPE(Variables),     INTENT(IN)    :: Var
    CHARACTER(LEN = 70), INTENT(IN)    :: PbName
    !----------------------------------------------

    CHARACTER(LEN = 75)  :: Ascfile, temp, temp_
    !----------------------------------------------

    REAL   , DIMENSION(:,:), POINTER :: Vp
    REAL   , DIMENSION(:,:), POINTER :: Coor
    INTEGER, DIMENSION(:)  , POINTER :: Nu, Glob2Loc
    !----------------------------------------------

    INTEGER ::  N_dofs, N_ele, bc, e_type

    INTEGER :: MyUnit, jt, k, is, ns, js, istat
    !----------------------------------------------

    MyUnit = 97

    Ascfile = " "
    Ascfile(1: ) = TRIM(ADJUSTL(PbName)) // "-P.msh"

    OPEN( UNIT = MyUnit, FILE = TRIM(Ascfile), IOSTAT = istat, STATUS = "REPLACE" ) 
    IF (istat /= 0) THEN
       WRITE(*,*) 'ERROR file output gmsh'
       CALL delegate_stop()
    END IF
    REWIND(MyUnit)   

    WRITE(MyUnit, '("$MeshFormat")')
    WRITE(MyUnit, '("2.0 0 8")')
    WRITE(MyUnit, '("$EndMeshFormat")')
    WRITE(MyUnit, '("$Nodes")')

    N_dofs = Var%Ncells
    temp = " "; WRITE(temp, '(I10)') N_dofs
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp))
    
    ALLOCATE(        Glob2Loc(N_dofs) )
    ALLOCATE( coor(Data%Ndim, N_dofs) )

    ALLOCATE( Vp(SIZE(Var%Vp,1), N_dofs) )
  
    DO jt = 1, Mesh%Nelement

       DO k = 1, Mesh%Maill(jt)%Ndegre

          coor(:, Mesh%Maill(jt)%Nu(k)) = Mesh%Maill(jt)%coor(:,k)
            Vp(:, Mesh%Maill(jt)%Nu(k)) = Var%Vp(:, Mesh%Maill(jt)%Nu(k))

       ENDDO       

    ENDDO
   
    Glob2Loc = 0; ns = 0

    DO jt = 1, Mesh%Nelement

       DO k = 1, Mesh%Maill(jt)%NDegre

          js = Mesh%Maill(jt)%Nu(k)

          IF ( Glob2Loc( js ) == 0 ) THEN
             ns = ns + 1 
             Glob2Loc( js ) = ns
          ENDIF
          
       ENDDO

    ENDDO

    ns = 0 
    DO is = 1, N_dofs
          
       IF ( Glob2Loc(is) > 0) THEN

          ns = ns + 1;  Glob2Loc(is) = ns

          temp = " "; WRITE(temp, '(I12)') ns
          DO k = 1, Data%NDim
             temp_ = " "; WRITE(temp_, '(E24.16)') Coor(k, is)
             temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
          ENDDO
          IF( Data%Ndim == 2) THEN
             temp_ = " "; WRITE(temp_, '(F10.5)') 0.d0
             temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
          ENDIF
          WRITE(MyUnit, *) TRIM(ADJUSTL(temp))

       ENDIF

    ENDDO

    WRITE(MyUnit, '("$EndNodes")')
    WRITE(MyUnit, '("$Elements")')

    temp = " "; WRITE(temp, '(I10)') Mesh%Nelement + Mesh%NfacFr
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp))

    DO jt = 1, Mesh%Nelement
       
       temp = " "; WRITE(temp, '(I12)') jt

       SELECT CASE(Data%Ndim)
       CASE(2)
             
          SELECT CASE(Mesh%Maill(jt)%Ndegre)

          CASE(3) ! Triangle P1
             e_type = 2
          CASE(4) ! Quadrangle P1
             e_type = 3
          CASE(6) ! Triangle P2
             e_type = 9
          CASE(9) ! Quadrangle P2
             e_type = 10
          CASE DEFAULT
             WRITE(*,*) 'ERROR: unknown 2D domain element'
             STOP
          END SELECT

       CASE(3)
          
          SELECT CASE(Mesh%Maill(jt)%Ndegre)
          CASE(4) ! Tetrahedron P1
             e_type = 4
          CASE(8) ! Hexahedron P1
             e_type = 5
          CASE(10) ! Tetrahedon P2
             e_type = 11
          CASE(27) ! Hexahedron P2
             e_type = 12
          CASE DEFAULT
             WRITE(*,*) 'ERROR: unknown 3D domain element'
             STOP
          END SELECT

       END SELECT

       temp_ = " "; WRITE(temp_, '(I4)') e_type
       temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
       
       temp_ = " "; WRITE(temp_, '(3I3)') 2, 0, 0
       temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
          
       DO k = 1, COUNT(Mesh%Maill(jt)%Nu /= 0)
          temp_ = " "; WRITE(temp_, '(I12)') Glob2Loc( Mesh%Maill(jt)%Nu(k) )
          temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
       ENDDO

       WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 

    ENDDO

    DO jt = 1, Mesh%NfacFr

       temp = " "; WRITE(temp, '(I12)') jt + Mesh%Nelement

       SELECT CASE(Data%Ndim)
       CASE(2)

          SELECT CASE(Mesh%fr(jt)%Ndegre)

          CASE(2) ! Triangles P1
             e_type = 1
          CASE(3) ! Quadrangles P1
             e_type = 8
          CASE DEFAULT
             WRITE(*,*) 'ERROR: unknown 2D boundary element'
             STOP
          END SELECT

       CASE(3)
          
          SELECT CASE(Mesh%fr(jt)%Ndegre)
          CASE(3) ! Triangle P1
             e_type = 2
          CASE(4) ! Quadrangle P1
             e_type = 3
          CASE(6) ! Triangle P2
             e_type = 9
          CASE(9) ! Quadrangle P2
             e_type = 10
          CASE DEFAULT
             WRITE(*,*) 'ERROR: unknown 3D boundary element'
             STOP
          END SELECT
          
       END SELECT
       
       temp_ = " "; WRITE(temp_, '(I4)') e_type
       temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
       
       bc = Mesh%fr(jt)%inum
       
       temp_ = " "; WRITE(temp_, '(I3)') 2
       temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
       
       temp_ = " "; WRITE(temp_, '(I6)') bc
       temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
       
       temp_ = " "; WRITE(temp_, '(I6)') bc
       temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))          
       
       DO k = 1, COUNT(Mesh%fr(jt)%Nu /= 0)
          temp_ = " "; WRITE(temp_, '(I12)') Glob2Loc( Mesh%fr(jt)%Nu(k) )
          temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
       ENDDO

       WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 
       
    ENDDO
    
    WRITE(MyUnit, '("$EndElements")')
    
    WRITE(MyUnit, '("$NodeData")')

    temp = " "; WRITE(temp, '(I4)') 1
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 

    temp = " "; temp = '"rho"'    
    WRITE(MyUnit, *) temp

    temp = " "; WRITE(temp, '(I4)') 1
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 
    
    temp = " "; WRITE(temp, '(F24.16)') 0.0
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 

    temp = " "; WRITE(temp, '(I4)') 3
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 

    temp = " "; WRITE(temp, '(I4)') 0
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 

    temp = " "; WRITE(temp, '(I4)') 1
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 

    temp = " "; WRITE(temp, '(I12)') N_dofs
    WRITE(MyUnit, *) TRIM(ADJUSTL(temp)) 

    DO is = 1, N_dofs

       temp = " "; WRITE(temp, '(I12)') is
       temp_ = " "; WRITE(temp_, '(E24.16)') Vp(1, is)
       temp = TRIM(ADJUSTL(temp)) // " " // TRIM(ADJUSTL(temp_))
       
       WRITE(MyUnit, *) temp

    ENDDO

    WRITE(MyUnit, '("$EndNodeData")')

    CLOSE(MyUnit)

  END SUBROUTINE VisuGmsh
  !======================
  

END MODULE Visu


