!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                          */
!*                                                                           */
!*                  2000-2008 Institut de Mathématiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Université Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MODULE fluidbox

  USE LesTypes
  USE LibComm
  USE Outputs
  USE Inputs
  USE ReadMesh
  USE GeomGraph
  USE GeomParam
  USE Initialisation
  USE BoucleEnTemps
  USE ModelInterfaces
  USE Elements_driver
  
  IMPLICIT NONE

CONTAINS

  !===========================
  SUBROUTINE FBxMain(rootname)
  !===========================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "FBxMain"

    CHARACTER(LEN = *), INTENT(IN)  :: rootname

    INTEGER :: Unit_Estimation, ii, ios

    INTEGER,         PARAMETER :: dp = SELECTED_REAL_KIND(14)
    REAL(KIND = dp), PARAMETER :: xx = 1.0_dp

    REAL :: Temps_debut, Temps_final,   &
            Temps_total, Temps_elapsed, &
            temps_local, Temps_init,    &
            Temps_start, Temps_boucle

    INTEGER :: nb_periodes_initial, &
               nb_periodes_final,   &
               nb_periodes, t0, t1
    !--------------------------------------------

    TYPE(MeshCom)   :: Com
    TYPE(MeshDef)   :: Mesh_init
    TYPE(Variables) :: Var
    !--------------------------------------------

    !------------------
    ! Init the program
    !---------------------------------------------
    CALL My_Timer(nb_periodes_initial)
    CALL CPU_TIME(Temps_debut)

    Data%rootname = TRIM(rootname)

    CALL IniCom(Com)
    WRITE(*,*) mod_name, ' ME/1 = ', Com%Me, Com%NTasks

    IF (Com%Me == 0) THEN
       CALL System("hostname")
       CALL info_ifdef()
    END IF

    CALL alloc_set()

#ifdef TIMES
    Var%Tfilltot = 0.0
    Var%Tsolvetot = 0.0
#endif

    IF (com%me == 0) THEN   
       OPEN(UNIT=noregfile,FILE="noreg.xml", FORM = 'formatted')
    END IF

    Var%TotalSolveTime = 0

    CALL AffichageDeDebut(Com)
    !-------------------------------------------------

    !-------------
    ! Get the Data
    !---------------------------------------------
    CALL DonNumMeth(Com, Data)

    CALL DonMesh(Com, Mesh_Init, Data)

    CALL Model_Get_Data(Data, Mesh_init)

    CALL My_timer(t0)

    IF (com%me == 0) THEN   
       WRITE(noregfile,*) '<time step="read_mesh">',t0,"</time>"
    END IF

    IF (Data%Ndim /= Mesh_init%Ndim ) THEN

       WRITE(*,*) 'ERROR: Mis-match in spatial dimensions'
       WRITE(*,*) 'Data N_dim = ', Data%Ndim
       WRITE(*,*) 'Mesh N_dim = ', Mesh_init%Ndim 

#ifndef SEQUENTIEL
       CALL EndCom(Com)
#endif

       CALL delegate_stop()

    END IF

    CALL Model_Get_bc( DATA, Mesh_init )
    
    CALL AffichageDeDebut2(Com)
    !---------------------------------------------

    !---------
    ! Topology
    !---------------------------------------------
    SELECT CASE(data%sel_approche)

    CASE(cs_approche_vertexcentered)

       CALL InitVertexCentered(Com, Mesh_init, Var, Mat_fb)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: unknown approach ', TRIM(data%approche)
       CALL delegate_stop()

    END SELECT

    CALL My_timer(t1)
    IF (com%me == 0) THEN   
       WRITE(noregfile,*) '<time step="build_graph">',t1-t0,"</time>"
    END IF
    t0 = t1

    !------------------------
    ! Variable initialization
    !---------------------------------------------
    CALL Model_Alloc_Var(DATA, Mesh_init, Var)

    DO ii = 1, Var%Ncells, 2
       Var%Vp(:, ii) = HUGE(xx)
    END DO
    DO ii = 2, Var%Ncells, 2
       Var%Vp(:, ii) = TINY(xx)
    END DO

    IF (Data%Impre > 1) THEN
       PRINT *, mod_name, " Var%Ncells==", Var%Ncells
       PRINT *, mod_name, " ncoefmat==", Mat_fb%Ncoefmat
    END IF

    CALL InitFields(Com, DATA, Var)

    CALL Ecriture(DATA, Var, Com)

    IF (data%Impre > 1) THEN
       PRINT *, mod_name, Com%Me," ncoefmat==", Mat_fb%Ncoefmat
    END IF

    CALL My_timer(t1)
    IF (com%me == 0) THEN   
       WRITE(noregfile,*) '<time step="init">',t1-t0,"</time>"
    END IF
    t0 = t1

    CALL CPU_TIME(temps_start)
    temps_init = temps_start - Temps_debut
    !---------------------------------------------

    !-----------
    ! Time Loop
    !---------------------------------------------
    CALL en_minuscules(Data%methode, Data%methode)

    SELECT CASE(data%methode(1:3)) 
    
    CASE("imp")

!!$       IF( Data%Sel_Relax /= cs_relax_nl_lusgs ) THEN

          CALL boucle_en_temps(Com, Var, Mat_fb)

!!$       ELSE
!!$
!!$          CALL boucle_en_temps(Com, Var)          
!!$
!!$       ENDIF

    CASE("exp")
       
       CALL boucle_en_temps(Com, Var)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: unknow time method type'
       STOP

    END SELECT    
    !---------------------------------------------

    CALL CPU_TIME(temps_boucle)

    temps_boucle = temps_boucle - temps_start

    CALL CPU_TIME(Temps_final)
    CALL My_Timer(nb_periodes_final)

    nb_periodes = nb_periodes_final - nb_periodes_initial

    Temps_elapsed = nb_periodes * 0.001
    Temps_total   = Temps_final - Temps_debut
    Temps_local   = Temps_total

#ifndef SEQUENTIEL
    CALL  WaitGroup(Com)

#ifdef TIMES
    CALL Reduce(Com, cs_parall_sum, temps_total)
    CALL Reduce(Com, cs_parall_sum, Com%techgsolu)
    CALL Reduce(Com, cs_parall_sum, Com%twait)
    CALL Reduce(Com, cs_parall_sum, Com%treduce)
    IF (Com%Me == 0) THEN
       WRITE(6, 502) "Temps comm:      ", Com%techgsolu, "s"
       WRITE(6, 502) "Temps wait:      ", Com%twait, "s"
       WRITE(6, 502) "Temps red :      ", Com%treduce, "s"
       WRITE(6, 502) "Temps MPI :      ", Com%techgsolu + Com%twait + Com%treduce, "s"
    END IF
#endif

    CALL EndCom(Com) !-- mpi_finalize

#endif

#ifdef MORETIMES
    WRITE(6, 502) "Temps Fill:      ", Var%Tfilltot, "s"
    WRITE(6, 502) "Temps Solv:      ", Var%Tsolvetot, "s"
#endif

    WRITE(6, 501) "Temps init:      ", temps_init, "s"
    WRITE(6, 501) "Temps boucle:    ", temps_boucle, "s"
    WRITE(6, 501) "Temps simu:      ", temps_local, "s"
    IF (see_alloc) THEN
       CALL alloc_etat()
    END IF


    IF (Com%Me == 0) THEN
       WRITE(6, 501) "Temps total:     ", temps_total, "s"
       WRITE(6, 501) "Temps elapse:     ", temps_elapsed, "s"
       CALL log_replay()
       CALL AffichageDeFin(Com)
       PRINT *, mod_name, " Fin de l'execution"
    END IF

501 FORMAT( A35, 1X, F12.3, 1X, A1 ) 
502 FORMAT( A35, 1X, ES12.6, 1X, A1 ) 

    CALL My_timer(t1)  
    IF (com%me == 0) THEN   
       WRITE(noregfile,*) '<time step="solve">',Var%TotalSolveTime,"</time>"
       WRITE(noregfile,*) '<time step="main_loop">',t1-t0,"</time>"
       WRITE(noregfile,*) '<time step="build">',t1-t0-Var%TotalSolveTime,"</time>"
       WRITE(noregfile,*) '<time step="overall">',t1,"</time>"
       WRITE(noregfile,*) '<iter>', Var%kt, "</iter>"
       CLOSE(noregfile)
    END IF

  END SUBROUTINE FBxMain
  !=====================

  !=========================================================
  SUBROUTINE InitVertexCentered(Com, Mesh_init, Var, Mat_fb)
  !=========================================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "InitVertexCentered"

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(MeshDef),   INTENT(INOUT) :: Mesh_init
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Mat_fb
    !------------------------------------------

    Mesh_init%NCells = Mesh_init%Npoint

    SELECT CASE(data%sel_geotype)

    !----------------------
    CASE(cs_geotype_2dplan)
    !----------------------

       CALL GeomSegments2d(Mesh_init, Data%Impre)

       CALL MeshOrderUpdate(Mesh_init, Data)
       !------------------------------------

       Var%Ncells = Mesh_init%Npoint !?

       ! P. JACQ
#if (defined ORDRE_ELEVE && defined PARALLEL)

       IF (Data%Nordre == 3 .AND. Com%NTasks > 1) THEN

          CALL ReadSegCom(Com , Mesh_init, data%PbName)

       END IF

#endif

    !------------------
    CASE(cs_geotype_3d)
    !------------------

       CALL GeomSegments3d(DATA, Mesh_init)

       IF (Data%Impre > 0) THEN
          PRINT *, mod_name, "AVANT GeomFacettes3d"
       END IF

       CALL GeomFace3D(Mesh_init)

       CALL MeshOrderUpdate(Mesh_init, DATA)
       !------------------------------------

       Var%Ncells = Mesh_init%Npoint !?

       IF (Data%Impre > 0) THEN
          PRINT *, mod_name, "AVANT GrapheDesVois"
       END IF

       ! On construit une partie du graphe
       CALL GrapheDesVois(DATA, Mesh_init)
      
       ! P. JACQ
#if (defined ORDRE_ELEVE && defined PARALLEL)

       IF (Data%Nordre == 3 .AND. Com%NTasks > 1) THEN

          CALL ReadSegCom(Com , Mesh_init, data%PbName)

       END IF

#endif

    END SELECT

    Var%Ncells    = Mesh_init%NCells
    Var%NCellsIn  = Var%Ncells
    Var%NCellsSnd = 0

    CALL NodeTOele_connect(Mesh_init)

#ifndef SEQUENTIEL

    CALL EchangeInit(Com, DATA)

    Var%NCellsIn = Var%Ncells - Com%NPrcv2tot
    Var%NCellsSnd = Com%NPsnd2tot

    CALL Find_Wall_Overlapas(Com, Mesh_init)

    IF (data%Impre > 1) THEN
       PRINT *, mod_name, " <>apres EchangeVec"
    END IF

#endif

    !----------------------------------
    CALL Init_Elements(Data, Mesh_init)
    !----------------------------------


!!$    IF (Data%methode(1:3) == "IMP" .AND.    &
!!$        Data%Sel_Relax /= cs_relax_nl_lusgs ) THEN

    IF (Data%methode(1:3) == "IMP" ) THEN

       CALL GraphMatVertexCentered(DATA, Mesh_init, Mat_fb)
       CALL GraphMatAddDiag(DATA, Mesh_init, Mat_fb)

       Mat_fb%NNin    = Var%NCellsIn
       Mat_fb%NNsnd   = Var%NCellsSnd
       Mat_fb%blksize = Data%Nvar

       CALL GraphMatAllocTab(Mat_fb%blksize, Mat_fb)

       CALL initcoeftab(Mat_fb)

       IF (data%sel_Relax == cs_relax_gmresl_ilu  .OR. &
           data%sel_Relax == cs_relax_gmresr_ilu  .OR. &
           data%sel_Relax == cs_relax_gmresl_ssor .OR. &
           data%sel_Relax == cs_relax_gmresr_ssor .OR. &
           data%sel_Relax == cs_relax_bicgsl_ilu  .OR. &
           data%sel_Relax == cs_relax_mf_ilu) THEN

          ALLOCATE( Mat_fb%iluvals(SIZE(Mat_fb%Vals, 1), &
                                   SIZE(Mat_fb%Vals, 1), &
                                        Mat_fb%Ncoefmat) )
          IF (see_alloc) THEN
             CALL alloc_allocate(mod_name, "mat_fb%iluvals", &
                                 SIZE(Mat_fb%iluvals))
          END IF

       ENDIF
       
    END IF
      
    DEALLOCATE( Mesh_init%Coor, &
                Mesh_init%Nu    )

    IF(data%sel_geotype == cs_geotype_2dplan) THEN

       DEALLOCATE( Mesh_init%Nuseg,  &
                   Mesh_init%Nutv     )

    ELSEIF(data%sel_geotype == cs_geotype_3d) THEN
       
       DEALLOCATE( Mesh_init%NuFacette, &
                   Mesh_init%EleVois    )

    ENDIF  
    
    IF( Mesh_init%NFacFr > 0 ) THEN
       DEALLOCATE( Mesh_init%NsFacFr, &
                   Mesh_init%Logfr)
    ENDIF

  END SUBROUTINE InitVertexCentered
  !================================

END MODULE fluidbox

