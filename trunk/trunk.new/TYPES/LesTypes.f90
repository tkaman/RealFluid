MODULE types_enumeres

#ifndef SEQUENTIEL
#ifdef MPICH2
      USE mpi
#endif
#endif

      IMPLICIT NONE

      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_amdba = 1000
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_amdba4 = 1001
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_mesh = 1002
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_meshemc2 = 1003
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_meshemc2a = 1004
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_meshemc2p = 1005
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_geom3d = 1006
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_gmsh = 1007
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_gmsh2  = 1010
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_gmsh3d = 1011
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_murge   = 1012
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_meshemc2p2p2 = 1013
      INTEGER, PARAMETER, PUBLIC :: cs_meshtype_gmsh_hexa = 1014

      INTEGER, PARAMETER, PUBLIC :: cs_loith_constant = 2000
      INTEGER, PARAMETER, PUBLIC :: cs_loith_prandtl = 2001
      INTEGER, PARAMETER, PUBLIC :: cs_loith_no = 2002
      INTEGER, PARAMETER, PUBLIC :: cs_loith_chungdilute = 2003
      INTEGER, PARAMETER, PUBLIC :: cs_loith_chungdense = 2004
      
      INTEGER, PARAMETER, PUBLIC :: cs_RGrad_ex    = 0 !*
      INTEGER, PARAMETER, PUBLIC :: cs_RGrad_GG    = 1 !*
      INTEGER, PARAMETER, PUBLIC :: cs_RGrad_GG_b  = 2 !*
      INTEGER, PARAMETER, PUBLIC :: cs_RGrad_LSQ   = 3 !*
      INTEGER, PARAMETER, PUBLIC :: cs_RGrad_SPRZZ = 4 !*
      INTEGER, PARAMETER, PUBLIC :: cs_RGrad_SPRZN = 5 !*

      INTEGER, PARAMETER, PUBLIC :: cs_dist_scheman = 1
      INTEGER, PARAMETER, PUBLIC :: cs_dist_laxf = 2
      INTEGER, PARAMETER, PUBLIC :: cs_dist_godunov = 3

      INTEGER, PARAMETER, PUBLIC :: cs_icas_uniforme = 0
      INTEGER, PARAMETER, PUBLIC :: cs_icas_ringleb = 10
      INTEGER, PARAMETER, PUBLIC :: cs_icas_MS      = 20
      INTEGER, PARAMETER, PUBLIC :: cs_icas_supvort = 30
      INTEGER, PARAMETER, PUBLIC :: cs_icas_test    = 100
      INTEGER, PARAMETER, PUBLIC :: cs_icas_4etats  = 31
      INTEGER, PARAMETER, PUBLIC :: cs_icas_dmr     = 40
      INTEGER, PARAMETER, PUBLIC :: cs_icas_vortex  = 50


      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk1 = 5000
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk1_fake = 5001
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk2 = 5002
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk2_fake = 5003
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk3_fake = 5004
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk4_fake = 5005
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk3 = 5006
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk4 = 5007
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_pc2 = 5008
      INTEGER, PARAMETER, PUBLIC :: cs_methode_imp_rk1 = 5009
      INTEGER, PARAMETER, PUBLIC :: cs_methode_imp_rk1_rd = 5010
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk_Mario_rd = 6000
      INTEGER, PARAMETER, PUBLIC :: cs_methode_imp_exp_rd = 5011
      INTEGER, PARAMETER, PUBLIC :: cs_methode_implin_rk1 = 5012
      INTEGER, PARAMETER, PUBLIC :: cs_methode_implin_gear = 5013
      INTEGER, PARAMETER, PUBLIC :: cs_methode_implin_gear_rd = 5014
      INTEGER, PARAMETER, PUBLIC :: cs_methode_imp_gear = 5015
      INTEGER, PARAMETER, PUBLIC :: cs_methode_imp_dce = 5016
      INTEGER, PARAMETER, PUBLIC :: cs_methode_imp_gear_rd = 5017
      INTEGER, PARAMETER, PUBLIC :: cs_methode_imp_dce_rd = 5018
      INTEGER, PARAMETER, PUBLIC :: cs_methode_implin_rk1_rd = 5019
      INTEGER, PARAMETER, PUBLIC :: cs_methode_exp_rk1_rd = 5020

      INTEGER, PARAMETER, PUBLIC :: cs_nordre_1 = 1
      INTEGER, PARAMETER, PUBLIC :: cs_nordre_2 = 2
      INTEGER, PARAMETER, PUBLIC :: cs_nordre_3 = 3
      INTEGER, PARAMETER, PUBLIC :: cs_nordre_4 = 4
      INTEGER, PARAMETER, PUBLIC :: cs_nordre_5 = 5

!-- un document commente pour la version "279f" ce qui est fait pour chaque lgc dans chaque mod�le : $RFB/MAN/TEX/lgc.tex (.pdf)

!!$      INTEGER, PARAMETER, PUBLIC :: lgc_far_field              = 1    ! rho, vel, P, + turb |_oo
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_subsonic_inflow        = 10   ! rho, vel, P, + turb |_in
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_supersonic_inflow      = 11   ! rho, vel, P, + turb |_in
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_subsonic_outflow       = 20   ! P |_out
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_supersonic_outflow     = 21   ! -
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_slip_wall              = 30   ! -
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_symmetry_plane         = 31   ! -
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_noslip_adiabatic_wall  = 40   ! vel    |_wall
!!$      INTEGER, PARAMETER, PUBLIC :: lgc_noslip_isothermal_wall = 41   ! vel, T |_wall

      INTEGER, PARAMETER, PUBLIC :: lgc_dirichlet = 1 !-- Rho, Vitesse, Pression lues | Vitesse, Pression et Temp�rature Lues (InComp)
      INTEGER, PARAMETER, PUBLIC :: lgc_neumann = 2 !-- Rho, Vitesses, Pression lues (code non employ� dans la version courante %%)
      INTEGER, PARAMETER, PUBLIC :: lgc_entree_sa = 10 !-- entree turbulence SA
      INTEGER, PARAMETER, PUBLIC :: lgc_sortie_sa = 11 !-- entree turbulence SA
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_adh_adiab = 19 !-- vitesse Nulle
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_adh_adiab_flux_nul = 20 !-- Vitesse nulle (InComp) et Flux nul
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_glissante = 21 !-- aucune lecture
      INTEGER, PARAMETER, PUBLIC :: lgc_symetrie_plane = 22 !-- aucune lecture
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_lagrange = 23 !-- aucune lecture
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_misc = 25 !-- soit T et V impos�e (OrdreEleve)
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_pression_donnee = 26 !-- Pression lue
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_temp_donnee = 27 !-- Temp�rature lue
      INTEGER, PARAMETER, PUBLIC :: lgc_symetrie_lagrange = 30 !-- Normale au plan lue
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_v_et_p_donnes = 35 !-- Vitesse et Pression lues
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_v_et_t_donnes = 36 !-- Vitesse et Temp�rature lues
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_p_et_t_donnes = 37 !-- Pression et Temp�rature lues
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_diric_vpt_donnes = 38 !-- Vitesse, Pression, Temp�rature Lues (%%%% quelle diff�rence avec dirichlet)
      INTEGER, PARAMETER, PUBLIC :: lgc_gradient_nul_exterieur = 40 !-- Rho, Vitesses, Pression lues
      INTEGER, PARAMETER, PUBLIC :: lgc_paroi_conducteur_parfait = 41 !-- Vitesse nulle, flux de champ magn�tique nul -> un = bn = 0
      INTEGER, PARAMETER, PUBLIC :: lgc_gradient_nul_interieur = 50 !-- aucune lecture
      INTEGER, PARAMETER, PUBLIC :: lgc_sortie_subsonique = 51 !-- Rho, Vitesses, Pression lues
      INTEGER, PARAMETER, PUBLIC :: lgc_sortie_mixte = 52 !-- Rho, Vitesses, Pression lues
      INTEGER, PARAMETER, PUBLIC :: lgc_special_steger_warming = 53 !-- aucune lecture
      INTEGER, PARAMETER, PUBLIC :: lgc_flux_interne = 54 !-- Rho, Vitesses, Pression lues
      INTEGER, PARAMETER, PUBLIC :: lgc_sortie_vitesse_normale = 60 !-- %%%% Ce qui est lu est � v�rifier
      INTEGER, PARAMETER, PUBLIC :: lgc_sortie_pression_donnee = 61 !-- Pression lue
      INTEGER, PARAMETER, PUBLIC :: lgc_flux_de_steger = 64 !-- Rho, Vitesses, Pression lues
      INTEGER, PARAMETER, PUBLIC :: lgc_incomp_v_donne = 75 !-- Vitesse Lue ET profil impos�
      INTEGER, PARAMETER, PUBLIC :: lgc_periodique = 90 !-- aucune lecture
      INTEGER, PARAMETER, PUBLIC :: lgc_entree_turbine_subsonique = 91 !-- Pression totale infinie, densité totale infinie, angle
      INTEGER, PARAMETER, PUBLIC :: lgc_sortie_turbine_subsonique = 92 !-- rapport de pression : Pression entrée / Pression sortie
      INTEGER, PARAMETER, PUBLIC :: lgc_subsonic_inflow_Pt = 95 !-- Total Pressure, Total Temperature, angle of the velocity
      INTEGER, PARAMETER, PUBLIC :: lgc_ns_steger_warming = 154 !-- Rho, Vitesses, Pression


      INTEGER, PARAMETER, PUBLIC :: cs_loicfl_clfmax = 0
      INTEGER, PARAMETER, PUBLIC :: cs_loicfl_minmult = 2
      INTEGER, PARAMETER, PUBLIC :: cs_loicfl_rapport_res = 3

      INTEGER, PARAMETER, PUBLIC :: cs_loivisq_constant = 9000
      INTEGER, PARAMETER, PUBLIC :: cs_loivisq_sutherland = 9001
      INTEGER, PARAMETER, PUBLIC :: cs_loivisq_pant = 9002
      INTEGER, PARAMETER, PUBLIC :: cs_loivisq_no = 9003
      INTEGER, PARAMETER, PUBLIC :: cs_loivisq_chungdilute = 9004
      INTEGER, PARAMETER, PUBLIC :: cs_loivisq_chungdense = 9005

      INTEGER, PARAMETER, PUBLIC :: cs_iflux_1 = 1
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_2 = 2
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_3 = 3
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_4 = 4

      INTEGER, PARAMETER, PUBLIC :: cs_iflux_5 = 5
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_6 = 6
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_7 = 7
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_8 = 8
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_9 = 9
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_10 = 10
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_11 = 11
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_12 = 12
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_13 = 13
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_14 = 14
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_15 = 15
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_16 = 16
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_20 = 20
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_21 = 21
      INTEGER, PARAMETER, PUBLIC :: cs_iflux_22 = 22

      INTEGER, PARAMETER, PUBLIC :: cs_imeth_1 = 1
      INTEGER, PARAMETER, PUBLIC :: cs_imeth_2 = 2
      INTEGER, PARAMETER, PUBLIC :: cs_imeth_3 = 3
      INTEGER, PARAMETER, PUBLIC :: cs_imeth_4 = 4
      INTEGER, PARAMETER, PUBLIC :: cs_imeth_5 = 5
      INTEGER, PARAMETER, PUBLIC :: cs_imeth_6 = 6

      INTEGER, PARAMETER, PUBLIC :: cs_modele_euler = 11000
      INTEGER, PARAMETER, PUBLIC :: cs_modele_eulerns = 11001 !-- "NAVIER-STOKES", "NAVIERSTOKES" aussi
      INTEGER, PARAMETER, PUBLIC :: cs_modele_incomptherm = 11002
      INTEGER, PARAMETER, PUBLIC :: cs_modele_incomp = 11003 !-- "INCOMPNS" aussi
      INTEGER, PARAMETER, PUBLIC :: cs_modele_eulermf = 11004
      INTEGER, PARAMETER, PUBLIC :: cs_modele_eulermfv = 11005 !-- %%%% qu'est ce ?
      INTEGER, PARAMETER, PUBLIC :: cs_modele_idealmhd = 11006
      INTEGER, PARAMETER, PUBLIC :: cs_modele_fullmhd  = 11007

      INTEGER, PARAMETER, PUBLIC :: cs_loietat_userdefined = 12000
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_ig = 12001
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_gp = 12002
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_sg = 12003
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_sgref = 12004
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_sgmf = 12005
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_eau = 12006
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_mhd_gp = 12007
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_vdw = 12008
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_prsv = 12009
      INTEGER, PARAMETER, PUBLIC :: cs_loietat_sw = 120010


      INTEGER, PARAMETER, PUBLIC :: cs_geotype_1d = 13000
      INTEGER, PARAMETER, PUBLIC :: cs_geotype_2dplan = 13001
      INTEGER, PARAMETER, PUBLIC :: cs_geotype_2dper = 13002
      INTEGER, PARAMETER, PUBLIC :: cs_geotype_2daxisym = 13003
      INTEGER, PARAMETER, PUBLIC :: cs_geotype_2daxisymx = 13004
      INTEGER, PARAMETER, PUBLIC :: cs_geotype_2daxisymy = 13005
      INTEGER, PARAMETER, PUBLIC :: cs_geotype_3d = 13006

      INTEGER, PARAMETER, PUBLIC :: cs_approche_vertexcentered = 14000
      INTEGER, PARAMETER, PUBLIC :: cs_approche_multidomaine = 14001
      INTEGER, PARAMETER, PUBLIC :: cs_approche_cellcentered = 14002

      INTEGER, PARAMETER, PUBLIC :: cs_relax_jacobi = 15000
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gauss_seid = 15001
      INTEGER, PARAMETER, PUBLIC :: cs_relax_bicgs = 15002
      INTEGER, PARAMETER, PUBLIC :: cs_relax_bicgsl_gsd = 15003
      INTEGER, PARAMETER, PUBLIC :: cs_relax_bicgsl_ilu = 15004
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmres = 15005
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresl = 15006
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresr = 15007
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresl_gsd = 15008
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresr_gsd = 15009
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresl_ilu = 15010
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresr_ilu = 15011
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresl_ssor = 15012
      INTEGER, PARAMETER, PUBLIC :: cs_relax_gmresr_ssor = 15013
      INTEGER, PARAMETER, PUBLIC :: cs_relax_mf          = 15014
      INTEGER, PARAMETER, PUBLIC :: cs_relax_mf_jacobi   = 15015
      INTEGER, PARAMETER, PUBLIC :: cs_relax_mf_ilu      = 15016
      INTEGER, PARAMETER, PUBLIC :: cs_relax_mf_lusgs    = 15017
      INTEGER, PARAMETER, PUBLIC :: cs_relax_nl_lusgs    = 15020
      INTEGER, PARAMETER, PUBLIC :: cs_relax_blusgs      = 15021
      INTEGER, PARAMETER, PUBLIC :: cs_relax_glusgs      = 15022

      INTEGER, PARAMETER, PUBLIC :: cs_parall_min = 16000
      INTEGER, PARAMETER, PUBLIC :: cs_parall_max = 16001
      INTEGER, PARAMETER, PUBLIC :: cs_parall_sum = 16002

      INTEGER, PARAMETER, PUBLIC :: noregfile = 1234

      !! element_type
      INTEGER, PARAMETER, PUBLIC :: et_segP1   = 1      
      INTEGER, PARAMETER, PUBLIC :: et_triaP1  = 2
      INTEGER, PARAMETER, PUBLIC :: et_quadP1  = 3
      INTEGER, PARAMETER, PUBLIC :: et_tetraP1 = 4
      INTEGER, PARAMETER, PUBLIC :: et_hexaP1  = 5
      INTEGER, PARAMETER, PUBLIC :: et_segP2   = 10
      INTEGER, PARAMETER, PUBLIC :: et_triaP2  = 11
      INTEGER, PARAMETER, PUBLIC :: et_quadP2  = 12
      INTEGER, PARAMETER, PUBLIC :: et_tetraP2 = 13
      INTEGER, PARAMETER, PUBLIC :: et_hexaP2  = 14
      INTEGER, PARAMETER, PUBLIC :: et_pyramP1 = 15
      INTEGER, PARAMETER, PUBLIC :: et_pyramP2 = 16

      PUBLIC :: decoder_data_meshtype
      PUBLIC :: encoder_data_meshtype
      PUBLIC :: decoder_data_loith
      PUBLIC :: encoder_data_loith
      PUBLIC :: encoder_data_RGrad
      PUBLIC :: encoder_data_icas
      PUBLIC :: decoder_data_methode
      PUBLIC :: encoder_data_methode
      PUBLIC :: encoder_data_nordre
      PUBLIC :: encoder_data_logfr
      PUBLIC :: encoder_data_loicfl
      PUBLIC :: decoder_data_loivisq
      PUBLIC :: encoder_data_loivisq
      PUBLIC :: encoder_data_iflux
      PUBLIC :: decoder_data_modele
      PUBLIC :: encoder_data_modele
      PUBLIC :: decoder_data_loietat
      PUBLIC :: encoder_data_loietat
      PUBLIC :: decoder_data_geotype
      PUBLIC :: encoder_data_geotype
      PUBLIC :: decoder_data_approche
      PUBLIC :: encoder_data_approche
      PUBLIC :: decoder_data_relax
      PUBLIC :: encoder_data_relax

      REAL, PUBLIC, SAVE :: nantemp = -9.99e29

      INTERFACE
         SUBROUTINE makenan(res, xx) !-- BIND(C,NAME="makenan_")
            INTEGER, INTENT(IN) :: xx
            REAL, INTENT(OUT) :: res
         END SUBROUTINE makenan
      END INTERFACE


      PUBLIC :: delegate_stop

      PUBLIC :: en_minuscules

      CHARACTER(LEN = 200), PRIVATE :: test

      LOGICAL, SAVE, PUBLIC :: debug_precision = .FALSE.
      CHARACTER(LEN = *), PARAMETER, PUBLIC :: fmt_prec1 = "(A, 1X, A, F45.30)" !-- 15 d�cimales, avec 15 0 devant �vent.
      CHARACTER(LEN = *), PARAMETER, PUBLIC :: fmt_prec2 = "(A, 1X, A, I5, 1X, A, F45.30)"


      INTERFACE RE_ALLOCATE
         MODULE PROCEDURE RE_ALLOCATE_R2,RE_ALLOCATE_I2
      END INTERFACE RE_ALLOCATE

   CONTAINS !-- types_enumeres


     SUBROUTINE RE_ALLOCATE_R2(Tab, dims)
       REAL   , DIMENSION(:,:), POINTER    :: Tab
       INTEGER, DIMENSION(:),   INTENT(IN) :: dims
       
       REAL   , DIMENSION(:,:), POINTER    :: TabTmp

       INTEGER :: s1,s2, m1, m2

       s1 = SIZE(Tab, DIM=1)
       s2 = SIZE(Tab, DIM=2)

       ALLOCATE( TabTmp(s1, s2) )

       TabTmp = 0.d0

       TabTmp(1:s1,1:s2) = Tab(1:s1,1:s2)

       DEALLOCATE( Tab )
       ALLOCATE( Tab(dims(1), dims(2)) )

       Tab = 0.d0
       
       m1 = MIN(s1, dims(1))
       m2 = MIN(s2, dims(2))
       
       Tab(1:m1, 1:m2) = TabTmp(1:m1, 1:m2) 

       DEALLOCATE( TabTmp )

     END SUBROUTINE RE_ALLOCATE_R2


     SUBROUTINE RE_ALLOCATE_I2(Tab, dims)
       INTEGER, DIMENSION(:,:), POINTER    :: Tab
       INTEGER, DIMENSION(:),   INTENT(IN) :: dims
       
       INTEGER, DIMENSION(:,:), POINTER    :: TabTmp

       INTEGER :: s1,s2, m1, m2

       s1 = SIZE(Tab, DIM=1)
       s2 = SIZE(Tab, DIM=2)

       ALLOCATE( TabTmp(s1, s2) )

       TabTmp(1:s1,1:s2) = Tab(1:s1,1:s2)

       DEALLOCATE( Tab )
       ALLOCATE( Tab(dims(1), dims(2)) )

       Tab = 0

       m1 = MIN(s1, dims(1))
       m2 = MIN(s2, dims(2))
       
       Tab(1:m1, 1:m2) = TabTmp(1:m1, 1:m2) 

       DEALLOCATE( TabTmp )

     END SUBROUTINE RE_ALLOCATE_I2



!***********************************************************************
!                                                    -- makenan_d1..7
!***********************************************************************

      SUBROUTINE makenan_d1(res, xx)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "makenan_d1"
         INTEGER, INTENT(IN) :: xx
         REAL, DIMENSION(: ), INTENT(OUT) :: res
         INTEGER :: ii
         DO ii = 1, SIZE(res)
            CALL makenan(res(ii), xx)
         END DO
      END SUBROUTINE makenan_d1
      SUBROUTINE makenan_d2(res, xx)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "makenan_d2"
         INTEGER, INTENT(IN) :: xx
         REAL, DIMENSION(:, : ), INTENT(OUT) :: res
         INTEGER :: ii, jj
         DO jj = 1, SIZE(res, 2)
            DO ii = 1, SIZE(res, 1)
               CALL makenan(res(ii, jj), xx)
            END DO
         END DO
      END SUBROUTINE makenan_d2
      SUBROUTINE makenan_d3(res, xx)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "makenan_d3"
         INTEGER, INTENT(IN) :: xx
         REAL, DIMENSION(:, :, : ), INTENT(OUT) :: res
         INTEGER :: ii, jj, kk
         DO kk = 1, SIZE(res, 3)
            DO jj = 1, SIZE(res, 2)
               DO ii = 1, SIZE(res, 1)
                  CALL makenan(res(ii, jj, kk), xx)
               END DO
            END DO
         END DO
      END SUBROUTINE makenan_d3
      SUBROUTINE makenan_d4(res, xx)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "makenan_d4"
         INTEGER, INTENT(IN) :: xx
         REAL, DIMENSION(:, :, :, : ), INTENT(OUT) :: res
         INTEGER :: ii, jj, kk, ll
         DO ll = 1, SIZE(res, 4)
            DO kk = 1, SIZE(res, 3)
               DO jj = 1, SIZE(res, 2)
                  DO ii = 1, SIZE(res, 1)
                     CALL makenan(res(ii, jj, kk, ll), xx)
                  END DO
               END DO
            END DO
         END DO
      END SUBROUTINE makenan_d4
      SUBROUTINE makenan_d5(res, xx)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "makenan_d5"
         INTEGER, INTENT(IN) :: xx
         REAL, DIMENSION(:, :, :, :, : ), INTENT(OUT) :: res
         INTEGER :: ii, jj, kk, ll, mm
         DO mm = 1, SIZE(res, 5)
            DO ll = 1, SIZE(res, 4)
               DO kk = 1, SIZE(res, 3)
                  DO jj = 1, SIZE(res, 2)
                     DO ii = 1, SIZE(res, 1)
                        CALL makenan(res(ii, jj, kk, ll, mm), xx)
                     END DO
                  END DO
               END DO
            END DO
         END DO
      END SUBROUTINE makenan_d5
      SUBROUTINE makenan_d6(res, xx)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "makenan_d6"
         INTEGER, INTENT(IN) :: xx
         REAL, DIMENSION(:, :, :, :, :, : ), INTENT(OUT) :: res
         INTEGER :: ii, jj, kk, ll, mm, nn
         DO nn = 1, SIZE(res, 6)
            DO mm = 1, SIZE(res, 5)
               DO ll = 1, SIZE(res, 4)
                  DO kk = 1, SIZE(res, 3)
                     DO jj = 1, SIZE(res, 2)
                        DO ii = 1, SIZE(res, 1)
                           CALL makenan(res(ii, jj, kk, ll, mm, nn), xx)
                        END DO
                     END DO
                  END DO
               END DO
            END DO
         END DO
      END SUBROUTINE makenan_d6
      SUBROUTINE makenan_d7(res, xx)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "makenan_d7"
         INTEGER, INTENT(IN) :: xx
         REAL, DIMENSION(:, :, :, :, :, :, : ), INTENT(OUT) :: res
         INTEGER :: ii, jj, kk, ll, mm, nn, oo
         DO oo = 1, SIZE(res, 7)
            DO nn = 1, SIZE(res, 6)
               DO mm = 1, SIZE(res, 5)
                  DO ll = 1, SIZE(res, 4)
                     DO kk = 1, SIZE(res, 3)
                        DO jj = 1, SIZE(res, 2)
                           DO ii = 1, SIZE(res, 1)
                              CALL makenan(res(ii, jj, kk, ll, mm, nn, oo), xx)
                           END DO
                        END DO
                     END DO
                  END DO
               END DO
            END DO
         END DO
      END SUBROUTINE makenan_d7


!***********************************************************************
!                                               -- delegate_stop
!***********************************************************************
!-- (C) UMR 5251 IMB   C.N.R.S.  2008/04/22

!-- Summary : arreter proprement un job // comme s�quentiel, avec la pile d'appel si possible
!-- End Summary

      SUBROUTINE delegate_stop(ident)
         INTEGER, INTENT(IN), OPTIONAL :: ident
         CHARACTER(LEN = *), PARAMETER :: mod_name = "delegate_stop"
#ifndef SEQUENTIEL
#ifndef MPICH2
         INCLUDE "mpif.h"
#endif
         INTEGER :: statinfo
#endif
         REAL :: nan

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!                                                   -- delegate_stop.1
!-------------------------------------------- mise en ordre des fichiers

         CALL FLUSH(0)
         CALL FLUSH(6)

!-----------------------------------------------------------------------
!                                                   -- delegate_stop.2
!--------------------------------------------------------- cas parall�le

#ifndef SEQUENTIEL
         PRINT *, mod_name, " Mpi_Abort imminent"
         PRINT *, mod_name, " mode classique"
         CALL Mpi_abort(MPI_comm_world, 1, statinfo)
         CALL Mpi_finalize(statinfo) !-- si cela ne suffit pas...
#endif

!-----------------------------------------------------------------------
!                                                   -- delegate_stop.4
!-------------------------------------------------------- cas s�quentiel

         PRINT *, mod_name, " Abort imminent"

         CALL makenan(nan, 1)
         PRINT *, mod_name, " ABORT :", SQRT(nan) !-- avec de la chance, (par ex. gfortran -fbacktrace), on r�cup�re la pile d'appel

         CALL Abort() !-- un probl�me s'est produit, il faut arr�ter le job avec la pile d'appels si possible
         PRINT *, ident
      END SUBROUTINE delegate_stop

!***********************************************************************
!                                               -- en_minuscules
!***********************************************************************
!-- (C) UMR 5251 IMB   C.N.R.S.  2007/05/24

!-- Summary :
!-- End Summary

      SUBROUTINE en_minuscules(nin, nout) !-- Interface

         CHARACTER(LEN = *), PARAMETER :: mod_name = "en_minuscules"

!-- arguments

         CHARACTER(LEN = *), INTENT(IN) :: nin
         CHARACTER(LEN = *), INTENT(OUT) :: nout

!-- locales

         INTEGER :: ii, offset, icode
         CHARACTER(LEN = 1) :: car

!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!                                               -- en_minuscules.1
!-----------------------------------------------------------------------

         offset = IACHAR("a") - IACHAR("A")

         nout = ADJUSTL(TRIM(nin))
         DO ii = 1, LEN_TRIM(nout)

            car = nout(ii: ii)

            IF (car == "_") THEN
            ELSE IF (car == "%") THEN
               nout(ii: ii) = "_"
            ELSE IF (car >= "0" .AND. car <= "9") THEN
            ELSE IF (car >= "a" .AND. car <= "z") THEN
            ELSE IF (car == "-") THEN
               nout(ii: ii) = "_"
            ELSE IF (car >= "A" .AND. car <= "Z") THEN
               icode = IACHAR(car) + offset
               nout(ii: ii) = ACHAR(icode)
            ELSE
               PRINT *, mod_name, " ERREUR : Caractere invalide dans ", TRIM(nout), " ", ii, " ", car
               CALL delegate_stop()
            END IF

         END DO

      END SUBROUTINE en_minuscules

!--*********************************************************************
!                                              -- decoder_data_meshtype
!--*********************************************************************

      SUBROUTINE decoder_data_meshtype(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_meshtype"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "amdba") THEN
            code = cs_meshtype_amdba
         ELSE IF (TRIM(test) == "amdba4") THEN
            code = cs_meshtype_amdba4
         ELSE IF (TRIM(test) == "mesh") THEN
            code = cs_meshtype_mesh
         ELSE IF (TRIM(test) == "meshemc2") THEN
            code = cs_meshtype_meshemc2
         ELSE IF (TRIM(test) == "meshemc2a") THEN
            code = cs_meshtype_meshemc2a
         ELSE IF (TRIM(test) == "meshemc2p") THEN
            code = cs_meshtype_meshemc2p
         ELSE IF (TRIM(test) == "geom3d") THEN
            code = cs_meshtype_geom3d
         ELSE IF (TRIM(test) == "gmsh") THEN
            code = cs_meshtype_gmsh
         ELSE IF (TRIM(test) == "meshemc2p2") THEN
            code = cs_meshtype_meshemc2p2p2
         ELSE IF (TRIM(test) == "gmsh2") THEN
            code = cs_meshtype_gmsh2
         ELSE IF (TRIM(test) == "gmsh3d") THEN
            code = cs_meshtype_gmsh3d
         ELSE IF (TRIM(test) == "gmsh_hexa") THEN
            code = cs_meshtype_gmsh_hexa
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
             PRINT*, "A bientot"
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_meshtype

!--*********************************************************************
!                                              -- encoder_data_meshtype
!--*********************************************************************

      SUBROUTINE encoder_data_meshtype(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_meshtype"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_meshtype_amdba) THEN
            chaine = "amdba"
         ELSE IF (code == cs_meshtype_amdba4) THEN
            chaine = "amdba4"
         ELSE IF (code == cs_meshtype_mesh) THEN
            chaine = "mesh"
         ELSE IF (code == cs_meshtype_meshemc2) THEN
            chaine = "meshemc2"
         ELSE IF (code == cs_meshtype_meshemc2a) THEN
            chaine = "meshemc2a"
         ELSE IF (code == cs_meshtype_meshemc2p) THEN
            chaine = "meshemc2p"
         ELSE IF (code == cs_meshtype_geom3d) THEN
            chaine = "geom3d"
         ELSE IF (code == cs_meshtype_gmsh) THEN
            chaine = "gmsh"
        ELSE IF (code == cs_meshtype_meshemc2p2p2) THEN
            chaine = "meshemc2p2"
         ELSE IF (code == cs_meshtype_gmsh2) THEN
            chaine = "gmsh2"
         ELSE IF (code == cs_meshtype_gmsh3d) THEN
            chaine = "gmsh3d"
         ELSE IF (code == cs_meshtype_gmsh_hexa) THEN
            chaine = "gmsh_hexa"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_meshtype

!--*********************************************************************
!                                              -- decoder_data_loith
!--*********************************************************************

      SUBROUTINE decoder_data_loith(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_loith"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "constant") THEN
            code = cs_loith_constant
         ELSE IF (TRIM(test) == "prandtl") THEN
            code = cs_loith_prandtl
         ELSE IF (TRIM(test) == "no") THEN
            code = cs_loith_no
         ELSE IF (TRIM(test) == "chungdilute") THEN
            code = cs_loith_chungdilute
         ELSE IF (TRIM(test) == "chungdense") THEN
            code = cs_loith_chungdense
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_loith

!--*********************************************************************
!                                              -- encoder_data_loith
!--*********************************************************************

      SUBROUTINE encoder_data_loith(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_loith"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_loith_constant) THEN
            chaine = "constant"
         ELSE IF (code == cs_loith_prandtl) THEN
            chaine = "prandtl"
         ELSE IF (code == cs_loith_no) THEN
            chaine = "no"
         ELSE IF (code == cs_loith_chungdilute) THEN
            chaine = "chungdilute"
         ELSE IF (code == cs_loith_chungdense) THEN
            chaine = "chungdense"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_loith

!--*********************************************************************
!                                              -- encoder_data_RGrad
!--*********************************************************************

      SUBROUTINE encoder_data_RGrad(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_RGrad"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_RGrad_ex) THEN
            chaine = "0=exact"
         ELSEIF (code == cs_RGrad_GG) THEN
            chaine = "1=Green_Gauss"
         ELSE IF (code == cs_RGrad_GG_b) THEN
            chaine = "2=Green_Gauss b"
         ELSE IF (code == cs_RGrad_LSQ) THEN
            chaine = "3=Least_square"
         ELSE IF (code == cs_RGrad_SPRZZ) THEN
            chaine = "4=SPR_ZZ"
         ELSE IF (code == cs_RGrad_SPRZN) THEN
            chaine = "5=SPR_ZN"
         ELSE
            PRINT *, mod_name, " ERREOR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

       END SUBROUTINE encoder_data_RGrad

!--*********************************************************************
!                                              -- encoder_data_icas
!--*********************************************************************

      SUBROUTINE encoder_data_icas(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_icas"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_icas_uniforme) THEN
            chaine = "0=uniforme"
         ELSE IF (code == cs_icas_ringleb) THEN
            chaine = "10=ringleb"
         ELSE IF(code== cs_icas_MS) THEN
            chaine = "20=manufactured_solution"
         ELSE IF(code == cs_icas_supvort) THEN
            chaine = "30=supersonic vortex"
         ELSE IF (code == cs_icas_test) THEN
            chaine = "100=test"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_icas

!--*********************************************************************
!                                              -- decoder_data_methode
!--*********************************************************************

      SUBROUTINE decoder_data_methode(chaine, code)

        CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_methode"

        CHARACTER(LEN = *), INTENT(IN) :: chaine
        INTEGER, INTENT(OUT) :: code

        CALL en_minuscules(chaine, test)

        IF (TRIM(test) == "exp_rk1") THEN
           code = cs_methode_exp_rk1
        ELSE IF (TRIM(test) == "exp_rk1_fake") THEN
           code = cs_methode_exp_rk1_fake
        ELSE IF (TRIM(test) == "exp_rk2") THEN
           code = cs_methode_exp_rk2
        ELSE IF (TRIM(test) == "exp_rk2_fake") THEN
           code = cs_methode_exp_rk2_fake
        ELSE IF (TRIM(test) == "exp_rk3_fake") THEN
           code = cs_methode_exp_rk3_fake
        ELSE IF (TRIM(test) == "exp_rk4_fake") THEN
           code = cs_methode_exp_rk4_fake
        ELSE IF (TRIM(test) == "exp_rk3") THEN
           code = cs_methode_exp_rk3
        ELSE IF (TRIM(test) == "exp_rk4") THEN
           code = cs_methode_exp_rk4
        ELSE IF (TRIM(test) == "exp_pc2") THEN
           code = cs_methode_exp_pc2
        ELSE IF (TRIM(test) == "imp_rk1") THEN
           code = cs_methode_imp_rk1
        ELSE IF (TRIM(test) == "imp_rk1_rd") THEN
           code = cs_methode_imp_rk1_rd
        ELSE IF (TRIM(test) == "imp_exp_rd") THEN
           code = cs_methode_imp_exp_rd
        ELSE IF (TRIM(test) == "implin_rk1") THEN
           code = cs_methode_implin_rk1
        ELSE IF (TRIM(test) == "implin_gear") THEN
           code = cs_methode_implin_gear
        ELSE IF (TRIM(test) == "implin_gear_rd") THEN
           code = cs_methode_implin_gear_rd
        ELSE IF (TRIM(test) == "imp_gear") THEN
           code = cs_methode_imp_gear
        ELSE IF (TRIM(test) == "imp_dce") THEN
           code = cs_methode_imp_dce
        ELSE IF (TRIM(test) == "imp_gear_rd") THEN
           code = cs_methode_imp_gear_rd
        ELSE IF (TRIM(test) == "imp_dce_rd") THEN
           code = cs_methode_imp_dce_rd
        ELSE IF (TRIM(test) == "implin_rk1_rd") THEN
           code = cs_methode_implin_rk1_rd
        ELSE IF (TRIM(test) == "exp_rk1_rd") THEN
           code = cs_methode_exp_rk1_rd
        ELSE  IF (TRIM(test) == "exp_rk_mario_rd") THEN
           code = cs_methode_exp_rk_Mario_rd
        ELSE
           PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
           CALL delegate_stop()
        END IF

      END SUBROUTINE decoder_data_methode

!--*********************************************************************
!                                              -- encoder_data_methode
!--*********************************************************************

      SUBROUTINE encoder_data_methode(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_methode"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_methode_exp_rk1) THEN
            chaine = "exp_rk1"
         ELSE IF (code == cs_methode_exp_rk1_fake) THEN
            chaine = "exp_rk1_fake"
         ELSE IF (code == cs_methode_exp_rk2) THEN
            chaine = "exp_rk2"
         ELSE IF (code == cs_methode_exp_rk2_fake) THEN
            chaine = "exp_rk2_fake"
         ELSE IF (code == cs_methode_exp_rk3_fake) THEN
            chaine = "exp_rk3_fake"
         ELSE IF (code == cs_methode_exp_rk4_fake) THEN
            chaine = "exp_rk4_fake"
         ELSE IF (code == cs_methode_exp_rk3) THEN
            chaine = "exp_rk3"
         ELSE IF (code == cs_methode_exp_rk4) THEN
            chaine = "exp_rk4"
         ELSE IF (code == cs_methode_exp_pc2) THEN
            chaine = "exp_pc2"
         ELSE IF (code == cs_methode_imp_rk1) THEN
            chaine = "imp_rk1"
         ELSE IF (code == cs_methode_imp_rk1_rd) THEN
            chaine = "imp_rk1_rd"
         ELSE IF (code == cs_methode_imp_exp_rd) THEN
            chaine = "imp_exp_rd"
         ELSE IF (code == cs_methode_implin_rk1) THEN
            chaine = "implin_rk1"
         ELSE IF (code == cs_methode_implin_gear) THEN
            chaine = "implin_gear"
         ELSE IF (code == cs_methode_implin_gear_rd) THEN
            chaine = "implin_gear_rd"
         ELSE IF (code == cs_methode_imp_gear) THEN
            chaine = "imp_gear"
         ELSE IF (code == cs_methode_imp_dce) THEN
            chaine = "imp_dce"
         ELSE IF (code == cs_methode_imp_gear_rd) THEN
            chaine = "imp_gear_rd"
         ELSE IF (code == cs_methode_imp_dce_rd) THEN
            chaine = "imp_dce_rd"
         ELSE IF (code == cs_methode_implin_rk1_rd) THEN
            chaine = "implin_rk1_rd"
         ELSE IF (code == cs_methode_exp_rk1_rd) THEN
            chaine = "exp_rk1_rd"
         ELSE  IF (code == cs_methode_exp_rk_Mario_rd) THEN
           chaine = "exp_rk_Mario_rd"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_methode

!--*********************************************************************
!                                              -- encoder_data_nordre
!--*********************************************************************

      SUBROUTINE encoder_data_nordre(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_nordre"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_nordre_1) THEN
            chaine = "1"
         ELSE IF (code == cs_nordre_2) THEN
            chaine = "2"
         ELSE IF (code == cs_nordre_3) THEN
            chaine = "3"
         ELSE IF (code == cs_nordre_4) THEN
            chaine = "4"
         ELSE IF (code == cs_nordre_5) THEN
            chaine = "5"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_nordre

!--*********************************************************************
!                                              -- encoder_data_logfr
!--*********************************************************************

      SUBROUTINE encoder_data_logfr(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_logfr"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == lgc_dirichlet) THEN
            chaine = "1=dirichlet"
         ELSE IF (code == lgc_neumann) THEN
            chaine = "2=neumann"
         ELSE IF (code == lgc_entree_sa) THEN
            chaine = "10=entree_sa"
         ELSE IF (code == lgc_sortie_sa) THEN
            chaine = "11=entree_sa"
         ELSE IF (code == lgc_paroi_adh_adiab) THEN
            chaine = "19=paroi_adh_adiab"
         ELSE IF (code == lgc_paroi_adh_adiab_flux_nul) THEN
            chaine = "20=paroi_adh_adiab_flux_nul"
         ELSE IF (code == lgc_paroi_glissante) THEN
            chaine = "21=paroi_glissante"
         ELSE IF (code == lgc_symetrie_plane) THEN
            chaine = "22=symetrie_plane"
         ELSE IF (code == lgc_paroi_lagrange) THEN
            chaine = "23=paroi_lagrange"
         ELSE IF (code == lgc_paroi_misc) THEN
            chaine = "25=paroi_misc"
         ELSE IF (code == lgc_paroi_pression_donnee) THEN
            chaine = "26=paroi_pression_donnee"
         ELSE IF (code == lgc_paroi_temp_donnee) THEN
            chaine = "27=paroi_temp_donnee"
         ELSE IF (code == lgc_symetrie_lagrange) THEN
            chaine = "30=symetrie_lagrange"
         ELSE IF (code == lgc_paroi_v_et_p_donnes) THEN
            chaine = "35=paroi_v_et_p_donnes"
         ELSE IF (code == lgc_paroi_v_et_t_donnes) THEN
            chaine = "36=paroi_v_et_t_donnes"
         ELSE IF (code == lgc_paroi_p_et_t_donnes) THEN
            chaine = "37=paroi_p_et_t_donnes"
         ELSE IF (code == lgc_paroi_diric_vpt_donnes) THEN
            chaine = "38=paroi_diric_vpt_donnes"
         ELSE IF (code == lgc_gradient_nul_exterieur) THEN
            chaine = "40=gradient_nul_exterieur"
         ELSE IF (code == lgc_paroi_conducteur_parfait) THEN
            chaine = "41=paroi_conducteur_parfait"
         ELSE IF (code == lgc_gradient_nul_interieur) THEN
            chaine = "50=gradient_nul_interieur"
         ELSE IF (code == lgc_sortie_subsonique) THEN
            chaine = "51=sortie_subsonique"
         ELSE IF (code == lgc_sortie_mixte) THEN
            chaine = "52=sortie_mixte"
         ELSE IF (code == lgc_special_steger_warming) THEN
            chaine = "53=special_steger_warming"
         ELSE IF (code == lgc_flux_interne) THEN
            chaine = "54=flux_interne"
         ELSE IF (code == lgc_sortie_vitesse_normale) THEN
            chaine = "60=sortie_vitesse_normale"
         ELSE IF (code == lgc_sortie_pression_donnee) THEN
            chaine = "61=sortie_pression_donnee"
         ELSE IF (code == lgc_flux_de_steger) THEN
            chaine = "64=flux_de_steger"
         ELSE IF (code == lgc_incomp_v_donne) THEN
            chaine = "75=incomp_v_donne"
         ELSE IF (code == lgc_periodique) THEN
            chaine = "90=periodique"
         ELSE IF (code == lgc_entree_turbine_subsonique) THEN
            chaine = "91=entree_turbine_subsonique"
         ELSE IF (code == lgc_sortie_turbine_subsonique) THEN
            chaine = "92=sortie_turbine_subsonique"
         ELSE IF (code == lgc_subsonic_inflow_Pt) THEN
            chaine = "95=subsonic_inflow_Pt"
         ELSE IF (code == lgc_ns_steger_warming) THEN
            chaine = "154=ns_steger_warming"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_logfr

!--*********************************************************************
!                                              -- encoder_data_loicfl
!--*********************************************************************

      SUBROUTINE encoder_data_loicfl(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_loicfl"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_loicfl_clfmax) THEN
            chaine = "0=clfmax"
         ELSE IF (code == cs_loicfl_minmult) THEN
            chaine = "2=minmult"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_loicfl

!--*********************************************************************
!                                              -- decoder_data_loivisq
!--*********************************************************************

      SUBROUTINE decoder_data_loivisq(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_loivisq"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "constant") THEN
            code = cs_loivisq_constant
         ELSE IF (TRIM(test) == "sutherland") THEN
            code = cs_loivisq_sutherland
         ELSE IF (TRIM(test) == "pant") THEN
            code = cs_loivisq_pant
         ELSE IF (TRIM(test) == "no") THEN
            code = cs_loivisq_no
         ELSE IF (TRIM(test) == "chungdilute") THEN
            code = cs_loivisq_chungdilute
         ELSE IF (TRIM(test) == "chungdense") THEN
            code = cs_loivisq_chungdense
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_loivisq

!--*********************************************************************
!                                              -- encoder_data_loivisq
!--*********************************************************************

      SUBROUTINE encoder_data_loivisq(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_loivisq"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_loivisq_constant) THEN
            chaine = "constant"
         ELSE IF (code == cs_loivisq_sutherland) THEN
            chaine = "sutherland"
         ELSE IF (code == cs_loivisq_pant) THEN
            chaine = "pant"
         ELSE IF (code == cs_loivisq_no) THEN
            chaine = "no"
         ELSE IF (code == cs_loivisq_chungdilute) THEN
            chaine = "chungdilute"
         ELSE IF (code == cs_loivisq_chungdense) THEN
            chaine = "chungdense"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_loivisq

!--*********************************************************************
!                                              -- encoder_data_iflux
!--*********************************************************************

      SUBROUTINE encoder_data_iflux(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_iflux"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_iflux_1) THEN
            chaine = "1"
         ELSE IF (code == cs_iflux_2) THEN
            chaine = "2"
         ELSE IF (code == cs_iflux_3) THEN
            chaine = "3"
         ELSE IF (code == cs_iflux_4) THEN
            chaine = "4"
         ELSE IF (code == cs_iflux_5) THEN
            chaine = "5"
         ELSE IF (code == cs_iflux_6) THEN
            chaine = "6"
         ELSE IF (code == cs_iflux_7) THEN
            chaine = "7"
         ELSE IF (code == cs_iflux_8) THEN
            chaine = "8"
         ELSE IF (code == cs_iflux_9) THEN
            chaine = "9"
         ELSE IF (code == cs_iflux_10) THEN
            chaine = "10"
         ELSE IF (code == cs_iflux_11) THEN
            chaine = "11"
         ELSE IF (code == cs_iflux_12) THEN
            chaine = "12"
         ELSE IF (code == cs_iflux_13) THEN
            chaine = "13"
         ELSE IF (code == cs_iflux_14) THEN
            chaine = "14"
         ELSE IF (code == cs_iflux_15) THEN
            chaine = "15"
         ELSE IF (code == cs_iflux_16) THEN
            chaine = "16"
         ELSE IF (code == cs_iflux_20) THEN
            chaine = "20"
         ELSE IF (code == cs_iflux_21) THEN
            chaine = "21"
         ELSE IF (code == cs_iflux_22) THEN
            chaine = "22"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_iflux

!--*********************************************************************
!                                              -- encoder_data_imeth
!--*********************************************************************

      SUBROUTINE encoder_data_imeth(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_imeth"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_imeth_1) THEN
            chaine = "1"
         ELSE IF (code == cs_imeth_2) THEN
            chaine = "2"
         ELSE IF (code == cs_imeth_3) THEN
            chaine = "3"
         ELSE IF (code == cs_imeth_4) THEN
            chaine = "4"
         ELSE IF (code == cs_imeth_5) THEN
            chaine = "5"
         ELSE IF (code == cs_imeth_6) THEN
            chaine = "6"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_imeth

!--*********************************************************************
!                                              -- decoder_data_modele
!--*********************************************************************

      SUBROUTINE decoder_data_modele(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_modele"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "euler") THEN
            code = cs_modele_euler
         ELSE IF (TRIM(test) == "eulerns") THEN
            code = cs_modele_eulerns
         ELSE IF (TRIM(test) == "navier_stokes") THEN
            code = cs_modele_eulerns
         ELSE IF (TRIM(test) == "navierstokes") THEN
            code = cs_modele_eulerns
         ELSE IF (TRIM(test) == "incomptherm") THEN
            code = cs_modele_incomptherm
         ELSE IF (TRIM(test) == "incomp") THEN
            code = cs_modele_incomp
         ELSE IF (TRIM(test) == "incompns") THEN
            code = cs_modele_incomp
         ELSE IF (TRIM(test) == "eulermf") THEN
            code = cs_modele_eulermf
         ELSE IF (TRIM(test) == "eulermfv") THEN
            code = cs_modele_eulermfv
         ELSE IF (TRIM(test) == "idealmhd") THEN
            code = cs_modele_idealmhd
         ELSE IF (TRIM(test) == "fullmhd") THEN
            code = cs_modele_fullmhd
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_modele

!--*********************************************************************
!                                              -- encoder_data_modele
!--*********************************************************************

      SUBROUTINE encoder_data_modele(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_modele"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_modele_euler) THEN
            chaine = "euler"
         ELSE IF (code == cs_modele_eulerns) THEN
            chaine = "eulerns"
         ELSE IF (code == cs_modele_incomptherm) THEN
            chaine = "incomptherm"
         ELSE IF (code == cs_modele_incomp) THEN
            chaine = "incomp"
         ELSE IF (code == cs_modele_eulermf) THEN
            chaine = "eulermf"
         ELSE IF (code == cs_modele_eulermfv) THEN
            chaine = "eulermfv"
         ELSE IF (code == cs_modele_idealmhd) THEN
            chaine = "idealmhd"
         ELSE IF (code == cs_modele_fullmhd) THEN
            chaine = "fullmhd"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_modele

!--*********************************************************************
!                                              -- decoder_data_loietat
!--*********************************************************************

      SUBROUTINE decoder_data_loietat(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_loietat"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "userdefined") THEN
            code = cs_loietat_userdefined
         ELSE IF (TRIM(test) == "ig") THEN
            code = cs_loietat_ig
         ELSE IF (TRIM(test) == "gp") THEN
            code = cs_loietat_gp
         ELSE IF (TRIM(test) == "sg") THEN
            code = cs_loietat_sg
         ELSE IF (TRIM(test) == "sgref") THEN
            code = cs_loietat_sgref
         ELSE IF (TRIM(test) == "sgmf") THEN
            code = cs_loietat_sgmf
         ELSE IF (TRIM(test) == "eau") THEN
            code = cs_loietat_eau
         ELSE IF (TRIM(test) == "mhd_gp") THEN
            code = cs_loietat_mhd_gp
         ELSE IF (TRIM(test) == "vdw") THEN
            code = cs_loietat_vdw
         ELSE IF (TRIM(test) == "prsv") THEN
            code = cs_loietat_prsv
         ELSE IF (TRIM(test) == "sw") THEN
            code = cs_loietat_sw
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_loietat

!--*********************************************************************
!                                              -- encoder_data_loietat
!--*********************************************************************

      SUBROUTINE encoder_data_loietat(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_loietat"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_loietat_userdefined) THEN
            chaine = "userdefined"
         ELSE IF (code == cs_loietat_ig) THEN
            chaine = "IG"
         ELSE IF (code == cs_loietat_gp) THEN
            chaine = "GP"
         ELSE IF (code == cs_loietat_sg) THEN
            chaine = "SG"
         ELSE IF (code == cs_loietat_sgref) THEN
            chaine = "SGref"
         ELSE IF (code == cs_loietat_sgmf) THEN
            chaine = "SGMF"
         ELSE IF (code == cs_loietat_eau) THEN
            chaine = "eau"
         ELSE IF (code == cs_loietat_mhd_gp) THEN
            chaine = "MHD_gp"
         ELSE IF (code == cs_loietat_vdw) THEN
            chaine = "VDW"
         ELSE IF (code == cs_loietat_prsv) THEN
            chaine = "PRSV"
         ELSE IF (code == cs_loietat_sw) THEN
            chaine = "SW"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_loietat

!--*********************************************************************
!                                              -- decoder_data_geotype
!--*********************************************************************

      SUBROUTINE decoder_data_geotype(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_geotype"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "1d") THEN
            code = cs_geotype_1d
         ELSE IF (TRIM(test) == "2dplan") THEN
            code = cs_geotype_2dplan
         ELSE IF (TRIM(test) == "2dper") THEN
            code = cs_geotype_2dper
         ELSE IF (TRIM(test) == "2daxisym") THEN
            code = cs_geotype_2daxisym
         ELSE IF (TRIM(test) == "2daxisymx") THEN
            code = cs_geotype_2daxisymx
         ELSE IF (TRIM(test) == "2daxisymy") THEN
            code = cs_geotype_2daxisymy
         ELSE IF (TRIM(test) == "3d") THEN
            code = cs_geotype_3d
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_geotype

!--*********************************************************************
!                                              -- encoder_data_geotype
!--*********************************************************************

      SUBROUTINE encoder_data_geotype(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_geotype"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_geotype_1d) THEN
            chaine = "1d"
         ELSE IF (code == cs_geotype_2dplan) THEN
            chaine = "2dplan"
         ELSE IF (code == cs_geotype_2dper) THEN
            chaine = "2dper"
         ELSE IF (code == cs_geotype_2daxisym) THEN
            chaine = "2daxisym"
         ELSE IF (code == cs_geotype_2daxisymx) THEN
            chaine = "2daxisymx"
         ELSE IF (code == cs_geotype_2daxisymy) THEN
            chaine = "2daxisymy"
         ELSE IF (code == cs_geotype_3d) THEN
            chaine = "3d"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_geotype

!--*********************************************************************
!                                              -- decoder_data_approche
!--*********************************************************************

      SUBROUTINE decoder_data_approche(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_approche"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "vertexcentered") THEN
            code = cs_approche_vertexcentered
         ELSE IF (TRIM(test) == "multidomaine") THEN
            code = cs_approche_multidomaine
         ELSE IF (TRIM(test) == "cellcentered") THEN
            code = cs_approche_cellcentered
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_approche

!--*********************************************************************
!                                              -- encoder_data_approche
!--*********************************************************************

      SUBROUTINE encoder_data_approche(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_approche"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_approche_vertexcentered) THEN
            chaine = "vertexcentered"
         ELSE IF (code == cs_approche_multidomaine) THEN
            chaine = "multidomaine"
         ELSE IF (code == cs_approche_cellcentered) THEN
            chaine = "cellcentered"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_approche

!--*********************************************************************
!                                              -- decoder_data_relax
!--*********************************************************************

      SUBROUTINE decoder_data_relax(chaine, code)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "decoder_data_relax"

         CHARACTER(LEN = *), INTENT(IN) :: chaine
         INTEGER, INTENT(OUT) :: code

         CALL en_minuscules(chaine, test)

         IF (TRIM(test) == "jacobi") THEN
            code = cs_relax_jacobi
         ELSE IF (TRIM(test) == "gauss_seid") THEN
            code = cs_relax_gauss_seid
         ELSE IF (TRIM(test) == "bicgs") THEN
            code = cs_relax_bicgs
         ELSE IF (TRIM(test) == "bicgsl_gsd") THEN
            code = cs_relax_bicgsl_gsd
         ELSE IF (TRIM(test) == "bicgsl_ilu") THEN
            code = cs_relax_bicgsl_ilu
         ELSE IF (TRIM(test) == "gmres") THEN
            code = cs_relax_gmres
         ELSE IF (TRIM(test) == "gmresl") THEN
            code = cs_relax_gmresl
         ELSE IF (TRIM(test) == "gmresr") THEN
            code = cs_relax_gmresr
         ELSE IF (TRIM(test) == "gmresl_gsd") THEN
            code = cs_relax_gmresl_gsd
         ELSE IF (TRIM(test) == "gmresr_gsd") THEN
            code = cs_relax_gmresr_gsd
         ELSE IF (TRIM(test) == "gmresl_ilu") THEN
            code = cs_relax_gmresl_ilu
         ELSE IF (TRIM(test) == "gmresr_ilu") THEN
            code = cs_relax_gmresr_ilu
         ELSE IF (TRIM(test) == "gmresl_ssor") THEN
            code = cs_relax_gmresl_ssor
         ELSE IF (TRIM(test) == "gmresr_ssor") THEN
            code = cs_relax_gmresr_ssor
         ELSE IF (TRIM(test) == "mf") THEN
            code = cs_relax_mf
         ELSE IF (TRIM(test) == "mf_jacobi") THEN
            code = cs_relax_mf_jacobi
         ELSE IF (TRIM(test) == "mf_ilu") THEN
            code = cs_relax_mf_ilu
         ELSE IF(TRIM(test) == "mf_lusgs") THEN
            code = cs_relax_mf_lusgs
         ELSE IF(TRIM(test) == "nl_lusgs") THEN
            code = cs_relax_NL_lusgs
         ELSE IF(TRIM(test) == "blusgs") THEN
            code = cs_relax_blusgs
         ELSE IF(TRIM(test) == "glusgs") THEN
            code = cs_relax_glusgs
         ELSE
            PRINT *, mod_name, " ERREUR : chaine `", chaine, "' inconnue", TRIM(test)
            CALL delegate_stop()
         END IF

      END SUBROUTINE decoder_data_relax

!--*********************************************************************
!                                              -- encoder_data_relax
!--*********************************************************************

      SUBROUTINE encoder_data_relax(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_relax"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_relax_jacobi) THEN
            chaine = "jacobi"
         ELSE IF (code == cs_relax_gauss_seid) THEN
            chaine = "gauss_seid"
         ELSE IF (code == cs_relax_bicgs) THEN
            chaine = "bicgs"
         ELSE IF (code == cs_relax_bicgsl_gsd) THEN
            chaine = "bicgsl_gsd"
         ELSE IF (code == cs_relax_bicgsl_ilu) THEN
            chaine = "bicgsl_ilu"
         ELSE IF (code == cs_relax_gmres) THEN
            chaine = "gmres"
         ELSE IF (code == cs_relax_gmresl) THEN
            chaine = "gmresl"
         ELSE IF (code == cs_relax_gmresr) THEN
            chaine = "gmresr"
         ELSE IF (code == cs_relax_gmresl_gsd) THEN
            chaine = "gmresl_gsd"
         ELSE IF (code == cs_relax_gmresr_gsd) THEN
            chaine = "gmresr_gsd"
         ELSE IF (code == cs_relax_gmresl_ilu) THEN
            chaine = "gmresl_ilu"
         ELSE IF (code == cs_relax_gmresr_ilu) THEN
            chaine = "gmresr_ilu"
         ELSE IF (code == cs_relax_gmresl_ssor) THEN
            chaine = "gmresl_ssor"
         ELSE IF (code == cs_relax_gmresr_ssor) THEN
            chaine = "gmresr_ssor"
         ELSE IF (code == cs_relax_mf) THEN
            chaine = "mf"
         ELSE IF (code == cs_relax_mf_jacobi) THEN
            chaine = "mf_jacobi"
         ELSE IF (code == cs_relax_mf_ilu) THEN
            chaine = "mf_ilu"
         ELSE IF (code == cs_relax_mf_lusgs) THEN
            chaine = "mf_lusgs"
         ELSE IF (code == cs_relax_nl_lusgs) THEN
            chaine = "nl_lusgs"
         ELSE IF (code == cs_relax_blusgs) THEN
            chaine = "blusgs"
         ELSE IF (code == cs_relax_glusgs) THEN
            chaine = "glusgs"
         ELSE      
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_relax

!--*********************************************************************
!                                              -- encoder_data_parall
!--*********************************************************************

      SUBROUTINE encoder_data_parall(code, chaine)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "encoder_data_parall"

         INTEGER, INTENT(IN) :: code
         CHARACTER(LEN = *), INTENT(OUT) :: chaine

         IF (code == cs_parall_min) THEN
            chaine = "MIN"
         ELSE IF (code == cs_parall_max) THEN
            chaine = "MAX"
         ELSE IF (code == cs_parall_sum) THEN
            chaine = "SUM"
         ELSE
            PRINT *, mod_name, " ERREUR : code ", code, " inconnu"
            CALL delegate_stop()
         END IF

      END SUBROUTINE encoder_data_parall

   END MODULE types_enumeres

   MODULE LesTypes
     USE element_class     ! off
     USE element_class_seg ! off
     USE types_enumeres

     USE Elements_Class !*
     USE Face_Class     !*

     IMPLICIT NONE
     !
     ! type cellule
     ! ************
     !
integer,parameter,private::Ndimparam=2, Nvarparam=7
     INTEGER, PARAMETER, PUBLIC :: pas_de_frontiere = -20071015

     INTEGER, PARAMETER, PUBLIC :: knd10 = SELECTED_REAL_KIND(18) !-- 80 bits

     TYPE cellule
        INTEGER                :: val
        TYPE(cellule), POINTER :: suiv
     END TYPE cellule
     TYPE cellule2d !-- pour les segments/facettes 2D (triangles) des �l�ments 3D
        INTEGER                  :: is2, is3 !< seront rang�s suivant l'ordre lexicographique
        INTEGER                  :: jt1, jt2 !< num�ro de l'�l�ment contenant la facette; une facette appartient � 2 elts au plus
        TYPE(cellule2d), POINTER :: suiv
     END TYPE cellule2d
     TYPE rpointer
        REAL, DIMENSION(: ),     POINTER :: Vp !< [[1:data%nvarphy]] Variables phys. au bord
     END TYPE rpointer

     ! P. JACQ : Ordre eleve
     TYPE SegCom
        INTEGER                :: Seg
        INTEGER                :: Elt
        INTEGER                :: Pt1
        INTEGER                :: Pt2
        REAL,    DIMENSION(3)  :: Coor
        TYPE(SegCom), POINTER  :: suivant
     END TYPE SegCom

     TYPE SegList
        INTEGER                :: DomV
        INTEGER                :: NbSnd,NbRcv
        TYPE(SegCom) , POINTER :: snd, rcv
        TYPE(SegList), POINTER :: suivant
     END TYPE SegList

     !********************************************************
     TYPE :: elements_ptr
        CLASS(element_str), POINTER :: e
     END type elements_ptr

     TYPE :: b_faces_ptr
        CLASS(face_str), POINTER :: b_f
     END type b_faces_ptr
     
     TYPE(elements_ptr), DIMENSION(:), ALLOCATABLE :: d_element
     TYPE(b_faces_ptr),  DIMENSION(:), ALLOCATABLE :: b_element
     !*********************************************************

     ! ===================================================================
     ! ===================================================================
     TYPE Donnees
        ! pilote.data
        CHARACTER(LEN = 1024)      :: RootName !< nom racine de presque tous les fichiers lus ou �crits

        !< valeurs possibles pour Data%GeoType
        !< "1D"
        !< "2Dplan"
        !< "2Dper"
        !< "2Daxisym"
        !< "2DaxisymX"
        !< "2DaxisymY"
        !< "3D"
        CHARACTER(LEN = 10) :: Geotype 
        INTEGER             :: sel_GeoType
        INTEGER             :: MeshOrder
        !< valeurs possibles pour Data%MeshType
        !<"AMDBA", "amdba"
        !<"AMDBA4", "amdba4"
        !<"MESH"
        !<"MESHEMC2", "meshemc2", "MeshEMC2"
        !<"MESHEMC2A", "meshemc2A", "MeshEMC2A"
        !<"MESHEMC2P", "meshemc2p", "MeshEMC2P"
        !<"GEOM3D", "geom3D"
        !<"GMSH", "gmsh"
        CHARACTER(LEN = 10) :: MeshType !< type du maillage (les valeurs autoris�es sont donn�es dans donmesh)
        INTEGER             :: sel_MeshType

        INTEGER :: Ndim !< vaut 2 ou 3, dimensionnalit� du probl�me trait�

        !! NumMeth
        LOGICAL :: Stationnaire !< r�solution d'un probl�me stationnaire, lu sur fichier (boucle_en_temps, fin de la boucle principale);
        LOGICAL :: Reprise !< reprise d'un job, lu sur fichier, appelle l'interface reprise_in
        LOGICAL :: Deformable !< maillage d�formable ou mobile
        LOGICAL :: Bz_en_2d !< donn�e sp�cifique MHD : pr�sence ou non d'un champ orthogonal au plan en 2D
        LOGICAL :: Loc !< Pas de temps local
        LOGICAL :: div_cleaning !< mhd, pour la correction de div(B)

        !< valeurs possibles pour Data%Modele
        !< "Euler"
        !< "EulerNS", "Navier-Stokes" (tout autre choix que Euler en mode /= Incomp, Multif envoie vers NavierStokes = .TRUE.)
        !< "IncompTherm" (par d�faut en cas makefile Eul* %%%%%%%%%%%% faux)
        !< "Incomp" (par d�faut implicite en makefile Incomp %%%%%%%%%% faux); tout autre choix que IncompTherm en mode Incomp
        !< "EulerMF" multi-fluide
        !< "EulerMFV" multi-fluide Visqueux
        !< "IdealMHD" MHD id�ale
        !< "FullMHD" MHD compl�te (r�sistive)
        CHARACTER(LEN = 50) :: Modele !< choix du sous-mod�le
        INTEGER             :: sel_Modele

        !< valeurs possibles pour Data%Approche
        !< "VertexCentered"
        !< "MultiDomaine"
        !< "CellCentered"
        CHARACTER(LEN = 20) :: Approche !< approche math�matique utilis�e
        INTEGER             :: sel_Approche

        !< valeurs possibles pour Data%Methode
        !< "EXP-RK1"
        !< "EXP-RK1-FAKE"
        !< "EXP-RK2"
        !< "EXP-RK2-FAKE"
        !< "EXP-RK3-FAKE"
        !< "EXP-RK4-FAKE"
        !< "EXP-RK3"
        !< "EXP-RK4"
        !< "EXP-PC2"
        !< "IMP-RK1"
        !< "IMP-RK1-RD"
        !< "IMP-EXP-RD"
        !< "IMPLIN-RK1"
        !< "IMPLIN-GEAR"
        !< "IMPLIN-GEAR-RD"
        !< "IMP-GEAR"
        !< "IMP-DCE"
        !< "IMP-GEAR-RD"
        !< "IMP-DCE-RD"
        !< "IMPLIN-RK1-RD"
        CHARACTER(LEN = 20) :: Methode !< m�thode temporelle (EXPlicite ou IMPlicite)
        INTEGER             :: sel_Methode

        !< valeurs possibles pour Data%Relax
        !< "Jacobi" : CALL Jacobi(COM, DATA, Mat, Var, Vecteur )
        !< "Gauss-Seid" : CALL GaussSeidel(COM, DATA, Mat, Var, Vecteur )
        !< "BiCgs" : CALL BiCGS(COM, DATA, Mat, Var, Vecteur )
        !< "BiCgsL-Gsd" : CALL BiCgsLGsd(COM, DATA, Mat, Var, Vecteur )
        !< "BiCgsL-Ilu" : CALL ILUMatrix(Mat ) CALL BiCgsLUpDo(COM, DATA, Mat, Var, Vecteur )
        !< "Gmres" : CALL Gmres(COM, DATA, Mat, Var, Vecteur )
        !< "GmresL" : CALL Gmres(COM, DATA, Mat, Var, Vecteur )
        !< "GmresR" : CALL Gmres(COM, DATA, Mat, Var, Vecteur )
        !< "GmresL-Gsd" : CALL GmresLGsd(COM, DATA, Mat, Var, Vecteur )
        !< "GmresR-Gsd" : CALL GmresRGsd(COM, DATA, Mat, Var, Vecteur )
        !< "GmresL-Ilu" : CALL ILUMatrix(Mat ) CALL GmresLUpDo(COM, DATA, Mat, Var, Vecteur )
        !< "GmresR-Ilu" : CALL ILUMatrix(Mat ) CALL GmresRUpDo(COM, DATA, Mat, Var, Vecteur )
        !< "GmresL-Ssor" : CALL SSORMatrix(Mat ) CALL GmresLUpDo(COM, DATA, Mat, Var, Vecteur )
        !< "GmresR-Ssor" : CALL SSORMatrix(Mat ) CALL GmresRUpDo(COM, DATA, Mat, Var, Vecteur )
        CHARACTER(LEN = 20) :: Relax !< m�thode de relaxation
        INTEGER             :: sel_Relax

        !< valeurs possibles pour Data%LoiEtat
        !< "UserDefined"
        !< "IG" : gaz incompressible
        !< "GP" : gaz parfait
        !< "SG" : gaz raide
        !< "SGRef" : gaz raide de r�f�rence
        !< "SGMF" : gaz raide multi-fluide
        !< "Eau" : 
        !< "VDW" : gaz Van Der Waals
        !< "PRSV" : gaz Peng Robinson
        !< "SW" : gaz Span Wagner
        CHARACTER(LEN = 20) :: LoiEtat !< choix de la loi d'�tat
        INTEGER             :: sel_LoiEtat

!!$        CHARACTER(LEN = 20) :: Fluid_Model
!!$        CHARACTER(LEN = 20) :: Turbulence_model

        !< valeurs possibles pour Data%LoiVisq
        !<"Constant" : EOSvisco=MUREF
        !<"Sutherland" : EOSvisco = visco_sutherland(MUREF, T)
        !<"Pant" : EOSvisco = visco_pant(T)
        !<"No" : EOSvisco = 0.0
        !<"Chung" : Loi de Chung qui dépend de T 
        CHARACTER(LEN = 20) :: LoiVisq !< loi pour la viscosit�
        INTEGER             :: sel_LoiVisq

        !< valeurs possibles pour Data%LoiTh
        !<"Constant" : EOSCondTh=LBDAREF
        !<"Prandtl" : d�pend aussi de LoiVisq
        !<"No" : EOSCondTh=0.0
        !<"Chung" : Loi de Chung qui dépend de rho et T (car de Cv)
        CHARACTER(LEN = 20) :: LoiTh !< loi pour la thermique
        INTEGER             :: sel_LoiTh
        INTEGER             :: Impre !< niveau de mise au point, pour contr�ler les impressions

        !< valeurs possibles pour Data%Icas
        !< 0=guillard_muronne ! cas Guillard Muronne)
        !< 1=laplace ! cas Laplace
        !< 3=perigaud ! cas Perigaud
        !< 4=perigaud2 ! cas Perigaud compl�t�
        !< 5=a_la_main ! initialisation des variables physiques par des constantes uniformes � la main, INOUT/Initialisation/Cas5
        !< 6=uniforme ! initialisation des variables physiques par des constantes uniformes sur tout le domaine
        !< 7=mhd_blast ! Balsara Spicer 1er pb
        !< 8=mhd_rotor ! Balsara Spicer 2�me pb
        !< 10=autre
        !< 20=karni_quirck ! Karni Quirck
        INTEGER :: Icas !< lu par DonNumMeth; controle l'initialisation des variables
        INTEGER :: Nnewton !< nombre d'it�rations de la m�thode de newton, pour imp_gear

        !< valeurs possibles pour Data%Iflux : euler         | eulnsvf              | multif | incomp : indiff�renci�
        !< 1  ! EulNarrow           | EulerElem + EulHLLC  |
        !< 2  ! EulLDA              |    "      + EulAcous | FluxAcoustiqueR5 mode1
        !< 3  ! EulBlend            |    "      + EulRELAX |
        !< 4  ! EulNarrow ++ BLOQU� |    "      + EulRELAC |
        !< 5  ! EulLDA ++ BLOQU�    |    "      + EulROE1  |
        !< 6  ! BLOQU�              |    "      + EulROE2  |
        !< 7  ! BLOQU�              | BLOQU�               |
        !< 10 ! EulNarrowOLD BLOQU� | BLOQU�               |
        !< 11 !                     | EulHLLC              | FluxHLLC
        !< 12 !                     | EulAcous             | FluxAcoustiqueR5 mode1
        !< 13 !                     | EulRELAX             |
        !< 14 !                     | EulRELAC             |
        !< 15 !                     | EulROE1              |
        !< 16 !                     | EulROE2              |
        !< 20 !                     |                      | FluxAcoustiqueR5 mode2
        !< 21 !                     |                      | FluxHLLC mode2
        !< 22 !                     |                      | FluxAcoustiqueR5 mode2
        ! "1 -> Narrow          (10 -> Narrow old)"
        ! "2 -> LDA"
        ! "3 -> LDA/Narrow      Blending"
        ! "4 -> Galerkin/Narrow Blending"
        ! "5 -> Galerkin/LDA    Blending"
        ! "6 -> Lax-Friedrichs Limite PSI           (en developpement, cedric)"
        ! "7 -> Lax-Friedrichs Limite PSI stabilise (en developpement, cedric)"
        INTEGER :: Iflux !< s�lecteur de m�thode pour calculer le flux

        !< valeurs possibles pour Data%Nordre
        !< 1
        !< 2 ! totalement d�centr�
        !< 3 ! Ordre 3 (th�orie monodimensionnelle scalaire sur convection, et plus ...)
        !< 4 ! FROMM
        !< 5
        INTEGER :: Nordre !< ordre du sch�ma spatial
        INTEGER :: Ipred !< no use (vaut 0, sauf schema explicite d'ordre 2)
        INTEGER :: RGrad ! Gradient recovery method

        REAL      :: Tmax !< temps maximal de la simulation
        REAL      :: dtMax !< pas de temps maximal
        INTEGER   :: ktmax !< nombre maximal d'it�ration
        INTEGER   :: kt0 !< num�ro de l'it�ration initiale
        REAL      :: CflMax !< CFL Maximale, lue sur fichier, employ�e par calcfl
        REAL      :: Mult !< multiplicateur du pas de temps pour une CFL adaptable, lu sur fichier, employ� par calcfl
        REAL      :: CFL_Phys !< initialis� dans Inputs.f90 (ou EulerNS/ModelInterfaces.f90), employ� par MODELCalPasDeTemps

        !< valeurs possibles pour Data%LoiCFL
        !< 0=clfmax ! data%cflmax
        !< 2=minmult ! MIN(data%mult*var*kt, data%cflmax)
        INTEGER  :: LoiCFl !< mode de calcul de la CFL variable
        INTEGER  :: Dt_Wave !< onde sur laquelle se baser pour le calcul du pas de temps en MHD+Instat_RD : 1 = c_fast, 2 = c_alfven, 3 = c_slow 

        INTEGER :: Nrelax !< nombre de relaxations de la m�thode de Jacobi (Lecture 107a)
        REAL    :: ResdLin !< residu minimal pour la m�thode de Gauss-Seidel (Lecture 108a)
        REAL    :: ResdSta !< residu minimal pour d�cider que l'on a atteint la solution en mode stationnaire
        INTEGER :: Mkrylov !< dimension des sous-espaces de Krylov (???%%)

        REAL, DIMENSION(:, : ), POINTER :: VarPhyInit !< [[1:Data%NVarPhy, 1:MAXLOGCELL]] initialisation des variables physiques (initialis� par ModelDonData (qui, suivant les modeles, est direct ou interface %%%%); maxlogcell vaut 1 ou 2 dans les cas tests

        REAL :: LRef !< normalisation des coordonn�es du maillage (longueur de r�f�rence)
        REAL :: VRef !< %% no use
        REAL :: BRef !< champ magn�tique (norme)
        REAL :: PsiRef !< correcteur pour la contrainte div(B) == 0
        REAL :: RhoRef !< %% no use
        REAL :: Tref !< temp�rature de r�f�rence pour MODELS/EulerNS/VertexCentered/Turbulence.f90/viscoy

        REAL :: AoA   ! Angle of attack
        REAL :: AoSS  ! Side-slip angle 

        REAL :: Tcrit !< temp�rature critique, pour les calculs en gaz réels
        REAL :: Tinf  !< temperature infinie, gaz denses, T_oo = T(rho_oo,P_oo)
        REAL :: AcentricFactor !< Facteur acentrique (modèles Gaz PRSV et SW)

        REAL :: P_oo_turb     !< Pression totale infinie pour entrée turbine
        REAL :: rho_oo_turb   !< Densité totale infinie pour entrée turbine
        REAL :: alpha_oo_turb !< angle d'attaque infini pour entrée turbine
        REAL :: r_turb        !< rapport de pression : entrée / sortie  pour sortie turbine
        REAL :: P_s_turb      !< Pression de sortie de la turbine
       !REAL :: P_t_in        !< Total pressure inflow
       !REAL :: T_t_in        !< Total temperature inflow
       !REAL :: alpha_in      !< velocity angle inflow

        REAL, DIMENSION(3) :: Gravite !< employ� par ContribGRAVITE dans les cas incompressible et biphasique

        INTEGER  :: Ifres !< fr�quence d'�criture des r�sultats
        INTEGER  :: Ifre !< fr�quence d'affichage des r�sultats � l'�cran
        INTEGER  :: Ifrel !< %% no use

        !! D�duit
        CHARACTER(LEN = 10), DIMENSION(: ), POINTER  :: VarNames !< [[1:Data%NVarPhy]] noms des variables du modele traite :
                                                        !rho, u, v, w, ei ou epsi, c, t, a1, a1*rho1, a2*rho2, p, t1, t2, phi; 
        LOGICAL :: NavierStokes 
        INTEGER :: Nvar 
        !< par exemple pour EulerNS
        !< 1 ="rho"
        !< 2 =" u "
        !< 3 =" v "
        !< 1+data%ndim =" w "
        !< Data%Nvar   =" ei"
        !< 1+Data%Nvar =" p "
        !< 2+Data%Nvar =" C " !-- vitesse du son non normalis�e (voir gp_son)
        !< 3+Data%Nvar =" T "
        INTEGER :: Nvart !< nombre de variables suppl�mentaires, d�pend du mod�le
        INTEGER :: NVarPhy !< somme de Nvar et NvarT

        CHARACTER(LEN = 80)   :: PbName !< nom du probl�me � r�soudre (%%%%-pm : documenter les emplois)
        CHARACTER(LEN = 1024) :: OutputName !< === RootName

        REAL                     :: ExpEntr !< pas initialise %%%%%% mais pass� en argument � CALL Residu* dans des modes exotiques
        REAL                     :: MachRef !< nombre de mach ? (calcul� par nComp/ModelInterfaces.f90 par ex.)
        
        REAL                     :: Phydro !< une variable lue pour le cas incompressible (ModelU2VpPt) %%
        
        REAL, DIMENSION(:), POINTER :: Residus0 ! Initial L2 residual
        REAL, DIMENSION(:), POINTER :: Res_oo0  ! Initial L_oo residual

        !! a lire dans  Climites_RootName.data
        !! ***********************************
        INTEGER                                :: Nbord !< nombre de types de bords diff�rents
        INTEGER,        DIMENSION(: ), POINTER :: FacMap !< [[1:data%nbord]], initialis� par modeldonclimites � partir de lgc, num�ro de logique de cl
        TYPE(rpointer), DIMENSION(: ), POINTER :: Fr !< [[1:1:MaxLogFac]][[1:data%nvarphy]] variables Physiques IMPOSEES au bord
        LOGICAL                                :: tension_surface !< la tension de surface est utilis�e en multi-fluide
        LOGICAL                                :: exacte !< solution initiale exacte, pour estimation d'erreur

        INTEGER :: N_wall_points
        INTEGER :: N_wall_faces
        INTEGER :: N_boundary_points

     END TYPE Donnees
     TYPE(Donnees)   :: Data

     TYPE Maillage
        INTEGER                          :: Npoint !< nombre de sommets du maillage, lu sur fichier--> a partir de ReadMesh, mais cr�� dans GeomGraph/MeshOrderUpdate
        INTEGER                          :: Ndofs  !< nombre de dofs dans le maillage. meme lieu que Npoint
        INTEGER                          :: Nelement !< nombre d'�l�ments du maillage, lu sur fichier
        INTEGER                          :: NfacFr   !< nombre total de segments/facettes frontiere
        INTEGER                          :: NdegreMax !< degr� max d'un �l�ment
        INTEGER                          :: NSommetsMax !< nbde de sommet max d'un �l�ment
        INTEGER                          :: Ndim !< 2 ou 3, en fonction de l'appel de la m�thode de lecture du maillage
        INTEGER                          :: Nfac !< nombre total de segments/facettes

        TYPE(element_seg),DIMENSION(:)  ,POINTER:: fr
        TYPE(element)    ,DIMENSION(:)  ,POINTER :: maill
     END TYPE Maillage

     TYPE MeshDef
        !! Maillage composante volumique
        INTEGER                          :: Npoint !< nombre de points du maillage, lu sur fichier
        INTEGER                          :: Nelemt !< nombre d'�l�ments du maillage, lu sur fichier
        INTEGER                          :: Nfac !< nombre total de segments/facettes
        INTEGER                          :: Ndim !< 2 ou 3, en fonction de l'appel de la m�thode de lecture du maillage
        INTEGER                          :: NOrdre !! Ordre actuel du maillage (on peut l'enrichir ou l'apauvrir)
        INTEGER                          :: NdegreMax !< degr� max d'un �l�ment
        
        INTEGER                            :: NSommetsMax
        INTEGER, DIMENSION(: ),    POINTER :: NSommets !< [[nelemt]] -> nSommetsMax :: nombre de sommets port�s par l'�l�ment
        INTEGER, DIMENSION(: ),    POINTER :: Ndegre   !< [[nelemt]] -> nDegresMax :: nb. de degr�s de libert� port�s par l'�l�m.
        INTEGER, DIMENSION(: ),    POINTER :: EltType  !< [[nelemt]] -> Type de l'element voir les types et_*
        INTEGER, DIMENSION(:, : ), POINTER :: Nu     !< [[nDegresMax, nelemt]] -> ncells -> degr�s de libert� de l'�l�ment
        INTEGER, DIMENSION(:, : ), POINTER :: NuFac !< pour des t�traedres, [[3, nfac]]; sommets de la facette courante; employe pour le mod�le Lagrangien
        INTEGER, DIMENSION(:, : ), POINTER :: EleVois !< pour des t�traedres, [[2, nfac]] : les 1 ou 2 t�tra�dres voisins la facette courante; employ� en particulier par le mod�le Lagrangien
        INTEGER, DIMENSION(: ),   POINTER :: LogElt !< [[nelemt]], contrainte de l'�l�memt
        INTEGER, DIMENSION(: ),   POINTER :: LogPt  !< [[ncells]], contrainte du point, max sur les �l�ments contenant le point; est initialis� aussi par modeldondata, et vaut alors 0 ou 1, en fonction de la g�om�trie de l'�l�ment et de data%icas
        INTEGER, DIMENSION(: ),   POINTER :: LogPer  !< [[ncells]] -> ncells corresp. entre deg. de lib. pour des g�om. p�riod.
        REAL, DIMENSION(:, : ), POINTER :: Coor !< [[ndim, ncells]] -> R, coordonn�es des points, dans R^2 ou R^3
        REAL, DIMENSION(:, : ), POINTER :: Coon !< [[ndim, ncells]] -> R coor-Next pour les maillages mobiles
        REAL, DIMENSION(: ),   POINTER :: VolEl !< [[nelemt]] -> R surface/volume de chaque �l�ment (geomvol2dgeneric, GEOMvol3D par ex.)
        REAL, DIMENSION(: ),   POINTER :: Vols !< [[ncells]] -> R surface/volume associ� � chaque degr� de libert� (geomvol2dgeneric fait la somme pond�r�e par 1/degr� sur les points d'un �l�ment, idem pour GEOMvol3D)
        REAL, DIMENSION(:, : ), POINTER   :: Vno !< [[ndim, nsegmt/nfac]] -> R, coord. du vecteur normal � un segment/facette
        REAL, DIMENSION(: ), POINTER     :: norme !< [[nsegmt/nfac]] -> R, norme du vecteur normal
        REAL, DIMENSION(:, :, : ), POINTER :: Vnod !< no use at all %%, sauf dans des m�thodes plac�es en commentaire
        REAL, DIMENSION(:, : ),   POINTER :: CentrEl !< [[ndim, nelemt]] -> R, Coordonn�es des barycentres des �l�ments
        !! Maillage composante frontiere
        INTEGER                          :: NfacFr !< nombre de segments/facettes fronti�res

        !< valeurs possibles pour Data%LogFr
        !< 1=dirichlet                      ! Dirichlet
        !< 2=neumann                        ! ??? Neumann ???
        !< 19=paroi_adh_adiab               ! Paroi adh�rente adiabatique
        !< 20=paroi_adh_adiab_flux_nul      ! Paroi adh�rente adiabatique � dite flux nul
        !< 21=paroi_glissante               ! paroi glissante
        !< 22=symetrie_plane                ! Sym�trie plane
        !< 23=paroi_lagrange                ! paroi lagrange
        !< 25=paroi_misc                    ! soit temp�rature impos�e, soit flux thermique nul suivant les mod�les
        !< 26=paroi_pression_donnee         ! Paroi � pression impos�e
        !< 27=paroi_temp_donnee             ! Paroi � Temperature impos�e
        !< 30=symetrie_lagrange             ! Symetrie Lagrange
        !< 35=paroi_v_et_p_donnes           ! Vitesse et pression impos�ss
        !< 36=paroi_v_et_t_donnes           ! Vitesse et Temperature impos�s
        !< 37=paroi_p_et_t_donnes           ! Paroi � pression et temperature impos�es
        !< 38=paroi_diric_vpt_donnes        ! Dirichlet sur vitesse, pression et temperature (impos�es)
        !< 40=gradient_nul_exterieur        ! gradient nul ./. ext�rieur
        !< 50=gradient_nul_interieur        ! gradient nul ./. int�rieur
        !< 51=sortie_subsonique             ! sortie subsonique
        !< 52=sortie_mixte                  ! sortie mixte super/subsonique
        !< 54=flux_interne                  ! Flux interne utilise en sortie ou entree
        !< 60=sortie_vitesse_normale        ! sortie mixte avec vitesse normale
        !< 61=sortie_pression_donnee        ! sortie pression impos�e
        !< 64=flux_de_steger                ! Flux de Steger warming modifie en sortie ou entree
        !< 75=incomp_v_donne                ! Vitesse et Profil de vitesse impos�s dans la version incomp
        !< 90=periodique                    ! Conditions aux limites p�riodiques
        !< 91=entrée turbine                ! Conditions entrée turbine
        !< 92=sortie turbine                ! Conditions sortie turbine
        !< 154=ns_steger_warming            ! ...
        INTEGER, DIMENSION(: ),   POINTER :: LogFr !< [[nfacfr]], contraintes sur les segments/facettes fronti�res (rq. 0 <=> frontiere interbloc);
        !-- tient 2 r�les %%%% : d'abord une num�rotation, puis une indirection sur le num�ro de la logique de la C.L. (lue par modeldonclimites)
        INTEGER, DIMENSION(: ),    POINTER :: LogFrReel !< [[nfacfr]]; copie de logfr, si -DADAPT
        INTEGER, DIMENSION(: ),    POINTER :: NumFacFr !< recopie de NumFac pour ORDRE_ELEVE
        INTEGER, DIMENSION(: ),    POINTER :: NumFac !< [[nfacfr]] -> nsegmt/nfac, chang. de num�rotation : bord -> segment/facette
        INTEGER, DIMENSION(: ),    POINTER :: NumElemtFr !< [[nfacfr]] -> nelemt : t�tra�dres dont une facette est fronti�re
        INTEGER, DIMENSION(:, : ), POINTER :: NsfacFr  !< [[ndim, nfacfr]] -> ncells : degr�s de lib. du segment/facette frontiere
        INTEGER, DIMENSION(:, : ), POINTER :: Nutv !< [[2, nsegmt]] -> nelemt; �l�ment(s) contenant le segment, en 2D; TetVois en 3D
        REAL,    DIMENSION(:, : ), POINTER :: VnoFr !< [[ndim, nfacfr]] -> R taille des segments/facettes fronti�res si calcul� par calsurfaces2d, puis vecteur normal par GEOMMvvnoVertexCentered2D,
        REAL,    DIMENSION(:,:,:), POINTER :: CoordFr

        !! graphe du maillage
        INTEGER                          :: NCoefMatEx !< SUM( Mesh%NbVois )
        INTEGER, DIMENSION(: ),   POINTER :: JvSeg !< [[ncoefmat]] -> nsegmt/nfac : segment/facette reliant 2 deg. de lib. voisins (GrapheDesVois)
        INTEGER, DIMENSION(: ),   POINTER :: JvPnt !< [[ncoefmatex]] -> ncells : points voisins d'un point (point de d�part : jposi)
        INTEGER, DIMENSION(: ),   POINTER :: JPosi !< [[npoint+1]] -> ncoefmat, allou� par GraphMatVertexCentered, calcul� par GrapheDesVois : points de d�part dans les listes de liste JvPnt, JvSeg
        INTEGER, DIMENSION(: ),   POINTER :: IvPos !< [[ncoefmatex]] -> ncoefmat, place dans le stockage ligne des �lts de la col. k
        REAL, DIMENSION(: ),   POINTER :: Gs => NULL() !< [[npoint]] -> R, quantit� sp�ciale (%%%%) d'un degr� de libert�
        REAL, DIMENSION(: ),   POINTER :: h => NULL() !< [[npoint]] -> R, minimum des rayons approxim�s, par geomvol2dgeneric
        !! graphe du maillage
        INTEGER                          :: Nsegmt !< nombre d'ar�tes/facettes du maillage (calcul� apr�s lecture du maillage)
        INTEGER, DIMENSION(: ),   POINTER :: NbVois !< [[npoint]] -> # nombre de voisins d'un sommet
        INTEGER, DIMENSION(: ),   POINTER :: NbVoit !< %% no use at all
        INTEGER, DIMENSION(:, : ), POINTER :: Nubo !< [[2, nsegmt]] -> npoint; points d'un segment, dans l'ordre croissant (recopie de la hashtable)
        !< l'indice 1 contient en 1 le num�ro du point, en 2 celui de son voisin dans le parcours de l'�l�ment
        INTEGER, DIMENSION(:, : ), POINTER :: Nuseg !< [[3/4/6, nelemt]] -> nsegmt : segments de l'�l�ment
        INTEGER, DIMENSION(:, : ), POINTER :: NuFacette  !< [[nface/elemt nelemt]] numeros des faces de l'element
        INTEGER, DIMENSION(:, : ), POINTER :: TetSegmt !< %% no use at all
        INTEGER, DIMENSION(:, : ), POINTER :: Nusv !< [[2, nsegmt]] les un ou deux sommets tel qu'avec le segment, ils forment un �l�ment (2D)
        REAL, DIMENSION(: ),   POINTER :: Sigma !< [[nsegmt/nfac]], en cas de maillage mobile : vitesse de d�placement de l'interface
        REAL, DIMENSION(: ),     POINTER :: SigmaFr !< [[nfacfr]], en cas de maillage mobile : vitesse de d�placement pour les interfaces du bord

       

        INTEGER :: NCells
       
        REAL, DIMENSION(:), POINTER :: dmin

        INTEGER, DIMENSION(:), POINTER :: b_ovlp
        
     END TYPE MeshDef

     !--------------------------------------------------
     TYPE :: con_t
        INTEGER, DIMENSION(:), ALLOCATABLE :: cn_ele
        INTEGER, DIMENSION(:), ALLOCATABLE :: cn_bele
     END TYPE con_t
     TYPE(con_t), DIMENSION(:), ALLOCATABLE :: Connect

     INTEGER, DIMENSION(:,:), ALLOCATABLE :: per_conn
     !---------------------------------------------------

     !--------------------------------------------------
     TYPE :: color_str
        INTEGER, DIMENSION(:), ALLOCATABLE :: NU
     END TYPE color_str
     TYPE(color_str), DIMENSION(:), ALLOCATABLE :: ColorMap
     !---------------------------------------------------



     TYPE MeshCom
        CHARACTER(LEN = 24)              :: GroupName !< GroupName = Nom du groupe de la tache courante
        INTEGER                          :: NTasks !< NTasks = nombre total de taches
        LOGICAL                          :: GetNTasks
        INTEGER ::  Ngroup
        INTEGER ::  Iencod
        INTEGER                          :: Me  !< Me = num�ro du processeur courant (processeur ou t�che ?)
        INTEGER ::  Mpe
        INTEGER                          :: MyTid  !< MyId = identification de la tache courante
        INTEGER ::  LNiveau
        INTEGER ::  PeInj
        REAL                             :: Tcom
        REAL                             :: Twait
        REAL                             :: TechgSolu !< temps total, pour chaque tache, pass� � �changer des solutions
        REAL                             :: Treduce !< temps total, pour chaque tache, pass� dans les r�ductions
        REAL                             :: TechgGrad !< temps total, pour chaque tache, pass� � �changer des gradients
        INTEGER, DIMENSION(: ),   POINTER :: Tids !< [[0:ntask-1]] tableau pour MPI, taille NTasks, 0 sauf pour Me, o� cela vaut Me
        INTEGER, DIMENSION(: ),   POINTER :: MapDom !< [[1:NTasks]], tableau donnant pour la tache <n0> le nombre <n0>-1
        INTEGER, DIMENSION(: ),   POINTER :: InvMap !< [[0:NTasks-1]], tableau donnant pour la tache <n0> le nombre <n0>+1
        INTEGER, DIMENSION(: ),   POINTER :: MeInGr !< [[1:NTasks]], 1 -> num�ro de la t�che courante
        INTEGER, DIMENSION(: ),   POINTER :: Position !< [[1:Ntasks]]; num�ro du i�me sous-domaine voisin
        INTEGER                          :: Ndomv !< Nombre de sous-domaines voisins du domaine courant
        INTEGER, DIMENSION(: ),   POINTER :: Jdomv !! -- [[1:Ndomv]]
        INTEGER, DIMENSION(: ),   POINTER :: NEsnd0, NErcv0 !! -- [[1:Ndomv]]
        INTEGER, DIMENSION(:, : ), POINTER :: ELsnd0, ELrcv0 !! -- [[1:Nrcv0Max, 1:Ndomv]]
        INTEGER, DIMENSION(: ),   POINTER :: NEsnd1, NErcv1 !! -- [[1:Ndomv]]
        INTEGER, DIMENSION(:, : ), POINTER :: ELsnd1, ELrcv1 !! -- [[1:Nrcv1Max, 1:Ndomv]]
        INTEGER, DIMENSION(: ),   POINTER :: NPsnd2 !< [[1:ndomv]], nombre de points emetteurs pour un sous-domaine
        INTEGER, DIMENSION(: ),   POINTER :: NPrcv2 !< [[1:ndomv]], nombre de points recepteurs pour un sous-domaine
        INTEGER, DIMENSION(:, : ), POINTER :: PTsnd2 !< [[1:nsnd2max, 1:ndomv]] : points emetteurs pour un sous-domaine (permutation)
        INTEGER, DIMENSION(:, : ), POINTER :: PTrcv2 !< [[1:nrcv2max, 1:ndomv]] : points recepteurs pour un sous-domaine (permutation)
        INTEGER                          :: NPsnd2tot !< nombre total de points emetteurs niveau 2
        INTEGER                          :: NPrcv2tot !< nombre total de points recepteurs niveau 2
        INTEGER                          :: MaxLengthSnd0
        INTEGER                          :: MaxLengthRcv0
        INTEGER                          :: MaxLengthSnd1
        INTEGER                          :: MaxLengthRcv1
        INTEGER                          :: MaxLengthSnd2
        INTEGER                          :: MaxLengthRcv2
        INTEGER                          :: Nsnd0Max, Nrcv0Max
        INTEGER                          :: Nsnd1Max, Nrcv1Max
        INTEGER                          :: Nsnd2Max, Nrcv2Max
        INTEGER, DIMENSION(: ),   POINTER :: RMap !< no use at all
        INTEGER, DIMENSION(: ),   POINTER :: Niveau
        INTEGER, DIMENSION(: ),   POINTER :: MapIs, NivIs
        INTEGER, DIMENSION(: ),   POINTER :: MapJt, NivJt
        INTEGER, DIMENSION(: ),   POINTER   :: Request !< [[1:2*ndomv]]
        INTEGER, DIMENSION(:, : ), POINTER   :: Stat !< [[1:MPI_STATUS_SIZE, 1:2*NdomV]]
        REAL, DIMENSION(:, : ), POINTER   :: SndData !< [1:Nvar*MaxLengthSnd, NdomV]]
        REAL, DIMENSION(:, : ), POINTER   :: RcvData !< [1:Nvar*MaxLengthRcv, NdomV]]

#ifdef ORDRE_ELEVE
        INTEGER, DIMENSION(:)  , POINTER :: SegPerm                  ! [1:Mesh%Npoint]
#endif

#ifdef MURGE
        INTEGER, DIMENSION(:, : ), POINTER   :: MURGEGatherRcvNum !< [1:Nvar*MaxLengthRcv,NdomV]] (proc 0)
        INTEGER, DIMENSION(:)    , POINTER   :: MURGEGatherNbRcv !< [NdomV] (proc 0)
        REAL   , DIMENSION(:)    , POINTER   :: MURGEGatherSnd !< [1:Nvar*MaxLengthSnd]]
        REAL   , DIMENSION(:, : ), POINTER   :: MURGEGatherRcv !< [1:Nvar*MaxLengthRcv, NdomV]] (proc 0)
#endif
     END TYPE MeshCom

     TYPE variables
        INTEGER                            :: Nvar    !! nobre de variables
        INTEGER                            :: NCells  !< === npoint, sauf dans le cas ordre �lev�
        INTEGER                            :: NCellsIn !< === var%NCells, pour l'instant
        INTEGER                            :: NCellsSnd !< === 0 si sequentiel, Com%NPsnd2tot en parallele : nombre de variables qui n�cessitent synchronisation par �change de niveau 2
        REAL, DIMENSION(: ),    POINTER :: CellVol !< [[1:npoint]]; === mesh%vols; initialis� par main
        REAL, DIMENSION(: ),    POINTER :: DtLoc !< [[1:Var%Ncells]]
        REAL, DIMENSION(:, : ), POINTER :: Ua !< [[1:Data%Nvar, 1:Var%NCells]] : Variables math�matiques ("conservatives") actuelles
        REAL, DIMENSION(:, : ), POINTER :: Vp !< [[1:Data%NvarPhy, 1:Var%NCells]] : Variables physiques (d�finies par chaque modele)
      REAL, DIMENSION(:, : ),    POINTER :: Vp_0=>NULL()
        REAL, DIMENSION(:,:,:), POINTER :: D_UA ! [1:Ndim, 1:Nvar, 1:NCells] : Gradient of the conservative variables
        REAL, DIMENSION(:,:,:), POINTER :: D_UA_b !
        REAL, DIMENSION(:, : ), POINTER :: Initialisation !< [[1:Data%NvarPhy, 1:Var%NCells]] : copie de Vp � kT=0
        REAL, DIMENSION(:, : ), POINTER :: Erreur !< [[1:Data%NvarPhy, 1:Var%NCells]] : Vp - Initialisation
        REAL, DIMENSION(:, : ), POINTER :: Flux !< [[1:Data%Nvar, 1:Var%NCells]] : flux des variables math�matiques � travers les segments/facettes

        REAL, DIMENSION(:),       POINTER :: Re_loc

        REAL, DIMENSION(:),   POINTER :: Cp
        REAL, DIMENSION(:),   POINTER :: Cf
        REAL, DIMENSION(:,:), POINTER :: C_loc

REAL, DIMENSION(:),       POINTER :: p_nu, d_nu, e_nu

        REAL                               :: Resid_1
        REAL                               :: Resid_2
        REAL                               :: time_loop

        REAL, DIMENSION(:), POINTER :: Residus ! L2 Residual
        REAL, DIMENSION(:), POINTER :: Res_oo  ! L_oo Residual
        
        INTEGER                            :: kt  !< num�ro de l'iteration
        REAL                               :: dt !< pas de temps courant
        REAL                               :: Temps !< temps courant
        REAL                               :: cfl !< C.F.L.
        REAL, DIMENSION(:, : ),    POINTER :: source_instat
        REAL                               :: dt_phys  !< le pas de temps physique, en instationnaire
        REAL, DIMENSION(2)                 :: poids !< rapport des pas de temps successifs (cas instationnaire %%)
        REAL, DIMENSION(: ),       POINTER :: poids_flux !! poids de discr�tisation en temps sur les flux (cas instationnaire)
        REAL, DIMENSION(:, : ),    POINTER :: ua_0 !< [[1:Data%Nvar, 1:Var%Ncells]] (si instationnaire avec r�sidu); solution au pas de temps pr�c�dent
        REAL, DIMENSION(:, : ),    POINTER :: ua_1 !< [[1:Data%Nvar, 1:Var%Ncells]] (si instationnaire avec r�sidu); solution 2 pas de temps avant
        REAL                               :: TFillTot !< === 0.0
        REAL                               :: TsolveTot !< === 0.0
 
        !! Donc il y a un swap � la fin, c'est fait � la fin de MODEL/EulNSRD/JacEul.f90

        REAL, DIMENSION(:, : ),    POINTER :: h 
        LOGICAL                            :: avec_h
         
        INTEGER                            :: TotalSolveTime

        REAL :: CL, CD_P, CD_V
          
     END TYPE variables

     TYPE Matrice
        INTEGER, DIMENSION(: ),       POINTER :: JvCell !< [[ncoefmat]] -> ncells, liste de listes de points, voisins par un m�me �l�ment, points de d�part JPosi; construit par GraphMatVertexCentered, reconstruit (%%) par GraphMatAddDiag
        INTEGER, DIMENSION(: ),       POINTER :: JPosi !< [[npoint+1]], %% il a d�j� un JPosi ailleurs; joue le m�me r�le que pour le maillage (relation par segment), mais dans la matrice (relation par �l�ment)
        INTEGER, DIMENSION(: ),       POINTER :: IvPos !< [[ncoefmat]], �quivalent dans mesh%%
        INTEGER, DIMENSION(: ),       POINTER :: IDiag !< [[npoint]]; calcul� par GraphMatAddDiag; pour chaque point, donne la position de la diagonale correspondante dans 1..ncoefmat
        REAL, DIMENSION(:, :, : ),  POINTER  :: Vals !< [[blksize, blksize, mat%ncoefmat]]; valeurs de la matrice
        REAL, DIMENSION(:, :, : ),  POINTER  :: ILUVals !< [[blksize, blksize, mat%ncoefmat]]; d�composition LU de la matrice, si demand�
        INTEGER                              :: NNtot !< === npoint
        INTEGER                              :: NNin !< en cas de methode implicite, npoint
        INTEGER                              :: NNsnd !< === var%ncellssnd === Com%NPsnd2tot
        INTEGER                              :: blksize !< === data%nvar (par le program main)
        INTEGER                              :: NcoefMat !< nombre total de voisins (pour chaque point) au sens �l�ment (graphmatvertexcentered)

#ifdef MURGE
        INTEGER                               :: NMurge          !< NNtot
        INTEGER                               :: NMurgeNzeros    !< 
        INTEGER, DIMENSION(: ),       POINTER :: nodelist       !< Noeuds Locaux -> Noeuds Globaux
        INTEGER, DIMENSION(: ),       POINTER :: PosNodelist    
        REAL   , DIMENSION(: ),       POINTER :: MURGErhs
        INTEGER, DIMENSION(:) ,       POINTER :: mapnodelist   ! num nodelist 'local' (a cause d'un bug)-> odelist de MURGE
#endif         
     END TYPE Matrice
     TYPE(Matrice) :: Mat_fb

     !***********************************************************************
     !                -- variables globales publiques : indirections dans les VP*
     !***********************************************************************

     !-- la valeur debordement forcera un d�bordement de tableau si emploi sans initialisation correcte
     INTEGER, PRIVATE, PARAMETER :: debordement = 1000000000
     !< les variables suivantes sont initialis�es par ModelInitSize dans le ModelInterfaces.f90 adhoc pour le mod�le choisi
     !< les i_*_vp concernent les Vp, Ua1, Ua2, VpFrl, Uint
     !< les i_*_ua concernent les Ua, Flux
     !< ces variables ne sont pas toutes initialis�es � mod�le donn�, par exemple i_ar1_vp ne concerne que le multi-fluide
     INTEGER, PUBLIC, SAVE :: i_rho_vp = debordement !< rho, densite
     INTEGER, PUBLIC, SAVE :: i_vi1_vp = debordement !< u, composante 1 de la vitesse
     INTEGER, PUBLIC, SAVE :: i_vi2_vp = debordement !< v, composante 2 de la vitesse
     INTEGER, PUBLIC, SAVE :: i_vid_vp = debordement !< v ou bien w, derniere composante de la vitesse
     INTEGER, PUBLIC, SAVE :: i_eps_vp = debordement !< ei, epsilon, energie interne
     INTEGER, PUBLIC, SAVE :: i_pre_vp = debordement !< p, pression
     INTEGER, PUBLIC, SAVE :: i_son_vp = debordement !< c, vitesse du son
     INTEGER, PUBLIC, SAVE :: i_tem_vp = debordement !< t, temperature
     INTEGER, PUBLIC, SAVE :: i_alp_vp = debordement !< alpha
     INTEGER, PUBLIC, SAVE :: i_ar1_vp = debordement !< alpha * rho1
     INTEGER, PUBLIC, SAVE :: i_ar2_vp = debordement !< alpha * rho2
     INTEGER, PUBLIC, SAVE :: i_tm1_vp = debordement !< temperature 1
     INTEGER, PUBLIC, SAVE :: i_tm2_vp = debordement !< temperature 2
     INTEGER, PUBLIC, SAVE :: i_mss_vp = debordement !< masse (dans le cas lagrangien)
     INTEGER, PUBLIC, SAVE :: i_nuts_vp = debordement !< viscosite turbulence (dans le cas turbulence)
!    INTEGER, PUBLIC, SAVE :: i_mur_vp = debordement !< distance au mur (dans le cas turbulence)

     INTEGER, PUBLIC, SAVE :: i_bx_vp = debordement !< champ magnetique en X
     INTEGER, PUBLIC, SAVE :: i_by_vp = debordement !< champ magnetique en Y
     INTEGER, PUBLIC, SAVE :: i_bz_vp = debordement !< champ magnetique en Z
     INTEGER, PUBLIC, SAVE :: i_psi_vp = debordement !< correction pour div(B)=0
     INTEGER, PUBLIC, SAVE :: i_ca_vp = debordement !< vitesse d'onde d'Alfven
     INTEGER, PUBLIC, SAVE :: i_cf_vp = debordement !< vitesse d'onde magnetoacoustique rapide (fast)
     INTEGER, PUBLIC, SAVE :: i_cs_vp = debordement !< vitesse d'onde magnetoacoustique lente  (slow)
     INTEGER, PUBLIC, SAVE :: i_nrm_vp = debordement !< norme de la vitesse
     INTEGER, PUBLIC, SAVE :: i_pma_vp = debordement !< pression magn�tique
     INTEGER, PUBLIC, SAVE :: i_divB_vp = debordement !< div(B) moyen (par dual cell)

     ! Ajout pour la MHD

     INTEGER, PUBLIC, SAVE :: i_bm1_vp = debordement !< Bx !-- %%%%%%%%%% redondant avec i_bx_vp ???
     INTEGER, PUBLIC, SAVE :: i_bm2_vp = debordement !< By
     INTEGER, PUBLIC, SAVE :: i_bmd_vp = debordement !< Bz
     INTEGER, PUBLIC, SAVE :: i_sns_vp = debordement !< senseur de choc

     INTEGER, PUBLIC, SAVE :: i_rho_ua = debordement !< rho, densit�
     INTEGER, PUBLIC, SAVE :: i_vi1_ua = debordement !< u, composante 1 de la vitesse
     INTEGER, PUBLIC, SAVE :: i_vi2_ua = debordement !< v, composante 2 de la vitesse
     INTEGER, PUBLIC, SAVE :: i_vid_ua = debordement !< v ou bien w, derni�re composante de la vitesse
     INTEGER, PUBLIC, SAVE :: i_pre_ua = debordement !< p, pression
     INTEGER, PUBLIC, SAVE :: i_tem_ua = debordement !< t, temp�rature

     INTEGER, PUBLIC, SAVE :: i_alp_ua = debordement !< alpha
     INTEGER, PUBLIC, SAVE :: i_ar1_ua = debordement !< alpha * rho1
     INTEGER, PUBLIC, SAVE :: i_ar2_ua = debordement !< alpha * rho2
     INTEGER, PUBLIC, SAVE :: i_rv1_ua = debordement !< rho * u
     INTEGER, PUBLIC, SAVE :: i_rv2_ua = debordement !< rho * v
     INTEGER, PUBLIC, SAVE :: i_rvd_ua = debordement !< rho * v ou bien rho * w
     INTEGER, PUBLIC, SAVE :: i_ren_ua = debordement !< rho * energie totale
     INTEGER, PUBLIC, SAVE :: i_muts_ua = debordement !< rho * viscosite turbulence

     ! Ajout pour la MHD

     INTEGER, PUBLIC, SAVE :: i_bm1_ua = debordement !< Bx
     INTEGER, PUBLIC, SAVE :: i_bm2_ua = debordement !< By
     INTEGER, PUBLIC, SAVE :: i_bmd_ua = debordement !< Bz
     INTEGER, PUBLIC, SAVE :: i_psi_ua = debordement !< Bz

     !***********************************************************************
     !                                     -- variables globales publiques
     !***********************************************************************

     INTEGER, PARAMETER, PUBLIC :: ios_mxl = 512 !< longueur maximale de la chaine pour un code d'erreur d'E/S
     LOGICAL, PUBLIC, SAVE :: ios_debug = .FALSE. !< mise au point pour la reconnaissance des erreurs d'E/S

     !***********************************************************************
     !                                     -- interfaces
     !***********************************************************************

     PUBLIC :: tester_nan0, tester_nan1, tester_nan2, tester_nan3, tester_nan4, tester_nan5, tester_nan6

     INTERFACE
        FUNCTION isnan(xx) RESULT(res) !-- BIND(C,NAME="isnan_")
          REAL, INTENT(IN) :: xx
          INTEGER :: res
        END FUNCTION isnan
        SUBROUTINE printnan(xx) !-- BIND(C,NAME="printnan_")
          REAL, INTENT(IN) :: xx
        END SUBROUTINE printnan
        FUNCTION isinf(xx) RESULT(res) !-- BIND(C,NAME="isinf_")
          REAL, INTENT(IN) :: xx
          INTEGER :: res
        END FUNCTION isinf
        SUBROUTINE get_set_prec(new_prec, new_round, old_stat) !-- BIND(C,NAME="get_set_prec_")
          INTEGER, INTENT(IN) :: new_prec, new_round
          INTEGER, INTENT(OUT) :: old_stat
        END SUBROUTINE get_set_prec
     END INTERFACE

     !***********************************************************************
     !                                     -- variables globales privates
     !***********************************************************************

     LOGICAL, PRIVATE, SAVE :: init = .FALSE.

     INTEGER, PARAMETER, PRIVATE :: mxs = 100
     INTEGER, PRIVATE, SAVE :: nbs = 0
     INTEGER, DIMENSION(mxs), PRIVATE :: code
     CHARACTER(LEN = ios_mxl), DIMENSION(mxs), PRIVATE :: chaine

     !***********************************************************************
     !                                     -- allocations'dump
     !***********************************************************************

     LOGICAL, PUBLIC, SAVE :: see_alloc = .FALSE.

     TYPE, PRIVATE :: item
        CHARACTER(LEN = 31) :: mod_name
        CHARACTER(LEN = 31) :: var_name
        INTEGER :: curr_size !-- taille de la derniere allocation
        INTEGER :: stack_size !-- somme des allocations - somme des deallocation
        INTEGER :: nb_alloc !-- nombre de fois ou l'allocation a eu lieu
     END TYPE item

     INTEGER, PARAMETER, PRIVATE :: mx_entree = 2000
     TYPE(item), DIMENSION(mx_entree), PRIVATE :: entrees
     INTEGER, PRIVATE, SAVE :: nb_entree = 0
     PRIVATE :: i_alloc_trouver

     !***********************************************************************
     !                                     -- logfile
     !***********************************************************************

     INTEGER, SAVE, PRIVATE :: unit_log = -1
     CHARACTER(LEN = 512), PUBLIC :: message_log
     CHARACTER(LEN = *), PARAMETER, PRIVATE :: logname = "logfile.txt"
     REAL, PRIVATE, SAVE :: xlimit = 0.0
     REAL, PRIVATE, SAVE :: xtiny = 0.0

     !***********************************************************************
     !                                     -- optimisation acces m�moire
     !***********************************************************************

     TYPE, PUBLIC :: doublet
        INTEGER :: n0_ds_buf
        INTEGER :: n0_ds_mat
        INTEGER :: jt
     END TYPE doublet

     PUBLIC :: straight_insertion

     LOGICAL, SAVE, PUBLIC               :: optim1_jacns = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim2_jacns = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim3_jacns = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim4_jacns = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim5_jacns = .FALSE.

     LOGICAL, SAVE, PUBLIC               :: optim1_plain = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim2_plain = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim3_plain = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim4_plain = .FALSE.
     LOGICAL, SAVE, PUBLIC               :: optim5_plain = .FALSE.

     PUBLIC :: read_optims_jacns

   CONTAINS

     !***********************************************************************
     !                                               -- straight_insertion
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2008/11/25

     !-- Summary : a partir de l'algorithme de N.Wirth, ajout dans un tableau tri�
     !-- End Summary

     SUBROUTINE straight_insertion(nn, array, twin)
       CHARACTER(LEN = *), PARAMETER :: mod_name = "straight_insertion"

       INTEGER, INTENT(INOUT) :: nn
       TYPE(doublet), DIMENSION(: ), INTENT(INOUT) :: array
       TYPE(doublet), INTENT(IN) :: twin

       INTEGER :: ii, trouve

       !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- straight_insertion.1
       !-----------------------------------------------------------------------

       trouve = nn + 1

       IF (trouve > SIZE(array)) THEN
          PRINT *, mod_name, " ERREUR : trop d'appels, Size(array)==", SIZE(array)
          CALL delegate_stop()
       END IF

       DO ii = nn, 1, -1
          IF (twin%n0_ds_mat > array(ii)%n0_ds_mat) THEN
             EXIT
          END IF
          array(ii + 1) = array(ii)
          trouve = ii
       END DO
       array(trouve) = twin
       nn = nn + 1

     END SUBROUTINE straight_insertion

     !***********************************************************************
     !                                               -- read_optims_jacns
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2007/05/03

     !-- Summary : tester si un scalaire a une valeur ind�finie
     !-- End Summary

     SUBROUTINE read_optims_jacns(accum)

       CHARACTER(LEN=*), PARAMETER :: mod_name = "read_optims_jacns"

       INTEGER, INTENT(OUT) :: accum

       CHARACTER(LEN = 100) :: string
       INTEGER :: ios, val

       !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- read_optims_jacns.1
       !-----------------------------------------------------------------------

       accum = 1

       CALL getenv("OPTIM1", string)
       READ(string, *, IOSTAT = ios) val
       IF (ios == 0) THEN
          optim1_jacns = val /= 0
       ELSE
          optim1_jacns = .FALSE.
       END IF

       CALL getenv("OPTIM2", string)
       READ(string, *, IOSTAT = ios) val
       IF (ios == 0) THEN
          optim2_jacns = val /= 0
       ELSE
          optim2_jacns = .FALSE.
       END IF

       accum = val !-- si pbs d'allocation m�moire, mettre 1
       IF (accum == 0) THEN
          accum = 1
       END IF

       CALL getenv("OPTIM3", string)
       READ(string, *, IOSTAT = ios) val
       IF (ios == 0) THEN
          optim3_jacns = val /= 0
       ELSE
          optim3_jacns = .FALSE.
       END IF

       CALL getenv("OPTIM4", string)
       READ(string, *, IOSTAT = ios) val
       IF (ios == 0) THEN
          optim4_jacns = val /= 0
       ELSE
          optim4_jacns = .FALSE.
       END IF

       CALL getenv("OPTIM5", string)
       READ(string, *, IOSTAT = ios) val
       IF (ios == 0) THEN
          optim5_jacns = val /= 0
       ELSE
          optim5_jacns = .FALSE.
       END IF

     END SUBROUTINE read_optims_jacns

     !***********************************************************************
     !                                               -- tester_nan0
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2007/05/03

     !-- Summary : tester si un scalaire a une valeur ind�finie
     !-- End Summary

     SUBROUTINE tester_nan0(scalar, message) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "tester_nan0"

       !-- arguments

       REAL, INTENT(IN) :: scalar
       CHARACTER(LEN = *), INTENT(IN) :: message

       !-- locales

       CHARACTER(LEN = 100) :: repres

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- tester_nan0.1
       !-----------------------------------------------------------------------

       !OFF    PRINT *, mod_name, " DEBUT ", message

       IF (xlimit == 0.0) THEN
          xlimit = SQRT(HUGE(1.0))
          IF (.FALSE.) THEN
             xtiny = SQRT(TINY(1.0)) ** 1.99
          END IF
       END IF

       IF (scalar > xlimit .OR. scalar < - xlimit) THEN
          PRINT *, mod_name, " ERREUR : ", message
          CALL printnan(scalar)
          CALL delegate_stop()
       END IF
       IF (scalar /= 0.0 .AND. scalar > - xtiny .AND. scalar < xtiny) THEN
          PRINT *, mod_name, " ERREUR : ", message
          CALL printnan(scalar)
          CALL delegate_stop()
       END IF
       IF (.FALSE.) THEN
          WRITE(repres, *) scalar
          IF (INDEX(repres, "NaN") > 0 .OR. INDEX(repres, "Infinite") > 0) THEN
             PRINT *, mod_name, " ERREUR : ", message
             CALL printnan(scalar)
             CALL delegate_stop()
          END IF
       ELSE
          IF (isnan(scalar) /= 0 .OR. isinf(scalar) /= 0) THEN
             PRINT *, scalar, isnan(scalar), isinf(scalar)
             PRINT *, mod_name, " ERREUR : ", message
             CALL printnan(scalar)
             CALL delegate_stop()
          END IF
       END IF

       !OFF    PRINT *, mod_name, " FIN ", message

     END SUBROUTINE tester_nan0

     !***********************************************************************
     !                                               -- tester_nan1
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2007/05/03

     !-- Summary : tester si un tableau contient des valeurs ind�finies
     !-- End Summary

     SUBROUTINE tester_nan1(array, message) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "tester_nan1"

       !-- arguments

       REAL, DIMENSION(: ), INTENT(IN) :: array
       CHARACTER(LEN = *), INTENT(IN) :: message

       !-- locales

       INTEGER :: ii
       CHARACTER(LEN = 100) :: repres

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- tester_nan1.1
       !-----------------------------------------------------------------------

       !OFF    PRINT *, mod_name, " DEBUT ", message

       IF (xlimit == 0.0) THEN
          xlimit = SQRT(HUGE(1.0))
          IF (.FALSE.) THEN
             xtiny = SQRT(TINY(1.0)) ** 1.99
          END IF
       END IF

       DO ii = 1, SIZE(array, 1)
          IF (array(ii) > xlimit .OR. array(ii) < - xlimit) THEN
             PRINT *, "ii=", ii, array(ii)
             PRINT *, mod_name, " ERREUR : ", message
             CALL printnan(array(ii))
             CALL delegate_stop()
          END IF
          IF (array(ii) /= 0.0 .AND. array(ii) > - xtiny .AND. array(ii) < xtiny) THEN
             PRINT *, "ii=", ii, array(ii)
             PRINT *, mod_name, " ERREUR : ", message
             CALL printnan(array(ii))
             CALL delegate_stop()
          END IF
          IF (.FALSE.) THEN
             WRITE(repres, *) array(ii)
             IF (INDEX(repres, "NaN") > 0 .OR. INDEX(repres, "Infinite") > 0) THEN
                PRINT *, "ii=", ii, array(ii)
                PRINT *, mod_name, " ERREUR : ", message
                CALL printnan(array(ii))
                CALL delegate_stop()
             END IF
          ELSE
             IF (isnan(array(ii)) /= 0 .OR. isinf(array(ii)) /= 0) THEN
                PRINT *, "ii=", ii, array(ii), isnan(array(ii)), isinf(array(ii))
                PRINT *, mod_name, " ERREUR : ", message
                CALL printnan(array(ii))
                CALL delegate_stop()
             END IF
          END IF
       END DO

       !OFF    PRINT *, mod_name, " FIN ", message

     END SUBROUTINE tester_nan1

     !***********************************************************************
     !                                               -- tester_nan2
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2007/05/03

     !-- Summary : tester si un tableau 2D contient des valeurs ind�finies
     !-- End Summary

     SUBROUTINE tester_nan2(array, message) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "tester_nan2"

       !-- arguments

       REAL, DIMENSION(:, : ), INTENT(IN) :: array
       CHARACTER(LEN = *), INTENT(IN) :: message

       !-- locales

       INTEGER :: ii, jj
       CHARACTER(LEN = 100) :: repres
       REAL :: val

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- tester_nan2.1
       !-----------------------------------------------------------------------

       IF (.FALSE.) THEN
          PRINT *, mod_name, " DEBUT ", message
       END IF

       IF (xlimit == 0.0) THEN
          xlimit = SQRT(HUGE(1.0))
          IF (.FALSE.) THEN
             xtiny = SQRT(TINY(1.0)) ** 1.99
          END IF
       END IF

       DO ii = 1, SIZE(array, 1)
          DO jj = 1, SIZE(array, 2)
             val = array(ii, jj)
             IF (val > xlimit .OR. val < - xlimit) THEN
                PRINT *, "ii=", ii, " jj=", jj, val
                PRINT *, mod_name, " ERREUR : ", message
                CALL printnan(val)
                CALL delegate_stop()
             END IF
             IF (val /= 0.0 .AND. val > - xtiny .AND. val < xtiny) THEN
                PRINT *, "ii=", ii, " jj=", jj, val
                PRINT *, mod_name, " ERREUR : ", message
                CALL printnan(val)
                CALL delegate_stop()
             END IF
             IF (.FALSE.) THEN
                WRITE(repres, *) val
                IF (INDEX(repres, "NaN") > 0 .OR. INDEX(repres, "Infinite") > 0) THEN
                   PRINT *, "ii=", ii, " jj=", jj, val
                   PRINT *, mod_name, " ERREUR : ", message
                   CALL printnan(val)
                   CALL delegate_stop()
                END IF
             ELSE
                IF (isnan(val) /= 0) THEN
                   PRINT *, "ii=", ii, " jj=", jj, val, "==NAN"
                   PRINT *, mod_name, " ERREUR : ", message
                   CALL printnan(val)
                   CALL delegate_stop()
                END IF
                IF (isinf(val) /= 0) THEN
                   PRINT *, "ii=", ii, " jj=", jj, val, "==INFINITE"
                   PRINT *, mod_name, " ERREUR : ", message
                   CALL printnan(val)
                   CALL delegate_stop()
                END IF
             END IF
          END DO
       END DO

       IF (.FALSE.) THEN
          PRINT *, mod_name, " FIN ", message
       END IF

     END SUBROUTINE tester_nan2

     !***********************************************************************
     !                                               -- tester_nan3
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2007/05/03

     !-- Summary : tester si un tableau 3D contient des valeurs ind�finies
     !-- End Summary

     SUBROUTINE tester_nan3(array, message) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "tester_nan3"

       !-- arguments

       REAL, DIMENSION(:, :, : ), INTENT(IN) :: array
       CHARACTER(LEN = *), INTENT(IN) :: message

       !-- locales

       INTEGER :: ii, jj, kl
       CHARACTER(LEN = 100) :: repres
       REAL :: val

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- tester_nan3.1
       !-----------------------------------------------------------------------

       PRINT *, mod_name, " DEBUT ", message

       IF (xlimit == 0.0) THEN
          xlimit = SQRT(HUGE(1.0))
          IF (.FALSE.) THEN
             xtiny = SQRT(TINY(1.0)) ** 1.99
          END IF
       END IF

       !-----------------------------------------------------------------------
       !                                               -- tester_nan3.2
       !-----------------------------------------------------------------------

       DO ii = 1, SIZE(array, 1)
          DO jj = 1, SIZE(array, 2)
             DO kl = 1, SIZE(array, 3)
                val = array(ii, jj, kl)
                IF (val > xlimit .OR. val <  - xlimit) THEN
                   PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, val
                   PRINT *, mod_name, " ERREUR : ", message
                   CALL printnan(val)
                   CALL delegate_stop()
                END IF
                IF (val /= 0.0 .AND. val > - xtiny .AND. val < xtiny) THEN
                   PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, val
                   PRINT *, mod_name, " ERREUR : ", message
                   CALL printnan(val)
                   CALL delegate_stop()
                END IF
                IF (.FALSE.) THEN
                   WRITE(repres, *) val
                   IF (INDEX(repres, "NaN") > 0 .OR. INDEX(repres, "Infinite") > 0) THEN
                      PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, val
                      PRINT *, mod_name, " ERREUR : ", message
                      CALL printnan(val)
                      CALL delegate_stop()
                   END IF
                ELSE
                   IF (isnan(val) /= 0 .OR. isinf(val) /= 0) THEN
                      PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, &
                           & val, isnan(val), isinf(val)
                      PRINT *, mod_name, " ERREUR : ", message
                      CALL printnan(val)
                      CALL delegate_stop()
                   END IF
                END IF
             END DO
          END DO
       END DO

       !-----------------------------------------------------------------------
       !                                               -- tester_nan3.3
       !-----------------------------------------------------------------------

       PRINT *, mod_name, " FIN ", message

     END SUBROUTINE tester_nan3

     !***********************************************************************
     !                                               -- tester_nan4
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2008/12/18

     !-- Summary : tester si un tableau 4D contient des valeurs ind�finies
     !-- End Summary

     SUBROUTINE tester_nan4(array, message) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "tester_nan4"

       !-- arguments

       REAL, DIMENSION(:, :, :, : ), INTENT(IN) :: array
       CHARACTER(LEN = *), INTENT(IN) :: message

       !-- locales

       INTEGER :: ii, jj, kl, mm
       CHARACTER(LEN = 100) :: repres
       REAL :: val

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- tester_nan4.1
       !-----------------------------------------------------------------------

       PRINT *, mod_name, " DEBUT ", message

       IF (xlimit == 0.0) THEN
          xlimit = SQRT(HUGE(1.0))
          IF (.FALSE.) THEN
             xtiny = SQRT(TINY(1.0)) ** 1.99
          END IF
       END IF

       !-----------------------------------------------------------------------
       !                                               -- tester_nan4.2
       !-----------------------------------------------------------------------

       DO ii = 1, SIZE(array, 1)
          DO jj = 1, SIZE(array, 2)
             DO kl = 1, SIZE(array, 3)
                DO mm = 1, SIZE(array, 4)
                   val = array(ii, jj, kl, mm)
                   IF (val > xlimit .OR. val <  - xlimit) THEN
                      PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, val
                      PRINT *, mod_name, " ERREUR : ", message
                      CALL printnan(val)
                      CALL delegate_stop()
                   END IF
                   IF (val /= 0.0 .AND. val > - xtiny .AND. val < xtiny) THEN
                      PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, val
                      PRINT *, mod_name, " ERREUR : ", message
                      CALL printnan(val)
                      CALL delegate_stop()
                   END IF
                   IF (.FALSE.) THEN
                      WRITE(repres, *) val
                      IF (INDEX(repres, "NaN") > 0 .OR. INDEX(repres, "Infinite") > 0) THEN
                         PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, val
                         PRINT *, mod_name, " ERREUR : ", message
                         CALL printnan(val)
                         CALL delegate_stop()
                      END IF
                   ELSE
                      IF (isnan(val) /= 0 .OR. isinf(val) /= 0) THEN
                         PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, &
                              & val, isnan(val), isinf(val)
                         PRINT *, mod_name, " ERREUR : ", message
                         CALL printnan(val)
                         CALL delegate_stop()
                      END IF
                   END IF
                END DO
             END DO
          END DO
       END DO

       !-----------------------------------------------------------------------
       !                                               -- tester_nan4.3
       !-----------------------------------------------------------------------

       PRINT *, mod_name, " FIN ", message

     END SUBROUTINE tester_nan4

     !***********************************************************************
     !                                               -- tester_nan5
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2008/12/18

     !-- Summary : tester si un tableau 5D contient des valeurs ind�finies
     !-- End Summary

     SUBROUTINE tester_nan5(array, message) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "tester_nan5"

       !-- arguments

       REAL, DIMENSION(:, :, :, :, : ), INTENT(IN) :: array
       CHARACTER(LEN = *), INTENT(IN) :: message

       !-- locales

       INTEGER :: ii, jj, kl, mm, nn
       CHARACTER(LEN = 150) :: repres
       REAL :: val

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- tester_nan5.1
       !-----------------------------------------------------------------------

       PRINT *, mod_name, " DEBUT ", message

       IF (xlimit == 0.0) THEN
          xlimit = SQRT(HUGE(1.0))
          IF (.FALSE.) THEN
             xtiny = SQRT(TINY(1.0)) ** 1.99
          END IF
       END IF

       DO ii = 1, SIZE(array, 1)
          DO jj = 1, SIZE(array, 2)
             DO kl = 1, SIZE(array, 3)
                DO mm = 1, SIZE(array, 4)
                   DO nn = 1, SIZE(array, 5)
                      val = array(ii, jj, kl, mm, nn)
                      IF (val > xlimit .OR. val <  - xlimit) THEN
                         PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, val
                         PRINT *, mod_name, " ERREUR : ", message
                         CALL printnan(val)
                         CALL delegate_stop()
                      END IF
                      IF (val /= 0.0 .AND. val > - xtiny .AND. val < xtiny) THEN
                         PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, val
                         PRINT *, mod_name, " ERREUR : ", message
                         CALL printnan(val)
                         CALL delegate_stop()
                      END IF
                      IF (.FALSE.) THEN
                         WRITE(repres, *) val
                         IF (INDEX(repres, "NaN") > 0 .OR. INDEX(repres, "Infinite") > 0) THEN
                            PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, val
                            PRINT *, mod_name, " ERREUR : ", message
                            CALL printnan(val)
                            CALL delegate_stop()
                         END IF
                      ELSE
                         IF (isnan(val) /= 0 .OR. isinf(val) /= 0) THEN
                            PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, &
                                 & val, isnan(val), isinf(val)
                            PRINT *, mod_name, " ERREUR : ", message
                            CALL printnan(val)
                            CALL delegate_stop()
                         END IF
                      END IF
                   END DO
                END DO
             END DO
          END DO
       END DO

       PRINT *, mod_name, " FIN ", message

     END SUBROUTINE tester_nan5

     !***********************************************************************
     !                                               -- tester_nan6
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2008/12/18

     !-- Summary : tester si un tableau 6D contient des valeurs ind�finies
     !-- End Summary

     SUBROUTINE tester_nan6(array, message) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "tester_nan6"

       !-- arguments

       REAL, DIMENSION(:, :, :, :, :, : ), INTENT(IN) :: array
       CHARACTER(LEN = *), INTENT(IN) :: message

       !-- locales

       INTEGER :: ii, jj, kl, mm, nn, oo
       CHARACTER(LEN = 200) :: repres
       REAL :: val

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- tester_nan6.1
       !-----------------------------------------------------------------------

       PRINT *, mod_name, " DEBUT ", message

       IF (xlimit == 0.0) THEN
          xlimit = SQRT(HUGE(1.0))
          IF (.FALSE.) THEN
             xtiny = SQRT(TINY(1.0)) ** 1.99
          END IF
       END IF

       !-----------------------------------------------------------------------
       !                                               -- tester_nan6.2
       !-----------------------------------------------------------------------

       DO ii = 1, SIZE(array, 1)
          DO jj = 1, SIZE(array, 2)
             DO kl = 1, SIZE(array, 3)
                DO mm = 1, SIZE(array, 4)
                   DO nn = 1, SIZE(array, 5)
                      DO oo = 1, SIZE(array, 6)
                         val = array(ii, jj, kl, mm, nn, oo)
                         IF (val > xlimit .OR. val <  - xlimit) THEN
                            PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, "oo==", oo, val
                            PRINT *, mod_name, " ERREUR : ", message
                            CALL printnan(val)
                            CALL delegate_stop()
                         END IF
                         IF (val /= 0.0 .AND. val > - xtiny .AND. val < xtiny) THEN
                            PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, "oo==", oo, val
                            PRINT *, mod_name, " ERREUR : ", message
                            CALL printnan(val)
                            CALL delegate_stop()
                         END IF
                         IF (.FALSE.) THEN
                            WRITE(repres, *) val
                            IF (INDEX(repres, "NaN") > 0 .OR. INDEX(repres, "Infinite") > 0) THEN
                               PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, "oo==", oo, val
                               PRINT *, mod_name, " ERREUR : ", message
                               CALL printnan(val)
                               CALL delegate_stop()
                            END IF
                         ELSE
                            IF (isnan(val) /= 0 .OR. isinf(val) /= 0) THEN
                               PRINT *, "ii=", ii, " jj=", jj, " kl==", kl, "mm==", mm, "nn==", nn, "oo==", oo, &
                                    & val, isnan(val), isinf(val)
                               PRINT *, mod_name, " ERREUR : ", message
                               CALL printnan(val)
                               CALL delegate_stop()
                            END IF
                         END IF
                      END DO
                   END DO
                END DO
             END DO
          END DO
       END DO

       !-----------------------------------------------------------------------
       !                                               -- tester_nan6.3
       !-----------------------------------------------------------------------

       PRINT *, mod_name, " FIN ", message

     END SUBROUTINE tester_nan6

     !***********************************************************************
     !                                               -- iostat_string
     !***********************************************************************
     !-- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/05/11

     !-- Summary :
     !-- End Summary

     FUNCTION iostat_string(istat) RESULT(string) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "iostat_string"

       !-- arguments

       INTEGER, INTENT(IN) :: istat
       CHARACTER(LEN = ios_mxl) :: string

       !-- locales

       CHARACTER(LEN = 100) :: test
       INTEGER :: ii, ival
       LOGICAL :: found, lval
       REAL :: rval

       INTERFACE
          SUBROUTINE ecrire_en_c(iunit) !-- BIND(C,NAME="ecrire_en_c")
            INTEGER, INTENT(IN) :: iunit
          END SUBROUTINE ecrire_en_c
       END INTERFACE

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- iostat_string.1
       !-----------------------------------------------------------------------

       IF (.NOT. init) THEN

          init = .TRUE.

          test = "!"
          READ(test, FMT = *, IOSTAT = ii) lval
          IF (ii /= 0) THEN
             nbs = nbs + 1
             code(nbs) = ii
             chaine(nbs) = "caractere invalide pour la lecture d'un Logical;"
          END IF

          test = "1.0"
          READ(test, FMT = *, IOSTAT = ii) ival
          IF (ii /= 0) THEN
             nbs = nbs + 1
             code(nbs) = ii
             chaine(nbs) = "caractere invalide pour la lecture d'un integer;"
          END IF

          test = "hah!"
          READ(test, FMT = *, IOSTAT = ii) rval
          IF (ii /= 0) THEN
             nbs = nbs + 1
             code(nbs) = ii
             chaine(nbs) = "caractere invalide pour la lecture d'un Real;"
          END IF

          CALL ecrire_en_c(97)
          READ(UNIT = 97, FMT = *, IOSTAT = ii) rval
          IF (ii /= 0) THEN
             nbs = nbs + 1
             code(nbs) = ii
             chaine(nbs) = "lecture Fortran d'un fichier ecrit en c;"
          END IF

          IF (ios_debug) THEN
             DO ii = 1, nbs
                PRINT *, ii, code(ii), TRIM(chaine(ii))
             END DO
          END IF

       END IF

       !-----------------------------------------------------------------------
       !                                               -- iostat_string.2
       !-----------------------------------------------------------------------

       string = ""
       found = .FALSE.

       DO ii = 1, nbs
          IF (code(ii) == istat) THEN
             string = TRIM(string) // TRIM(chaine(ii))
             found = .TRUE.
          END IF
       END DO

       IF (.NOT. found) THEN
          string = "Desole, pas d'explication enregistree pour cet iostat"
       END IF

     END FUNCTION iostat_string

     !***********************************************************************
     !                                               -- i_alloc_trouver
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/08/30

     !< Summary : chercher une entree dans la table
     !< End Summary

     FUNCTION i_alloc_trouver(mdname, vname) RESULT (trouve)

       CHARACTER(LEN = *), PARAMETER :: mod_name = "i_alloc_trouver"

       !-- arguments

       CHARACTER(LEN = *), INTENT(IN) :: mdname
       CHARACTER(LEN = *), INTENT(IN) :: vname
       INTEGER :: trouve

       !-- locales

       CHARACTER(LEN = 31) :: mdx
       CHARACTER(LEN = 31) :: vx
       INTEGER :: ii

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- i_alloc_trouver.1
       !-----------------------------------------------------------------------

       mdx = mdname
       vx = vname

       !    PRINT *, mod_name, " :: ", Trim(mdname), ".", Trim(vname)

       trouve = 0
       IF (TRIM(mdx) == ".global.") THEN !-- pour une deallocation hors de la m�thode qui fait le ALLOCATE %%%%
          DO ii = 1, nb_entree
             IF (TRIM(entrees(ii)%var_name) == TRIM(vx)) THEN
                trouve = ii
                EXIT
             END IF
          END DO
       ELSE
          DO ii = 1, nb_entree
             IF (TRIM(entrees(ii)%mod_name) == TRIM(mdx) .AND. TRIM(entrees(ii)%var_name) == TRIM(vx)) THEN
                trouve = ii
                EXIT
             END IF
          END DO
       END IF

     END FUNCTION i_alloc_trouver

     !***********************************************************************
     !                                               -- alloc_allocate
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/08/30

     !< Summary : creer ou incrementer une entree dans la table
     !< End Summary

     SUBROUTINE alloc_allocate(mdname, vname, taille) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "alloc_allocate"

       !-- arguments

       CHARACTER(LEN = *), INTENT(IN) :: mdname
       CHARACTER(LEN = *), INTENT(IN) :: vname
       INTEGER, INTENT(IN) :: taille

       !-- locales

       INTEGER :: trouve

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- alloc_allocate.1
       !-----------------------------------------------------------------------

       trouve = i_alloc_trouver(mdname, vname)
       IF (trouve == 0) THEN
          nb_entree = nb_entree + 1
          IF (nb_entree > mx_entree) THEN
             PRINT *, mod_name, " ERREUR : Augmenter mx_entree"
             CALL delegate_stop()
          END IF
          entrees(nb_entree)%mod_name = mdname
          entrees(nb_entree)%var_name = vname
          entrees(nb_entree)%curr_size = taille
          entrees(nb_entree)%stack_size = 1
          entrees(nb_entree)%nb_alloc = 1
       ELSE
          entrees(trouve)%curr_size = taille
          entrees(trouve)%stack_size = entrees(trouve)%stack_size + 1
          entrees(trouve)%nb_alloc = entrees(trouve)%nb_alloc + 1
       END IF

     END SUBROUTINE alloc_allocate

     !***********************************************************************
     !                                               -- alloc_allocate
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/08/30

     !< Summary : decrementer une entree dans la table
     !< End Summary

     SUBROUTINE alloc_deallocate(mdname, vname) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "alloc_deallocate"

       !-- arguments

       CHARACTER(LEN = *), INTENT(IN) :: mdname
       CHARACTER(LEN = *), INTENT(IN) :: vname

       !-- locales

       INTEGER :: trouve

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- alloc_deallocate.1
       !-----------------------------------------------------------------------

       trouve = i_alloc_trouver(mdname, vname)
       IF (trouve == 0) THEN
          IF (TRIM(mdname) == ".dummy.") THEN
             nb_entree = nb_entree + 1
             IF (nb_entree > mx_entree) THEN
                PRINT *, mod_name, " ERREUR : Augmenter mx_entree"
                CALL delegate_stop()
             END IF
          END IF
          trouve = nb_entree
          entrees(nb_entree)%mod_name = mdname
          entrees(nb_entree)%var_name = vname
          entrees(nb_entree)%curr_size = 0
          entrees(nb_entree)%stack_size = 1
          entrees(nb_entree)%nb_alloc = 0
       END IF
       IF (trouve == 0) THEN
          PRINT *, mod_name, " ERREUR : ", TRIM(mdname), ".", TRIM(vname), " non retrouve dans la table"
          CALL alloc_etat()
          CALL delegate_stop()
       ELSE
          entrees(trouve)%stack_size = entrees(trouve)%stack_size - 1
       END IF

     END SUBROUTINE alloc_deallocate

     !***********************************************************************
     !                                               -- alloc_etat
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/08/30

     !< Summary : lister l'�tat de la table d'allocation
     !< End Summary

     SUBROUTINE alloc_etat() !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "alloc_etat"

       !-- locales

       INTEGER :: ii, nbc

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- alloc_etat.1
       !-----------------------------------------------------------------------

       PRINT *, mod_name, nb_entree, " item(s) defini(s)"
       DO ii = 1, nb_entree
          nbc = entrees(ii)%stack_size
          IF (nbc < 0) THEN
             PRINT *, ii, TRIM(entrees(ii)%mod_name), ".", TRIM(entrees(ii)%var_name), entrees(ii)%curr_size, &
                  & " alloue ", entrees(ii)%nb_alloc, "fois, EN DEALLOC DE TROP", nbc
          ELSE IF (nbc == 0) THEN
             IF (entrees(ii)%nb_alloc > 10000) THEN
                PRINT *, ii, TRIM(entrees(ii)%mod_name), ".", TRIM(entrees(ii)%var_name), entrees(ii)%curr_size, &
                     & " alloue ", entrees(ii)%nb_alloc, "FOIS, libre"
             ELSE
                PRINT *, ii, TRIM(entrees(ii)%mod_name), ".", TRIM(entrees(ii)%var_name), entrees(ii)%curr_size, &
                     & " alloue ", entrees(ii)%nb_alloc, "fois, libre"
             END IF
          ELSE IF (nbc == 1) THEN
             PRINT *, ii, TRIM(entrees(ii)%mod_name), ".", TRIM(entrees(ii)%var_name), entrees(ii)%curr_size, &
                  & " alloue ", entrees(ii)%nb_alloc, "fois, non de-alloue"
          ELSE
             PRINT *, ii, TRIM(entrees(ii)%mod_name), ".", TRIM(entrees(ii)%var_name), entrees(ii)%curr_size, &
                  & " alloue ", entrees(ii)%nb_alloc, "fois, EN FUITE MEMOIRE", nbc
          END IF
       END DO

     END SUBROUTINE alloc_etat

     !***********************************************************************
     !                                               -- alloc_set
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/08/31

     !< Summary :
     !< End Summary

     SUBROUTINE alloc_set() !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "alloc_set"

       !-- locales

       LOGICAL :: existe

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- alloc_set.1
       !-----------------------------------------------------------------------

       INQUIRE(FILE = ".alloc", EXIST = existe) !-- %%%%
       see_alloc = existe

     END SUBROUTINE alloc_set


     !***********************************************************************
     !                                               -- alloc_test
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/08/30

     !< Summary : test du module
     !< End Summary

     SUBROUTINE alloc_test() !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "alloc_test"

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- alloc_test.1
       !-----------------------------------------------------------------------

       CALL alloc_allocate(mod_name, "var1", 1000)
       CALL alloc_allocate(mod_name, "var2", 100)
       CALL alloc_allocate(mod_name, "var3", 10)

       CALL alloc_allocate(mod_name, "var1", 1000)

       CALL alloc_deallocate(mod_name, "var3")

       !-----------------------------------------------------------------------
       !                                               -- alloc_test.2
       !-----------------------------------------------------------------------

       CALL alloc_etat()

     END SUBROUTINE alloc_test

     !***********************************************************************
     !                                               -- log_record
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/10/05

     !< Summary : enregistrer les messages de log
     !< End Summary

     SUBROUTINE log_record(chaine) !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "log_record"

       !-- arguments

       CHARACTER(LEN = *), INTENT(IN) :: chaine

       !-- locales

       INTEGER :: istat

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- log_record.1
       !-----------------------------------------------------------------------

       IF (unit_log < 0) THEN
          unit_log = 1
          OPEN(UNIT = unit_log, FILE = logname, STATUS = "REPLACE", ACTION = "WRITE", POSITION = "REWIND", IOSTAT = istat)
          IF (istat /= 0) THEN
             PRINT *, mod_name, " ERREUR : pb a l'ouverture de logname", istat, logname
             CALL delegate_stop()
          END IF
       END IF

       WRITE(UNIT = unit_log, FMT = "(A)", IOSTAT = istat) TRIM(chaine)
       IF (istat /= 0) THEN
          PRINT *, mod_name, " ERREUR : pb a l'ecriture de logname", istat, logname
          CALL delegate_stop()
       END IF

       PRINT *, TRIM(chaine)

     END SUBROUTINE log_record

     !***********************************************************************
     !                                               -- log_replay
     !***********************************************************************
     !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2007/10/05

     !< Summary : remettre les messages de log sur le stdout
     !< End Summary

     SUBROUTINE log_replay() !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "log_replay"

       !-- arguments

       !-- locales

       CHARACTER(LEN = 512) :: chaine
       INTEGER :: istat

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- log_replay.1
       !-----------------------------------------------------------------------

       IF (unit_log > 0) THEN

          CLOSE(UNIT = unit_log)

          OPEN(UNIT = unit_log, FILE = logname, STATUS = "OLD", ACTION = "READ", POSITION = "REWIND", IOSTAT = istat)
          IF (istat /= 0) THEN
             PRINT *, mod_name, " ERREUR : pb a re-ouverture de logname", istat, logname
             CALL delegate_stop()
          END IF

          DO
             READ(UNIT = unit_log, FMT = "(A)", IOSTAT = istat) chaine
             IF (istat /= 0) THEN
                EXIT
             END IF
             PRINT *, TRIM(chaine)
          END DO

          CLOSE(unit_log)

       END IF

     END SUBROUTINE log_replay

     !***********************************************************************
     !                                               -- info_ifdef
     !***********************************************************************
     !-- UMR 5251 IMB   C.N.R.S.  2007/04/27

     !< Summary : imprimer les valeurs des drapeaux de compilation conditionnelle
     !-- End Summary

     SUBROUTINE info_ifdef() !-- Interface

       CHARACTER(LEN = *), PARAMETER :: mod_name = "info_ifdef"

       !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       !                                               -- info_ifdef.1
       !-----------------------------------------------------------------------

       ! impression du drapeau
#ifdef ADAPT
       PRINT *, "-DADAPT"
#endif
#ifdef ANYEOS
       PRINT *, "-DANYEOS"
#endif
#ifdef DEBUG
       PRINT *, "-DDEBUG"
#endif
#ifdef DEBUG_ALGO
       PRINT *, "-DDEBUG_ALGO"
#endif
#ifdef DEBUG_PALG
       PRINT *, "-DDEBUG_PALG"
#endif
#ifdef G95
       PRINT *, "-DG95"
#endif
#ifdef INCOMP
       PRINT *, "-DINCOMP"
#endif
#ifdef INCOMPGAS
       PRINT *, "-DINCOMPGAS"
#endif
#ifdef INFO
       PRINT *, "-DINFO"
#endif
#ifdef INSTAT_RD
       PRINT *, "-DINSTAT_RD"
#endif
#ifdef MAILLAGE_MOBILE
       PRINT *, "-DMAILLAGE_MOBILE"
#endif
#ifdef MHD
       PRINT *, "-DMHD"
#endif
#ifdef MHD_GP
       PRINT *, "-DMHD_GP"
#endif
#ifdef MPICH2
       PRINT *, "-DMPICH2"
#endif
#ifdef OLDEULER
       PRINT *, "-DOLDEULER"
#endif
#ifdef NAVIER_STOKES
       PRINT *, "-DNAVIER_STOKES"
#endif
#ifdef ORDRE_ELEVE
       PRINT *, "-DORDRE_ELEVE"
#endif
#ifdef PERFECTGAS
       PRINT *, "-DPERFECTGAS"
#endif
#ifdef RESIDUAL
       PRINT *, "-DRESIDUAL"
#endif
#ifdef RINGLEB
       PRINT *, "-DRINGLEB"
#endif
#ifdef SAVEASC
       PRINT *, "-DSAVEASC"
#endif
#ifdef SEQUENTIEL
       PRINT *, "-DSEQUENTIEL"
#endif
#ifdef STIFFENEDGAS
       PRINT *, "-DSTIFFENEDGAS"
#endif
#ifdef STIFFENEDGASMF
       PRINT *, "-DSTIFFENEDGASMF"
#endif
#ifdef STIFFENEDGASREF
       PRINT *, "-DSTIFFENEDGASREF"
#endif
#ifdef TIMES
       PRINT *, "-DTIMES"
#endif
#ifdef TURBULENCE_SA
       PRINT *, "-DTURBULENCE_SA"
#endif
#ifdef VERIFIER
       PRINT *, "-DVERIFIER"
#endif
#ifdef VISIT
       PRINT *, "-DVISIT"
#endif
#ifdef MURGE
       PRINT *, "-DMURGE"
#endif
#ifdef PASTIX_MURGE
       PRINT *, "-DPASTIX_MURGE"
#endif
#ifdef HIPS
       PRINT *, "-DHIPS"
#endif
     END SUBROUTINE info_ifdef


     !***********************************************************************
     !                                               -- My_timer
     !***********************************************************************
     !> \brief Timer of the program: milliseconds 
     !!
     !! Return 0 at the first call
     !! \param temps return the wall time spent since the begining of the program
     !! \author Pascal JACQ  2010/01/04

     SUBROUTINE My_Timer(temps)
       CHARACTER(LEN=*), PARAMETER :: mod_name = "My_Timer"

       INTEGER , INTENT(OUT)        :: temps
       LOGICAL , SAVE               :: FirstPass = .TRUE.
       INTEGER , SAVE               :: nb_periodes_sec , time_init, time_max
       INTEGER                      :: nb_periodes

       CALL SYSTEM_CLOCK(Count = nb_periodes)
       IF (FirstPass) THEN
          ! We save the first timer
          time_init = nb_periodes
          CALL SYSTEM_CLOCK(count_rate = nb_periodes_sec, COUNT_MAX = time_max)
          FirstPass = .FALSE.
       END IF

       nb_periodes = nb_periodes - time_init
       IF (nb_periodes < 0) THEN
          nb_periodes = nb_periodes + time_max
       END IF

       temps = INT(1000* (REAL(nb_periodes) / REAL(nb_periodes_sec)))
     END SUBROUTINE My_Timer

   END MODULE LesTypes
