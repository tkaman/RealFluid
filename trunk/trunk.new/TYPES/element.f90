MODULE element_class

  IMPLICIT NONE

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  INTEGER, PARAMETER, PRIVATE :: et_seg     = 0
  INTEGER, PARAMETER, PRIVATE :: et_triaP1  = 1
  INTEGER, PARAMETER, PRIVATE :: et_quadQ1  = 2
  INTEGER, PARAMETER, PRIVATE :: et_tetraP1 = 3
  INTEGER, PARAMETER, PRIVATE :: et_hexaQ1  = 4
  INTEGER, PARAMETER, PRIVATE :: et_triaP2  = 11
  INTEGER, PARAMETER, PRIVATE :: et_quadQ2  = 12
  INTEGER, PARAMETER, PRIVATE :: et_tetraP2 = 13
  INTEGER, PARAMETER, PRIVATE :: et_hexaQ2  = 14
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  TYPE, PUBLIC :: element
  
     INTEGER :: ndegre   = 0 ! nbre de sommets du simplexe,
     INTEGER :: nsommets = 0 ! nbre de dof ds l'element
     INTEGER :: EltType  = 0 !  

     REAL,    DIMENSION(:,:), POINTER :: coor ! Coordonnees de l'élement
     INTEGER, DIMENSION(:),   POINTER :: nu   ! indices de l'e'elements
     REAL,    DIMENSION(:,:), POINTER :: n    ! normales pour le calcul des residus

     REAL, DIMENSION(:), POINTER :: dmin

     REAL :: volume = 0 ! Volume de l'element
     
   CONTAINS
   
     PROCEDURE, PUBLIC :: normale     => normale_element
     PROCEDURE, PUBLIC :: aire        => aire_element
     PROCEDURE, PUBLIC :: gradient    => gradient_element
     PROCEDURE, PUBLIC :: gradient_bg => gradient_element_cg ! --- dante
     PROCEDURE, PUBLIC :: base        => base_element

  END TYPE element
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
  PRIVATE :: normale_element
!!$  PRIVATE :: normale_element_point
  PRIVATE :: aire_element
  PRIVATE :: long_element 
  PRIVATE :: gradient_element
  PRIVATE :: gradient_element_cg
  PRIVATE :: base_element

  PRIVATE :: normale_seg
  PRIVATE :: normale_tri
  PRIVATE :: normale_quad_Q1
!!$  PRIVATE :: normale_quad_point
  PRIVATE :: normale_quad_Q2
  PRIVATE :: normale_tetra_P1
  PRIVATE :: normale_tetra_P2

  PRIVATE :: aire_tri
  PRIVATE :: aire_quad
  PRIVATE :: aire_tetra

  PRIVATE :: base_element_tri_P2
  PRIVATE :: base_element_tri_P1
  PRIVATE :: gradient_element_tri_P1
  PRIVATE :: gradient_element_tri_P2
  PRIVATE :: gradient_element_quad_Q1
  PRIVATE :: gradient_element_quad_Q2
  PRIVATE :: gradient_element_tri_P1_cg
  PRIVATE :: gradient_element_tri_P2_cg
  PRIVATE :: gradient_element_tetra_P1
  PRIVATE :: gradient_element_tetra_P2

!!$  PRIVATE:: base_element_hexa_q1
!!$  PRIVATE:: base_element_hexa_q2


!#######
CONTAINS
!#######


  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONCTIONS GENERALES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !============================
  REAL FUNCTION aire_element(e)
  !============================
  
  !> Compute the volume of the element
  
    
    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e
    !------------------------------

    SELECT CASE(e%EltType)

    CASE(et_seg)
       
       aire_element = long_element(e)
       
    CASE(et_triaP1, et_triaP2)

       aire_element = aire_tri(e)

    CASE(et_quadQ1, et_quadQ2)

       aire_element = aire_quad(e)

    CASE(et_tetraP1, et_tetraP2)

       aire_element = aire_tetra(e)

    CASE DEFAULT

       PRINT*, "element.f90/aire_element"
       PRINT*, "cet element n'et pas defini, Type=",e%EltType
       STOP

    END SELECT

  END FUNCTION aire_element
  !========================


  !=============================================
  FUNCTION normale_element(e, i) RESULT(normale)
  !=============================================

    !> cette routine permet de calculer la normale integree permettant
    !> de calculer le residu de bord en employant une formule de 
    !> quadrature ou on a interpole les flux.
    !> Ces "normales" sont en general
    !> differentes des normales des fonctions de base, sauf dans le cas triangle

    IMPLICIT NONE
    
    CLASS(element),   INTENT(IN) :: e
    INTEGER,OPTIONAL, INTENT(IN) :: i

    INTEGER, DIMENSION(6), PARAMETER :: ipt = (/1,2,3,3,1,2/)
    
    REAL,DIMENSION(3):: normale ! il me fallait preciser la taille
    !------------------------------------

    SELECT CASE(e%EltType)

    CASE(et_seg)
    
       normale(1:2) = normale_seg(e)
       
    CASE(et_triaP1)
    
       IF(.NOT. PRESENT(i)) THEN
          PRINT*, "element.f90/normale_element"
          STOP
       ENDIF

       normale(1:2) = normale_tri(e,i)
       
    CASE(et_quadQ1)
    
       IF(.NOT. PRESENT(i)) THEN
          PRINT*, "element.f90/normale_element"
          STOP
       ENDIF

       normale(1:2) = normale_quad_Q1(e,i)
       
    CASE(et_triaP2)
    
       IF(.NOT. PRESENT(i)) THEN
          PRINT*, "element.f90/normale_element"
          STOP
       ENDIF

       SELECT CASE(i)
          CASE(1,2,3)
             normale(1:2) = normale_tri(e,i)
          CASE(4,5,6)
             normale(1:2) = -normale_tri(e,ipt(i))
        END SELECT
        
    CASE(et_quadQ2)
    
       IF(.NOT. PRESENT(i)) THEN
          PRINT*, "element.f90/normale_element"
          STOP
       ENDIF

       normale(1:2) = normale_quad_Q2(e,i)

    CASE(et_tetraP1)

       IF(.NOT. PRESENT(i)) THEN
          PRINT*, "element.f90/normale_element"
          STOP
       ENDIF

       normale(1:3) = normale_tetra_P1(e,i)

    CASE(et_tetraP2)

       IF(.NOT. PRESENT(i)) THEN
          PRINT*, "element.f90/normale_element"
          STOP
       ENDIF

       normale(1:3) = normale_tetra_P2(e,i)
       
    CASE DEFAULT

       PRINT*, "element.f90/normale_element"
       PRINT*, "cet element n'est pas defini, Type=",e%EltType
       STOP

    END SELECT

  END FUNCTION normale_element
  !===========================



!!$  FUNCTION normale_element_point(e, i, x) RESULT(normale)
!!$    !> On calcule la normale de la ieme fonction de base au point de coordonnees
!!$    !> barycentriques x
!!$    IMPLICIT NONE
!!$    
!!$    CLASS(element),       INTENT(in) :: e
!!$    INTEGER, OPTIONAL,    INTENT(in) :: i
!!$    REAL, DIMENSION(:,:), INTENT(in) :: x
!!$    
!!$    REAL, DIMENSION(:), POINTER:: normale
!!$    !=====================================
!!$
!!$    normale = 0.
!!$
!!$  END FUNCTION normale_element_point
!!$  !=================================

!!$ FUNCTION normale_quad_point(e, x) RESULT(normale)
!!$    !> evaluation du gradient Q1 au point de coordonnee barycentrique x
!!$    IMPLICIT NONE
!!$    
!!$    CLASS(element),       INTENT(in) :: e
!!$    REAL, DIMENSION(:,:), INTENT(in) :: x
!!$    
!!$    REAL, DIMENSION(:), POINTER :: normale
!!$    !=====================================
!!$    
!!$    normale=0.
!!$    
!!$  END FUNCTION normale_quad_point


  !=========================================
  REAL FUNCTION  base_element(e, l, x, y, z)
  !=========================================

    !> Shape function l-th at the point of
    !> barycentric coordinates x,y,z

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e
    INTEGER,        INTENT(IN) :: l
    REAL,           INTENT(IN) :: x, y, z
    !----------------------------------------
    
    SELECT CASE (e%EltType)

       CASE(et_triaP1)

          base_element = base_element_tri_P1(l, x, y, z)

       CASE(et_triaP2)

          base_element = base_element_tri_P2(l, x, y, z)

       CASE DEFAULT

          PRINT*, "element/base_element, type",e%EltType
          STOP

    END SELECT
    
  END FUNCTION base_element
  !========================


  !===================================================
  FUNCTION  gradient_element(e, i, j) RESULT(gradient)
  !===================================================

    !> calcul du gradient de la fonction de base i au sommet j

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e
    INTEGER,        INTENT(IN) :: i
    INTEGER,        INTENT(IN) :: j
    
    REAL, DIMENSION(2) :: gradient
    !-----------------------------------

    SELECT CASE (e%EltType)

       CASE(et_seg)
          PRINT*, "element/gradient_element, Type",e%EltType
          STOP

       CASE(et_triaP1)

          gradient = gradient_element_tri_P1(e,i,j)

       CASE(et_quadQ1)

          gradient = gradient_element_quad_Q1(e,i,j)

       CASE(et_triaP2 )

          gradient = gradient_element_tri_P2(e,i,j)

       CASE(et_quadQ2)

          gradient = gradient_element_quad_Q2(e,i,j)

       CASE(et_tetraP1)

          gradient = gradient_element_tetra_P1(e,i,j)
          
       CASE(et_tetraP2)

          gradient = gradient_element_tetra_P2(e,i,j)          

       CASE DEFAULT

          PRINT*, "element.f90/gradient_element"
          PRINT*, "cet element n'est pas defini, Type=",e%EltType
          STOP

    END SELECT
    
  END FUNCTION gradient_element
  !============================


  !============================================================
  FUNCTION  gradient_element_cg(e, i, x, y, z) RESULT(gradient)
  !============================================================

    !> Compute the greadient of the shape function
    !> at the point of barycentric coordiantes x,y,z

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e
    INTEGER,        INTENT(IN) :: i
    REAL,           INTENT(IN) :: x, y, z
    
    REAL,DIMENSION(2):: gradient
    !----------------------------------------
    
    
    SELECT CASE (e%EltType)

    CASE(et_seg)

       PRINT*, "element/gradient_element, Type",e%EltType
       STOP

    CASE(et_triaP1)

       gradient = gradient_element_tri_P1_cg(e, i, x, y, z)

    CASE(et_triaP2)

       gradient = gradient_element_tri_P2_cg(e, i, x, y, z)

    CASE default

       PRINT*, "element.f90/gradient_element"
       PRINT*, "cet element n'est pas defini, Type=",e%EltType
       STOP

    END SELECT
    
  END FUNCTION gradient_element_cg
  !===============================
 

!---------------------------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------------------


  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONCTIONS SPECIFIQUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        SEGMENT        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !============================
  REAL FUNCTION long_element(e)
  !============================
     IMPLICIT NONE
      
     CLASS(element),INTENT(IN)::e
     !---------------------------
     
     long_element = SQRT( (e%coor(1,2)-e%coor(1,1))**2 + &
                          (e%coor(2,2)-e%coor(2,1))**2 )
    
  END FUNCTION long_element
  !========================


  !=========================================
  FUNCTION normale_seg(e, i) RESULT(normale)

    !> ici on suppose que la numerotation
    !> est telle que les normales sont exterieures au domaine

    IMPLICIT NONE
    
    CLASS(element),    INTENT(IN) :: e
    INTEGER, OPTIONAL, INTENT(IN) :: i

    REAL,DIMENSION(2):: normale
    !---------------------------------

    normale(1)= (e%Coor(2,2)-e%Coor(2,1))
    normale(2)= (e%Coor(1,2)-e%Coor(1,1))
    
  END FUNCTION normale_seg
  !=======================



  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONCTIONS SPECIFIQUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       TRIANGLE        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !========================
  REAL FUNCTION aire_tri(e)
  !========================

    !> Calcule l'aire d'un triangle de coordonnees e%coor

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e
    !-------------------------------
    
    aire_tri = 0.5*( (e%coor(1,2)-e%coor(1,1))*(e%coor(2,3)-e%coor(2,1)) - &
                     (e%coor(2,2)-e%coor(2,1))*(e%coor(1,3)-e%coor(1,1)) )

    aire_tri = ABS(aire_tri) 
                     
  END FUNCTION aire_tri
  !====================


  !=========================================
  FUNCTION normale_tri(e, i) RESULT(normale)
  !=========================================

    !> Calcule les normales entrantes au triangle de coordonnees e%coor
    !> Pour calculer les gradients, il faut diviser par 1/2 de l'aire.....

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e
    INTEGER,        INTENT(IN) :: i
    
    REAL, DIMENSION(2) :: normale

    INTEGER, DIMENSION(3), PARAMETER:: ip =(/2,3,1/), &
                                       ip1=(/3,1,2/)
    !--------------------------------------------------

    normale(1) = e%Coor(2,ip(i) ) - e%Coor(2,ip1(i))
    normale(2) = e%Coor(1,ip1(i)) - e%Coor(1,ip (i))
    
  END FUNCTION normale_tri
  !=======================


  !============================================
  REAL FUNCTION base_element_tri_p1(l, x, y, z)
  !============================================

    !> evaluation de la fonction de base P1 numero "l"
    !> au point de coordonnees barycentrique x,y,z

    IMPLICIT NONE
    
    REAL,    INTENT(IN) :: x, y, z
    INTEGER, INTENT(IN) :: l
    !-------------------------------
    
    SELECT CASE (l)

       CASE(1)
          base_element_tri_p1 = x
          
       CASE(2)
          base_element_tri_p1 = y
          
       CASE(3)
          base_element_tri_p1 = z
          
       CASE DEFAULT
          PRINT*, "numero de fct de base",l
          STOP
          
    END SELECT
    
  END FUNCTION base_element_tri_p1
  !===============================

  
  !============================================
  REAL FUNCTION base_element_tri_p2(l, x, y, z)
  !============================================
    !> evaluation de la fonction de base P2 numero "l"
    !> au point de coordonnees barycentrique x,y,z
    IMPLICIT NONE
    
    REAL,    INTENT(IN) :: x, y, z
    INTEGER, INTENT(IN) :: l
    !------------------------------
    
    SELECT CASE (l)
       
    CASE(1)
       base_element_tri_p2 = (2.*x-1.)*x
       
    CASE(2)
       base_element_tri_p2 = (2.*y-1.)*y
       
    CASE(3)
       base_element_tri_p2 = (2.*z-1.)*z
       
    CASE(4)
       base_element_tri_p2 = 4.*x*y
       
    CASE(5)
       base_element_tri_p2 = 4.*y*z
       
    CASE(6)
       base_element_tri_p2 = 4.*z*x
       
    CASE default

       PRINT*, "numero de fct de base",l
       STOP

    END SELECT

  END FUNCTION base_element_tri_p2
  !===============================


  !==========================================================
  FUNCTION  gradient_element_tri_P1(e, igrad, j) RESULT(grad)
  !==========================================================

    !> evaluation du gradient de la fonction de base
    !> P1 numero "igrad" au sommet j

    IMPLICIT NONE

    CLASS(element), INTENT(IN) :: e
    INTEGER,        INTENT(IN):: igrad
    INTEGER,        INTENT(IN) :: j
    
    REAL, DIMENSION(2)  :: grad(2)
    REAL                :: fx, fy, fz
    !-----------------------------------

    SELECT CASE(igrad)
       CASE(1)
          fx = 1.0; fy = 0.0; fz = 0.0
       CASE(2)
          fx = 0.0; fy = 1.0; fz = 0.0
       CASE(3)
          fx = 0.0; fy = 0.0; fz = 1.0
    END SELECT
    
    grad = fx*e%n(:,1) + fy*e%n(:,2) + fz*e%n(:,3)

    grad = 0.5*grad/e%volume !--- dante bug 1/2 fixed
    
  END FUNCTION gradient_element_tri_P1
  !===================================


  !===================================================================
  FUNCTION  gradient_element_tri_P1_cg(e, igrad, x, y, z) RESULT(grad)
  !===================================================================

    !> evaluation du gradient de la fonction de base P1 numero "igrad"
    !> au point de coordonnees barycentrique x,y,z

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e
    INTEGER,        INTENT(IN) :: igrad
    REAL,           INTENT(IN) :: x,y,z
    
    REAL, DIMENSION(2) :: grad(2)
    REAL               :: fx, fy, fz
    !---------------------------------------

    SELECT CASE(igrad)
       CASE(1)
          fx = 1.0; fy = 0.0; fz = 0.0
       CASE(2)
          fx = 0.0; fy = 1.0; fz = 0.0
       CASE(3)
          fx = 0.0; fy = 0.0; fz = 1.0
    END SELECT
    
    grad= fx*e%n(:,1) + fy*e%n(:,2) + fz*e%n(:,3)

    grad = 0.5*grad/e%volume ! --- dante !bug 1/2 fixed

  END FUNCTION gradient_element_tri_P1_cg
  !======================================


  !==========================================================
  FUNCTION  gradient_element_tri_P2(e, igrad, j) RESULT(grad)
  !==========================================================
 
    !> evaluation du gradient de la fonction P2 de base numero "igrad"
    !> au point de coordonnees barycentrique x,y,z
    
    CLASS(element), INTENT(IN) :: e 
    INTEGER,        INTENT(IN) :: igrad
    INTEGER,        INTENT(IN) :: j
    
    REAL, DIMENSION(2) :: grad(2)
    REAL               :: fx, fy, fz
    REAL               :: x, y, z
    !--------------------------------------
    
    SELECT CASE (j)
    CASE(1)  
       x = 1.0; y = 0.0; z = 0.0
    CASE(2)       
       x = 0.0; y = 1.0; z = 0.0
    CASE(3)       
       x = 0.0; y = 0.0; z = 1.0
    CASE(4)       
       x = 0.5; y = 0.5; z = 0.0
    CASE(5)       
       x = 0.0; y = 0.5; z = 0.5
    CASE(6)       
       x = 0.5; y = 0.0; z = 0.5       
    END SELECT

    SELECT CASE(igrad)
       CASE(1)
          fx = 4.0*x - 1.0; fy = 0.0;         fz = 0.0
       CASE(2)
          fx = 0.0;         fy = 4.0*y - 1.0; fz = 0.0
       CASE(3)
          fx = 0.0;         fy = 0.0;         fz = 4.0*z - 1.0
       CASE(4)
          fx = 4.0*y;       fy = 4.0*x;       fz = 0.0
       CASE(5)
          fx = 0.0;         fy = 4.0*z;       fz = 4.0*y
       CASE(6)
          fx = 4.0*z;       fy = 0.0;         fz = 4.0*x
    END SELECT
    
    grad = fx*e%n(:,1) + fy*e%n(:,2) + fz*e%n(:,3)

    grad = 0.5*grad/e%volume
    
  END FUNCTION gradient_element_tri_P2
  !===================================
  

  !====================================================================
  FUNCTION  gradient_element_tri_p2_cg(e, igrad, x, y, z) RESULT(grad)
  !====================================================================
    
    !> evaluation du gradient de la fonction P2 de base numero "igrad"
    !> au point de cooedonnees barycentriques x,y,z

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e 
    INTEGER,        INTENT(IN) :: igrad
    REAL,           INTENT(IN) :: x, y, z
    
    REAL, DIMENSION(2) :: grad(2)
    REAL               :: fx, fy, fz
    !---------------------------------------

    SELECT CASE(igrad)
       CASE(1)
          fx = 4.0*x - 1; fy = 0.0;       fz = 0.0
       CASE(2)
          fx = 0.0;       fy = 4.0*y-1.0; fz = 0.0
       CASE(3)
          fx = 0.0;       fy = 0.0;       fz = 4.0*z - 1.0
       CASE(4)
          fx = 4.0*y;     fy = 4.0*x;     fz = 0.0
       CASE(5)
          fx = 0.0;       fy = 4.0*z;     fz = 4.0*y
       CASE(6)
          fx = 4.0*z;     fy = 0.0;       fz = 4.0*x
    END SELECT
    
    grad= fx*e%n(:,1) + fy*e%n(:,2) + fz*e%n(:,3)

    grad=0.5*grad/e%volume ! --- dante

  END FUNCTION gradient_element_tri_p2_cg
  !======================================




  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONCTIONS SPECIFIQUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       QUADRANGLE      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !=========================
  REAL FUNCTION aire_quad(e)
  !=========================

    !> Calcule l'aire d'un quadrangle de coordonnees e%coor

    IMPLICIT NONE
    
    CLASS(element), INTENT(IN) :: e

    REAL:: xg1, xg2, yg1, yg2
    REAL, DIMENSION(2):: g

    INTEGER, DIMENSION(4), PARAMETER :: ip=(/2,3,4,1/)
    INTEGER:: k, kp1   
    !--------------------------------------
    
    g(1) = SUM(e%coor(1,1:4))/4.
    g(2) = SUM(e%coor(2,1:4))/4.
    
    aire_quad = 0

    DO k = 1, 4
       kp1 = ip(k)
       xG1 = e%Coor(1, k)   - g(1)
       yG1 = e%Coor(2, k)   - g(2)
       xG2 = e%Coor(1, kp1) - g(1)
       yG2 = e%Coor(2, kp1) - g(2)
       aire_quad = aire_quad + xG1 * yG2 - xG2 * yG1
    END DO
    
  END FUNCTION aire_quad
  !=====================


  !=============================================
  FUNCTION normale_quad_Q1(e, i) RESULT(normale)
  !=============================================

    !> Calcule les normales d'un quadrangle telles
    !> qu'on en a besoin pour evaluer
    !> \int_{boundary of Q} f\cdot n dl quand approxime f, en Q1.
    !> Employe pour le calcul des residus.

    IMPLICIT NONE
    
    CLASS(element),    INTENT(IN) :: e
    INTEGER,           INTENT(IN) :: i 

    REAL, DIMENSION(2) :: normale
    
    INTEGER :: j, j1, j2
    !----------------------------------
    
    SELECT CASE(i)
       CASE(1)
          j1=2; j2=4
       CASE(2)
          j1=3; j2=1
       CASE(3)
          j1=4; j2=2
       CASE(4)
          j1=1; j2=3
    END SELECT

    normale(1) =  e%Coor(2,j1)-e%Coor(2,j2)
    normale(2) =  e%Coor(1,j2)-e%Coor(1,j1)

  END FUNCTION normale_quad_Q1
  !===========================


  !=============================================
  FUNCTION normale_quad_Q2(e, i) RESULT(normale)
  !=============================================

    !> Calcule les normales integree d'un quadrangle telles 
    !> qu'on en a besoin pour evaluer
    !> \int_{boundary of Q} f\cdot n dl quand approxime f, en Q2.
    !> Employe pour le calcul des residus.

    IMPLICIT NONE

    CLASS(element),    INTENT(in) :: e
    INTEGER, OPTIONAL, INTENT(in) :: i ! not optional ?

    REAL, DIMENSION(2) :: normale
    
    INTEGER::j, j1, j2
    !-----------------------------------

    SELECT CASE(i)
    
       CASE(1)! comme  q1
          j1=2; j2=4
       CASE(2)! comme  q1
          j1=3; j2=1
       CASE(3)! comme  q1
          j1=4;j2=2
       CASE(4)! comme  q1
          j1=1; j2=3
       CASE(5)  ! nouveaux termes : suivants
          j1=2; j2=1
       CASE(6)
          j1=3;j2=2
       CASE(7)
          j1=4;j2=3
       CASE(8)
          j1=1;j2=4
       CASE (9)
          j1=1;j2=1! c'est le centre de gravité, il n'apparait pas
       CASE DEFAULT
          print*, "normale_quad_q2"
          STOP
    END SELECT

    normale(1) =  e%Coor(2,j1)-e%Coor(2,j2)
    normale(2) =  e%Coor(1,j2)-e%Coor(1,j1)
    
  END FUNCTION normale_quad_Q2
  !===========================


  !==========================================================
  FUNCTION gradient_element_quad_Q1(e, igrad, j) RESULT(grad)
  !==========================================================

    !> evaluation du gradient de la fonction Q1
    !> de base numero "igrad" au sommet j

    IMPLICIT NONE
    
    CLASS(element), INTENT(in) :: e
    INTEGER,        INTENT(in) :: igrad
    INTEGER,        INTENT(in) :: j
    
    INTEGER, PARAMETER :: Ndim=2

    REAL, DIMENSION(2) :: grad(2)

    ! les coordonnees "x" des sommets dans l'element de reference
    REAL, DIMENSION(4), PARAMETER:: postabx=(/-1., 1.,1.,-1./) 

    ! les coordonnees "y" des sommets dans l'element de reference
    REAL, DIMENSION(4), PARAMETER:: postaby=(/-1.,-1.,1., 1./) 

    INTEGER :: ix, iy

    REAL :: cx, cy, determinant

    REAL,DIMENSION(2,2) :: JInvQuad
    !------------------------------------

    ! les coordonnees du point ou on va estimer le gradient de la "iquad"ieme fct de base
    cx = postabx(j)
    cy = postaby(j)

    JInvQuad(1,2) = 0.25*( &
         (1.D0-cy)*(e%Coor(2,1)-e%Coor(2,2)) + (1.D0+cy)*(e%Coor(2,4)-e%Coor(2,3))&
         &)

    JInvQuad(2,2) = 0.25*(&
         (1.D0-cy)*(e%Coor(1,2)-e%Coor(1,1)) + (1.D0+cy)*(e%Coor(1,3)-e%Coor(1,4))&
         &)

    JInvQuad(1,1) = 0.25*(&
         &(1.D0-cx)*(e%Coor(2,4)-e%Coor(2,1)) + (1.D0+cx)*(e%Coor(2,3)-e%Coor(2,2))&
         &)

    JInvQuad(2,1) = 0.25*(&
         & (1.D0-cx)*(e%Coor(1,1)-e%Coor(1,4)) + (1.D0+cx)*(e%Coor(1,2)-e%Coor(1,3))&
         &)

    Determinant = JInvQuad(1,1)*JInvQuad(2,2) - JInvQuad(2,1)*JInvQuad(1,2)

    SELECT CASE(igrad)

       CASE(1)
          Grad(1:NDIM) = MATMUL(JInvQuad,0.25D0*(/cy-1.D0,cx-1.D0/))/Determinant
          
       CASE(2)
          Grad(1:NDIM) = MATMUL(JInvQuad,0.25D0*(/1.D0-cy,-1.D0-cx/))/Determinant
          
       CASE(3)
          Grad(1:NDIM) = MATMUL(JInvQuad,0.25D0*(/1.D0+cy,1.D0+cx/))/Determinant
          
       CASE(4)
          Grad(1:NDIM) = MATMUL(JInvQuad,0.25D0*(/-cy-1.D0,1.D0-cx/))/Determinant
          
    END SELECT

  END FUNCTION gradient_element_quad_Q1
  !====================================


  !==========================================================
  FUNCTION gradient_element_quad_Q2(e, igrad, j) RESULT(grad)
  !==========================================================

    !> evaluation du gradient de la fonction Q1
    !> de base numero "igrad" au sommet j

    IMPLICIT NONE
    
    CLASS(element), INTENT(in) :: e 
    INTEGER,        INTENT(in) :: igrad
    INTEGER,        INTENT(in) :: j

    INTEGER, PARAMETER :: Ndim=2
    REAL, DIMENSION(2) :: grad, Gbulle

    ! les coordonnees "x" des sommets dans l'element de reference
    REAL,DIMENSION(9), PARAMETER:: postabx=(/-1.,1.,1.,-1.,0.,1.,0.,-1.,0./)

    ! les coordonnees "y" des sommets dans l'element de reference
    REAL,DIMENSION(9), PARAMETER:: postaby=(/-1.,-1.,1.,1.,-1.,0.,1.,0.,0./)

    INTEGER :: ix, iy
    REAL :: cx, cy, determinant
    REAL, DIMENSION(2,2) :: JInvQuad
    !----------------------------------------------

    cx = postabx(j)
    cy = postaby(j)

    JInvQuad(1:2,2) = 0.25*(/ (1.D0-cy)*(e%Coor(2,1)-e%Coor(2,2)) + &
                    & (1.D0+cy)*(e%Coor(2,4)-e%Coor(2,3)), (1.D0-cy)*(e%Coor(1,2)-e%Coor(1,1)) + &
                    & (1.D0+cy)*(e%Coor(1,3)-e%Coor(1,4)) /)

    JInvQuad(1:2,1) = 0.25*(/(1.D0-cx)*(e%Coor(2,4)-e%Coor(2,1)) + &
                    & (1.D0+cx)*(e%Coor(2,3)-e%Coor(2,2)), (1.D0-cx)*(e%Coor(1,1)-e%Coor(1,4)) &
                    & + (1.D0+cx)*(e%Coor(1,2)-e%Coor(1,3))/)

    Determinant = JInvQuad(1,1)*JInvQuad(2,2) - JInvQuad(2,1)*JInvQuad(1,2)

    Gbulle(1:NDIM) = (/ -2*cx* (1-cy**2), -2*cy* (1-cx**2) /)

    SELECT CASE(igrad)

    CASE(1)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.25D0*(/(2.D0*cx+cy)*(1.D0-cy),(2.D0*cy+cx)*(1.D0-cx)/) + 0.25*GBulle(:) )/Determinant
       
    CASE(2)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.25D0*(/(2.D0*cx-cy)*(1.D0-cy),(2.D0*cy-cx)*(1.D0+cx)/)  + 0.25*GBulle(:) )/Determinant

    CASE(3)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.25D0*(/(2.D0*cx+cy)*(1.D0+cy),(2.D0*cy+cx)*(1.D0+cx)/) + 0.25*GBulle(:) )/Determinant

    CASE(4)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.25D0*(/(2.D0*cx-cy)*(1.D0+cy),(2.D0*cy-cx)*(1.D0-cx)/) + 0.25*GBulle(:) )/Determinant

    CASE(5)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.50D0*(/-2.D0*cx*(1.D0-cy),cx**2-1.D0/) - 0.5*GBulle(:) ) /Determinant

    CASE(6)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.50D0*(/1.D0-cy**2,-2.D0*cy*(1.D0+cx)/)- 0.5*GBulle(:) )/Determinant

    CASE(7)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.50D0*(/-2.D0*cx*(1.D0+cy),1.D0-cx**2/)- 0.5*GBulle(:) )/Determinant

    CASE(8)
       Grad(1:NDIM) = MATMUL(JInvQuad, &
                    & 0.50D0*(/cy**2-1.D0,-2.D0*cy*(1.D0-cx)/)- 0.5*GBulle(:) )/Determinant

    CASE(9)
       Grad(1:NDIM) = MATMUL(JInvQuad, Gbulle(1:NDIM) ) / Determinant

    END SELECT
    
  END FUNCTION gradient_element_quad_Q2
  !====================================




  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONCTIONS SPECIFIQUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      TETRAHEDRON      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !==========================
  REAL FUNCTION aire_tetra(e)
  !==========================

    !> Compute the volume of the tetrahedron 

    IMPLICIT NONE
    
    CLASS(element), INTENT(in) :: e
    REAL :: z12, z13, z14, z23, z24, z34, &
           & b1, b2, b3, b4
    !-----------------------------------------

    z12  = e%Coor(3,2) - e%Coor(3,1)
    z13  = e%Coor(3,3) - e%Coor(3,1) 
    z14  = e%Coor(3,4) - e%Coor(3,1)
    z23  = e%Coor(3,3) - e%Coor(3,2)
    z24  = e%Coor(3,4) - e%Coor(3,2)
    z34  = e%Coor(3,4) - e%Coor(3,3)
         

    b1 =  e%Coor(2,2)*z34 - e%Coor(2,3)*z24 + e%Coor(2,4)*z23
    b2 = -e%Coor(2,1)*z34 + e%Coor(2,3)*z14 - e%Coor(2,4)*z13
    b3 =  e%Coor(2,1)*z24 - e%Coor(2,2)*z14 + e%Coor(2,4)*z12
    b4 = -e%Coor(2,1)*z23 + e%Coor(2,2)*z13 - e%Coor(2,3)*z12

    aire_tetra = ( e%Coor(1,1)*b1 + e%Coor(1,2)*b2 + e%Coor(1,3)*b3 + e%Coor(1,4)*b4 ) / 6.0
    
  END FUNCTION aire_tetra
  !======================


  !==============================================
  FUNCTION normale_tetra_P1(e, i) RESULT(normale)
  !==============================================

    !> Compute the inward normals to the face of the tetrahedron P1.
    !> The module of the normal is the area of the face,
    !> which the normal belongs to.
    !> Used to compute the residual

    IMPLICIT NONE
    
    CLASS(element), INTENT(in) :: e
    INTEGER,        INTENT(in) :: i 
    
    REAL, DIMENSION(3) :: normale

    REAL, DIMENSION(3) :: U, V

    INTEGER, DIMENSION(4, 3), PARAMETER :: ifac = &
                            & RESHAPE( (/ 4, 1, 4, 1, & 
                                        & 3, 3, 2, 2, &
                                        & 2, 4, 1, 3 /), (/4, 3/) )
    !------------------------------------------

    U = e%Coor(:, ifac(i, 2)) - e%Coor(:, ifac(i, 1))
    V = e%Coor(:, ifac(i, 3)) - e%Coor(:, ifac(i, 1))
    
    normale(1) = U(2)*V(3) - U(3)*V(2)
    normale(2) = U(3)*V(1) - U(1)*V(3)
    normale(3) = U(1)*V(2) - U(2)*V(1)

    normale = normale/2.0
 
  END FUNCTION normale_tetra_P1
  !=============================


  !=============================================
  FUNCTION normale_tetra_P2(e, i) RESULT(normale)
  !=============================================

    !> Compute the normals to the face of the tetrahedron P2.
    !> The module of the normals is the area of the corresponding face.
    !> The normal associated to internal DOFs is the opposite of the
    !> sum of the two neighbour normals, 
    !> Used to compute the residual

    IMPLICIT NONE
    
    CLASS(element),    INTENT(in) :: e
    INTEGER, OPTIONAL, INTENT(in) :: i !Not optional ?
    
    REAL, DIMENSION(3) :: normale

    INTEGER, DIMENSION(5:10, 2), PARAMETER :: ip = &
                               & RESHAPE( (/3,1,2,2,1,1, &
                                         &  4,4,4,3,3,2 /), (/6, 2/))
    !----------------------------------------------------- 

    SELECT CASE(i)

    CASE(1, 2, 3, 4)

       normale(:) = normale_tetra_P1(e, i)

    CASE(5, 6, 7, 8, 9, 10)

       normale(:) = normale_tetra_P1(e, ip(i, 1)) + normale_tetra_P1(e, ip(i, 2))
       
       normale = -normale
                                
    END SELECT
 
  END FUNCTION normale_tetra_P2
  !============================


  !============================================================
  FUNCTION  gradient_element_tetra_P1(e, igrad, j) RESULT(grad)
  !============================================================

    !> evaluation du gradient de la fonction de base
    !> P1 numero "igrad" au sommet j

    IMPLICIT NONE

    CLASS(element), INTENT(in) :: e
    INTEGER,        INTENT(in):: igrad
    INTEGER,        INTENT(in) :: j
    
    REAL, DIMENSION(2)  :: grad(2)
    REAL                :: fw, fx, fy, fz
    !-----------------------------------

    SELECT CASE(igrad)
       CASE(1)
          fw = 1.0; fx = 0.0; fy = 0.0; fz = 0.0
       CASE(2)
          fw = 0.0; fx = 1.0; fy = 0.0; fz = 0.0
       CASE(3)
          fw = 0.0; fx = 0.0; fy = 1.0; fz = 0.0
       CASE(4)
          fw = 0.0; fx = 0.0; fy = 0.0; fz = 1.0          
    END SELECT
    
    grad = fw*e%n(:,1) + fx*e%n(:,2) + fy*e%n(:,3) + fz*e%n(:,4)

    grad = grad/(3.0*e%volume)
        
  END FUNCTION gradient_element_tetra_P1
  !=====================================


  !============================================================
  FUNCTION  gradient_element_tetra_P2(e, igrad, j) RESULT(grad)
  !============================================================

    !> evaluation du gradient de la fonction de base
    !> P1 numero "igrad" au sommet j

    IMPLICIT NONE

    CLASS(element), INTENT(in) :: e
    INTEGER,        INTENT(in):: igrad
    INTEGER,        INTENT(in) :: j
    
    REAL, DIMENSION(2)  :: grad(2)
    REAL                :: fw, fx, fy, fz
    REAL                :: w, x, y, z
    !--------------------------------------

    SELECT CASE(j)
    CASE(1)
       w = 1.0; x = 0.0; y = 0.0; z = 0.0
       
    CASE(2)
       w = 0.0; x = 1.0; y = 0.0; z = 0.0
       
    CASE(3)
       w = 0.0; x = 0.0; y = 1.0; z = 0.0
       
    CASE(4)
       w = 0.0; x = 0.0; y = 0.0; z = 1.0
       
    CASE(5)
       w = 0.5; x = 0.5; y = 0.0; z = 0.0
       
    CASE(6)
       w = 0.0; x = 0.5; y = 0.5; z = 0.0
       
    CASE(7)
       w = 0.5; x = 0.0; y = 0.5; z = 0.0
       
    CASE(8)
       w = 0.5; x = 0.0; y = 0.0; z = 0.5

    CASE(9)
       w = 0.0; x = 0.5; y = 0.0; z = 0.5

    CASE(10)
       w = 0.0; x = 0.0; y = 0.5; z = 0.5       
    END SELECT
    

    SELECT CASE(igrad)
       CASE(1)
          fw = 4.0*w - 1.0; fx = 0.0;         fy = 0.0;         fz = 0.0
       CASE(2)
          fw = 0.0;         fx = 4.0*x - 1.0; fy = 0.0;         fz = 0.0
       CASE(3)
          fw = 0.0;         fx = 0.0;         fy = 4.0*y - 1.0; fz = 0.0
       CASE(4)
          fw = 0.0;         fx = 0.0;         fy = 0.0;         fz = 4.0*z -1.0
       CASE(5)
          fw = 4.0*x;       fx = 4.0*w;       fy = 0.0;         fz = 0.0
       CASE(6)
          fw = 0.0;         fx = 4.0*y;       fy = 4.0*x;       fz = 0.0
       CASE(7)
          fw = 4.0*y;       fx = 0.0;         fy = 4.0*w;       fz = 0.0
       CASE(8)
          fw = 4.0*z;       fx = 0.0;         fy = 0.0;         fz = 4.0*w
       CASE(9)
          fw = 0.0;         fx = 4.0*z;       fy = 0.0;         fz = 4.0*x
       CASE(10)
          fw = 0.0;         fx = 0.0;         fy = 4.0*z;       fz = 4.0*y
          
    END SELECT
    
    grad = fw*e%n(:,1) + fx*e%n(:,2) + fy*e%n(:,3) + fz*e%n(:,4)

    grad = grad/(3.0*e%volume)        

  END FUNCTION gradient_element_tetra_P2
  !=====================================



  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONCTIONS SPECIFIQUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       HEXAHEDRON      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !============================================
  FUNCTION normale_hexaP1(e, i) RESULT(normale) 
  !============================================

    !> Calcule les normales d'un hexahedre telles qu'on en a besoin pour evaluer
    !> \int_{boundary of Q} f\cdot n dl quand approxime f en Q1.
    !>  Employe pour le calcul des residus.
    IMPLICIT NONE
    
    CLASS(element),    INTENT(in) :: e
    INTEGER, OPTIONAL, INTENT(in) :: i ! numero du sommet ???opt

    INTEGER, DIMENSION(4,6), PARAMETER :: fHex= RESHAPE( (/1, 4, 3, 2,&
         &1, 2, 6, 5, &
         &1, 5, 8, 4, &
         &2, 3, 7, 6, &
         &3, 4, 8, 7, &
         &5, 6, 7, 8 /), (/4,6/) )

    INTEGER, DIMENSION(3,8), PARAMETER :: face= RESHAPE( (/ 1,2,3, &
         & 1,2,4, &
         & 1,4,5, &
         & 1,3,5, &
         & 2,3,6, &
         & 2,4,6, &
         & 4,5,6, &
         & 3,5,6 /) ,(/3,8/) )! defini 

    INTEGER,DIMENSION(4),PARAMETER:: ip=(/2,3,4,1/)
    INTEGER                       :: iface, k, Nsplex, is,is1,is2, id, isom
    REAL, DIMENSION(3)            :: V1, V2, xg, xgt
    REAL, DIMENSION(3)            :: normale
    REAL, DIMENSION(3,6)          :: vno_f
    !--------------------------------------------------

    ! Calcul du centre de gravite de l'element
    xgt = 0.0
    DO k = 1, 8
       xgt = xgt + e%Coor(:, k)
    END DO
    xgt = xgt / 8.0

    DO isom = 1, 3
       iface=face(isom,i)
       xg = 0.0
       Nsplex = 4
       DO  k = 1,  Nsplex
          is          = fHex(k, iface)
          xg          = xg + e%Coor(:, is)
       END DO
       xg = xg / REAL(Nsplex)

       ! On decompose la face en 4 triangles avec pour sommets
       ! les sommets du quadrangle et le centre de gravite
       Vno_f(:,iface) = 0.0
       DO k = 1, 4
          is1 = fHex(k    , iface)
          is2 = fHex(ip(k), iface)
          V1(:) = e%Coor(:, is1) - xg(:)
          V2(:) = e%Coor(:, is2) - xg(:)

          ! Produit vectoriel = Aire triangle* normale *2
          Vno_f(1,iface) = Vno_f(1,iface) + (V1(2)*V2(3) - V1(3)*V2(2)) *0.5
          Vno_f(2,iface) = Vno_f(2,iface) + (V1(3)*V2(1) - V1(1)*V2(3)) *0.5
          Vno_f(3,iface) = Vno_f(3,iface) + (V1(1)*V2(2) - V1(2)*V2(1)) *0.5
       END DO

       ! Ce test règle le problème des maillages mal orientés.
       ! -----------------------------------------------------
       IF (SUM(Vno_f(:,iface) * (xg - xgt)) < 0 ) THEN
          Vno_f(:,iface) = - Vno_f(:,iface)
       END IF
    END DO

    normale = 0.0

    DO id = 1, 3
       normale(id) = SUM(Vno_f(id, face(:,isom))) / 4.0
    END DO

  END FUNCTION Normale_hexaP1
  !==========================


END MODULE element_class
