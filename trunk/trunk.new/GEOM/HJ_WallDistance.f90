MODULE HJ_WallDistance

  USE LesTypes
  USE Assemblage
 
  IMPLICIT NONE
!!$
!!$#include "finclude/petscsys.h"
!!$#include "finclude/petscvec.h"
!!$#include "finclude/petscvec.h90"
!!$#include "finclude/petscmat.h"
!!$#include "finclude/petscksp.h"
!!$#include "finclude/petscpc.h"
!!$
!!$  Mat :: J_mat
!!$  KSP :: ksp
!!$  PC  :: pc
!!$  Vec :: sol, rhs
!!$  PetscErrorCode :: ierr

CONTAINS

  !============================================
  SUBROUTINE Near_wall_distance(Var, N_tot_DOF)
  !============================================

    IMPLICIT NONE
    TYPE(Variables), INTENT(INOUT) :: Var
    INTEGER,         INTENT(IN)    :: N_tot_DOF
    !--------------------------------------------

    INTEGER,      PARAMETER :: ite_max = 900000
    REAL(KIND=8), PARAMETER :: Res_toll = 1.0E-10
    REAL(KIND=8), PARAMETER :: CFL0 = 0.001d0
    !--------------------------------------------

    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: psi
    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: HH
    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: B_i
    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: dt
    !---------------------------------------------

    TYPE(element_Str) :: loc_ele

    TYPE(Matrice) :: AA

    INTEGER,      DIMENSION(:), ALLOCATABLE :: NU
    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: u
    REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: HH_num
    !---------------------------------------------

    INTEGER :: ite, N_dofs, bc_idx, bc_type
    REAL(KIND=8) :: Res, dt_ele, dt_loc, alpha, CFL, Res_old

    INTEGER :: je, jb, i
    !--------------------------------------------

    ALLOCATE(  HH(N_tot_DOF), &
              psi(N_tot_DOF), &
              B_i(N_tot_DOF)  )

    ALLOCATE( dt(N_tot_DOF)  )
    
    ite = 0; Res = 1.0; psi = 0.0; CFL = CFL0

    !---------------
    ! Jacobian Matrix
    !----------------------------------------
    IF( .NOT.ASSOCIATED(Mat_fb%Vals) ) THEN
       WRITE(*,*) 'ERROR: Jacobian not allocated'
       STOP
    END IF
    
    AA%NNtot    = Mat_fb%NNtot
    AA%NNin     = Mat_fb%NNin
    AA%NNsnd    = Mat_fb%NNsnd
    AA%blksize  = 1
    AA%NcoefMat = Mat_fb%NcoefMat

    AA%JPosi  => Mat_fb%JPosi 
    AA%JVCell => Mat_fb%JVCell
    AA%IDiag  => Mat_fb%IDiag

    ALLOCATE( AA%Vals(1,1, AA%NcoefMat) )

!!$    CALL init_PETSc(N_tot_DOF, AA)
    !-----------------------------------------

    DO

       HH = 0.0; B_i = 0.0; dt = 0.0

       CALL resetcoeftab(AA)

       ite = ite + 1

       DO je = 1, SIZE(d_element)

          loc_ele = d_element(je)%e

          N_dofs = loc_ele%N_points

          ALLOCATE( NU(N_dofs), u(N_dofs), &
                    HH_num(N_dofs)         )
          
          NU = loc_ele%NU

          u = psi(Nu)

          CALL HH_LxF(loc_ele, u, HH_num, dt_ele, alpha)

          !dt_loc = CFL*dt_ele

          dt(NU) = dt(NU) + dt_ele * loc_ele%Volume

          HH(NU) = HH(NU) + HH_num

          B_i(NU) = B_i(NU) + loc_ele%Volume

          !***
          CALL Assembly_Jacobian_HH_LxF(loc_ele, u, dt_loc, alpha, AA)
          !***

          DEALLOCATE( NU, u, HH_num )

       ENDDO

       HH = HH / B_i
       dt = dt / B_i

       psi = psi - CFL0*dt*HH

       ! B.C.
       IF( ALLOCATED(b_element) ) THEN

          DO je = 1, SIZE(b_element)

             N_dofs = b_element(je)%b_f%N_points

             ALLOCATE( NU(N_dofs) )
             NU = b_element(je)%b_f%Nu

             bc_idx  = b_element(je)%b_f%bc_type
             bc_type = Data%FacMap(bc_idx)

             IF( bc_type == lgc_paroi_adh_adiab_flux_nul .OR. &
                 bc_type == lgc_paroi_misc               ) THEN
                
                HH(NU)  = 0.d0
                psi(NU) = 0.d0 !

                !***
                DO i = 1, N_dofs
                   CALL assemblage_setidentblokline(NU(i), AA)
                ENDDO
                !***

             ENDIF

             DEALLOCATE( NU )
             
          ENDDO          

       ENDIF

       Res_old = Res

       Res = MAXVAL( ABS(HH) )
       write(*, '(I6, 2X, E13.5)' ) ite, Res      
       
       IF( ite > ite_max .OR. Res < Res_toll ) EXIT

!!$       ! Solve linear system
!!$       HH = -HH
!!$       dt = CFL*dt
!!$
!!$       ! [(V/Dt) + J]*d_psi = -HH
!!$       ! note: d_psi -> HH
!!$       CALL solve_lin_syst(AA, HH, dt)
!!$       
!!$       psi = psi + HH

    ENDDO

    Var%Vp(1, :) = psi !

    DEALLOCATE( HH, B_i, psi , dt)

    NULLIFY( AA%JPosi, AA%JVCell, AA%IDiag)
    DEALLOCATE( AA%Vals )

!!$    CALL MatDestroy(J_mat, ierr)
!!$    CALL VecDestroy(rhs, ierr)
!!$    CALL VecDestroy(sol, ierr)
!!$    CALL KSPDestroy(ksp, ierr)
!!$    CALL PetscFinalize(ierr)

  END SUBROUTINE Near_wall_distance
  !================================

  !===========================================
  SUBROUTINE HH_LxF(ele, u, HH_num, dt, alpha)
  !===========================================

    IMPLICIT NONE

    TYPE(element_Str),          INTENT(IN)  :: ele
    REAL(KIND=8), DIMENSION(:), INTENT(IN)  :: u
    REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: HH_num
    REAL(KIND=8),               INTENT(OUT) :: dt
    REAL(KIND=8),               INTENT(OUT) :: alpha
    !------------------------------------------------
    REAL(KIND=8) :: sum_u, ff, mod_n, h_max, p

    REAL(KIND=8), DIMENSION(ele%N_dim) :: D_u_q

    INTEGER :: N_dim, id, k, j, iq
    !------------------------------------------------

    N_dim = Data%Ndim

    alpha = HUGE(1.d0); h_max = 0.d0; p = 0.d0

    DO k = 1, ele%N_verts
       
       mod_n = DSQRT( SUM( ele%rd_n(:, k)**2 ) )
       alpha = MIN(alpha, mod_n)

       h_max = MAX( h_max, mod_n)
       p = p + mod_n

    ENDDO
    
    ff = 0.0
    
    DO iq = 1, ele%N_quad

       DO id = 1, N_dim

          D_u_q(id) = SUM( u * ele%D_phi_q(id, :, iq) )
          
       ENDDO
       
       ff = ff + ele%w_q(iq) * ( SQRT( SUM(D_u_q**2) ) - 1.d0 )

    ENDDO
        
    DO k = 1, ele%N_points

       sum_u = 0.0

       DO j = 1, ele%N_points

          sum_u = sum_u + (u(k) - u(j))

       ENDDO
       
       HH_num(k) = ff + alpha*sum_u

    ENDDO

    !dt = ele%volume/h_max
    dt = ele%volume/p

  END SUBROUTINE HH_LxF
  !====================

  !=========================================================
  SUBROUTINE Assembly_Jacobian_HH_LxF(ele, u, dt, alpha, AA)
  !=========================================================

    IMPLICIT NONE

    TYPE(element_Str),  INTENT(IN)    :: ele
    REAL, DIMENSION(:), INTENT(IN)    :: u
    REAL,               INTENT(IN)    :: dt
    REAL,               INTENT(IN)    :: alpha
    TYPE(Matrice),      INTENT(INOUT) :: AA
    !----------------------------------------

    REAL, DIMENSION(:), ALLOCATABLE :: DH_Du
    REAL, DIMENSION(:), ALLOCATABLE :: S_Vol

    REAL, DIMENSION(ele%N_dim) :: D_u_q

    REAL, DIMENSION(1,1) :: mm

    REAL :: inv_norm_Du, D_u_D_u
    REAL :: r_N_dofs, inv_dt, const

    INTEGER :: N_dofs, N_dim
    INTEGER :: i, j, k, iq, id, je, cn_e
    !-----------------------------------------

    N_dim  = ele%N_dim
    N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs)

    ALLOCATE( DH_Du(N_dofs), S_Vol(N_dofs) )
    
    DH_Du = 0.0;  S_Vol = 0.0 

    DO i = 1, N_dofs

       DO iq = 1, ele%N_quad

          DO id = 1, N_dim

             D_u_q(id) = SUM( u * ele%D_phi_q(id, :, iq) ) 

          ENDDO

          inv_norm_Du = SQRT( SUM(D_u_q**2) )
          ! Avoid division by 0
          inv_norm_Du = MAX( inv_norm_Du, 1.0e-16)
          inv_norm_Du = 1.d0 / inv_norm_Du

          D_u_D_u = 0.0

          DO id = 1, N_dim

             DO k = 1, N_dofs

                D_u_D_u =  D_u_D_u + &
                           ele%D_phi_q(id, i, iq)*ele%D_phi_q(id, k, iq)*u(k)

             ENDDO

          ENDDO

          DH_Du(i) = DH_Du(i) + &
                     ele%w_q(iq) * inv_norm_Du * D_u_D_u
                     

       ENDDO ! N_quad -> iq

       DO je = 1, SIZE(ele%b_e_con(i)%cn_ele)

          cn_e = ele%b_e_con(i)%cn_ele(je)

          S_Vol(i) = S_vol(i) + d_element(cn_e)%e%Volume

       ENDDO
       
    ENDDO ! N_dof -> i

    !---------------
    ! Diagonal term
    !----------------------------------------------------
    Const = alpha * (r_N_dofs - 1.d0) !+ (1.d0/dt)

    DO i = 1, N_dofs

       mm = Const + DH_Du(i)/S_Vol(i)

       CALL Assemblage_AddDiag(mm, ele%NU(i), AA)
       
    ENDDO

    !----------------------
    ! Extra-Diagonal terms 
    !---------------------------------------------------
    Const = -alpha

    DO i = 1, N_dofs

       mm = Const + DH_Du(i)/S_Vol(i)

       DO j = 1, N_dofs

          IF(j == i) CYCLE

          CALL Assemblage_Add(mm, ele%NU(i), ele%NU(j), AA)

       ENDDO

    ENDDO

    DEALLOCATE( DH_Du, S_Vol )    
    
  END SUBROUTINE Assembly_Jacobian_HH_LxF
  !======================================
!!$
!!$  !===================================
!!$  SUBROUTINE init_PETSc(N_dof, Mat_fb)
!!$  !===================================
!!$
!!$    IMPLICIT NONE
!!$   
!!$    INTEGER,       INTENT(IN) :: N_dof
!!$    TYPE(Matrice), INTENT(IN) :: Mat_fb
!!$    !---------------------------------------
!!$
!!$    PetscInt, DIMENSION(:), ALLOCATABLE :: N_nz
!!$
!!$    PetscInt :: Nvar, N_syst
!!$  
!!$    PetscInt :: max_res, max_ite
!!$    PetscReal :: rtol, atol, dtol
!!$
!!$    INTEGER :: i, id_s, id_e
!!$    !---------------------------------------
!!$
!!$    CALL PetscInitialize(PETSC_NULL_CHARACTER, ierr)
!!$
!!$    !-------------------------
!!$    ! INIZIALIZATION of MATRIX
!!$    !-----------------------------------------------
!!$
!!$    ! Number of NonZero elements in each row
!!$    ALLOCATE( N_nz(N_dof) )
!!$
!!$    DO i = 1, N_dof
!!$
!!$       N_nz(i) = Mat_fb%JPosi(i+1) - Mat_fb%JPosi(i)
!!$
!!$    ENDDO
!!$
!!$    ! Preallocation of matrix memory
!!$    CALL MatCreateSeqAIJ(PETSC_COMM_SELF, N_dof,N_dof, &
!!$                         PETSC_NULL_INTEGER, N_nz, J_mat, ierr)
!!$
!!$    DEALLOCATE( N_nz )
!!$      
!!$    !---------------------------------
!!$    ! CREATION of KSP and PC enviroment
!!$    !-----------------------------------------------
!!$    CALL KSPCreate(PETSC_COMM_WORLD, ksp, ierr)
!!$		
!!$    CALL KSPSetType(ksp, KSPGMRES, ierr)
!!$    max_res = 60 !***
!!$    CALL KSPGMRESSetRestart(ksp, max_res, ierr)
!!$
!!$    rtol    = 1.d-5  !***
!!$    atol    = 1.d-15 !***
!!$    dtol    = 1.d5
!!$    max_ite = 240
!!$
!!$    CALL KSPSetTolerances(ksp, rtol, atol, dtol, max_ite, ierr)
!!$
!!$    CALL KSPGetPC(ksp, pc, ierr)
!!$    CALL PCSetType(pc, PCILU, ierr)
!!$    CALL PCFactorSetReuseOrdering(pc, PETSC_TRUE)
!!$    CALL PCFactorSetReuseFill(pc, PETSC_TRUE)
!!$    CALL PCFactorSetLevels(pc, 1);
!!$
!!$    !-------------------------
!!$    ! RHS ans solution vectors
!!$    !--------------------------------------------
!!$    CALL VecCreate(PETSC_COMM_WORLD, rhs, ierr)
!!$    CALL VecSetSizes(rhs, PETSC_DECIDE, N_dof, ierr)
!!$    CALL VecSetFromOptions(rhs, ierr)
!!$
!!$    CALL VecDuplicate(rhs, sol, ierr)
!!$
!!$    write(*,*) '*** PETSc Initialized ***'
!!$    !CALL KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD,ierr)
!!$
!!$   END SUBROUTINE init_PETSc
!!$   !========================
!!$
!!$   !=======================================
!!$   SUBROUTINE solve_lin_syst(MAt_fb, b, dt)
!!$   !=======================================
!!$
!!$     IMPLICIT NONE
!!$
!!$     TYPE(Matrice),      INTENT(IN) :: Mat_fb
!!$
!!$     REAL, DIMENSION(:), INTENT(INOUT) :: b
!!$     REAL, DIMENSION(:), INTENT(IN)    :: dt
!!$     !----------------------------------------
!!$
!!$     INTEGER, DIMENSION(:), ALLOCATABLE :: Id_col, col_dof
!!$      
!!$    
!!$
!!$     PetscScalar, DIMENSION(:), ALLOCATABLE :: M_val
!!$
!!$     PetscScalar :: v_val
!!$
!!$     PetscInt :: Nvar, row_p, nnz
!!$
!!$     PetscInt, PARAMETER :: one = 1
!!$      
!!$     PetscScalar, POINTER ::  sol_v(:)
!!$
!!$     PetscInt       :: ite
!!$      
!!$     INTEGER :: t0, t1
!!$
!!$     INTEGER :: i, i_dof, j, k, row, &
!!$                col_s, col_e, id_e, id_s, &
!!$                N_syst, N_dof, nnz_dof
!!$     !-----------------------------------------
!!$
!!$     ! Number of dof
!!$     N_dof = SIZE(b)
!!$
!!$     !------------------------------
!!$     ! COSTRUCTION of MATRIX and RHS
!!$     !-----------------------------------------------
!!$     DO i_dof = 1, N_dof
!!$         
!!$        nnz_dof = Mat_fb%JPosi(i_dof+1) - Mat_fb%JPosi(i_dof)
!!$
!!$        row = i_dof
!!$
!!$        ALLOCATE( Id_col(nnz_dof), col_dof(nnz_dof) )
!!$         
!!$        ALLOCATE( M_val(nnz_dof) )
!!$         
!!$        Id_col = (/ (i, i = Mat_fb%Jposi(i_dof), &
!!$                            Mat_fb%Jposi(i_dof+1)-1) /)
!!$
!!$        col_dof =  Mat_fb%Jvcell(Id_col)
!!$
!!$        ! Construct the row matrix from the Mat structure
!!$        DO j = 1, SIZE(Id_col)
!!$           M_val(j) = Mat_fb%Vals(1,1, Id_col(j) )
!!$           IF( col_dof(j) == i_dof ) THEN
!!$              M_val(j) = M_val(j) + 1.d0/dt(i_dof)
!!$           ENDIF
!!$        ENDDO
!!$
!!$                          ! Zero-start indexing
!!$        col_dof = col_dof - 1 
!!$
!!$                  ! Zero-start indexing
!!$        row = row - 1
!!$           
!!$        CALL MatSetValues(J_mat, one,row, nnz_dof,col_dof, &
!!$                          M_val(:), INSERT_VALUES, ierr)
!!$
!!$
!!$        DEALLOCATE( Id_col, col_dof, M_val )
!!$
!!$         ! --- Insertion of each column vector in the rhs ---         
!!$         v_val = b(i_dof)
!!$         
!!$         CALL VecSetValues(rhs, one,row, v_val, INSERT_VALUES, ierr)
!!$
!!$      ENDDO
!!$
!!$      CALL MatAssemblyBegin(J_Mat, MAT_FINAL_ASSEMBLY, ierr)
!!$      CALL MatAssemblyEnd(J_Mat, MAT_FINAL_ASSEMBLY, ierr)
!!$
!!$      !------------------------------
!!$      ! SOLUTION of the LINEAR SYSTEM
!!$      !-------------------------------------------------
!!$      CALL KSPSetOperators(ksp, J_Mat, J_mat, DIFFERENT_NONZERO_PATTERN, ierr)
!!$
!!$      CALL KSPSetFromOptions(ksp, ierr)
!!$      CALL KSPSetUp(ksp, ierr)
!!$
!!$      ! Solve the linear system
!!$      CALL KSPSolve(ksp, rhs, sol, ierr)
!!$
!!$      IF (ierr /= 0) THEN
!!$         WRITE(*,*) 'Catastrophic error: PETSc failed to solve the linear system'
!!$         WRITE(*,*) 'STOP!'
!!$         STOP
!!$      ENDIF
!!$
!!$      ! Repacking the solution and save it in b
!!$      CALL VecGetArrayF90(sol, sol_v, ierr)
!!$      b = sol_v
!!$      CALL VecRestoreArrayF90(sol, sol_v, ierr)
!!$
!!$      CALL KSPGetIterationNumber(ksp, ite)
!!$!      WRITE(*,*) '++++++++ ITE', ite, '++++++++'
!!$
!!$   END SUBROUTINE solve_lin_syst
!!$   !============================   

END MODULE HJ_WallDistance
