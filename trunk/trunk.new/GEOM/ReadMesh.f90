!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 R.Butel, R.Huart, A.Larat, B.Nkonga, M.Papin      */
!*             2009-2010   P. Jacq, R. Abgrall                               */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MODULE ReadMesh
  USE types_enumeres
  USE LesTypes
  USE LibMESh

  IMPLICIT NONE

  ! GMSH_ELE(i, j): i = #Ndofs, j = # Verts
  INTEGER, DIMENSION(14, 2), PARAMETER :: GMSH_ELE = &
           RESHAPE( (/ 2, 3, 4, 4, 8, 6, 5, 3, 6, 9, 10, 27, 18, 14, &
                       2, 3, 4, 4, 8, 6, 5, 2, 3, 4,  4,  8,  6,  5 /), (/14, 2/) )

!!$    !---------------------------------------------------------
!!$    GMSH_ELE(1,  :) = (/ 2, 2 /) ! 2-node line 
!!$    GMSH_ELE(2,  :) = (/ 3, 3 /) ! 3-node triangle
!!$    GMSH_ELE(3,  :) = (/ 4, 4 /) ! 4-node quadrangle
!!$    GMSH_ELE(4,  :) = (/ 4, 4 /) ! 4-node tetrahedron
!!$    GMSH_ELE(5,  :) = (/ 8, 8 /) ! 8-node hexahedron
!!$    GMSH_ELE(6,  :) = (/ 6, 6 /) ! 6-node prism
!!$    GMSH_ELE(7,  :) = (/ 5, 5 /) ! 5-node pyramid 
!!$    GMSH_ELE(8,  :) = (/ 3, 2 /) ! 3-node second order line
!!$    GMSH_ELE(9,  :) = (/ 6, 3 /) ! 6-node second order triangle
!!$    GMSH_ELE(10, :) = (/ 9, 4 /) ! 9-node second order quad
!!$    GMSH_ELE(11, :) = (/ 10, 4 /) ! 10-node second order tetrahedron
!!$    GMSH_ELE(12, :) = (/ 27, 8 /) ! 27-node second order hexahedron
!!$    GMSH_ELE(13, :) = (/ 18, 6 /) ! 18-node second order prism
!!$    GMSH_ELE(14, :) = (/ 14, 5 /) ! 14-node second order pyramid
!!$    !-----------------------------------------------------------

CONTAINS


  !>   DonMesh : Choix de la proc�dure de maillage et lecture de la g�om�trie
  !!
  !!-- Lecture du maillage, suivant diff�rents formats possibles

  !!-- Formats de maillage : valeurs autoris�es pour MeshType ::
  !!--     "AMDBA" -> ReadAMDBA2D_tri; extension .ambda
  !!--     "AMDBA4" -> ReadAMDBA2D_quad; extension .ambda
  !!--     "MESH" -- obligatoire pour le parallelisme; -> ReadMesh2D extension .pmsh
  !!--     "MESHEMC2" -> ReadMeshEMC2; extension .msh
  !!--     "MESHEMC2A" -> ReadMeshEMC2A; -- extension .meshb ou .mesh si .meshb n'existe pas
  !!--     "GEOM3D" -> ReadGeom3D; extension .geom
  !!--     "GMSH"   -> ReadMeshGMSH; extension .msh
  !!--     "GMSH2"  -> ReadMeshGMSH2; extension .msh (v2.0 format ASCII)
  !!--     "GMSH3D" -> ReadMeshGMSH2; idem en 3D

  !!-- Alloue ensuite des informations compl�mentaires sur le maillage, comme le degremax, le volume des �l�ments
  !!-- Puis �crit le maillage via writemesh (en cas de maillage adaptatif #ifdef)
  !!-- En 2D, trie nsfacfr pour que les deux sommets soient dans l'ordre de num�rotation
  !!-- v�rifie l'orientation des rectangles/pentagones 2D, ou corrige dans le cas de triangles
  SUBROUTINE DonMesh(Com, Mesh, DATA, nodelist)
    CHARACTER(LEN = *),                  PARAMETER :: mod_name = "DonMesh"
    TYPE(Donnees), INTENT(INOUT)                   :: DATA
    TYPE(MeshCom), INTENT(INOUT)                   :: Com
    TYPE(MeshDef), INTENT(INOUT)                   :: Mesh
    INTEGER, DIMENSION(:), INTENT(INOUT), OPTIONAL :: nodelist

    CHARACTER(LEN = 80)                            :: MeshType, MeshFile, PerFile
    REAL,    DIMENSION(:, : ), ALLOCATABLE         :: coor
    INTEGER, DIMENSION(: )   , ALLOCATABLE         :: NULoc
    REAL,    DIMENSION(: )   , ALLOCATABLE         :: g
    INTEGER                                        :: jt, k, kp1, Degre, is, ifac
    REAL                                           :: xg1, xg2, yG1, yG2
    INTEGER                                        :: isave

    CALL decoder_data_meshtype(Data%MeshType, Data%sel_MeshType)
    
    MeshType  = ADJUSTL(Data%MeshType)

    Mesh%Nordre = Data%MeshOrder

    PRINT *, mod_name, ": Mesh%Nordre=", Mesh%Nordre

    IF (.NOT. PRESENT(nodelist)) THEN
       MeshFile(1: ) = ADJUSTL(Data%PbName)

       IF (Com%Me == 0 ) THEN
          PRINT *, mod_name, " MeshFile==", TRIM(MeshFile), " MeshType", Data%sel_MeshType, TRIM(Data%MeshType)
       END IF

    ELSE

       IF (Com%Me == 0 ) THEN
          PRINT *, mod_name, " MeshType MURGE non defini (compiler en mode MURGE)"
       END IF
       CALL delegate_stop()

    END IF

    Mesh%Ndim = Data%Ndim

    SELECT CASE(Data%sel_MeshType)

!#############################################################
!######################## 2D 2D 2D ###########################
!#############################################################
       
    !======================
    CASE(cs_meshtype_amdba) ! OK 
    !======================
  
       MeshFile(1: ) = TRIM(MeshFile) // ".amdba"

       WRITE(6, *) mod_name, " Maillage de type ", TRIM(MeshType), " : ", TRIM(MeshFile)
       
       CALL ReadAMDBA2D_tri( Mesh, MeshFile )
            !^^^^^^^^^^^^^^

    !=======================
    CASE(cs_meshtype_amdba4) ! not tested
    !=======================

       MeshFile(1: ) = TRIM(MeshFile) // ".amdba"

       WRITE(6, *) mod_name, " Maillage de type ", TRIM(MeshType), " : ", TRIM(MeshFile)
       
       CALL ReadAMDBA2D_quad( Mesh, MeshFile )
            !^^^^^^^^^^^^^^^
  
    !=========================
    CASE(cs_meshtype_meshemc2) ! OK
    !=========================

       MeshFile(1: ) = TRIM(MeshFile) // ".mesh"

       WRITE(6, '(" * Maillage MeshEMC2 :")', ADVANCE = 'no')
       WRITE(6, *) TRIM(MeshFile)

       CALL  ReadMeshEMC2(Mesh, MeshFile)
             !^^^^^^^^^^^

    !==========================
    CASE(cs_meshtype_meshemc2a) !???
    !==========================

       MeshFile(1: ) = TRIM(MeshFile)

       WRITE(6, '(" * Maillage MeshEMC2 :")', ADVANCE = 'no')
       WRITE(6, *) TRIM(MeshFile)

       CALL  ReadMeshEMC2a(Mesh, MeshFile)
             !^^^^^^^^^^^^

       write(*,*) "Format non supporte *"

       CALL delegate_stop()


!#############################################################
!######################## 3D 3D 3D ###########################
!#############################################################

    !=======================
    CASE(cs_meshtype_geom3d) 
    !=======================

       MeshFile(1: ) = TRIM(MeshFile) // ".geom"
       
       WRITE(6, '(" * Maillage GEOM3D :")', ADVANCE = 'no')
       WRITE(6, *) TRIM(MeshFile)
       
       CALL ReadGeom3d(Mesh, MeshFile)
            !^^^^^^^^^

    !=======================          
    CASE(cs_meshtype_gmsh3d) ! not tested
    !=======================
       
       MeshFile(1: ) = TRIM(MeshFile) // ".msh"

       WRITE(6, '(" * Maillage GMSH 2.0 3D :")', ADVANCE = 'no')
       WRITE(6, *) TRIM(MeshFile)

       CALL ReadMeshGMSH2(Mesh, MeshFile)
            !^^^^^^^^^^^^

       write(*,*) "Format non supporte !"
       CALL delegate_stop()

!#############################################################
!######################## GENERIC ############################
!#############################################################

   !======================
    CASE(cs_meshtype_gmsh2) ! OK
   !======================

       MeshFile(1: ) = TRIM(MeshFile) // ".msh"
       
       WRITE(6, '(" * Maillage GMSH 2.0 2D :")', ADVANCE = 'no')
       
       WRITE(6, *) TRIM(MeshFile)
       
       CALL ReadMeshGMSH2(Mesh, MeshFile)
            !^^^^^^^^^^^^
          
    !=====================
    CASE(cs_meshtype_mesh) ! OK
    !=====================
       
       MeshFile(1: ) = TRIM(MeshFile) // ".pmsh"
       
       WRITE(6, '(" * Maillage MESH     :")', ADVANCE = 'no')
       WRITE(6, *) TRIM(MeshFile)
       
       CALL ReadMeshParallel( Com, Mesh, MeshFile )
            !^^^^^^^^^^^^^^^

    !===========
    CASE DEFAULT
    !===========

       WRITE(*, *) 'ERROR: unknown mesh format'
       STOP

    END SELECT

    IF (.NOT. ASSOCIATED(Mesh%Ndegre)) THEN
       PRINT *, mod_name, " : ERREUR : Mesh%Ndegre non alloue, MeshType==", TRIM(Data%MeshType)
       CALL delegate_stop()
    END IF

    ALLOCATE( Mesh%Nsommets( Mesh%Nelemt ) )
    
    ! determination du type des elements
    !-----------------------------------
    ALLOCATE( Mesh%EltType( Mesh%Nelemt ) )
    
    DO jt = 1, Mesh%Nelemt

       IF (Mesh%Ndim == 2) THEN
          
          SELECT CASE( Mesh%Ndegre(jt) )
          CASE(3)
             Mesh%EltType(jt)  = et_triaP1
             Mesh%Nsommets(jt) = 3
          CASE(4)
             Mesh%EltType(jt)  = et_QuadP1
             Mesh%Nsommets(jt) = 4
          CASE(6)
             Mesh%EltType(jt)  = et_triaP2
             Mesh%Nsommets(jt) = 3
          CASE(9)
             Mesh%EltType(jt)  = et_QuadP2
             Mesh%Nsommets(jt) = 4
          CASE DEFAULT
             PRINT *, mod_name, " : ERREUR : Element 2D inconnu Ndegre =", Mesh%Ndegre(jt)
             CALL delegate_stop()
          END SELECT
          
       ELSE
          
          SELECT CASE( Mesh%Ndegre(jt) )
          CASE(4)
             Mesh%EltType(jt)  = et_tetraP1
             Mesh%Nsommets(jt) = 4
          CASE(8)
             Mesh%EltType(jt)  = et_HexaP1
             Mesh%Nsommets(jt) = 8
          CASE(5)
             Mesh%EltType(jt)  = et_PyramP1
             Mesh%Nsommets(jt) = 5
          CASE(10)
             Mesh%EltType(jt)  = et_tetraP2
             Mesh%Nsommets(jt) = 4
          CASE(27)
             Mesh%EltType(jt)  = et_hexap2
             Mesh%Nsommets(jt) = 8
          CASE(14)
             Mesh%EltType(jt)  = et_PyramP2
             Mesh%Nsommets(jt) = 5

          CASE DEFAULT
             PRINT *, mod_name, " : ERREUR : Element 3D inconnu Ndegre =", Mesh%Ndegre(jt)
             CALL delegate_stop()
          END SELECT

       END IF
       
    END DO

    Mesh%NDegreMax   = MAXVAL(Mesh%Ndegre)
    Mesh%NSommetsMax = MAXVAL( Mesh%NSommets(:) )

    NULLIFY(Mesh%Coon)
    Mesh%Coon => Mesh%coor  

    IF (Data%Impre > 0) THEN
       PRINT *, mod_name, " FIN"
    END IF

  END SUBROUTINE DonMesh

  SUBROUTINE Savemesh2D3d(Mesh, FileName, The)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Savemesh2D3d"
    ! ****************
    ! variables d'appel
    TYPE(MeshDef), INTENT(IN)     :: Mesh
    CHARACTER(LEN = *), INTENT(IN)    :: FileName
    CHARACTER(LEN = *), INTENT(IN), OPTIONAL    :: The

    IF (  Mesh%Ndim == 2 ) THEN
       CALL writeMesh(TRIM(FileName) // ".meshb", &
            &  Mesh%Ndim, Ver = Mesh%coor, VerRef = Mesh%LogPt, &
            &  Tri = Mesh%Nu, TriRef = Mesh%Nu(1, : ),  &
            &  Edg = Mesh%NsFacFr, EdgRef = Mesh%LogFr)
    ELSE IF (Mesh%Ndim == 3 ) THEN
       IF (SIZE(Mesh%Nu, 1) == 4 ) THEN
          CALL writeMesh(TRIM(FileName) // ".meshb", &
               & Mesh%Ndim, Ver = Mesh%coor, VerRef = Mesh%LogPt, &
               & Tet = Mesh%Nu, TetRef = Mesh%Nu(1, : ),  &
               & Tri = Mesh%NsFacFr, TriRef = Mesh%LogFr)

       ELSE IF (SIZE(Mesh%Nu, 1) == 8 ) THEN
          IF (PRESENT(The) ) THEN
             CALL writeMesh(TRIM(FileName) // ".meshb", &
                  & Mesh%Ndim, Ver = Mesh%coor, VerRef = Mesh%LogPt, &
                  & Tet = Mesh%Nu, TetRef = Mesh%Nu(1, : ),  &
                  & Quad = Mesh%NsFacFr, QuadRef = Mesh%LogFrReel)
          ELSE
             CALL writeMesh(TRIM(FileName) // ".meshb", &
                  & Mesh%Ndim, Ver = Mesh%coor, VerRef = Mesh%LogPt, &
                  & Tet = Mesh%Nu, TetRef = Mesh%Nu(1, : ))
             ! & Tri = Mesh%NsFacFr, TriRef = Mesh%LogFrReel, &
          END IF
       ELSE
          PRINT *, mod_name, " ERREUR: NYIP/2"
          CALL delegate_stop()
       END IF
    ELSE
       PRINT *, mod_name, " ERREUR: Ndim invalide", Mesh%Ndim
       CALL delegate_stop()
    END IF
  END SUBROUTINE Savemesh2D3d


  !!-- lecture d'un maillage 3d
  !!-- remplit la structure mesh (de base)
  !#####################################
  SUBROUTINE ReadGeom3d( Mesh, FileName)
  !#####################################
    
    CHARACTER(LEN = *), PARAMETER     :: mod_name = "ReadGeom3d"

    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef),      INTENT(INOUT) :: Mesh
    CHARACTER(LEN = *), INTENT(IN)    :: FileName
    !variables locales
    INTEGER                           :: MyUnit, is, jt, ifac, i

    INTEGER                           :: Npoint, Nelemt, NFacFr, Nsmplx, Ndim, statut, NsmplxFr
    LOGICAL                           :: existe

    !***************************
    !  lecture de la geometrie
    !***************************

    MyUnit = 11

    !ouverture du fichier
    !--------------------
    INQUIRE(FILE = FileName, EXIST = existe)
    IF (.NOT. existe) THEN
       CALL info(mod_name, FileName, "geom")
       PRINT *, mod_name, " : ERREUR : le fichier ", TRIM(FileName), " n'existe pas"
       CALL delegate_stop()
    END IF

    OPEN(UNIT = MyUnit, FILE = FileName, IOSTAT = statut)
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " ouvrir ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    READ(MyUnit, *, IOSTAT = statut) Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr !*

    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour la 1ere ligne de ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    Npoint = Mesh%Npoint
    Nelemt = Mesh%Nelemt
    NFacFr = Mesh%NFacFr
    Ndim   = Mesh%Ndim

    !lecture des coord des sommets
    !-----------------------------
    WRITE(6, *) mod_name, " : lecture des coord des sommets ...."

    ALLOCATE( Mesh%coor(Ndim, Npoint)    )
    ALLOCATE( Mesh%LogElt(1: Mesh%Nelemt))
    ALLOCATE( Mesh%LogPt(1: Mesh%Npoint) )
    
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%coor",   SIZE(Mesh%coor))
       CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
       CALL alloc_allocate(mod_name, "mesh%logpt",  SIZE(Mesh%LogPt))
    END IF

    READ(MyUnit, *, IOSTAT = statut) ((Mesh%coor(i, is), i = 1, Ndim), is = 1, Npoint) !*


    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour ligne 'coor' de ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    Mesh%LogPt  = 0
    Mesh%LogElt = 0

    !lecture des numeros des sommets
    !-------------------------------

    ! Travail sur des tetra seulement i.e :
    ! 4 sommets par element,
    ! 3 sommets sur les faces frontieres
    
    Nsmplx   = 4    ! Ndim + 1
    NsmplxFr = 3    ! Ndim

    WRITE(6, *) mod_name, " : lecture des numeros des sommets ...."

    ALLOCATE( Mesh%Ndegre(Nelemt))
    ALLOCATE( Mesh%Nu(Nsmplx, Nelemt) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
       CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
    END IF

    Mesh%Ndegre = Nsmplx

    READ(MyUnit, *, IOSTAT = statut) ((Mesh%Nu(i, jt), i = 1, Mesh%Ndegre(jt)), jt = 1, Nelemt) !*

    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour ligne 'nu' de ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    !lecture des segments frontieres
    !-------------------------------
    WRITE(6, *) mod_name, " : lecture de la frontiere ...."
    
    IF ( Mesh%NFacFr /= 0 ) THEN
       
       ALLOCATE(Mesh%LogFr(NFacFr), Mesh%NsFacFr(NsmplxFr, NFacFr) )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
       END IF
       
       READ(MyUnit, *, IOSTAT = statut) (Mesh%LogFr(ifac), ifac = 1, NFacFr) !*

       IF (statut /= 0) THEN
          PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour ligne 'logfr' de ", TRIM(FileName)
          CALL delegate_stop()
       END IF

       READ(MyUnit, *, IOSTAT = statut) ((Mesh%NsFacFr(i, ifac), i = 1, NsmplxFr), ifac = 1, NFacFr) !*

       IF (statut /= 0) THEN
          PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour ligne 'nsfacfr' de ", TRIM(FileName)
          CALL delegate_stop()
       END IF
       
    END IF

    WRITE(6, *) mod_name, " : fin  .... ", MINVAL(Mesh%LogFr), MAXVAL(Mesh%LogFr)

    CLOSE(MyUnit)

  END SUBROUTINE ReadGeom3d
 !#########################

  !!-- Commence par lire les communications (casparall),
  !!-- puis calcule les communications de niveau 2 (plusieurs possibles par point) dans com%NPsnd2tot

  !!-- tous les points sont num�rot�s, de 1 � npsnd2tot pour les �metteurs, de npoint - nbrcv2tot + 1 � npoint pour les recepteurs,
  !!-- au milieu pour les autres; cela est fait dans le tableau perm, qui est recopi� dans com%ptsnd2; c'est une permutation
  !!-- idem pour com%ptrcv2

  !!-- Ensuite on compl�te des informations pour mesh
  !!-- � savoir : les coordonn�es des points
  !!-- lecture des coordonn�es des points (permut�s), sans contr�le
  !!-- puis, pour chaque �l�ment, lecture du degr� et de la contrainte
  !!-- puis lecture des segments fronti�re
  SUBROUTINE ReadMeshParallel(Com, Mesh, FileName)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ReadMeshParallel"
    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef), INTENT(INOUT)     :: Mesh
    TYPE(MeshCom), INTENT(INOUT)     :: Com
    CHARACTER(LEN = *), INTENT(IN) :: FileName

    !variables locales
    INTEGER     :: MyUnit, is, jt, ifac, k, j, NbTot, iv
    LOGICAL, DIMENSION(: ), ALLOCATABLE :: Placed
    INTEGER, DIMENSION(: ), ALLOCATABLE :: Perm, Nu
    LOGICAL :: existe
    INTEGER :: statut
    INTEGER :: DegreMax, DegreMaxFr, degreFr
    !***************************
    !  lecture de la geometrie
    !***************************

    MyUnit = 11

    !ouverture du fichier

    INQUIRE(FILE = FileName, EXIST = existe)

    IF (.NOT. existe) THEN
       CALL info(mod_name, FileName, "pmsh")
       PRINT *, mod_name, " :: ERREUR : il n'y a pas de fichier ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    OPEN(UNIT = MyUnit, FILE = FileName, IOSTAT = statut)

    IF (statut /= 0) THEN
       CALL info(mod_name, FileName, "pmsh")
       PRINT *, mod_name, " :: ERREUR : impossible d'ouvrir ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    CALL casparall(Com, MyUnit) !-- ReadMeshParallel

    READ(MyUnit, *, IOSTAT = statut) Mesh%Ndim, Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr, DegreMax, DegreMaxFr

    IF (statut /= 0) THEN
       CALL info(mod_name, FileName, "pmsh")
       PRINT *, mod_name, " :: ERREUR : impossible de lire la ligne Ndim/Npoint/Nelemt/NFacFr de ", TRIM(FileName), statut
       CALL delegate_stop()
    END IF

    !******************************************************************************************************
    ! On fait le traitement pour les noeuds. On pourra le refaire pour les �l�ments si on on en a besoin
    ALLOCATE(Perm(1: Mesh%Npoint), Placed(1: Mesh%Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "perm", SIZE(Perm))
       CALL alloc_allocate(mod_name, "placed", SIZE(Placed))
    END IF
    ! On met les comm send au d�but et les Recv � la fin (pour pouvoir les �carter facilement le cas �cheant car
    ! ce sont des points qui ne sont pas dans l'int�rieur du domaine).
    Placed = .FALSE.
    Perm = 0 ! PERM(Oldnum) = NewNum
    ! ATTENTION: il est possible d'avoir plusieurs �missions � partir d'un point !!!!!!!
    NbTot = 0
    DO iv = 1, Com%Ndomv
       DO j = 1, Com%NPsnd2(iv)
          is = Com%PTsnd2(j, iv)
          IF (.NOT. Placed(is)) THEN
             NbTot = NbTot + 1
             Perm(is) = NbTot
             Placed(is) = .TRUE.
          END IF
       END DO
    END DO
    Com%NPsnd2tot = NbTot
    ! PAR contre, on DOIT avoir une seule r�ception par point
    NbTot = 0
    Com%NPrcv2tot = SUM(Com%NPrcv2)
    DO iv = Com%Ndomv, 1, -1
       DO j = Com%NPrcv2(iv), 1, -1
          is = Com%PTrcv2(j, iv)
          NbTot = NbTot + 1
          Perm(is) = Mesh%Npoint + 1 - NbTot
          IF (Placed(is)) THEN
             PRINT *, mod_name, " ERREUR : Noeud rcv deja trie....!!!!!"
             CALL delegate_stop()
          END IF
          Placed(is) = .TRUE.
       END DO
    END DO
    IF (NbTot /= Com%NPrcv2tot) THEN
       PRINT *, mod_name, " ERREUR : Probleme dans le reordering des comm recv ", NbTot, Com%NPrcv2tot
       CALL delegate_stop()
    END IF
    IF (COUNT(Placed) /= Com%NPsnd2tot + Com%NPrcv2tot) THEN
       PRINT *, mod_name, " ERREUR : Mauvais compte d'overlaps"
       CALL delegate_stop()
    END IF

    NbTot = Com%NPsnd2tot
    DO is = 1, Mesh%Npoint
       IF (.NOT. Placed(is)) THEN
          NbTot = NbTot + 1
          Perm(is) = NbTot
          Placed(is) = .TRUE.
       END IF
    END DO
    IF (NbTot + Com%NPrcv2tot /= Mesh%Npoint ) THEN
       PRINT *, mod_name, " ERREUR : Probleme dans le reordering", NbTot + Com%NPrcv2tot, Mesh%Npoint
       CALL delegate_stop()
    END IF
    DEALLOCATE(Placed)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "placed")
    END IF

    DO iv = 1, Com%Ndomv
       Com%PTsnd2(1: Com%NPsnd2(iv), iv) = Perm(Com%PTsnd2(1: Com%NPsnd2(iv), iv))
       Com%PTrcv2(1: Com%NPrcv2(iv), iv) = Perm(Com%PTrcv2(1: Com%NPrcv2(iv), iv))
    END DO
    ! Fin du calcul de la permutation
    !******************************************************************************************************

#ifdef ORDRE_ELEVE
    ! P. JACQ : SegPerm est juste temporaire, il est utilise 
    ! et desalloue dans readmeshseg
    ALLOCATE( Com%SegPerm(1:Mesh%Npoint) )
    com%SegPerm = Perm
    IF (see_alloc) CALL alloc_allocate(mod_name, "com%segperm", SIZE(Com%SegPerm))
#endif

    !allocation memoire
    ALLOCATE(  Mesh%coor(1: Mesh%Ndim, 1: Mesh%Npoint) )
    ALLOCATE(  Mesh%Nu(1: DegreMax, 1: Mesh%Nelemt)   ) !!!!!!!!!!!!!!!!!
    ALLOCATE(  Mesh%Ndegre(1: Mesh%Nelemt) )
    ALLOCATE(  Mesh%LogElt(1: Mesh%Nelemt), Mesh%LogPt(1: Mesh%Npoint ) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%coor", SIZE(Mesh%coor))
       CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
       CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
       CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
       CALL alloc_allocate(mod_name, "mesh%logpt", SIZE(Mesh%LogPt))
    END IF
#ifdef ORDRE_ELEVE
    Mesh%Ndegre = DegreMax
#endif
    !lecture des coord des sommets
    DO is = 1, Mesh%Npoint
       READ(MyUnit, *, IOSTAT = statut) Mesh%coor(1: Mesh%Ndim, Perm(is))
       IF (statut /= 0) THEN
          PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour la ligne ", &
               & is + 1, " de ", TRIM(FileName)
          CALL delegate_stop()
       END IF
    END DO
    !lecture des numeros des sommets
    ALLOCATE(Nu(DegreMax))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nu", SIZE(Nu))
    END IF
    Mesh%LogPt = 0
    DO jt = 1, Mesh%Nelemt
       READ(MyUnit, *) Mesh%Ndegre(jt),  Mesh%LogElt(jt)
       READ(MyUnit, *) ( Nu(k), k = 1, Mesh%Ndegre(jt) )
       Mesh%Nu(1: Mesh%Ndegre(jt), jt) = Perm(Nu(1: Mesh%Ndegre(jt)))
       Mesh%LogPt(Mesh%Nu(1: Mesh%Ndegre(jt), jt)) = MAX(Mesh%LogPt(Mesh%Nu(1: Mesh%Ndegre(jt), jt)), Mesh%LogElt(jt))
    END DO
    DEALLOCATE(Nu)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "nu")
    END IF
    !lecture des segments frontieres
    IF ( Mesh%NFacFr /= 0 ) THEN
       ALLOCATE(Nu(DegreMaxFr))
       ALLOCATE(Mesh%LogFr(Mesh%NFacFr))

       ALLOCATE(Mesh%NsFacFr(DegreMaxFr,Mesh%NFacFr))
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "nu", SIZE(Nu))
          CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
          CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
       END IF

       DO ifac = 1, Mesh%NFacFr
          READ(MyUnit, *) Mesh%LogFr(ifac), degreFr
          READ(MyUnit, *) Nu(1: degreFr)
          Mesh%NsFacFr(1:degreFr,ifac)=Perm(Nu(1: degreFr))
       END DO
       DEALLOCATE(Nu)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nu")
       END IF
    END IF

    CLOSE(MyUnit)
    DEALLOCATE(Perm)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "perm")
    END IF

  END SUBROUTINE ReadMeshParallel

  !!-- concerne donc la communication parall�le, il y a 3 paires de sender-receiver (i.e. 3 niveaux de communication)
  !!-- appel�e par ReadMeshParallel

  !!-- lit sur l'unit� 11, r�ouverte, maillage au format MESH g�n�r� par interf, des donn�es qui sont plac�es dans la structure Com

  SUBROUTINE casparall(Com, MeshFileUnit)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "casparall"
    TYPE(MeshCom), INTENT(OUT)     :: Com
    INTEGER,       INTENT(IN)      :: MeshFileUnit
    INTEGER :: iv, j
    INTEGER :: statut
    INTEGER :: check_ntasks
    CHARACTER(LEN = 1000) :: fnm, try

    READ( MeshFileUnit, FMT = '(A)', IOSTAT = statut)  try
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut==", statut, " sur la 1ere ligne de ", MeshFileUnit
       INQUIRE(UNIT = MeshFileUnit, NAME = fnm)
       PRINT *, mod_name, " fnm==", TRIM(fnm)
       CALL delegate_stop()
    END IF

    INQUIRE(UNIT = MeshFileUnit, NAME = fnm)

    READ( try, *, IOSTAT = statut)  Com%Ndomv, check_ntasks !-- 1er essai, essayer de lire le nombre de t�ches, pour coh�rence
    IF (statut /= 0) THEN
       READ( try, *, IOSTAT = statut)  Com%Ndomv !-- anciens fichiers
    ELSE
       IF (.NOT. Com%GetNTasks) THEN
          IF (check_ntasks /= Com%NTasks) THEN
             IF (check_ntasks == 0) THEN
                WRITE(try, *) mod_name, " :: ATTENTION : check_ntasks==0, *.pmsh generes avec une version boguee de Interf"
                CALL log_record(try)
             ELSE
                PRINT *, mod_name, " :: ERREUR : check_ntasks==", check_ntasks, " NTasks==", Com%NTasks
                CALL delegate_stop()
             END IF
          END IF
       ELSE
          Com%NTasks = check_ntasks
       END IF
    END IF
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut==", statut, " sur la 1ere ligne de ", MeshFileUnit
       PRINT *, mod_name, " ", TRIM(try)
       INQUIRE(UNIT = MeshFileUnit, NAME = fnm)
       PRINT *, mod_name, " fnm==", TRIM(fnm)
       CALL delegate_stop()
    END IF

    READ( MeshFileUnit, *, IOSTAT = statut)  Com%Nsnd2Max, Com%Nrcv2Max
    IF (statut /= 0) THEN
       PRINT *, mod_name, " ERREUR ", statut, " ligne Com%Nsnd2Max, Com%Nrcv2Max"
       CALL delegate_stop()
    END IF

    IF ( Com%Ndomv > 0 ) THEN
       ALLOCATE(Com%Jdomv(Com%Ndomv) )
       ALLOCATE(Com%NPsnd2(Com%Ndomv), Com%NPrcv2(Com%Ndomv) )
       ALLOCATE(Com%PTsnd2(Com%Nsnd2Max, Com%Ndomv) )
       ALLOCATE(Com%PTrcv2(Com%Nrcv2Max, Com%Ndomv) )
       ALLOCATE(Com%Position(Com%NTasks) )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "com%jdomv", SIZE(Com%Jdomv))
          CALL alloc_allocate(mod_name, "com%npsnd2", SIZE(Com%NPsnd2))
          CALL alloc_allocate(mod_name, "com%nprcv2", SIZE(Com%NPrcv2))
          CALL alloc_allocate(mod_name, "com%ptsnd2", SIZE(Com%PTsnd2))
          CALL alloc_allocate(mod_name, "com%ptrcv2", SIZE(Com%PTrcv2))
          CALL alloc_allocate(mod_name, "com%position", SIZE(Com%Position))
       END IF
       ! NdomV =  Nombre de sous-domaines voisins du domaine courant

       DO iv = 1, Com%Ndomv
          READ( MeshFileUnit, *, IOSTAT = statut) Com%Jdomv(iv)
          IF (statut /= 0) THEN
             PRINT *, mod_name, " ERREUR ", statut, " ligne Com%Jdomv(iv)", iv
             CALL delegate_stop()
          END IF
          Com%Position(Com%Jdomv(iv))    = iv ! Numero du ieme sous-domaine voisin
          ! Sommets sur lesquels il y a des communications de niveau 2
          ! ---------------------------------------------------------
          READ( MeshFileUnit, *)    Com%NPsnd2(iv),  Com%NPrcv2(iv)
          READ( MeshFileUnit, *) (Com%PTsnd2(j, iv), j = 1, Com%NPsnd2(iv))
          READ( MeshFileUnit, *) (Com%PTrcv2(j, iv), j = 1, Com%NPrcv2(iv))
       END DO

       Com%MaxLengthSnd2 = MAXVAL(Com%NPsnd2)
       Com%MaxLengthRcv2 = MAXVAL(Com%NPrcv2)
    END IF

    IF (Com%Me == 0) THEN
       PRINT *, mod_name, " fini"
    END IF

  END SUBROUTINE casparall



  !!@author Version B. Nkonga (FBox + corrections)
  !!-- Appelle getmeshinfo, puis lit les infos. dans mesh � partir de loadmesh, avec 2 cas diff�rents : 2D et 3D
  SUBROUTINE ReadMeshEMC2a(Mesh, FileName)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ReadMeshEMC2a"
    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef), INTENT(INOUT)     :: Mesh
    CHARACTER(LEN = *), INTENT(IN)    :: FileName
    !variables locales
    INTEGER     :: MyUnit,  jt, ifac, ifr, jq
    INTEGER     :: Ndim, Nsmplx
    INTEGER     :: Nt, Nq, Ntet, Nhex, NsegFr, NewNsegFr

    INTEGER, DIMENSION(:, : ), POINTER     ::  Nut, Nuq, NuTet, NuHex, NsFacFr
    INTEGER :: DegreMax
    INTEGER, DIMENSION(GmfMaxSol + 2, GmfMaxKwd) :: KwdTab
    !***************************
    !  lecture de la geometrie
    !***************************
    MyUnit = 11
    DegreMax = 0

    WRITE(6, *) mod_name, " Lecture Mesh !  !!!!!!!!!!!!!!!!!"
    !ouverture du fichier
    Nt = 0
    Nq = 0

    CALL getMeshInfo(FileName, MyUnit, Mesh%Ndim, KwdTab)

    Mesh%Npoint = KwdTab(1, GmfVertices)
    Nt          = KwdTab(1, GmfTriangles)
    Nq          = KwdTab(1, Gmfquadrilaterals)
    Ntet        = KwdTab(1, GmfTetrahedra)
    Nhex        = KwdTab(1, GmfHexahedra)
    NsegFr      = KwdTab(1, GmfEdges)

    Ndim          = Mesh%Ndim
    Nsmplx        = Ndim

    NULLIFY(Nut)
    NULLIFY(Nuq)
    NULLIFY(NuTet)
    NULLIFY(NuHex)
    NULLIFY(NsFacFr)

    WRITE(6, *) mod_name, " Ns, Nt, Nq, Ntet, Nhexa, NsegFr = ", Mesh%Npoint, Nt, Nq, Ntet, Nhex, NsegFr

    IF (NsegFr < 0 .OR. NsegFr > 1000000000) THEN
       PRINT *, mod_name, " ERREUR :: NsegFr invalide", NsegFr
       CALL delegate_stop()
    END IF
    !allocation memoire
    ALLOCATE(  Mesh%coor(1: Mesh%Ndim, Mesh%Npoint), Mesh%LogPt(1: Mesh%Npoint ) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%coor", SIZE(Mesh%coor))
       CALL alloc_allocate(mod_name, "mesh%logpt", SIZE(Mesh%LogPt))
    END IF

    IF (NsegFr > 0) THEN
       ALLOCATE(NsFacFr(Mesh%Ndim + 1, NsegFr))
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "nsfacfr", SIZE(NsFacFr))
       END IF
    END IF

    IF (Nt > 0) THEN
       ALLOCATE( Nut(4, Nt) )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "nut", SIZE(Nut))
       END IF
    END IF

    IF (Nq > 0) THEN
       ALLOCATE( Nuq(5, Nq)  )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "nuq", SIZE(Nuq))
       END IF
    END IF

    IF (Ntet > 0) THEN
       ALLOCATE( NuTet(5, Ntet)  )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "nutet", SIZE(NuTet))
       END IF
    END IF

    IF (Nhex > 0) THEN
       ALLOCATE( NuHex(9, Nhex)  )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "nuhex", SIZE(Nuhex))
       END IF
    END IF

    SELECT CASE(Mesh%Ndim)
    CASE(2) !-- ndim
       CALL loadMesh(MyUnit, Mesh%coor, Mesh%LogPt, Tri = Nut, Edg = NsFacFr)

       Mesh%Nelemt = Nt + Nq
       DegreMax    = Mesh%Ndim + 1
       IF (Nq > 0 .AND. Mesh%Ndim == 2 ) THEN
          DegreMax = DegreMax + 1
       END IF
       !allocation memoire
       ALLOCATE(  Mesh%Nu(DegreMax, Mesh%Nelemt)   )
       ALLOCATE(  Mesh%Ndegre(Mesh%Nelemt) )
       ALLOCATE(  Mesh%LogElt(1: Mesh%Nelemt) )
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
          CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
          CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
       END IF
       Mesh%LogPt = 0
       DO jt = 1, Nt
          Mesh%Ndegre(jt) = 3
          Mesh%Nu(1: 3, jt) = Nut(1: 3, jt)
          Mesh%LogElt(jt) = Nut(4, jt)
       END DO
       DO jq = 1, Nq
          jt = jq + Nt
          Mesh%Ndegre(jt) = 4
          Mesh%Nu(1: 4, jt) = Nuq(1: 4, jq)
          Mesh%LogElt(jt) = Nuq(5, jq)
       END DO

       NewNsegFr = 0
       DO ifac = 1, NsegFr
          IF ( NsFacFr(3, ifac) /= 0 ) THEN
             NewNsegFr = NewNsegFr + 1
          END IF
       END DO
       Mesh%NFacFr = NewNsegFr
       IF ( Mesh%NFacFr /= 0 ) THEN
          ALLOCATE(Mesh%LogFr(Mesh%NFacFr))
          ALLOCATE(Mesh%NsFacFr(2, Mesh%NFacFr))
          IF (see_alloc) THEN
             CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
             CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
          END IF
       END IF
       ifr  = 0
       DO ifac = 1, NsegFr
          IF ( NsFacFr(3, ifac) /= 0 ) THEN
             ifr = ifr + 1
             Mesh%NsFacFr(1: 2, ifr) = NsFacFr(1: 2, ifac)
             Mesh%LogFr(ifr)       = NsFacFr(3, ifac)
          END IF
       END DO

    CASE(3) !-- ndim

       IF ( Ntet > 0 .AND. Nhex == 0 ) THEN
          CALL loadMesh(MyUnit, Mesh%coor, Mesh%LogPt, Tet = NuTet, Tri = Nut)
          Mesh%Nelemt = Ntet
          DegreMax    = Mesh%Ndim + 1
          !allocation memoire
          ALLOCATE(  Mesh%Nu(Ndim + 1, Mesh%Nelemt)   )
          ALLOCATE(  Mesh%Ndegre(Mesh%Nelemt) )
          ALLOCATE(  Mesh%LogElt(1: Mesh%Nelemt) )
          IF (see_alloc) THEN
             CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
             CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
             CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
          END IF
          Mesh%LogPt  = 0
          Mesh%Ndegre = Mesh%Ndim + 1
          DO jt = 1, Ntet
             Mesh%Nu(1: Ndim + 1, jt) = NuTet(1: Ndim + 1, jt)
             Mesh%LogElt(jt)      = NuTet(Ndim + 2, jt)
          END DO
          !OFF Mesh%LogElt = 0
#ifdef TURBULENCE_SA
          Mesh%LogElt = 0
          WRITE(6, *) mod_name, " ATTENTION, Mesh%LogElt = 0 ???!!!"
#endif
          WRITE(6, *) mod_name, " mise a jour de la frontiere ...."
          NewNsegFr   = 0
          DO ifac = 1, Nt
             IF ( Nut(Ndim + 1, ifac) >= 0 ) THEN
                NewNsegFr = NewNsegFr + 1
             END IF
          END DO
          Mesh%NFacFr = NewNsegFr
          IF ( Mesh%NFacFr /= 0 ) THEN
             ALLOCATE(Mesh%LogFr(Mesh%NFacFr))
             ALLOCATE(Mesh%NsFacFr(Mesh%Ndim, Mesh%NFacFr))
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
                CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
             END IF
          END IF
          ifr  = 0
          DO ifac = 1,  Nt
             IF ( Nut(Ndim + 1, ifac) >= 0 ) THEN
                ifr = ifr + 1
                Mesh%NsFacFr(1: Ndim, ifr) = Nut(1: Ndim, ifac)
                Mesh%LogFr(ifr)          = Nut(Ndim + 1, ifac)
             END IF
          END DO
          IF ( MINVAL(Mesh%LogFr) == 0 ) THEN
             WRITE(6, *) mod_name, " **************LogiFr == 0 ************"
             WRITE(6, *) " On decale tout de 1"
             Mesh%LogFr = Mesh%LogFr + 1
          END IF

          WRITE(6, *) mod_name, " fin  .... ", MINVAL(Mesh%LogFr), MAXVAL(Mesh%LogFr)

       ELSE IF ( Ntet == 0 .AND. Nhex > 0 ) THEN

          CALL loadMesh(MyUnit, Mesh%coor, Mesh%LogPt, HeXa = NuHex, Quad = Nuq)
          Mesh%Nelemt = Nhex
          DegreMax    = 8
          !allocation memoire
          ALLOCATE(  Mesh%Nu(DegreMax, Mesh%Nelemt)   )
          ALLOCATE(  Mesh%Ndegre(Mesh%Nelemt) )
          ALLOCATE(  Mesh%LogElt(1: Mesh%Nelemt) )
          IF (see_alloc) THEN
             CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
             CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
             CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
          END IF
          Mesh%LogPt  = 0
          Mesh%Ndegre = DegreMax

          DO jt = 1, NHex
             Mesh%Nu(:, jt)     = NuHex(1: DegreMax, jt)
             Mesh%LogElt(jt)   = NuHex(DegreMax + 1, jt)
          END DO
          !OFF Mesh%LogElt = 0
          WRITE(6, *) mod_name, " mise a jour de la frontiere ...."
          NewNsegFr = 0
          DegreMax  = 4
          DO ifac = 1, Nq
             IF ( Nuq(DegreMax + 1, ifac) >= 0 ) THEN
                NewNsegFr = NewNsegFr + 1
             END IF
          END DO
          Mesh%NFacFr = NewNsegFr
          WRITE(6, *) mod_name, " mise a jour de la frontiere .... ", Mesh%NFacFr, DegreMax
          IF ( Mesh%NFacFr /= 0 ) THEN
             ALLOCATE(Mesh%LogFr(Mesh%NFacFr))
             ALLOCATE(Mesh%NsFacFr(DegreMax, Mesh%NFacFr))
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
                CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
             END IF
          END IF
          ifr  = 0
          DO ifac = 1,  Nq
             IF ( Nuq(DegreMax + 1, ifac) >= 0 ) THEN
                ifr = ifr + 1
                Mesh%NsFacFr(:, ifr) = Nuq(1: DegreMax, ifac)
                Mesh%LogFr(ifr)     = Nuq(DegreMax + 1, ifac)
             END IF
          END DO
          IF ( MINVAL(Mesh%LogFr) == 0 ) THEN
             WRITE(6, *) mod_name, " **************LogiFr == 0 ************"
             WRITE(6, *) " On decale tout de 1"
             Mesh%LogFr = Mesh%LogFr + 1
          END IF

          WRITE(6, *) mod_name, " fin  .... ", MINVAL(Mesh%LogFr), MAXVAL(Mesh%LogFr)

       ELSE

          PRINT *, mod_name, " :: ERREUR : cas NIP, Ntet, Nhex==", Ntet, Nhex !-- CCE CCE
          CALL delegate_stop()

       END IF

    END SELECT

    IF (ASSOCIATED(Nut)) THEN
       DEALLOCATE(Nut)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nut")
       END IF
    END IF
    IF (ASSOCIATED(NuTet)) THEN
       DEALLOCATE(NuTet)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nutet")
       END IF
    END IF
    IF (ASSOCIATED(NuHex)) THEN
       DEALLOCATE(NuHex)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nuhex")
       END IF
    END IF
    IF (ASSOCIATED(Nuq)) THEN
       DEALLOCATE(Nuq)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nuq")
       END IF
    END IF
    IF (ASSOCIATED(NsFacFr)) THEN
       DEALLOCATE(NsFacFr)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nsfacfr")
       END IF
    END IF
  END SUBROUTINE ReadMeshEMC2a

  !!@author Version de M. Papin (FBox + corrections)

  !!-- Lecture, toujours sur une unit� num�rot�e 11, avec un OPEN sans contr�le
  !!-- en boucle
  !!-- d'un blanc suivi d'un motcl�, qui peut �tre :
  !!-- Dimension, Vertices, Edges, Triangles, Quadrandles, End
  !!-- et, en fonction du mot cl�, les donn�es correspondantes dans mesh
  !!-- Cela permet de lire � la fois des triangles et des rectangles
  SUBROUTINE ReadMeshEMC2(Mesh, FileName)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ReadMeshEMC2"
    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef), INTENT(INOUT)     :: Mesh
    CHARACTER(LEN = *), INTENT(IN)    :: FileName
    !variables locales
    INTEGER     :: MyUnit, is, jt, ifac, ifr, jq
    INTEGER     :: Nt, Nq, NsegFr, NewNsegFr

    CHARACTER(LEN = 30)                        :: MotCle
    INTEGER, DIMENSION(:, : ), POINTER     ::  Nut, Nuq
    INTEGER, DIMENSION(:, : ), ALLOCATABLE ::  NsFacFr
    INTEGER, DIMENSION(: ), ALLOCATABLE     ::  LogFac, Logt, Logq
    REAL, DIMENSION(1: Mesh%Ndim) :: c__coor !-- [KADNA-PROTECTED]
    INTEGER :: DegreMax
    LOGICAL :: existe
    INTEGER :: statut
    INTEGER :: np
    !***************************
    !  lecture de la geometrie
    !***************************
    PRINT *, mod_name, " :: DEBUT"
    MyUnit = 11
    DegreMax = 0

    NULLIFY(Nut)
    NULLIFY(Nuq)

    INQUIRE(FILE = FileName, EXIST = existe)
    IF (.NOT. existe) THEN
       CALL info(mod_name, FileName, "msh")
       PRINT *, mod_name, " :: ERREUR : pas de fichier ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    !ouverture du fichier
    Nt = 0
    Nq = 0
    OPEN(UNIT = MyUnit, FILE = FileName, IOSTAT = statut)
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour ouvrir ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    READ(MyUnit, *, IOSTAT = statut) !-- la version du [g�n�rateur de] maillage
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour la 1ere ligne de ", TRIM(FileName)
       CALL delegate_stop()
    END IF
    READ(MyUnit, *) MotCle

    NewNsegFr = -1

    DO
       PRINT *, mod_name, " MotCle==", TRIM(MotCle)
       SELECT CASE(TRIM(MotCle))
       CASE("Dimension")
          READ(MyUnit, *) Mesh%Ndim !-- [KADNA-PROTECTED]
          READ(MyUnit, *) MotCle

       CASE("Vertices")
          READ(MyUnit, *) Mesh%Npoint !-- [KADNA-PROTECTED]
          np = Mesh%Npoint
          PRINT *, mod_name, " Mesh%Npoint==", np
          !allocation memoire
          ALLOCATE(  Mesh%coor(1: Mesh%Ndim, Mesh%Npoint) )
          IF (see_alloc) THEN
             CALL alloc_allocate(mod_name, "mesh%coor", SIZE(Mesh%coor))
          END IF

          !lecture des coord des sommets
          DO is = 1, Mesh%Npoint
             READ(MyUnit, *, IOSTAT = statut) c__coor(1: Mesh%Ndim)
             Mesh%coor(1: Mesh%Ndim, is) = c__coor
             IF (statut /= 0) THEN
                PRINT *, mod_name, " :: ERREUR : statut == ", statut, &
                     & " pour la ligne ", is + 1, " de ", TRIM(FileName)
                CALL delegate_stop()
             END IF
          END DO
          READ(MyUnit, *) MotCle

       CASE("Edges")
          READ(MyUnit, *) NsegFr
          NewNsegFr  = 0
          ALLOCATE(NsFacFr(2, NsegFr), LogFac(NsegFr))
          IF (see_alloc) THEN
             CALL alloc_allocate(mod_name, "nsfacfr", SIZE(NsFacFr))
             CALL alloc_allocate(mod_name, "logfac", SIZE(LogFac))
          END IF
          DO ifac = 1, NsegFr
             READ(MyUnit, *) NsFacFr(1, ifac), NsFacFr(2, ifac), LogFac(ifac)
             IF ( LogFac(ifac) /= 0 ) THEN
                NewNsegFr = NewNsegFr + 1
             END IF
          END DO
          READ(MyUnit, *) MotCle

       CASE("Triangles")
          IF ( Mesh%Ndim == 2 ) THEN
             READ(MyUnit, *) Nt
             ALLOCATE( Nut(3, Nt), Logt(Nt))
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "nut", SIZE(Nut))
                CALL alloc_allocate(mod_name, "logt", SIZE(Logt))
             END IF
             DO jt = 1, Nt
                READ(MyUnit, *) Nut(1, jt), Nut(2, jt), Nut(3, jt), Logt(jt)
             END DO
             READ(MyUnit, *) MotCle
             DegreMax = MAX(DegreMax, 3)
          END IF
       CASE("Quadrangles", "Quadrilaterals")
          READ(MyUnit, *) Nq
          ALLOCATE( Nuq(4, Nq), Logq(Nq) )
          IF (see_alloc) THEN
             CALL alloc_allocate(mod_name, "nuq", SIZE(Nuq))
             CALL alloc_allocate(mod_name, "logq", SIZE(Logq))
          END IF
          DO jt = 1, Nq
             READ(MyUnit, *) Nuq(1, jt), Nuq(2, jt), Nuq(3, jt), Nuq(4, jt), Logq(jt)
          END DO
          READ(MyUnit, *) MotCle
          DegreMax = MAX(DegreMax, 4)
       CASE("End")
          EXIT

       CASE DEFAULT
          WRITE(6, *) mod_name, " ERREUR : Mot cle", TRIM(MotCle), " non prevu"
          CALL delegate_stop()
       END SELECT
    END DO
    CLOSE(MyUnit)
    PRINT *, mod_name, " :: INTER"


    Mesh%Nelemt = Nt + Nq

    !allocation memoire
    ALLOCATE(  Mesh%Nu(DegreMax, Mesh%Nelemt)   )
    ALLOCATE(  Mesh%Ndegre(Mesh%Nelemt) )
    ALLOCATE(  Mesh%LogElt(1: Mesh%Nelemt), Mesh%LogPt(1: Mesh%Npoint ) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
       CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
       CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
       CALL alloc_allocate(mod_name, "mesh%logpt", SIZE(Mesh%LogPt))
    END IF
    Mesh%LogPt = 0
    DO jt = 1, Nt
       Mesh%Ndegre(jt) = 3
       Mesh%Nu(1: 3, jt) = (/ Nut(1, jt), Nut(2, jt), Nut(3, jt) /)
       Mesh%LogElt(jt) = Logt(jt)
       Mesh%LogPt(Mesh%Nu(1: 3, jt)) = MAX(Mesh%LogPt(Mesh%Nu(1: 3, jt)), Mesh%LogElt(jt))
    END DO
    DO jq = 1, Nq
       jt = jq + Nt
       Mesh%Ndegre(jt) = 4
       Mesh%Nu(1: 4, jt) = (/ Nuq(1, jq), Nuq(2, jq), Nuq(3, jq), Nuq(4, jq) /)
       Mesh%LogElt(jt) = Logq(jq)
       Mesh%LogPt(Mesh%Nu(1: 4, jt)) = MAX(Mesh%LogPt(Mesh%Nu(1: 4, jt)), Mesh%LogElt(jt))
    END DO
    IF (ASSOCIATED(Nut)) THEN
       DEALLOCATE(Nut, Logt)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nut")
          CALL alloc_deallocate(mod_name, "logt")
       END IF
    END IF
    IF (ASSOCIATED(Nuq)) THEN
       DEALLOCATE(Nuq, Logq)
       IF (see_alloc) THEN
          CALL alloc_deallocate(mod_name, "nuq")
          CALL alloc_deallocate(mod_name, "logq")
       END IF
    END IF


    IF (NewNsegFr < 0) THEN
       PRINT *, mod_name, " :: ERREUR : NewNsegFr non initialise"
       CALL delegate_stop()
    END IF

    Mesh%NFacFr = NewNsegFr
    IF ( Mesh%NFacFr /= 0 ) THEN
       ALLOCATE(Mesh%LogFr(Mesh%NFacFr))
       ALLOCATE(Mesh%NsFacFr(2, Mesh%NFacFr))
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
          CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
       END IF
    END IF
    ifr  = 0
    DO ifac = 1, NsegFr
       IF ( LogFac(ifac) /= 0 ) THEN
          ifr = ifr + 1
          Mesh%NsFacFr(1: 2, ifr) = NsFacFr(1: 2, ifac)
          Mesh%LogFr(ifr)    = LogFac(ifac)
       END IF
    END DO
    DEALLOCATE(NsFacFr, LogFac)
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "nsfacfr")
       CALL alloc_deallocate(mod_name, "logfac")
    END IF
    PRINT *, mod_name, " :: FIN"
  END SUBROUTINE ReadMeshEMC2



  SUBROUTINE ReadMeshP2P2(Mesh, FileName, Nordre)
    CHARACTER(LEN=*), PARAMETER :: mod_name = "ReadMeshP2P2"
    ! R. Abgrall 20/10/08
    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef), INTENT(INOUT)     :: Mesh
    CHARACTER(LEN=*), INTENT(IN)   :: FileName
    INTEGER         , INTENT(IN)   :: Nordre
    !variables locales
    INTEGER     :: MyUnit, is, jt, ifac, k, ifr
    INTEGER     :: Nt, Nq, NsegFr, NewNsegFr

    CHARACTER(LEN=30)                        :: MotCle
    INTEGER, DIMENSION(:,:), POINTER     ::  Nut, Nuq
    INTEGER, DIMENSION(:,:), ALLOCATABLE ::  NsFacFr
    INTEGER, DIMENSION(:)  , ALLOCATABLE     ::  LogFac,Logt,Logq
    INTEGER :: DegreMax, NSommetMax, NsommetTemp
    INTEGER :: ios, mordre
    !***************************
    !  lecture de la geometrie
    !***************************
    MyUnit = 11
    DegreMax = 0
    NSommetMax = 0
    SELECT CASE(Nordre)
    CASE(3)
       NsommetTemp=6
    CASE(4)
       NsommetTemp=10
    CASE default
       PRINT*, mod_name, "Erreur de Nsommets, L 2074"
       STOP
    END SELECT


    NULLIFY(Nut)
    NULLIFY(Nuq)

    !ouverture du fichier
    Nt=0 ; Nq=0
    OPEN(UNIT=MyUnit, FILE=FileName, IOSTAT=ios)
    IF (ios /= 0) THEN
       PRINT *, mod_name, " impossible d'ouvrir le fichier ", TRIM(FileName), " iostat=", ios
       CALL delegate_stop()
    END IF

    NewNsegFr = -1 !-- CCE 2007/11/20

    READ(MyUnit,*,IOSTAT=ios) mordre
    IF (ios /= 0) THEN
       PRINT *, mod_name, " impossible de lire la ligne 1 de ", TRIM(FileName), " iostat=", ios
       CALL delegate_stop()
    END IF
    IF (.not.(mordre == Nordre)) THEN
       PRINT*, mod_name, "L'ordre prevu par le fichier n'est pas l'ordre"
       PRINT*, "donne dans NumMeth*.data"
       PRINT*, "prevu maillage=", mordre
       PRINT*, "prevu donnees=", Nordre
       CALL delegate_stop()
    ENDIF
    READ(MyUnit,*,IOSTAT=ios)
    IF (ios /= 0) THEN
       PRINT *, mod_name, " impossible de lire la ligne 2 de ", TRIM(FileName), " iostat=", ios
       CALL delegate_stop()
    END IF
    READ(MyUnit,*,IOSTAT=ios) MotCle
    IF (ios /= 0) THEN
       PRINT *, mod_name, " impossible de lire la ligne 2 de ", TRIM(FileName), " iostat=", ios
       CALL delegate_stop()
    END IF

    DO 
       PRINT*, mod_name, " mot cle : ", MotCle
       SELECT CASE(TRIM(MotCle))
       CASE("Dimension")
          READ(MyUnit,*) Mesh%Ndim
          PRINT*, mod_name, " ndim==", Mesh%Ndim
          READ(MyUnit,*) MotCle

       CASE("Vertices")

          READ(MyUnit,*)  Mesh%Ncells, Mesh%Npoint
          ! Mesh%Ncells : les points Pk. Msh%Npoints : les points P1
          PRINT*, mod_name , " npoint==",  Mesh%Ncells, Mesh%Npoint
          ALLOCATE(  Mesh%coor(1:Mesh%Ndim,Mesh%Ncells) )

          !lecture des coord des sommets
          DO is=1,Mesh%Ncells
             READ(MyUnit,*,IOSTAT=ios) Mesh%coor(1:Mesh%Ndim,is)
             IF (ios /=0) THEN
                PRINT*, mod_name, "erreur lecture coordonnees", is
             ENDIF
          END DO
          READ(MyUnit,*) MotCle

       CASE("Edges")

          READ(MyUnit,*) NsegFr
          PRINT*, mod_name, " nsegfr==", NsegFr
          ALLOCATE(NsFacFr(nordre,NsegFr),  LogFac(NsegFr))
          DO ifac = 1, NsegFr
             READ(MyUnit,*) (NsFacFr(k,ifac), k=1,Nordre),LogFac(ifac)
          END DO
          READ(MyUnit,*) MotCle
       CASE("Triangles")
          READ(MyUnit,*) Nt
          PRINT*, mod_name, " nt==", Nt
          ALLOCATE( Nut(NsommetTemp,Nt) ,Logt(Nt))!ordre 3,4


          DO jt=1, Nt
             DO k=1,NsommetTemp
                READ(MyUnit,*,IOSTAT=ios) Nut(k,jt)
             ENDDO
             READ(MyUnit,*) logt(jt)
             ! READ(MyUnit,10,IOSTAT=ios) Nut(1,jt), Nut(2,jt), Nut(3,jt), Nut(4,jt), Nut(5,jt), Nut(6,jt),Logt(jt)
             !10            FORMAT(6(1x,i8),1x,i2)
             IF (ios /=0) THEN
                PRINT*, mod_name, "erreur lecture triangles", jt,k
             ENDIF
          END DO
          READ(MyUnit,*) MotCle

       CASE("End")
          EXIT
       CASE DEFAULT
          PRINT*, mod_name, " ERREUR : Mot cle", TRIM(MotCle), " non prevu "
          CALL delegate_stop()
       END SELECT
    ENDDO
    CLOSE(myunit)
    Mesh%Nelemt= Nt
    !allocation memoire
    ALLOCATE(  Mesh%Nu(NsommetTemp,Mesh%Nelemt)   )
    ALLOCATE(  Mesh%Ndegre(Mesh%Nelemt) , Mesh%NSommets(Mesh%Nelemt))
    ALLOCATE(  Mesh%LogElt(1:Mesh%Nelemt),Mesh%LogPt(1:Mesh%Ncells ) )
    Mesh%LogPt=0

    DO jt=1,Nt
       SELECT CASE(Nordre)
       CASE(2)
          Mesh%Ndegre(jt) = 3
       CASE(3)
          Mesh%Ndegre(jt) = 6
       CASE(4)
          Mesh%Ndegre(jt) = 10
       CASE DEFAULT
          PRINT *, mod_name, " ERREUR, ReadMesh.f90 L795 ..."
          CALL delegate_stop()
       END SELECT
       Mesh%NSommets(jt) = 3
       Mesh%Nu(1:NsommetTemp,jt) = Nut(1:NsommetTemp,jt)
       Mesh%LogElt(jt) = Logt(jt)
       Mesh%LogPt(Mesh%Nu(1:NsommetTemp,jt)) = MAX(Mesh%LogPt(Mesh%Nu(1:NsommetTemp,jt)),Mesh%LogElt(jt))
    END DO


    IF (ASSOCIATED(Nut)) DEALLOCATE(Nut,Logt)
    IF (ASSOCIATED(Nuq)) DEALLOCATE(Nuq,Logq)





    Mesh%NFacFr = NsegFr
    IF( Mesh%NFacFr /= 0 ) THEN
       ALLOCATE(Mesh%LogFr(Mesh%NFacFr))
       ALLOCATE(Mesh%NsFacFr(Nordre,Mesh%NFacFr))
    END IF


    ifr  = 0

    DO ifac = 1, NsegFr
       IF( LogFac(ifac) /= 0 )THEN
          ifr = ifr +1
          Mesh%NsFacFr(1:Nordre,ifr) = NsFacFr(1:Nordre,ifac)

          Mesh%LogFr(ifr)    = LogFac(ifac)
       END IF
    END DO


    DEALLOCATE(NsFacFr,  LogFac)

#ifdef INTERF
    Mesh%Ndegre = 3
#endif


  END SUBROUTINE ReadMeshP2P2




  !!@author Version de M. Papin (Fluids release 28)
  !! Lit le format amdba et permet, si le mailllage a �t� construit avec
  !! des quadrangles, de les reformer (ils sont stock�s triangul�s)

  !!-- Initialisation de la structure mesh du maillage, par lecture du fichier de maillage <racine>.<type_de_maillage>

  !!-- Appelle SegmentsSinusFr2D pour construire diverses infos, les recopie

  !!-- la structure du fichier donne le rectangle en 2 triangles, A1, B1, C1 puis A2, B2, C2 -> A2, C1, B2, C2,
  !!-- avec deux num�ros, que l'on divise par 2 (jt/2) pour n'avoir qu'un �l�ment.

  !!-- Nu a ici ci un range de 1:4, pour contenir 4 sommets
  SUBROUTINE ReadAMDBA2D_quad(Mesh, FileName)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ReadAMDBA2D_quad"
    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    !
    TYPE(MeshDef),    INTENT(INOUT)   :: Mesh
    CHARACTER(LEN = *), INTENT(IN)    :: FileName
    !variables locales
    INTEGER                          :: MyUnit, j, is, jt, ljt, ifac
    INTEGER                          :: NsegmtFr
    !variables locales
    INTEGER, DIMENSION(: ),   POINTER :: LogFr, LogFac
    INTEGER, DIMENSION(:, : ), POINTER :: NsFacFr
    INTEGER::a1, b1, c1, a2, b2, c2
    LOGICAL :: existe
    INTEGER :: statut
    !***************************
    !  lecture de la geometrie
    !***************************

    MyUnit = 11

    INQUIRE(FILE = FileName, EXIST = existe)
    IF (.NOT. existe) THEN
       CALL info(mod_name, FileName, "amdba")
       PRINT *, mod_name, " : ERREUR : le fichier ", TRIM(FileName), " n'existe pas"
       CALL delegate_stop()
    END IF

    !ouverture du fichier
    OPEN(UNIT = MyUnit, FILE = FileName, IOSTAT = statut)
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour ouvrir ", TRIM(FileName)
       CALL delegate_stop()
    END IF
    READ(MyUnit, *, IOSTAT = statut) Mesh%Npoint, Mesh%Nelemt
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour la 1ere ligne de ", TRIM(FileName)
       CALL delegate_stop()
    END IF
    Mesh%Nelemt = Mesh%Nelemt / 2


    !allocation memoire
    Mesh%Ndim = 2
    ALLOCATE(  Mesh%coor(1: 2, 1: Mesh%Npoint) )
    ALLOCATE(  Mesh%Nu(1: 4, 1: Mesh%Nelemt)   )
    ALLOCATE(  Mesh%Ndegre(1: Mesh%Nelemt) )
    ALLOCATE(  Mesh%LogElt(1: Mesh%Nelemt), Mesh%LogPt(1: Mesh%Npoint ) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%coor", SIZE(Mesh%coor))
       CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
       CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
       CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
       CALL alloc_allocate(mod_name, "mesh%logpt", SIZE(Mesh%LogPt))
    END IF

    Mesh%Ndegre(1: Mesh%Nelemt) = 4
    Mesh%LogPt = 0
    !allocation memoire
    ALLOCATE(  LogFr(1: Mesh%Npoint) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "logfr", SIZE(LogFr))
    END IF

    !lecture des coord des sommets
    DO j = 1, Mesh%Npoint
       READ(MyUnit, *) is, Mesh%coor(1, is), Mesh%coor(2, is), LogFr(is)
    END DO
    !lecture des numeros des sommets
    DO j = 1, Mesh%Nelemt
       READ(MyUnit, *) jt, a1, b1, c1, ljt
       READ(MyUnit, *) jt, a2, b2, c2, ljt
       Mesh%LogElt(jt / 2) = ljt
       Mesh%Nu(1, jt / 2) = a2
       Mesh%Nu(2, jt / 2) = c1
       Mesh%Nu(3, jt / 2) = b2
       Mesh%Nu(4, jt / 2) = c2
       Mesh%LogPt(Mesh%Nu(1: 4, jt / 2)) = MAX(Mesh%LogPt(Mesh%Nu(1: 4, jt / 2)), ljt)
    END DO
    CLOSE(MyUnit)


    CALL SegmentsSinusFr2d(Mesh%Npoint, Mesh%Nelemt, Mesh%Ndegre, LogFr, Mesh%Nu, &
         &             NsegmtFr, LogFac, NsFacFr)

    Mesh%NFacFr = NsegmtFr
    IF ( Mesh%NFacFr /= 0 ) THEN
       ALLOCATE(Mesh%LogFr(1: Mesh%NFacFr))
       ALLOCATE(Mesh%NsFacFr(1: 2, 1: Mesh%NFacFr))
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
          CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
       END IF
    END IF

    DO ifac = 1, Mesh%NFacFr
       Mesh%LogFr(ifac)    = LogFac(ifac)
       Mesh%NsFacFr(1: 2, ifac) = NsFacFr(1: 2, ifac)
    END DO

    DEALLOCATE( LogFac, NsFacFr )
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "nsfacfr")
       CALL alloc_deallocate(mod_name, "logfac")
    END IF
  END SUBROUTINE ReadAMDBA2D_quad


  !!@author Version de M. Papin (Fluids release 28)
  !! Lit le format amdba (triangles)

  !!-- Initialisation de la structure mesh du maillage, par lecture du fichier de maillage <racine>.<type_de_maillage>

  !!-- Appelle SegmentsSinusFr2D pour construire diverses infos, les recopie
  SUBROUTINE ReadAMDBA2D_tri(Mesh, FileName)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ReadAMDBA2D_tri"
    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef),    INTENT(INOUT)   :: Mesh
    CHARACTER(LEN = *), INTENT(IN)    :: FileName
    !variables locales
    INTEGER :: Stat
    INTEGER                          :: MyUnit, j, is, jt, ifac
    INTEGER                          :: NsegmtFr

    !variables locales
    INTEGER, DIMENSION(: ),   POINTER :: LogFr, LogFac
    INTEGER, DIMENSION(:, : ), POINTER ::  NsFacFr
    LOGICAL :: existe
    INTEGER :: maxv, ii

    !***************************
    !  lecture de la geometrie
    !***************************

    PRINT *, mod_name, " DEBUT"

    MyUnit = 11

    !ouverture du fichier
    INQUIRE(FILE = FileName, EXIST = existe)
    IF (.NOT. existe) THEN
       CALL info(mod_name, FileName, "amdba")
       PRINT *, mod_name, " :: ERREUR : pas de fichier ", TRIM(FileName)
       CALL delegate_stop()
    END IF
    OPEN(UNIT = MyUnit, FILE = FileName, IOSTAT = Stat)
    IF (Stat /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : IOSTAT = ", Stat,  " a l'ouverture de ", TRIM(FileName)
       CALL delegate_stop()
    END IF

    READ(MyUnit, *) Mesh%Npoint, Mesh%Nelemt

    !allocation memoire
    Mesh%Ndim = 2
    ALLOCATE(  Mesh%coor(1: 2, 1: Mesh%Npoint) )
    ALLOCATE(  Mesh%Nu(1: 3, 1: Mesh%Nelemt)   )
    ALLOCATE(  Mesh%Ndegre(1: Mesh%Nelemt) )
    ALLOCATE(  Mesh%LogElt(1: Mesh%Nelemt), Mesh%LogPt(1: Mesh%Npoint ) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%coor", SIZE(Mesh%coor))
       CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
       CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
       CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
       CALL alloc_allocate(mod_name, "mesh%logpt", SIZE(Mesh%LogPt))
    END IF

    Mesh%LogPt = 0
    !allocation memoire
    ALLOCATE(  LogFr(1: Mesh%Npoint) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "logfr", SIZE(LogFr))
    END IF

    !lecture des coord des sommets
    DO j = 1, Mesh%Npoint
       READ(MyUnit, *) is, Mesh%coor(1, is), Mesh%coor(2, is), LogFr(is)
    END DO

    !lecture des numeros des sommets
    DO j = 1, Mesh%Nelemt
       READ(MyUnit, *) jt, Mesh%Nu(1, jt), Mesh%Nu(2, jt), Mesh%Nu(3, jt), Mesh%LogElt(jt)
       Mesh%LogPt(Mesh%Nu(1: 3, jt)) = MAX(Mesh%LogPt(Mesh%Nu(1: 3, jt)), Mesh%LogElt(jt))
    END DO

    !OFF     PRINT *, mod_name, " Mesh%Ncells==", Mesh%Ncells, Maxval(Mesh%nu)
    !OFF     DO j = 1, Maxval(Mesh%Nu)
    !OFF        PRINT *, "<>", mod_name, j, Mesh%LogPt(j)
    !OFF     END DO

    CLOSE(MyUnit)

    Mesh%Ndegre(1: Mesh%Nelemt) = 3

    CALL SegmentsSinusFr2d(Mesh%Npoint, Mesh%Nelemt, Mesh%Ndegre, LogFr, Mesh%Nu, &
         &             NsegmtFr, LogFac, NsFacFr)

    Mesh%NFacFr = NsegmtFr
    IF ( Mesh%NFacFr /= 0 ) THEN
       ALLOCATE(Mesh%LogFr(1: Mesh%NFacFr))
       ALLOCATE(Mesh%NsFacFr(1: 2, 1: Mesh%NFacFr))
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
          CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
       END IF
    END IF

    DO ifac = 1, Mesh%NFacFr
       Mesh%LogFr(ifac)    = LogFac(ifac)
       Mesh%NsFacFr(1: 2, ifac) = NsFacFr(1: 2, ifac)
    END DO

    DEALLOCATE( LogFac, NsFacFr ) !-- allocations faites dans SegmentsSinusFr2d
    DEALLOCATE( LogFr)
    IF (see_alloc) THEN
       CALL alloc_deallocate(".dummy.", "logfac")
       CALL alloc_deallocate(".dummy.", "nsfacfr")
       CALL alloc_deallocate(".dummy.", "logfr")
    END IF

    maxv = MAXVAL(Mesh%LogFr)
    IF (maxv == 0) THEN
       DO ii = 1, 5
          WRITE(message_log, *) mod_name, " ATTENTION : maillage sans point frontiere"
          CALL log_record(message_log)
       END DO
       Mesh%LogFr = pas_de_frontiere
       Mesh%NFacFr = 0
    END IF

  END SUBROUTINE ReadAMDBA2D_tri



  !!@author  B. Nkonga (Fluids release 28)

  !!-- Cette m�thode travaille explicitement en 2D pour des �l�ments triangulaires ou quadrangulaires
  !!-- Appel� par : ReadAMDBA2D_quad, ReadAMDBA2D_tri

  !!-- hashtable contient des listes tri�es dans l'ordre de num�ro de sommet, partant d'un point donn�

  !!-- nubo contient cette hashtable de fa�on condens�e Nubo(k, js) :: numero du Keme point du segment js, dans l'ordre croissant (recopie de la hashtable)
  !!-- Remarque : en dimension 2, un segment a 2 points, il ne semble pas que, du moins dans GEOM/, il y ait une autre valeur que 2
  !!-- En fait nubo contient tous les segments, ordonn�s par le num�ro du 1er point

  !!-- Ensuite on construire nutv : Nutv(k, js) :: numero du Keme element contenant le segment js
  !!-- Contient 1 ou 2 �lements qui sont adjacents au segment; 1 �l�ment <=> segment fronti�re

  !!-- Ensuite on construit les contraintes pour les segments fronti�res, comme le max des contraintes aux sommets du segment
  SUBROUTINE SegmentsSinusFr2d(Npoint, Nelemt, NSommets, LogFr, Nu, &
       &                              NsegmtFr, LogFac, NsFacFr)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "SegmentsSinusFr2d"
    !==============================================================
    ! Nsommets(jt) ::  type de l'element
    ! Nu(k, jt)   ::  numero du Keme point de l'element jt
    ! Nubo(k, js) ::  numero du Keme point du segment js
    ! NsFacFr(k, is)::  numero du Keme point du segment frontiere is
    ! Nuseg(k, jt)::  numero du Keme segment de l'element jt
    ! Nusv(k, js) ::  numero du Keme sommet tel qu'avec le
    !                segment js, ils forment un element
    ! Nutv(k, js) ::  numero du Keme element contenant le segment js
    ! NbVois(k)  ::  nombre de voisins du sommet k
    ! Logfac(k)  ::  contrainte du segment k
    !==============================================================
    ! ****************
    !   declarations
    ! ***************
    ! variables d'appel
    !INTEGER                         , INTENT(IN) :: Npoint, Nelemt

    !INTEGER, DIMENSION(1:Nelemt)    , INTENT(IN) :: Nsommets
    !INTEGER, DIMENSION(1:Npoint)    , INTENT(IN) :: Logfr
    !INTEGER, DIMENSION(1:4, 1:Nelemt), INTENT(IN) :: Nu
    INTEGER,                          INTENT(IN) :: Npoint, Nelemt

    INTEGER, DIMENSION(: ),   POINTER   :: NSommets
    INTEGER, DIMENSION(: ),   POINTER   :: LogFr
    INTEGER, DIMENSION(:, : ),   POINTER   :: Nu

    INTEGER, INTENT(OUT)             :: NsegmtFr
    INTEGER, DIMENSION(: ),   POINTER ::  LogFac
    INTEGER, DIMENSION(:, : ), POINTER ::  NsFacFr

    ! variables locales
    INTEGER                   :: Nsegmt
    INTEGER                   :: is, iv, jt, k, is1, is2
    INTEGER                   :: is_min, is_max, jseg, iseg, isegfr
    INTEGER                   :: next, noth, ntyp
    TYPE(cellule), POINTER    :: NewCell, PtCell, PtCellPred
    TYPE(cellule), DIMENSION (: ),  POINTER :: hashtable
    INTEGER,       DIMENSION(: ),   ALLOCATABLE :: NbVois
    INTEGER, DIMENSION(:, : ), ALLOCATABLE :: Nutv
    INTEGER, DIMENSION(:, : ), POINTER ::  Nubo

    ALLOCATE( hashtable(Npoint) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "hashtable", SIZE(hashtable))
    END IF

    ! ***************************
    !   initialisations
    ! ***************************
    DO is = 1, Npoint
       hashtable(is)%val = is
       NULLIFY(hashtable(is)%suiv)
    END DO
    ALLOCATE(NbVois(Npoint))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nbvois", SIZE(NbVois))
    END IF
    NbVois(1: Npoint) = 0
    Nsegmt = 0

    ! *******************************************
    !   construction de hashtable, nbvois, Nsegmt
    ! *******************************************
    ! construction de la table des segments
    DO jt = 1, Nelemt
       ntyp = NSommets(jt)
       DO k = 1, ntyp
          next = MOD(k, ntyp) + 1
          is1 = Nu(   k, jt)
          is2 = Nu(next, jt)
          is_min = MIN(is1, is2)
          is_max = MAX(is1, is2)

          ! recherche de la position d'insertion de is_max dans
          ! le tableau hashtable(is_min). on utilise une
          ! recherche sequentielle simple.
          ! initialisation des pointeurs
          PtCell     => hashtable(is_min)%suiv
          PtCellPred => hashtable(is_min)

          ! on recherche la place du nouveau voisin
          DO
             IF (.NOT. ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             IF (PtCell%val > is_max) THEN
                EXIT
             END IF
             ! on a trouve la place, on arrete

             PtCellPred => PtCell
             PtCell     => PtCell%suiv
          END DO

          ! on teste si le sommet n'est pas deja la
          IF (PtCellPred%val < is_max) THEN

             ! creation de la nouvelle cellule
             ALLOCATE(NewCell)
             IF (see_alloc) THEN
                CALL alloc_allocate(mod_name, "newcell.X", 1)
             END IF
             NewCell%val  = is_max

             ! insertion de la nouvelle cellule
             NewCell%suiv    => PtCell
             PtCellPred%suiv => NewCell

             ! on a rajoute un voisin, donc on incremente
             ! NbVois(is_min) et Nsegmt
             NbVois(is_min) = NbVois(is_min) + 1
             Nsegmt = Nsegmt + 1

          END IF
       END DO

    END DO

    ! le tableau temporaire hashtable est constitue,
    ! on le recopie dans le tableau Nubo

    ! ****************************
    !   creation du tableau Nubo
    ! ****************************
    ! allocation memoire
    ALLOCATE(Nubo(1: 2, 1: Nsegmt))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "nubo", SIZE(Nubo))
    END IF
    ! compteur pour les segments frontieres
    next = 0
    k    = 0
    DO is = 1, Npoint

       ! si le sommet is a un(des) voisin(s)
       IF (NbVois(is) /= 0) THEN
          PtCell => hashtable(is)%suiv

          ! on prend tous les voisins du sommet is
          DO iv = 1, NbVois(is)
             k = k + 1
             Nubo(1, k) = is
             Nubo(2, k) = PtCell%val

             ! on pointe sur le voisin suivant
             PtCell => PtCell%suiv
          END DO

       END IF

    END DO

    ! ***************************************
    !   construction de Nuseg, Nusv et Nutv
    ! ***************************************

    ! initialisation des tableaux Nuseg, Nusv et Nutv

    ALLOCATE(Nutv(1: 2, 1: Nsegmt))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "Nutv", SIZE(Nutv))
    END IF
    Nutv(1: 2, 1: Nsegmt) = 0

    DO jt = 1, Nelemt

       DO k = 1, NSommets(jt)!3

          ntyp = NSommets(jt)!3

          next = MOD(   k, ntyp) + 1
          noth = MOD(next, ntyp) + 1
          is1  = Nu(next, jt)
          is2  = Nu(noth, jt)
          noth = k
          iseg = WhichSeg(1, Nsegmt, Nubo, is1, is2)
          IF (Nutv(1, iseg) == 0) THEN
             Nutv(1, iseg) = jt
          ELSE
             IF (Nutv(2, iseg) == 0) THEN
                Nutv(2, iseg) = jt
             ELSE
                WRITE(6, *) mod_name, " ERREUR : Probleme dans les elements voisins"
                CALL delegate_stop()
             END IF
          END IF

       END DO
    END DO

    NsegmtFr = 0
    DO iseg = 1, Nsegmt
       IF (Nutv(2, iseg) == 0 ) THEN
          NsegmtFr = NsegmtFr + 1
       END IF
    END DO

    ALLOCATE(LogFac(1: NsegmtFr))
    ALLOCATE(NsFacFr(1: 2, 1: NsegmtFr))
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "logfac", SIZE(LogFac))
       CALL alloc_allocate(mod_name, "nsfacfr", SIZE(NsFacFr))
    END IF
    isegfr = 0
    DO iseg = 1, Nsegmt
       IF ( Nutv(2, iseg) == 0 ) THEN
          isegfr = isegfr + 1
          is1    = Nubo(1, iseg)
          is2    = Nubo(2, iseg)
          NsFacFr(1, isegfr) = is1
          NsFacFr(2, isegfr) = is2
          LogFac(isegfr)  = MAX( LogFr(is1), LogFr(is2) )
       END IF
    END DO
    ! Nettoyage memoire !!

    DO is = 1, Npoint
       IF (NbVois(is) /= 0) THEN
          PtCellPred => hashtable(is)%suiv
          PtCell => PtCellPred%suiv
          DO
             DEALLOCATE(PtCellPred)
             IF (see_alloc) THEN
                CALL alloc_deallocate(".dummy.", "ptcellpred.X")
             END IF
             IF (.NOT.ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             PtCellPred => PtCell
             PtCell => PtCell%suiv
          END DO
       END IF
    END DO

    DEALLOCATE(NbVois, Nutv, Nubo)
    DEALLOCATE( hashtable )
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "nbvois")
       CALL alloc_deallocate(mod_name, "nutv")
       CALL alloc_deallocate(mod_name, "nubo")
       CALL alloc_deallocate(mod_name, "hashtable")
    END IF

  CONTAINS

    !!-- Recherche dichotomique d'un segment entre 2 sommets, la recherche ayant lieu dans le tableau nubo
    !!-- Ensuite recherche non dichotomique croissante ou d�croissante du 2�me sommet (on parcourt les segments qui bordent l'�l�ment)
    FUNCTION WhichSeg(idebut, ifin, Nubo, is1, is2) RESULT(i_seg)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "WhichSeg"

      ! variables d'appel
      INTEGER, INTENT(IN)              :: is1, is2, idebut, ifin
      INTEGER, DIMENSION(:, : ), POINTER :: Nubo
      INTEGER :: i_seg

      ! variables locales
      INTEGER              :: jseg2, debut, Fin, incr, is_min, is_max

      ! initialisation des variables
      is_min = MIN(is1, is2)
      is_max = MAX(is1, is2)
      debut = idebut
      Fin = ifin
      jseg = 0

      ! recherche du premier point is1 dans Nubo
      DO
         jseg2 = jseg
         jseg = (debut + Fin) / 2

         ! fin de la recherche, on sort
         IF ((Nubo(1, jseg) == is_min) .OR. (jseg == jseg2)) THEN
            EXIT
         END IF

         IF (Nubo(1, jseg) < is_min) THEN
            debut = jseg + 1
         ELSE
            Fin = jseg
         END IF

      END DO

      ! si is_min est dans Nubo, on cherche is_max
      IF (Nubo(1, jseg) == is_min) THEN

         ! ruse pour forcer jseg a diminuer ou augmenter
         IF (Nubo(2, jseg) > is_max) THEN
            incr = - 1
         ELSE
            incr = 1
         END IF

         ! recherche de is_max
         DO
            IF ( Nubo(2, jseg) == is_max .OR. Nubo(1, jseg) /= is_min) THEN
               EXIT
            END IF
            jseg = jseg + incr
         END DO

      END IF

      IF ( Nubo(1, jseg) /= is_min .OR. Nubo(2, jseg) /= is_max ) THEN
         jseg = 0
      END IF

      i_seg = jseg !-- %%%%%%%%%% variable externe � la m�thode courante

    END FUNCTION WhichSeg

  END SUBROUTINE SegmentsSinusFr2d

  !!-- Lecture d'un maillage 2D, g�n�r� par GMSH
  !!-- C'est un format qui contient des mots cl�s
  !!-- Les segments et les triangles sont m�lang�s.
  !!-- element(1) semble contenir la contrainte
  !!-- element(2) semble contenir le type :
  !!--   2 -> ar�te
  !!--   3 -> triangle

  SUBROUTINE ReadMeshGMSh(Mesh, FileName)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ReadMeshGMSh"
    !---------------
    ! Lecture des maillages de GMSH
    ! cedric

    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef), INTENT(INOUT)               :: Mesh
    CHARACTER(LEN = *), INTENT(IN)             :: FileName
    !variables locales
    INTEGER                                  :: MyUnit, k
    INTEGER                                  :: Nt, NsegFr
    INTEGER                                  :: DegreMax, i, nel, dummy, nseg
    CHARACTER(LEN = 30)                        :: MotCle
    !allouables
    INTEGER, DIMENSION(:, : ), ALLOCATABLE     ::  element

    REAL                                     :: xg1, yG1, xg2, yG2
    INTEGER                                  :: i1, i2, i3, statut
    LOGICAL :: existe
    ! ----------------- Ouverture du fichier -------------
    !
    !
    MyUnit = 11
    DegreMax = 0

    INQUIRE(FILE = FileName, EXIST = existe)
    IF (.NOT. existe) THEN
       CALL info(mod_name, FileName, "msh")
       PRINT *, mod_name, " : ERREUR : le fichier ", TRIM(FileName), " n'existe pas"
       CALL delegate_stop()
    END IF

    OPEN( UNIT = MyUnit, FILE = FileName, IOSTAT = statut )

    IF (statut /= 0) THEN
       CALL info(mod_name, FileName, "msh")
       PRINT *, mod_name, " : ERREUR : impossible d'ouvrir le fichier ", TRIM(FileName), statut
       CALL delegate_stop()
    END IF

    READ(MyUnit, *, IOSTAT = statut) MotCle
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour la 1ere ligne de ", TRIM(FileName)
       CALL delegate_stop()
    END IF
    IF ( MotCle /= "$NOD" ) THEN
       PRINT *, mod_name, " :: ERREUR : La lecture du maillage a echouee $NOD ", TRIM(MotCle)
       CALL delegate_stop()
    END IF

    ! ----------------- Lecture des noeuds -------------
    ! facile
    !

    READ(MyUnit, *) Mesh%Npoint
    Mesh%Ndim = 2
    ALLOCATE(  Mesh%LogPt(1: Mesh%Npoint ) )
    ALLOCATE(  Mesh%coor(1: Mesh%Ndim, Mesh%Npoint) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%coor", SIZE(Mesh%coor))
       CALL alloc_allocate(mod_name, "mesh%logpt", SIZE(Mesh%LogPt))
    END IF

    DO i = 1, Mesh%Npoint
       Mesh%LogPt(i) = 1
       READ(MyUnit, *) dummy, Mesh%coor(1, i), Mesh%coor(2, i)
    END DO

    READ(MyUnit, *) MotCle ! mot clef de fin $NOD
    IF ( MotCle /= "$ENDNOD" ) THEN
       PRINT *, mod_name, " :: ERREUR : La lecture du maillage a echouee $ENDNOD ", TRIM(MotCle)
       CALL delegate_stop()
    END IF
    READ(MyUnit, *) MotCle ! mot clef suivant
    IF ( MotCle /= "$ELM" ) THEN
       PRINT *, mod_name, " :: ERREUR : La lecture du maillage a echouee $ELM ", TRIM(MotCle)
       CALL delegate_stop()
    END IF

    ! --------------- Lecture des elements ----------------
    ! segments et triangles melanges... quelle idee
    ! on utilise un tableau temporaire

    READ(MyUnit, *) nel
    ALLOCATE( element(1: 10, 1: nel) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "element", SIZE(element))
    END IF
    !premiere composante: 1 -> logique, 2-> nnodes, 3 � 10-> les noeuds
    DO i = 1, nel
       READ(MyUnit, *) dummy, dummy, element(1, i), dummy, element(2, i), element(3: 2 + element(2, i), i)
    END DO
    READ(MyUnit, *) MotCle ! mot clef de fin $ENDELM
    IF ( MotCle /= "$ENDELM" ) THEN
       PRINT *, mod_name, " :: ERREUR : La lecture du maillage a echouee $ENDELM ", TRIM(MotCle)
       CALL delegate_stop()
    END IF
    ! ----------------- fin de lecture ----------------------
    CLOSE(MyUnit)

    ! on compte de nombre de segments dans elements
    NsegFr = 0
    DO i = 1, nel
       IF ( element(2, i) == 2 ) THEN
          NsegFr = NsegFr + 1
       END IF
    END DO

    ! on en deduit le nombre d element autre que segment (des triangles en fait)
    Nt = nel - NsegFr

    !c'est parti ------------- on rempli les tableaux de connectivite
    !
    !allocation
    Mesh%Nelemt = Nt
    Mesh%NFacFr = NsegFr
    ALLOCATE(Mesh%NsFacFr(2, NsegFr), Mesh%LogFr(NsegFr))
    ALLOCATE(Mesh%LogElt(1: Mesh%Nelemt), Mesh%Ndegre(1: Mesh%Nelemt) )
    ALLOCATE(Mesh%Nu(3, Nt) )
    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "mesh%logfr", SIZE(Mesh%LogFr))
       CALL alloc_allocate(mod_name, "mesh%nu", SIZE(Mesh%Nu))
       CALL alloc_allocate(mod_name, "mesh%ndegre", SIZE(Mesh%Ndegre))
       CALL alloc_allocate(mod_name, "mesh%logelt", SIZE(Mesh%LogElt))
       CALL alloc_allocate(mod_name, "mesh%nsfacfr", SIZE(Mesh%NsFacFr))
    END IF

    nseg = 0
    k = 0

    DO i = 1, nel

       IF ( element(2, i) == 2 ) THEN
          ! les aretes
          nseg = nseg + 1
          Mesh%NsFacFr(1, nseg) = element(3, i)
          Mesh%NsFacFr(2, nseg) = element(4, i)
          Mesh%LogFr(nseg)     = element(1, i)
       ELSE IF (element(2, i) == 3 ) THEN
          ! les triangles
          k = k + 1
          Mesh%Ndegre(k) = 3
          Mesh%Nu(1, k)   = element(3, i)
          Mesh%Nu(2, k)   = element(4, i)
          Mesh%Nu(3, k)   = element(5, i)
          Mesh%LogElt(k) = element(1, i)
          ! test l'orientation
          i1 = Mesh%Nu(1, k)
          i2 = Mesh%Nu(2, k)
          i3 = Mesh%Nu(3, k)

          xg1 = Mesh%coor(1, i1) - Mesh%coor(1, i2)
          yG1 = Mesh%coor(2, i1) - Mesh%coor(2, i2)
          xg2 = Mesh%coor(1, i1) - Mesh%coor(1, i3)
          yG2 = Mesh%coor(2, i1) - Mesh%coor(2, i3)
          IF (xg1 * yG2 - xg2 * yG1 < 0.0 ) THEN
             PRINT *, mod_name, " ATTENTION : WARNING: orientation triangle corrigee (", k, ")"
             Mesh%Nu(1, k) = i2
             Mesh%Nu(2, k) = i1
          END IF
       END IF
    END DO

    ! si par hasard les compteurs ne sont pas bons
    IF ( nseg /= NsegFr) THEN
       PRINT *, mod_name, " :: ERREUR/1 : erreur lecture"
       CALL delegate_stop()
    END IF
    IF ( k /= Nt) THEN
       PRINT *, mod_name, " :: ERREUR/2 : erreur lecture"
       CALL delegate_stop()
    END IF


    ! on libere la memoire
    DEALLOCATE( element )
    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "element")
    END IF

  END SUBROUTINE ReadMeshGMSh

  !=======================================
  SUBROUTINE ReadMeshGMSH2(Mesh, FileName)
  !=======================================

    IMPLICIT NONE

    TYPE(MeshDef),      INTENT(INOUT) :: Mesh    
    CHARACTER(LEN = *), INTENT(IN)    :: FileName
    !---------------------------------------------
    REAL, DIMENSION(:,:), ALLOCATABLE :: Coords

    TYPE :: ele_str
       INTEGER, DIMENSION(:), ALLOCATABLE :: NU
       INTEGER, DIMENSION(:), ALLOCATABLE :: VV
       INTEGER :: log_type
    END type ele_str
    TYPE(ele_str), DIMENSION(:), ALLOCATABLE :: ele

    INTEGER :: N_nodes, N_ele, N_b, i, j, &
               N_E_max, N_B_max, idummy, &
               ele_type, n_tag

    INTEGER, DIMENSION(:), ALLOCATABLE :: v_dummy
    
    REAL :: ver, dummy

    INTEGER :: UNIT, status, Form, ds

    LOGICAL  :: exist
    !---------------------------------------------

    INQUIRE(FILE = FileName, EXIST = exist)
    IF( .NOT. exist) THEN
       WRITE(*,*) 'ERROR: file ', trim(FileName), ' not found'
       STOP
    ENDIF

    UNIT = 15

    OPEN(UNIT, FILE = FileName, IOSTAT = status)

    READ(UNIT, *) !! $MeshFormat !!
    READ(UNIT, *) Ver, Form, ds
    IF( ver /= 2.2 ) THEN
       WRITE(*,*) 'ERROR: unknown GMSH version'
       STOP
    ENDIF    
    IF( Form /= 0 ) THEN
       WRITE(*,*) 'ERROR: no ASCII file'
       STOP
    ENDIF
    READ(UNIT, *) !!$EndMeshFormat !!
    
    READ(UNIT, *) !!$Nodes !!
    READ(UNIT, *) N_Nodes
    ALLOCATE( Coords(3, N_Nodes) )
    DO i = 1, N_nodes
       READ(UNIT, *) dummy, Coords(:, i)
    ENDDO
    READ(UNIT, *) !!$EndNodes !!

    READ(UNIT, *) !!$Elements !!
    READ(UNIT, *) N_ele
    ALLOCATE( ele(N_ele) )

    N_E_max = 0
    N_B_max = 0

    DO i = 1, N_ele

       READ(UNIT, *) idummy, ele_type, n_tag
              
       ALLOCATE( ele(i)%NU(GMSH_ELE(ele_type, 1)) )
       ALLOCATE( ele(i)%VV(GMSH_ELE(ele_type, 2)) )

       ALLOCATE( v_dummy(n_tag-1) )

       BACKSPACE(UNIT)
       
       READ(UNIT, *) idummy, idummy, idummy,  &
                     ele(i)%log_type, v_dummy, &
                     ele(i)%NU 

       ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(ele_type, 2) )

       IF( ele(i)%log_type == 0 ) THEN
          N_E_max = MAX(N_E_max, SIZE(ele(i)%NU))
       ELSE
          N_B_max = MAX(N_B_max, SIZE(ele(i)%NU))
       ENDIF
       
       DEALLOCATE(v_dummy)

    ENDDO

    READ(UNIT, *) !!$EndElements !!

    CLOSE(UNIT)

    Mesh%Npoint = N_Nodes

    ALLOCATE( Mesh%Coor(Mesh%Ndim, N_Nodes) )

    Mesh%Coor = 0.d0
    DO i = 1, Mesh%Ndim
       Mesh%Coor(i, :) = Coords(i, :)
    ENDDO
    
    N_b = COUNT(ele%log_type /= 0)

    Mesh%Nelemt = N_ele - N_b

    Mesh%NdegreMax = N_E_max

    ALLOCATE( Mesh%Nu(Mesh%NdegreMax, Mesh%Nelemt) )
    ALLOCATE( Mesh%Ndegre(Mesh%Nelemt) )
    ALLOCATE( Mesh%LogPt(Mesh%Npoint) )
    ALLOCATE( Mesh%LogElt(Mesh%Nelemt) )
   
    Mesh%Nu = 0
    Mesh%LogPt = 0
    Mesh%LogElt = 0

    DO i = 1, Mesh%Nelemt

       Mesh%Ndegre(i) = SIZE( ele(i)%NU )
       Mesh%Nu(1:SIZE(ele(i)%NU), i) = ele(i)%NU
    
    ENDDO

    Mesh%NFacFr = N_b
    
    ALLOCATE( Mesh%LogFr(Mesh%NFacFr) )
    ALLOCATE( Mesh%NsFacFr(N_B_max, Mesh%NFacFr) )

    Mesh%LogFr   = 0
    Mesh%NsFacFr = 0

    j = 0
    DO i = N_ele-N_b+1, N_ele

       j = j+1

       Mesh%NsFacFr(1:SIZE(ele(i)%NU), j) = ele(i)%NU
       Mesh%LogFr(j) = ele(i)%log_type       

    ENDDO

    DEALLOCATE(ele)
        
  END SUBROUTINE ReadMeshGMSH2
  !===========================


#ifdef ORDRE_ELEVE
  SUBROUTINE ReadSegCom( Com , Mesh, FileName)
    CHARACTER(LEN=*), PARAMETER :: mod_name = "GEOM/ReadSegCom"
    ! ****************
    !   declarations
    ! ****************
    ! variables d'appel
    TYPE(MeshDef), INTENT(INOUT)    :: Mesh
    TYPE(MeshCom), INTENT(INOUT)    :: Com
    CHARACTER(LEN=*), INTENT(IN)    :: FileName

    INTEGER , PARAMETER  :: MyUnit = 44
    INTEGER :: Ndom , MaxR , MaxS , Ndim
    INTEGER :: i , j , Elt , Pt1 , Pt2

    REAL , DIMENSION(Mesh%Ndim)     :: Coor

    INTEGER :: Nseg, jseg

    INTEGER                          :: NdomVSeg   , MaxSndSeg , MaxRcvSeg
    INTEGER, DIMENSION(:)  , POINTER :: IdVoisSeg                ! [1:NdomvSeg]
    INTEGER, DIMENSION(:)  , POINTER :: NSndSeg    , NRcvSeg     ! [1:NdomvSeg]
    INTEGER, DIMENSION(:,:), POINTER :: MapSndSeg  , MapRcvSeg   ! [1:Max..Seg,1:NdomvSeg]

    Ndim = Mesh%Ndim
    OPEN(UNIT=MyUnit , FILE = TRIM(Filename)//".pseg")

    READ(MYUnit,*) Ndom, MaxR, MaxS
    NdomVSeg = Ndom
    MaxRcvSeg = MaxR
    MaxSndSeg = MaxS

    ! KKL : Verifier des choses importantes ici
    ! 1) Meme nombre de procs voisins qu'avec les com points
    ! 2) Meme ordre des voisins (normalement si 1 est ok ici c'est bon)

    ! On alloue tout ce qui est necessaire a la lecture des segments de com
    ! KKL : Vu que la renumerotatione st faite juste apres il n'y a pas besoin
    ! d'alourdir MeshCom avec des membres supplementaires

    ALLOCATE( IdVoisSeg(Ndom) , NRcvSeg(Ndom) , NSndSeg(Ndom) )
    ALLOCATE( MapRcvSeg(MaxR,Ndom) , MapSndSeg(MaxS,Ndom) )
    MapRcvSeg = -10
    MapSndSeg = -10

    DO i = 1,Ndom
       READ(MYUnit,*) IdVoisSeg(i) , NRcvSeg(i) , NSndSeg(i)
       DO j = 1, NRcvSeg(i)
          READ(MYUnit,*) Elt, Pt1, Pt2
          READ(MYUnit,*) Coor
          Pt1 = Com%SegPerm(Pt1)
          Pt2 = Com%SegPerm(Pt2)
          MapRcvSeg(j,i) = FindSeg(Elt,Pt1,Pt2)
          Mesh%Coor(1:Ndim, MapRcvSeg(j,i) + Mesh%Npoint)  = Coor(1:Ndim)
       ENDDO
       DO j = 1, NSndSeg(i)
          READ(MYUnit,*) Elt, Pt1, Pt2
          Pt1 = Com%SegPerm(Pt1)
          Pt2 = Com%SegPerm(Pt2)
          MapSndSeg(j,i) = FindSeg(Elt,Pt1,Pt2)
       ENDDO
    ENDDO
    IdVoisSeg = IdVoisSeg - 1

    ! High order mesh
    IF( Mesh%Nordre == 3 ) THEN

       READ(MYUnit,*) Nseg

       DO i = 1, Nseg

          READ(MYUnit,*) Pt1, Pt2, Coor

          Pt1 = Com%SegPerm(Pt1)
          Pt2 = Com%SegPerm(Pt2)

          jseg = FindSeg2(Pt1, Pt2)

          IF (jseg /= 0) THEN
             ! Should be ok also for the tetra,
             ! to be checked 
             Mesh%Coor(1:Ndim, jseg+Mesh%Npoint) = Coor(1:Ndim)
          END IF

       ENDDO

    ENDIF

    CLOSE(MyUnit)

    DEALLOCATE(Com%SegPerm)
    IF (see_alloc) CALL alloc_deallocate(mod_name, "com%segperm")

    CALL RenumP2Com(Com, Mesh)

    Mesh%Nordre = 3

    DEALLOCATE(IdVoisSeg, NRcvSeg, NSndSeg)
    DEALLOCATE(MapRcvSeg, MapSndSeg)

  CONTAINS
    FUNCTION FindSeg(elt , pt1 , pt2) RESULT(S)
      INTEGER , INTENT(IN) :: elt,pt1,pt2
      INTEGER :: S

      INTEGER :: i ,mini,maxi

      mini = MIN(pt1,pt2)
      maxi = MAX(pt1,pt2) 

      DO i = 1,SIZE(Mesh%NuSeg,1)
         S = Mesh%NuSeg(i,elt)
         IF (S <= 0) CYCLE
         IF (MINVAL(Mesh%Nubo(:,S)) == mini .AND. &
             MAXVAL(Mesh%Nubo(:,S)) == maxi) RETURN 
      END DO

      S = 0
      PRINT *, "Segment non trouve : ", TRIM(FileName), elt,pt1,pt2, "-",Mesh%Nu(:,elt)
      CALL delegate_stop()
    END FUNCTION FindSeg


    FUNCTION FindSeg2(pt1 , pt2) RESULT(S)
      INTEGER , INTENT(IN) :: pt1, pt2
      INTEGER :: S

      INTEGER :: mini,maxi

      mini = MIN(pt1,pt2)
      maxi = MAX(pt1,pt2) 

!!$! --- dante: maybe not correct since the the numeration
!!$!            on the boundary segment is not in an incresing order anymore
!!$      DO S = 1, Mesh%Nsegmt
!!$         IF (Mesh%Nubo(1,S) == mini) THEN
!!$            IF (maxi ==  Mesh%Nubo(2,S) ) THEN
!!$               RETURN 
!!$            END IF
!!$         END IF
!!$      END DO
!!$

      DO S = 1, Mesh%Nsegmt

         IF( MINVAL(Mesh%Nubo(:, S)) == mini .AND.  &
             MAXVAL(Mesh%Nubo(:, S)) == maxi) RETURN 

      END DO
     
      PRINT *, "Segment non trouve : ", TRIM(FileName), pt1,pt2
      PRINT *, "dans la liste", Mesh%Nubo
      S = 0
      CALL delegate_stop()
    END FUNCTION FindSeg2



    SUBROUTINE RenumP2Com(Com, Mesh)
      CHARACTER(LEN=*) , PARAMETER :: mod_name = "RenumP2Com"
      TYPE(MeshCom), INTENT(INOUT) :: Com
      TYPE(MeshDef), INTENT(INOUT) :: Mesh

      LOGICAL, DIMENSION(:), ALLOCATABLE :: Placed
      INTEGER, DIMENSION(:), ALLOCATABLE :: Perm

      INTEGER :: NbTot, iv, j, is, NbTotSnd, NbTotRcv

      ! Variables pour la copie des comm
      INTEGER, DIMENSION(:,:), ALLOCATABLE :: tmp
      INTEGER :: oldsnd2max, oldrcv2max


      ! Recopie de ReadMeshParallel
      ALLOCATE( Placed(Mesh%NCells), Perm(Mesh%NCells) )

      ! On met les comm send au d�but et les Recv � la fin (pour pouvoir les �carter facilement le cas �cheant car
      ! ce sont des points qui ne sont pas dans l'int�rieur du domaine).
      Placed = .FALSE.
      Perm = 0


      NbTot = 0
      DO is = 1, Com%NPsnd2tot
         Perm(is) = is
         Placed(is) = .TRUE.
      END DO

      NbTot = Com%NPsnd2tot
      ! ATTENTION: il est possible d'avoir plusieurs �missions � partir d'un point !!!!!!!
      DO iv = 1, NdomVSeg
         DO j = 1, NSndSeg(iv)
            is = MapSndSeg(j, iv) + Mesh%NPoint
            IF (.NOT. Placed(is)) THEN
               NbTot = NbTot + 1
               Perm(is) = NbTot
               Placed(is) = .TRUE.
            END IF
         END DO
      END DO
      NbTotSnd = NbTot


      NbTot = 0
      DO j = Mesh%Npoint - Com%NPrcv2tot + 1, Mesh%Npoint
         is = j +  Mesh%NSegmt
         Perm(j) = is
         Placed(j) = .TRUE.
      END DO


      ! PAR contre, on DOIT avoir une seule r�ception par point
      NbTot = Com%NPrcv2tot
      DO iv = NdomVSeg, 1, -1
         DO j = NRcvSeg(iv), 1, -1
            is = MapRcvSeg(j, iv) + Mesh%NPoint
            NbTot = NbTot + 1
            Perm(is) = Mesh%NCells + 1 - NbTot
            IF (Placed(is)) THEN
               PRINT *, mod_name, " ERREUR : Noeud rcv deja trie....!!!!!"
               CALL delegate_stop()
            END IF
            Placed(is) = .TRUE.
         END DO
      END DO
      NbTotRcv = NbTot

      NbTot = NbTotSnd
      DO is = 1, Mesh%NCells
         IF (.NOT. Placed(is)) THEN
            NbTot = NbTot + 1
            Perm(is) = NbTot
            Placed(is) = .TRUE.
         END IF
      END DO

      IF (NbTot + NbTotRcv /= Mesh%NCells ) THEN
         PRINT *, mod_name, " ERREUR : Probleme dans le reordering", NbTot + NbTotRcv, Mesh%NCells
         CALL delegate_stop()
      END IF
      DEALLOCATE(Placed)

      !On applique la permutation sur les com
      DO iv = 1, Com%Ndomv
         Com%PTsnd2(1: Com%NPsnd2(iv), iv) = Perm(Com%PTsnd2(1: Com%NPsnd2(iv), iv))
         Com%PTrcv2(1: Com%NPrcv2(iv), iv) = Perm(Com%PTrcv2(1: Com%NPrcv2(iv), iv))
      END DO

      DO iv = 1, NdomVSeg
         MapSndSeg(1: NSndSeg(iv), iv) = Perm(MapSndSeg(1: NSndSeg(iv), iv)+Mesh%Npoint)
         MapRcvSeg(1: NRcvSeg(iv), iv) = Perm(MapRcvSeg(1: NRcvSeg(iv), iv)+Mesh%Npoint)
      END DO



      Mesh%Coor(:,Perm(:)) = Mesh%Coor(:, :)

      DO j = 1,Mesh%Nelemt
         Mesh%Nu(:, j) = Perm( Mesh%Nu(:, j) )         
      END DO

      DO j = 1,Mesh%NFacFr
         Mesh%NsFacFr(:, j) = Perm( Mesh%NsFacFr(:, j) )         
      END DO
      DO j = 1,Mesh%NSegmt
         Mesh%Nubo(:, j) = Perm( Mesh%Nubo(:, j) )         
      END DO

      DEALLOCATE(Perm)

      ! On met toutes les comm dans le conteneur P1
      Com%NPsnd2(:) = Com%NPsnd2(:) + NSndSeg(:)
      Com%NPrcv2(:) = Com%NPrcv2(:) + NRcvSeg(:)
      oldsnd2max = Com%Nsnd2Max
      oldrcv2max = Com%Nrcv2Max

      Com%MaxLengthSnd2 = MAXVAL(Com%NPsnd2)
      Com%MaxLengthRcv2 = MAXVAL(Com%NPrcv2)

      Com%Nsnd2Max = MAXVAL(Com%NPsnd2 )
      Com%Nrcv2Max = MAXVAL(Com%NPrcv2 )

      ! Recopie de PTsnd2
      ALLOCATE( tmp(oldsnd2Max, Com%Ndomv) )
      tmp(:,:) = Com%PTsnd2(:,:)
      DEALLOCATE(Com%PTsnd2)

      ALLOCATE( Com%PTsnd2(Com%Nsnd2Max, Com%Ndomv) )
      Com%PTsnd2(1:oldsnd2Max, :) = tmp(:,:)
      DEALLOCATE( tmp)


      ! Recopie de PTrcv2
      ALLOCATE( tmp(oldrcv2Max, Com%Ndomv) )
      tmp(:,:) = Com%PTrcv2(:,:)
      DEALLOCATE(Com%PTrcv2)

      ALLOCATE( Com%PTrcv2(Com%Nrcv2Max, Com%Ndomv) )
      Com%PTrcv2(1:oldrcv2Max, :) = tmp(:,:)
      DEALLOCATE( tmp)


      !Compl�ment pour PTsnd2 et PTrcv2
      DO iv = 1, Com%NdomV
         Com%PTsnd2(Com%NPsnd2(iv)-NSndSeg(iv)+1: Com%NPsnd2(iv), iv) = MapSndSeg(1: NSndSeg(iv), iv)
         Com%PTrcv2(Com%NPrcv2(iv)-NRcvSeg(iv)+1: Com%NPrcv2(iv), iv) = MapRcvSeg(1: NRcvSeg(iv), iv)
      END DO
      Com%NPsnd2tot = NbTotSnd
      Com%NPrcv2tot = NbTotRcv

    END SUBROUTINE RenumP2Com


  END SUBROUTINE ReadSegCom


#endif

  SUBROUTINE RenumTest( Mesh)
    CHARACTER(LEN=*) , PARAMETER :: mod_name = "RenumTest"
    TYPE(MeshDef), INTENT(INOUT) :: Mesh

    INTEGER, DIMENSION(:), ALLOCATABLE :: Perm

    INTEGER :: idPt, jt, j, is

    ! Recopie de ReadMeshParallel
    ALLOCATE( Perm(Mesh%NCells) )

    ! On parcours les elements dans l'ordre et on numerote leur ddls de facon contigue
    Perm = 0

    idPt = 0
    DO jt = 1, Mesh%Nelemt
       DO j = 1, Mesh%Ndegre(jt)
          is = Mesh%Nu(j, jt)
          if ( Perm(is) == 0 ) THEN
             idPt = idPt + 1
             Perm(is) = idPt
          END if
       END DO
    END DO

    IF (idPt /= Mesh%NCells ) THEN
       PRINT *, mod_name, " ERREUR : Probleme dans le reordering", idPt, Mesh%NCells
       CALL delegate_stop()
    END IF

    Mesh%Coor(:,Perm(:)) = Mesh%Coor(:, :)

    DO j = 1,Mesh%Nelemt
       Mesh%Nu(:, j) = Perm( Mesh%Nu(:, j) )         
    END DO

    DO j = 1,Mesh%NFacFr
       Mesh%NsFacFr(:, j) = Perm( Mesh%NsFacFr(:, j) )         
    END DO
    DO j = 1,Mesh%NSegmt
       Mesh%Nubo(:, j) = Perm( Mesh%Nubo(:, j) )         
    END DO

    DEALLOCATE(Perm)

  END SUBROUTINE RenumTest

  FUNCTION produit_vectoriel_geom(v1, v2) RESULT(vv)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "produit_vectoriel_geom"
    REAL, DIMENSION(: ), INTENT(IN) :: v1, v2
    REAL, DIMENSION(3) :: vv

    IF (SIZE(v1) /= 3 .OR. SIZE(v2) /= 3) THEN
       PRINT *, mod_name, " ERREUR : size(v1)==", SIZE(v1), SIZE(v2), "/=3"
       CALL delegate_stop()
    END IF

    vv(1) = v1(2) * v2(3) - v1(3) * v2(2)
    vv(2) = v1(3) * v2(1) - v1(1) * v2(3)
    vv(3) = v1(1) * v2(2) - v1(2) * v2(1)
  END FUNCTION produit_vectoriel_geom

  !***********************************************************************
  !                                               -- info
  !***********************************************************************
  !!@author -- (C) R.Butel   UMR 5251 IMB   C.N.R.S.  2008/04/03

  !!-- Summary :
  !!-- End Summary

  SUBROUTINE info(call_name, file_name, extension) !-- Interface

    CHARACTER(LEN = *), PARAMETER :: mod_name = "info"

    !-- arguments

    CHARACTER(LEN = *), INTENT(IN) :: call_name, file_name, extension

    !-- locales

    !--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    !                                               -- info.1
    !-----------------------------------------------------------------------

    PRINT *, "La methode ", call_name, " demande un fichier `" // extension // "'"
    PRINT *, "Le fichier propose est ", TRIM(file_name)
    PRINT *, "Le type de fichier dans NumMeth est rappele sur la ligne imprimee MeshFile==*"
    PRINT *, "Les fichiers presents dans le repertoire d'execution sont :"
    CALL system("pwd")
    CALL system("/bin/ls -l")

  END SUBROUTINE info

  !======================
  SUBROUTINE check_mesh()
  !======================

    IMPLICIT NONE

    INTEGER :: n_ele, je, k, iq
    REAL :: tot_V

    tot_V = 0.d0

    DO je = 1, SIZE(d_element)
IF( je > 24 ) cycle
       WRITE(*, '("ELE: " I6, "  Vol: ", E12.6)') je, SUM(d_element(je)%e%w_q)

       DO k = 1, d_element(je)%e%N_points

          WRITE(*, '(I6, ": " 3(E13.6, ", "))') d_element(je)%e%Nu(k), d_element(je)%e%Coords(:, k)

       ENDDO

       WRITE(*,*) 'RD n'
       WRITE(*,*) '----------'
       DO k = 1, d_element(je)%e%N_verts
          WRITE(*, '(3(F12.6, ", "))' ) d_element(je)%e%rd_n(:, k)
       ENDDO

       tot_V = tot_v + SUM(d_element(je)%e%w_q)

       WRITE(*,*) 'Quadrature'
       write(*,*) '----------'     
       DO iq = 1, d_element(je)%e%N_quad
          
          write(*, '(3(F12.6, ", "))') d_element(je)%e%xx_q(:, iq)

       ENDDO

       WRITE(*,*) 'FACES'
   
       DO k = 1,  d_element(je)%e%N_Faces

          WRITE(*, '("G_f ", I6,  " C_E ", I6)' ) d_element(je)%e%Faces(k)%f%g_face, &
                                                  d_element(je)%e%Faces(k)%f%c_ele

          DO iq = 1, d_element(je)%e%Faces(k)%f%N_quad

              WRITE(*, '(6(F12.6, ", "))') d_element(je)%e%Faces(k)%f%xx_q(:, iq), &
                                           d_element(je)%e%Faces(k)%f%n_q(: , iq)
             
          ENDDO

          WRITE(*,*) '-----------------------'

       ENDDO
       
       WRITE(*,*)

    ENDDO

    WRITE(*, '("Volume ", F24.12)') tot_V

  END SUBROUTINE check_mesh
  !========================

  !=======================
  SUBROUTINE check_bmesh()
  !=======================

    IMPLICIT NONE

    INTEGER :: jf, k, iq

    REAL :: tot_S

    tot_S = 0.d0

    IF(SIZE(b_element) == 0) RETURN

    WRITE(*, '("$ DATA=VECTOR")')
    WRITE(*, '("% vscale = 0.1")')

    DO jf = 1, SIZE(b_element)

!       WRITE(*, '("b_FACE: " I6, I2, F12.6)') jf, b_element(jf)%b_f%bc_type, &
!                                              SUM(b_element(jf)%b_f%w_q)

       DO k = 1, b_element(jf)%b_f%N_points

!          WRITE(*, '(I6, ": " 2(F10.5))') b_element(jf)%b_f%Nu(k), b_element(jf)%b_f%Coords(:, k)

       ENDDO

       tot_S = tot_S + SUM(b_element(jf)%b_f%w_q)

!       WRITE(*,*) 'Normals'
if(b_element(jf)%b_f%bc_type == 1 ) then

       DO k = 1, b_element(jf)%b_f%N_points

          WRITE(*, '(6(F10.5))')  b_element(jf)%b_f%Coords(:, k), &
                                  b_element(jf)%b_f%n_b(: , k)

       ENDDO
endif
!       WRITE(*,*)

    END DO
    
    WRITE(*, '("$END")')
    
!    WRITE(*, '("Surface ", F24.12)') tot_S

  END SUBROUTINE check_bmesh
  !=========================

END MODULE ReadMesh
