!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga, M.Papin                                 */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  !!@author Module initialement Cod� par B. NKonga, Univ. Bx I, LaMAB
  !!
  !!@author Revisit� par M. Papin, Univ. Bx I, LaMAB le 05/09/2003
  !!@author Revisit� par M. Papin, INRIA, Univ. Bx I le 11/01/2006
  !!
  !! Role: interface parall�le MPI pour Fluids/FluidBox

   MODULE  LibComm
      USE types_enumeres
      USE Lestypes

#ifdef MPICH2
      USE mpi
#endif
      IMPLICIT NONE

      INTERFACE Reduce
         MODULE PROCEDURE VR_Reduce  !! Vecteur de real
         MODULE PROCEDURE VI_Reduce  !! Vecteur de integer
         MODULE PROCEDURE SR_Reduce  !! Simple real
         MODULE PROCEDURE SI_Reduce  !! simple integer
      END INTERFACE

#ifndef MPICH2
      INCLUDE "mpif.h"
#endif

      INTEGER, PRIVATE            :: StatInfo
      INTEGER, PRIVATE, SAVE      :: FluidBox_Comm = 0

  ! Variables locales tableaux
  !----------------------------
!--OFF      INTEGER, DIMENSION( mpi_status_size ) ::  Status

      INTEGER, PRIVATE, SAVE :: type_real, type_int = -100

      INTEGER, PRIVATE, SAVE :: MsgTag = 0

   CONTAINS

      SUBROUTINE stop_run(ident)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "stop_run"
         INTEGER, INTENT(IN), OPTIONAL :: ident
         PRINT *, mod_name, "MPI_Abort va demarrer"
         IF (Present(ident)) THEN
            CALL delegate_stop(ident)
         ELSE
            CALL delegate_stop( -1)
         END IF
      END SUBROUTINE stop_run


    !     -----------------------------------------------
    !!     r�le : Phase d'initilisation des communications
    !!
    !!     les variables de la structure "Com" initialis�es dans le cas parall�le sont :
    !!
    !!         Ntasks    = nombre total de t�ches
    !!
    !!         MyId      = identification de la t�che courante
    !!
    !!         Me        =  num�ro de la t�che courante
    !!
    !!         GroupName = Non du groupe de la t�che courante
    !
    !     -----------------------------------------------
    !!@author     Boniface Nkonga      Universite de Bordeaux 1
    !     -----------------------------------------------
    !!
    !!@author     Modif M. PAPIN pour Fluids
    !!@author     Modif P.Jacq pour le couplage

      SUBROUTINE IniCom(Com)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "IniCom"

    !--------------------------------------------------------------------
    !                 Sp�cifications et D�clarations
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------

         TYPE(MeshCom), INTENT(INOUT)       :: Com

    ! Variables locales scalaires
    !----------------------------

         INTEGER             :: Me, MyTid, BarrierOpt, NTasks
         INTEGER             :: itask
    !--------------------------------------------------------------------
    !                Partie ex�cutable de la proc�dure
    !--------------------------------------------------------------------

         BarrierOpt               = 0

    ! Enr�le dans MPI

         CALL mpi_init(StatInfo)

         IF (StatInfo /= 0) THEN
            PRINT *, mod_name, " : ERREUR : StatInfo(Mpi_init)==", StatInfo
            CALL stop_run()
         END IF

         ! P. JACQ : Cr�ation d'un communicateur priv� (important pour couplage)
         FluidBox_Comm = mpi_comm_world

         CALL mpi_comm_size(FluidBox_Comm, NTasks, StatInfo)
         CALL mpi_comm_rank(FluidBox_Comm, Me, StatInfo)
         MyTid = Me
         PRINT *, mod_name, " Me==", Me, " NTasks==", NTasks


         ! Remplissage de la structure "COM"

         !  Defines the direct and inverse mapping

         !if (ntasks==1) return

         ALLOCATE( Com%Tids(0: NTasks -1) )
         Com%Tids = 0
         ALLOCATE( Com%MeInGr(1: NTasks) )
         Com%MeInGr = 0
         ALLOCATE( Com%MapDom(1: NTasks) )
         ALLOCATE( Com%InvMap(0: NTasks-1) )
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "com%tids", SIZE(Com%Tids))
            CALL alloc_allocate(mod_name, "com%meingr", SIZE(Com%MeInGr))
            CALL alloc_allocate(mod_name, "com%mapdom", SIZE(Com%MapDom))
            CALL alloc_allocate(mod_name, "com%invmap", SIZE(Com%InvMap))
         END IF

         ! correction for a warning with gfortran on mygale 
         itask = 0
         
         Com%MapDom      = (/ (itask - 1, itask=1, NTasks) /)
         Com%InvMap      = (/ (itask, itask=1, NTasks) /)
         Com%NTasks      = NTasks
         Com%MyTid       = MyTid
         Com%Me          = Me
         Com%Tids(Me)    = MyTid
         Com%Ngroup      =  1
         Com%MeInGr( 1 ) = Me

         Com%GroupName   = "Tous"
    !
    ! IENCOD = 0 : MPIDEFAULT
    !          1 : MPIRAW
    !          2 : MPIINPLACE
    !
         Com%iencod        = 1

         Com%Tcom          = 0.0
         Com%Twait         = 0.0
         Com%TechgSolu     = 0.0
         Com%TechgGrad     = 0.0
         Com%Treduce       = 0.0
         IF (KIND(1.0)==SELECTED_REAL_KIND(14)) THEN
            type_real = mpi_double_precision
         ELSE IF (KIND(1.0)==SELECTED_REAL_KIND(6)) THEN
            type_real = mpi_real
         ELSE
            PRINT *, mod_name, " : ERREUR : kind(1.0)==", KIND(1.0), " cas non pris en compte"
            CALL stop_run()
         END IF
         IF (KIND(1) ==SELECTED_INT_KIND(14) .OR.  KIND(1) ==SELECTED_INT_KIND(6)) THEN
            type_int  = mpi_integer
         END IF
         IF (type_int < 0) THEN
            PRINT *, mod_name, " : ERREUR : kind(1)==", KIND(1), " cas non pris en compte"
            CALL stop_run()
         END IF
         ! P.J : Set to True when used with the moulinette to get the number of process
         Com%GetNTasks = .FALSE.
      END SUBROUTINE IniCom

      SUBROUTINE bcast(un_entier)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "bcast"
         INTEGER, INTENT(IN) :: un_entier
         INTEGER :: pst

         CALL mpi_bcast(un_entier, 1, mpi_integer, 0, fluidbox_comm, pst)

         IF (pst < 0) THEN
            PRINT *, mod_name, ' ERREUR dans bcast : ', pst
            CALL stop_run()
         END IF

      END SUBROUTINE bcast

      SUBROUTINE  VR_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "VR_Reduce"
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         INTEGER, INTENT(IN)                  ::  FCas
         REAL, DIMENSION(: ), INTENT(INOUT)   ::  VecToRdc
         REAL, DIMENSION(Size(VecToRdc))   :: buffer
         INTEGER   :: Length
         REAL      :: Temps_debut, Temps_final

    !
         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()
    !
         Length           = Size(VecToRdc)
         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_real, mpi_min, FluidBox_Comm, StatInfo)
            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_real, mpi_max, FluidBox_Comm, StatInfo)
            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_real, mpi_sum, FluidBox_Comm, StatInfo)
            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans VR8 Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT
         VecToRdc  = buffer
    !
         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans VR8 MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF
    !
    !****************************************
         Temps_final     = CpuTime()
         Com%Treduce     = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE VR_Reduce


      SUBROUTINE  VI_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "VI_Reduce"
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         INTEGER, INTENT(IN)      ::  FCas
         INTEGER, DIMENSION(: ), INTENT(INOUT)   ::  VecToRdc
         INTEGER, DIMENSION(Size(VecToRdc)) ::  buffer
         INTEGER   :: Length
         REAL      :: Temps_debut, Temps_final
         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()
    !
         Length           = Size(VecToRdc)
         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_int, mpi_min, FluidBox_Comm, StatInfo)
            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_int, mpi_max, FluidBox_Comm, StatInfo)
            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_int, mpi_sum, FluidBox_Comm, StatInfo)
            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans VI4 Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT
    !
         VecToRdc = buffer

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans VI4, MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF
    !****************************************
         Temps_final     = CpuTime()
         Com%Treduce      = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE VI_Reduce


      SUBROUTINE  SR_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SR_Reduce"
         TYPE(MeshCom), INTENT(INOUT)   ::  Com
         INTEGER, INTENT(IN)      ::  FCas
         REAL, INTENT(INOUT)   ::  VecToRdc
         REAL                                ::  buffer
         REAL      :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_real, mpi_min, FluidBox_Comm, StatInfo)
            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_real, mpi_max, FluidBox_Comm, StatInfo)
            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_real, mpi_sum, FluidBox_Comm, StatInfo)
            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans SR Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT

         VecToRdc = buffer

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans SR, MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF
    !
    !****************************************
         Temps_final      = CpuTime()
         Com%Treduce      = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE SR_Reduce

      SUBROUTINE  SI_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SI_Reduce"
         TYPE(MeshCom), INTENT(INOUT)  :: Com
         INTEGER, INTENT(IN)     ::  FCas
         INTEGER(KIND = 4), INTENT(INOUT)  ::  VecToRdc
         INTEGER(KIND = 4) :: buffer
         REAL      :: Temps_debut, Temps_final
         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()
    !
         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_int, mpi_min, FluidBox_Comm, StatInfo)

            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_int, mpi_max, FluidBox_Comm, StatInfo)

            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_int, mpi_sum, FluidBox_Comm, StatInfo)

            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans SI4 Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT

         VecToRdc = buffer
    !
         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans SI4, MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF

    !****************************************
         Temps_final      = CpuTime()
         Com%Treduce      = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE SI_Reduce

    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeInit(Com, Data)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "EchangeInit"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         TYPE(Donnees), INTENT(IN)      :: Data
         INTEGER    :: MaxLengthRcv, MaxLengthSnd
         REAL       :: Temps_debut, Temps_final
         INTEGER    :: Nvar, Ndomv, Ndim

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF

         Temps_debut = CpuTime()
         Ndomv   = Com%Ndomv
         Nvar    = Data%NvarPhy !-- gradients � l'ordre 2 => besoin de la pression
         Ndim = Data%Ndim

         MaxLengthSnd = 0
         MaxLengthRcv = 0
         SELECT CASE (Data%sel_approche)
            CASE(cs_approche_vertexcentered)      !-- Vertex centered !
               MaxLengthSnd = Com%MaxLengthSnd2
               MaxLengthRcv = Com%MaxLengthRcv2
               Com%LNiveau  = 2
            CASE DEFAULT
               WRITE(6, *) mod_name, " ERREUR : L'approche ", Data%Approche, " n'est pas traitee"
               CALL stop_run()
         END SELECT

         ALLOCATE(Com%SndData(Ndim * Nvar * MaxLengthSnd, Ndomv ) ) ! PJ 09/02/09 taille*Ndim pour les gradients
         ALLOCATE(Com%RcvData(Ndim * Nvar * MaxLengthRcv, Ndomv ) )
         ALLOCATE(Com%Request(2 * Ndomv), Com%Stat(mpi_status_size, 2 * Ndomv))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "com%snddata", Size(Com%SndData))
            CALL alloc_allocate(mod_name, "com%rcvdata", Size(Com%RcvData))
            CALL alloc_allocate(mod_name, "com%request", Size(Com%Request))
            CALL alloc_allocate(mod_name, "com%stat", Size(Com%Stat))
         END IF

    !****************************************
         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
    !****************************************

      END SUBROUTINE EchangeInit


    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeSolRecv( Com, Ua )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "EchangeSolRecv"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         REAL, DIMENSION(:, : ), INTENT(INOUT)   :: Ua
    !REAL, DIMENSION(:, :), pointer   :: Ua
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: iv, j ,is, ipos, Nvar, Ndomv
         REAL       :: Temps_debut, Temps_final
!    INTEGER, dimension(MPI_STATUS_SIZE, 1) :: Stat

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

         Ndomv   = Com%Ndomv
         Nvar    = Size(Ua, 1)

         CALL mpi_waitall(2 * Ndomv, Com%Request, Com%Stat, StatInfo) ! On attend tout

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
!       DO ivv = 1, NdomV  ! On laisse tomber, ca fout le b... dans la continuit� m�moire
!          CALL MPI_WAITANY( NdomV, Com%Request(1:NdomV), iv, Stat, StatInfo )
                  DO  j = 1, Com%NPrcv2(iv)
                     is                   = Com%PTrcv2(j, iv)
                     ipos                 = (j -1) * Nvar + 1
                     Ua(1: Nvar, is)        = Com%RcvData(ipos: ipos + Nvar -1, iv)
                  END DO
               END DO
         END SELECT

         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
      END SUBROUTINE EchangeSolRecv

    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeSolPrepSend( Com, Ua )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "EchangeSolPrepSend"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         REAL, DIMENSION(:, : ), INTENT(INOUT)   :: Ua
    !REAL, DIMENSION(:, :), pointer   :: Ua
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: iv, j, is, ipos
         INTEGER    :: Nvar, MyTid, Domv
         INTEGER    :: MsgLength, FromId, DestId, NumbMsg, Ndomv
         REAL       :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF

         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag = MsgTag + 1

         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv
         Nvar    = Size(Ua, 1)

         Com%Request = mpi_request_null
    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            FromId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPrcv2(iv)  !-- Vertex centered !
            MsgLength = Nvar * NumbMsg
            CALL mpi_irecv( Com%RcvData(1, iv), MsgLength, type_real, &
            &  FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO

    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv, 1, -1
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            DestId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPsnd2(iv)
            DO  j = 1, NumbMsg
               is                            = Com%PTsnd2(j, iv)
               ipos                          = (j -1) * Nvar + 1
               Com%SndData(ipos: ipos + Nvar -1, iv) = Ua(:, is)
            END DO
            MsgLength = Nvar * NumbMsg
            CALL mpi_isend(Com%SndData(1, iv), MsgLength, type_real, DestId, MsgTag, FluidBox_Comm, Com%Request(Ndomv + iv), &
            & StatInfo)
         END DO

         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
      END SUBROUTINE EchangeSolPrepSend


    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeSol( Com, Ua)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "EchangeSol"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         REAL, DIMENSION(:, : ), INTENT(INOUT)   :: Ua
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: iv, j, is, ipos
         INTEGER    :: Nvar, MyTid, Domv
         INTEGER    :: MsgLength, FromId, DestId, NumbMsg, Ndomv
         REAL       :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag = MsgTag + 1
    !
         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv
         Nvar    = Size(Ua, 1)

         Com%Request = mpi_request_null

    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            FromId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPrcv2(iv)  !-- Vertex centered !
            MsgLength = Nvar * NumbMsg
            CALL mpi_irecv( Com%RcvData(1, iv), MsgLength, type_real, &
            &  FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO
    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv, 1, -1
            Domv  =  Com%Jdomv(iv)        ! Numero du (iv)ieme sous-domaine voisin
            DestId =  Com%MapDom(Domv)    ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPsnd2(iv)
            DO  j = 1, NumbMsg
               is                                   = Com%PTsnd2(j, iv)
               ipos                                 = (j -1) * Nvar + 1
               Com%SndData(ipos: ipos + Nvar -1, iv) = Ua(:, is)
            END DO
            MsgLength = Nvar * NumbMsg
            CALL mpi_isend(Com%SndData(1, iv), MsgLength, type_real, DestId, &
            &  MsgTag, FluidBox_Comm, Com%Request(Ndomv + iv), StatInfo)
         END DO

         CALL mpi_waitall(2 * Ndomv, Com%Request, Com%Stat, StatInfo)

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
                  DO  j = 1, Com%NPrcv2(iv)
                     is                   = Com%PTrcv2(j, iv)
                     ipos                 = (j -1) * Nvar + 1
                     Ua(1: Nvar, is)        = Com%RcvData(ipos: ipos + Nvar -1, iv)
                  END DO
               END DO
         END SELECT
    !****************************************
         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE EchangeSol



    !     -----------------------------------------------
    !!@author     Pascal JACQ (adaptation de EchangeSol de Mikael Papin)
    !     -----------------------------------------------
      SUBROUTINE EchangeGrad( Com, Grad)
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom)       , INTENT(IN OUT)   :: Com
         REAL, DIMENSION(:,:,:), INTENT(IN OUT)   :: Grad
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: iv, j, is, ipos,i
         INTEGER    :: Nvar, MyTid, Domv, Dim
         INTEGER    :: MsgLength, FromId, DestId, NumbMsg, Ndomv
         REAL       :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) RETURN
         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag=MsgTag+1
    !
         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv
         Dim     = Size(Grad,1)
         Nvar    = Size(Grad,2)

         Com%Request = mpi_request_null

    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            FromId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPrcv2(iv)  !-- Vertex centered !
            MsgLength = Dim*Nvar*NumbMsg
            CALL mpi_irecv( Com%RcvData(1, iv), MsgLength, type_real, &
            &  FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO
    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv,1,-1
            Domv  =  Com%Jdomv(iv)        ! Numero du (iv)ieme sous-domaine voisin
            DestId =  Com%MapDom(Domv)    ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPsnd2(iv)
            DO  j = 1, NumbMsg , 1
               is                                   = Com%PTsnd2(j,iv)
               ipos                                 = (j-1)*Nvar*Dim + 1
               do i = 1,Nvar
                  Com%SndData(ipos:ipos+dim-1, iv) = Grad(1:dim,i,is)
                  ipos = ipos + dim
               end do
            END DO
            MsgLength = Dim*Nvar*NumbMsg
            CALL mpi_isend(Com%SndData(1,iv), MsgLength , type_real, DestId,&
            &  MsgTag, FluidBox_Comm,Com%Request(Ndomv+iv),StatInfo)
         END DO

         CALL mpi_waitall(2*Ndomv, Com%Request,Com%Stat,StatInfo)

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
                  DO  j = 1, Com%NPrcv2(iv) , 1
                     is                   = Com%PTrcv2(j,iv)
                     ipos                 = (j-1)*Nvar*Dim + 1
                     do i = 1,Nvar
                        Grad(1:dim,i,is) = Com%RcvData(ipos:ipos+Dim-1,iv)
                        ipos = ipos + dim
                     end do
                  END DO
               END DO
         END SELECT
    !****************************************
         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE EchangeGrad



    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeVec( Com, Vec, Data )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "EchangeVec"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         TYPE(Donnees), INTENT(IN)      :: Data
         REAL, DIMENSION(: ), INTENT(INOUT)   :: Vec
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: iv, j, is
         INTEGER    :: MyTid, Domv
         INTEGER    :: MsgLength, FromId, DestId, Ndomv
         REAL       :: Temps_debut, Temps_final
         INTEGER :: MaxLengthSnd, MaxLengthRcv
    ! Variables locales tableaux
    !----------------------------
         REAL, DIMENSION(:, : ), ALLOCATABLE    :: SndData, RcvData
         INTEGER :: istat

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag = MsgTag + 1
    !
         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv

         MaxLengthSnd = 0
         MaxLengthRcv = 0
         SELECT CASE (Data%sel_approche)
            CASE(cs_approche_vertexcentered)      !-- Vertex centered !
               MaxLengthSnd = Com%MaxLengthSnd2
               MaxLengthRcv = Com%MaxLengthRcv2
               Com%LNiveau  = 2
            CASE DEFAULT
               WRITE(6, *) mod_name, " ERREUR : L'approche ", Data%Approche, " n'est pas traitee"
               CALL stop_run()
         END SELECT

         IF (.FALSE.) THEN
            WRITE(7 + MyTid, *) mod_name, " MaxLengthSnd==", MaxLengthSnd, Ndomv, MyTid
            WRITE(7 + MyTid, *) mod_name, " MaxLengthRcv==", MaxLengthRcv, Ndomv, MyTid
         END IF

         ALLOCATE(SndData(MaxLengthSnd, Ndomv ), STAT = istat )
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot allocate SndData ", istat
            CALL stop_run()
         END IF
         ALLOCATE(RcvData(MaxLengthRcv, Ndomv ), STAT = istat )
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot allocate RcvData ", istat
            CALL stop_run()
         END IF
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "snddata", Size(SndData))
            CALL alloc_allocate(mod_name, "rcvdata", Size(RcvData))
         END IF
         Com%Request = mpi_request_null

    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)
            FromId =  Com%MapDom(Domv)
            MsgLength  = Com%NPrcv2(iv)  !-- Vertex centered !!!
            CALL mpi_irecv( RcvData(1, iv), MsgLength, type_real, FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO

    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv, 1, -1
            Domv  =  Com%Jdomv(iv)
            DestId =  Com%MapDom(Domv)
            MsgLength = Com%NPsnd2(iv) !-- Vertex centered !!!
            DO  j = 1, MsgLength
               is             = Com%PTsnd2(j, iv)
               IF (is > Size(Vec)) THEN
                  PRINT *, mod_name, " : ERREUR : iv=", iv, "j=", j, "is=", is, "size(vec)=", Size(Vec)
                  CALL stop_run()
               END IF
               SndData(j, iv) = Vec(is)
            END DO
            CALL mpi_isend(SndData(1, iv), MsgLength, type_real, DestId, MsgTag, FluidBox_Comm, Com%Request(Ndomv + iv), StatInfo)
         END DO

         CALL mpi_waitall(2 * Ndomv, Com%Request, Com%Stat, StatInfo)

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
!          CALL MPI_WAIT(Com%Request(iv), Status,  StatInfo)
                  DO  j = 1, Com%NPrcv2(iv)
                     is             = Com%PTrcv2(j, iv)
                     Vec(is)        = RcvData(j, iv)
                  END DO
               END DO
         END SELECT

         DEALLOCATE(SndData, STAT = istat)
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot deallocate SndData ", istat
            CALL stop_run()
         END IF
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "snddata")
         END IF
         DEALLOCATE(RcvData, STAT = istat)
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot deallocate SndData ", istat
            CALL stop_run()
         END IF
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "rcvdata")
         END IF
    !****************************************
         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
    !****************************************

      END SUBROUTINE EchangeVec



    !       -------------------------------------------------
    !!--      Role: Synchronise tous les noeuds du standard central
    !       -----------------------------------------------

      SUBROUTINE WaitGroup(Com)
         CHARACTER(LEN=*), PARAMETER :: mod_name = "WaitGroup"
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         REAL                        :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

         CALL mpi_barrier(FluidBox_Comm, StatInfo )

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_BARRIER: ', StatInfo
            CALL stop_run()
         END IF
    !****************************************
         Temps_final  = CpuTime()
         Com%Twait    = Com%Twait + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE WaitGroup


    !       -------------------------------------------------
    !!--      Role:
    !!--                   Synchronise tous les noeuds
    !!--                   du standard central et le Desactive
    !       -----------------------------------------------
      SUBROUTINE EndCom(Com)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "EndCom"
         TYPE(MeshCom), INTENT(INOUT)   :: Com

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF

         CALL mpi_barrier(FluidBox_Comm, StatInfo )

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_BARRIER: ', StatInfo
            CALL stop_run()
         END IF

         DEALLOCATE(Com%SndData, Com%RcvData)
         DEALLOCATE(Com%Stat, Com%Request)
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "com%snddata")
            CALL alloc_deallocate(mod_name, "com%rcvdata")
            CALL alloc_deallocate(mod_name, "com%stat")
            CALL alloc_deallocate(mod_name, "com%request")
         END IF

         CALL mpi_finalize(StatInfo)
         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_FINALIZE: ', StatInfo
            CALL stop_run()
         END IF
      END SUBROUTINE EndCom

      FUNCTION CpuTime() RESULT(time)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "CpuTime"
    !
    !       -------------------------------------------------
    !!      Role: remplacer la fonction intrins�que Cputime par la fonction MPI
    !!
    !       -----------------------------------------------
    !!@author      Boniface Nkonga      Universite de Bordeaux 1
    !       -----------------------------------------------

         REAL :: time

    !    REAL, EXTERNAL    :: second, tsecnd
    !
    !  CpuTime = SECOND( )
    ! CpuTime = TSECND( )

         time = mpi_wtime()

      END FUNCTION CpuTime

   END MODULE LibComm

