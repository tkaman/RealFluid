MODULE PETSc_driver

#ifdef PETSC

   USE Lestypes
  
   IMPLICIT NONE

#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscvec.h90"
#include "finclude/petscmat.h"
#include "finclude/petscksp.h"
#include "finclude/petscpc.h"

CONTAINS

   !-------------------------------------------
   SUBROUTINE init_PETSc(N_dof, Mat_fb, A, ksp)
   !-------------------------------------------

   IMPLICIT NONE
   
      INTEGER,       INTENT(IN) :: N_dof
      TYPE(Matrice), INTENT(IN) :: Mat_fb

      Mat, INTENT(OUT) :: A
      KSP, INTENT(OUT) :: ksp
      
      !==========================================
      PetscInt, DIMENSION(:), ALLOCATABLE :: N_nz

      PetscInt :: Nvar, N_syst
      PC :: pc
      PetscInt :: max_res, max_ite
      PetscReal :: rtol, atol, dtol
      PetscBool     :: flg
      PetscErrorCode :: ierr

      INTEGER :: i, id_s, id_e
      !==========================================

      Nvar = Mat_fb%blksize

      N_syst = Nvar*N_dof

      CALL PetscInitialize(PETSC_NULL_CHARACTER, ierr)
      
!!$   CALL MPI_Comm_size(PETSC_COMM_WORLD, size, ierr)

      !-------------------------
      ! INIZIALIZATION of MATRIX
      !-----------------------------------------------
      CALL PetscOptionsGetInt(PETSC_NULL_CHARACTER, '-n', N_syst, flg, ierr)

      ! Number of NonZero elements in each row
      ALLOCATE( N_nz(N_syst) )

      DO i = 1, N_dof

         id_s = (i - 1)*Nvar + 1

         id_e = id_s + (Nvar - 1)

         N_nz(id_s : id_e) = Mat_fb%JPosi(i+1) - Mat_fb%JPosi(i)

         N_nz(id_s : id_e) = N_nz(id_s : id_e)*Nvar

      ENDDO

      ! Preallocation of matrix memory
      CALL MatCreateSeqAIJ(PETSC_COMM_SELF, N_syst,N_syst, &
                           PETSC_NULL_INTEGER, N_nz, A, ierr)

      DEALLOCATE( N_nz )
      
      !---------------------------------
      ! CREATION of KSP and PC enviroment
      !-----------------------------------------------
      CALL KSPCreate(PETSC_COMM_WORLD, ksp, ierr)
		
      CALL KSPSetType(ksp, KSPGMRES, ierr)
      max_res = 60 !***
      CALL KSPGMRESSetRestart(ksp, max_res, ierr)

      rtol    = 1.d-5  !***
      atol    = 1.d-15 !***
      dtol    = 1.d5
      max_ite = 240

      CALL KSPSetTolerances(ksp, rtol, atol, dtol, max_ite, ierr)

      CALL KSPGetPC(ksp, pc, ierr)
      CALL PCSetType(pc, PCILU, ierr)
      CALL PCFactorSetReuseOrdering(pc, PETSC_TRUE)
      CALL PCFactorSetReuseFill(pc, PETSC_TRUE)
      CALL PCFactorSetLevels(pc, 3)

      write(*,*) '*** PETSc Initialized ***'
      CALL KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD,ierr)

   END SUBROUTINE init_PETSc
   !------------------------


   !------------------------------------------
   SUBROUTINE solve_PETSc(Var, Mat_fb, A, ksp)
   !------------------------------------------

   IMPLICIT NONE

      TYPE(Variables), INTENT(INOUT) :: Var
      TYPE(Matrice),   INTENT(IN)    :: Mat_fb
      Mat,             INTENT(INOUT) :: A
      KSP,             INTENT(INOUT) :: ksp
      !====================================================
      INTEGER, DIMENSION(:), ALLOCATABLE :: Id_col, col_dof
      
      Vec :: sol, rhs

      PetscScalar, DIMENSION(:,:), ALLOCATABLE :: M_val
      PetscInt,    DIMENSION(:),   ALLOCATABLE :: col_ext
      
      PetscScalar, DIMENSION(:), ALLOCATABLE :: v_val
      PetscInt,    DIMENSION(:), ALLOCATABLE :: idx
      
      PetscInt :: Nvar, row_p, nnz

      PetscInt, PARAMETER :: one = 1
      
      PetscScalar, POINTER ::  sol_v(:)

      PetscInt       :: ite
      PetscBool      :: flg
      PetscErrorCode :: ierr
      
      INTEGER :: t0, t1

      INTEGER :: i, i_dof, j, k, row, &
                 col_s, col_e, id_e, id_s, &
                 N_syst, N_dof, nnz_dof
      !====================================================
      CALL My_Timer(t0)
      
      ! Number of dof (mesh + upgrade_order)
      N_dof = Var%Ncells

      Nvar = Mat_fb%blksize

      N_syst = Nvar*N_dof

      ALLOCATE (v_val(Nvar), idx(Nvar))

      !-----------------------------------
      ! INIZIALIZATION of RHS and SOLUTION
      !----------------------------------------------
      CALL VecCreate(PETSC_COMM_WORLD, rhs, ierr)
      CALL VecSetSizes(rhs, PETSC_DECIDE, N_syst, ierr)
      CALL VecSetFromOptions(rhs, ierr)

      CALL VecCreate(PETSC_COMM_WORLD, sol, ierr)
      CALL VecSetSizes(sol, PETSC_DECIDE, N_syst, ierr)
      CALL VecSetFromOptions(sol, ierr)
      
      !------------------------------
      ! COSTRUCTION of MATRIX and RHS
      !-----------------------------------------------
      ! Each sub-block [1:Nvar] of the matrix and the
      ! rhs are extend. The subscript dof means no extented.
      
      ! Keep the same pattern of the matrix and reset the coeff.
      IF(Var%kt > 1) CALL MatZeroEntries(A)
      
      DO i_dof = 1, N_dof
         
         nnz_dof = Mat_fb%JPosi(i_dof+1) - Mat_fb%JPosi(i_dof)

         nnz = nnz_dof*Nvar

         row = (i_dof - 1)*Nvar + 1

         ALLOCATE( Id_col(nnz_dof), col_dof(nnz_dof) )
         
         ALLOCATE( col_ext(nnz), M_val(Nvar, nnz) )
         
         Id_col = (/ (i, i = Mat_fb%Jposi(i_dof), &
                             Mat_fb%Jposi(i_dof+1)-1) /)

         col_dof =  Mat_fb%Jvcell(Id_col)

         ! Construct the row matrix from the Mat structure
         DO j = 1, SIZE(Id_col)

            col_s = (col_dof(j)-1)*Nvar + 1

            col_e = col_s + (Nvar-1)

            id_s = (j - 1)*Nvar + 1
            id_e = id_s + (Nvar-1)

            col_ext(id_s:id_e) = (/ (i, i = col_s, col_e) /)

            M_val(:, id_s:id_e) = Mat_fb%Vals( :, :, Id_col(j) )

         ENDDO

                           ! Zero-start indexing
         col_ext = col_ext - 1 

         ! --- Insertion of each extented row into the matrix ---
         DO k = 1, Nvar

                                  ! Zero-start indexing
            row_p = (row + (k-1)) - 1

            CALL MatSetValues(A, one,row_p, nnz,col_ext, &
                              M_val(k,:), INSERT_VALUES, ierr)

         ENDDO

         DEALLOCATE( Id_col, col_dof, col_ext, M_val )
         
         ! --- Insertion of each column vector in the rhs ---
         idx = (/ (i, i = row, row+(Nvar-1)) /)
         
                   ! Zero-start indexing
         idx = idx - 1
         
         v_val = -Var%Flux(:, i_dof)
         
         CALL VecSetValues(rhs, Nvar, idx, v_val, INSERT_VALUES, ierr)

      ENDDO

      CALL MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr)
      CALL MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr)

      ! Check if the pattern of the matrix has been changed
      ! and return error if so
      CALL MatSetOption(A, MAT_NEW_NONZERO_LOCATION_ERR, flg, ierr) 

      CALL VecAssemblyBegin(rhs, ierr)
      CALL VecAssemblyEnd(rhs, ierr)

      !------------------------------
      ! SOLUTION of the LINEAR SYSTEM
      !-------------------------------------------------
      CALL KSPSetOperators(ksp, A, A, DIFFERENT_NONZERO_PATTERN, ierr) !???

      ! Use the previous solution as initial guess
      IF(Var%kt > 1) CALL KSPSetInitialGuessNonzero(ksp, flg, ierr)

      CALL KSPSetFromOptions(ksp,ierr)
      CALL KSPSetUp(ksp,ierr)

      ! Solve the linear system
      CALL KSPSolve(ksp, rhs, sol, ierr)

      IF (ierr /= 0) THEN
         WRITE(*,*) 'Catastrophic error: PETSc failed to solve the linear system'
         WRITE(*,*) 'STOP!'
         STOP
      ENDIF

      ! Repacking the solution and save it in var%Flux
      CALL VecGetArrayF90(sol, sol_v, ierr)
      DO i_dof = 1, N_dof

         id_s = (i_dof - 1)*Nvar + 1

         id_e = id_s + (Nvar-1)

         var%Flux(:, i_dof) = sol_v(id_s:id_e)
         
      ENDDO
      CALL VecRestoreArrayF90(sol, sol_v, ierr)

      CALL KSPGetIterationNumber(ksp, ite)
      WRITE(*,*) '++++++++ ITE', ite, '++++++++'

      CALL VecDestroy(rhs, ierr)
      CALL VecDestroy(sol, ierr)
      
      CALL My_Timer(t1)
      
      Var%TotalSolveTime = Var%TotalSolveTime + t1 - t0
      
   END SUBROUTINE solve_PETSc
   !-------------------------

#endif //-PETSC

END MODULE PETSc_driver
