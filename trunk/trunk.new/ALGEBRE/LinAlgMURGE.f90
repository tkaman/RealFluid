!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *!
!*                                                                           *!
!*                  This file is part of the FluidBox program                *!
!*                                                                           *!
!* Copyright (C) 2008-2009 P. JACQ                                           *!
!*                                                                           *!
!*                  2008-2009 INRIA Bordeaux / Sud-Ouest                     *!
!*                                                                           *!
!*  FluidBox is distributed under the terms of the Cecill-B License.         *!
!*                                                                           *!
!*  You can find a copy of the Cecill-B License at :                         *!
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 *!
!*                                                                           *!
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *!
!
! This file implements an interface to Murge compliant solvers.
! 
!

#define FBXDEBUG PRINT *, mod_name, 

#ifdef HIPS
#define ADD_OVER HIPS_ASSEMBLY_OVW
#else
#define ADD_OVER MURGE_ASSEMBLY_OVW
#endif

  MODULE LinAlg
    !
    ! Modules utilis�s
    ! ----------------
    USE Lestypes
    USE LibComm
    USE ReadMesh
    IMPLICIT NONE
    INCLUDE "murge.inc"
#ifdef HIPS
    INCLUDE "hips.inc"
#endif
    
    INTEGER , SAVE :: Precond = 0
  CONTAINS

    ! Subroutine: Murge_Init
    !
    ! Initialize MURGE
    !
    ! Parameters: Com
    !
    SUBROUTINE Murge_Init(Com)
      CHARACTER(LEN = *), PARAMETER     :: mod_name = "InitMurge"
      TYPE(MeshCom), INTENT( IN OUT)    :: Com
      INTEGER                           :: id
      INTEGER                           :: method, itmax, restart , verbose, domsize
      REAL(KIND=8)                      :: PRECISION , PrecondTol
      LOGICAL                           :: existe
      INTEGER                           :: ierr
      INTEGER(kind=MURGE_INTS_KIND)     :: ierror
      INTEGER(kind=MURGE_INTS_KIND)     :: solver
      INTEGER(kind=MURGE_INTS_KIND)     :: thread_number

      CALL MURGE_INITIALIZE(1, ierr)
      id = 0
      
      INQUIRE(FILE = "MURGE.data", EXIST = existe)

      IF (.NOT. existe) THEN
         FBXDEBUG "Fichier MURGE.data introuvable"
         CALL delegate_stop()
      ENDIF

      ! Read some data for MURGE's method
      OPEN(unit=37,FILE="MURGE.data")
      ! Method : 1 ITERATIVE , 2 : HYBRID or EXACT
      READ(37,*) method
      ! Verbose Level
      READ(37,*) verbose
      ! Precision asked for ITERATIVE or HYBRID method
      READ(37,*) PRECISION
      ! Maximum Number of iterations and Restart for ITERATIVE Method (GMRES)
      READ(37,*) itmax, restart

      ! HIPS Params
      ! Parameter for HYBRID Method
      READ(37,*) domsize
      ! Number of iteration between 2 Preconditioning
      READ(37,*) Precond
      ! Precision of preconditionning (HIPS)
      READ(37,*) PrecondTol

      ! PaStiX Params
      ! Number of Thread used by the solver
      READ(37,*) thread_number

      CLOSE(37)
 
      CALL MURGE_GetSolver(solver, ierror)

      ! Set Options
      IF (solver == MURGE_SOLVER_PASTIX) THEN
         CALL MURGE_SetDefaultOptions(id, 0, ierror)
         ! IPARM parameters needs PaStiX murge.inc 
#ifdef PASTIX_MURGE 
         IF (method == 1) THEN
            CALL MURGE_SetOptionINT(id , IPARM_ONLY_RAFF          , API_YES,    ierror)
         END IF
         CALL MURGE_SetOptionINT(id, IPARM_ITERMAX,             itmax,          ierror)
         CALL MURGE_SetOptionINT(id, IPARM_GMRES_IM,            restart,        ierror)
         CALL MURGE_SetOptionINT(id, IPARM_VERBOSE,             verbose,        ierror)
         CALL MURGE_SetOptionINT(id, IPARM_MATRIX_VERIFICATION, API_NO,         ierror)
         CALL MURGE_SetOptionINT(id, IPARM_THREAD_NBR,          thread_number,  ierror)
#else
         FBXDEBUG "Bad Murge library (linked with PaStiX, but without -DPATIX_MURGE)"
         CALL Delegate_stop()
#endif
      ELSE IF (solver == MURGE_SOLVER_HIPS) THEN
#ifdef HIPS
         IF (method == 1) THEN
            CALL MURGE_SetDefaultOptions(id, HIPS_ITERATIVE, ierror)
         ELSE
            CALL MURGE_SetDefaultOptions(id, HIPS_HYBRID, ierror )
            CALL MURGE_SetOptionINT(id, HIPS_PARTITION_TYPE, 0, ierror)
            CALL MURGE_SetOptionINT(id, HIPS_DOMSIZE, domsize, ierror)
         END IF
         CALL MURGE_SetOptionINT(id, HIPS_SYMMETRIC, 0, ierror)
         CALL MURGE_SetOptionINT(id, HIPS_LOCALLY, 0, ierror)
         CALL MURGE_SetOptionINT(id, HIPS_ITMAX, itmax, ierror)
         CALL MURGE_SetOptionINT(id, HIPS_KRYLOV_RESTART, restart, ierror)
         CALL MURGE_SetOptionINT(id, HIPS_VERBOSE, verbose, ierror)
         CALL MURGE_SetOptionINT(id, HIPS_DOMNBR, Com%NTasks, ierror)
         CALL MURGE_SetOptionINT(id, HIPS_CHECK_GRAPH, 1, ierror)
         CALL MURGE_SetOptionINT(id, HIPS_CHECK_MATRIX, 1, ierror)

         CALL MURGE_SetOptionREAL(id, HIPS_PREC, Precision, ierror)

         ! During the preconditioning stage if a value is less than "PrecondTol" then it's ignored
         CALL MURGE_SetOptionREAL(id, HIPS_DROPTOL0, PrecondTol, ierror)
         CALL MURGE_SetOptionREAL(id, HIPS_DROPTOL1, PrecondTol, ierror)
         CALL MURGE_SetOptionREAL(id, HIPS_DROPTOLE, PrecondTol, ierror)
#else
         FBXDEBUG "Bad Murge library (linked with HIPS, but without -DHIPS)"
         CALL Delegate_stop()
#endif
      ELSE
         
         FBXDEBUG "Murge solver not available", "(", solver, ")", MURGE_SOLVER_HIPS, MURGE_SOLVER_PASTIX
         CALL Delegate_stop()
      ENDIF

#ifndef HIPS
      CALL MURGE_SetOptionINT(id, MURGE_IPARAM_SYM    , MURGE_BOOLEAN_FALSE, ierror)
      CALL MURGE_SetOptionINT(id, MURGE_IPARAM_BASEVAL, 1, ierror)
      CALL MURGE_SetOptionREAL(id, MURGE_RPARAM_EPSILON_ERROR, PRECISION, ierror)
#endif
    END SUBROUTINE Murge_Init

    ! Subroutine: MURGE_SetGraph
    !
    ! Send FluidBox's matrix graph to MURGE
    !
    ! Parameters:
    !   Com  - Fluidbox communication structure.
    !   Mat  - FluidBox's matrix
    !   Mesh - FluidBox's mesh
    !
    SUBROUTINE MURGE_SetGraph(Com,Mat,Mesh)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "MURGE_SetGraph"
      ! Type et vocations des Variables d'appel
      !----------------------------------------
      TYPE(MeshCom), INTENT( INOUT ) :: Com
      TYPE(Matrice), INTENT( IN )    :: Mat
      TYPE(MeshDef), INTENT( IN )    :: Mesh

      INTEGER , DIMENSION( 2 ) :: infos
      INTEGER :: id ,i 
      INTEGER(kind=MURGE_INTS_KIND) :: ierr

      id = 0

      ! The Global Graph is only known on proc 0
      IF (Com%Me == 0) THEN
#ifdef PASTIX_MURGE
         infos = (/ Mesh%Npoint, 2*Mesh%Nsegmt +Mesh%Npoint /)
#else
         infos = (/ Mesh%Npoint, Mesh%Nsegmt /)
#endif
      ELSE
         infos = (/ Mesh%Npoint, 0 /)
      END IF

#ifdef HIPS
      CALL MURGE_SetOptionINT(id, HIPS_DOF, Mat%Blksize, ierr)
#else
      CALL MURGE_SetOptionINT(id, MURGE_IPARAM_DOF, Mat%Blksize, ierr)
#endif
      ! Gather the graph size data
      
      ! Start sending the graph to MURGE
      PRINT * , infos(1) , infos(2)
      CALL MURGE_GRAPHBEGIN(id, infos(1) , infos(2) , ierr)
      IF (Com%Me == 0) THEN
         DO i = 1,  Mesh%Nsegmt
            CALL MURGE_GRAPHEDGE(id, Mesh%Nubo(1,i), Mesh%Nubo(2,i), ierr)
#ifdef PASTIX_MURGE
            ! Graph is symmetrized for a non symmetric matrix
            CALL MURGE_GRAPHEDGE(id, Mesh%Nubo(2,i), Mesh%Nubo(1,i), ierr)
         END DO
         DO i = 1,  Mesh%Npoint
            CALL MURGE_GRAPHEDGE(id, i, i, ierr)
#endif
         END DO      
      END IF
      ! Graph sent
      CALL MURGE_GRAPHEND(id, ierr)
    
    END SUBROUTINE MURGE_SetGraph

    ! Subroutine: MURGE_GetNodesAndRemesh
    !
    ! Get MURGE's mapping and rebuild the local mesh
    !
    ! Parameters:
    !   Com  - Fluidbox communication structure.
    !   Mesh - FluidBox's mesh
    !   Mat  - FluidBox's matrix
    !   Var  - ...
    !   Data - ...
    !
    SUBROUTINE MURGE_GetNodesAndRemesh(Com,Mesh,Mat,Var,DATA)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "MURGE_GetNodesAndRemesh"
      TYPE(MeshDef)  , INTENT( INOUT ) :: Mesh
      TYPE(Variables), INTENT( INOUT ) :: Var
      TYPE(Donnees)  , INTENT( INOUT ) :: DATA
      TYPE(MeshCom)  , INTENT( INOUT ) :: Com
      TYPE(Matrice)  , INTENT( OUT )   :: Mat
      INTEGER :: id , totalnode , NbNodes

      INTEGER :: Len,Len2
      INTEGER :: ierr

      NbNodes = Mesh%Npoint

      IF (Com%Me == 0) THEN
         ALLOCATE( Var%GlobalVp(Data%NvarPhy , NbNodes) )
      END IF
      id = 0
      ! Get Local nodes
      CALL MURGE_GETLOCALNODENBR(id, totalnode, ierr)
      ALLOCATE(Mat%nodelist(totalnode))        

      CALL MURGE_GETLOCALNODELIST(id, Mat%nodelist, ierr)

      ALLOCATE( Mat%mapnodelist(totalnode) )        
      Mat%mapnodelist = Mat%nodelist
      Mesh%SizeOfFlux = totalnode * Data%NVar

      ! Remesh
      CALL DonMesh(Com, Mesh, DATA, Mat%nodelist)

      ! Changing name
      data%PbName(1: ) = TRIM(data%rootname) // "-"
      Len = INDEX(data%PbName, " ")
      IF (com%Ntasks == 1) THEN
         WRITE(data%PbName(Len: Len + 2), "(i3)")0
      ELSE
         WRITE(data%PbName(Len: Len + 2), "(i3)") com%Me+ 1
      END IF
      data%PbName(Len: Len + 2) = ADJUSTL(data%PbName(Len: Len + 2))
      len2 = INDEX(data%PbName(Len: Len + 2), " ") -1
      data%PbName(Len: Len + 2) = REPEAT("0", 3 - len2) // TRIM(data%PbName(Len: Len + len2))


      ! Fill the communication structures
      Mat%Nnin  = totalnode
      Mat%NMurge = totalnode
      CALL CalculeCom(Com, DATA, Mesh, Mat%nodelist, NbNodes)
      Mesh%nodelist => Mat%nodelist
 
      CALL PrepGather2Visu(Com, DATA, Mat%nodelist ,  totalnode)
      Mat%NMurgeNzeros = -1

    END SUBROUTINE MURGE_GetNodesAndRemesh

    ! Subroutine: MurgeMatrixNnzeros
    !
    ! Count the number of non-Zeros in FluidBox's Matrix
    !
    ! Parameters: 
    !   Mat - Fluidbox's matrix
    !
    SUBROUTINE MurgeMatrixNnzeros(Mat)
      TYPE(Matrice), INTENT(INOUT)   :: Mat

      INTEGER :: i, j , n
      INTEGER :: deb, fin

      n = 0
      DO i = 1, Mat%NMurge
         deb = Mat%Jposi(i)
         fin = Mat%Jposi(i+1)-1           
         DO j = deb, fin
            IF (Mat%JvCell(j) <= Mat%NMurge) THEN
               n = n + 1
            END IF
         END DO
      END DO

      Mat%NMurgeNzeros = n * Mat%blksize**2
    END SUBROUTINE MurgeMatrixNnzeros

    ! Subroutine: MatrixFluidBox2Murge
    !
    ! Send FluidBox's Matrix to MURGE
    !
    ! Parameters: 
    !   Mat - Fluidbox's matrix
    !
    SUBROUTINE MatrixFluidBox2Murge(Mat)
      TYPE(Matrice), INTENT(INOUT)   :: Mat

      INTEGER :: dof
      INTEGER :: i, j, k
      INTEGER :: deb, fin
      INTEGER :: id , ierr

      REAL , DIMENSION(Mat%blksize**2) :: vals

      dof = Mat%blksize
      id = 0
      IF (Mat%NMurgeNzeros < 0) THEN
         CALL MurgeMatrixNnzeros(Mat)
      END IF

      
      CALL MURGE_MatrixReset(id, ierr)
#ifndef HIPS
      CALL MURGE_ASSEMBLYBEGIN(id, Mat%NMurgeNzeros, ADD_OVER , ADD_OVER, MURGE_ASSEMBLY_FOOL, 0, ierr)
#else
      CALL MURGE_ASSEMBLYBEGIN(id, Mat%NMurgeNzeros, ADD_OVER , ADD_OVER, MURGE_ASSEMBLY_RESPECT, 0, ierr)
#endif
      DO i = 1, Mat%NMurge
         deb = Mat%Jposi(i)
         fin = Mat%Jposi(i+1)-1           
         DO j = deb, fin
            IF (Mat%JvCell(j) <= Mat%NMurge) THEN
               DO k = 1, dof   
                  vals(k::dof) = Mat%Vals(k,1:dof,j)
               END DO
               CALL MURGE_ASSEMBLYSETNODEVALUES(id, Mat%Nodelist(i), Mat%Nodelist(Mat%JvCell(j)),  vals , ierr)
            END IF
         END DO
      END DO
      CALL MURGE_ASSEMBLYEND(id, ierr)

    END SUBROUTINE MatrixFluidBox2Murge



    ! Subroutine: SolveSystem
    !
    ! Solves a linear system using Murge compliant solvers.
    !
    ! Parameters
    !   Com  - Fluidbox communication structure.
    !   DATA - ...
    !   Var  - ...
    !   Mat  - Fluidbox's matrix.
    !
    SUBROUTINE SolveSystem(Com, DATA, Var, Mat)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "SolveSystem"
      ! Type et vocations des Variables d'appel
      !----------------------------------------
      TYPE(MeshCom), INTENT( IN OUT)   :: Com
      TYPE(Variables), INTENT( IN OUT)   :: Var
      TYPE(Donnees), INTENT(IN)      :: DATA
      TYPE(Matrice), INTENT(INOUT)   :: Mat

      ! Variables locales 
      !------------------
      REAL , DIMENSION(Mesh%SizeOfFlux) :: rhs

      ! Variables locales scalaires
      !----------------------------
      INTEGER :: i,j,id
      INTEGER :: ierr
      INTEGER :: fin

      id = 0
      rhs = 0.

#ifdef HIPS
      ! With HIPS we can do the preconditionning step once and keep the preconditionner during the next iterations
      ! A better technique will be to launch the preconditionning step if the number of GMRES iteration is too big
      ! or if the precision required is not obtained
      IF ( Precond > 1 ) THEN
         IF ( MOD(Var%kt, Precond ) /= 0) THEN
            CALL MURGE_SetOptionINT(id, HIPS_DISABLE_PRECOND, 1, ierr)
         ELSE
            CALL MURGE_SetOptionINT(id, HIPS_DISABLE_PRECOND, 0, ierr)
         END IF
      END IF
#endif


      ! Map FBx's rhs to MURGE's numerotation
      j = 1
      fin = size(Mat%nodelist)
      DO i = 1, fin
         rhs(j:j + Data%Nvar-1) = - Var%Flux(:,i)
         j = j + Data%Nvar
      END DO

      ! Send FluidBox's matrix to MURGE
      CALL MatrixFluidBox2Murge(Mat)

      ! Send the RHS to MURGE     
      CALL MURGE_SETLOCALRHS(id, rhs, ADD_OVER, ADD_OVER, ierr)

      ! Solve and get the solution
      CALL MURGE_GETLOCALSOLUTION(id, rhs, ierr)

      ! Map the solution to FBx numerotation
      j = 1
      DO i = 1, fin
         Var%Flux(:,i) = rhs(j:j + Data%Nvar-1)
         j = j + Data%Nvar
      END DO

      CALL EchangeSol( Com, Var%Flux)

    END SUBROUTINE SolveSystem





    ! Subroutine: cleanMurge
    !
    ! Clean it up
    !
    SUBROUTINE CleanMurge(com, Mat, Mesh)
      CHARACTER(LEN = *), PARAMETER    :: mod_name = "CleanMURGE"
      TYPE(MeshCom), INTENT(INOUT)     :: Com
      TYPE(Matrice), INTENT(INOUT)     :: Mat
      TYPE(MeshDef), INTENT(INOUT)     :: Mesh
      INTEGER :: id, ierr
      id = 0
      FBXDEBUG "Byebye MURGE"
      DEALLOCATE(Mat%nodelist)
      DEALLOCATE(Mat%mapnodelist)
      DEALLOCATE(Mesh%overlap)
      CALL MURGE_CLEAN(id, ierr)
      CALL MURGE_FINALIZE(ierr)
      CALL CleanCom(com)
    END SUBROUTINE CleanMurge

    !> Subroutine: statistiques_linalg
    SUBROUTINE statistiques_linalg()
      CHARACTER(LEN = *), PARAMETER :: mod_name = "statistiques_linalg"

      FBXDEBUG "C'est vraiment des super Stats!"
    END SUBROUTINE statistiques_linalg

  END MODULE LinAlg


