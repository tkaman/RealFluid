!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                          */
!                2002-2010 R. Abgrall                                        */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                  2002-2010 INRIA                                          */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MODULE PLinAlg
  USE LesTypes
  IMPLICIT NONE
  INTERFACE  Inverse
     MODULE PROCEDURE InverseGauss
  END INTERFACE

  LOGICAL, SAVE, PRIVATE :: initialise = .FALSE.
  LOGICAL, SAVE, PRIVATE :: module_debug = .FALSE.

CONTAINS

  !------------------------------------------
  !!--  Inversion d'une matrice carree par LU et pivot partiel par ligne
  !!--  Ref : Numerical recipes in C
  !   R. Abgrall
  !------------------------------------------
  FUNCTION InverseLu( Mat)  RESULT (Mat1)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "InverseLu"
    REAL, DIMENSION(:, : ), INTENT(IN) :: Mat
    REAL, DIMENSION(SIZE(Mat, dim = 1), SIZE( Mat, dim = 1)) :: Mat1, mat2
    REAL, DIMENSION(SIZE(Mat, dim = 1))   :: col
    REAL                        :: d
    INTEGER    :: j, Nsize, l, zz
    INTEGER, DIMENSION(SIZE(Mat, dim = 1)) :: indx !-- CCE R.B. 2008/10/21

#ifdef DEBUG_PALG
    IF (SIZE(Mat, dim = 1) /= SIZE(Mat, dim = 2) ) THEN
       PRINT *, mod_name, " ERREUR : not a square matrix."
       CALL delegate_stop()
    END IF
#endif

    IF (.NOT. initialise) THEN
       optim1_plain = .FALSE.
       optim2_plain = .TRUE.
       optim3_plain = .TRUE. !-- celle l� est particuli�rement efficace
       optim4_plain = .TRUE.

       optim2_plain = .FALSE. !-- ces 2 optimisations l� mettre MHD en l'air, et pourtant elles sont meilleures en pr�cusion, ce qui ne saute pas aux yeux au niveau des r�sidus %%%%%%
       optim4_plain = .FALSE.

       optim3_plain = .FALSE. !-- cette optimisation met MHD en l'air uniquement en -O5, pas en -O2
    END IF

    Nsize = SIZE(Mat, dim=1)
    mat2  = Mat

    IF (module_debug) THEN
       DO l = 1, SIZE(mat, 2)
          DO zz = 1, SIZE(mat, 1)
             PRINT fmt_prec2, mod_name, "Mat ", 10000 * zz + l, " ", mat(zz, l)
          END DO
       END DO
    END IF

    CALL ludcmp(mat2, Nsize, indx, d) !-- indx OUT

    IF (module_debug) THEN
       DO l = 1, SIZE(mat2, 2)
          DO zz = 1, SIZE(mat2, 1)
             PRINT fmt_prec2, mod_name, "mat2 ", 10000 * zz + l, " ", mat2(zz, l)
          END DO
       END DO
    END IF

    DO j = 1, Nsize
       col = 0.0
       col(j) = 1.0
       CALL luksb(mat2, Nsize, indx, col) !-- col IN OUT; indx IN
       Mat1(:, j) = col(: )
    END DO

    IF (module_debug) THEN
       DO l = 1, SIZE(mat1, 2)
          DO zz = 1, SIZE(mat1, 1)
             PRINT fmt_prec2, mod_name, "mat1 ", 10000 * zz + l, " ", mat1(zz, l)
          END DO
       END DO
    END IF

  CONTAINS
    SUBROUTINE ludcmp(mat3, Nsize, indx, d)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "ludcmp"

      INTEGER, INTENT(IN) :: Nsize
      REAL, DIMENSION(Nsize, Nsize ), INTENT(INOUT) :: mat3 !-- Passer en :,: pose des pbs avec le -O5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      INTEGER, DIMENSION(Nsize ), INTENT(OUT) :: indx !-- Passer en :,: pose des pbs avec le -O5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      REAL, INTENT(OUT) :: d

      REAL, DIMENSION(Nsize) :: vv
      INTEGER :: i, j, k, imax, zz
      REAL :: Big, dum, somme, temp

      d = 1.0
      DO i = 1, Nsize
         IF (optim1_plain) THEN
            Big = MAXVAL(ABS(mat3(i, :)))
         ELSE
            Big = 0.0
            DO j = 1, Nsize
               temp = ABS(mat3(i, j))
               IF (temp > Big) THEN
                  Big = temp
               END IF
            END DO
         END IF
         IF (Big == 0.0) THEN
            WRITE( *, *) mod_name, " ERREUR : Matrice singuliere, i==", i
            CALL delegate_stop()
         END IF
         vv(i) = 1.0 / Big
      END DO

      IF (module_debug) THEN
         DO zz = 1, SIZE(vv, 1)
            PRINT fmt_prec2, mod_name, "vv ", zz, " ", vv(zz)
         END DO
      END IF

      DO j = 1, Nsize

         DO i = 1, j -1
            IF (optim2_plain) THEN
               mat3(i, j) = mat3(i, j) - SUM(mat3(i, 1: i - 1) * mat3(1: i - 1, j))
            ELSE
               somme = mat3(i, j)
               DO k = 1, i -1
                  somme = somme - mat3(i, k) * mat3(k, j)
               END DO
               mat3(i, j) = somme
            END IF
         END DO !-- boucle sur i

         IF (module_debug) THEN
            DO zz = 1, j - 1
               PRINT fmt_prec2, mod_name, "mat3/a ", 10000 * zz + j, " ", mat3(zz, j)
            END DO
         END IF

         Big = 0.0
         imax = -1
         DO i = j, Nsize
            somme = mat3(i, j)
            IF (module_debug) THEN
               PRINT fmt_prec2, mod_name, "somme/a ", 10000 * i + j, " ", somme
            END IF
            DO k = 1, j -1
               somme = somme - mat3(i, k) * mat3(k, j)
               !                     IF (module_debug) THEN
               !                        PRINT fmt_prec2, mod_name, "somme/b ", k, " ", somme
               !                     END IF
            END DO
            IF (module_debug) THEN
               PRINT fmt_prec2, mod_name, "somme ", 10000 * i + j, " ", somme
            END IF
            mat3(i, j) = somme
            dum = vv(i) * ABS(somme)
            IF (dum >= Big) THEN
               Big = dum
               imax = i
            END IF
         END DO !-- boucle sur i

         IF (module_debug) THEN
            DO zz = j, Nsize
               PRINT fmt_prec2, mod_name, "mat3/b ", 10000 * zz + j, " ", mat3(zz, j)
            END DO
         END IF

         IF (imax < 0) THEN
            PRINT *, mod_name, " :: ERREUR : imax non initialise"
            CALL delegate_stop()
         END IF

         IF (j /= imax) THEN
            DO k = 1, Nsize
               dum = mat3(imax, k)
               mat3(imax, k) = mat3(j, k)
               mat3(j, k) = dum
            END DO !-- boucle sur k
            d = - d
            vv(imax ) = vv(j)
         END IF

         IF (module_debug) THEN
            DO zz = 1, Nsize
               PRINT fmt_prec2, mod_name, "mat3/c ", 10000 * zz + j, " ", mat3(zz, j)
            END DO
         END IF

         indx(j) = imax

         IF (ABS(mat3(j, j)) <= 1.0e-20) THEN
            mat3(j, j) = SIGN(1.0e-20, mat3(j, j)) !-- CCE 2007/04/24 !-- CCE 2008/12/17
         END IF

         IF (j /= Nsize) THEN
#ifdef OLD
            dum = 1.0 / mat3(j, j)
            DO i = j + 1, Nsize
               mat3(i, j) = mat3(i, j) * dum
            END DO !-- boucle sur i
#else
            DO i = j + 1, Nsize
               mat3(i, j) = mat3(i, j) / mat3(j, j)
            END DO !-- boucle sur i
#endif
         END IF

         IF (module_debug) THEN
            DO zz = j + 1, Nsize
               PRINT fmt_prec2, mod_name, "mat3/d ", 10000 * zz + j, " ", mat3(zz, j)
            END DO
         END IF

      END DO !-- boucle sur j

    END SUBROUTINE ludcmp


    SUBROUTINE luksb(mat2, Nsize, indx, col)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "luksb"

      INTEGER, INTENT(IN) :: Nsize
      REAL,  DIMENSION(Nsize, Nsize), INTENT(IN) :: mat2 !-- Passer en :,: pose des pbs avec le -O5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      INTEGER,  DIMENSION(Nsize), INTENT(IN) :: indx !-- Passer en :,: pose des pbs avec le -O5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      REAL, DIMENSION(Nsize), INTENT(INOUT) :: col !-- Passer en :,: pose des pbs avec le -O5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      INTEGER ::  i, ii, ip, j
      REAL  :: somme

      ii = 1
      DO i = 1, Nsize
         ip = indx(i)
         IF (ip <= 0 .OR. ip > SIZE(col)) THEN
            PRINT *, mod_name, " ERREUR : indx(", i, ") invalide", ip
            CALL delegate_stop()
         END IF
         somme = col(ip)
         col(ip) = col(i)
         IF (ii > 0) THEN
            IF (optim3_plain) THEN
               somme = somme - SUM(mat2(i, ii: i -1) * col(ii: i -1))
            ELSE
               DO j = ii, i -1
                  somme = somme - mat2(i, j) * col(j)
               END DO
            END IF
         ELSE
            IF (somme == 0.0) THEN
               ii = i
            END IF
         END IF

         col(i) = somme
      END DO

      DO i = Nsize, 1, -1
         IF (optim4_plain) THEN
            col(i) = (col(i) - SUM(mat2(i, i + 1: Nsize) * col(i + 1: Nsize))) / mat2(i, i)
         ELSE
            somme = col(i)
            DO j = i + 1, Nsize
               somme = somme - mat2(i, j) * col(j)
            END DO
            col(i) = somme / mat2(i, i)
         END IF
      END DO
    END SUBROUTINE  luksb

  END FUNCTION InverseLu

  !---------------------------------------------------------
  !!--   Inversion d'une matrice carr�e par la m�thode de Gauss
  !---------------------------------------------------------
  !!-- Algorithme de Gauss sans pivot. Appel� par precondiagonal (nvar x nvar)

  FUNCTION InverseGauss( Mat ) RESULT(Mat1)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "InverseGauss"
    REAL, DIMENSION(:, : ), INTENT(IN) :: Mat
    REAL, DIMENSION(SIZE(Mat, dim = 1), SIZE(Mat, dim = 2)) :: Mat1
    INTEGER    :: Nsize, k, i
    REAL       :: var

#ifdef DEBUG_PALG
    IF (SIZE(Mat, dim = 1) /= SIZE(Mat, dim = 2) ) THEN
       PRINT *, mod_name, " ERREUR : not a square matrix."
       CALL delegate_stop()
    END IF
#endif

    Nsize = SIZE(Mat, dim=1)
    Mat1  = Mat
    DO k = 1, Nsize
       IF (Mat1(k, k) == 0.0) THEN
          PRINT *, mod_name, " ERREUR : matrice singuliere, k ==", k, Mat1(k, k)
          PRINT *, mod_name
          CALL delegate_stop()
       END IF
       var       = 1.0 / Mat1(k, k)
       Mat1(k, k) = 1.0
       Mat1(k, : ) = Mat1(k, : ) * var
       DO i = 1, Nsize
          IF (i == k ) THEN
             CYCLE
          END IF
          var       = Mat1(i, k)
          Mat1(i, k) = 0.0
          Mat1(i, : ) = Mat1(i, : ) - var * Mat1(k, : )
       END DO
    END DO
  END FUNCTION InverseGauss

  !======================================
  FUNCTION Identity_Matrix(nn) RESULT(II)
  !======================================

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: nn
    
    REAL, DIMENSION(:,:), ALLOCATABLE :: II
    !--------------------------------------
    INTEGER :: k
    !--------------------------------------

    ALLOCATE( II(nn, nn) )

    II = 0.d0

    DO k = 1, nn
       II(k,k) = 1.d0
    ENDDO
    
  END FUNCTION Identity_Matrix
  !===========================

  !==================================
  FUNCTION inverse_easy(A) RESULT (B)
  !==================================

      IMPLICIT NONE
     
      REAL(KIND=8), DIMENSION(:,:),     INTENT(IN)  ::  A
      REAL(KIND=8), DIMENSION(SIZE(A,1),SIZE(A,2))  ::  B

      REAL(KIND=8)  ::  det


      IF ( SIZE(A,1) .NE. SIZE(A,2) ) THEN
         WRITE(*,*) ' Matrix A is not square.' 
         WRITE(*,*) ' in FUNCTION inverse, MODULE lin_algebra' 
         WRITE(*,*) ' STOP. ' 
         STOP
      ENDIF

      det = determinant(A)

      IF ( det == 0 ) THEN
         WRITE(*,*) ' Matrix A is singular.' 
         WRITE(*,*) ' in FUNCTION inverse, MODULE lin_algebra' 
         WRITE(*,*) ' STOP. ' 
         STOP
      ENDIF

      SELECT CASE ( SIZE(A,1) )

      CASE (2) 
      ! ------
         B(1,1) =   A(2,2);  B(1,2) = - A(1,2) 
         B(2,1) = - A(2,1);  B(2,2) =   A(1,1) 
         B = B/det

      CASE (3)
      ! ------
         B(1,1) =   A(2,2)*A(3,3) - A(3,2)*A(2,3)
         B(1,2) = - A(1,2)*A(3,3) + A(3,2)*A(1,3)
         B(1,3) =   A(1,2)*A(2,3) - A(2,2)*A(1,3)

         B(2,1) = - A(2,1)*A(3,3) + A(3,1)*A(2,3)
         B(2,2) =   A(1,1)*A(3,3) - A(3,1)*A(1,3)
         B(2,3) = - A(1,1)*A(2,3) + A(2,1)*A(1,3)

         B(3,1) =   A(2,1)*A(3,2) - A(3,1)*A(2,2)
         B(3,2) = - A(1,1)*A(3,2) + A(3,1)*A(1,2)
         B(3,3) =   A(1,1)*A(2,2) - A(2,1)*A(1,2)

         B = B/det

      CASE DEFAULT
         WRITE(*,*) ' Matrix of size ', SIZE(A,1), ' not implemented' 
         WRITE(*,*) ' in FUNCTION inverse, MODULE lin_algebra' 
         WRITE(*,*) ' STOP. ' 
         STOP
         ! CALL ......

      END SELECT

    END FUNCTION inverse_easy
    !========================

  !====================================
  FUNCTION Cross_Product(AA) RESULT(Vn)
  !====================================

    IMPLICIT NONE

    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: AA

    REAL(KIND=8), DIMENSION(SIZE(AA,2)) :: Vn
    !----------------------------------------------

    REAL(KIND=8), DIMENSION(3)   :: a, b, c
    REAL(KIND=8), DIMENSION(3,3) :: a_cross
    !----------------------------------------------

    IF( (SIZE(AA,2) /= SIZE(AA,1)+1) .AND. &
        (SIZE(AA,2) /= 2 .OR. SIZE(AA,2) /= 3) ) THEN
        WRITE(*,*) 'ERROR: inconsistent sizes for the cross product'   
        STOP
    ENDIF

    a = 0.d0
    a(1:SIZE(AA,2)) = AA(1, :)

    b = 0.d0
    IF(SIZE(AA,1) == 1) THEN
       b(3) = 1.d0
    ELSE
       b(:) = AA(2, :)
    ENDIF
        
    a_cross(1, :) = (/  0.d0, -a(3),  a(2) /)
    a_cross(2, :) = (/  a(3),  0.d0, -a(1) /)
    a_cross(3, :) = (/ -a(2),  a(1),  0.d0 /)

    c = MATMUL(a_cross, b)   

    Vn = c(1:SIZE(AA,2))
    
  END FUNCTION Cross_Product
  !=========================

   !===================================
   FUNCTION determinant(A) RESULT (det)
   !=================================== 

      IMPLICIT NONE
     
      REAL(KIND=8), DIMENSION(:,:), INTENT(IN)  ::  A
      REAL(KIND=8)  ::  det 


      IF ( SIZE(A,1) .NE. SIZE(A,2) ) THEN
         WRITE(*,*) ' Matrix A is not square.' 
         WRITE(*,*) ' in FUNCTION determinant, MODULE lin_algebra' 
         WRITE(*,*) ' STOP. ' 
         STOP
      ENDIF

      SELECT CASE ( SIZE(A,1) )

      CASE (2)
         det = A(1,1)*A(2,2) - A(1,2)*A(2,1)

      CASE (3)
         det = A(1,1)*(A(2,2)*A(3,3) - A(3,2)*A(2,3))  &
             - A(1,2)*(A(2,1)*A(3,3) - A(3,1)*A(2,3))  &
             + A(1,3)*(A(2,1)*A(3,2) - A(3,1)*A(2,2))

      CASE DEFAULT 
         WRITE(*,*) ' Matrix of size ', SIZE(A,1), ' not implemented' 
         WRITE(*,*) ' in FUNCTION determinant, MODULE lin_algebra' 
         WRITE(*,*) ' STOP. ' 
         STOP

      END SELECT

   END FUNCTION determinant 
   !=======================

END MODULE PLinAlg

