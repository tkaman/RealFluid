MODULE MF_method

  USE LesTypes
  USE LibComm
  USE ModelInterfaces
  USE PLinAlg
  USE LinAlg,         ONLY : Norme2

  IMPLICIT NONE

  ! This parameter must be the square root of
  ! the machine epsilon (32bit --> mach. eps. 1.0E-16)
  ! Do not touch it for God's Sake!!
  REAL,    PARAMETER :: eps_mf = 1.0E-8

  INTEGER, PARAMETER :: PC_NONE   = cs_relax_mf
  INTEGER, PARAMETER :: PC_JACOBI = cs_relax_mf_jacobi
  INTEGER, PARAMETER :: PC_ILU0   = cs_relax_mf_ilu
  INTEGER, PARAMETER :: PC_LUSGS  = cs_relax_mf_lusgs

  REAL :: R_1, R_0, eta_1
  
CONTAINS
  
  !================================
  SUBROUTINE MF_GMRES(Com, Var, Pc)
  !================================
  !
  ! Matrix free method 
  !
    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "mf_gmres"

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Pc
    !--------------------------------------

    INTEGER :: NCells, Nvar, NvarP, NCellssnd, NCellsIn
    
    INTEGER :: m, km, ite_max
    INTEGER :: j, i, is, ite, irelax, pc_type

    REAL :: hij, hjj, hbj, cosx, sinx
    REAL :: resid, resid_0, rel_toll
    
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vj, Du, Avj, z
    REAL, DIMENSION(:,:,:), ALLOCATABLE :: v
    REAL, DIMENSION(:,:),   ALLOCATABLE :: H
    REAL, DIMENSION(:),     ALLOCATABLE :: Hb
    REAL, DIMENSION(:),     ALLOCATABLE :: Pcos, Psin
    REAL, DIMENSION(:),     ALLOCATABLE :: y    
    !----------------------------------------------------

    pc_type = Data%sel_relax

    m        = Data%MKrylov
    rel_toll = Data%Resdlin
    ite_max  = Data%Nrelax

    NVar   = SIZE(Var%Ua, 1)
    NCells = SIZE(Var%Ua, 2)
    NVarP  = SIZE(Var%Vp, 1)
    
    NCellsIn  = Var%NCellsIn
    NCellssnd = Var%NCellssnd

    ALLOCATE(  Du(Nvar, NCells) )
    ALLOCATE(  Vj(Nvar, NCells) )
    ALLOCATE(   z(Nvar, NCells) )
    ALLOCATE( AVj(Nvar, NCells) )

    ALLOCATE( V(Nvar, NCells, m+1) )

    ALLOCATE( h(m+1, m), Hb(m+1) )

    ALLOCATE( Pcos(m), Psin(m), y(m) )

    IF (see_alloc) THEN
       CALL alloc_allocate(mod_name, "du",   SIZE(du)  )
       CALL alloc_allocate(mod_name, "vj",   SIZE(vj)  )
       CALL alloc_allocate(mod_name, "z",    SIZE(z)   )
       CALL alloc_allocate(mod_name, "avj",  SIZE(avj) )
       CALL alloc_allocate(mod_name, "v",    SIZE(v)   )
       CALL alloc_allocate(mod_name, "h",    SIZE(h)   )
       CALL alloc_allocate(mod_name, "hb",   SIZE(hb)  )
       CALL alloc_allocate(mod_name, "pcos", SIZE(pcos))
       CALL alloc_allocate(mod_name, "psin", SIZE(psin))
       CALL alloc_allocate(mod_name, "y",    SIZE(y)   )
    ENDIF
           
    !******************************************************!
    !             START GMRES ALGORITHM                    !
    !******************************************************! 
  
    IF(Pc_Type == PC_ILU0) THEN
       CALL ILU0_matrix(Pc)
    ENDIF

#ifndef SEQUENTIEL
    CALL EchangeSol(Com, Var%Flux)
#endif

    ! Initial Guess: DU_0 = 0
    Du =  0.0
    Vj = -Var%Flux
    
    resid = Norme2(Vj, Com, NCellsIn)

    v(:, :, 1) = Vj / resid

    Hb(1) = resid
    Hb(2) = 0.0

    resid_0 = resid;   ite = 0

!!$    rel_toll = Forcing_term(com, resid, Var%Kt)

    DO ! Outer Loop
              
       DO j = 1, m ! Inner Loop

          ite = ite + 1

          Vj(:,:) = v(:,:,j)

          ! z <= Pc^-1 * Vj
          CALL Precond(Com, Pc, Pc_Type, Vj,  z)

          ! AVj <= Jac*z
          CALL FD_approx(Com, Var, z,  AVj)
               !^^^^^^^^

          !----------------
          ! Arnoldi Process
          !----------------------------------------------------
          DO i = 1, j

             Vj = v(:,:,i)

             h(i, j) = 0.0
             DO is = 1, NCellsIn
                h(i, j) = h(i, j) + SUM(AVj(:, is) * Vj(:, is))
             END DO

          END DO

#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_sum, h(1:j, j))
#endif

          ! Krylov vector          
          Vj = AVj
          DO i = 1, j
             Vj = Vj - h(i, j) * v(:, :, i)
          END DO

          h(j+1, j) = Norme2(Vj, Com, NCellsIn)

          resid = h(j+1, j)
         
          v(:, :, j+1) = Vj / h(j+1, j)
 
          DO  i = 1, j-1

             cosx = Pcos(i)
             sinx = Psin(i)

             hjj = h(i, j)
             hij = h(i+1, j)

             h(i, j)   =  cosx*hjj + sinx*hij
             h(i+1, j) = -sinx*hjj + cosx*hij

          END DO

          hjj = h(j, j)
          hij = h(j+1, j)

          cosx =  hjj / DSQRT(hjj**2 + hij**2)
          sinx =  hij / DSQRT(hjj**2 + hij**2)

          Pcos(j) = cosx
          Psin(j) = sinx

          h(j, j) = cosx*hjj + sinx*hij

          hbj = Hb(j)

          Hb(j)   =  cosx * Hb(j)
          Hb(j+1) = -sinx * hbj

          ! Residual
          resid = ABS(Hb(j+1)) ; !!write(*,*) ite, resid, '+'

          km = j

          IF (resid <= rel_toll*resid_0 .OR. ite >= ite_max) THEN
             EXIT
          ENDIF
          
       END DO

       y(km) = Hb(km) / h(km, km)

       DO  i = km-1, 1, -1

          hbj = Hb(i)
          DO  j = i + 1, km
             hbj  = hbj  - h(i, j) * y(j)
          END DO

          y(i) = hbj / h(i, i)

       END DO ! Inner Loop

       !---------------------
       ! Approximate solution
       !------------------------------------------------
       Vj = 0.0
 
#ifndef SEQUENTIEL

       DO  i = 1, km
          DO is = 1, NCellssnd
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO

       CALL EchangeSolPrepSend(Com, Vj)

#endif

       DO  i = 1, km
          DO is = NCellssnd+1, NCellsIn
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO

#ifndef SEQUENTIEL
       CALL EchangeSolRecv(Com, Vj)
#endif

       ! z <= Pc^-1 * Vj
       CALL Precond(Com, Pc, Pc_type, Vj,  z)

#ifndef SEQUENTIEL

       Du(:, 1:NCellssnd) = Du(:, 1:NCellssnd) + z(:, 1:NCellssnd)

       CALL EchangeSolPrepSend(Com, Du)

       Du(:, NCellssnd+1:NCellsIn) = Du(:, NCellssnd+1:NCellsIn) + &
                                   &  z(:, NCellssnd+1:NCellsIn)

       CALL EchangeSolRecv(Com, Du)

#else

       Du = Du + z

#endif
      
       ! Vj <= Jac*DU
       CALL FD_approx(Com, Var, Du,  Vj)
            !^^^^^^^^

       Vj = -Var%Flux - Vj

       resid = Norme2(Vj, Com, NCellsIn) ; !!write(*,*) ite, resid, '*'

       IF (resid < rel_toll*resid_0 .OR. ite >= ite_max) THEN
          EXIT
       END IF
       
       v(:, :, 1) = Vj / resid

       Hb(1) = resid
       Hb(2) = 0.0

    ENDDO ! Outer Loop
    
    !******************************************************!
    !               END GMRES ALGORITHM                    !
    !******************************************************!

    IF(Com%Me == 0) THEN
       WRITE(*, 118) ite, resid_0, resid
    ENDIF

#ifndef SEQUENTIEL
    CALL EchangeSolPrepSend(Com, Du)
#endif

    ! Solution increment
    Var%Flux(:, 1:NCellsIn) = Du(:, 1:NCellsIn)

#ifndef SEQUENTIEL
    CALL EchangeSolRecv(Com, Var%Flux)
#endif
           
    DEALLOCATE(Du, Vj, AVj, v, h, Hb, Pcos, Psin, y, z)

    IF (see_alloc) THEN
       CALL alloc_deallocate(mod_name, "du"  )
       CALL alloc_deallocate(mod_name, "vj"  )
       CALL alloc_deallocate(mod_name, "z"   )
       CALL alloc_deallocate(mod_name, "avj" )
       CALL alloc_deallocate(mod_name, "v"   )
       CALL alloc_deallocate(mod_name, "h"   )
       CALL alloc_deallocate(mod_name, "hb"  )
       CALL alloc_deallocate(mod_name, "pcos")
       CALL alloc_deallocate(mod_name, "psin")
       CALL alloc_deallocate(mod_name, "y"   )
    ENDIF

118 FORMAT(' + Lin ite = ', I3, '    RES_0 = ', E12.5, '   RES = ', E12.5, '   +' )
    
  END SUBROUTINE MF_GMRES
  !======================


  !=====================================
  SUBROUTINE FD_approx(Com, Var, v,  Jv)
  !=====================================
  !
  ! J*v ~= (R(u + a*v) - R(u))/a;  J := dR/du
  !
    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "fd_approx"

    TYPE(MeshCom),        INTENT(INOUT) :: Com
    TYPE(Variables),      INTENT(IN)    :: Var
    REAL, DIMENSION(:,:), INTENT(INOUT) :: v

    REAL, DIMENSION(:,:), INTENT(OUT)   :: Jv
    !-------------------------------------------
    TYPE(Variables) :: Var_0

    REAL :: alpha, norm

    INTEGER :: NCells, Nvar, NVarP, &
               NCellsIn, NCellssnd, i
    !-------------------------------------------

    Nvar   = SIZE(Var%Ua, 1)
    NCells = SIZE(Var%Ua, 2)
    NvarP  = SIZE(Var%Vp, 1)

    NCellsIn  = Var%NCellsIn
    NCellssnd = Var%NCellssnd

    !*****************************************
    IF (.NOT. ASSOCIATED(Var_0%Vp)) THEN
       ALLOCATE(Var_0%Vp  (NvarP, NCells))
       ALLOCATE(Var_0%Ua  (Nvar,  NCells))
       ALLOCATE(Var_0%Flux(Nvar,  NCells))
       ALLOCATE(Var_0%h   (2,     NCells))
#ifdef NAVIER_STOKES
       ALLOCATE(Var_0%D_Ua(DATA%Ndim, Nvar, NCells) )
       ALLOCATE(Var_0%Re_loc(NCells) )
#endif

       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "var_0%vp",   SIZE(var_0%vp)  )
          CALL alloc_allocate(mod_name, "var_0%ua",   SIZE(var_0%ua)  )
          CALL alloc_allocate(mod_name, "var_0%flux", SIZE(var_0%flux))
          CALL alloc_allocate(mod_name, "var_0%h",    SIZE(var_0%h)   )
#ifdef NAVIER_STOKES
          CALL alloc_allocate(mod_name, "var_0%d_ua",   SIZE(var_0%d_ua)   )
          CALL alloc_allocate(mod_name, "var_0%re_loc", SIZE(var_0%re_loc) )
#endif
          
       ENDIF
    ENDIF
          
    Var_0%Ncells    = Var%NCells
    Var_0%NCellsIn  = Var%NCellsIn 
    Var_0%NCellsSnd = Var%NCellsSnd
    Var_0%h         = 0.0 ! To be fix, not used by now
    Var_0%avec_h    = .FALSE.

    Var_0%CFL   = Var%CFL
    !****************************************

    !----------------------------
    ! Finite difference parameter
    !------------------------------------------------
    alpha = SQRT((1.0 + Norme2(Var%Ua, Com, NCellsIn)))*eps_mf
    alpha = alpha/Norme2(v, Com, NCellsIn)

    !---------------------------------
    ! Finite increment of the solution 
    ! u + a*v
    !------------------------------------------------------    
    DO i = 1, NCells
       Var_0%Ua(:, i) = Var%Ua(:, i) + alpha*v(:, i)
       CALL ModelU2VpPt(Var_0%Ua(:, i), Var_0%Vp(:, i))
    ENDDO

    Var_0%Flux = 0.0

    !-----------------------------
    ! Finite increment of the flux
    ! R(u + a*v)
    !----------------------------------------
    CALL Model_compute_RHS(Com, Var_0)

#ifndef SEQUENTIEL
    CALL EchangeSol(Com, Var_0%Flux)
#endif

    !------------------------
    ! Jacobian-vector product
    !---------------------------------------------------
    DO i = 1, NCellsin
       Jv(:,i) = (Var_0%Flux(:,i) - Var%Flux(:,i))/alpha + &
                 v(:,i) * Var%Dtloc(i)
    ENDDO

  END SUBROUTINE FD_approx
  !=======================


  !==========================================
  SUBROUTINE Precond(Com, Pc, Pc_Type, v,  z)
  !==========================================

    IMPLICIT NONE

    TYPE(MeshCom),        INTENT(INOUT) :: Com
    TYPE(Matrice),        INTENT(INOUT) :: Pc
    INTEGER,              INTENT(IN)    :: Pc_type
    REAL, DIMENSION(:,:), INTENT(INOUT) :: v

    REAL, DIMENSION(:,:), INTENT(OUT)   :: z
    !-----------------------------------------

    SELECT CASE (Pc_Type)
      
    CASE(PC_NONE)

       CALL NONE_preconditioner(Com, v, z)

    CASE(PC_JACOBI)      

       CALL Jacobi_preconditioner(Com, Pc, v, z)

    CASE(PC_ILU0)

       CALL ILU0_preconditioner(Com, Pc, v, z)

    CASE(PC_LUSGS)

       CALL LUSGS_preconditioner(Com, Pc, v, z)

    CASE DEFAULT

       WRITE(*,*) 'Preconditioner of unknown type.'
       WRITE(*,*) 'STOP!'

       STOP

    END SELECT
    

  END SUBROUTINE Precond
  !=====================


!------------------------------------------------------------------
!---------------------- PRECONDITIONING ---------------------------
!------------------------------------------------------------------

  !========================================
  SUBROUTINE NONE_preconditioner(Com, v, z)
  !========================================

    IMPLICIT NONE

    TYPE(MeshCom),        INTENT(INOUT) :: Com
    REAL, DIMENSION(:,:), INTENT(INOUT) :: v

    REAL, DIMENSION(:,:), INTENT(OUT)   :: z
    !------------------------------------------

    z = v

  END SUBROUTINE NONE_preconditioner
  !=================================

  !==============================================
  SUBROUTINE Jacobi_preconditioner(Com, Pc, v, z)
  !==============================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "jacobi_preconditioner"
    
    TYPE(MeshCom),        INTENT(INOUT) :: Com
    TYPE(Matrice),        INTENT(IN)    :: Pc
    REAL, DIMENSION(:,:), INTENT(INOUT) :: v

    REAL, DIMENSION(:,:), INTENT(OUT) :: z
    !----------------------------------------

    REAL, DIMENSION(:,:), ALLOCATABLE, SAVE :: inv

    INTEGER :: i, id
    !-----------------------------------------------

    IF (.NOT. ALLOCATED(inv)) THEN
       ALLOCATE( inv(SIZE(v,1), SIZE(v,1)) )

       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "inv", SIZE(inv))
       ENDIF
    ENDIF

    DO i = 1, Pc%NNin

       id  = Pc%IDiag(i)

       inv = inverse(Pc%Vals(:,:,id))

       z(:, i) = MATMUL(inv, v(:, i))
       
    ENDDO
    
  END SUBROUTINE Jacobi_preconditioner
  !===================================

  !============================================
  SUBROUTINE ILU0_preconditioner(Com, Pc, v, z)
  !============================================

    IMPLICIT NONE

    TYPE(MeshCom),        INTENT(INOUT) :: Com
    TYPE(Matrice),        INTENT(IN)    :: Pc
    REAL, DIMENSION(:,:), INTENT(IN)    :: v

    REAL, DIMENSION(:,:), INTENT(OUT) :: z
    !----------------------------------------

    INTEGER :: i, j, jm, i_s, i_e, i_d
    !----------------------------------------

    z = v

    !--------------
    ! Backward step
    !-----------------------------------
#ifndef SEQUENTIEL

    DO i = 1, Pc%NNsnd

       i_s = Pc%JPosi(i)
       i_e = Pc%JPosi(i+1) - 1

       DO jm = i_s, i_e

          j = Pc%JvCell(jm)

          IF (j >= i) EXIT

          z(:, i) = z(:, i) - MATMUL(Pc%ILUVals(:,:, jm), z(:, j))

       ENDDO

    ENDDO

    CALL EchangeSolPrepSend(Com, z)

#endif


    DO i = Pc%NNsnd+1, Pc%NNin

       i_s =  Pc%JPosi(i)
       i_e =  Pc%JPosi(i+1) - 1

       DO jm = i_s, i_e

          j = Pc%JvCell(jm)

          IF (j >= i) EXIT

          z(:, i)  = z(:, i) - MATMUL(Pc%ILUVals(:,:, jm), z(:, j))

       ENDDO

    ENDDO

#ifndef SEQUENTIEL
    CALL EchangeSolRecv(Com, z)
#endif

    !--------------
    ! Forward step
    !--------------------------------- 
    DO i = Pc%NNin, 1, -1

       i_s = Pc%JPosi(i)
       i_e = Pc%JPosi(i+1) - 1

       DO jm = i_s, i_e

          j = Pc%JvCell(jm)

          IF (j <= i) EXIT

          z(:, i) = z(:, i) - MATMUL(Pc%ILUVals(:,:, jm), z(:, j))

       ENDDO

       i_d = Pc%IDiag(i)

       z(:, i) = MATMUL(Pc%ILUVals(:,:, i_d), z(:, i))

    ENDDO

#ifndef SEQUENTIEL
    CALL EchangeSol(Com, z) 
#endif     

  END SUBROUTINE ILU0_preconditioner
  !=================================

  !=============================================
  SUBROUTINE LUSGS_preconditioner(Com, Pc, v, z)
  !=============================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "lusgs_preconditioner"

    TYPE(MeshCom),        INTENT(INOUT) :: Com
    TYPE(Matrice),        INTENT(IN)    :: Pc
    REAL, DIMENSION(:,:), INTENT(IN)    :: v

    REAL, DIMENSION(:,:), INTENT(OUT) :: z
    !------------------------------------------

    REAL, DIMENSION(:,:,:), ALLOCATABLE, SAVE :: inv_D
    REAL, DIMENSION(:,:),   ALLOCATABLE, SAVE :: z_old

    REAL :: res

    INTEGER :: NCells, i, j, jm, i_s, i_e, l, k
    !---------------------------------------------------

    REAL,    PARAMETER :: in_toll = 1.0E-6
    INTEGER, PARAMETER :: k_max = 1
    INTEGER, PARAMETER :: k_min = 1
    !-----------------------------------------------

    NCells = SIZE(v, 2)

    IF(.NOT. ALLOCATED(inv_D)) THEN
       ALLOCATE( inv_D(SIZE(v, 1), SIZE(v, 1), SIZE(v, 2)) )

       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "inv_d", SIZE(inv_d))
       ENDIF
    ENDIF

    IF(.NOT. ALLOCATED(z_old)) THEN
       ALLOCATE( z_old(SIZE(v, 1), SIZE(v, 2)) )

       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "z_old", SIZE(z_old))
       ENDIF
    ENDIF
   
    ! Store the inverse of the diagonal
    DO i = 1, Pc%NNin

       j  = Pc%IDiag(i)
       
       inv_D(:,:, i) = inverse(Pc%Vals(:,:, j))

    ENDDO

    z = 0.d0
    DO k = 1, k_max
       
       z_old = z

       !--------------
       ! FORWARD SWEEP
       !-----------------------------------------------
#ifndef SEQUENTIEL

       DO i = 1, Pc%NNsnd

          z(:, i) = v(:, i)
          
          i_s = Pc%JPosi(i)
          i_e = Pc%JPosi(i+1) - 1

          DO jm = i_s, i_e

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

             z(:, i) = z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO

       CALL EchangeSolPrepSend(Com, z)

#endif

       DO i = Pc%NNsnd+1, Pc%NNin

          z(:, i) = v(:, i)

          i_s = Pc%JPosi(i)
          i_e = Pc%JPosi(i+1) - 1

          DO jm = i_s, i_e

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

              z(:, i) = z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO
       !----------------------------------------------------

#ifndef SEQUENTIEL
    CALL EchangeSolRecv(Com, z)
#endif

       !----------------
       ! BACKWARD SWEEP
       !----------------------------------------------------
       DO i = Pc%NNin, 1, -1

          z(:, i) = v(:, i)

          i_s = Pc%JPosi(i+1) - 1
          i_e = Pc%JPosi(i)

          DO jm = i_s, i_e, -1

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

              z(:, i) =  z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO
       !----------------------------------------------------
    
#ifndef SEQUENTIEL
    CALL EchangeSol(Com, z)
#endif

       res = Norme2( z-z_old, Com, NCells )
       
       IF( k >= k_min  ) THEN
          IF( res <= in_toll ) EXIT
       ENDIF

    ENDDO

#ifndef SEQUENTIEL
    CALL EchangeSolPrepSend(Com, z)
#endif

  END SUBROUTINE LUSGS_preconditioner
  !==================================

  !=========================
  SUBROUTINE ILU0_matrix(Pc)
  !=========================

    IMPLICIT NONE

    TYPE(Matrice), INTENT(INOUT) :: Pc
    !---------------------------------

    REAL, DIMENSION(Data%NVar, Data%NVar) :: inv_D

    INTEGER :: i, k, j, ik, ij, kj, n, ck, cj
    INTEGER :: idx_diag_i, idx_diag_k
    INTEGER :: idx_c_s, idx_c_e, idx_s, idx_e
    !------------------------------

    ! ILU(0) matrix factorization
    !
    !    L : lower trinagular matrix 
    !        Identity diagonal (not-stored) 
    !
    !    U : Upper tringular matrix
    !        Diagonal elements stored inverted
    !

    Pc%ILUVals = Pc%Vals

    DO i = 1, Pc%NNin

       idx_diag_i = Pc%IDiag(i)

       idx_c_s = Pc%JPosi(i)
       idx_c_e = Pc%JPosi(i+1) - 1
 
       DO k = idx_c_s, idx_c_e

          IF( k >= idx_diag_i ) EXIT

          ck = Pc%JvCell(k)
          idx_diag_k = Pc%IDiag(ck)

          inv_D = inverse( Pc%ILUVals(:,:, idx_diag_k) )

          ik = k

          Pc%ILUVals(:,:, ik) = MATMUL( Pc%ILUVals(:,:, ik), inv_D )

          DO j = k+1, idx_c_e
          
             ij = j

             cj = Pc%JvCell(j)

             idx_s = Pc%JPosi(ck)
             idx_e = Pc%JPosi(ck+1)-1

             DO n = idx_s, idx_e
                
                IF( Pc%JvCell(n) == cj ) THEN

                   kj = n

                   Pc%ILUVals(:,:, ij) = Pc%ILUVals(:,:, ij) - &
                                         MATMUL( Pc%ILUVals(:,:, ik), &
                                                 Pc%ILUVals(:,:, kj) )

                ENDIF

             ENDDO ! n

          ENDDO ! j

       ENDDO ! k

    ENDDO ! i

    ! Invert the diagonal blocks
    DO i = 1, Pc%NNin

       n = Pc%IDiag(i)

       Pc%ILUVals(:,:, n) = inverse( Pc%ILUVals(:,:, n) )

    ENDDO

  END SUBROUTINE ILU0_matrix
  !=========================

!!$  !=============================================
!!$  FUNCTION Forcing_term(Com, Res, k) RESULT(eta)
!!$  !=============================================
!!$
!!$    IMPLICIT NONE
!!$
!!$    TYPE(MeshCom),   INTENT(INOUT) :: Com
!!$    REAL,    INTENT(IN) :: Res
!!$    INTEGER, INTENT(IN) :: k
!!$
!!$    REAL :: eta
!!$    !---------------------------------
!!$
!!$    REAL :: eta_0
!!$
!!$    REAL, PARAMETER :: p_g = 0.2d0
!!$    REAL, PARAMETER :: p_a = 2.0d0
!!$    REAL, PARAMETER :: eta_max = 0.9d0
!!$    !---------------------------------
!!$
!!$!!IF( com%me == 0) THEN
!!$!!write(*,*) '-----------------------------------'
!!$!!write(*, '(I5, 3E16.5)') k, Res, R_0, R_1
!!$!!write(*, '(5X, 2E16.5)')    eta_0, eta_1 
!!$!!write(*,*)
!!$!!ENDIF
!!$
!!$    IF(k <= 1) THEN
!!$
!!$       eta_0 = Data%Resdlin
!!$
!!$       R_1 = 1.d0
!!$       R_0 = Res
!!$
!!$    ELSE
!!$
!!$       R_1 = R_0
!!$       R_0 = Res
!!$    
!!$       eta_0 = p_g * (R_0/R_1)**p_a
!!$
!!$       IF( (p_g*eta_1**2) > 0.1d0 ) THEN
!!$          eta_0 = MIN( eta_max, MAX( eta_0, p_g*eta_1**2 ) )
!!$       ELSE
!!$          eta_0 = MIN( eta_max, eta_0 )
!!$       ENDIF
!!$
!!$    ENDIF
!!$
!!$    eta_1 = eta_0
!!$
!!$    eta = eta_0
!!$
!!$!!IF( com%me == 0) THEN
!!$!!write(*, '(5X, 1E16.5)')    eta
!!$!!write(*,*) '-----------------------------------'
!!$!!ENDIF
!!$
!!$  END FUNCTION Forcing_term
!!$  !========================

END MODULE MF_method
