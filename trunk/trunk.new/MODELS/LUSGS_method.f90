MODULE LUSGS_method

  USE LesTypes
  USE LibComm
  USE LinAlg

  USE ModelInterfaces

  IMPLICIT NONE

CONTAINS

  !======================================
  SUBROUTINE NonLin_BLU_SGS(Com, Var, Pc)
  !======================================
    
    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(IN)    :: Pc
    !--------------------------------------

    REAL, DIMENSION(:,:), ALLOCATABLE, SAVE :: z, z_old
    REAL, DIMENSION(:,:), ALLOCATABLE, SAVE :: Vc_s

    REAL, DIMENSION(:,:,:), ALLOCATABLE, SAVE :: inv_D
    REAL, DIMENSION(Data%NVar, Data%NVar) :: DD

    REAL, DIMENSION(Data%NVar) :: R_s, d_Ua, Vc_ref
    !----------------------------------------------

    REAL :: res

    INTEGER :: i, j, jm, k, i_e, i_s, l
    INTEGER :: N_var, NNin, NNsnd, NN
    !-----------------------------------------------

    REAL,    PARAMETER :: eps = 1.0E-4
    REAL,    PARAMETER :: in_toll = 1.0E-9
    INTEGER, PARAMETER :: k_max = 60
    INTEGER, PARAMETER :: k_min = 5
    INTEGER, PARAMETER :: p_freeze = 1
    !-----------------------------------------------

    N_Var = SIZE(Var%Ua, 1)

    NN    = Var%NCells
    NNIn  = Var%NCellsIN
    NNSnd = Var%NCellsSND

!!$CALL ModelVp2u( Data%VarPhyInit(:, 1), Vc_ref ) 
  
    IF( .NOT. ALLOCATED(inv_D) ) THEN

       ALLOCATE( inv_D(N_var, N_var, NNin) )

       ALLOCATE(     z(N_var, NN) ) 
       ALLOCATE( z_old(N_var, NN) )
       ALLOCATE(  Vc_s(N_var, NN) )

    ENDIF

#ifndef SEQUENTIEL
    CALL EchangeSol(Com, Var%Flux)
    CALL EchangeSol(Com, Var%Ua)
#endif

    Vc_s = Var%Ua

!!$    IF( MOD(Var%kt-1, p_freeze) == 0 ) THEN
!!$
!!$       ! Store the inverse of the diagonal
!!$       DO i = 1, NNin
!!$
!!$          if( Var%kt == 1 ) then
!!$             d_UA = 1.0E-12
!!$          else
!!$             d_Ua = eps * SQRT((Var%Ua(:,i) - Vc_ref)**2)
!!$          endif
!!$          d_Ua(5) = MIN(1.0E-4, d_Ua(5))
!!$
!!$          !d_Ua = eps * SQRT((Var%Ua(:,i) - Vc_ref)**2)
!!$
!!$          DD = 0.d0
!!$
!!$          DO l = 1, N_var
!!$
!!$             ! Perturbation of the solution
!!$             Vc_s(l, i) = Var%Ua(l, i) + d_Ua(l)
!!$
!!$             R_s = Model_compute_RHS_i(i, Vc_s)
!!$
!!$             ! Finite difference approx
!!$             IF( d_Ua(l) /= 0.d0 ) THEN
!!$                DD(:, l) = ( R_s - Var%Flux(:, i) ) / ( d_Ua(l) )
!!$             ENDIF
!!$
!!$             ! Diagonal time term
!!$             IF( DD(l,l) /= 1.d0 ) THEN
!!$                DD(l, l) = DD(l, l) + Var%Dtloc(i)
!!$             ENDIF
!!$
!!$             ! Back to the original solution
!!$             Vc_s(l, i) = Var%Ua(l, i)
!!$
!!$          ENDDO
!!$
!!$          inv_D(:, :, i) = inverse( DD )
!!$
!!$       ENDDO
!!$
!!$    ENDIF

    DO i = 1, Pc%NNin

       l = Pc%IDiag(i)
       
       inv_D(:,:, i) = inverse( Pc%Vals(:,:, l) )

    ENDDO

    z = 0.d0
    DO k = 1, k_max

       z_old = z
      
       !---------------
       ! FORWARD SWEEP
       !----------------------------------------------------
#ifndef SEQUENTIEL

       DO i = 1, NNsnd

          R_s = Model_compute_RHS_i(i, Vc_s)

          z(:, i) = z(:, i) - MATMUL( inv_D(:,:, i), R_s )

          Vc_s(:, i) = Var%Ua(:, i) + z(:, i)

       ENDDO

       CALL EchangeSolPrepSend(Com, z)
       CALL EchangeSolPrepSend(Com, Vc_s)

#endif

       DO i = NNsnd+1, NNin

          R_s = Model_compute_RHS_i(i, Vc_s)

          z(:, i) = z(:, i) - MATMUL( inv_D(:,:, i), R_s )

          Vc_s(:, i) = Var%Ua(:, i) + z(:, i)

       ENDDO
       !----------------------------------------------------

#ifndef SEQUENTIEL
       CALL EchangeSolRecv(Com, z)
       CALL EchangeSolRecv(Com, Vc_s)
#endif

       !----------------
       ! BACKWARD SWEEP
       !----------------------------------------------------
       DO i = NNin, 1, -1

          R_s = Model_compute_RHS_i(i, Vc_s)

          z(:, i) = z(:, i) - MATMUL( inv_D(:,:, i), R_s )

          Vc_s(:, i) = Var%Ua(:, i) + z(:, i)

       ENDDO
       !----------------------------------------------------

#ifndef SEQUENTIEL
    CALL EchangeSol(Com, z)
    CALL EchangeSol(Com, Vc_s) 
#endif
      
       res = Norme2( z-z_old, Com, NNin )

       IF( Com%me == 0 ) THEN
          IF( MOD(k, 1) == 0 ) THEN
             WRITE(*,'("   +++ ", I3, 2X, E12.6, " +++")') k, res
          ENDIF
       ENDIF

       IF( k >= k_min  ) THEN
          IF( res <= in_toll ) EXIT
       ENDIF

    ENDDO

#ifndef SEQUENTIEL
    CALL EchangeSolPrepSend(Com, z)
#endif

    Var%Flux(:, 1:NNin) = z(:, 1:NNin)

#ifndef SEQUENTIEL
    CALL EchangeSolRecv(Com, Var%Flux)
#endif

  END SUBROUTINE NonLin_BLU_SGS  
  !============================

  !===============================
  SUBROUTINE BLU_SGS(Com, Var, Pc)
  !===============================
    
    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(IN)    :: Pc
    !--------------------------------------

    REAL, DIMENSION(:,:), ALLOCATABLE :: z, z_old, Vc_s

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: inv_D
    !----------------------------------------------

    REAL :: res, d_Ua

    REAL, DIMENSION(Data%NVar) :: R_s

    INTEGER :: i, j, jm, k, i_e, i_s, l
    INTEGER :: N_var, N_dofs, N_In, N_snd

    REAL,    PARAMETER :: eps = 1.0E-6
    REAL,    PARAMETER :: in_toll = 1.0E-6
    INTEGER, PARAMETER :: k_max = 20
    INTEGER, PARAMETER :: k_min = 5
    INTEGER, PARAMETER :: p_freeze = 5
    !-----------------------------------------------

    N_Var  = SIZE(Var%Ua, 1)
    N_dofs = SIZE(Var%Ua, 2)
    N_In   = Var%NCellsIN
    N_Snd  = Var%NCellsSND

    ALLOCATE( inv_D(N_var, N_var, N_dofs) )

    ALLOCATE(     z(N_var, N_dofs) , &
              z_old(N_var, N_dofs) )
   
    ! Store the inverse of the diagonal
    DO i = 1, Pc%NNin

       l = Pc%IDiag(i)
       
       inv_D(:,:, i) = inverse( Pc%Vals(:,:, l) )

    ENDDO

#ifndef SEQUENTIEL
    CALL EchangeSol(Com, Var%Flux)
#endif
    
    z = 0.d0
    DO k = 1, k_max
       
       z_old = z

       !--------------
       ! FORWARD SWEEP
       !-----------------------------------------------
#ifndef SEQUENTIEL

       DO i = 1, Pc%NNsnd

          z(:, i) = -Var%Flux(:, i)
          
          i_s = Pc%JPosi(i)
          i_e = Pc%JPosi(i+1) - 1

          DO jm = i_s, i_e

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

             z(:, i) = z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO

       CALL EchangeSolPrepSend(Com, z)

#endif

       DO i = Pc%NNsnd+1, Pc%NNin

          z(:, i) = -Var%Flux(:, i)

          i_s = Pc%JPosi(i)
          i_e = Pc%JPosi(i+1) - 1

          DO jm = i_s, i_e

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

              z(:, i) = z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO
       !----------------------------------------------------

#ifndef SEQUENTIEL
       CALL EchangeSolRecv(Com, z)
#endif

       !----------------
       ! BACKWARD SWEEP
       !----------------------------------------------------
       DO i = Pc%NNin, 1, -1

          z(:, i) = -Var%Flux(:, i)

          i_s = Pc%JPosi(i+1) - 1
          i_e = Pc%JPosi(i)

          DO jm = i_s, i_e, -1

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

              z(:, i) =  z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO
       !----------------------------------------------------
    
#ifndef SEQUENTIEL
    CALL EchangeSol(Com, z)
#endif

       res = Norme2( z-z_old, Com, N_In )
       
       IF( Com%Me == 0 ) THEN
          WRITE(*,'("   +++ ", I3, 2X, E12.6, " +++")') k, res
       ENDIF

       IF( k >= k_min  ) THEN
          IF( res <= in_toll ) EXIT
       ENDIF

    ENDDO

#ifndef SEQUENTIEL
    CALL EchangeSolPrepSend(Com, z)
#endif
 
    Var%Flux(:, 1:N_in) = z(:, 1:N_in)

#ifndef SEQUENTIEL
    CALL EchangeSolRecv(Com, Var%Flux)
#endif

    DEALLOCATE( inv_D, z, z_old )

  END SUBROUTINE BLU_SGS
  !=====================

END MODULE LUSGS_method
