MODULE Turbulence_models

  USE LesTypes
  USE EOSLaws,                ONLY: EOSCp__T_rho
  USE Spalart_Allmaras_model
  
  IMPLICIT NONE

  !==============================================
  LOGICAL :: Laminar, Turbulent
  INTEGER :: turb_models, N_eqn_turb
  !----------------------------------------------

  INTEGER, PRIVATE, PARAMETER :: TURB_NONE             = 0, &
                                 TURB_SPALART_ALLMARAS = 1

  CHARACTER(*), DIMENSION(0:1), PRIVATE, PARAMETER ::&
                turb_names = (/ 'NONE',     &
                                'SPALART-ALLMARAS' /)
  !==============================================

  PUBLIC  :: Laminar, Turbulent, N_eqn_turb

  PRIVATE :: turb_models, decoder_turb_models

  REAL, PRIVATE :: Re_ref
  !==============================================

CONTAINS

 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INITIALIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !========================================================
  SUBROUTINE Init_Turbulence(Fluid_model, Turbulence_model)
  !========================================================

    IMPLICIT NONE

    CHARACTER(LEN = *), INTENT(IN) :: Fluid_model
    CHARACTER(LEN = *), INTENT(IN) :: Turbulence_model
    !-------------------------------------------------

    IF( TRIM(fluid_model) == "laminar" ) THEN

       Laminar   = .TRUE.
       Turbulent = .FALSE.

    ELSEIF( TRIM(fluid_model) == "turbulent" ) THEN

       Laminar   = .FALSE.       
       Turbulent = .TRUE.

    ELSE

       WRITE(*,*) 'ERROR: fluid model unknown'
       STOP

    ENDIF

    IF( Turbulent ) THEN
       
       CALL decoder_turb_models(Turbulence_model, turb_models)       

       SELECT CASE(turb_models)

       CASE(TURB_SPALART_ALLMARAS)

          CALL Init_Turb_SA(N_eqn_turb)
          
       CASE(TURB_NONE)

          WRITE(*,*) 'ERROR: turbulent flows with no turbulent model'
          STOP

       CASE DEFAULT

          WRITE(*,*) 'ERROR: Unknown turbulence model'          
          STOP          

       END SELECT       

       WRITE(*,*) 'Turbulent flow, with ',  turb_names(turb_models), ' model'

    ELSE

       WRITE(*,*) 'Laminar flow'

    ENDIF
    
  END SUBROUTINE Init_Turbulence
  !=============================

  !===============================
  SUBROUTINE Turb_Set_VarVP(N_var)
  !===============================

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: N_var
    !---------------------------

    INTEGER :: N_
    !---------------------------

    N_ = N_var - N_eqn_turb

    SELECT CASE(turb_models)
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL set_VarVp_SA(N_)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT   


  END SUBROUTINE Turb_Set_VarVP
  !============================

  !===============================
  SUBROUTINE Turb_Set_VarUa(N_var)
  !===============================

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: N_var
    !---------------------------

    INTEGER :: N_
    !---------------------------

    N_ = N_var - N_eqn_turb

    SELECT CASE(turb_models)
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL set_VarUa_SA(N_)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT   

  END SUBROUTINE Turb_Set_VarUa
  !============================

  !==========================================
  SUBROUTINE Turb_Read_InitVals(line, p_read)
  !==========================================
  !
  ! Read the values of the turbulent model
  ! used to initialize the solution in the domain  
  !
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: line
    INTEGER,          INTENT(IN) :: p_read
    !----------------------------------------------

    SELECT CASE(turb_models)
       
    CASE(TURB_SPALART_ALLMARAS)
          
       CALL Read_InitVals_SA(line, p_read)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT   

  END SUBROUTINE Turb_Read_InitVals
  !================================

  !================================================
  SUBROUTINE Turb_init_var(Vp_init, Re_adim, mu_oo)
  !================================================
  !
  ! Initialize the turbulent quantities
  ! in the domain
  !
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp_init
    REAL,               INTENT(IN)    :: Re_adim
    REAL,               INTENT(IN)    :: mu_oo
    !-------------------------------------------

    SELECT CASE(turb_models)
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL InitVar_SA(Vp_init, Re_adim, mu_oo)
       Re_ref = Re_adim

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END SUBROUTINE Turb_init_var
  !===========================

  !==========================================
  SUBROUTINE Turb_Read_bc(line, p_read, Vp_b)
  !==========================================
  !
  ! Far-field or Inlet boundary conditions
  ! for the turbulent equations
  !
    IMPLICIT NONE

    CHARACTER(LEN=*),   INTENT(IN)    :: line
    INTEGER ,           INTENT(IN)    :: p_read
    REAL, DIMENSION(:), INTENT(INOUT) :: Vp_b
    !----------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL Read_bc_SA(line, p_read, Vp_b)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT   

  END SUBROUTINE Turb_Read_bc
  !==========================  

  !=================================
  SUBROUTINE Turb_Init_Wall_bc(Vp_b)
  !=================================
  ! 
  ! Wall boundary conditions for the
  ! turbulence models
  ! 
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp_b
    !----------------------------------------

    SELECT CASE(turb_models)       
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL Init_Wall_bc_SA(Vp_b)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT     

  END SUBROUTINE Turb_Init_Wall_bc
  !===============================
  
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%% COUPLING EQUATIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !=====================================
  SUBROUTINE Turb_cons_variables(Vp, Vc)
  !=====================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:), INTENT(INOUT) :: Vc
    !---------------------------------------

    SELECT CASE(turb_models)       
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL Cons_Variables_SA(Vp, Vc)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT     

  END SUBROUTINE Turb_cons_variables
  !=================================  

  !=====================================
  SUBROUTINE Turb_prim_variables(Vc, Vp)
  !=====================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)    :: Vc
    REAL, DIMENSION(:), INTENT(INOUT) :: Vp
    !---------------------------------------

    SELECT CASE(turb_models)       
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL Prim_Variables_SA(Vc, Vp)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT     

  END SUBROUTINE Turb_prim_variables
  !=================================

  !=======================================
  SUBROUTINE Turb_interp_Vp(Vp, phi, Vp_o)
  !=======================================

    IMPLICIT NONE

    REAL, DIMENSION(:,:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:),   INTENT(IN)    :: phi
    REAL, DIMENSION(:),   INTENT(INOUT) :: Vp_o
    !------------------------------------------

    SELECT CASE(turb_models)       
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL interp_Vp_SA(Vp, phi, Vp_o)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT 

  END SUBROUTINE Turb_interp_Vp  
  !============================

  !===================================
  SUBROUTINE Turb_mean_state(Vp, Vp_b)
  !===================================

    IMPLICIT NONE

    REAL, DIMENSION(:,:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:),   INTENT(INOUT) :: Vp_b
    !------------------------------------------

    SELECT CASE(turb_models)       
       
    CASE(TURB_SPALART_ALLMARAS)

       CALL mean_state_SA(Vp(i_nuts_vp,:), Vp_b(i_nuts_vp))

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT 

  END SUBROUTINE Turb_mean_state
  !=============================  

  !===============================================
  FUNCTION Turb_eddy_viscosity(Vp, mu) RESULT(mut)
  !===============================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    REAL,               INTENT(IN) :: mu

    REAL :: mut
    !-----------------------------------

    REAL :: rho, mut_s, mu_
    !------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       rho   = Vp(i_rho_vp)
       mut_s = Vp(i_nuts_vp)*rho

       mu_ = mu*Re_ref

       mut = eddy_viscosity_SA(mut_s, mu_)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END FUNCTION Turb_eddy_viscosity
  !===============================

  !===============================================================
  FUNCTION Turb_eddy_thermal_conductivity(Vp, mu) RESULT(CondTh_t)
  !===============================================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    REAL,               INTENT(IN) :: mu

    REAL :: CondTh_t
    !-----------------------------------

    REAL :: Cp, mut_s, mu_
    !-----------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       Cp = EOSCp__T_rho( Vp(i_rho_vp), Vp(i_tem_vp) )

       mut_s = Vp(i_nuts_vp)*Vp(i_rho_vp)

       mu_ = mu*Re_ref

       CondTh_t = eddy_therm_cond_SA(mut_s, mu_, cp)
       
    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END FUNCTION Turb_eddy_thermal_conductivity
  !==========================================
  
  !=====================================
  SUBROUTINE Turb_advection_flux(Vp, ff)
  !=====================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:), INTENT(INOUT) :: ff
    !----------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL advection_flux_SA( Vp, ff(i_muts_ua, :) )

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT
    
  END SUBROUTINE Turb_advection_flux
  !=================================

  !================================================  
  SUBROUTINE Turb_Advection_Jacobian(N_dim, Vp, AA)
  !================================================

    IMPLICIT NONE

    INTEGER,                INTENT(IN)    :: N_dim    
    REAL, DIMENSION(:),     INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:,:), INTENT(INOUT) :: AA
    !-------------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL Advection_Jacobian_SA( N_dim, Vp, AA(i_muts_ua, :, :) )

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END SUBROUTINE Turb_Advection_Jacobian
  !=====================================  

  !====================================================  
  SUBROUTINE Turb_Advection_eigenvalues(Vp, nn, lambda)
  !====================================================

    IMPLICIT NONE
  
    REAL, DIMENSION(:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:), INTENT(IN)    :: nn
    REAL, DIMENSION(:), INTENT(INOUT) :: lambda
    !-------------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL Advection_eigenvalues_SA( Vp, nn, lambda(i_muts_ua) )

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END SUBROUTINE Turb_Advection_eigenvalues  
  !========================================

  !======================================================= 
  SUBROUTINE Turb_Advection_eigenvectors(Vp, v_nn, RR, LL)
  !=======================================================

    IMPLICIT NONE
  
    REAL, DIMENSION(:),   INTENT(IN)    :: Vp
    REAL, DIMENSION(:),   INTENT(IN)    :: v_nn
    REAL, DIMENSION(:,:), INTENT(INOUT) :: RR
    REAL, DIMENSION(:,:), INTENT(INOUT) :: LL
    !-------------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL Advection_eigenvectors_SA( Vp, v_nn, RR(i_muts_ua, :), &
                                                 LL(i_muts_ua, :) )

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END SUBROUTINE Turb_Advection_eigenvectors  
  !=========================================

  !========================================================
  SUBROUTINE Turb_diffusion_flux_function(Vp, D_vc, mu, ff)
  !========================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),     INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:),   INTENT(IN)    :: D_Vc
    REAL,                   INTENT(IN)    :: mu
    REAL, DIMENSION(:,:),   INTENT(INOUT) :: ff
    !---------------------------------------------

    REAL :: mu_
    !---------------------------------------------
    
    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       mu_ = mu*Re_ref

       CALL diffusion_flux_function_SA(Vp, D_Vc, mu_, ff(i_muts_ua, :))

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END SUBROUTINE Turb_diffusion_flux_function  
  !==========================================

  !====================================================
  SUBROUTINE Turb_Diffusion_Jacobian(N_dim, Vp, mu, KK)
  !====================================================

    IMPLICIT NONE

    INTEGER,                   INTENT(IN)    :: N_dim
    REAL, DIMENSION(:),        INTENT(IN)    :: Vp
    REAL,                      INTENT(IN)    :: mu
    REAL, DIMENSION(:,:, :,:), INTENT(INOUT) :: KK
    !----------------------------------------------

    REAL :: mu_
    !----------------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       mu_ = mu*Re_ref

       CALL Diffusion_Jacobian_SA(N_dim, Vp, mu_, KK(i_muts_ua,:, :,:))

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT
            
  END SUBROUTINE Turb_Diffusion_Jacobian
  !=====================================

  !===============================================================
  FUNCTION Turb_Source_term(Vp, G_Vc, mu, ww_mod, d_min) RESULT(S)
  !===============================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_vc
    REAL,                 INTENT(IN) :: mu
    REAL,                 INTENT(IN) :: ww_mod
    REAL,                 INTENT(IN) :: d_min

    REAL, DIMENSION(SIZE(G_Vc, 2)) :: S
    !----------------------------------------------

    REAL, DIMENSION(SIZE(G_Vc,1)) :: G_rho, G_mut_s

    REAL :: rho, mut_s, mu_
    !----------------------------------------------

    SELECT CASE(turb_models)       

    CASE(TURB_SPALART_ALLMARAS)

       S = 0.d0
         
       rho     = Vp(i_rho_vp)
       mut_s   = Vp(i_nuts_vp)*rho

       mu_ = mu*Re_ref

       G_rho   = G_vc(:, i_rho_ua)
       G_mut_s = G_vc(:, i_muts_ua)

       S(i_muts_ua) = Source_SA(mut_s, mu_, rho, ww_mod, d_min, G_rho, G_mut_s)
       
    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT   

  END FUNCTION Turb_Source_term
  !============================

  !======================================================================
  FUNCTION Turb_Jacobian_Source( Vp, G_Vc, mu, ww_mod, d_min ) RESULT(JJ)
  !======================================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_vc
    REAL,                 INTENT(IN) :: mu
    REAL,                 INTENT(IN) :: ww_mod
    REAL,                 INTENT(IN) :: d_min

    REAL, DIMENSION( SIZE(G_Vc,2), SIZE(G_Vc,2) ) :: JJ
    !------------------------------------------------

    REAL :: mu_
    !------------------------------------------------

    SELECT CASE(turb_models)       

    CASE(TURB_SPALART_ALLMARAS)

       mu_ = mu*Re_ref

       JJ = Jacobian_Source_SA( Vp, G_Vc, mu_, ww_mod, d_min )

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT

  END FUNCTION Turb_Jacobian_Source
  !================================  
 
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%% Boundary Condidions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !=======================================
  SUBROUTINE Turb_sym_plane_bc(nn, Vp, ff)
  !=======================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)    :: nn    
    REAL, DIMENSION(:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:), INTENT(INOUT) :: ff
    !--------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL sym_plane_bc_SA(nn, Vp, ff)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT

  END SUBROUTINE Turb_sym_plane_bc
  !===============================

  !=========================================
  SUBROUTINE Turb_Jac_sym_plane(Vp, nn, Jac)
  !=========================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)    :: Vp    
    REAL, DIMENSION(:),   INTENT(IN)    :: nn
    REAL, DIMENSION(:,:), INTENT(INOUT) :: Jac
    !-----------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL Jac_sym_plane_SA(Vp, nn, Jac)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT    

  END SUBROUTINE Turb_Jac_sym_plane
  !================================

  !==============================
  SUBROUTINE Turb_wall_bc(Vp, ff)
  !==============================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp
    REAL, DIMENSION(:), INTENT(INOUT) :: ff
    !--------------------------------------

    SELECT CASE(turb_models)

    CASE(TURB_SPALART_ALLMARAS)

       CALL wall_bc_SA(Vp, ff)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown turbulence model'
       STOP

    END SELECT

  END SUBROUTINE Turb_wall_bc
  !==========================

  !=========================================================
  SUBROUTINE decoder_turb_models(Turbulence_model, Cod_turb)    
  !=========================================================
    IMPLICIT NONE
    
    CHARACTER(LEN=*), INTENT(IN)  :: Turbulence_model
    INTEGER,          INTENT(OUT) :: Cod_turb
    !-------------------------------------------------
    
    IF( TRIM(Turbulence_model) == "spalartallmaras" ) THEN
       
       Cod_turb = TURB_SPALART_ALLMARAS
       
    ELSEIF( TRIM(Turbulence_model) == "none" ) THEN
       
       Cod_turb = TURB_NONE
              
    ELSE
       
       WRITE(*,*) 'ERROR: turbulent model unknow'
       STOP
       
    ENDIF          

  END SUBROUTINE decoder_turb_models
  !=================================

END MODULE Turbulence_models
