MODULE Num_schemes

  USE LesTypes
  USE Scheme_LW
  USE Scheme_LDA
  USE Scheme_LF
  USE Scheme_SUPG

  IMPLICIT NONE

  INTEGER, PARAMETER :: LW   = cs_iflux_1, &
                        LDA  = cs_iflux_2, &
                        LF   = cs_iflux_3, &
                        SUPG = cs_iflux_4

CONTAINS

  !===================================================================
  SUBROUTINE Distribute_residual_imp(ele, Vp, Vc, CFL, Phi_tot, JJ_tot, &
                                     SS, Phi_i, inv_dt, Mat, D_Vc)
  !===================================================================

    IMPLICIT NONE

    TYPE(element_str),                INTENT(IN)    :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
    REAL,                             INTENT(IN)    :: CFL
    REAL, DIMENSION(:),               INTENT(IN)    :: Phi_tot
    REAL, DIMENSION(:,:,:),           INTENT(IN)    :: JJ_tot
    REAL,                             INTENT(IN)    :: SS
    REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
    REAL,                             INTENT(OUT)   :: inv_dt
    TYPE(Matrice),                    INTENT(INOUT) :: Mat
    REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: D_Vc
    !--------------------------------------------------------

    SELECT CASE(Data%Iflux)

    CASE(LW)
       
       CALL LW_scheme_imp(ele, Vp, Vc, CFL, Phi_tot, JJ_tot, &
                          Phi_i, inv_dt, Mat, D_Vc)

    CASE(LDA)

       CALL ResiduLDA_imp(ele, Vp, Vc, CFL, Phi_tot, JJ_tot, &
                          Phi_i, inv_dt, Mat)
       
    CASE(LF)
       
       CALL LF_scheme_imp(ele, Vp, Vc, CFL, Phi_tot, JJ_tot, &
                          SS, Phi_i, inv_dt, Mat, D_Vc)

    CASE(SUPG)
       
       CALL SUPG_scheme_imp(ele, Vp, Vc, CFL, Phi_i, inv_dt, Mat, D_Vc)

    CASE DEFAULT
       
       WRITE(*,*) 'ERROR: unknown scheme'
       STOP         

    END SELECT

  END SUBROUTINE Distribute_residual_imp
  !=====================================  

  !===========================================================
  SUBROUTINE Distribute_residual_exp(ele, Vp, Vc, dVc, CFL, Phi_tot, &
                                     SS, Phi_i, inv_dt, dt,  D_Vc)
  !===========================================================

    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name ="Distribute_residual_exp"

    TYPE(element_str),                INTENT(IN)  :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)  :: VC
   REAL, DIMENSION(:,:),             INTENT(IN)  :: dVc
    REAL,                             INTENT(IN)  :: CFL
    REAL, DIMENSION(:),               INTENT(IN)  :: Phi_tot
    REAL,                             INTENT(IN)  :: SS
    REAL,                             INTENT(IN)    :: dt
    REAL, DIMENSION(:,:),             INTENT(OUT) :: Phi_i
    REAL,                             INTENT(OUT) :: inv_dt
    REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)  :: D_Vc
    !----------------------------------------------------

    SELECT CASE(Data%Iflux)

    CASE(LW)
       
       CALL LW_scheme_exp(ele, Vp, Vc, CFL, Phi_tot, &
                          Phi_i, inv_dt, D_Vc)

    CASE(LDA)

       CALL ResiduLDA_exp(ele, Vp, Vc, CFL, Phi_tot, Phi_i, inv_dt) 

    CASE(LF)
       
       CALL LF_scheme_exp(ele, Vp, Vc, dVc, CFL, Phi_tot, &
                          SS, Phi_i, inv_dt, dt, D_Vc)

    CASE(SUPG)
       
       CALL SUPG_scheme_exp(ele, Vp, Vc, CFL, Phi_i, inv_dt, D_Vc)

    CASE DEFAULT
       
       WRITE(*,*) 'ERROR: unknown scheme'
       STOP         

    END SELECT

  END SUBROUTINE Distribute_residual_exp
  !=====================================  


END MODULE Num_schemes
