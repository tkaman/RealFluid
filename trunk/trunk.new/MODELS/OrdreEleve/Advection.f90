MODULE advection

  USE Contrib_Eul,       ONLY: Euler_flux, Euler_Jacobian, &
                               Euler_eigenvalues,          &
                               Euler_eigenvectors,         &
                               Euler_cons_variables,       &
                               Euler_prim_variables

  USE Turbulence_models, ONLY: Turbulent, Turb_advection_flux, &
                               Turb_Advection_Jacobian,        &
                               Turb_advection_eigenvalues,     &
                               Turb_advection_eigenvectors,    &
                               Turb_cons_variables,            &
                               Turb_prim_variables,            &
                               Turb_interp_Vp,                 &
                               Turb_mean_state

  USE EOSlaws

  USE LesTypes

  IMPLICIT NONE

CONTAINS

  !=======================================
  SUBROUTINE advection_flux(N_dim, Vp, ff)
  !=======================================
  ! 
  ! Compute the advection flux in the general case
  ! of turbulnet flows. It reduces to the Euler flux
  ! in the laminar or inviscid cases
  !
    IMPLICIT NONE

    INTEGER,              INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:), INTENT(OUT) :: ff
    !----------------------------------------

    CALL Euler_flux(N_dim, Vp, ff)

#ifdef NAVIER_STOKES   
    IF( Turbulent ) THEN
       CALL Turb_advection_flux(Vp, ff) ! ff in-out
    END IF
#endif


  END SUBROUTINE advection_flux
  !============================  

  !===========================================
  SUBROUTINE advection_Jacobian(N_dim, Vp, AA)
  !===========================================

    IMPLICIT NONE

    INTEGER,                INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),     INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:,:), INTENT(OUT) :: AA
    !--------------------------------------------

    CALL Euler_Jacobian(N_dim, Vp, AA)

#ifdef NAVIER_STOKES
    IF(turbulent) THEN
       CALL Turb_Advection_Jacobian(N_dim, Vp, AA) ! AA in-out
    END IF    
#endif

  END SUBROUTINE advection_Jacobian
  !================================

  !===============================================
  SUBROUTINE advection_eigenvalues(Vp, nn, lambda)
  !===============================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vp
    REAL, DIMENSION(:), INTENT(IN)  :: nn
    REAL, DIMENSION(:), INTENT(OUT) :: lambda
    !-----------------------------------------

integer :: i

    CALL Euler_eigenvalues(Vp, nn, lambda)

#ifdef NAVIER_STOKES
    IF( Turbulent ) THEN
       CALL Turb_advection_eigenvalues(Vp, nn, lambda) ! lambda in-out
    END IF
#endif
    
  END SUBROUTINE advection_eigenvalues
  !===================================  

  !==================================================
  SUBROUTINE advection_eigenvectors(Vp, v_nn, RR, LL)
  !==================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:),   INTENT(IN)  :: v_nn
    REAL, DIMENSION(:,:), INTENT(OUT) :: RR
    REAL, DIMENSION(:,:), INTENT(OUT) :: LL    
    !----------------------------------------

    CALL Euler_eigenvectors(Vp, v_nn, RR, LL)

#ifdef NAVIER_STOKES
     IF( Turbulent ) THEN
       CALL Turb_advection_eigenvectors(Vp, v_nn, RR, LL) ! RR, LL in-out
    END IF
#endif
    
  END SUBROUTINE advection_eigenvectors  
  !====================================

  !==============================
  SUBROUTINE Prim_to_Cons(Vp, Vc)
  !==============================
  !
  ! Vp -> Ua
  !
    IMPLICIT NONE

    REAL,DIMENSION(:), INTENT(IN)  :: Vp
    REAL,DIMENSION(:), INTENT(OUT) :: Vc
    !-----------------------------------

    CALL Euler_cons_variables(Vp, Vc)

#ifdef NAVIER_STOKES
    IF(turbulent) CALL Turb_cons_variables(Vp, Vc) ! Vc in-out
#endif
 
  END SUBROUTINE Prim_to_Cons  
  !==========================

  !==============================
  SUBROUTINE Cons_to_Prim(Vc, Vp)
  !==============================
  !
  ! Ua -> Vp
  !
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vc
    REAL, DIMENSION(:), INTENT(OUT) :: Vp   
    !------------------------------------    

    CALL Euler_prim_variables(Vc, Vp)

#ifdef NAVIER_STOKES
    IF(turbulent) CALL Turb_prim_variables(Vc, Vp) ! Vp in-out
#endif

  END SUBROUTINE Cons_to_Prim
  !==========================

  !===================================
  FUNCTION Vp_P(Vp, P) RESULT(Vp_pimp)
  !===================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    REAL,               INTENT(IN) :: P
    
    REAL, DIMENSION(SIZE(Vp)) :: Vp_pimp
    !-----------------------------------

    VP_pimp = Vp ! rho, vel, [turb]

    VP_pimp(i_pre_vp) = P                        ! P* imposed

    !rho et P known
    !Calcul T(rho,P)
    VP_pimp(i_tem_vp) = T__rho_P(VP_pimp(i_rho_vp), VP_pimp(i_pre_vp))
    
    !Calcul e(rho,T)
    VP_pimp(i_eps_vp) = e__rho_T(VP_pimp(i_rho_vp), VP_pimp(i_tem_vp))
    
    !Calcul c(rho,T)
    VP_pimp(i_son_vp) = c__rho_T(VP_pimp(i_rho_vp), VP_pimp(i_tem_vp))

  END FUNCTION Vp_P
  !================
  
  !==========================================
  FUNCTION Interp_Vp(Vp, f_base) RESULT(Vp_o)
  !==========================================
  !
  ! Compute the interpolate state of the primitive
  ! variables. Do not use the directly the interpolation
  ! since that violete the thermodinamics laws
  !
    IMPLICIT NONE

    REAL, DIMENSION(:,:), INTENT(IN) :: Vp
    REAL, DIMENSION(:),   INTENT(IN) :: f_base
    
    REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_o
    !-----------------------------------------

    INTEGER :: i
    !-----------------------------------------

    Vp_o = 0.d0

    ! Density
    Vp_o(i_rho_vp) = SUM( Vp(i_rho_vp, :)*f_base )
    
    ! Velocity
    DO i = i_vi1_vp, i_vid_vp
       Vp_o(i) = SUM( Vp(i, :)*f_base )
    ENDDO

    ! Pressure
    Vp_o(i_pre_vp) = SUM( Vp(i_pre_vp, :)*f_base )
    
    !rho et P known
    !Calcul T(rho,P)
    Vp_o(i_tem_vp) = T__rho_P(Vp_o(i_rho_vp), Vp_o(i_pre_vp))
    
    !Calcul e(rho,T)
    Vp_o(i_eps_vp) = e__rho_T(Vp_o(i_rho_vp), Vp_o(i_tem_vp))
    
    !Calcul c(rho,T)
    Vp_o(i_son_vp) = c__rho_T(Vp_o(i_rho_vp), Vp_o(i_tem_vp))

#ifdef NAVIER_STOKES
    IF(turbulent) CALL Turb_interp_Vp(Vp, f_base, Vp_o) ! Vp_o in-out
#endif

  END FUNCTION Interp_Vp
  !=====================

  !======================================
  FUNCTION Mean_State_Vp(Vp) RESULT(Vp_b)
  !======================================
  !
  ! Compute the average state of the primitive
  ! variables. Do not use the directly the arithmetic
  ! averagere, since that violete the thermodinamics laws
  !
    IMPLICIT NONE

    REAL, DIMENSION(:,:), INTENT(IN) :: Vp

    REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_b
    !-------------------------------------
    
    REAL :: r_N_dofs

    INTEGER :: i
    !--------------------------------------

    r_N_dofs = REAL( SIZE(Vp, 2), 8 )

    Vp_b(i_rho_vp) = SUM( Vp(i_rho_vp, :) ) / r_N_dofs

    DO i = i_vi1_vp, i_vid_vp
       Vp_b(i) = SUM( Vp(i, :) ) / r_N_dofs
    ENDDO

    Vp_b(i_pre_vp) = SUM( Vp(i_pre_vp, :) ) / r_N_dofs

    !rho et P known
    !Calcul T(rho,P)
    Vp_b(i_tem_vp) = T__rho_P(Vp_b(i_rho_vp), Vp_b(i_pre_vp))
    
    !Calcul e(rho,T)
    Vp_b(i_eps_vp) = e__rho_T(Vp_b(i_rho_vp), Vp_b(i_tem_vp))
    
    !Calcul c(rho,T)
    Vp_b(i_son_vp) = c__rho_T(Vp_b(i_rho_vp), Vp_b(i_tem_vp))

#ifdef NAVIER_STOKES
    IF(turbulent) CALL Turb_mean_state(Vp, Vp_b) ! Vp_b in-out
#endif

  END FUNCTION Mean_State_Vp
  !=========================

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TEST %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !----------------------------------------------------------
  function check_eigenstr(N_var, N_dim, Vp, nn) RESULT(idx)
  !----------------------------------------------------------
  !
  ! Function to test the eigen-structure of the advection part
  !
    implicit none

    integer,            intent(in) :: N_var
    integer,            intent(in) :: N_dim
    real, dimension(:), intent(in) :: Vp
    real, dimension(:), intent(in) :: nn

    integer :: idx
    !************************************

    real, dimension(:),     allocatable :: v_nn
    real, dimension(:,:),   allocatable :: RR, LL, II, JJ_n
    real, dimension(:),     allocatable :: lambda
    real, dimension(:,:,:), allocatable :: Jac_A

    real :: err, err_max

    integer :: i, j
    !************************************

    allocate( RR(N_var, N_var),  &
              LL(N_var, N_var),  &
              II(N_var, N_var),  &
            JJ_n(N_var, N_var)   )
  
    allocate( lambda(N_var) )

    allocate( Jac_A(N_var, N_var, N_dim) )
  
    allocate( v_nn(N_dim) )

    idx = 0; err_max = 0.d0

    v_nn = nn / ( SQRT(SUM(nn*nn)) )

    CALL advection_eigenvalues( Vp, v_nn, lambda)
    CALL advection_eigenvectors(Vp, v_nn, RR, LL)

    II = MATMUL( RR, LL )

    DO i = 1, N_var
       DO j = 1, N_var
          IF( ABS(II(i,j)) < 1.0E-15 ) II(i,j) = 0.d0
       ENDDO
    ENDDO

    write(*,*)
    write(*,*) 'Orthogonality RR LL'
    DO i = 1, N_Var
       write(*,'(5(E12.5))') II(i, :)
    ENDDO

    CALL advection_Jacobian(N_dim, Vp, Jac_A)
  
    II = 0.0
    DO i = 1, N_var
       II(i,i) = lambda(i)
    ENDDO

    JJ_n = 0.0
    DO i = 1, N_dim
       JJ_n = JJ_n +  Jac_A(:,:, i)*v_nn(i)
    ENDDO

    err = MAXVAL(  ABS(  JJ_n - MATMUL( RR, MATMUL(II, LL) )  )  )

    err_max = MAX(err, err_max)

    write(*,*)
    write(*,*) 'error JJ - RR*l*LL'
    write(*,*)  err
    write(*,*) '-------------------------'
   
    IF( err > 8.0E-13 ) THEN
       WRITE(*,*) 'WARNING, error too big'
       WRITE(*,*) 'STOP'
       STOP
    ENDIF

    WRITE(*,*) 'Err_max: ', err_max

  end function check_eigenstr
  !-------------------------------------------
  
END MODULE advection
