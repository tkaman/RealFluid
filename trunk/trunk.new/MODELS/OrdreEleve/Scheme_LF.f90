MODULE Scheme_LF

   USE LesTypes
   USE PLinAlg

   USE Advection
   USE Diffusion 
   USE Source

   USE Turbulence_models,   ONLY: Turbulent

   USE PLinAlg
   USE Assemblage

   IMPLICIT NONE

CONTAINS

  !===========================================================
   SUBROUTINE LF_scheme_imp(ele, Vp, Vc, CFL, Phi_tot, JJ_tot, &
                                SS, Phi_i, inv_dt, Mat, Dr_Vc)
  !===========================================================

     IMPLICIT NONE

     TYPE(element_str),                INTENT(IN)    :: ele
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
     REAL,                             INTENT(IN)    :: CFL
     REAL, DIMENSION(:),               INTENT(IN)    :: Phi_tot
     REAL, DIMENSION(:,:,:),           INTENT(IN)    :: JJ_tot
     REAL,                             INTENT(IN)    :: SS
     REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
     REAL,                             INTENT(OUT)   :: inv_dt
     TYPE(Matrice),                    INTENT(INOUT) :: Mat
     REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: Dr_Vc
     !---------------------------------------------------------
 
     REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: Jac_Stab_LW

     REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_b, Vp_q
     REAL, DIMENSION(SIZE(Vc, 1)) :: Vc_b

     REAL, DIMENSION(Data%NVar)   :: SS_q
     !---------------------------------------------------------

     REAL :: d_min_q

     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: Dr_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_i
     REAL, DIMENSION(Data%NDim, Data%NVar) :: temp

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau, Tau_
     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_i
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_k
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: KKD_phi

     REAL, DIMENSION(Data%NVar, Data%Nvar) :: A_alpha

     REAL, DIMENSION(Data%NVar) :: Stab_right

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim, &
                                           Data%NDim) :: KK_q, KK_i

     REAL, DIMENSION(Data%Nvar) :: D_KKD_Vc_q, KK_DVc, DpDVc

     REAL, DIMENSION(Data%NVar) :: lambda
     !---------------------------------------------------

     REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
     REAL, DIMENSION(:,:,:), POINTER :: D_phi_q
     REAL, DIMENSION(:,:),   POINTER :: phi_q
     REAL, DIMENSION(:),     POINTER :: w  
     !----------------------------------------------------

     REAL, DIMENSION(Data%NVar, Data%Nvar) :: M_ik, Diag_i, k_d_d
     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: temp_d

     REAL, DIMENSION(Data%NDim) :: v_nn

     REAL :: alpha, r_N_dim, r_N_dofs, r_N_verts, mod_n, C_Dt, Dt
     REAL :: u_max, c_max, p_max, S_ele, l_max, h_e, alpha_v

     INTEGER :: N_var, N_dim, N_dofs, N_verts
     INTEGER :: i, j, k, id, jd, iq
     !---------------------------------------------------------

     N_var = Data%NVar
     N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

     N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

     N_verts = ele%N_verts; r_N_verts = REAL(N_verts, 8)

     ! Mean state
     Vc_b = 0.d0
     DO i = 1, N_dofs
        Vc_b = Vc_b + Vc(:,i)
     END DO
     Vc_b = Vc_b / r_N_dofs

     Vp_b = Mean_State_Vp(Vp)

     !-----------
     ! LF scheme
     !-----------------------------------
!!$     alpha = 0.d0
!!$     DO i = 1, N_dofs
!!$        alpha = MAX( alpha, &
!!$                     SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) + Vp(i_son_vp, i) )
!!$     ENDDO
!!$     l_max = alpha
!!$
!!$     mod_n = 0.d0
!!$     DO i = 1, SIZE(ele%rd_n, 2)
!!$        mod_n = MAX(mod_n, SQRT(SUM(ele%rd_n(:, i)**2)) )
!!$     ENDDO
!!$
!!$     alpha = r_N_dofs * alpha * mod_n 
!!$
!!$     DO j = 1, N_var
!!$        Phi_i(j, :) = (Phi_tot(j)/r_N_dofs) + &
!!$                      alpha*( Vc(j, :) - Vc_b(j) )
!!$     ENDDO

     Pos = 0.d0; alpha = 0.d0; A_alpha = 0.d0
     DO i = 1, SIZE(ele%rd_n, 2)

        v_nn = ele%rd_n(:, i) / SQRT( SUM(ele%rd_n(:, i)**2) )

        CALL Advection_eigenvalues( Vp_b, v_nn, lambda)
        CALL Advection_eigenvectors(Vp_b, v_nn, RR, LL)

        mod_n = SQRT( SUM(ele%rd_n(:, i)**2) )       

        DO j = 1, N_var
           Pos(j, j) = MAX( Pos(j,j), 0.5d0*ABS(lambda(j))*mod_n ) 
           alpha = MAX( alpha, 0.5d0*ABS(lambda(j))*mod_n )
        ENDDO

     ENDDO
     A_alpha = A_alpha + MATMUL(RR, MATMUL(Pos, LL))
     A_alpha = (1.d0/r_N_dofs)*A_alpha

     DO i = 1, N_dofs
        Phi_i(:, i) = (Phi_tot / r_N_dofs) + &
                      MATMUL( A_alpha, (Vc(:, i) - Vc_b) )
     ENDDO
     !-------------------------------------

     !-------------
     ! Limitiation
     !-------------------------------------------
     CALL Limitation(Vp_b, Phi_i)

     !---------------
     ! Scaling Matrix
     !--------------------------------------
     Tau = Tau_matrix( N_verts, ele%Volume, ele%rd_n, Vp_b )
     !--------------------------------------

     !-------------------
     ! Stabilization term
     !-------------------------------------------
     ALLOCATE( Jac_Stab_LW(N_var, N_var, N_dofs, N_dofs) )
     Jac_stab_LW = 0.d0

     D_phi_k => ele%D_phi_k  
     D_phi_q => ele%D_phi_q
       phi_q => ele%phi_q
           w => ele%w_q
          
     DO iq = 1, ele%N_quad
       
        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )

        D_Vc_q = 0.d0
        DO i = 1, N_dofs
           DO id = 1, N_dim
              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                              D_phi_q(id, i, iq) * Vc(:, i)
           ENDDO
        ENDDO
        
        CALL  Advection_Jacobian( N_dim, Vp_q, AA_q )

        Stab_right = 0.d0
        DO id = 1, N_dim
           Stab_right = Stab_right + &
                        MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )
        ENDDO

#ifdef NAVIER_STOKES
 
        temp = 0.d0
        DO i = 1, N_dofs

           CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_q )

           DO id = 1, N_dim

              KK_DVc = 0.d0
              DO  jd = 1, N_dim
                 KK_DVc = KK_DVc + &
                          MATMUL( KK_q(:,:, id,jd), Dr_Vc(jd, :, i) )
              ENDDO

              temp(id, :) = temp(id, :) + KK_DVc*D_phi_q(id, i, iq)
          
           ENDDO

        ENDDO

        D_KKD_Vc_q = 0.d0
        DO jd = 1, N_dim
           D_KKD_Vc_q = D_KKD_Vc_q + temp(jd, :)
        ENDDO
              
       Stab_right = Stab_right - D_KKD_Vc_q

       ! Source
       IF( turbulent ) THEN

          d_min_q = SUM(ele%d_min * phi_q(:, iq))

          CALL Source_term(Vp_q, D_Vc_q, d_min_q, SS_q)

          Stab_right = Stab_right - SS_q

       ENDIF
            
#endif

        DO i = 1, N_dofs

           Stab_left_i = 0.d0
           DO id = 1, N_dim
              Stab_left_i = Stab_left_i + &
                            AA_q(:,:, id) * D_phi_q(id, i, iq)
           ENDDO

#ifdef NAVIER_STOKES

           temp_d = 0.d0
           DO k = 1, N_dofs

              CALL Diffusion_Jacobian( N_dim, Vp(:,k), KK_i )
              
              DO id = 1, N_dim

                 k_d_d = 0.d0
                 DO jd = 1, N_dim
                    K_d_d = K_d_d + &
                            KK_i(:,:, id,jd) * D_phi_k(jd, i, k)
                 ENDDO

                 temp_d(:,:, id) = temp_d(:,:, id) + &
                                   D_phi_q(id, k, iq) * K_d_d

              ENDDO
              
           ENDDO

           DO id = 1, N_dim
              Stab_left_i = Stab_left_i - temp_d(:,:, id)
           ENDDO

#endif

           Phi_i(:, i) = Phi_i(:, i) +                &
                         w(iq) * MATMUL( Stab_left_i, &
                                         MATMUL(Tau, Stab_right) )

#ifdef NAVIER_STOKES

           !---------------------
           ! F.O.S. Stabilization
           !-----------------------------------------
           CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)
          
           ! Interpolated reconstructed gradient
           Dr_Vc_q = 0.d0
           DO k = 1, N_dofs
              DO id = 1, N_dim
                 Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                                  phi_q(k, iq) * Dr_Vc(id, :, k)
              ENDDO
           ENDDO

           DO id = 1, N_dim

              KKD_phi = 0.d0
              DO jd = 1, N_dim
                 KKD_phi = KKD_phi + &
                           KK_q(:,:, id, jd) * D_phi_q(jd, i, iq)
              ENDDO

              
              Phi_i(:, i) = Phi_i(:, i) +   &
                            w(iq) * MATMUL( KKD_phi, &
                                            D_Vc_q(id, :) - Dr_Vc_q(id, :) )

           ENDDO
           !-----------------------------------------

#endif

           !---------
           ! JACOBIAN
           !-----------------------------------------
           DO k = 1, N_dofs
             
              Stab_left_k = 0.d0
              DO id = 1, N_dim
                 Stab_left_k = Stab_left_k + &
                               AA_q(:,:, id) * D_phi_q(id, k, iq)
              ENDDO


#ifdef NAVIER_STOKES

              temp_d = 0.d0
              DO j = 1, N_dofs

                 CALL Diffusion_Jacobian( N_dim, Vp(:,j), KK_i )

                 DO id = 1, N_dim

                    K_d_d = 0.d0
                    DO jd = 1, N_dim
                       K_d_d = K_d_d + &
                               KK_i(:,:, id,jd) * D_phi_k(jd, k, j)
                    ENDDO

                    temp_d(:,:, id) = temp_d(:,:, id) + &
                                      K_d_d * D_phi_q(id, j, iq)
          
                 ENDDO

              ENDDO

              DO jd = 1, N_dim
                 Stab_left_k = Stab_left_k - temp_d(:,:, jd)
              ENDDO
              
              ! Source Term
              IF( turbulent ) THEN
                 Stab_left_k = Stab_left_k - phi_q(k, iq) * &
                               Jacobian_Source(Vp_q, D_Vc_q, d_min_q)
              ENDIF

#endif


              Jac_stab_LW(:,:, i, k) = Jac_stab_LW(:,:, i, k) +      &
                                       w(iq) * MATMUL( Stab_left_i,  &
                                               MATMUL(Tau, Stab_left_k) )

           ENDDO ! # dofs -> k

        ENDDO ! i <- N_dofs

     ENDDO ! iq <- N_quad
     !--------------------------------------

     NULLIFY( D_phi_k, D_phi_q, phi_q, w )

     !-----------------------
     ! Time stepping ~ |V|/Dt
     !-------------------------------------------
     !inv_dt = alpha

     u_max = 0.d0; c_max = 0.d0
     DO i = 1, N_dofs
        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
        c_max = MAX( c_max, Vp(i_son_vp, i) )
     ENDDO

     p_max = u_max + c_max

     ! Element surface
     S_ele = 0.d0
     DO i = 1, ele%N_faces
        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
     ENDDO
   
     h_e = r_N_dim * ele%Volume/S_ele

#ifdef NAVIER_STOKES
     Dt = h_e / ( p_max + alpha_visc(ele, Vp) )
#else
     Dt = h_e / p_max
#endif

     DT = CFL * Dt

     inv_dt = (ele%volume/r_N_dofs)/Dt


     !//////////////////////////////////////////////////
     !//         Add the local contribution to        //
     !//          the global Jacobian matrix          //
     !//////////////////////////////////////////////////

     !---------------
     ! Diagonal term
     !----------------------------------------------------
     C_Dt = inv_dt !/ CFL

     DO i = 1, N_dofs

        Diag_i = (JJ_tot(:,:, i) / r_N_dofs) + &                  
                  Jac_stab_LW(:,:, i, i) + &
                  A_alpha*(r_N_dofs - 1.d0)/r_N_dofs
                  

        DO j = 1, N_var
           Diag_i(j,j) = Diag_i(j,j) + C_Dt !+ &
                         !alpha*(r_N_dofs - 1.d0)/r_N_dofs
        ENDDO

        CALL Assemblage_AddDiag(Diag_i, ele%NU(i), Mat)

     ENDDO

     !----------------------
     ! Extra-Diagonal terms 
     !---------------------------------------------------
     DO i = 1, N_dofs

        DO k = 1, N_dofs

           IF(k == i) CYCLE

           M_ik = (JJ_tot(:,:, k) / r_N_dofs) + &
                  Jac_stab_LW(:,:, i, k) - &
                  A_alpha/r_N_dofs

           !DO j = 1, N_var
           !   M_ik(j,j) = M_ik(j,j) - (alpha/r_N_dofs)
           !ENDDO

           CALL Assemblage_Add(M_ik, ele%NU(i), ele%NU(k), Mat)

        ENDDO

     ENDDO     

     DEALLOCATE( Jac_Stab_LW )

   END SUBROUTINE LF_scheme_imp
   !===========================


   !======================================================
   SUBROUTINE LF_scheme_exp(ele, Vp, Vc, dVc,  CFL, Phi_tot, SS, &
        Phi_i, inv_dt, dt, Dr_Vc)
     !======================================================
     !> \brief
     ! LXF routine for the RHS in steady computations and  also for explicit
     ! time steping
     ! Because of that, I need to initialise correctly Stab_right, and this is
     ! why I MULTIPLY by the 'time step', in fact its inverse, because for steady
     ! calculation, this number is set to 0. See ModelInterface/Model_Time_step
     !
     IMPLICIT NONE
     CHARACTER(LEN = *), PARAMETER :: mod_name = "OrdreEleve/LF_Scheme_exp"

     TYPE(element_str),                INTENT(IN)    :: ele
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
     REAL, DIMENSION(:,:),             INTENT(IN)    :: dVc
     REAL,                             INTENT(IN)    :: CFL
     REAL, DIMENSION(:),               INTENT(IN)    :: Phi_tot
     REAL,                             INTENT(IN)    :: SS
     REAL,                             INTENT(in)    :: dt
     REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
     REAL,                             INTENT(OUT)   :: inv_dt
     REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: Dr_Vc
     !---------------------------------------------------------

     REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_b, Vp_q
     REAL, DIMENSION(SIZE(Vc, 1)) :: Vc_b

     REAL, DIMENSION(Data%NVar)   :: SS_q
     !---------------------------------------------------------

     REAL :: d_min_q

     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: Dr_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_i
     REAL, DIMENSION(Data%NDim, Data%NVar) :: temp

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau, Tau_
     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_i
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_k
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: KKD_phi

     REAL, DIMENSION(Data%NVar, Data%Nvar) :: A_alpha

     REAL, DIMENSION(Data%NVar) :: Stab_right

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim, &
          Data%NDim) :: KK_q, KK_i

     REAL, DIMENSION(Data%Nvar) :: D_KKD_Vc_q, KK_DVc, DpDVc

     REAL, DIMENSION(Data%NVar) :: lambda
     !---------------------------------------------------

     REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
     REAL, DIMENSION(:,:,:), POINTER :: D_phi_q
     REAL, DIMENSION(:,:),   POINTER :: phi_q
     REAL, DIMENSION(:),     POINTER :: w  
     !----------------------------------------------------

     REAL, DIMENSION(Data%NVar, Data%Nvar) :: k_d_d
     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: temp_d

     REAL, DIMENSION(Data%NDim) :: v_nn

     REAL :: alpha, r_N_dim, r_N_dofs, r_N_verts, mod_n, C_Dt
     REAL :: u_max, c_max, p_max, S_ele, l_max, h_e, alpha_v, dt0, theta

     INTEGER :: N_var, N_dim, N_dofs, N_verts
     INTEGER :: i, j, k, id, jd, iq
     !---------------------------------------------------------

     N_var = Data%NVar
     N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

     N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

     N_verts = ele%N_verts; r_N_verts = REAL(N_verts, 8)

     ! Mean state
     Vc_b = 0.d0
     DO i = 1, N_dofs
        Vc_b = Vc_b + Vc(:,i)
     END DO
     Vc_b = Vc_b / r_N_dofs

     Vp_b = Mean_State_Vp(Vp)

     !-----------
     ! LF scheme
     !-----------------------------------
!!$     alpha = 0.d0
!!$     DO i = 1, N_dofs
!!$        alpha = MAX( alpha, &
!!$                     SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) + Vp(i_son_vp, i) )
!!$     ENDDO
!!$     l_max = alpha
!!$
!!$     mod_n = 0.d0
!!$     DO i = 1, SIZE(ele%rd_n, 2)
!!$        mod_n = MAX(mod_n, SQRT(SUM(ele%rd_n(:, i)**2)) )
!!$     ENDDO
!!$
!!$     alpha = r_N_dofs * alpha * mod_n 
!!$
!!$     DO j = 1, N_var
!!$        Phi_i(j, :) = (Phi_tot(j)/r_N_dofs) + &
!!$                      alpha*( Vc(j, :) - Vc_b(j) )
!!$     ENDDO

     Pos = 0.d0; alpha = 0.d0; A_alpha = 0.d0
     DO i = 1, SIZE(ele%rd_n, 2)

        v_nn = ele%rd_n(:, i) / SQRT( SUM(ele%rd_n(:, i)**2) )

        CALL Advection_eigenvalues( Vp_b, v_nn, lambda)
        CALL Advection_eigenvectors(Vp_b, v_nn, RR, LL)

        mod_n = SQRT( SUM(ele%rd_n(:, i)**2) )                

        DO j = 1, N_var
           Pos(j, j) = MAX( Pos(j,j), 0.5d0*ABS(lambda(j))*mod_n )
           !           alpha = MAX( alpha, 0.5d0*ABS(lambda(j))*mod_n )
        ENDDO

     ENDDO
     A_alpha = A_alpha + MATMUL(RR, MATMUL(Pos, LL))
     A_alpha = (1.d0/r_N_dofs)*A_alpha

     DO i = 1, N_dofs
        Phi_i(:, i) = (Phi_tot / r_N_dofs) + &
             MATMUL( A_alpha, (Vc(:, i) - Vc_b) ) &
             & + (ele%volume/r_N_dofs)* dt * dVc(:,i) ! here is the trick for steady
     ENDDO
     !-------------------------------------

     !-------------
     ! Limitiation
     !-------------------------------------------
     CALL Limitation(Vp_b, Phi_i)

     !---------------
     ! Scaling Matrix
     !--------------------------------------
    DO j=1, N_Var
       lambda(j)= 1.-ABS( SUM( Phi_i(j,:) ) )/( SUM( ABS( Phi_i(j,:) )) +TINY(1.0) ) 
    ENDDO
    theta= minval(lambda(1:N_var))
     Tau = Tau_matrix(N_verts, ele%Volume, ele%rd_n, Vp_b)

!!$     Tau_ = 0.d0
!!$
!!$     !p = REAL(Data%NOrdre,8) - 1.d0
!!$
!!$     DO i = 1, N_Verts!SIZE(ele%rd_n, 2)
!!$       
!!$        v_nn = ele%rd_n(:, i) / SQRT( SUM(ele%rd_n(:, i)**2) )
!!$
!!$        CALL Advection_eigenvalues( Vp_b, v_nn, lambda)
!!$        CALL Advection_eigenvectors(Vp_b, v_nn, RR, LL)
!!$
!!$        mod_n = SQRT( SUM(ele%rd_n(:, i)**2) ) / &
!!$                (r_N_dim * ele%Volume)
!!$
!!$        Pos = 0.d0
!!$        DO j = 1, N_var
!!$           Pos(j, j) = ABS(lambda(j)) * mod_n * 0.5d0 !xxx
!!$           !Pos(j, j) = phip_4(lambda(j), l_max) * mod_n *2.d0
!!$        ENDDO
!!$
!!$        Tau_ = Tau_ + MATMUL(RR, MATMUL(Pos, LL))
!!$        !Tau_ = Tau_ + (1.d0/r_N_dim) *MATMUL(RR, MATMUL(Pos, LL)) ! xxx
!!$
!!$#ifdef NAVIER_STOKES
!!$
!!$        CALL Diffusion_Jacobian(N_dim, Vp_b, KK_q)
!!$
!!$        v_nn = v_nn * mod_n
!!$        
!!$        DO id = 1, N_dim
!!$           DO jd = 1, N_dim
!!$              Tau_ = Tau_ + v_nn(id)*( KK_q(:,:, id, jd)*v_nn(jd) )* p**2
!!$           ENDDO
!!$        ENDDO
!!$
!!$#endif
!!$
!!$     ENDDO
!!$     Tau = InverseLU(Tau_)
     !--------------------------------------

     !-------------------
     ! Stabilization term
     !-------------------------------------------
     D_phi_k => ele%D_phi_k  
     D_phi_q => ele%D_phi_q
     phi_q => ele%phi_q
     w => ele%w_q

     DO iq = 1, ele%N_quad

        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )
              Stab_right = ele%interpolation( dVc , iq ) * dt ! here is the trick for steady
        D_Vc_q = 0.d0
        DO i = 1, N_dofs
           DO id = 1, N_dim
              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                   D_phi_q(id, i, iq) * Vc(:, i)
           ENDDO
        ENDDO

        CALL  Advection_Jacobian( N_dim, Vp_q, AA_q )

        Stab_right = 0.d0
        DO id = 1, N_dim
           Stab_right = Stab_right + &
                MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )
        ENDDO

#ifdef NAVIER_STOKES

        temp = 0.d0
        DO i = 1, N_dofs

           CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_q )

           DO id = 1, N_dim

              KK_DVc = 0.d0
              DO  jd = 1, N_dim
                 KK_DVc = KK_DVc + &
                      MATMUL( KK_q(:,:, id,jd), Dr_Vc(jd, :, i) )
              ENDDO

              temp(id, :) = temp(id, :) + KK_DVc*D_phi_q(id, i, iq)

           ENDDO

        ENDDO

        D_KKD_Vc_q = 0.d0
        DO jd = 1, N_dim
           D_KKD_Vc_q = D_KKD_Vc_q + temp(jd, :)
        ENDDO

        Stab_right = Stab_right - D_KKD_Vc_q

        ! Source
        IF( turbulent ) THEN

           d_min_q = SUM(ele%d_min * phi_q(:, iq))

           CALL Source_term(Vp_q, D_Vc_q, d_min_q, SS_q)

           Stab_right = Stab_right - SS_q

        ENDIF

#endif

        DO i = 1, N_dofs

           Stab_left_i = 0.d0
           DO id = 1, N_dim
              Stab_left_i = Stab_left_i + &
                   AA_q(:,:, id) * D_phi_q(id, i, iq)
           ENDDO

#ifdef NAVIER_STOKES

           temp_d = 0.d0
           DO k = 1, N_dofs

              CALL Diffusion_Jacobian( N_dim, Vp(:,k), KK_i )

              DO id = 1, N_dim

                 k_d_d = 0.d0
                 DO jd = 1, N_dim
                    K_d_d = K_d_d + &
                         KK_i(:,:, id,jd) * D_phi_k(jd, i, k)
                 ENDDO

                 temp_d(:,:, id) = temp_d(:,:, id) + &
                      D_phi_q(id, k, iq) * K_d_d

              ENDDO

           ENDDO

           DO id = 1, N_dim
              Stab_left_i = Stab_left_i - temp_d(:,:, id)
           ENDDO

#endif

           Phi_i(:, i) = Phi_i(:, i) +                &
                w(iq) * MATMUL( Stab_left_i, &
                MATMUL(Tau, Stab_right) )

#ifdef NAVIER_STOKES

           !---------------------
           ! F.O.S. Stabilization
           !-----------------------------------------
           CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)

           ! Interpolated reconstructed gradient
           Dr_Vc_q = 0.d0
           DO k = 1, N_dofs
              DO id = 1, N_dim
                 Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                      phi_q(k, iq) * Dr_Vc(id, :, k)
              ENDDO
           ENDDO

           DO id = 1, N_dim

              KKD_phi = 0.d0
              DO jd = 1, N_dim
                 KKD_phi = KKD_phi + &
                      KK_q(:,:, id, jd) * D_phi_q(jd, i, iq)
              ENDDO


              Phi_i(:, i) = Phi_i(:, i) +   &
                   w(iq) * MATMUL( KKD_phi, &
                   D_Vc_q(id, :) - Dr_Vc_q(id, :) )

           ENDDO
           !-----------------------------------------

#endif

        ENDDO ! i <- N_dofs

     ENDDO ! iq <- N_quad
     !--------------------------------------

     NULLIFY( D_phi_k, D_phi_q, phi_q, w )

     !-----------------------
     ! Time stepping ~ |V|/Dt
     !-------------------------------------------
     !     inv_dt = alpha
     u_max = 0.d0; c_max = 0.d0
     DO i = 1, N_dofs
        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
        c_max = MAX( c_max, Vp(i_son_vp, i) )
     ENDDO

     p_max = u_max + c_max

     ! Element surface
     S_ele = 0.d0
     DO i = 1, ele%N_faces
        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
     ENDDO

     h_e = r_N_dim * ele%Volume/S_ele

#ifdef NAVIER_STOKES
     Dt0 = h_e / ( p_max + alpha_visc(ele, Vp) )
#else
     Dt0 = h_e / p_max
#endif

     !     DT = CFL * Dt

     inv_dt = (ele%volume/r_N_dofs)/Dt0

   END SUBROUTINE LF_scheme_exp
   !===========================

   !===============================
   SUBROUTINE Limitation(Vp, Phi_i)
   !===============================

     IMPLICIT NONE

     REAL, DIMENSION(:) ,  INTENT(IN)    :: Vp
     REAL, DIMENSION(:,:), INTENT(INOUT) :: Phi_i
     !-------------------------------------------
     
     REAL, DIMENSION(SIZE(Phi_i,1), SIZE(Phi_i,1)) :: RR, LL

     REAL                        :: mod_u
     REAL, DIMENSION(Data%NDim)  :: v_nn
     INTEGER                     :: i, j, N_var, N_dofs

     REAL, PARAMETER             :: e_tol = 1.0E-6
     !-------------------------------------------------

     N_var  = SIZE(Phi_i, 1)
     N_dofs = SIZE(Phi_i, 2)

     mod_u = SQRT( SUM(Vp(i_vi1_vp:i_vid_vp)**2) )

     IF (mod_u <= e_tol) THEN
              
       DO j = 1, N_Var
          Phi_i(j, :) = Lim_Roe(Phi_i(j, :))
       END DO
       
    ELSE
       
       v_nn = Vp(i_vi1_vp:i_vid_vp) / mod_u

       CALL Advection_eigenvectors(Vp, v_nn, RR, LL)
       
       DO i = 1, N_dofs
          Phi_i(:, i) = MATMUL(LL, Phi_i(:, i))
       END DO

       DO j = 1, N_var
          Phi_i(j, :) = Lim_Roe( Phi_i(j, :) )
       END DO

       DO i = 1, N_dofs
          Phi_i(:, i) = MATMUL(RR, Phi_i(:, i))
       END DO

    ENDIF

   END SUBROUTINE Limitation   
   !========================

   !====================================
   FUNCTION Lim_Roe(Phi_L) RESULT(Phi_H)
   !====================================
    
    IMPLICIT NONE
    
    REAL, DIMENSION(:), INTENT(IN) :: Phi_L

    REAL, DIMENSION(SIZE(Phi_L)) :: Phi_H
    !--------------------------------------

    REAL, DIMENSION(SIZE(Phi_L)) :: Beta

    REAL :: Phi_t
    !----------------------------------

    Phi_H = 0.d0

    Phi_t = SUM(Phi_L)

    IF (ABS(Phi_t) > 0.d0) THEN

       Beta = MAX( Phi_L/Phi_t, 0.d0 )
       Beta = Beta / SUM(Beta)
       
       Phi_H = Beta * Phi_t 

    END IF

  END FUNCTION Lim_Roe
  !===================

  !===================================
  FUNCTION phip_4(l, l_max) RESULT(l_)
  !===================================
    
    IMPLICIT NONE
    
    REAL, INTENT(IN) :: l
    REAL, INTENT(IN) :: l_max

    REAL :: l_
    !----------------------------------------------

    REAL, PARAMETER :: eps_ = 0.01d0

    REAL :: l_abs, eps_l
    !-----------------------------------------------
       
    eps_l = l_max * eps_
       
    l_abs = ABS(l)
       
    IF ( l_abs >= eps_l ) THEN 

       l_ = 0.5d0 * (l + l_abs)

    ELSE

       l_ = 0.25d0 *( (l + eps_l)**2 )/eps_l

    END IF
           
  END FUNCTION phip_4
  !==================


   !========================================================
   FUNCTION Tau_matrix(N_verts, Vol, rd_n, Vp_b) RESULT(Tau)
   !========================================================

     IMPLICIT NONE

     INTEGER,              INTENT(IN) :: N_verts
     REAL,                 INTENT(IN) :: Vol
     REAL, DIMENSION(:,:), INTENT(IN) :: rd_n
     REAL, DIMENSION(:),   INTENT(IN) :: Vp_b

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau
     !-------------------------------------------

     REAL, DIMENSION(Data%NVar, Data%NVar, Data%NDim, Data%Ndim) :: KK

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau_

     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar) :: lambda

     REAL, DIMENSION(Data%NDim) :: v_nn

     REAL :: mod_n, nu, kt, p, r_N_dim, l_max

     INTEGER :: i, j, id, jd, N_dim, N_var
     !----------------------------------------------------

     N_Var = Data%NVar
     N_dim = Data%NDim; r_N_dim = REAL(N_dim, 8)

     p = REAL(Data%NOrdre,8) -1.d0

     Tau_ = 0.d0
     
     DO i = 1, N_verts !SIZE(ele%rd_n, 2)
            
        v_nn = rd_n(:, i) / SQRT( SUM(rd_n(:, i)**2) )

        CALL Advection_eigenvalues( Vp_b, v_nn, lambda)
        CALL Advection_eigenvectors(Vp_b, v_nn, RR, LL)

        mod_n = SQRT( SUM(rd_n(:, i)**2) ) / (r_N_dim * Vol)

        Pos = 0.d0
        DO j = 1, N_var
           !Pos(j, j) = ABS(lambda(j)) * mod_n *0.5d0 !xxx
           Pos(j, j) = phip_4(lambda(j), l_max) * mod_n * 2.d0
        ENDDO

        Tau_ = Tau_ + MATMUL(RR, MATMUL(Pos, LL))
        !Tau_ = Tau_ + (1.d0/r_N_dim) *MATMUL(RR, MATMUL(Pos, LL)) ! xxx

#ifdef NAVIER_STOKES

        CALL Diffusion_Jacobian(N_dim, Vp_b, KK)

        v_nn = v_nn * mod_n
        
        DO id = 1, N_dim
           DO jd = 1, N_dim
              Tau_ = Tau_ + v_nn(id)*( KK(:,:, id, jd)*v_nn(jd) )* p**2
           ENDDO
        ENDDO

#endif
        
     ENDDO

     Tau = InverseLU(Tau_)

   END FUNCTION Tau_matrix
   !======================

END MODULE Scheme_LF
