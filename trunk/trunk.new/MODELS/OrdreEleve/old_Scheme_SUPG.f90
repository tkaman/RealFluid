MODULE Scheme_SUPG

   USE LesTypes
   USE PLinAlg

   USE Advection
   USE Diffusion 
   USE Source
   USE EOSLaws
   USE Visc_Laws
   USE Turbulence_models,   ONLY: Turbulent

   USE PLinAlg
   USE Assemblage

  IMPLICIT NONE

CONTAINS

   !======================================================================
   SUBROUTINE SUPG_scheme_imp(ele, Vp, Vc, CFL, Phi_i, inv_dt, Mat, Dr_Vc)
   !======================================================================

     IMPLICIT NONE

     TYPE(element_str),                INTENT(IN)    :: ele
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
     REAL,                             INTENT(IN)    :: CFL
     REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
     REAL,                             INTENT(OUT)   :: inv_dt
     TYPE(Matrice),                    INTENT(INOUT) :: Mat
     REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: Dr_Vc
     !---------------------------------------------------------

     REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: Jac_c, Jac_s

     REAL, DIMENSION(SIZE(Vp, 1) ) :: Vp_b, Vp_q
     REAL, DIMENSION(SIZE(Vc, 1) ) :: Vc_q
     REAL, DIMENSION(Data%NVar)    :: S_q

     REAL, DIMENSION(Data%NVar, Data%NDim) :: fv_q
     REAL, DIMENSION(Data%NVar)            :: A_Fa, D_Fv, S_t

     REAL :: d_min_q

     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_i, FV_i_D
     REAL, DIMENSION(Data%NDim, Data%NVar) :: Dr_Vc_q

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau
     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_i
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_k
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: KKD_phi

     REAL, DIMENSION(Data%NVar) :: Stab_right

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q, KK_d_D 
     REAL, DIMENSION(Data%NVar, Data%Nvar)            :: D_J, KK_d

     REAL, DIMENSION(Data%NVar, Data%Nvar, &
                     Data%NDim, Data%NDim) :: KK_q, KK_i


     REAL, DIMENSION(Data%NDim, Data%NDim, Data%NVar) :: nu_sc_q

     REAL, DIMENSION(Data%Nvar) :: D_KKDVc_q, FV_i_id
     !----------------------------------------------------

     REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
     REAL, DIMENSION(:,:,:), POINTER :: D_phi_q
     REAL, DIMENSION(:,:),   POINTER :: phi_q
     REAL, DIMENSION(:),     POINTER :: w  
     !----------------------------------------------------

     REAL, DIMENSION(Data%NVar, Data%Nvar) :: M_ik, Diag_i

     REAL, DIMENSION(Data%NDim) :: v_nn

     REAL :: r_N_dim, r_N_dofs, C_Dt
     REAL :: u_max, c_max, p_max, S_ele, h_e, Dt
     INTEGER :: N_var, N_dim, N_dofs, N_verts, N_quad
     INTEGER :: i, j, iq, id, jd, k
     !----------------------------------------------------

     N_var = Data%NVar
     N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

     N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

     N_verts = ele%N_verts

     N_quad = ele%N_quad

     D_phi_k => ele%D_phi_k 
     D_phi_q => ele%D_phi_q
       phi_q => ele%phi_q
           w => ele%w_q

     ALLOCATE( Jac_c(N_var, N_var, N_dofs, N_dofs) )
     ALLOCATE( Jac_s(N_var, N_var, N_dofs, N_dofs) )

     Vp_b = Mean_state_Vp(Vp)

     Jac_c = 0.d0; Jac_s = 0.d0;  Phi_i = 0.d0

     !----------------------
     ! Central contribution
     !-------------------------------------     
     DO iq = 1, N_quad
        
        ! Interpolated solution
        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )

        ! FEM Gradient
        D_Vc_q = 0.d0
        DO k = 1, N_dofs
           DO id = 1, N_dim
              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                              D_phi_q(id, k, iq) * Vc(:, k)
           ENDDO
        ENDDO

        ! Reconstructed Gradient
#ifdef NAVIER_STOKES
        Dr_Vc_q = 0.d0
        DO k = 1, N_dofs
           DO id = 1, N_dim
              Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                               phi_q(k, iq) * Dr_Vc(id, :, k)
           ENDDO
        ENDDO
#endif

        ! Advective flux & Jacobian 
        CALL advection_Jacobian(N_dim, Vp_q, AA_q)

        ! Viscous flux & Jacobian 
#ifdef NAVIER_STOKES
        CALL diffusion_flux(N_dim, Vp_q, D_Vc_q, fv_q)
        CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)

        ! Turbulent source term
        IF( Turbulent ) THEN 
           d_min_q = SUM(ele%d_min * phi_q(:, iq))
           CALL Source_term(Vp_q, D_Vc_q, d_min_q, S_q)
        ENDIF
#endif

        ! For each DOF i...
        DO i = 1, N_dofs

           A_Fa = 0.d0;  D_Fv = 0.d0; S_t = 0.d0
           DO id = 1, N_dim

              A_Fa = A_Fa + phi_q(i, iq) * &
                            MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )

#ifdef NAVIER_STOKES
              D_Fv = D_Fv + D_phi_q(id, i, iq) * fv_q(:, id)
#endif

           ENDDO

#ifdef NAVIER_STOKES
           IF( Turbulent ) S_t = phi_q(i, iq) * S_q
#endif
           
           Phi_i(:, i) = Phi_i(:, i) +  w(iq) * (A_Fa + D_Fv - S_t)

           ! SUPG Jacobian
           DO k = 1, N_dofs

              D_J = 0.d0
              DO id = 1, N_dim
                 D_J = D_J + AA_q(:,:, id) * D_phi_q(id, k, iq) 
              ENDDO
              D_J = D_J * phi_q(i, iq)

              KK_d = 0.0
#ifdef NAVIER_STOKES
              DO id = 1, N_dim
                 DO jd = 1, N_dim
                    KK_d = kK_d + &
                            KK_q(:,:, id, jd)* D_phi_q(jd, k, iq) * &
                                               D_phi_q(id, i, iq)
                 ENDDO
              ENDDO              
#endif

              Jac_c(:,:, i, k) = Jac_c(:,:, i, k) + &
                                 w(iq) * (D_J + KK_d)

           ENDDO 

        ENDDO ! i <= # Dofs

     ENDDO ! iq <= # Quad
     !-------------------------------------

     !-------------------
     ! Stabilization term
     !--------------------------------------         
     DO iq = 1, N_quad

        ! Solution and Gradient at quadrature point
        !------------------------------------------
        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )

        DO k = 1, N_var
           Vc_q(k) = SUM( Vc(k, :) * phi_q(:, iq) )
        ENDDO

        ! FEM Gradient
        D_Vc_q = 0.d0
        DO i = 1, N_dofs
           DO id = 1, N_dim
              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                              D_phi_q(id, i, iq) * Vc(:, i)
           ENDDO
        ENDDO

        ! Reconstructed Gradient
#ifdef NAVIER_STOKES
        Dr_Vc_q = 0.d0
        DO k = 1, N_dofs
           DO id = 1, N_dim
              Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                               phi_q(k, iq) * Dr_Vc(id, :, k)
           ENDDO
        ENDDO
#endif

        CALL Advection_Jacobian(N_dim, Vp_q, AA_q)
#ifdef NAVIER_STOKES
        CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)
#endif
        !------------------------------------------------------

        ! Inviscid contribution
        !------------------------------------------------------
        Stab_right = 0.d0
        DO id = 1, N_dim
           Stab_right = Stab_right + &
                        MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )
        ENDDO

        ! Viscous and Source contributions
        !------------------------------------------------------- 
#ifdef NAVIER_STOKES
        FV_i_D = 0.d0
        DO i = 1, N_dofs

           CALL Diffusion_Jacobian(N_dim, Vp(:,i), KK_i)

           DO id = 1, N_dim

              Fv_i_id = 0.d0
              DO jd = 1, N_dim
                 FV_i_id = FV_i_id + &
                           MATMUL( KK_i(:,:, id,jd), Dr_Vc(jd, :, i) )
              ENDDO

              FV_i_D(id, :) = FV_i_D(id, :) + FV_i_id*D_phi_q(id, i, iq)
          
           ENDDO

        ENDDO

        D_KKDVc_q = 0.d0
        DO jd = 1, N_dim
           D_KKDVc_q = D_KKDVc_q + FV_i_D(jd, :)
        ENDDO 

        Stab_right = Stab_right - D_KKDVc_q

        ! Source
        IF( turbulent ) THEN

           d_min_q = SUM(ele%d_min * phi_q(:, iq))

           CALL Source_term(Vp_q, D_Vc_q, d_min_q, S_q)

           Stab_right = Stab_right - S_q

        ENDIF
#endif
        !--------------------------------------------------------------------

        ! Stabilization matrix
        !!Tau = Tau_matrix( D_phi_q(:,:, iq), Vp_q ) ! ****
        Tau = Tau_matrix( D_phi_q(:,:, iq), Vp_b )

        nu_sc_q = Shock_viscosity(ele, Vp_q, D_Vc_q, Stab_right)

        DO i = 1, N_dofs

           ! Inviscid contribution
           !--------------------------------------------------
           Stab_left_i = 0.d0
           DO id = 1, N_dim
              Stab_left_i = Stab_left_i + &
                            AA_q(:,:, id) * D_phi_q(id, i, iq)
           ENDDO
           !---------------------------------------------------
       
           Phi_i(:, i) = Phi_i(:, i) +                &
                         w(iq) * MATMUL( Stab_left_i, &
                                         MATMUL(Tau, Stab_right) )

           !---------------------
           ! F.O.S. Stabilization
           !-----------------------------------------
#ifdef NAVIER_STOKES
           DO id = 1, N_dim

              KKD_phi = 0.d0
              DO jd = 1, N_dim
                 KKD_phi = KKD_phi + &
                           KK_q(:,:, id, jd) * D_phi_q(jd, i, iq)
              ENDDO

              Phi_i(:, i) = Phi_i(:, i) +            &
                            w(iq) * MATMUL( KKD_phi, &
                                            D_Vc_q(id, :) - Dr_Vc_q(id, :) )

           ENDDO
#endif
           !-----------------------------------------

           !-----------------
           ! Shock-capturing
           !------------------------------------------
           FV_i_id = 0.d0
           DO id = 1, N_dim
              DO jd = 1, N_dim 
                 FV_i_id = FV_i_id + &
                           D_phi_q(id, i, iq) * D_Vc_q(jd, :) * nu_sc_q(id, jd, :)
              ENDDO
           ENDDO
      
           Phi_i(:,i) = Phi_i(:, i) + w(iq) * FV_i_id
           !------------------------------------------

           !----------------
           ! Jacobian scheme
           !-------------------------------------------------------
           DO k = 1, N_dofs

              ! Inviscid contribution
              !-----------------------------------------------------
              D_J = 0.d0
              DO id = 1, N_dim
                 D_J = D_J + &
                       AA_q(:,:, id) * D_phi_q(id, k, iq)
              ENDDO

              Stab_left_k = D_J

              ! Viscous and source contributions
              !-----------------------------------------------------
#ifdef NAVIER_STOKES
              KK_d_D = 0.d0
              DO j = 1, N_dofs

                 CALL Diffusion_Jacobian(N_dim, Vp(:,j), KK_i)

                 DO id = 1, N_dim

                    KK_d = 0.d0
                    DO jd = 1, N_dim
                       KK_d = KK_d + &
                              KK_i(:,:, id,jd) * D_phi_k(jd, k, j)
                    ENDDO

                    KK_d_D(:,:, id) = KK_d_D(:,:, id) + &
                                      KK_d * D_phi_q(id, j, iq)
          
                 ENDDO

              ENDDO
              
              !Stab_left_k = D_J
              DO jd = 1, N_dim
                 Stab_left_k = Stab_left_k - KK_d_D(:,:, jd)
              ENDDO

              ! Source Term
              IF( turbulent ) THEN
                 Stab_left_k = Stab_left_k -   &
                                phi_q(k, iq) * &
                                Jacobian_Source(Vp_q, D_Vc_q, d_min_q)
              ENDIF
#endif
              !--------------------------------------------------------
            
              Jac_s(:,:, i, k) = Jac_s(:,:, i, k) +            &
                                 w(iq) * MATMUL( Stab_left_i,  &
                                                 MATMUL(Tau, Stab_left_k) )

              ! Shock capturing
              !------------------------------------------------
              D_J = 0.d0
              DO id = 1, N_dim
                 DO jd = 1, N_dim
                    DO j = 1, N_var
                       D_J(j,j) = D_J(j,j) + &
                                  D_phi_q(id, i, iq) * &
                                  D_phi_q(jd, k, iq) * &
                                  nu_sc_q(id, jd, j)
                    ENDDO
                 ENDDO
              ENDDO

              Jac_s(:,:, i, k) = Jac_s(:,:, i, k) + w(iq)*D_J
              !------------------------------------------------

           ENDDO ! # dofs -> k

        ENDDO ! i <- N_dofs

     ENDDO ! iq <- N_quad
     !--------------------------------------

     NULLIFY( D_phi_k, D_phi_q, phi_q, w )

     !-----------------------
     ! Time stepping ~ |V|/Dt
     !-------------------------------------------
     u_max = 0.d0; c_max = 0.d0
     DO i = 1, N_dofs
        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
        c_max = MAX( c_max, Vp(i_son_vp, i) )
     ENDDO

     p_max = u_max + c_max

     ! Element surface
     S_ele = 0.d0
     DO i = 1, ele%N_faces
        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
     ENDDO

     h_e = r_N_dim * ele%Volume/S_ele

#ifdef NAVIER_STOKES
     Dt = h_e / ( p_max + alpha_visc(ele, Vp) )
#else
     Dt = h_e /   p_max
#endif

     Dt = CFL * Dt

     inv_dt = (ele%Volume/r_N_dofs)/Dt 
!     inv_dt = ele%Volume/Dt 

     !//////////////////////////////////////////////////
     !//         Add the local contribution to        //
     !//          the global Jacobian matrix          //
     !//////////////////////////////////////////////////

     !---------------
     ! Diagonal term
     !----------------------------------------------------
     DO i = 1, N_dofs

        Diag_i = Jac_c(:,:, i, i) + Jac_s(:,:, i, i)

        DO j = 1, N_var
           Diag_i(j,j) = Diag_i(j,j) + inv_dt
        ENDDO

        CALL Assemblage_AddDiag(Diag_i, ele%NU(i), Mat)

     ENDDO

     !----------------------
     ! Extra-Diagonal terms 
     !---------------------------------------------------
     DO i = 1, N_dofs

        DO k = 1, N_dofs

           IF(k == i) CYCLE

           M_ik = Jac_c(:,:, i, k) + Jac_s(:,:, i, k)

           CALL Assemblage_Add(M_ik, ele%NU(i), ele%NU(k), Mat)

        ENDDO

     ENDDO     

     DEALLOCATE( Jac_c, Jac_s )

   END SUBROUTINE SUPG_scheme_imp   
   !=============================

   !=================================================================
   SUBROUTINE SUPG_scheme_exp(ele, Vp, Vc, CFL, Phi_i, inv_dt, Dr_Vc)
   !=================================================================

     IMPLICIT NONE

     TYPE(element_str),                INTENT(IN)  :: ele
     REAL, DIMENSION(:,:),             INTENT(IN)  :: Vp
     REAL, DIMENSION(:,:),             INTENT(IN)  :: VC
     REAL,                             INTENT(IN)  :: CFL
     REAL, DIMENSION(:,:),             INTENT(OUT) :: Phi_i
     REAL,                             INTENT(OUT) :: inv_dt
     REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)  :: Dr_Vc
     !-----------------------------------------------------

     REAL, DIMENSION(SIZE(Vp, 1) ) :: Vp_b, Vp_q
     REAL, DIMENSION(SIZE(Vc, 1) ) :: Vc_q
     REAL, DIMENSION(Data%NVar)    :: S_q

     REAL, DIMENSION(Data%NVar, Data%NDim) :: fv_q
     REAL, DIMENSION(Data%NVar)            :: A_Fa, D_Fv, S_t

     REAL :: d_min_q

     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_i, FV_i_D
     REAL, DIMENSION(Data%NDim, Data%NVar) :: Dr_Vc_q

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau
     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_i
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: KKD_phi

     REAL, DIMENSION(Data%NVar) :: Stab_right

     REAL, DIMENSION(Data%NVar, Data%Nvar) :: K_sc     

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q

     REAL, DIMENSION(Data%NVar, Data%Nvar, &
                     Data%NDim, Data%NDim) :: KK_q, KK_i

     REAL, DIMENSION(Data%NDim, Data%NDim, Data%NVar) :: nu_sc_q     

     REAL, DIMENSION(Data%Nvar) :: D_KKDVc_q, FV_i_id
     !----------------------------------------------------

     REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
     REAL, DIMENSION(:,:,:), POINTER :: D_phi_q
     REAL, DIMENSION(:,:),   POINTER :: phi_q
     REAL, DIMENSION(:),     POINTER :: w  
     !----------------------------------------------------

     REAL :: r_N_dim, r_N_dofs
     REAL :: u_max, c_max, p_max, S_ele, h_e, Dt
     INTEGER :: N_var, N_dim, N_dofs, N_verts, N_quad
     INTEGER :: i, j, iq, id, jd, k
     !----------------------------------------------------
  
     N_var = Data%NVar
     N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

     N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

     N_verts = ele%N_verts

     N_quad = ele%N_quad

     D_phi_k => ele%D_phi_k  
     D_phi_q => ele%D_phi_q
       phi_q => ele%phi_q
           w => ele%w_q

     Vp_b = Mean_state_Vp(Vp)

     Phi_i = 0.d0

     !----------------------
     ! Central contribution
     !-------------------------------------
     DO iq = 1, N_quad
        
        ! Interpolated solution
        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )

        ! FEM Gradient
        D_Vc_q = 0.d0
        DO k = 1, N_dofs
           DO id = 1, N_dim
              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                              D_phi_q(id, k, iq) * Vc(:, k)
           ENDDO
        ENDDO

        ! Reconstructed Gradient
#ifdef NAVIER_STOKES
        Dr_Vc_q = 0.d0
        DO k = 1, N_dofs
           DO id = 1, N_dim
              Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                               phi_q(k, iq) * Dr_Vc(id, :, k)
           ENDDO
        ENDDO
#endif

        ! Advective flux & Jacobian 
        CALL advection_Jacobian(N_dim, Vp_q, AA_q)

        ! Viscous flux
#ifdef NAVIER_STOKES
        CALL diffusion_flux( N_dim, Vp_q, D_Vc_q, fv_q )

        ! Turbulent source term
        IF( Turbulent ) THEN 
           d_min_q = SUM(ele%d_min * phi_q(:, iq))
           CALL Source_term(Vp_q, D_Vc_q, d_min_q, S_q)
        ENDIF
#endif

        ! For each DOF i...
        DO i = 1, N_dofs
           
           A_Fa = 0.d0;  D_Fv = 0.d0; S_t = 0.d0
           DO id = 1, N_dim

              A_Fa = A_Fa + phi_q(i, iq) * &
                            MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )

#ifdef NAVIER_STOKES
              D_Fv = D_Fv + D_phi_q(id, i, iq) * fv_q(:, id)
#endif

           ENDDO

#ifdef NAVIER_STOKES
           IF( Turbulent ) S_t = phi_q(i, iq) * S_q
#endif

           Phi_i(:, i) = Phi_i(:, i) +  w(iq) * (A_Fa + D_Fv - S_t)

        ENDDO ! i <= # Dofs

     ENDDO ! iq <= # Quad
     !-------------------------------------

     !-------------------
     ! Stabilization term
     !--------------------------------------
     DO iq = 1, N_quad

        ! Solution and Gradient at quadrature point
        !------------------------------------------       
        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )

        DO k = 1, N_var
           Vc_q(k) = SUM( Vc(k, :) * phi_q(:, iq) )
        ENDDO

        ! FEM Gradient
        D_Vc_q = 0.d0
        DO i = 1, N_dofs
           DO id = 1, N_dim
              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                              D_phi_q(id, i, iq) * Vc(:, i)
           ENDDO
        ENDDO

        ! Reconstructed Gradient
#ifdef NAVIER_STOKES
        Dr_Vc_q = 0.d0
        DO k = 1, N_dofs
           DO id = 1, N_dim
              Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                               phi_q(k, iq) * Dr_Vc(id, :, k)
           ENDDO
        ENDDO
#endif

        CALL Advection_Jacobian(N_dim, Vp_q, AA_q)
#ifdef NAVIER_STOKES
        CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)
#endif
        !------------------------------------------------------

        ! Inviscid contribution
        !------------------------------------------------------
        Stab_right = 0.d0
        DO id = 1, N_dim
           Stab_right = Stab_right + &
                        MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )
        ENDDO

        ! Viscous and Source contributions
        !-------------------------------------------------------
#ifdef NAVIER_STOKES
        FV_i_D = 0.d0
        DO i = 1, N_dofs

           CALL Diffusion_Jacobian(N_dim, Vp(:,i), KK_i) 

           DO id = 1, N_dim

              FV_i_id = 0.d0
              DO jd = 1, N_dim
                 FV_i_id = FV_i_id + &
                          MATMUL( KK_i(:,:, id,jd), Dr_Vc(jd, :, i) )
              ENDDO

              FV_i_D(id, :) = FV_i_D(id, :) + FV_i_id*D_phi_q(id, i, iq)
          
           ENDDO

        ENDDO

        D_KKDVc_q = 0.d0
        DO jd = 1, N_dim
           D_KKDVc_q = D_KKDVc_q + FV_i_D(jd, :)
        ENDDO 

        Stab_right = Stab_right - D_KKDVc_q

        ! Source
        IF( turbulent ) THEN

           d_min_q = SUM(ele%d_min * phi_q(:, iq))

           CALL Source_term(Vp_q, D_Vc_q, d_min_q, S_q)

           Stab_right = Stab_right - S_q

        ENDIF
#endif
        !--------------------------------------------------------------------

        ! Stabilization matrix
        !!Tau = Tau_matrix( D_phi_q(:,:, iq), Vp_q ) ! ****
        Tau = Tau_matrix( D_phi_q(:,:, iq), Vp_b )

        nu_sc_q = Shock_viscosity(ele, Vp_q, D_Vc_q, Stab_right) 

        DO i = 1, N_dofs

           ! Inviscid contribution
           !--------------------------------------------------
           Stab_left_i = 0.d0
           DO id = 1, N_dim
              Stab_left_i = Stab_left_i + &
                            AA_q(:,:, id) * D_phi_q(id, i, iq)
           ENDDO
           !---------------------------------------------------

           Phi_i(:, i) = Phi_i(:, i) +                &
                         w(iq) * MATMUL( Stab_left_i, &
                                         MATMUL(Tau, Stab_right) )

           !---------------------
           ! F.O.S. Stabilization
           !-----------------------------------------
#ifdef NAVIER_STOKES
           DO id = 1, N_dim

              KKD_phi = 0.d0
              DO jd = 1, N_dim
                 KKD_phi = KKD_phi + &
                           KK_q(:,:, id, jd) * D_phi_q(jd, i, iq)
              ENDDO

              Phi_i(:, i) = Phi_i(:, i) +            &
                            w(iq) * MATMUL( KKD_phi, &
                                            D_Vc_q(id, :) - Dr_Vc_q(id, :) )


           ENDDO
#endif
           !-----------------------------------------

           !-----------------
           ! Shock-capturing
           !------------------------------------------
           FV_i_id = 0.d0
           DO id = 1, N_dim
              DO jd = 1, N_dim 
                 FV_i_id = FV_i_id + &
                           D_phi_q(id, i, iq) * D_Vc_q(jd, :) * nu_sc_q(id, jd, :)
              ENDDO
           ENDDO

           Phi_i(:,i) = Phi_i(:, i) + w(iq) * FV_i_id
           !------------------------------------------

        ENDDO ! i <- N_dofs

     ENDDO ! iq <- N_quad
     !--------------------------------------

     NULLIFY( D_phi_k, D_phi_q, phi_q, w )

     !-----------------------
     ! Time stepping ~ |V|/Dt
     !-------------------------------------------
     u_max = 0.d0; c_max = 0.d0
     DO i = 1, N_dofs
        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
        c_max = MAX( c_max, Vp(i_son_vp, i) )
     ENDDO

     p_max = u_max + c_max

     ! Element surface
     S_ele = 0.d0
     DO i = 1, ele%N_faces
        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
     ENDDO

     h_e = r_N_dim * ele%Volume/S_ele

#ifdef NAVIER_STOKES
     Dt = h_e / ( p_max + alpha_visc(ele, Vp) )
#else
     Dt = h_e /   p_max
#endif
    
!!$     inv_dt = p_max*S_ele + alpha_visc(ele, Vp)
!!$
!!$!     inv_dt = inv_dt * tau_param * r_N_Dim
!!$     inv_dt = inv_dt * tau_param / r_N_Dim

!     Dt = CFL * Dt

   END SUBROUTINE SUPG_scheme_exp   
   !=============================

   !=============================================
   FUNCTION Tau_matrix(D_phi_q, Vp_q) RESULT(Tau)
   !=============================================

     IMPLICIT NONE
     
     REAL, DIMENSION(:,:), INTENT(IN) :: D_phi_q
     REAL, DIMENSION(:),   INTENT(IN) :: Vp_q

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau
     !-------------------------------------------

     REAL, DIMENSION(Data%NVar, Data%NVar, Data%NDim, Data%Ndim) :: KK

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau_
     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar) :: lambda

     REAL, DIMENSION(Data%NDim) :: v_nn

     REAL :: mod_n, p

     INTEGER :: i, j, id, jd, N_dofs, N_Var, N_dim
     !--------------------------------------------

     Tau_ = 0.d0

     N_dofs = SIZE(D_phi_q, 2)
     N_Var = Data%NVar
     N_dim = Data%NDim

     p = REAL(Data%NOrdre - 1.d0, 8)

     DO i = 1, N_dofs

        v_nn = D_Phi_q(:, i)
        
        mod_n = SQRT( SUM(v_nn**2) )

        v_nn = ( v_nn /  mod_n )

        CALL Advection_eigenvalues( Vp_q, v_nn, lambda)
        CALL Advection_eigenvectors(Vp_q, v_nn, RR, LL)

        Pos = 0.d0
        DO j = 1, N_var
           Pos(j,j) = ABS(lambda(j)) * mod_n

           ! Entropy fix-like correction.
           ! The matrix is ill conditioned
           ! when only 1 eigenval. is /= 0
           !Pos(j,j) = MAX( Pos(j,j), 0.01d0 )
        ENDDO
        
        Tau_ = Tau_ +  MATMUL(RR, MATMUL(Pos, LL))

        v_nn = v_nn * mod_n !* p

#ifdef NAVIER_STOKES
        CALL Diffusion_Jacobian(N_dim, Vp_q, KK)

        DO id = 1, N_dim
           DO jd = 1, N_dim
              !Tau_ = Tau_ + v_nn(id) * KK(:,:, id, jd) * v_nn(jd)
              Tau_ = Tau_ + v_nn(id) * KK(:,:, id, jd) * v_nn(jd) * p**2 
           ENDDO
        ENDDO
#endif

     ENDDO

     Tau = InverseLU(Tau_)

   END FUNCTION Tau_matrix
   !======================

   !=======================================================
   FUNCTION Tau_matrix_SUPG(Vp, phi_q, D_phi_q) RESULT(Tau)
   !=======================================================

     IMPLICIT NONE

     REAL, DIMENSION(:,:), INTENT(IN) :: Vp
     REAL, DIMENSION(:),   INTENT(IN) :: phi_q
     REAL, DIMENSION(:,:), INTENT(IN) :: D_phi_q

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau
     !-------------------------------------------

     REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_q

     REAL, DIMENSION(Data%NDim) :: j_u, uu

     REAL :: tau_1, tau_3_u, tau_3_e, tau_rho, tau_u, tau_e
     REAL :: c, nu, kt, h_u, mod_uu, Cv

     REAL, PARAMETER :: eps = 1.0E-15

     INTEGER :: i, id, N_dim, N_dof
     !-------------------------------------------
     
     N_dim = Data%NDim
     N_dof = SIZE(phi_q)

     DO id = 1, N_dim
        uu(id) = SUM( Vp(id+1, :) * phi_q )
     ENDDO
     
     mod_uu = SQRT(SUM(uu**2))

     c = SUM( Vp(i_son_vp, :) * phi_q )

     j_u = (uu + eps) / ( mod_uu + eps )

     h_u = 0.d0
     DO i = 1, N_dof
        h_u = h_u + ABS( DOT_PRODUCT(j_u, D_phi_q(:, i)) )
     ENDDO

     h_u = 2.d0 / h_u

     tau_1 = ( 2.d0 * (mod_uu + c) / h_u )**2

#ifdef NAVIER_STOKES
     Vp_q = Interp_Vp( Vp, phi_q )

     nu =  Viscosity(Vp_q) / Vp_q(i_rho_vp)

     Cv = EOSCv__T_rho( Vp_q(i_rho_vp), Vp_q(i_tem_vp) )

     kt = Thermal_conductivity(Vp_q) / ( Vp_q(i_rho_vp) * Cv )

     !!nu = nu * (M_oo/Re_oo)
     !!kt = kt * (M_oo/Re_oo)

     tau_3_u = ( 4.d0 * nu / (h_u**2) )**2

     tau_3_e = ( 4.d0 * kt / (h_u**2) )**2
#endif

#ifdef NAVIER_STOKES
     tau_rho = ( tau_1           )**(-1.d0/2.d0)
     tau_u   = ( tau_1 + tau_3_u )**(-1.d0/2.d0)
     tau_e   = ( tau_1 + tau_3_e )**(-1.d0/2.d0)
#else
     tau_rho = ( tau_1           )**(-1.d0/2.d0)
     tau_u   = ( tau_1           )**(-1.d0/2.d0)
     tau_e   = ( tau_1           )**(-1.d0/2.d0)
#endif
     
     Tau = 0.d0
     Tau(i_rho_ua, i_rho_ua) = tau_rho
     Tau(i_rv1_ua, i_rv1_ua) = tau_u 
     Tau(i_rv2_ua, i_rv2_ua) = tau_u
     Tau(i_rvd_ua, i_rvd_ua) = tau_u  
     Tau(i_ren_ua, i_ren_ua) = tau_e

   END FUNCTION Tau_matrix_SUPG
   !===========================
!!$
!!$   !=======================================================
!!$   FUNCTION delta_shock(ele, Vp_q, D_Vc_q, R) RESULT(delta)
!!$   !=======================================================
!!$
!!$     IMPLICIT NONE
!!$
!!$     TYPE(element_str),    INTENT(IN) :: ele
!!$     REAL, DIMENSION(:),   INTENT(IN) :: Vp_q
!!$     REAL, DIMENSION(:,:), INTENT(IN) :: D_Vc_q
!!$     REAL, DIMENSION(:),   INTENT(IN) :: R
!!$
!!$     REAL :: delta
!!$     !------------------------------------------------
!!$
!!$     REAL, DIMENSION(Data%NVar, Data%NVar, Data%NDim) :: AA
!!$     REAL, DIMENSION(Data%NVar, Data%NVar) :: AA_0
!!$
!!$     REAL, DIMENSION(Data%NVar) :: Z
!!$
!!$     REAL :: num, den
!!$
!!$     INTEGER :: N_dofs, N_dim, l, id, jd
!!$     !------------------------------------------------
!!$
!!$     N_dofs = ele%N_points
!!$     N_dim = Data%NDim
!!$
!!$     DO l = 1, N_dofs
!!$
!!$        CALL advection_Jacobian(N_dim, Vp_q, AA)
!!$        CALL   Symmetric__dV_dU(N_dim, Vp_q, AA_0)
!!$
!!$        num =  DOT_PRODUCT(R, MATMUL(AA_0, R))
!!$          
!!$        den = 0.d0
!!$        DO id = 1, N_dim
!!$
!!$           Z = 0.d0
!!$           DO jd = 1, N_dim
!!$              Z = Z + ele%GG(jd, id, l)*D_Vc_q(jd, :)
!!$           ENDDO
!!$
!!$           den = den + DOT_PRODUCT(Z, MATMUL(AA_0, Z ))
!!$
!!$        ENDDO
!!$        den = den + 1.0E-13
!!$         
!!$        delta = SQRT(num/den)
!!$
!!$     ENDDO
!!$
!!$   END FUNCTION delta_shock
!!$   !=======================

   !=======================================================
   FUNCTION Shock_viscosity(ele, Vp, D_Vc, R) RESULT(nu_sc)
   !=======================================================

     IMPLICIT NONE

     TYPE(element_str),    INTENT(IN) :: ele
     REAL, DIMENSION(:),   INTENT(IN) :: Vp
     REAL, DIMENSION(:,:), INTENT(IN) :: D_Vc
     REAL, DIMENSION(:),   INTENT(IN) :: R

     REAL, DIMENSION(Data%NDim, Data%NDim, Data%NVar) :: nu_sc
     !------------------------------------------------

     REAL, DIMENSION(Data%NVar) :: dP_dVc

     REAL, DIMENSION(Data%NDim) :: s, h, D_P 

     REAL :: dP_drho, dP_dEt, R_P, mod_DP, f_p
     REAL :: r_N_dim, den, P_s, d_ij

     INTEGER :: i, j, k, id, jf, m, N_var, N_dim

     REAL, PARAMETER :: C_e = 0.2d0
     !------------------------------------------------

     N_dim = Data%NDim; r_N_dim = REAL(N_dim, 8)
     N_var = Data%NVar
     
     dP_drho = EOS_d_PI_rho(Vp)
     dP_dEt  = EOS_d_PI_E(Vp) 

     dP_dVc = 0.d0
     dP_dVc(i_rho_ua)          =  dP_drho
     dP_dVc(i_rv1_ua:i_rvd_ua) = -dP_dEt * Vp(i_vi1_vp:i_vid_vp)
     dP_dVc(i_ren_ua)          =  dP_dEt

     R_p = DOT_PRODUCT(dP_dVc , R)/Vp(i_pre_vp)

     ! Element lenght
     P_s = 1.d0
     DO id = 1, N_dim

        den = 0.d0
        DO jf = 1, ele%N_faces
           den = den + SUM( ABS(ele%faces(jf)%f%n_q(id, :)) * &
                                ele%faces(jf)%f%w_q )
        ENDDO

        s(id) = 2.d0*ele%volume/den
        
        P_s = P_s * s(id)

     ENDDO

     DO id = 1, N_dim

        h(id) = s(id) * (ele%volume/P_s)**(1.d0/r_N_dim)
        h(id) = h(id) / Data%NOrdre

        D_P(id) = DOT_PRODUCT(dP_dVc, D_Vc(id, :))

     ENDDO
     
     mod_DP = SQRT( SUM(D_P**2) )

     nu_sc = 0.d0

     DO i = 1, N_dim
        DO j = 1, N_dim

           f_p = h(i) * mod_DP / ( Vp(i_pre_vp) + 1.0E-12 )

           d_ij = 0.d0
           IF(j == i) d_ij = 1.d0

           DO m = 1, i_ren_ua ! No contribution to the turbulent equation
              nu_sc(i,j,m) = C_e * (h(i)**2) * f_p * ABS(R_p) * d_ij
              !nu_sc(i,j,m) = C_e * (h(i)**2) *  ABS(R_p) * d_ij
           ENDDO

        ENDDO
     ENDDO

   END FUNCTION Shock_viscosity   
   !===========================

END MODULE Scheme_SUPG
 

