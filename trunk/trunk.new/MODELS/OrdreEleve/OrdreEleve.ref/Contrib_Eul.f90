MODULE Contrib_Eul

  USE LesTypes
  USE EOSlaws

  IMPLICIT NONE

CONTAINS

  !===================================
  SUBROUTINE Euler_flux(N_dim, Vp, ff)
    !===================================
    ! > Compute the flux matrix in one node

    IMPLICIT NONE

    INTEGER,              INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:), INTENT(OUT) :: ff
    !--------------------------------------

    REAL :: e_k, h_t
    REAL, DIMENSION(i_vi1_vp:i_vid_vp) :: II
    INTEGER i
    !--------------------------------------

    ff = 0.d0

    ! Mass conservation law ( 1 equation )
    ff(i_rho_ua, :) = Vp(i_rho_vp)*Vp(i_vi1_vp:i_vid_vp)

    ! Momentum conservation law ( N_dim equations )
    DO i = i_vi1_vp, i_vid_vp

       II = 0.d0; II(i) = 1.d0

       ff(i, :) = Vp(i)*Vp(i_vi1_vp:i_vid_vp)*Vp(i_rho_vp) + &
            II*Vp(i_pre_vp)

    ENDDO

    e_k = 0.5d0*SUM(Vp(i_vi1_vp:i_vid_vp)**2)

    h_t = EOSenthalpy(Vp) + e_k

    ff(i_ren_ua, :) = Vp(i_rho_vp)*h_t*Vp(i_vi1_vp:i_vid_vp)

  END SUBROUTINE Euler_flux
  !========================

  !=======================================
  SUBROUTINE Euler_Jacobian(N_dim, Vp, AA)
    !=======================================
    ! > Compute the component of the Jacobian matrix
    ! >  in one node

    IMPLICIT NONE

    INTEGER,                INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),     INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:,:), INTENT(OUT) :: AA
    !-----------------------------------------

    INTEGER :: p

    REAL :: u, v ,w , e_k, h_t, pi_rho, pi_E
    !----------------------------------------

    AA = 0.0

    u = Vp(i_vi1_vp)
    v = Vp(i_vi2_vp)
    w = VP(i_vid_vp)

    ! Number of Euler's convervative variables
    p = i_ren_ua

    e_k = 0.5d0 * SUM(Vp(i_vi1_vp:i_vid_vp)**2)                     ! > Kinetic Energy

    !EOS
    h_t = EOSenthalpy(Vp) + e_k                                     ! > Total Enthalpy
    pi_rho = EOS_d_PI_rho(Vp)                                       ! > Pressure Derivative // rho
    pi_E   = EOS_d_PI_E(Vp)                                         ! > Pressure Derivative // rho*E

    SELECT CASE(N_dim)

    CASE(2)

       AA(i_rho_ua, 1:p, 1) = (/  0.d0,                   1.d0,                0.d0,               0.d0                /)
       AA(i_rv1_ua, 1:p, 1) = (/  pi_rho - u**2,          (2.d0 - pi_E)*u,    -v*pi_E,             pi_E                /)
       AA(i_rv2_ua, 1:p, 1) = (/ -u*v,                    v,                   u,                  0.d0                /)
       AA(i_ren_ua, 1:p, 1) = (/  u*(pi_rho - h_t),       h_t - pi_E*u**2,    -u*v*pi_E,           u*(1 + pi_E)        /)

       !-----------------------------------------------------------------------------------------------

       AA(i_rho_ua, 1:p, 2) = (/  0.d0,                   0.d0,                1.d0,               0.d0                /)
       AA(i_rv1_ua, 1:p, 2) = (/ -u*v,                    v,                   u,                  0.d0                /)
       AA(i_rv2_ua, 1:p, 2) = (/ -v**2 + pi_rho,         -u*pi_E,              v*(2.d0 - pi_E),    pi_E                /)
       AA(i_ren_ua, 1:p, 2) = (/  v*(pi_rho - h_t),      -u*v*pi_E,            h_t - v**2*pi_E,    v*(1 + pi_E)        /)

    CASE(3)

       AA(i_rho_ua, 1:p, 1) = (/  0.d0,                   1.d0,                0.d0,               0.d0,               0.d0         /)
       AA(i_rv1_ua, 1:p, 1) = (/  pi_rho - u**2,         (2.d0 - pi_E)*u,     -pi_E*v,            -pi_E*w,             pi_E         /)
       AA(i_rv2_ua, 1:p, 1) = (/ -u*v,                    v,                   u,                  0.d0,               0.d0         /) 
       AA(i_rvd_ua, 1:p, 1) = (/ -u*w,                    w,                   0.d0,               u,                  0.d0         /) 
       AA(i_ren_ua, 1:p, 1) = (/  u*(pi_rho - h_t),       h_t - u**2*pi_E,     -u*v*pi_E,         -u*w*pi_E,           u*(1 + pi_E) /)

       !------------------------------------------------------------------------------------------------------------

       AA(i_rho_ua, 1:p, 2) = (/  0.d0,                   0.d0,                1.d0,               0.d0,               0.d0         /)
       AA(i_rv1_ua, 1:p, 2) = (/ -u*v,                    v,                   u,                  0.d0,               0.d0         /) 
       AA(i_rv2_ua, 1:p, 2) = (/  pi_rho - v**2,         -pi_E*u,             (2.d0 - pi_E)*v,    -pi_E*w,             pi_E         /) 
       AA(i_rvd_ua, 1:p, 2) = (/ -w*v,                    0.d0,                w,                  v,                  0.d0         /) 
       AA(i_ren_ua, 1:p, 2) = (/  v*(pi_rho - h_t),      -u*v*pi_E,            h_t - pi_E*v**2,   -w*v*pi_E,           v*(1 + pi_E) /)

       !------------------------------------------------------------------------------------------------------------

       AA(i_rho_ua, 1:p, 3) = (/  0.d0,                   0.d0,                0.d0,               1.d0,               0.d0         /)
       AA(i_rv1_ua, 1:p, 3) = (/ -u*w,                    w,                   0.d0,               u,                  0.d0         /)
       AA(i_rv2_ua, 1:p, 3) = (/ -w*v,                    0.d0,                w,                  v,                  0.d0         /)
       AA(i_rvd_ua, 1:p, 3) = (/  pi_rho - w**2,         -u*pi_E,             -v*pi_E,            (2.d0 - pi_E)*w,     pi_E         /)
       AA(i_ren_ua, 1:p, 3) = (/  w*(pi_rho - h_t),      -u*w*pi_E,           -w*v*pi_E,           h_t - pi_E*w**2,    w*(1 + pi_E) /)

    END SELECT

  END SUBROUTINE Euler_Jacobian
  !============================   

  !===========================================
  SUBROUTINE Euler_eigenvalues(Vp, nn, lambda)
    !===========================================
    ! > Compute the eigenvalues of the Euler equations
    ! > along the direction nn, |nn| = or /= 1
    !
    ! Ordering:
    !
    !  l(1) = u.n-c, l(2) = u.n, l(3) = u.n+c, l(4:Ndim+2) = u.n
    ! 
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vp
    REAL, DIMENSION(:), INTENT(IN)  :: nn
    REAL, DIMENSION(:), INTENT(OUT) :: lambda
    !----------------------------------------

    REAL :: un, c, mod_n
    INTEGER :: Nvar
    !-----------------------------------

    lambda = 0.0

    Nvar = i_ren_ua

    mod_n = DSQRT(SUM(nn*nn))

    un = SUM(Vp(i_vi1_vp:i_vid_vp) * nn)

    c = Vp(i_son_vp)

    lambda(1)      = un - c*mod_n
    lambda(2)      = un
    lambda(3)      = un + c*mod_n
    lambda(4:Nvar) = un

  END SUBROUTINE Euler_eigenvalues
  !===============================

  !==============================================
  SUBROUTINE Euler_eigenvectors(Vp, v_nn, RR, LL)
    !==============================================
    ! > Compute the eigenvectors of the Euler equations
    ! > along the direction nn, |nn| = 1
    ! > Normalization: RR*LL = II
    !
    ! Ordering of corresponding eigenvalues:
    !
    !  l(1) = u.n-c, l(2) = u.n, l(3) = u.n+c, l(4:Ndim+2) = u.n
    !
    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:),   INTENT(IN)  :: v_nn
    REAL, DIMENSION(:,:), INTENT(OUT) :: RR
    REAL, DIMENSION(:,:), INTENT(OUT) :: LL
    !--------------------------------------

    INTEGER :: N_dim, p, d_max

    REAL, DIMENSION(SIZE(v_nn)) :: uu
    REAL, DIMENSION(SIZE(v_nn)) :: nn, nn_p, nn_t

    REAL :: ek, u_n, u_p, u_t, g_1, ht, cc, pi_rho, pi_E, xi, phi, u, v, w, nx, ny, nz
    !--------------------------------------

    N_dim = SIZE(v_nn)

    ! Number of Euler equations
    p = i_ren_ua

    nn = v_nn / DSQRT( SUM(v_nn*v_nn) )                             ! > Unit vector
    uu  = Vp(i_vi1_vp:i_vid_vp)                                     ! > u vector

    u_n = SUM(uu*nn)                                                ! > u.n

    ek = 0.5d0 * SUM(Vp(i_vi1_vp:i_vid_vp)**2)                     ! > Kinetic Energy



    !EOS
    ht = EOSenthalpy(Vp) + ek                                     ! > Total Enthalpy
    pi_rho = EOS_d_PI_rho(Vp)                                       ! > Pressure Derivative // rho
    pi_E   = EOS_d_PI_E(Vp)                                         ! > Pressure Derivative // rho*E
    cc  = Vp(i_son_vp)                                              ! > Sound Speed

    xi  = 2*ek - pi_rho/pi_E
    phi = pi_rho - cc*cc

    !Matrices Initialization
    LL = 0.0; RR = 0.0

    ! ------------------
    ! Right eigenvectors (column oriented)
    ! ------------------
    ! 2-D !
    IF( N_dim == 2 ) THEN

       nn_p = 0.d0
       nn_p(1) = nn(2); nn_p(2) = -nn(1)                            ! > nn_p.nn = 0
       u_p = SUM(uu*nn_p)                                           ! > u_p = u.nn_p

       RR(1,         1) = 1.d0
       RR(2:N_dim+1, 1) = uu - cc*nn
       RR(p,         1) = ht - cc*u_n

       RR(1,         2) = 1.d0
       RR(2:N_dim+1, 2) = uu
       RR(p,         2) = xi

       RR(1,         3) = 1.d0
       RR(2:N_dim+1, 3) = uu + cc*nn
       RR(p,         3) = ht + cc*u_n

       RR(1,         4) = 0.d0
       RR(2:N_dim+1, 4) = nn_p
       RR(p,         4) = u_p

       ! 3-D !
    ELSEIF ( N_dim == 3 ) THEN

       !Unit Vector
       nx = nn(1)
       ny = nn(2)
       nz = nn(3)

       !Speed vector
       u  = uu(1)
       v  = uu(2)
       w  = uu(3)

       RR(1,         1) = 1.d0
       RR(2:N_dim+1, 1) = uu - cc*nn
       RR(p,         1) = ht - cc*u_n

       RR(1,         2) = nx
       RR(2,         2) = u*nx
       RR(3,         2) = v*nx + nz
       RR(4,         2) = w*nx - ny
       RR(p,         2) = xi*nx + v*nz - w*ny

       RR(1,         3) = 1
       RR(2,         3) = u + cc*nx
       RR(3,         3) = v + cc*ny
       RR(4,         3) = w + cc*nz
       RR(p,         3) = ht + cc*u_n

       RR(1,         4) = ny
       RR(2,         4) = u*ny - nz
       RR(3,         4) = v*ny
       RR(4,         4) = w*ny + nx
       RR(p,         4) = xi*ny + w*nx - u*nz

       RR(1,         5) = nz
       RR(2,         5) = u*nz + ny
       RR(3,         5) = v*nz - nx
       RR(4,         5) = w*nz
       RR(p,         5) = xi*nz + u*ny - v*nx

    ENDIF

    ! -----------------
    ! Left eigenvectors (row oriented)
    ! -----------------
    ! 2-D !

    IF( N_dim == 2 ) THEN

       LL(1, 1)         =  cc*u_n + pi_rho
       LL(1, 2:N_dim+1) = -cc*nn  - pi_E*uu
       LL(1, p)         =  pi_E

       LL(2, 1)         = -2.d0 * (pi_rho - cc*cc)
       LL(2, 2:N_dim+1) =  2.d0 *  pi_E*uu
       LL(2, p)         = -2.d0 *  pi_E

       LL(3, 1)         = -cc*u_n + pi_rho
       LL(3, 2:N_dim+1) =  cc*nn  - pi_E*uu
       LL(3, p)         =  pi_E

       LL(4, 1)         = -2.d0*cc*cc * u_p
       LL(4, 2:N_dim+1) =  2.d0*cc*cc * nn_p
       LL(4, p)         =  0.d0

       LL = LL / 2.d0

       ! 3-D !
    ELSEIF( N_dim == 3 ) THEN

       !Unit Vector
       nx = nn(1)
       ny = nn(2)
       nz = nn(3)

       !Speed vector
       u  = uu(1)
       v  = uu(2)
       w  = uu(3)

       LL(1,         1) = (pi_rho + cc*u_n) / 2.d0
       LL(1,         2) = (-u*pi_E - cc*nx) / 2.d0
       LL(1,         3) = (-v*pi_E - cc*ny) / 2.d0
       LL(1,         4) = (-w*pi_E - cc*nz) / 2.d0
       LL(1,         p) = pi_E              / 2.d0

       LL(2,         1) = -phi*nx - cc*cc*(v*nz - w*ny)
       LL(2,         2) = u*pi_E*nx
       LL(2,         3) = v*pi_E*nx + cc*cc*nz
       LL(2,         4) = w*pi_E*nx - cc*cc*ny
       LL(2,         p) = -nx*pi_E

       LL(3,         1) = (pi_rho - cc*u_n) / 2.d0
       LL(3,         2) = (-u*pi_E + cc*nx) / 2.d0
       LL(3,         3) = (-v*pi_E + cc*ny) / 2.d0
       LL(3,         4) = (-w*pi_E + cc*nz) / 2.d0
       LL(3,         p) = pi_E              / 2.d0

       LL(4,         1) = -phi*ny - cc*cc*(w*nx - u*nz)
       LL(4,         2) = u*pi_E*ny - cc*cc*nz
       LL(4,         3) = v*pi_E*ny
       LL(4,         4) = w*pi_E*ny + cc*cc*nx
       LL(4,         p) = -ny*pi_E

       LL(5,         1) = -phi*nz - cc*cc*(u*ny - v*nx)
       LL(5,         2) = u*pi_E*nz + cc*cc*ny
       LL(5,         3) = v*pi_E*nz - cc*cc*nx
       LL(5,         4) = w*pi_E*nz
       LL(5,         p) = -nz*pi_E


    ENDIF

    ! Normalization
    LL = LL / (cc*cc)

  END SUBROUTINE Euler_eigenvectors
  !================================

  !======================================
  SUBROUTINE Euler_cons_variables(Vp, Vc)
    !======================================
    !> Compute the conservative variables of
    !> the Euler equations, given the primitive
    !> Variables  

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vp
    REAL, DIMENSION(:), INTENT(OUT) :: Vc
    !------------------------------------

    Vc(i_rho_vp:i_ren_ua) = Vp(i_rho_vp:i_eps_vp)
    Vc(i_ren_ua)          = Vp(i_eps_vp) + 0.5d0*SUM(Vc(i_rv1_ua:i_rvd_ua)**2)
    Vc(i_rv1_ua:i_ren_ua) = Vc(2:i_ren_ua)* Vp(i_rho_vp)          

  END SUBROUTINE Euler_cons_variables
  !==================================

  !======================================
  SUBROUTINE Euler_prim_variables(Vc, Vp)
    !======================================
    !> Compute the primitive variables of
    !> the Euler equations, given the conservative
    !> Variables  

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vc
    REAL, DIMENSION(:), INTENT(OUT) :: Vp
    !------------------------------------
    REAL :: ek, e
    !------------------------------------

    Vp(i_rho_vp)          = Vc(i_rho_ua)     
    Vp(i_vi1_vp:i_eps_vp) = Vc(i_rv1_ua:i_ren_ua)/Vc(i_rho_ua)     

    ek = 0.5*SUM(Vp(i_vi1_vp:i_vid_vp)**2)

    e = Vp(i_eps_vp) - ek

    Vp(i_eps_vp) = e

    CALL EOSEos(Vp)

  END SUBROUTINE Euler_prim_variables
  !==================================

END MODULE Contrib_Eul
