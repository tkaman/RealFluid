MODULE Source

  USE LesTypes
  USE Visc_Laws,           ONLY: EOSVisco
  USE Contrib_NS
  USE Turbulence_models,   ONLY: Turbulent, N_eqn_turb, &
                                 Turb_source_term,      &
                                 Turb_Jacobian_Source

  IMPLICIT NONE

CONTAINS

#ifdef NAVIER_STOKES

  !=========================================
  SUBROUTINE Source_term(Vp, G_Vc, d_min, S)
  !=========================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_Vc
    REAL,                 INTENT(IN) :: d_min
    
    REAL, DIMENSION(:),  INTENT(OUT) :: S    
    !--------------------------------------------

    REAL, DIMENSION(3) :: w

    REAL :: mu, w_mod
    !--------------------------------------------

    mu = EOSVisco( Vp(i_rho_vp), Vp(i_tem_vp) )
    
    w = Vorticity_vector( Vp(:), G_Vc(:,:) )
    
    w_mod = SQRT(SUM(w*w))
        
    S = Turb_source_term(Vp, G_Vc, mu, w_mod, d_min)

  END SUBROUTINE Source_term
  !=========================

  !===================================================
  FUNCTION Jacobian_Source(Vp, G_Vc, d_min) RESULT(JJ)
  !===================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_Vc
    REAL,                 INTENT(IN) :: d_min

    REAL, DIMENSION(SIZE(G_vc, 2), SIZE(G_vc, 2)) :: JJ
    !-----------------------------------------------

    REAL, DIMENSION(3) :: w

    REAL :: mu, w_mod
    !-------------------------------------

    w = Vorticity_vector( Vp, G_Vc )
    
    w_mod = SQRT(SUM(w*w))

    mu = EOSVisco( Vp(i_rho_vp), Vp(i_tem_vp) )

    JJ = Turb_Jacobian_Source( Vp, G_Vc, mu, w_mod, d_min )

  END FUNCTION Jacobian_Source
  !===========================  

#endif

END MODULE Source
