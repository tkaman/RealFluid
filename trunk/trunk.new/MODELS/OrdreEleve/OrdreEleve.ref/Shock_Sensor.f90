MODULE Shock_sensor
  
  USE LesTypes
  USE LibComm

  IMPLICIT NONE

CONTAINS

  !==============================
  FUNCTION ss_JST(Var) RESULT(Xi)
  !==============================

    IMPLICIT NONE
    
    TYPE(Variables), INTENT(INOUT) :: Var

    REAL, DIMENSION(:), ALLOCATABLE :: Xi
    !------------------------------------------

    REAL :: P_i, P_j, d

    INTEGER :: Nu_i, Nu_j, l, j, je, N_dofs
    !------------------------------------------

    ALLOCATE( Xi(Var%NCells) )

    Xi = 0.d0

    DO Nu_i = 1, Var%NCells

       P_i = Var%Vp(i_pre_vp, Nu_i)

       DO l = 1, SIZE(Connect(Nu_i)%cn_ele)

          je = Connect(Nu_i)%cn_ele(l)

          N_dofs = d_element(je)%e%N_points

          d = 0.d0
          DO j = 1, N_dofs

             Nu_j = d_element(je)%e%NU(j)

             P_j = Var%Vp(i_pre_vp, Nu_j)

             d = d + ABS(P_i - P_j) / 0.5d0*(P_i + P_j)

          ENDDO

          Xi(Nu_i) = Xi(Nu_i) + d

       ENDDO

    ENDDO

    DO Nu_i = 1, Var%NCells 

       d = REAL( SIZE(Connect(Nu_i)%cn_ele), 8 )

       Xi(Nu_i) = Xi(Nu_i) / d

       Xi(Nu_i) = MAX( 1.0E-16, Xi(Nu_i) )

    ENDDO

  END FUNCTION ss_JST  
  !==================

  !=================================
   FUNCTION ss_P(Com, Var) RESULT(Xi)
  !=================================
    
    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var

    REAL, DIMENSION(:), ALLOCATABLE :: Xi
    !------------------------------------------

    TYPE(element_str) :: ele

    REAL, DIMENSION(:), ALLOCATABLE :: SS

    REAL, DIMENSION(Data%NDim) :: vv, int_Dp, Dp_q

    REAL :: P_max, P_min, P_Mn, nn, v2, d_Pv2, mod_v

    REAL :: L_ref, h, sc

    INTEGER :: Nu_i, l, i, je, id, iq
    !------------------------------------------

    P_max = MAXVAL(Var%Vp(i_pre_vp, :))
    P_min = MINVAL(Var%Vp(i_pre_vp, :))

#ifndef SEQUENTIEL
    CALL Reduce(Com, cs_parall_max, P_max)
    CALL Reduce(Com, cs_parall_min, P_min)
#endif

    P_Mn = P_max - P_min

    DO id = 1, Data%NDim
       vv(id) = SUM( Var%Vp(id+1, :) )
    ENDDO

    nn = REAL(Var%NCells, 8)

#ifndef SEQUENTIEL
    CALL Reduce(Com, cs_parall_sum, nn)

    DO id = 1, Data%NDim
       CALL Reduce(Com, cs_parall_sum, vv(id))
    ENDDO
#endif

    vv = vv / nn

    v2 = SUM( vv**2 )

    d_Pv2 = P_Mn * v2

    ALLOCATE( SS(SIZE(d_element)) )

    DO je = 1, SIZE(d_element)
       
       ele = d_element(je)%e

       DO id = 1, Data%NDim
          vv(id) = SUM(Var%Vp(id+1, ele%NU)) / &
                   REAL(ele%N_points, 8)
       ENDDO

       mod_v = SQRT( SUM(vv**2) )

       int_Dp = 0.d0
       DO iq = 1, ele%N_quad

          Dp_q = 0.d0
          DO id = 1, Data%NDim
             Dp_q(id) = SUM( Var%Vp(i_pre_vp, ele%NU) * &
                              ele%D_phi_q(id, :, iq) )
          ENDDO

          int_Dp = int_Dp + ele%w_q(iq) * Dp_q

       ENDDO

       sc = DOT_PRODUCT( int_Dp, vv ) * mod_v / (d_Pv2 * ele%Volume)

       L_ref = 1.d0

       h = HUGE(1.d0)
       DO i = 1, ele%N_verts
          h = MIN( h, SQRT(SUM(ele%rd_n(:, i)**2)) )
       ENDDO

       SS(je) = MIN(1.d0, L_ref * h * sc**2)

    ENDDO


    ALLOCATE( Xi(Var%NCells) )

    Xi = 0.d0

    DO Nu_i = 1, Var%NCells

       DO l = 1, SIZE(Connect(Nu_i)%cn_ele)

          je = Connect(Nu_i)%cn_ele(l)

          Xi(Nu_i) = Xi(Nu_i) + SS(je)

       ENDDO

    ENDDO

    DO Nu_i = 1, Var%NCells 

       nn = REAL( SIZE(Connect(Nu_i)%cn_ele), 8 )

       Xi(Nu_i) = Xi(Nu_i) / nn

       Xi(Nu_i) = MAX( 1.0E-16, Xi(Nu_i) )

    ENDDO


  END FUNCTION ss_P
  !================

END MODULE Shock_sensor
