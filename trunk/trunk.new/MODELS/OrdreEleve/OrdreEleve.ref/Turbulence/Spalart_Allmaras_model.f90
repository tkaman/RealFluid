MODULE Spalart_Allmaras_model

  ! Spalart-Allmaras turtbulence model, for compressible
  ! flows, without the trip term and the f_t2 function.
  ! Modified version to avoid negative turbulence viscosity.
  !
  ! Ref: 
  ! A. Crivellini, V. D'Alessandro & F. Bassi
  ! A Spalart-Allmaras turbulence model implementation in
  ! a discontinuous Galerkin solver for inscompressible flows.
  ! JCP 241 (2013) 388-415

  ! Nomenclature:
  ! mut_s   | Turbulent viscosity, mut_s = rho*nut_s
  ! mu      | Cynematic viscosity of the fluid
  ! rho     | Density
  ! ww_mod  | Vorticity magnitute
  ! dmin    | Distance to nearest wall
  ! G_rho   | Density gradient
  ! G_mut_s | Gradient of the turbulent viscosity

  USE LesTypes
  USE EOSLaws,   ONLY: EOS_d_PI_rho, EOS_d_PI_E

  IMPLICIT NONE

  !==========================================
  ! Spalart-Allmaras constants
  REAL, PRIVATE :: C_b1, C_b2,       &
                   C_w1, C_w2, C_w3, &
                   C_v1, C_v2, C_v3, &
                   sigma_SA, k_SA

  ! Turbulent Prandtl number
  REAL, PRIVATE, PARAMETER :: Pr_t = 0.9d0

  ! Spalart-Allmaras specif functions
  !PRIVATE :: f_v1, f_v2, f_Ss, f_w,         &
  !           Production_SA, Destruction_SA, &
  !           Diffusion_SA

  ! Variables initialization (turbulent viscosity)
  REAL, PRIVATE :: r_SA, Re_ref, mu_oo
  !==========================================

CONTAINS

 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INITIALIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !==================================
  SUBROUTINE Init_Turb_SA(N_eqn_turb)
  !==================================

    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: N_eqn_turb
    !---------------------------------

    ! 1 Eq. model
    N_eqn_turb = 1

    k_SA = 0.41d0

    sigma_SA = 2.d0/3.d0

    C_b1 = 0.1355d0
    C_b2 = 0.6220d0

    C_w1 = C_b1/(k_SA**2) + (1.d0 + C_b2)/sigma_SA
    C_w2 = 0.30d0
    C_w3 = 2.d0

    C_v1 = 7.1d0
    C_v2 = 0.7d0
    C_v3 = 0.9d0  

  END SUBROUTINE Init_Turb_SA
  !==========================

  !============================
  SUBROUTINE set_VarVp_SA(Neq)
  !============================ 

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: Neq
    !--------------------------

    i_nuts_vp = Neq + 1
  
  END SUBROUTINE set_VarVp_SA
  !============================

  !============================
  SUBROUTINE set_VarUA_SA(Neq)
  !============================ 

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: Neq
    !--------------------------

    i_muts_ua = Neq + 1

  END SUBROUTINE set_VarUA_SA
  !===========================

  !========================================
  SUBROUTINE Read_InitVals_SA(line, p_read)
  !========================================
  ! 
  !           mut_s_oo
  ! r_SA := -----------, prescribed quantity
  !            mu_oo
  !
    IMPLICIT NONE
    
    CHARACTER(LEN=*), INTENT(IN) :: line
    INTEGER,          INTENT(IN) :: p_read
    !--------------------------------------

    INTEGER :: ios
    REAL, DIMENSION(p_read) :: v_read
    !------------------------------------

    READ(line, *, IOSTAT=ios) v_read, r_SA

    IF(ios /=0) THEN
       WRITE(*,*) 'ERROR: r_SA not specified. STOP!'
       STOP
    ENDIF

    WRITE(*,'("  r_SA  = ", E8.3)') r_SA
        
  END SUBROUTINE Read_InitVals_SA
  !==============================

  !==================================================
  SUBROUTINE InitVar_SA(Vp_init, Re_adim, mu_oo_adim)
  !=================================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp_init
    REAL,               INTENT(IN)    :: Re_adim
    REAL,               INTENT(IN)    :: mu_oo_adim
    !-----------------------------------------------

    Re_ref = Re_adim
    
    mu_oo = mu_oo_adim

    ! Dimensionless turbulent viscosity
    Vp_init(i_nuts_vp) = r_SA * (mu_oo*Re_ref/Vp_init(i_rho_vp))
    
  END SUBROUTINE InitVar_SA
  !=========================

  !========================================
  SUBROUTINE Read_bc_SA(line, p_read, Vp_b)
  !========================================
  !
  ! At the far-field or at inflow boundaries
  ! the quantity r_SA is prescribes
  !
    IMPLICIT NONE

    CHARACTER(LEN=*),   INTENT(IN)    :: line
    INTEGER,            INTENT(IN)    :: p_read
    REAL, DIMENSION(:), INTENT(INOUT) :: Vp_b
    !----------------------------------------

    REAL, DIMENSION(p_read) :: v_dummy
    INTEGER :: ios
    !----------------------------------------

    READ(line, *, IOSTAT=ios) v_dummy, r_SA

    Vp_b(i_nuts_vp) = r_SA * (mu_oo*Re_ref/Vp_b(i_rho_vp))

    IF(ios /=0) THEN
       WRITE(*,*) 'ERROR: r_SA not specified in BCs. STOP!'
       STOP
    ENDIF
    
  END SUBROUTINE Read_bc_SA
  !========================

  !===============================
  SUBROUTINE Init_Wall_bc_SA(Vp_b)
  !===============================
  !
  ! At wall the turbulent viscosity is zero
  !
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp_b
    !----------------------------------------

    Vp_b(i_nuts_vp) = 0.d0
    
  END SUBROUTINE Init_Wall_bc_SA
  !=============================

 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%% COUPLING EQUATIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !===================================
  SUBROUTINE Cons_Variables_SA(Vp, Vc)
  !===================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:), INTENT(INOUT) :: Vc
    !---------------------------------------

    Vc(i_muts_ua) = Vp(i_rho_vp)*Vp(i_nuts_vp) 

  END SUBROUTINE Cons_Variables_SA
  !===============================  

  !===================================
  SUBROUTINE Prim_Variables_SA(Vc, Vp)
  !===================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)    :: Vc
    REAL, DIMENSION(:), INTENT(INOUT) :: Vp
    !---------------------------------------

    Vp(i_nuts_vp) = Vc(i_muts_ua)/Vc(i_rho_ua)

  END SUBROUTINE Prim_Variables_SA  
  !===============================  
  
  !=====================================
  SUBROUTINE interp_Vp_SA(Vp, phi, Vp_o)
  !=====================================

    IMPLICIT NONE

    REAL, DIMENSION(:,:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:),   INTENT(IN)    :: phi
    REAL, DIMENSION(:),   INTENT(INOUT) :: Vp_o
    !------------------------------------------

    REAL, DIMENSION(SIZE(Vp, 2)) :: mu
    REAL :: mu_o
    !------------------------------------------

    mu = Vp(i_nuts_vp, :) * Vp(i_rho_vp, :)

    mu_o = SUM( mu * phi )

    Vp_o(i_nuts_vp) = mu_o / Vp_o(i_rho_vp)
     
  END SUBROUTINE interp_Vp_SA
  !==========================

  !===================================
  SUBROUTINE mean_state_SA(Vp_, Vp_b_)
  !===================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vp_
    REAL,               INTENT(OUT) :: Vp_b_
    !---------------------------------------

    REAL :: r_N_dofs
    !---------------------------------------

    r_N_dofs = REAL( SIZE(Vp_), 8 )

    Vp_b_ = SUM(Vp_) / r_N_dofs    
    
  END SUBROUTINE mean_state_SA
  !============================
  
  !====================================
  SUBROUTINE Advection_flux_SA(Vp, ff_)
  !====================================
  !
  ! ff_: only the last row of the flux function
  !
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vp
    REAL, DIMENSION(:), INTENT(OUT) :: ff_
    !----------------------------------------

    ! ff_sa = rho * vel * nut_s
    ff_(:) = Vp(i_vi1_vp:i_vid_vp) * Vp(i_rho_vp) * Vp(i_nuts_vp)

  END SUBROUTINE advection_flux_SA
  !===============================

  !===============================================
  SUBROUTINE Advection_Jacobian_SA(N_dim, Vp, AA_)
  !===============================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp    
    REAL, DIMENSION(:,:), INTENT(OUT) :: AA_
    !------------------------------------------
    
    REAL, DIMENSION(N_dim) :: u
    INTEGER :: i
    !-------------------------------------------

    u = Vp(i_vi1_vp:i_vid_vp)
    
    AA_ = 0.0

    DO i = 1, N_dim
       
       AA_(i_rho_ua,   i) = -u(i)*Vp(i_nuts_vp)
       AA_(i_rho_ua+i, i) =  Vp(i_nuts_vp)
       AA_(i_muts_ua,  i) =  u(i)

    END DO
    
  END SUBROUTINE Advection_Jacobian_SA
  !===================================

  !===================================================
  SUBROUTINE Advection_eigenvalues_SA(Vp, nn, lambda_)
  !===================================================
  ! > Compute the eigenvalues of the advective part
  ! > along the direction nn, |nn| = or /= 1
  !
  ! Ordering:
  !
  !  l(1) = u.n-c, l(2) = u.n, l(3) = u.n+c, l(4:Ndim+3) = u.n
  ! 
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vp
    REAL, DIMENSION(:), INTENT(IN)  :: nn
    REAL,               INTENT(OUT) :: lambda_
    !------------------------------------------

    REAL :: un
    !------------------------------------------

    un = SUM(Vp(i_vi1_vp:i_vid_vp) * nn)

    lambda_ = un

  END SUBROUTINE Advection_eigenvalues_SA
  !======================================

  !=======================================================
  SUBROUTINE Advection_eigenvectors_SA(Vp, v_nn, RR_, LL_)
  !=======================================================
  ! > Compute the eigenvectors of the advective part,
  ! > along the direction nn, |nn| = 1
  ! > Normalization: RR*LL = II
  !
  ! Ordering of corresponding eigenvalues:
  !
  !  l(1) = u.n-c, l(2) = u.n, l(3) = u.n+c, l(4:Ndim+3) = u.n
  !
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:), INTENT(IN)    :: v_nn
    REAL, DIMENSION(:), INTENT(INOUT) :: RR_
    REAL, DIMENSION(:), INTENT(INOUT) :: LL_
    !----------------------------------------

    REAL, DIMENSION(SIZE(v_nn)) :: u

    REAL :: nut_s, c, dP_drho, dP_dE

    INTEGER :: N_dim
    !----------------------------------------

    N_dim = SIZE(v_nn)

    u = Vp(i_vi1_vp:i_vid_vp)

    nut_s = Vp(i_nuts_vp)
    c     = Vp(i_son_vp)
     
    RR_ = 0.0;  LL_ = 0.0

    ! ------------------
    ! Right eigenvectors (column oriented)
    ! ------------------
    RR_(1)         =  nut_s
    RR_(3)         =  nut_s
    RR_(i_muts_ua) =  1.d0

    ! -----------------
    ! Left eigenvectors (row oriented)
    ! -----------------
    dP_drho = EOS_d_PI_rho(Vp)
    dP_dE   = EOS_d_PI_E(Vp) 

    LL_(1)         = -2.d0 * nut_s * dP_drho
    LL_(2:N_dim+1) =  2.d0 * nut_s * dP_dE * u
    LL_(N_dim+2)   = -2.d0 * nut_s * dP_dE
    LL_(i_muts_ua) =  2.d0*c*c

    ! Normalization
    LL_ = LL_ / (2.d0*c*c)
    
  END SUBROUTINE Advection_eigenvectors_SA
  !=======================================  

  !===================================================
  SUBROUTINE Diffusion_Jacobian_SA(N_dim, Vp, mu, KK_)
  !===================================================
  !
  ! KK_: only the last rows of the Jacobian matrices
  !
    IMPLICIT NONE

    INTEGER,                INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),     INTENT(IN)  :: Vp
    REAL,                   INTENT(IN)  :: mu
    REAL, DIMENSION(:,:,:), INTENT(OUT) :: KK_
    !------------------------------------------

    REAL :: mut_s
    INTEGER :: i
    !------------------------------------------

    mut_s = Vp(i_rho_vp)*Vp(i_nuts_vp)

    KK_ = 0.d0
    
    DO i = 1, N_dim

       KK_(i_rho_ua,  i,i) = -turb_eta_SA(mu, mut_s)*mut_s / (Vp(i_rho_vp)**2)
       KK_(i_muts_ua, i,i) =  turb_eta_SA(mu, mut_s)       /  Vp(i_rho_vp)

    ENDDO

!!    KK_ = (M_oo/Re_oo)*KK_
    KK_ = (1.d0/Re_ref)*KK_

  END SUBROUTINE Diffusion_Jacobian_SA
  !===================================

  !=======================================================
  SUBROUTINE Diffusion_flux_function_SA(Vp, D_Vc, mu, ff_)
  !=======================================================

    IMPLICIT NONE
    
    REAL, DIMENSION(:),     INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),   INTENT(IN)  :: D_Vc
    REAL,                   INTENT(IN)  :: mu
    REAL, DIMENSION(:),     INTENT(OUT) :: ff_
    !---------------------------------------------

    REAL, DIMENSION(SIZE(ff_)) :: D_nut

    REAL :: mut_s, eta
    !---------------------------------------------

    mut_s = Vp(i_rho_vp)*Vp(i_nuts_vp)

!!$    D_nut = ( D_Vc(:, i_muts_ua) - &
!!$              Vp(i_nuts_vp)*D_Vc(:, i_rho_ua) ) / Vp(i_rho_vp)
!!$
!!$    eta = turb_eta_SA(mu, mut_s)
!!$
!!$    ff_ = eta * D_nut

    ff_ = -D_Vc(:, i_rho_ua)  * turb_eta_SA(mu, mut_s)*mut_s / (Vp(i_rho_vp)**2) &
          +D_Vc(:, i_muts_ua) * turb_eta_SA(mu, mut_s)       /  Vp(i_rho_vp)
        
    ff_ = (1.d0/Re_ref)*ff_

  END SUBROUTINE Diffusion_flux_function_SA
  !========================================

 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%% S-A TURBULENT EQUATIONS %%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !=================================================
  FUNCTION eddy_viscosity_SA(mut_s, mu) RESULT(mu_t)
  !=================================================
  !
  ! mu_t = mut_s * f_v1. mu_t > 0
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: mut_s
    REAL, INTENT(IN) :: mu

    REAL             :: mu_t
    !---------------------------

    REAL :: Xi, Psi
    !---------------------------

    Xi = mut_s / mu
   
    Psi = Psi_Xi(Xi)

    IF( mut_s >= 0.d0 ) THEN
       mu_t = mut_s * f_v1(Psi)
    ELSE
       mu_t = 0.d0
    ENDIF

  END FUNCTION eddy_viscosity_SA  
  !=============================

  !========================================
  FUNCTION deddy_dmut(mut_s, mu) RESULT(dy)
  !========================================

    IMPLICIT NONE
    
    REAL, INTENT(IN) :: mut_s
    REAL, INTENT(IN) :: mu

    REAL             :: dy
    !----------------------
    
    REAL :: Xi
    !---------------------------

    Xi = mut_s / mu

    IF( mut_s >= 0.d0) THEN
       dy = f_v1(Xi) + mut_s * dfv1_dmut(mu, Xi)
    ELSE
       dy = 0.d0
    ENDIF

  END FUNCTION deddy_dmut  
  !======================
  
  !======================================================
  FUNCTION eddy_therm_cond_SA(mut_s, mu, Cp) RESULT(k_th)
  !======================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mut_s
    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Cp
 
    REAL             :: k_th
    !---------------------------

    REAL :: mut_adim
    !---------------------------

    mut_adim = eddy_viscosity_SA(mut_s, mu)

    k_th = Cp * mut_adim / Pr_t

  END FUNCTION eddy_therm_cond_SA  
  !==============================

  !=====================================================
  FUNCTION deddy_therm_cond(mut_s, mu, Cp) RESULT(d_kth)
  !=====================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mut_s
    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Cp
 
    REAL             :: d_kth
    !---------------------------

    d_kth = deddy_dmut(mut_s, mu) * Cp / Pr_t 

  END FUNCTION deddy_therm_cond 
  !============================

  !==========================================
  FUNCTION turb_eta_SA(mu, mut_s) RESULT(eta)
  !==========================================
  !
  ! eta = (mu + mut_s)/sigma
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: mut_s    

    REAL             :: eta
    !---------------------------

    REAL :: Xi, Psi
    !---------------------------

    Xi = mut_s / mu

    Psi = Psi_Xi(Xi)

    eta = mu*(1.d0 + Psi)
    eta = eta/sigma_SA
    
  END FUNCTION turb_eta_SA
  !======================= 

  !===============================================================
  FUNCTION Source_SA(mut_s, mu, rho, ww_mod, dmin, G_rho, G_mut_s) &
                                                          RESULT(Q)
  !===============================================================
  !
  ! Source = Production - Destruction + Diffusion
  !
    IMPLICIT NONE

    REAL,               INTENT(IN) :: mut_s
    REAL,               INTENT(IN) :: mu
    REAL,               INTENT(IN) :: rho
    REAL,               INTENT(IN) :: ww_mod
    REAL,               INTENT(IN) :: dmin
    REAL, DIMENSION(:), INTENT(IN) :: G_rho
    REAL, DIMENSION(:), INTENT(IN) :: G_mut_s

    REAL :: Q
    !----------------------------------------

    REAL :: Xi, P_nu, D_nu, E_nu
    !----------------------------------------

    Xi = mut_s/mu

    ! Production term
    P_nu = Production_SA(mu, Xi, rho, ww_mod, dmin)

    ! Destruction term
    D_nu = Destruction_SA(mu, Xi, rho, ww_mod, dmin)

    ! Diffusion term
    E_nu = Diffusion_SA(mut_s, rho, G_rho, G_mut_s)

    Q = P_nu - D_nu + E_nu
           !^^^    ^^^  

  END FUNCTION Source_SA
  !=====================

  !=================================================================
  FUNCTION Jacobian_Source_SA(Vp, G_Vc, mu, ww_mod, dmin) RESULT(JJ)
  !=================================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_vc
    REAL,                 INTENT(IN) :: mu
    REAL,                 INTENT(IN) :: ww_mod
    REAL,                 INTENT(IN) :: dmin

    REAL, DIMENSION(SIZE(G_Vc,2), SIZE(G_Vc, 2)) :: JJ
    !-----------------------------------------------

    REAL, DIMENSION(SIZE(G_Vc, 1)) :: G_rho, G_mut

    REAL :: rho, mut_s, Xi
    REAL :: dP_dmuts, dD_dmuts, dE_dmuts, P_dmuts, D_dmuts
    REAL :: dP, dD
    !-----------------------------------------------------

    JJ = 0.d0

    rho = Vp(i_rho_vp)

    mut_s = Vp(i_nuts_vp)*rho

    Xi = mut_s / mu

    G_rho = G_Vc(:, i_rho_ua)
    G_mut = G_Vc(:, i_muts_ua)

    ! Jacobian of the Production term
    !---------------------------------
    CALL dProd_dmut(mu, Xi, rho, ww_mod, dmin, dP_dmuts, P_dmuts)

    ! Jacobian of the Destruction term
    !----------------------------------
    CALL dDstr_dmut(mu, Xi, rho, ww_mod, dmin, dD_dmuts, D_dmuts)

    ! Jacobian of the Diffusion term
    !--------------------------------
    dE_dmuts = dDiff_dmut(mut_s, rho, G_rho, G_mut)

    dP = dP_dmuts + P_dmuts
    dD = dD_dmuts + D_dmuts

    !-----------------
    ! Optimal Version
    !------------------------------------------------------------
!!$    JJ(i_muts_ua, i_muts_ua) = MIN(dP_dmuts - dD_dmuts, 0.d0) + &
!!$                               MIN( P_dmuts -  D_dmuts, 0.d0) + &
!!$                               MIN(dE_dmuts, 0.d0)

    !------------------------
    ! Stable version (works!)
    !-------------------------------------------------------
!!$    JJ(i_muts_ua, i_muts_ua) = MIN(dP - dD, 0.d0) + dE_dmuts
    !--------------------------------------------------------

    !------------------------------
    ! Original version (not robust)
    !-------------------------------------------------------
    JJ(i_muts_ua, i_muts_ua) = dP - dD + dE_dmuts
    !-------------------------------------------------------

  END FUNCTION Jacobian_Source_SA
  !==============================  

 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%% Boundary Condidions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !=====================================
  SUBROUTINE sym_plane_bc_SA(nn, Vp, ff)
  !=====================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)    :: nn    
    REAL, DIMENSION(:), INTENT(IN)    :: Vp
    REAL, DIMENSION(:), INTENT(INOUT) :: ff
    !--------------------------------------
    REAL :: u_n
    !--------------------------------------

    u_n = SUM( Vp(i_vi1_vp:i_vid_vp)*nn )
    
    ff(i_muts_ua) = -Vp(i_rho_vp)*u_n*Vp(i_nuts_vp)    

  END SUBROUTINE sym_plane_bc_SA
  !=============================

  !=======================================
  SUBROUTINE Jac_sym_plane_SA(Vp, nn, Jac)
  !=======================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)    :: Vp    
    REAL, DIMENSION(:),   INTENT(IN)    :: nn
    REAL, DIMENSION(:,:), INTENT(INOUT) :: Jac
    !-----------------------------------------

    REAL :: u_n, nut_s
    !-----------------------------------------

    u_n = SUM(Vp(i_vi1_Vp:i_vid_vp)*nn)

    nut_s = Vp(i_nuts_vp) 

    Jac(i_muts_ua, i_rho_vp)          = -u_n*nut_s
    Jac(i_muts_ua, i_vi1_vp:i_vid_vp) =   nn*nut_s
    Jac(i_muts_ua, i_ren_ua)          = 0.d0
    Jac(i_muts_ua, i_muts_ua)         = u_n

    Jac(i_muts_ua, :) = -Jac(i_muts_ua, :)

  END SUBROUTINE Jac_sym_plane_SA
  !==============================

  !============================
  SUBROUTINE wall_bc_SA(Vp, ff)
  !============================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp    
    REAL, DIMENSION(:), INTENT(INOUT) :: ff
    !--------------------------------------

    Vp(i_nuts_vp) = 0.d0

    ff(i_muts_ua) = 0.d0

  END SUBROUTINE wall_bc_SA
  !========================  


!---------------------------------------------------------------------------
!------------------------- INTERNAL FUNCTIONS ------------------------------
!---------------------------------------------------------------------------

  !============================
  FUNCTION Psi_Xi(Xi) RESULT(y)
  !============================

    IMPLICIT NONE

    REAL, INTENT(IN) :: Xi

    REAL             :: y
    !---------------------------
    
    IF( Xi <= 10.d0 ) THEN

       y = 0.05d0 * LOG(1.d0 + EXP(20.d0 * Xi))

    ELSE

       y = Xi

    ENDIF

  END FUNCTION Psi_Xi
  !==================

  !====================================
  FUNCTION dPsi_dmut(mu, Xi) RESULT(dy)
  !====================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi

    REAL             :: dy
    !----------------------

    IF( Xi <= 10.d0 ) THEN
       
       dy = EXP(20.d0 * Xi) / &
           ( mu*(1.d0 + EXP(20.d0 * Xi)) )

    ELSE

       dy = 1.d0 / mu

    ENDIF
    
  END FUNCTION dPsi_dmut
  !=====================  

  !==========================
  FUNCTION f_v1(Xi) RESULT(y)
  !==========================

    IMPLICIT NONE
    
    REAL, INTENT(IN) :: Xi

    REAL             :: y
    !---------------------------

    REAL :: Psi
    !----------------------------

    Psi = Psi_Xi(Xi)

    y = (Psi**3) / (Psi**3 + C_v1**3)

  END FUNCTION f_v1
  !================

  !====================================
  FUNCTION dfv1_dmut(mu, Xi) RESULT(dy)
  !====================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi

    REAL             :: dy
    !---------------------

    REAL :: Psi, t
    !---------------------

    Psi = Psi_Xi(Xi)

    t = (Psi**3) + C_v1**3
    
    dy = (3.d0*(Psi**2) /   t ) * dPsi_dmut(mu, Xi) - &
         (3.d0*(Psi**5) /(t*t)) * dPsi_dmut(mu, Xi)

  END FUNCTION dfv1_dmut
  !=====================

  !==========================
  FUNCTION f_v2(Xi) RESULT(y)
  !==========================

    IMPLICIT NONE
    
    REAL, INTENT(IN) :: Xi

    REAL             :: y
    !---------------------------

    REAL :: Psi
    !----------------------------

    Psi = Psi_Xi(Xi)

    y = 1.d0 - Psi / ( 1.d0 + Psi*f_v1(Xi) )

  END FUNCTION f_v2
  !================

  !====================================
  FUNCTION dfv2_dmut(mu, Xi) RESULT(dy)
  !====================================

    IMPLICIT NONE
    
    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi

    REAL             :: dy
    !----------------------

    REAL :: Psi, dPsi, fv1, dfv1, t
    !------------------------------

    Psi = Psi_Xi(Xi)

    dPsi = dPsi_dmut(mu, Xi)

    fv1 = f_v1(Xi)
    
    dfv1 = dfv1_dmut(mu, Xi)

    t = 1.d0 + Psi*f_v1(Xi)

    dy = -(dPsi/t) + &
          (Psi/(t*t))*( fv1*dPsi + Psi*dfv1 )

  END FUNCTION dfv2_dmut
  !=====================  

  !=========================================
  FUNCTION f_Sb(mu, Xi, rho, dmin) RESULT(y)
  !=========================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: dmin

    REAL             :: y
    !------------------------

    REAL :: Psi
    !------------------------

    Psi = Psi_Xi(Xi)

    y = ( (mu/rho) * Psi * f_v2(Xi) ) / (k_SA*k_SA * dmin*dmin)
!!    y = y*(M_oo/Re_oo)
    y = y/Re_ref

  END FUNCTION f_Sb
  !================

  !===============================================
  FUNCTION dfSb_dmut(mu, Xi, rho, dmin) RESULT(dy)
  !===============================================
    
    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: dmin

    REAL             :: dy
    !------------------------

    REAL :: t, dPsi, dfv2, Psi
    !--------------------------

    Psi = Psi_Xi(Xi)

    t = (mu/rho)/(k_SA*K_SA * dmin*dmin)

    dPsi = dPsi_dmut(mu, Xi)
    dfv2 = dfv2_dmut(mu, Xi)

    dy = t * ( f_v2(Xi)*dPsi + Psi*dfv2 )
!!    dy = dy*(M_oo/Re_oo)
    dy = dy/Re_ref

  END FUNCTION dfSb_dmut
  !=====================

  !=================================================
  FUNCTION f_Ss(mu, Xi, rho, ww_mod, dmin) RESULT(y)
  !=================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin

    REAL :: y
    !---------------------------------

    REAL :: Psi, S_b, S_
    !---------------------------------

    Psi = Psi_Xi(Xi)

    S_b = f_Sb(mu, Xi, rho, dmin)
        
!!$    IF(S_b >= -C_v2*ww_mod) THEN

       y = ww_mod + S_b

!!$    ELSE
!!$
!!$       S_ = ww_mod*( (C_v2**2)*ww_mod + C_v3*S_b ) / &
!!$            ( (C_v3 - 2.d0*C_v2)*ww_mod - S_b )
!!$
!!$       y = ww_mod + S_
!!$
!!$   ENDIF
    
  END FUNCTION f_Ss
  !================

  !=======================================================
  FUNCTION dfSs_dmut(mu, Xi, rho, ww_mod, dmin) RESULT(dy)
  !=======================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin

    REAL             :: dy
    !------------------------

    REAL :: t, S_b, dSb
    !------------------------

    dSb = dfSb_dmut(mu, Xi, rho, dmin)

!!$    S_b = f_Sb(mu, Xi, rho, dmin)    
!!$
!!$    IF(S_b >= -C_v2*ww_mod) THEN

       dy = dSb

!!$    ELSE
!!$
!!$       t = (C_v3 - 2.d0*C_v2)*ww_mod - S_b
!!$
!!$       dy = (C_v3*ww_mod/t)*dSb + &
!!$            (ww_mod*(C_v2*C_v2*ww_mod + C_v3*S_b)/(t*t)) * dSb
!!$
!!$    ENDIF

  END FUNCTION DfSs_dmut
  !=====================

  !================================================
  FUNCTION f_r(mu, Xi, rho, ww_mod, dmin) RESULT(y)
  !================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin
    
    REAL             :: y
    !------------------------
    
    REAL :: Psi, Ss, den

    REAL, PARAMETER :: r_max = 10.d0
    !------------------------

    Psi = Psi_Xi(Xi)

    Ss = f_Ss(mu, Xi, rho, ww_mod, dmin)

    den = Ss * k_SA*K_SA * dmin*dmin + 1.0E-15

    y = ( (mu/rho) * Psi ) / den

!!    y = (M_oo/Re_oo)*y
    y = y/Re_ref

    IF( y < 0.d0 .OR. y >= r_max ) THEN
       y = r_max
    ENDIF

  END FUNCTION f_r
  !===============   
  
  !======================================================
  FUNCTION dfr_dmut(mu, Xi, rho, ww_mod, dmin) RESULT(dy)
  !======================================================

    IMPLICIT NONE
    
    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin
    
    REAL             :: dy
    !----------------------   

    REAL :: Psi, dPsi, Ss, dSs, r
    !----------------------------

    Psi = Psi_Xi(Xi)
    
    dPsi = dPsi_dmut(mu, Xi)

    Ss = f_Ss(mu, Xi, rho, ww_mod, dmin) 
    
    dSs = dfSs_dmut(mu, Xi, rho, ww_mod, dmin)

    r = f_r(mu, Xi, rho, ww_mod, dmin)

    dy = r*( (dPsi / (Psi + 1.0E-15)) - &
             (dSs  / (Ss  + 1.0E-15)) )

  END FUNCTION dfr_dmut
  !====================

  !========================
  FUNCTION f_g(r) RESULT(y)
  !========================

    IMPLICIT NONE

    REAL, INTENT(IN) :: r

    REAL             :: y
    !---------------------

    y = r + C_w2*( r**6 - r )

  END FUNCTION f_g
  !===============

  !======================================================
  FUNCTION dfg_dmut(mu, Xi, rho, ww_mod, dmin) RESULT(dy)
  !======================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin  

    REAL             :: dy
    !----------------------

    REAL :: r, dr
    !----------------------

    r = f_r(mu, Xi, rho, ww_mod, dmin)

    dr = dfr_dmut(mu, Xi, rho, ww_mod, dmin)

    dy = dr + C_w2*( 6.d0*dr*r**5 - dr )

  END FUNCTION dfg_dmut  
  !====================

  !====================================================
  FUNCTION dfg_dr(mu, Xi, rho, ww_mod, dmin) RESULT(dy)
  !====================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin  

    REAL             :: dy
    !----------------------

    REAL :: r
    !----------------------

    r = f_r(mu, Xi, rho, ww_mod, dmin)

    dy = 1.d0 + C_w2*( 6.d0*r**5 - 1.d0 )

  END FUNCTION dfg_dr
  !==================

  !================================================
  FUNCTION f_w(mu, Xi, rho, ww_mod, dmin) RESULT(y)
  !================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin

    REAL :: y
    !---------------------------------

    REAL :: r, g
    !---------------------------------

    r = f_r(mu, Xi, rho, ww_mod, dmin)

    g = f_g(r)

    y = g * (( (1.d0 + C_w3**6)/(g**6 + C_w3**6) )**(1.d0/6.d0))

  END FUNCTION f_w
  !===============

  !======================================================
  FUNCTION dfw_dmut(mu, Xi, rho, ww_mod, dmin) RESULT(dy)
  !======================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin

    REAL             :: dy
    !----------------------
    
    REAL :: r, g, dg, t1, t5
    !-----------------------    

    r = f_r(mu, Xi, rho, ww_mod, dmin)

    g = f_g(r)

    dg = dfg_dmut(mu, Xi, rho, ww_mod, dmin)

    t1 = ( (1.d0 + C_w3**6)/(g**6 + C_w3**6) )**(1.d0/6.d0)

    t5 = ( (1.d0 + C_w3**6)/(g**6 + C_w3**6) )**(-5.d0/6.d0)

    dy = t1*dg - (g**6)*t5 * dg * (1.d0 + C_w3**6)/( (g**6 + C_w3**6)**2 )

  END FUNCTION dfw_dmut
  !====================

  !====================================================
  FUNCTION dfw_dr(mu, Xi, rho, ww_mod, dmin) RESULT(dy)
  !====================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin

    REAL             :: dy
    !----------------------
    
    REAL :: r, g, dg, t1, t5
    !-----------------------    

    r = f_r(mu, Xi, rho, ww_mod, dmin)

    g = f_g(r)

    dg = dfg_dr(mu, Xi, rho, ww_mod, dmin)

    t1 = ( (1.d0 + C_w3**6)/(g**6 + C_w3**6) )**(1.d0/6.d0)

    t5 = ( (1.d0 + C_w3**6)/(g**6 + C_w3**6) )**(-5.d0/6.d0)

    dy = t1*dg - (g**6)*t5 * dg * (1.d0 + C_w3**6)/( (g**6 + C_w3**6)**2 )

  END FUNCTION dfw_dr
  !==================

  !==========================================================
  FUNCTION Production_SA(mu, Xi, rho, ww_mod, dmin) RESULT(y)
  !==========================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin

    REAL :: y
    !---------------------------

    REAL :: Psi, r, den
    !---------------------------

    Psi = Psi_Xi(Xi)

    r = f_r(mu, Xi, rho, ww_mod, dmin)

    den = rho * k_SA*k_SA * dmin*dmin * r + 1.0E-15

    y = C_b1 * mu*mu * Psi*Psi / den

!!    y = (M_oo/Re_oo)*y
    y = y/Re_ref

  END FUNCTION Production_SA
  !=========================

  !=========================================================
  SUBROUTINE dProd_dmut(mu, Xi, rho, ww_mod, dmin, dPa, dPb)
  !=========================================================

    IMPLICIT NONE

    REAL, INTENT(IN)  :: mu
    REAL, INTENT(IN)  :: Xi
    REAL, INTENT(IN)  :: rho
    REAL, INTENT(IN)  :: ww_mod
    REAL, INTENT(IN)  :: dmin
    REAL, INTENT(OUT) :: dPa, dPb
    !----------------------------

    REAL :: Psi, dPsi, r, t, den, dr
    !-------------------------------

    Psi = Psi_Xi(Xi)

    dPsi = dPsi_dmut(mu, Xi)

    r =  f_r(mu, Xi, rho, ww_mod, dmin) 

    dr = dfr_dmut(mu, Xi, rho, ww_mod, dmin)

    den = rho * k_SA*k_SA * dmin*dmin * r + 1.0E-15

    t = C_b1 * mu*mu * Psi / den

    dPa = -t * Psi * dr / (r + 1.0E-15)
    dPb = 2.d0 * t * dPsi 

!!    dPa = (M_oo/Re_oo)*dPa
!!    dPb = (M_oo/Re_oo)*dPb

    dPa = dPa/Re_ref
    dPb = dPb/Re_ref

  END SUBROUTINE dProd_dmut  
  !========================

  !===========================================================
  FUNCTION Destruction_SA(mu, Xi, rho, ww_mod, dmin) RESULT(y)
  !===========================================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: mu
    REAL, INTENT(IN) :: Xi    
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: ww_mod
    REAL, INTENT(IN) :: dmin

    REAL :: y
    !---------------------------

    REAL :: Psi
    !---------------------------

    Psi = Psi_Xi(Xi)

    y = C_w1 * rho * f_w(mu, Xi, rho, ww_mod, dmin) * &
       ((mu/rho) * Psi / dmin)**2

!!    y = (M_oo/Re_oo)*y
        y = y/Re_ref
       
  END FUNCTION Destruction_SA
  !==========================

  !=========================================================
  SUBROUTINE dDstr_dmut(mu, Xi, rho, ww_mod, dmin, dDa, dDb)
  !=========================================================

    IMPLICIT NONE

    REAL, INTENT(IN)  :: mu
    REAL, INTENT(IN)  :: Xi
    REAL, INTENT(IN)  :: rho
    REAL, INTENT(IN)  :: ww_mod
    REAL, INTENT(IN)  :: dmin
    REAL, INTENT(OUT) :: dDa, dDb
    !-----------------------------

    REAL :: Psi, dPsi, fw, dfW, dr, t
    !---------------------------------

    Psi = Psi_Xi(Xi)

    dPsi = dPsi_dmut(mu, Xi)

    fw = f_w(mu, Xi, rho, ww_mod, dmin)

    dfw  = dfw_dr(mu, Xi, rho, ww_mod, dmin)

    dr = dfr_dmut(mu, Xi, rho, ww_mod, dmin)

    t = C_w1 * mu*mu * Psi / (rho * dmin*dmin)

    dDa = t * Psi * dfw * dr
    dDb = 2.d0 * t * fw * dPsi

!!    dDa = (M_oo/Re_oo)*dDa
!!    dDb = (M_oo/Re_oo)*dDb
    
    dDa = dDa/Re_ref
    dDb = dDb/Re_ref

  END SUBROUTINE dDstr_dmut  
  !========================

  !========================================================
  FUNCTION Diffusion_SA(mut_s, rho, G_rho, G_mut) RESULT(y)
  !========================================================

    IMPLICIT NONE

    REAL,               INTENT(IN) :: mut_s
    REAL,               INTENT(IN) :: rho
    REAL, DIMENSION(:), INTENT(IN) :: G_rho
    REAL, DIMENSION(:), INTENT(IN) :: G_mut

    REAL :: y
    !----------------------------------------

    REAL, DIMENSION(SIZE(G_mut)) :: G_nut
    !-----------------------------------------

    ! G_mut --> G_nut
    G_nut = ( G_mut - (mut_s/rho)*G_rho ) / rho
    
    IF(mut_s > 0.d0) THEN

       y =  rho * C_b2 * SUM(G_nut*G_nut) / sigma_SA
       
    ELSE

       y = 0.d0

    ENDIF

!!    y = (M_oo/Re_oo)*y
    y = y/Re_ref

  END FUNCTION Diffusion_SA
  !========================

  !=====================================================
  FUNCTION dDiff_dmut(mut, rho, G_rho, G_mut) RESULT(dy)
  !=====================================================

    IMPLICIT NONE

    REAL,               INTENT(IN) :: mut
    REAL,               INTENT(IN) :: rho
    REAL, DIMENSION(:), INTENT(IN) :: G_rho
    REAL, DIMENSION(:), INTENT(IN) :: G_mut

    REAL                           :: dy
    !------------------------------------------

    REAL, DIMENSION(SIZE(G_mut)) :: G_nut
    !------------------------------------------

    G_nut = (G_mut - (mut/rho)*G_rho) / rho

    IF(mut > 0.d0) THEN

       dy = -2.d0 * C_b2 * SUM(G_nut*G_rho) / &
            (sigma_SA*rho)

    ELSE

       dy = 0.d0

    ENDIF

!!    dy = (M_oo/Re_oo)*dy
    dy = dy/Re_ref

  END FUNCTION dDiff_dmut
  !======================

END MODULE Spalart_Allmaras_model
