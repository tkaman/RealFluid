MODULE Contrib_NS

  USE LesTypes
 
  USE EOSLaws,     ONLY: EOSCv__T_rho,       &
                         EOS_dT_de__rho_T,   &
                         EOS_dT_drho__rho_T, &
                         EOS_dCp__T_rho

  USE Visc_Laws,   ONLY: EOSvisco, EOSCondTh

  USE Quadrature

  IMPLICIT NONE

CONTAINS

#ifdef NAVIER_STOKES
  
  !================================================
  SUBROUTINE NS_Jacobian(Ndim, Vp, mu, k_therm, KK)
  !================================================
  !
  ! Jacobian of the Navier-Stokes flux, such that:
  !
  !                        d Vc
  ! ff_i = Sum_j KK_ij * ---------,  i, j = 1, ..., Ndim
  !                        d x_j
    IMPLICIT NONE
    
    INTEGER,                  INTENT(IN)  :: Ndim
    REAL, DIMENSION(:),       INTENT(IN)  :: Vp
    REAL,                     INTENT(IN)  :: mu
    REAL,                     INTENT(IN)  :: k_therm
    REAL, DIMENSION(:,:,:,:), INTENT(OUT) :: KK
    !--------------------------------------------------

    REAL, DIMENSION(:), ALLOCATABLE :: dT_dVc

    REAL, DIMENSION(Ndim) :: uu

    REAL :: p2, p4, p1
    REAL :: rho, kk_val
    REAL :: coeff_visc, coeff_therm
    REAL :: mod_vel2

    INTEGER :: id, jd, kd
    !--------------------------------------------------

    p1 = 1.d0/3.d0
    p2 = 2.d0/3.d0
    p4 = 4.d0/3.d0

    rho = Vp(i_rho_vp)

    uu = Vp(i_vi1_vp:i_vid_vp)    
       
    mod_vel2 = SUM(uu*uu)

    ! Laminar (+ Turbulent) viscosity and thermal coeff.
    coeff_visc  = mu      !!coeff_visc  = (M_oo/Re_oo)*mu
    coeff_therm = k_therm !!coeff_therm = (M_oo/Re_oo)*k_therm

    ALLOCATE(dT_dVc(i_ren_ua))
    
    dT_dVc = D_Temp(Vp)

    KK = 0.d0
    
    DO id = 1, Ndim

       !------------------------
       ! Diagonal contribution
       !------------------------

       ! Contributions to the momentum
       KK(1+id,    1, id, id) = -p4*uu(id)*coeff_visc
       KK(1+id, id+1, id, id) =  p4*coeff_visc

       DO kd = 2, Ndim+1

          IF(kd == id+1) CYCLE

          KK(kd, 1,  id, id) = -uu(kd-1)*coeff_visc
          KK(kd, kd, id, id) =  coeff_visc

       ENDDO

       ! Contributions to the energy
       KK(i_ren_ua, 1,    id, id) = -p1*coeff_visc*uu(id)**2 - &
                                        coeff_visc*mod_vel2  + &
                                     rho * coeff_therm * dT_dVc(i_rho_ua)

       KK(i_ren_ua, id+1, id, id) = uu(id)*p4*coeff_visc + &
                                    rho * coeff_therm * dT_dVc(id+1)

       DO kd = 2, Ndim+1

          IF(kd == id+1) CYCLE

          KK(i_ren_ua, kd, id, id) = uu(kd-1)*coeff_visc + &
                                     rho * coeff_therm * dT_dVc(kd)

       ENDDO
          
       KK(i_ren_ua, i_ren_ua,  id, id) = rho * coeff_therm * dT_dVc(i_ren_ua)

       !------------------------------
       ! Extra-diagonal contribution
       !------------------------------
       DO jd = 1, Ndim

          IF(jd == id) CYCLE

          ! Contributions to the momentum             
          KK(1+id, 1,    id, jd) =  p2*uu(jd)*coeff_visc
          KK(1+id, jd+1, id, jd) = -p2*coeff_visc
          KK(1+jd, 1,    id, jd) = -uu(id)*coeff_visc
          KK(1+jd, id+1, id, jd) =  coeff_visc

          ! Contributions to the energy
          KK(i_ren_ua, 1,    id, jd) = -p1*uu(jd)*uu(id)*coeff_visc
          KK(i_ren_ua, id+1, id, jd) =  uu(jd)*coeff_visc
          KK(i_ren_ua, jd+1, id, jd) = -p2*uu(id)*coeff_visc

       ENDDO          
             
    ENDDO

    KK = KK / rho

    DEALLOCATE(dT_dVc)
    
  END SUBROUTINE NS_Jacobian
  !=========================

  !==============================================
  SUBROUTINE NS_flux(Vp, G_Vc, mu, k_therm, ff_v)
  !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:),     INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),   INTENT(IN)  :: G_Vc
    REAL,                   INTENT(IN)  :: mu
    REAL,                   INTENT(IN)  :: k_therm
    REAL, DIMENSION(:,:),   INTENT(OUT) :: ff_v
    !---------------------------------------------
    
    REAL, DIMENSION(:,:), ALLOCATABLE :: TT
    REAL, DIMENSION(:),   ALLOCATABLE :: qq, uu

    INTEGER :: N_dim, N_var, id
    !---------------------------------------------

    ff_v = 0.d0

    N_dim = SIZE(G_Vc, 1)

    N_var = i_ren_ua

    ALLOCATE(TT(N_dim, N_dim))
    
    ALLOCATE(uu(N_dim), qq(N_dim))
    
    uu = Vp(i_vi1_vp:i_vid_vp)

    TT = Stress_Tensor(Vp, G_Vc, mu)

    qq = Heat_flux(Vp, G_Vc, k_therm)

    ff_v(i_rv1_ua:i_rvd_ua, :) = TT
    
    DO id = 1, N_dim
        ff_v(i_ren_ua, id) = DOT_PRODUCT( uu, TT(id, :) ) - qq(id)
    ENDDO

    DEALLOCATE(TT, uu, qq)

  END SUBROUTINE NS_flux
  !=====================

  !==============================================
  FUNCTION Stress_Tensor(Vp, G_Vc, mu) RESULT(TT)
  !==============================================
  !
  ! Compute the components of the stress tensor
  !
  ! TT_ii = 2*mu*du_i/dx_i + lambda* Div(uu)
  ! TT_ij =   mu*(du_i/dx_j + du_j/dx_i)
  !
  ! lambda = -2/3 * mu
  !
  ! mu := Dynamic viscosity.
  !
  ! Variables ordering
  ! Vp: rho,u,v,w;   G_Vc: G_rho,G_mx,G_my,G_mz
  !
    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_Vc
    REAL, OPTIONAL,       INTENT(IN) :: mu

    REAL, DIMENSION(:,:), ALLOCATABLE :: TT
    !----------------------------------------

    REAL, DIMENSION(:,:), ALLOCATABLE :: Grad_uu

    REAL :: Div_uu, lambda, mu_a
    
    INTEGER :: N_dim, i, j
    !----------------------------------------
  
    N_dim = SIZE(G_Vc, 1)

    IF( PRESENT(mu) ) THEN

       mu_a = mu !! mu_a = mu * (M_oo / Re_oo)

    ELSE

       ! Stress tensor for unit 
       ! value of the viscosity
       mu_a = 1.d0

    ENDIF

    lambda = (-2.d0/3.d0)*mu_a

    ALLOCATE( Grad_uu(N_dim, N_dim) )

    Div_uu = 0.d0

    DO i = 1, N_dim

       Grad_uu(:, i) = ( G_Vc(:, i+1) - Vp(i+1)*G_Vc(:, 1) ) /  Vp(1)

       Div_uu = Div_uu + Grad_uu(i, i)

    ENDDO

    ALLOCATE( TT(N_dim, N_dim) )

    DO i = 1, N_dim

       TT(i,i) = 2.d0*mu_a*Grad_uu(i, i) + lambda*Div_uu

       DO j = 1, N_dim

          IF(j == i) CYCLE
          
          TT(i, j) = mu_a*( Grad_uu(i,j) + Grad_uu(j,i) )

       ENDDO

    ENDDO

    DEALLOCATE(Grad_uu)

  END FUNCTION Stress_Tensor
  !========================= 

  !============================================
  FUNCTION Vorticity_vector(Vp, G_Vc) Result(w)
  !============================================
  !
  ! Compute the components of the vorticity vector
  ! w(3) = w_z, w(2) = w_y, w(1) = w_x
  !
  ! In 2D, only w_z /= 0
  !
  ! Variables ordering
  ! Vp: rho,u,v,w;   G_Vc: G_rho,G_mx,G_my,G_mz
    
    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_Vc

    REAL, DIMENSION(3) :: w
    !----------------------------------------

    INTEGER, DIMENSION(3), PARAMETER :: perm = (/3, 1, 2/)

    REAL :: rho, dui_dxj, duj_dxi

    INTEGER :: i, j, k, N_dim
    !----------------------------------------

    N_dim = SIZE(G_Vc, 1)

    rho = Vp(1)

    w = 0.0

    DO k = 3, 1, -1

       i = perm(k); j= perm(perm(k))

       dui_dxj = (G_Vc(j, i+1) - Vp(i+1)*G_Vc(j, 1)) / rho
       duj_dxi = (G_Vc(i, j+1) - Vp(j+1)*G_Vc(i, 1)) / rho

       w(k) = dui_dxj - duj_dxi

       IF(N_dim == 2) EXIT

    ENDDO

  END FUNCTION Vorticity_vector
  !============================

  !=============================
  FUNCTION D_Temp(Vp) RESULT(dT)
  !=============================
  !
  ! Derivative of the temperature with
  ! respect of the conservative variables
  !
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    
    REAL, DIMENSION(:), ALLOCATABLE :: dT
    !------------------------------------

    REAL :: rho, Et, T, dT_drho, dT_de
    REAL, DIMENSION(:), ALLOCATABLE :: mm

    INTEGER :: N_var, N_dim
    !------------------------------------

    N_var = i_ren_ua
    N_dim = (i_vid_vp-i_vi1_vp)+1

    ALLOCATE( dT(i_ren_ua) )
    ALLOCATE( mm(N_dim) )

    rho = Vp(i_rho_vp)
     mm = rho*Vp(i_vi1_vp:i_vid_vp)
     Et = Vp(i_eps_vp) + 0.5d0*SUM(Vp(i_vi1_vp:i_vid_vp)**2)
     Et = rho*Et

     T =  Vp(i_tem_vp)

    dT_drho = EOS_dT_drho__rho_T(rho, T)
    dT_de   = EOS_dT_de__rho_T(  rho, T)
    
    dT(i_rho_ua) = dT_drho - dT_de * (Et - SUM(mm**2)/rho)/rho**2

    dT(i_rv1_ua:i_rvd_ua) = -(mm / (rho**2)) * dT_de
    
    dT(i_ren_ua) =  dT_de / rho

    DEALLOCATE( mm )

  END FUNCTION D_Temp
  !===================  

  !===========================================
  FUNCTION Heat_flux(Vp, G_Vc, k_th) RESULT(q)
  !===========================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:), INTENT(IN)  :: G_Vc
    REAL, OPTIONAL,       INTENT(IN)  :: k_th

    REAL, DIMENSION(:), ALLOCATABLE :: q
    !-----------------------------------------

    REAL, DIMENSION(:), ALLOCATABLE :: dT_dVc
    
    REAL :: kth_a

    INTEGER :: N_dim, id
    !-----------------------------------------
  
    N_dim = SIZE(G_Vc, 1)

    ALLOCATE( q(N_dim) )

    IF( PRESENT(k_th) ) THEN
       kth_a = k_th !! kth_a = k_th * (M_oo/Re_oo)      
    ELSE
       ! For unit value of coeff. therm
       kth_a = 1.d0
    ENDIF
    
    ALLOCATE( dT_dVc(i_ren_ua) )

    dT_dVc = D_Temp(Vp)

    DO id = 1, N_dim

       q(id) = -kth_a * DOT_PRODUCT( dT_dVc, G_Vc(id, i_rho_ua:i_ren_ua) )
              !^

    ENDDO

    DEALLOCATE( dT_dVc )
 
  END FUNCTION Heat_flux
  !=====================

  !=======================
  SUBROUTINE Loc_Re_w(Var)
  !=======================

    IMPLICIT NONE
     
    TYPE(Variables), INTENT(INOUT) :: Var
    !-------------------------------------

    TYPE(element_Str) :: loc_ele

    INTEGER, DIMENSION(:),   ALLOCATABLE :: Nu
    REAL,    DIMENSION(:),   ALLOCATABLE :: diag

    INTEGER :: N_dofs, N_dim, N_ele
    INTEGER :: je, i, k, jf

    REAL :: RE_loc
    !--------------------------------------

    N_dim = Data%NDim

    N_ele = SIZE(d_element)

    Var%Re_loc = 0.d0

    ALLOCATE( diag(Var%NCells) )

    diag = 0.d0

    !---------------------
    ! Loop on the elements
    !-----------------------------
    DO je = 1, N_ele
        
       !--------------------------
       ! Copy of the local element
       !-----------------------------
       loc_ele = d_element(je)%e
       
       N_dofs = loc_ele%N_Points

       ALLOCATE( NU(N_dofs) )

       DO k = 1, N_dofs

          Nu(k) = loc_ele%Nu(k)
        
       ENDDO
       
       DO k = 1, N_dofs
        
          ! Local Reynolds
          Re_loc = compute_local_Reynolds( N_dim, loc_ele, Var%Vp(:, Nu) )

          ! Area-weighted average Reynolds
          Var%Re_loc(Nu(k)) = Var%Re_loc(Nu(k)) + Re_loc*loc_ele%volume

       ENDDO

       diag(Nu) = diag(Nu) + loc_ele%volume

       DEALLOCATE( Nu )
       
    ENDDO

    Var%Re_loc = Var%Re_loc/diag

    DEALLOCATE( diag )
    
  END SUBROUTINE Loc_Re_w  
  !======================
  
  !=============================================================
  FUNCTION compute_local_Reynolds(N_dim, ele, Vp) RESULT(Re_loc)
  !=============================================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN) :: N_dim
    TYPE(element_str),    INTENT(IN) :: ele
    REAL, DIMENSION(:,:), INTENT(IN) :: Vp

    REAL :: Re_loc
    !-----------------------------------------

    REAL, DIMENSION(N_dim) :: x_g, dx
    REAL :: u_b, l_b, mu_b, rho_b

    INTEGER :: N_dofs, i
    !-----------------------------------------

    N_dofs = ele%N_Points
    
    u_b = ABS(HUGE(1.0))
    l_b = 0.0
    !l_b = ABS(HUGE(1.0))
    mu_b = 0.0

    DO i = 1, N_dim
       x_g(i) = SUM(ele%Coords(i, :))
    ENDDO
    x_g = x_g/REAL(N_dofs, 8)

    DO i = 1, N_dofs
       dx = ele%Coords(:, i) - x_g
       l_b = l_b + SUM(dx(:)**2)
    ENDDO    
    
    DO i = 1, SIZE(Vp, 2)
       u_b  = MIN( u_b, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
       !l_b  = MIN( l_b, SQRT(SUM(elem%n(:, i)**2)) )
       mu_b = MAX( mu_b,  EOSvisco(Vp(i_rho_vp, i), Vp(i_tem_vp, i)) )
       !! mu_b = MAX( mu_b,  EOSvisco(Vp(i_tem_vp, i)) ) * M_oo/Re_oo
    ENDDO

    l_b =  ele%volume/SQRT(N_dofs*l_b)

    rho_b = MAXVAL(Vp(i_rho_vp, :))

    Re_loc = u_b * l_b * rho_b / mu_b

    Re_loc = MAX(Re_loc, 1.0E-12)
        
  ENd FUNCTION compute_local_Reynolds
  !==================================

  !=====================================
  SUBROUTINE test_accuracy_gradient(Var)
  !=====================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    !-------------------------------------

    REAL(KIND=8), DIMENSION(2) :: D_u_ex
    REAL(KIND=8), DIMENSION(:), ALLOCATABLe :: err2, d2

    INTEGER :: je, k, i

    REAL(KIND=8) :: err_L2, int_d2, x, y, c, mu
    !----------------------------------------------------

    err_L2 = 0.d0; int_d2 = 0.d0

    c = 0.59d0
    mu = 10.0E-8

    DO je = 1, SIZE(d_element)

       ALLOCATE( err2(d_element(je)%e%N_points), &
                   d2(d_element(je)%e%N_points) )

       do k = 1, d_element(je)%e%N_points

          x = d_element(je)%e%coords(1, k)
          y = d_element(je)%e%coords(2, k)
           
          D_u_ex(1) = -y/sqrt(x*c*mu)**3*c*mu*exp(-y/sqrt(x*c*mu))/2
          D_u_ex(2) =  1/sqrt(x*c*mu)*exp(-y/sqrt(x*c*mu))
          
          err2(k) = SUM( ( Var%D_Ua(:, 2, d_element(je)%e%NU(k)) - D_u_ex )**2 )

          d2(k) =  SUM( (  D_u_ex**2 ) )

       enddo

       err_L2 = err_L2 + Int_d(d_element(je)%e, err2)
       int_d2 = int_d2 + Int_d(d_element(je)%e, d2)

       deallocate(  err2, d2 )

    enddo
    
    err_L2 = sqrt(err_l2)/sqrt(int_d2)

    WRITE(*,*) ' *** GRADIENT ERROR ***'
    write(*,22) SIZE(Var%Vp, 2), err_l2

22 FORMAT(I6, 1(1x,e24.16))

    stop

  END SUBROUTINE test_accuracy_gradient
  !====================================  

#endif //-Navier-Stokes

END MODULE Contrib_NS

