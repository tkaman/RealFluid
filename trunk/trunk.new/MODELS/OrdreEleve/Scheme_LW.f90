MODULE Scheme_LW

   USE LesTypes
   USE PLinAlg

   USE Advection
   USE Diffusion 
   USE Source

   USE Turbulence_models,   ONLY: Turbulent

   USE PLinAlg
   USE Assemblage

  IMPLICIT NONE

CONTAINS

   !===========================================================
   SUBROUTINE LW_scheme_imp(ele, Vp, Vc, CFL, Phi_tot, JJ_tot, &
                                    Phi_i, inv_dt, Mat, Dr_Vc)
   !===========================================================

     IMPLICIT NONE

     TYPE(element_str),                INTENT(IN)    :: ele
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
     REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
     REAL,                             INTENT(IN)    :: CFL
     REAL, DIMENSION(:),               INTENT(IN)    :: Phi_tot
     REAL, DIMENSION(:,:,:),           INTENT(IN)    :: JJ_tot
     REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
     REAL,                             INTENT(OUT)   :: inv_dt
     TYPE(Matrice),                    INTENT(INOUT) :: Mat
     REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: Dr_Vc
     !---------------------------------------------------------

     REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: Jac_Stab_LW

     REAL, DIMENSION(SIZE(Vp, 1) ) :: Vp_b, Vp_q
     REAL, DIMENSION(Data%NVar)    :: SS_q

     !----------------------------------------------------
     REAL, DIMENSION(Data%NVar, Data%Ndim) :: ff_advec, ff_visc
     REAL :: F_q
     !----------------------------------------------------

     REAL :: d_min_q

     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_i, temp
     REAL, DIMENSION(Data%NDim, Data%NVar) :: Dr_Vc_q

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau, Tau_
     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_i
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_k
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: KKD_phi

     REAL, DIMENSION(Data%NVar) :: Stab_right

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim, &
                                           Data%NDim) :: KK_q, KK_i

     REAL, DIMENSION(Data%Nvar) :: D_KKD_Vc_q, KK_DVc
!    REAL, DIMENSION(Data%Nvar) :: temp

     REAL, DIMENSION(Data%NVar) :: lambda
     !----------------------------------------------------

     REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
     REAL, DIMENSION(:,:,:), POINTER :: D_phi_q
     REAL, DIMENSION(:,:),   POINTER :: phi_q
     REAL, DIMENSION(:),     POINTER :: w  
     !----------------------------------------------------

     REAL, DIMENSION(Data%NVar, Data%Nvar) :: M_ik, Diag_i

     REAL, DIMENSION(Data%NDim) :: v_nn

     REAL :: r_N_dim, r_N_dofs, mod_n, C_Dt, tau_param
     REAL :: u_max, c_max, p_max, S_ele, h_e, Dt
     INTEGER :: N_var, N_dim, N_dofs, N_verts
     INTEGER :: i, j, iq, id, jd, k
     !----------------------------------------------------
    
     N_var = Data%NVar
     N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

     N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

     N_verts = ele%N_verts

     !---------------------
     ! Central distribution
     !-------------------------------------
     DO j = 1, N_var

        Phi_i(j, :) = Phi_tot(j)/r_N_dofs

     ENDDO
     !-------------------------------------

     !---------------
     ! Scaling Matrix
     !--------------------------------------
     Tau_ = 0.d0
  
     ! Mean state
     Vp_b = Mean_state_Vp(Vp)

     tau_param = 2.d0!REAL(Data%NOrdre-1, 8)

     inv_dt = 0.d0

     DO i = 1, N_verts !?
       
        mod_n = DSQRT( SUM(ele%rd_n(:, i)**2) )
       
        v_nn = ele%rd_n(:, i) / mod_n

        CALL Advection_eigenvalues( Vp_b, v_nn, lambda)
        CALL Advection_eigenvectors(Vp_b, v_nn, RR, LL)

        Pos = 0.d0; p_max = 0.d0
        DO j = 1, N_var

           Pos(j, j) = tau_param*MAX(lambda(j), 0.d0) * mod_n

           p_max = MAX( p_max, Pos(j,j) )

        ENDDO
        
        Tau_ = Tau_ + 0.5d0 * MATMUL(RR, MATMUL(Pos, LL))
        
     ENDDO

#ifdef NAVIER_STOKES

    CALL Diffusion_Jacobian(N_dim, Vp_b, KK_q)

    DO id = 1, N_dim

       Tau_ = Tau_ + KK_q(:,:, id, id)!*r_N_dim 

    ENDDO

#endif

     Tau = (1.d0/r_N_Dim) * ele%Volume * InverseLU(Tau_)
     !--------------------------------------

     !-------------------
     ! Stabilization term
     !--------------------------------------
     ALLOCATE( Jac_Stab_LW(N_var, N_var, N_dofs, N_dofs) )
     Jac_stab_LW = 0.d0

     D_phi_k => ele%D_phi_k  
     D_phi_q => ele%D_phi_q
       phi_q => ele%phi_q
           w => ele%w_q
          
     DO iq = 1, ele%N_quad
       
        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )

        D_Vc_q = 0.d0

        DO i = 1, N_dofs

           DO id = 1, N_dim

              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                              D_phi_q(id, i, iq) * Vc(:, i)

           ENDDO

        ENDDO
        
        CALL  Advection_Jacobian( N_dim, Vp_q, AA_q )

        Stab_right = 0.d0
        DO id = 1, N_dim

           Stab_right = Stab_right + &
                        MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )

        ENDDO

#ifdef NAVIER_STOKES

        temp = 0.d0
        DO i = 1, N_dofs

           CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_i )

           DO id = 1, N_dim

              KK_DVc = 0.d0
              DO  jd = 1, N_dim
                 KK_DVc = KK_DVc + &
                          MATMUL( KK_i(:,:, id,jd), Dr_Vc(jd, :, i) )
              ENDDO

              temp(id, :) = temp(id, :) + KK_DVc*D_phi_q(id, i, iq)
          
           ENDDO

        ENDDO

        D_KKD_Vc_q = 0.d0
        DO jd = 1, N_dim
           D_KKD_Vc_q = D_KKD_Vc_q + temp(jd, :)
        ENDDO

!---------------------------------------------------------------------------
!!$        ! OK
!!$        temp = 0.d0
!!$        DO i = 1, N_dofs
!!$
!!$           CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_i )
!!$
!!$           D_Vc_i = 0.d0
!!$           DO  jd = 1, N_dim
!!$              DO k = 1, N_dofs
!!$
!!$                 D_Vc_i(jd, :) = D_Vc_i(jd, :) + &
!!$                                 D_phi_k(jd, k, i)*Vc(:, k)
!!$
!!$              ENDDO
!!$           ENDDO
!!$
!!$           DO id = 1, N_dim
!!$
!!$              KK_DVc = 0.d0
!!$              DO  jd = 1, N_dim
!!$                 KK_DVc = KK_DVc + &
!!$                          MATMUL( KK_i(:,:, id,jd), D_Vc_i(jd, :) )
!!$              ENDDO
!!$
!!$              temp(id, :) = temp(id, :) + KK_DVc*D_phi_q(id, i, iq)
!!$          
!!$           ENDDO
!!$
!!$        ENDDO
!!$
!!$        D_KKD_Vc_q = 0.d0
!!$        DO jd = 1, N_dim
!!$           D_KKD_Vc_q = D_KKD_Vc_q + temp(jd, :)
!!$        ENDDO

!--------------------------------------------------------------------------

        Stab_right = Stab_right - D_KKD_Vc_q

        ! Source
        IF( turbulent ) THEN

           d_min_q = SUM(ele%d_min * phi_q(:, iq))

           CALL Source_term(Vp_q, D_Vc_q, d_min_q, SS_q)

           Stab_right = Stab_right - SS_q

        ENDIF
            
#endif

        DO i = 1, N_dofs

           Stab_left_i = 0.d0
           DO id = 1, N_dim

              Stab_left_i = Stab_left_i + &
                            AA_q(:,:, id) * D_phi_q(id, i, iq)

           ENDDO

           Phi_i(:, i) = Phi_i(:, i) +                &
                         w(iq) * MATMUL( Stab_left_i, &
                                         MATMUL(Tau, Stab_right) )

#ifdef NAVIER_STOKES

           !---------------------
           ! F.O.S. Stabilization
           !-----------------------------------------
           CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)
          
           ! Interpolated reconstructed gradient
           Dr_Vc_q = 0.d0
           DO k = 1, N_dofs

              DO id = 1, N_dim

                 Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                                  phi_q(k, iq) * Dr_Vc(id, :, k)

              ENDDO

           ENDDO

           DO id = 1, N_dim

              KKD_phi = 0.d0
              DO jd = 1, N_dim

                 KKD_phi = KKD_phi + &
                           KK_q(:,:, id, jd) * D_phi_q(jd, i, iq)
                
              ENDDO

              
              Phi_i(:, i) = Phi_i(:, i) +   &
                            w(iq) * MATMUL( KKD_phi, &
                                            D_Vc_q(id, :) - Dr_Vc_q(id, :) )

           ENDDO
           !-----------------------------------------

#endif

           !---------
           ! JACOBIAN
           !-----------------------------------------
           DO k = 1, N_dofs
             
              Stab_left_k = 0.d0
              DO id = 1, N_dim

                 Stab_left_k = Stab_left_k + &
                               AA_q(:,:, id) * D_phi_q(id, k, iq)

              ENDDO


#ifdef NAVIER_STOKES

              ! Should find a way to include the Jacobian
              ! of the second derivatives
              
              ! Source Term
              IF( turbulent ) THEN

                 Stab_left_k = Stab_left_k - phi_q(k, iq) * &
                               Jacobian_Source(Vp_q, D_Vc_q, d_min_q)

              ENDIF

#endif


              Jac_stab_LW(:,:, i, k) = Jac_stab_LW(:,:, i, k) +      &
                                       w(iq) * MATMUL( Stab_left_i,  &
                                               MATMUL(Tau, Stab_left_k) )

           ENDDO ! # dofs -> k

        ENDDO ! i <- N_dofs

     ENDDO ! iq <- N_quad
     !--------------------------------------

     NULLIFY( D_phi_k, D_phi_q, phi_q, w )

     !-----------------------
     ! Time stepping ~ |V|/Dt
     !-------------------------------------------
!!$     u_max = 0.d0; c_max = 0.d0
!!$     DO i = 1, N_dofs
!!$        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
!!$        c_max = MAX( c_max, Vp(i_son_vp, i) )
!!$     ENDDO
!!$
!!$     p_max = u_max + c_max
!!$
!!$     ! Element surface
!!$     S_ele = 0.d0
!!$     DO i = 1, ele%N_faces
!!$        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
!!$     ENDDO
!!$    
!!$     inv_dt = p_max*S_ele
!!$
!!$#ifdef NAVIER_STOKES
!!$
!!$     inv_dt = inv_dt + alpha_visc(ele, Vp)
!!$
!!$#endif
     u_max = 0.d0; c_max = 0.d0
     DO i = 1, N_dofs
        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
        c_max = MAX( c_max, Vp(i_son_vp, i) )
     ENDDO

     p_max = u_max + c_max

     ! Element surface
     S_ele = 0.d0
     DO i = 1, ele%N_faces
        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
     ENDDO

     h_e = r_N_dim * ele%Volume/S_ele

#ifdef NAVIER_STOKES
     Dt = h_e / ( p_max + alpha_visc(ele, Vp) )
#else
     Dt = h_e /   p_max
#endif

     Dt = CFL * Dt

     inv_dt = (ele%Volume)/Dt 
!     inv_dt = ele%Volume/Dt 
    
     !//////////////////////////////////////////////////
     !//         Add the local contribution to        //
     !//          the global Jacobian matrix          //
     !//////////////////////////////////////////////////

     !---------------
     ! Diagonal term
     !----------------------------------------------------
     C_Dt = inv_dt !/ CFL

     DO i = 1, N_dofs

        Diag_i = (JJ_tot(:,:, i) / r_N_dofs) + &
                  Jac_Stab_LW(:,:, i, i)

        DO j = 1, N_var
           Diag_i(j,j) = Diag_i(j,j) + C_Dt
        ENDDO

        CALL Assemblage_AddDiag(Diag_i, ele%NU(i), Mat)

     ENDDO

     !----------------------
     ! Extra-Diagonal terms 
     !---------------------------------------------------
     DO i = 1, N_dofs

        DO k = 1, N_dofs

           IF(k == i) CYCLE

           M_ik = (JJ_tot(:,:, k) / r_N_dofs) + &
                   Jac_Stab_LW(:,:, i, k)

           CALL Assemblage_Add(M_ik, ele%NU(i), ele%NU(k), Mat)

        ENDDO

     ENDDO     

     DEALLOCATE( Jac_Stab_LW )

   END SUBROUTINE LW_scheme_imp
   !===========================



   !========================================================
   SUBROUTINE LW_scheme_exp(ele, Vp, Vc, CFL, Phi_tot, Phi_i, &
                                              inv_dt, Dr_Vc)
   !========================================================

     IMPLICIT NONE

     TYPE(element_str),                INTENT(IN)  :: ele
     REAL, DIMENSION(:,:),             INTENT(IN)  :: Vp
     REAL, DIMENSION(:,:),             INTENT(IN)  :: VC
     REAL,                             INTENT(IN)  :: CFL
     REAL, DIMENSION(:),               INTENT(IN)  :: Phi_tot
     REAL, DIMENSION(:,:),             INTENT(OUT) :: Phi_i
     REAL,                             INTENT(OUT) :: inv_dt
     REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)  :: Dr_Vc
     !-----------------------------------------------------

     REAL, DIMENSION(SIZE(Vp, 1) ) :: Vp_b, Vp_q
     REAL, DIMENSION(Data%NVar)    :: SS_q

     REAL :: d_min_q

     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q
     REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_i, temp
     REAL, DIMENSION(Data%NDim, Data%NVar) :: Dr_Vc_q

     REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau, Tau_
     REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: Stab_left_i
     REAL, DIMENSION(Data%NVar, Data%Nvar) :: KKD_phi!, temp

     REAL, DIMENSION(Data%NVar) :: Stab_right

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q

     REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim, &
                                           Data%NDim) :: KK_q, KK_i

     REAL, DIMENSION(Data%Nvar) :: D_KKD_Vc_q, KK_DVc
!    REAL, DIMENSION(Data%Nvar) :: temp

     REAL, DIMENSION(Data%NVar) :: lambda
     !----------------------------------------------------

     REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
     REAL, DIMENSION(:,:,:), POINTER :: D_phi_q
     REAL, DIMENSION(:,:),   POINTER :: phi_q
     REAL, DIMENSION(:),     POINTER :: w  
     !----------------------------------------------------

     REAL, DIMENSION(Data%NDim) :: v_nn

     REAL :: r_N_dim, r_N_dofs, mod_n, tau_param
     REAL :: u_max, c_max, p_max, S_ele
     INTEGER :: N_var, N_dim, N_dofs, N_verts
     INTEGER :: i, j, iq, id, jd, k
     !----------------------------------------------------
    
     N_var = Data%NVar
     N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

     N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

     N_verts = ele%N_verts

     !---------------------
     ! Central distribution
     !-------------------------------------
     DO j = 1, N_var

        Phi_i(j, :) = Phi_tot(j)/r_N_dofs

     ENDDO
     !--------------------------------------

     !---------------
     ! Scaling Matrix
     !--------------------------------------
     Tau_ = 0.d0
    
     ! Mean state
     Vp_b = Mean_state_Vp(Vp)

     tau_param = 2.d0!REAL(Data%NOrdre-1, 8)

     inv_dt = 0.d0

     DO i = 1, N_verts !?
       
        mod_n = DSQRT( SUM(ele%rd_n(:, i)**2) )
       
        v_nn = ele%rd_n(:, i) / mod_n

        CALL Advection_eigenvalues( Vp_b, v_nn, lambda)
        CALL Advection_eigenvectors(Vp_b, v_nn, RR, LL)

        Pos = 0.d0; p_max = 0.d0
        DO j = 1, N_var

           Pos(j, j) = tau_param*MAX(lambda(j), 0.d0) * mod_n

           p_max = MAX( p_max, Pos(j,j) )

        ENDDO

        Tau_ = Tau_ + 0.5d0 * MATMUL(RR, MATMUL(Pos, LL))
        
     ENDDO

#ifdef NAVIER_STOKES

    CALL Diffusion_Jacobian(N_dim, Vp_b, KK_q)

    DO id = 1, N_dim

       Tau_ = Tau_ + KK_q(:,:, id, id)!*r_N_dim 

    ENDDO

#endif

     Tau = (1.d0/r_N_Dim) * ele%Volume * InverseLU(Tau_)
     !--------------------------------------

     !-------------------
     ! Stabilization term
     !--------------------------------------
     D_phi_k => ele%D_phi_k  
     D_phi_q => ele%D_phi_q
       phi_q => ele%phi_q
           w => ele%w_q
          
     DO iq = 1, ele%N_quad
       
        Vp_q = Interp_Vp( Vp, phi_q(:, iq) )

        D_Vc_q = 0.d0

        DO i = 1, N_dofs
          
           DO id = 1, N_dim

              D_Vc_q(id, :) = D_Vc_q(id, :) + &
                              D_phi_q(id, i, iq) * Vc(:, i)

           ENDDO

        ENDDO
        
        CALL  Advection_Jacobian( N_dim, Vp_q, AA_q )

        Stab_right = 0.d0
        DO id = 1, N_dim

           Stab_right = Stab_right + &
                        MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )

        ENDDO

#ifdef NAVIER_STOKES

        temp = 0.d0
        DO i = 1, N_dofs

           CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_i )

           DO id = 1, N_dim

              KK_DVc = 0.d0
              DO  jd = 1, N_dim
                 KK_DVc = KK_DVc + &
                          MATMUL( KK_i(:,:, id,jd), Dr_Vc(jd, :, i) )
              ENDDO

              temp(id, :) = temp(id, :) + KK_DVc*D_phi_q(id, i, iq)
          
           ENDDO

        ENDDO

        D_KKD_Vc_q = 0.d0
        DO jd = 1, N_dim
           D_KKD_Vc_q = D_KKD_Vc_q + temp(jd, :)
        ENDDO

!-----------------------------------------------------------------------
!!$        ! OK
!!$        temp = 0.d0
!!$        DO i = 1, N_dofs
!!$
!!$           CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_i )
!!$
!!$           D_Vc_i = 0.d0
!!$           DO  jd = 1, N_dim
!!$              DO k = 1, N_dofs
!!$
!!$                 D_Vc_i(jd, :) = D_Vc_i(jd, :) + &
!!$                                 D_phi_k(jd, k, i)*Vc(:, k)
!!$
!!$              ENDDO
!!$           ENDDO
!!$
!!$           DO id = 1, N_dim
!!$
!!$              KK_DVc = 0.d0
!!$              DO  jd = 1, N_dim
!!$                 KK_DVc = KK_DVc + &
!!$                          MATMUL( KK_i(:,:, id,jd), D_Vc_i(jd, :) )
!!$              ENDDO
!!$
!!$              temp(id, :) = temp(id, :) + KK_DVc*D_phi_q(id, i, iq)
!!$          
!!$           ENDDO
!!$
!!$        ENDDO
!!$
!!$        D_KKD_Vc_q = 0.d0
!!$        DO jd = 1, N_dim
!!$           D_KKD_Vc_q = D_KKD_Vc_q + temp(jd, :)
!!$        ENDDO
!-----------------------------------------------------------------------
 
        Stab_right = Stab_right - D_KKD_Vc_q

       ! Source
       IF( turbulent ) THEN

          d_min_q = SUM(ele%d_min * phi_q(:, iq))

          CALL Source_term(Vp_q, D_Vc_q, d_min_q, SS_q)

          Stab_right = Stab_right - SS_q

       ENDIF

#endif

        DO i = 1, N_dofs

           Stab_left_i = 0.d0
           DO id = 1, N_dim

              Stab_left_i = Stab_left_i + &
                            AA_q(:,:, id) * D_phi_q(id, i, iq)

           ENDDO

           Phi_i(:, i) = Phi_i(:, i) +                &
                         w(iq) * MATMUL( Stab_left_i, &
                                         MATMUL(Tau, Stab_right) )

#ifdef NAVIER_STOKES

           !---------------------
           ! F.O.S. Stabilization
           !-----------------------------------------
           CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)
          
           ! Interpolated reconstructed gradient
           Dr_Vc_q = 0.d0
           DO k = 1, N_dofs

              DO id = 1, N_dim

                 Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                                  phi_q(k, iq) * Dr_Vc(id, :, k)

              ENDDO

           ENDDO

           DO id = 1, N_dim

              KKD_phi = 0.d0
              DO jd = 1, N_dim

                 KKD_phi = KKD_phi + &
                           KK_q(:,:, id, jd) * D_phi_q(jd, i, iq)
                
              ENDDO
             
              Phi_i(:, i) = Phi_i(:, i) +            &
                            w(iq) * MATMUL( KKD_phi, &
                                            D_Vc_q(id, :) - Dr_Vc_q(id, :) )
 

           ENDDO
           !-----------------------------------------
#endif
 
        ENDDO ! i <- N_dofs

     ENDDO ! iq <- N_quad
     !--------------------------------------

     NULLIFY( D_phi_k, D_phi_q, phi_q, w )

     !-----------------------
     ! Time stepping ~ |V|/Dt
     !-------------------------------------------
     u_max = 0.d0; c_max = 0.d0
     DO i = 1, N_dofs
        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
        c_max = MAX( c_max, Vp(i_son_vp, i) )
     ENDDO

     p_max = u_max + c_max

     ! Element surface
     S_ele = 0.d0
     DO i = 1, ele%N_faces
        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
     ENDDO
    
     inv_dt = p_max*S_ele

#ifdef NAVIER_STOKES

     inv_dt = inv_dt + alpha_visc(ele, Vp)

#endif

   END SUBROUTINE LW_scheme_exp
   !===========================

END MODULE Scheme_LW

