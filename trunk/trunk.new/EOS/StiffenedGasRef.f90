!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Mathématiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Université Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   MODULE stiffenedgasref
      USE LesTypes
      USE LibComm
      IMPLICIT NONE
!          read(MyUnit,*)Data%EosPar(ifl)%Coef(1) ! gamma
!          read(MyUnit,*)Data%EosPar(ifl)%Coef(2) ! Pc
!          read(MyUnit,*)Data%EosPar(ifl)%Coef(3) ! Cv
!          read(MyUnit,*)Data%EosPar(ifl)%Coef(4) ! rho_ref
!          read(MyUnit,*)Data%EosPar(ifl)%Coef(5) ! P_ref
!          read(MyUnit,*)Data%EosPar(ifl)%Coef(6) ! E_ref
!          read(MyUnit,*)Data%EosPar(ifl)%Coef(7) ! T_ref
      REAL, PRIVATE, SAVE :: gama
      REAL, PRIVATE, SAVE :: gama1
      REAL, PRIVATE, SAVE :: pc
      REAL, PRIVATE, SAVE :: cv
      REAL, PRIVATE, SAVE :: rho0
      REAL, PRIVATE, SAVE :: p0
      REAL, PRIVATE, SAVE :: e0
      REAL, PRIVATE, SAVE :: t0
      INTEGER, PRIVATE, SAVE :: ndim, nvar

   CONTAINS

      SUBROUTINE SGRef_INIt(MyUnit, Dim, Var)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_INIt"
         INTEGER, INTENT( IN) ::MyUnit, Dim, Var

            READ(MyUnit, *)gama
            READ(MyUnit, *)pc
            READ(MyUnit, *)cv
            READ(MyUnit, *)rho0
            READ(MyUnit, *)p0
            READ(MyUnit, *)e0
            READ(MyUnit, *)t0
         gama1 = gama -1.0
         ndim = Dim
         nvar = Var
      END SUBROUTINE SGRef_INIt

      REAL FUNCTION SGRef_Kappa() RESULT(k)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_Kappa"
         k  = gama1
         RETURN
      END FUNCTION SGRef_Kappa


      REAL FUNCTION SGRef_Cp() RESULT(k)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_Cp"
    !Cp=gama*Cv
         k  = gama * cv
         RETURN
      END FUNCTION SGRef_Cp


      REAL FUNCTION SGRef_RhoEpsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_RhoEpsilon"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         REAL   :: rho
         rho     = Vp(1)
         re  = rho * e0   +  (gama * pc + Vp(nvar + 1)  - rho / rho0 * (p0 + gama * pc)) / gama1
         RETURN
      END FUNCTION SGRef_RhoEpsilon

      REAL FUNCTION SGRef_Epsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_Epsilon"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         REAL   :: rho
         rho     = Vp(1)
         re  = e0   +  (gama * pc + Vp(nvar + 1)  - rho / rho0 * (p0 + gama * pc)) / gama1 / rho
         RETURN
      END FUNCTION SGRef_Epsilon

      REAL FUNCTION SGRef_EpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_EpsilonDet"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         REAL   :: rho, epsi0, S_inf
         rho     = Vp(1)
         S_inf   = (p0 + pc) / rho ** gama - gama1 / rho0 ** gama1 * cv * t0
         epsi0   = e0  + pc / rho - (p0 + gama * pc) / (gama1 * rho0) + rho ** gama1 / gama1 * S_inf
         are  = cv * Vp(nvar + 3) + epsi0
         RETURN
      END FUNCTION SGRef_EpsilonDet


      REAL FUNCTION SGRef_RhoEpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_RhoEpsilonDet"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         REAL   :: rho, epsi0, S_inf
         rho     = Vp(1)
         S_inf   = (p0 + pc) / rho ** gama - gama1 / rho0 ** gama1 * cv * t0
         epsi0   = e0  + pc / rho - (p0 + gama * pc) / (gama1 * rho0) + rho ** gama1 / gama1 * S_inf
         are  = rho * (cv * Vp(nvar + 3) + epsi0)
         RETURN
      END FUNCTION SGRef_RhoEpsilonDet


      REAL FUNCTION SGRef_Rhoh(Vp) RESULT(rh)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_Rhoh"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         REAL   :: rho
         rho     = Vp(1)
         rh  = rho * e0 + (gama * (Vp(nvar + 1) + pc) - rho / rho0 * (p0 + gama * pc)) / gama1
         RETURN
      END FUNCTION SGRef_Rhoh


      REAL FUNCTION SGRef_Temperature(Vp) RESULT(Temp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_Temperature"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! T =(epsilon-Pc/rho)/cv
         REAL   :: Epsilon, rho, epsi0, S_inf
    !           if (Ua(ifl,1,is) /= 0 .and. Ua(ifl,2,is)/=0) then
         rho  = Vp(1)
         Epsilon = Vp(2 + ndim)
         S_inf   = (p0 + pc) / rho ** gama - gama1 / rho0 ** gama1 * cv * t0
         epsi0   = e0  + pc / rho - (p0 + gama * pc) / (gama1 * rho0) + rho ** gama1 / gama1 * S_inf
         Temp = (Epsilon  -  epsi0) / cv
         RETURN
      END FUNCTION SGRef_Temperature


      REAL FUNCTION SGRef_Pression(Vp) RESULT(p)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_Pression"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! P = (gama - 1)*rho*epsilon - gama*Pc
         REAL   :: Epsilon, rho
    !           if (Ua(ifl,1,is) /= 0 .and. Ua(ifl,2,is)/=0) then
         rho  = Vp(1)
         Epsilon = Vp(2 + ndim)
         p = - gama * pc + rho * (  gama1 * (Epsilon - e0) + (p0 + gama * pc) / rho0)
         RETURN
      END FUNCTION SGRef_Pression


      REAL FUNCTION SGRef_Son(Vp) RESULT (c)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_Son"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
         c = gama * (Vp(nvar + 1) + pc) / Vp(1) ! C^2
         c = Sqrt( c )
!    print*,rho,p,pc,gama,c
         RETURN
      END FUNCTION SGRef_Son

      SUBROUTINE SGRef_StiffenedGasRef(Vp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGRef_StiffenedGasRef"
         REAL, DIMENSION(: ), INTENT(INOUT) :: Vp
     ! P = (gama - 1)*rho*epsilon - gama*Pc
         REAL   :: Epsilon, rho
         REAL   :: epsi0, S_inf
         rho  = Vp(1)
         Epsilon = Vp(2 + ndim)

         S_inf   = (p0 + pc) / rho ** gama - gama1 / rho0 ** gama1 * cv * t0
         epsi0   = e0  + pc / rho - (p0 + gama * pc) / (gama1 * rho0) + rho ** gama1 / gama1 * S_inf

         Vp(nvar + 1) = rho * ( gama1 * (Epsilon - e0) + (p0 + gama * pc) / rho0) - gama * pc
         Vp(nvar + 2) = Sqrt( (gama * (Vp(nvar + 1) + pc) ) / rho)
         Vp(nvar + 3) = (Epsilon - epsi0) / cv
      END SUBROUTINE SGRef_StiffenedGasRef
   END MODULE stiffenedgasref
