
!----------------------------------------------------------------------------
  !-------------BEGINNING Vp FONCTIONS-----------------------------!

  !-------------ENDING Vp FONCTIONS-----------------------------!
!============================================================================
!----------------------------------------------------------------------------




MODULE perfectgas

  USE LesTypes
  USE LibComm

  IMPLICIT NONE

  REAL,    PRIVATE, SAVE :: R_gas ! dimensionless
  REAL,    PRIVATE, SAVE :: gama  
  REAL,    PRIVATE, SAVE :: gama1


  !***********************************************
  !       FILE EOS_1.data ; Perfect Gas Case     *
  !***********************************************
  !"GP"               !EosLaw                    * 
  !1.4                !gama, perfect gas constant*
  !***********************************************

CONTAINS

  !===================================
  SUBROUTINE gp_init(MyUnit, Dim, Var)
    !===================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "gp_init"

    INTEGER, INTENT(IN) :: MyUnit, Dim, Var
    !---------------------------------------

    INTEGER :: statut
    !---------------------------------------

    READ(MyUnit, *, IOSTAT = statut) gama

    gama1 = gama - 1.d0

    ! Dimensionless gas constant
    R_gas = 1.d0/gama

  END SUBROUTINE gp_init
  !=====================


!============================================================================
!============================================================================
  !BEGIN :           basic FUNCTIONS

  !Description
  !-------------------------------
  ! > GP_P__rho_T(rho,T)          P = P(rho  , T       )  
  ! > GP_T__rho_P(rho,P)          T = T(rho  , P       )
  ! > GP_T__rho_rhoe(rho,rhoe)    T = T(rho  , rho*e   ),     e := internal energy
  ! > GP_rho__P_T(P,T)            rho = rho(P  , T       )  
  ! > GP_c__rho_T(rho,T)          c = c(rho  , T       )
  ! > GP_e__rho_T(rho,T)          e = e(rho  , T       ),     e := internal energy
  ! > GP_s__rho_T(rho,T)          s = s(rho  , T       ),     s := entropy
  ! > GP_P__rho_rhoU_rhoE(rho,rhoU,rhoE)     P = P(rho, rho*|u|, rho*E)  E := total energy = e + |u|**2/2
  ! > GP_dP_de__rho_T(rho,T)                (dP/de)|rho    = dP/de(rho, T)   e:= internal energy
  ! > GP_dP_drho__rho_T(rho,T)              (dP/drho)|e    = dP/drho(rho, T)   e:= internal energy
  ! > GP_pi_rho__rho_u_T(rho,u,T)           (dPI/drho)     = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > GP_pi_rhoe__rho_T(rho,T)               dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > GP_GAMMA__rho_T(rho,T)                 GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 
  ! > GP_de_drho__rho_T(rho,T)              (de/drho)|T    = de/drho(rho,T)
  ! > GP_dT_de__rho_T(rho, T)               (dT/de)_rho    = dT/de(rho, T)
  ! > GP_dT_drho__rho_T(rho, T)             (dT/drho)_e    = dT/drho(rho, T)
  ! > GP_dP2__dT_dv(rho, T)                  d^2P/dT^2|_v & d^2P/dv^2|_T & d^2P/dvdT|_T

  !==========================================
  REAL FUNCTION GP_P__rho_T(rho, T) RESULT(P)
    !==========================================
    ! > P = P(rho,T)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    P = rho*R_gas*T

  END FUNCTION GP_P__rho_T
  !=======================

  !==========================================
  REAL FUNCTION GP_T__rho_P(rho, P) RESULT(T)
    !==========================================
    ! > T = T(rho,P)

    REAL, INTENT(IN) :: rho, P
    !-------------------------

    T = P / (rho*R_gas)

  END FUNCTION GP_T__rho_P
  !=======================

  !==========================================
  REAL FUNCTION GP_T__rho_rhoe(rho, rhoe) RESULT(T)
    !==========================================
    ! >  T = T(rho, rho*e), e := intern energy

    REAL, INTENT(IN) :: rho, rhoe
    !-------------------------

    T = rhoe / (rho*GP_cv())

  END FUNCTION GP_T__rho_rhoe
  !=======================

  !==========================================
  REAL FUNCTION GP_rho__P_T(P, T) RESULT(rho)
    !==========================================
    ! >  rho = rho(P,T)  

    REAL, INTENT(IN) :: P, T
    !-------------------------

    rho = P/(R_gas*T)

  END FUNCTION GP_rho__P_T
  !=======================


  !==========================================
  REAL FUNCTION GP_c__rho_T(rho, T) RESULT(c)
    !==========================================
    !  > c = c(rho,T)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    c = SQRT(gama*R_gas*T)

  END FUNCTION GP_c__rho_T
  !=======================

  !==========================================
  REAL FUNCTION GP_e__rho_T(rho, T) RESULT(e)
    !==========================================
    !  > e = e(rho,T), e := internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    e = GP_cv()*T

  END FUNCTION GP_e__rho_T
  !=======================

  !==========================================
  REAL FUNCTION GP_s__rho_T(rho, T) RESULT(s)
    !==========================================
    !  > s = s(rho,T), s := specific entropy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    s = GP_cv()* ( LOG ( T ) -(gama-1.0) * LOG ( rho ) )

  END FUNCTION GP_s__rho_T
  !=======================

 !==========================================
  REAL FUNCTION GP_T__rho_s(rho, s) RESULT(T)
    !==========================================
    !  > T = T(rho,s), T := temperature

    REAL, INTENT(IN) :: rho, s
    !-------------------------

    T = exp( s/GP_cv()) * (rho **(gama-1.0))

  END FUNCTION GP_T__rho_s
  !=======================

 !==========================================
  REAL FUNCTION GP_rho__s_T( s, T) RESULT(rho)
    !==========================================
    !  > T = T(rho,s), T := temperature

    REAL, INTENT(IN) ::  s, T
    real             :: dummy
    !-------------------------
    dummy = log(T)/(gama-1.0)- s/(GP_cv() *( gama-1.0))
    rho = exp( dummy)

  END FUNCTION GP_rho__s_T
  !=======================
  !==========================================
  REAL FUNCTION GP_P__rho_rhoU_rhoE(rho, rhoU, rhoE) RESULT(P)
    !==========================================
    !  >  P = P(rho, rho*|u|, rho*E) E total energy 

    REAL, INTENT(IN) :: rho, rhoU, rhoE
    !-------------------------

    REAL             :: e 
    !-------------------------

    e = rhoE/rho - 0.5*(rhoU/rho)**2
    P = rho*e*(gama - 1)


  END FUNCTION GP_P__rho_rhoU_rhoE
  !=======================

  !==========================================
  REAL FUNCTION GP_dP_de__rho_T(rho, T) RESULT(d)
    !==========================================
    !  > (dP/de)|rho = dP/de(rho, T)   e:= internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    d = (gama - 1)*rho

  END FUNCTION GP_dP_de__rho_T
  !=======================

  !==========================================
  REAL FUNCTION GP_dP_drho__rho_T(rho, T) RESULT(d)
    !==========================================
    !  > (dP/drho)|e = dP/drho(rho, T)   e:= internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    d = (gama - 1)*GP_e__rho_T(rho, T)

  END FUNCTION GP_dP_drho__rho_T
  !=======================

  !==========================================
  REAL FUNCTION GP_pi_rho__rho_u_T(rho, u, T) RESULT(pi)
    !==========================================
    !  > (dPI/drho) = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    REAL, INTENT(IN) :: rho, u, T
    !-------------------------

    REAL             :: e, dRho, dE
    !-------------------------

    e    = GP_e__rho_T(rho, T)
    dRho = GP_dP_drho__rho_T(rho, T)
    dE   = GP_dP_de__rho_T(rho, T)

    pi = dRho + (0.5*u*u - e)*dE/rho 

  END FUNCTION GP_pi_rho__rho_u_T
  !=======================

  !==========================================
  REAL FUNCTION GP_pi_rhoe__rho_T(rho, T) RESULT(pi)
    !==========================================
    !  > dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    pi = GP_dP_de__rho_T(rho, T) / rho

  END FUNCTION GP_pi_rhoe__rho_T
  !=======================

  !==========================================
  REAL FUNCTION GP_GAMMA__rho_T(rho, T) RESULT(G)
    !==========================================
    !  > GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    G = (gama + 1.d0)/2.d0

  END FUNCTION GP_GAMMA__rho_T
  !=======================


  !==========================================
  REAL FUNCTION GP_de_drho__rho_T(rho,T) RESULT(d)
    !==========================================
    ! > GP_de_drho__rho_T(rho,T)                (de/drho)|rho    = de/drho(rho,T)
    ! > d = 0

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    d = 0.d0

  END FUNCTION GP_de_drho__rho_T
  !=======================

  
  !===========================================
  FUNCTION GP_dT_drho__rho_T(rho, T) RESULT(d)
  !===========================================
  !
  !                 -(de/drho)|_T      -(de/drho)|_T 
  ! (dT/drho)|_e = ---------------- = ---------------- = 0
  !                  (de/dT)|_rho          Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    REAL             :: d
    !-------------------------

    REAL :: de_drho, c_v
    !-------------------------
    
!!$    de_drho = GP_de_drho__rho_T(rho, T)
!!$
!!$    c_v = GP_Cv()
!!$
!!$    d = -de_drho / c_v

    d = 0.d0

  END FUNCTION GP_dT_drho__rho_T
  !=============================

  !=========================================
  FUNCTION GP_dT_de__rho_T(rho, T) RESULT(d)
  !=========================================
  !
  !                         1          1
  ! (dT/de)|_rho = ---------------- = ----
  !                  (de/dT)|_rho      Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    REAL             :: d 
    !-------------------------

    REAL :: c_v
    !-------------------------
    
    c_v = GP_Cv()

    d = 1.d0 / c_v

  END FUNCTION GP_dT_de__rho_T
  !===========================

  !=======================================
  FUNCTION GP_dP__dT_dv(rho, T) RESULT(dP)
  !=======================================
  ! 
  ! dP(1) = dP/dT|_v,   dP(2) = dP/dv|_T
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: dP
    !-------------------------

    REAL :: v
    !-------------------------

    v = 1.d0 / rho
    
    dP(1) = R_gas/v

    dP(2) = -R_gas*T/v**2

  END FUNCTION GP_dP__dT_dv  
  !========================

  !=========================================
  FUNCTION GP_dP2__dT_dv(rho, T) RESULT(dP2)
  !=========================================
  ! 
  ! dP(1) = d^2P / dT^2|_v  
  ! dP(2) = d^2P / dv^2|_T 
  ! dP(3) = d^2P / dvdT|_vT
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(3) :: dP2
    !-------------------------

    REAL :: v
    !-------------------------

    v = 1.d0 / rho
    
    dP2(1) = 0.d0

    dP2(2) = -2.d0*R_gas*T/v**3

    dP2(3) = -R_gas/v**2

  END FUNCTION GP_dP2__dT_dv
  !=========================

  !---------------END basic FUNCTIONS          -----------------------------!
!============================================================================
!============================================================================


!============================================================================
!============================================================================
  !BEGIN :           derivated FUNCTIONS

  !Description
  ! > GP_c__rho_e(rho, e)          c  = c(rho,e)
  ! > GP_Temperature(Vp)           T  = T(rho,e)
  ! > GP_Pression(Vp)              P  = P(rho,e)
  ! > GP_Epsilon(Vp)               e  = e(rho,P)
  ! > GP_EpsilonDet(Vp)            e  = e(rho,T)
  ! > GP_Son(Vp)                   c  = c(rho,P)
  ! > GP_enthalpy(Vp)              h  = h(rho,P)
  ! > GP_Gas(Vp)            Vp = Vp(rho,e) "subroutine" computes all Vp components from (rho,e) <- Vp

    !==========================================
  REAL FUNCTION GP_c__rho_e(rho, e) RESULT(c)
    !==========================================
    ! > c = c(rho, e)
    
    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: e

    REAL :: T
    !-------------------------

    !INTERMEDIATE CALCULATION
    T = GP_T__rho_rhoe(rho,rho*e)

    !RESULT
    c = GP_c__rho_T(rho,T)

  END FUNCTION GP_c__rho_e
  !=======================  


  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !============================================
  REAL FUNCTION GP_Temperature(Vp) RESULT(T)
    !============================================
    ! > T = T(rho, e)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GP_Temperature"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, e
    !------------------------------------

    ! DATA
    e     = Vp(i_eps_vp) 
    rho   = Vp(i_rho_vp)

    ! RESULT
    T     = GP_T__rho_rhoe(rho,rho*e)

  END FUNCTION GP_Temperature
  !==========================


  !======================================
  REAL FUNCTION GP_Pression(Vp) RESULT(P)
    !======================================
    ! P = P (e, rho)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GP_Pression"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: e, rho, T
    !-------------------------------------

    !DATA
    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)    ! �nergie interne

    !INTERMEDIATE CALCULATION
    T       = GP_T__rho_rhoe(rho,rho*e)

    !RESULT
    P       = GP_P__rho_T(rho,T)

  END FUNCTION GP_Pression
  !=======================

  !======================================
  REAL FUNCTION GP_Epsilon(Vp) RESULT(e)
    !======================================
    ! > e = e(rho,P)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GP_Epsilon"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T   = GP_T__rho_P(rho,P)

    !RESULT
    e   = GP_e__rho_T(rho,T)

  END FUNCTION GP_Epsilon
  !======================

  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !==========================================
  REAL FUNCTION GP_EpsilonDet(Vp) RESULT(e)
    !==========================================
    ! > e = e(rho, T)

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    T   = Vp(i_tem_vp)

    ! e = e(rho, T)
    e   = GP_e__rho_T(rho,T)

  END FUNCTION GP_EpsilonDet
  !=========================


  !==================================
  REAL FUNCTION GP_Son(Vp) RESULT (c)
    !==================================
    ! c = c(P, rho)

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T = GP_T__rho_P(rho,P)

    !RESULT
    c = GP_c__rho_T(rho,T)

  END FUNCTION GP_Son
  !==================


  !======================================
  REAL FUNCTION GP_enthalpy(Vp) RESULT(h)
    !======================================
    ! > h = h (P, rho)

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T, e
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !CALCULATION
    T = GP_T__rho_P(rho,P)
    e = GP_e__rho_T(rho,T)

    !RESULT
    h = e + P/rho

  END FUNCTION GP_Enthalpy
  !=======================

  !===========================
  SUBROUTINE GP_Gas(Vp)
    !===========================
    ! > (T,P,c) in function of (rho, e), e:= internal energy

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GP_Gas"

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp
    !--------------------------------------

    REAL :: e, rho, P, T, c
    !---------------------------------------

    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)  ! �nergie interne i_eps_vp

    !CALCULATION
    T       = GP_T__rho_rhoe(rho,rho*e)         !-- Temperature
    P       = GP_P__rho_T(rho,T)                !-- Pressure
    c       = GP_c__rho_T(rho,T)                !-- Sound Speed

    !RESULT
    Vp(i_pre_vp) = P 
    Vp(i_tem_vp) = T
    Vp(i_son_vp) = c

  END SUBROUTINE GP_Gas
  !===========================


  !-------------ENDING derivated FONCTIONS     -----------------------------!
!----------------------------------------------------------------------------
!============================================================================



!============================================================================
!----------------------------------------------------------------------------
  !-------------BEGINNING useful specific FONCTIONS                  -------!

  !==========================
  FUNCTION gp_cv() RESULT(cv)
  !==========================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "gp_cv"
    
    REAL :: cv
    !---------

    !cv = cv_dim ! dim

    ! dimensionless Cv
    cv = 1.0/(gama*(gama - 1.d0))

  END FUNCTION gp_cv
  !=================

  !====================================
  FUNCTION GP_dCv__dT_dv() RESULT(d_Cv)
  !====================================
  ! 
  ! d_Cv(1) = dCv/dT|_rho
  ! d_Cv(2) = dCv/drho|_T
  !
    IMPLICIT NONE

    REAL, DIMENSION(2) :: d_Cv
    !-------------------------

    d_Cv = 0.d0

  END FUNCTION GP_dCv__dT_dv
  !=========================

  ! TO BE REMOVED XXX
  !==============================
  FUNCTION gp_d_cv() RESULT(d_cv)
  !==============================
    
    IMPLICIT NONE
    
    CHARACTER(LEN = *), PARAMETER :: mod_name = "gp_d_cv"
    
    REAL :: d_cv
    !-----------
    
    !cv = cv_dim ! dim

    ! dimensionless Cv
    d_cv = 0.d0

  END FUNCTION gp_d_cv  
  !===================

  !Used in MODELS/OrdreEleve/ModelInterfaces.f90
  !==========================================
  REAL FUNCTION GP_T__rho_c(rho, c) RESULT(T)
    !==========================================

    REAL, INTENT(IN) :: rho, c
    !-------------------------

    T = c*c / (gama*R_gas)

  END FUNCTION GP_T__rho_c
  !=======================


  !-------------ENDING useful specific FONCTIONS                  ----------!
!============================================================================
!----------------------------------------------------------------------------

!============================================================================
!----------------------------------------------------------------------------
  !-------------BEGINNING : useful to modify-----------------------------!


!=================================
  REAL FUNCTION GP_Kappa() RESULT(k)
    !=================================

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GP_Kappa"

    !Kappa = gamma - 1.0
    k = gama1

  END FUNCTION GP_Kappa
  !====================





  !-------------ENDING    : useful to modify-----------------------------!
!============================================================================
!----------------------------------------------------------------------------



END MODULE perfectgas
