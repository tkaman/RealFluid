MODULE EOSLaws

  USE Lestypes
  USE types_enumeres
  USE stiffenedgas
  USE stiffenedgasref
  USE stiffGasMf
  USE Perfectgas
  USE Incompgas
  USE VanDerWaals
  USE PRSV
  USE SW
  USE LibComm  

  IMPLICIT NONE

  CHARACTER(LEN = 20), SAVE :: EosLaw

  INTEGER, PUBLIC,  SAVE :: sel_EosLaw = -1
  INTEGER, PRIVATE, SAVE :: nvar
  !> \brief
  !!====================================================================
  !!====================================================================
  !! >>DESCRIPTION DES FUNCTIONS/SUBROUTINES IMPLEMENT�

  !!-- BEGIN  :: FUNCTIONS of primitive variables (T,P,rho,e,c)
  !!Description
  !!-------------------------------
  !! > P__rho_T(rho, T)            P = P(rho,T)
  !! > T__rho_P(rho, P)            T = T(rho,P)   **Couteuse : m�thode Newton
  !! > rho__P_T(P  , T)          rho = rho(P,T)   **Couteuse : Newton (PRSV - SW)
  !! > e__rho_T(rho, T)            e = e(rho,T)
  !! > c__rho_T(rho, T)            c = c(rho,T)
  !! > s__rho_T(rho, T)            s = s(rho,T)   ** PRSV - SW
  !! > T__rho_c                    T = T(rho,c)   ** A JOUR QUE POUR PAZ PARFAIT
  !! > rho__s_T(s  , T)          rho = rho(s,T)   ** PRSV - SW
  !! > T__rho_s(rho, s)            T = T(rho,s)   **Couteuse : Newton (PRSV - SW)
  !! > GAMMA__rho_T(rho,T)         G = G(rho,T)   ** Fondamental Derivative
  !! > EOS_de_dT__rho_T(rho, T)    de/dT|rho = Cv(rho,T)
  !! > EOS_de_drho__rho_T(rho, T)  de/drho|T (rho,T)
  !! > EOS_d_Cv_T(rho,T)           dCv/dT|rho
  !! > EOS_d_Cv_rho(rho,T)         dCv/drho|T
  !! > EOS_Zc()                    Zc=Pc.Tc/R.vc  (depends on the model)
  !! > EOS_omega()                 acentric factor  (depends on the model)
  !! > EOSKappa()                  VALABLE QUE EN GP 
  !! > EOS_TEICALC(h, rho, c1, c2) Calcul sp�cifique entr�e turbine (lgc = 91)
  !! > EOS_dP__dT_dv(rho, T)       dP/dT|_v & dP/dv|_T


  !!BEGIN :           SUBROUTINES/FUNCTIONS of Vp

  !!Description
  !! > EOSEos(Vp)                   Vp = Vp(rho,e) "subroutine" computes all Vp components from (rho,e) <- Vp
  !! > EOS_d_PI_rho(Vp)            (dPI/drho)     = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  !! > EOS_d_PI_E(Vp)               dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e) 
  !! > EOSCv(Vp)                    Cv = Cv(rho,T)
  !! > EOSCp(Vp)                    Cp = Cp(rho,T)
  !! > EOSEpsilon(Vp)               e  = e(rho,P)
  !! > EOSEpsilonDet(Vp)            e  = e(rho,T)
  !! > EOSTemperature(Vp)           T  = T(rho,e)
  !! > EOSpression(Vp)              P  = P(rho,e)
  !! > EOSson(Vp)                   c  = c(rho,P)
  !! > EOSenthalpy(Vp)              h  = h(rho,P)

  !! >>END
  !!====================================================================
  !!====================================================================
  !!              Vp = (r,u,v,[w,]e,p,c,T)
  !!  Turbulence: Vp = (r,u,v,[w,]e,nut,p,c,T)

CONTAINS
  !> \brief
  !!- Lecture de EOS_1.data
  !!- 
  !!-- Lois d'�tat possibles 
  !!-  >  "GP"   Gaz Parfait
  !!-  >  "VDW"  Van Der Waals
  !!-  >  "PRSV" Peng Robinson
  !!-  >  "SW"   Span Wagner

  !!-- On v�rifie que la loi lue sur UNIT=11 FILE=EOS_<numero>.data est la m�me que LoiEtat
  !!-- Si c'est le cas, la loi stock�e dans EosLaw (chaine caract�res), le code dans sel_EosLaw (entier) 

  !!-- Selon la loi choisie, la loi est initialis�e via UNIT=11 FILE=EOS_<numero>.data

  !================================================
  SUBROUTINE EOSInit(Dim, Nv, LoiEtat, Index, DATA)
    !================================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSInit"
    TYPE(Donnees),        INTENT(IN) :: DATA
    INTEGER,              INTENT(IN) :: Dim, Index, Nv
    CHARACTER(LEN = 20),  INTENT(IN) :: LoiEtat
    !---------------------------------------------------

    CHARACTER(LEN = 70)  :: FileName
    LOGICAL :: existe
    INTEGER :: statut, sel_temp, MyUnit, seen
    !----------------------------------------------------

    !sel_temp contient le code de LoiEtat
    Nvar = Nv
    CALL decoder_data_loietat(LoiEtat, sel_temp)

    !-------------
    !Lecture UNIT=11 FILE=EOS_<numero>.data
    !-------------
    MyUnit = 11
    FileName(1: ) = "EOS_" // CHAR(48 + Index) // ".data"

    !Test l'existence du fichier
    INQUIRE(FILE = FileName, EXIST = existe)
    IF (.NOT. existe) THEN
       PRINT *, mod_name, " :: ERREUR : pas de fichier ", TRIM(FileName)
       CALL stop_run()
    END IF

    !Ouverture fichier EOS_<numero>.data
    OPEN(UNIT = MyUnit, FILE = FileName, FORM = "formatted", STATUS = "old", IOSTAT = statut )
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " ouvrir ", TRIM(FileName)
       CALL stop_run()
    END IF

    !Lecture de la loi d'�tat : EosLaw
    READ(MyUnit, *, IOSTAT = statut) EosLaw
    IF (statut /= 0) THEN
       PRINT *, mod_name, " :: ERREUR : statut == ", statut, " pour la 1ere ligne de ", TRIM(FileName)
       CALL stop_run()
    END IF

    !sel_EosLaw est le code de EosLaw
    CALL decoder_data_loietat(EosLaw, sel_EosLaw)

    !Test la coh�rence de EosLaw et LoiEtat
    IF (sel_temp /= sel_EosLaw) THEN
       PRINT *, mod_name, " : ERREUR : Incoherence entre la loi donnee dans EOS_1.data et NumMeth_.data ", EosLaw
       CALL stop_run()
    END IF

    IF (Data%Impre > 0) THEN
       WRITE(6, *)"In ", TRIM(FileName), "EosLaw = ",EosLaw
    END IF


    !Initialise la loi, selon la loi
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       CALL GP_init(MyUnit, Dim, Nv) 

       !Cas VDW
    CASE(cs_loietat_vdw)
       CALL VDW_init(MyUnit, Dim, Nv) 

       !Cas PRSV
    CASE(cs_loietat_prsv)
       CALL PRSV_init(MyUnit, Dim, Nv) 

       !Cas SW
    CASE(cs_loietat_sw)
       CALL SW_init(MyUnit, Dim, Nv) 

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

    IF (MyUnit > 0) THEN
       PRINT *, "Model ", TRIM(EosLaw), " Initialised"
       CLOSE(MyUnit)
    END IF

  END SUBROUTINE EOSInit
  !=====================



  !============================================================================
  !============================================================================
  !BEGIN :           SUBROUTINES/FUNCTIONS of Vp

  !Description
  ! > EOSEos(Vp)                   Vp = Vp(rho,e) "subroutine" computes all Vp components from (rho,e) <- Vp
  ! > EOS_d_PI_rho(Vp)            (dPI/drho)     = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > EOS_d_PI_E(Vp)               dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e) 
  ! > EOSCv(Vp)                    Cv = Cv(rho,T)
  ! > EOSEpsilon(Vp)               e  = e(rho,P)
  ! > EOSEpsilonDet(Vp)            e  = e(rho,T)
  ! > EOSTemperature(Vp)           T  = T(rho,e)
  ! > EOSpression(Vp)              P  = P(rho,e)
  ! > EOSson(Vp)                   c  = c(rho,P)
  ! > EOSenthalpy(Vp)              h  = h(rho,P)

  !!-- equation d'�tat du/des gaz, en fonction de la loi s�lectionn�e
  !====================
  SUBROUTINE EOSEos(Vp)
    !====================
    ! > (T,P,c) in function of (rho, e), e:= internal energy

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSEos"

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp !-- CCE R.B. 2008/11/18 CCE

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       CALL GP_Gas(Vp)

       !Cas VDW
    CASE(cs_loietat_vdw)
       CALL VDW_Gas(Vp)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       CALL PRSV_Gas(Vp)

       !Cas SW
    CASE(cs_loietat_sw)
       CALL SW_Gas(Vp)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT


  END SUBROUTINE  EOSEos
  !=====================




  !====================================
  FUNCTION EOS_d_PI_rho(Vp) RESULT(res)
    !====================================
    ! > Pressure Derivative // rho

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_d_PI_rho"

    REAL, DIMENSION(:), INTENT(IN) :: Vp !-- CCE R.B. 2008/11/18 CCE

    REAL                           :: res

    REAL             :: T, rho, u
    !-------------------------

    T   = Vp(i_tem_vp)
    rho = Vp(i_rho_vp)
    u   = SQRT(SUM(Vp(i_vi1_vp:i_vid_vp)**2))

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       res = GP_pi_rho__rho_u_T(rho, u, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       res = VDW_pi_rho__rho_u_T(rho, u, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       res = PRSV_pi_rho__rho_u_T(rho, u, T)

       !Cas SW
    CASE(cs_loietat_sw)
       res = SW_pi_rho__rho_u_T(rho, u, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT


  END FUNCTION EOS_d_PI_rho
  !=====================


  !===================================
  FUNCTION EOS_d_PI_E(Vp)  RESULT(res)
    !===================================
    ! > Pressure Derivative // rho*E

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_d_PI_E"

    REAL, DIMENSION(:), INTENT(IN) :: Vp !-- CCE R.B. 2008/11/18 CCE

    REAL                           :: res

    REAL             :: T, rho
    !-------------------------

    T   = Vp(i_tem_vp)
    rho = Vp(i_rho_vp)

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       res = GP_pi_rhoe__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       res = VDW_pi_rhoe__rho_T(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       res = PRSV_pi_rhoe__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       res = SW_pi_rhoe__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_d_PI_E
  !=====================

  !=======================================
  FUNCTION EOSCv__T_rho(rho, T) RESULT(Cv)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSCv"

    REAL, INTENT(IN) :: rho, T
    REAL             :: Cv
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       Cv = GP_cv()

       !Cas VDW
    CASE(cs_loietat_vdw)
       Cv = VDW_Cv()

       !Cas PRSV
    CASE(cs_loietat_prsv)
       Cv = PRSV_Cv__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       Cv = SW_Cv__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSCv__T_rho
  !========================

  !===========================================
  FUNCTION EOS_dCv__dT_dv(rho, T) RESULT(d_Cv)
    !===========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_dCv__dT_dv"

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: d_Cv
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d_Cv = GP_dCv__dT_dv()

       !Cas VDW
    CASE(cs_loietat_vdw)
       d_Cv = VDW_dCv__dT_dv()

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d_Cv = PRSV_dCv__dT_dv(rho,T)

       !Cas SW
       !    CASE(cs_loietat_sw)
       !       d_Cv = SW_dCv__dT_dv(rho,T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_dCv__dT_dv
  !==========================

  !=======================================
  FUNCTION EOSCp__T_rho(rho, T) RESULT(Cp)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSCp"

    REAL, INTENT(IN) :: rho, T
    REAL             :: Cp
    !-------------------------------

    REAL, DIMENSION(2) :: dP__dT_dv 

    REAL :: Cv, dP_dT, dP_dv
    !-------------------------------

    Cv = EOSCv__T_rho(rho, T)

    dP__dT_dv = EOS_dP__dT_dv(rho, T)

    dP_dT = dP__dT_dv(1) 
    dP_dv = dP__dT_dv(2)

    Cp = Cv - T*( (dP_dT**2)/dP_dv )

  END FUNCTION EOSCp__T_rho
  !========================

  !===========================================
  FUNCTION EOS_dCp__T_rho(rho, T) RESULT(d_Cp)
    !===========================================
    ! 
    ! d_Cp(1) = dCp/dT|_rho
    ! d_Cp(2) = dCp/drho|_T
    !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: d_Cp
    !-------------------------

    REAL, DIMENSION(2) :: dP__dT_dv
    REAL, DIMENSION(2) :: dCv__dT_dv 
    REAL, DIMENSION(3) :: dP2__dT_dv

    REAL :: v, dP_dT, dP_dv
    REAL :: dP2_dT, dP2_dv, dP2_dTv
    REAL :: dCv_T, dCv_v, dCp_v, dCp_T
    !-----------------------------------

    v = 1.d0 / rho

    dP__dT_dv = EOS_dP__dT_dv(rho, T)
    dP_dT = dP__dT_dv(1) 
    dP_dv = dP__dT_dv(2)

    dP2__dT_dv = EOS_dP2__dT_dv(rho, T)
    dP2_dT  = dP2__dT_dv(1) 
    dP2_dv  = dP2__dT_dv(2)
    dP2_dTv = dP2__dT_dv(3)

    dCv__dT_dv = EOS_dCv__dT_dv(rho, T)
    dCv_T = dCv__dT_dv(1)
    dCv_v = dCv__dT_dv(2)

    dCp_T =  dCv_T &
         - (dP_dT**2) / dP_dv & 
         - 2.d0*T * dP_dT * dP2_dT / dP_dv &
         + T* dP2_dTv * (dP_dT/dP_dv)**2

    dCp_v = dCv_v &
         - (2.d0*T * dP_dT * dP2_dTv/dP_dv) &
         + T*(dP2_dv * dP_dT**2)/(dP_dv**2)

    d_Cp(1) = dCp_T

    d_Cp(2) = -(v**2) * dCp_v

  END FUNCTION EOS_dCp__T_rho
  !==========================

  !=================================
  FUNCTION EOSCv_oo(T) RESULT(Cv_oo)
    !=================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSCv_oo"

    REAL, INTENT(IN) :: T
    REAL             :: Cv_oo
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       Cv_oo = GP_cv()

       !Cas VDW
    CASE(cs_loietat_vdw)
       Cv_oo = VDW_Cv()

       !Cas PRSV
    CASE(cs_loietat_prsv)
       Cv_oo = PRSV_Cv_oo(T)

       !Cas SW
    CASE(cs_loietat_sw)
       Cv_oo = SW_Cv_oo(T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSCv_oo
  !====================     

  !========================================
  FUNCTION EOS_dP__dT_dv(rho, T) RESULT(dP)
    !========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSd_dP__dT_dv"

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: dP
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       dP = GP_dP__dT_dv(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       dP = VDW_dP__dT_dv(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       dP = PRSV_dP__dT_dv(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       dP = SW_dP__dT_dv(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_dP__dT_dv
  !=========================

  !==========================================
  FUNCTION EOS_dP2__dT_dv(rho, T) RESULT(dP2)
    !==========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSd_dP2__dT_dv"

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(3) :: dP2
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       dP2 = GP_dP2__dT_dv(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       dP2 = VDW_dP2__dT_dv(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       dP2 = PRSV_dP2__dT_dv(rho, T)

!!$       !Cas SW
!!$    CASE(cs_loietat_sw)
!!$       dP2 = SW_dP2__dT_dv(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_dP2__dT_dv
  !==========================

  !==========================================
  REAL FUNCTION EOSd_Cv_oo(T) RESULT(d_Cv_oo)
    !==========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSd_Cv_oo"

    REAL, INTENT(IN) :: T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d_Cv_oo = GP_d_cv()

       !Cas VDW
    CASE(cs_loietat_vdw)
       d_Cv_oo = VDW_d_Cv()

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d_Cv_oo = PRSV_d_Cv_oo(T)

       !Cas SW
    CASE(cs_loietat_sw)
       d_Cv_oo = SW_d_Cv_oo(T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSd_Cv_oo
  !======================   

  !======================================
  REAL FUNCTION EOSEpsilon(Vp) RESULT(re)
    !======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSEpsilon"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !-----------------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       re = GP_Epsilon(Vp)

       !Cas VDW
    CASE(cs_loietat_vdw)
       re = VDW_Epsilon(Vp)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       re = PRSV_Epsilon(Vp)

       !Cas SW
    CASE(cs_loietat_sw)
       re = SW_Epsilon(Vp)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSEpsilon
  !======================

  !=========================================
  REAL FUNCTION EOSEpsilonDet(Vp) RESULT(re)
    !=========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSEpsilonDet"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       re = GP_EpsilonDet(Vp)

       !Cas VDW
    CASE(cs_loietat_vdw)
       re = VDW_EpsilonDet(Vp)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       re = PRSV_EpsilonDet(Vp)

       !Cas SW
    CASE(cs_loietat_sw)
       re = SW_EpsilonDet(Vp)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSEpsilonDet
  !=========================

  !!-- temperature, en fonction de la loi s�lectionn�e
  !============================================
  REAL FUNCTION EOSTemperature(Vp) RESULT(Temp)
    !============================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSTemperature"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !-----------------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       Temp = GP_Temperature(Vp)

       !Cas VDW
    CASE(cs_loietat_vdw)
       Temp = VDW_Temperature(Vp)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       Temp = PRSV_Temperature(Vp)

       !Cas SW
    CASE(cs_loietat_sw)
       Temp = SW_Temperature(Vp)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSTemperature
  !==========================


  !!-- Calcul de la pression en fonction de l'�nergie interne, pour tous types de gaz (via des #ifdef)
  !======================================
  REAL FUNCTION EOSpression(Vp) RESULT(p)
    !======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSpression"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !-----------------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       p = GP_Pression(Vp)

       !Cas VDW
    CASE(cs_loietat_vdw)
       p = VDW_Pression(Vp)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       p = PRSV_Pression(Vp)

       !Cas SW
    CASE(cs_loietat_sw)
       p = SW_Pression(Vp)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSpression
  !=======================

  !!-- vitesse du son, en fonction de la loi s�lectionn�e
  !==================================
  REAL FUNCTION EOSson(Vp) RESULT (c)
    !==================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSson"

    REAL, DIMENSION(: ), INTENT( IN) :: Vp
    !-------------------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       c = GP_Son(Vp)

       !Cas VDW
    CASE(cs_loietat_vdw)
       c = VDW_Son(Vp)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       c = PRSV_Son(Vp)

       !Cas SW
    CASE(cs_loietat_sw)
       c = SW_Son(Vp)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSson
  !==================

  !======================================
  REAL FUNCTION EOSenthalpy(Vp) RESULT(h)
    !======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSenthalpy"

    REAL, DIMENSION(:), INTENT( IN) :: Vp
    !-------------------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       h = GP_enthalpy(Vp)

       !Cas VDW
    CASE(cs_loietat_vdw)
       h = VDW_enthalpy(Vp)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       h = PRSV_enthalpy(Vp)

       !Cas SW
    CASE(cs_loietat_sw)
       h = SW_enthalpy(Vp)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOSenthalpy
  !=======================


  !-- END    :: SUBROUTINES / FUNCTIONS of Vp
  !=======================================================================
  !=======================================================================



  !=======================================================================
  !=======================================================================
  !-- BEGIN  :: FUNCTIONS of primitive variables (T,P,rho,e,c)

  !Description
  !-------------------------------
  ! > P__rho_T(rho, T)            P = P(rho,T)
  ! > T__rho_P(rho, P)            T = T(rho,P)   **Couteuse : m�thode Newton
  ! > rho__P_T(P  , T)          rho = rho(P,T)   **Couteuse : Newton (PRSV - SW)
  ! > e__rho_T(rho, T)            e = e(rho,T)
  ! > c__rho_T(rho, T)            c = c(rho,T)
  ! > s__rho_T(rho, T)            s = s(rho,T)   ** PRSV - SW
  ! > T__rho_c                    T = T(rho,c)   ** A JOUR QUE POUR GAZ PARFAIT
  ! > rho__s_T(s  , T)          rho = rho(s,T)   ** PRSV - SW
  ! > T__rho_s(rho, s)            T = T(rho,s)   **Couteuse : Newton (PRSV - SW)
  ! > GAMMA__rho_T(rho,T)         G = G(rho,T)   ** Fondamental Derivative
  ! > EOS_de_dT__rho_T(rho, T)    de/dT|rho = Cv(rho,T)
  ! > EOS_de_drho__rho_T(rho, T)  de/drho|T (rho,T)
  ! > EOS_d_Cv_T(rho,T)           dCv/dT|rho
  ! > EOS_d_Cv_rho(rho,T)         dCv/drho|T
  ! > EOS_Zc()                    Zc=Pc.Tc/R.vc  (depends of the model)
  ! > EOSKappa()                  VALABLE QUE EN GP 
  ! > EOS_TEICALC(h, rho, c1, c2) Calcul sp�cifique entr�e turbine (lgc = 91)

  !=======================================
  REAL FUNCTION P__rho_T(rho, T) RESULT(P)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "P__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       P = GP_P__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       P = VDW_P__rho_T(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       P = PRSV_P__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       P = SW_P__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION P__rho_T
  !====================

  !=======================================
  REAL FUNCTION T__rho_P(rho, P) RESULT(T)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "T__rho_P"

    REAL, INTENT(IN) :: rho, P
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       T = GP_T__rho_P(rho, P)

       !Cas VDW
    CASE(cs_loietat_vdw)
       T = VDW_T__rho_P(rho, P)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       T = PRSV_T__rho_P(rho, P)

       !Cas SW
    CASE(cs_loietat_sw)
       T = SW_T__rho_P(rho, P)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION T__rho_P
  !====================

  !=======================================
  REAL FUNCTION rho__P_T(P, T) RESULT(rho)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "rho__P_T"

    REAL, INTENT(IN) :: P, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)

       rho = GP_rho__P_T(P, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       rho = PRSV_rho__P_T(P, T)

       !Cas SW
    CASE(cs_loietat_sw)
       rho = SW_rho__P_T(P, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable (GP et VDW non valides)", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION rho__P_T
  !====================



  !=======================================
  REAL FUNCTION e__rho_T(rho, T) RESULT(e)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "e__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       e = GP_e__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       e = VDW_e__rho_T(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       e = PRSV_e__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       e = SW_e__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION E__rho_T
  !====================


  !=======================================
  REAL FUNCTION c__rho_T(rho, T) RESULT(c)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "c__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       c = GP_c__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       c = VDW_c__rho_T(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       c = PRSV_c__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       c = SW_c__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION c__rho_T
  !====================


  !=======================================
  REAL FUNCTION s__rho_T(rho, T) RESULT(s)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "s__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)
       ! Cas GP
    CASE (cs_loietat_gp)
       s = GP_s__rho_T(rho,T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       s = PRSV_s__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       s = SW_s__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable (GP et VDW non valides)", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION s__rho_T
  !====================


  !=======================================
  REAL FUNCTION T__rho_c(rho, c) RESULT(T)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "T__rho_c"

    REAL, INTENT(IN) :: rho, c
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       T = GP_T__rho_c(rho, c)

       !Cas VDW
       !    CASE(cs_loietat_vdw)
       !       T = VDW_T__rho_c(rho, c)

       !Cas PRSV
       !    CASE(cs_loietat_prsv)
       !       T = PRSV_T__rho_c(rho, c)

       !Cas SW
       !    CASE(cs_loietat_sw)
       !       T = SW_T__rho_c(rho, c)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw, " Calcul de T(rho,c) valable uniquement en Gaz Parfait "
       CALL stop_run()
    END SELECT


  END FUNCTION T__rho_c
  !====================

  !=======================================
  REAL FUNCTION rho__s_T(s, T) RESULT(rho)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "rho__s_T"

    REAL, INTENT(IN) :: s, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)


       !Cas PRSV
    CASE(cs_loietat_prsv)
       rho = PRSV_rho__s_T(s, T)

       !Cas SW
    CASE(cs_loietat_sw)
       rho = SW_rho__s_T(s, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable (GP et VDW non valides)", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION rho__s_T
  !====================

  !=======================================
  REAL FUNCTION T__rho_s(rho, s) RESULT(T)
    !=======================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "T__rho_s"

    REAL, INTENT(IN) :: rho, s
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)
       ! Cas GP
    case (cs_loietat_gp)
       T = GP_T__rho_s(rho, s)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       T = PRSV_T__rho_s(rho, s)

       !Cas SW
    CASE(cs_loietat_sw)
       T = SW_T__rho_s(rho, s)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable (GP et VDW non valides)", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION T__rho_s
  !====================

  !===========================================
  REAL FUNCTION GAMMA__rho_T(rho, T) RESULT(g)
    !===========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "GAMMA__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       g = GP_GAMMA__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       !       g = VDW_GAMMA__rho_T(rho, T)
       PRINT*, "GAMMA__rho_T non valable en loi VDW"
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()

       !Cas PRSV
    CASE(cs_loietat_prsv)
       g = PRSV_GAMMA__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       g = SW_GAMMA__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION GAMMA__rho_T
  !========================


  !===============================================
  REAL FUNCTION EOS_de_dT__rho_T(rho, T) RESULT(d)
    !===============================================
    ! > de/dT|rho = Cv(rho,T)

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_de_dT__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d = GP_cv()

       !Cas VDW
    CASE(cs_loietat_vdw)
       d = VDW_Cv()

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d = PRSV_Cv__rho_T(rho,T)

       !Cas SW
    CASE(cs_loietat_sw)
       d = SW_Cv__rho_T(rho,T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_de_dT__rho_T
  !============================

  !=================================================
  REAL FUNCTION EOS_de_drho__rho_T(rho, T) RESULT(d)
    !=================================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_de_drho__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d = GP_de_drho__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       d = VDW_de_drho__rho_T(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d = PRSV_de_drho__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       d = SW_de_drho__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_de_drho__rho_T
  !==============================

  !=================================================
  REAL FUNCTION EOS_dT_drho__rho_T(rho, T) RESULT(d)
    !=================================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_dT_drho__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d = GP_dT_drho__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       d = VDW_dT_drho__rho_T(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d = PRSV_dT_drho__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       d = SW_dT_drho__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_dT_drho__rho_T
  !==============================

  !===============================================
  REAL FUNCTION EOS_dT_de__rho_T(rho, T) RESULT(d)
    !===============================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_dT_de__rho_T"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d = GP_dT_de__rho_T(rho, T)

       !Cas VDW
    CASE(cs_loietat_vdw)
       d = VDW_dT_de__rho_T(rho, T)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d = PRSV_dT_de__rho_T(rho, T)

       !Cas SW
    CASE(cs_loietat_sw)
       d = SW_dT_de__rho_T(rho, T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_dT_de__rho_T
  !============================

  !A FINIR
  !========================================
  REAL FUNCTION EOS_d_Cv_T(rho,T) RESULT(d)
    !========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_d_Cv_rho"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d = 0d0

       !Cas VDW
    CASE(cs_loietat_vdw)
       d = 0d0

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d = PRSV_d_Cv_T(rho,T)

       !Cas SW
    CASE(cs_loietat_sw)
       d = SW_d_Cv_T(rho,T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_d_Cv_T
  !====================

  !==========================================
  REAL FUNCTION EOS_d_Cv_rho(rho,T) RESULT(d)
    !==========================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_d_Cv_rho"

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       d = 0d0

       !Cas VDW
    CASE(cs_loietat_vdw)
       d = 0d0

       !Cas PRSV
    CASE(cs_loietat_prsv)
       d = PRSV_d_Cv_rho(rho,T)

       !Cas SW
    CASE(cs_loietat_sw)
       d = SW_d_Cv_rho(rho,T)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT

  END FUNCTION EOS_d_Cv_rho
  !====================



  !-- END    :: FUNCTIONS of primitive variables (T,P,rho,e,c)
  !=======================================================================
  !=======================================================================



  !-- Zc, en fonction de la loi s�lectionn�e
  !================================
  REAL FUNCTION EOS_Zc() RESULT(Zc)
    !================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_Zc"

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       Zc = 1d0

       !Cas VDW
    CASE(cs_loietat_vdw)
       Zc = 0.375d0

       !Cas PRSV
    CASE(cs_loietat_prsv)
       Zc = 0.311155425170673d0

       !Cas SW
    CASE(cs_loietat_sw)
       Zc = SW_Zc()

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT


  END FUNCTION EOS_Zc
  !====================

  !==================================
  REAL FUNCTION EOS_omega() RESULT(w)
    !==================================
    ! Acentric factor

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_omega"

    SELECT CASE(sel_EosLaw)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       w = PRSV_omega()

       !Cas SW
    CASE(cs_loietat_sw)
       w = SW_omega()

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable EOSINIT", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw
       CALL stop_run()
    END SELECT


  END FUNCTION EOS_omega
  !=====================

  !-- kappa, en fonction de la loi s�lectionn�e
  !=================================
  REAL FUNCTION EOSKappa() RESULT(k)
    !=================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOSKappa"

    !Initialise la loi, selon la loi
    SELECT CASE(sel_EosLaw)

       !Cas GP
    CASE(cs_loietat_gp)
       k = GP_Kappa()

    CASE DEFAULT
       WRITE(6, *)"La Fonction EOSKappa() n'est valable qu'en Gaz Parfait ; Ici, Loi : ", sel_EosLaw, TRIM(EosLaw)
       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : "
       CALL stop_run()
    END SELECT


  END FUNCTION EOSKappa
  !====================

  !==================================================
  REAL FUNCTION EOS_TEICALC(h, rho, c1, c2) RESULT(T)
    !==================================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "EOS_TEICALC"

    REAL, INTENT(IN) :: h, rho, c1, c2
    !-------------------------

    !Selon la loi d'�tat
    SELECT CASE(sel_EosLaw)

       !Cas PRSV
    CASE(cs_loietat_prsv)
       T = PRSV_TEICALC(h, rho, c1, c2)

       !Cas SW
    CASE(cs_loietat_sw)
       T = SW_TEICALC(h, rho, c1, c2)

    CASE DEFAULT
       WRITE(6, *)"La loi d'etat specfiee n'est pas valable (GP et VDW non valides)", sel_EosLaw, TRIM(EosLaw)

       CALL decoder_data_loietat(EosLaw, sel_EosLaw)
       PRINT *, mod_name, " ERREUR : ", EosLaw, sel_EosLaw, " Calcul de T(rho,c) valable uniquement en Gaz Parfait "
       CALL stop_run()
    END SELECT


  END FUNCTION EOS_TEICALC
  !====================




END MODULE EOSLaws




