MODULE Compute_Forces

  USE LesTypes
  USE LibComm
  USE Diffusion
  USE Contrib_NS
  USE Quadrature, ONLY: Int_d, bInt

  IMPLICIT NONE

CONTAINS
  
   !=============================================
   SUBROUTINE Compute_Aerodynamic_coeff(Com, Var)
   !=============================================

     IMPLICIT NONE

     TYPE(MeshCom),   INTENT(INOUT) :: Com
     TYPE(Variables), INTENT(INOUT) :: Var
     !------------------------------------

     REAL, DIMENSION(:,:), ALLOCATABLE :: NS
     REAL, DIMENSION(:,:), ALLOCATABLE :: SS
     REAL, DIMENSION(:,:), ALLOCATABLE :: TT

     INTEGER, DIMENSION(:), ALLOCATABLE :: NU

     REAL, DIMENSION(DATA%Ndim) :: vel_oo, t_v,      &
                                   L_dir, D_dir,     &
                                   Force_P, Force_V, &
                                   Loc_FP, Loc_FV
     !---------------------------------------------------

     TYPE(face_str) :: loc_ele

     REAL :: rho_oo, U_oo, q_oo, P_oo, mu, alpha, beta
     REAL :: iN_o
     REAL :: CL, CD_P, CD_V, surf

     INTEGER :: jf, j, k, bc_idx, bc_type, i_wall, f_wall
     INTEGER :: N_dim, N_dofs, ierror
     !---------------------------------------------------

     N_dim  = Data%Ndim

     rho_oo = Data%VarPhyInit(i_rho_vp, 1)
     vel_oo = Data%VarPhyInit(i_vi1_vp:i_vid_vp, 1)
     P_oo   = Data%VarPhyInit(i_pre_vp, 1)

     U_oo = SQRT(SUM(vel_oo**2))
     q_oo = 0.5d0 * rho_oo * U_oo**2

     alpha = Data%AoA
     beta  = Data%AoSS

     !------------------------------------------------------------
     ! Normalizes the forces acting on the body by means
     ! of the dynamic pressure q_oo = 0.5 rho |u|^2
     !--------------------------------------------------
     L_dir = 0.0; D_dir = 0.0

     IF(Data%NDim == 2) THEN

        L_dir = (/ -SIN(alpha), COS(alpha) /)
        D_dir = (/  COS(alpha), SIN(alpha) /)

     ELSE

        L_dir = (/ -COS(beta)*SIN(alpha), 0.d0,      COS(beta)*COS(alpha) /)
        D_dir = (/  COS(beta)*COS(alpha), SIN(beta), COS(beta)*SIN(alpha) /)

     ENDIF

     Force_P = 0.0; Force_V = 0.0

     surf = 0.d0

     IF( Data%N_wall_points > 0 ) THEN

        Var%Cp = 0.0; Var%Cf = 0.0

        i_wall = 0; f_wall = 0
     
        ALLOCATE( TT(N_dim, N_dim) )
     
        DO jf = 1, SIZE(b_element)

           loc_ele = b_element(jf)%b_f

           bc_idx  = loc_ele%bc_type
           bc_type = Data%FacMap(bc_idx)

           IF( bc_type == lgc_paroi_glissante          .OR. & 
               bc_type == lgc_paroi_adh_adiab_flux_nul .OR. &
               bc_type == lgc_paroi_misc               ) THEN

              f_wall = f_wall + 1

              N_dofs = loc_ele%N_points

              ALLOCATE( NU(N_dofs) ) 
              ALLOCATE( NS(N_dim, N_dofs), &
                        SS(N_dim, N_dofs) )
                      
              NU = loc_ele%NU

              DO k = 1, N_dofs

                 i_wall = i_wall + 1

                 !--------------------------------------------------
                 ! Computes the normal stress on the body
                 ! due to the pressure
                 !----------------------------------------
                 NS(:, k) = Var%Vp(i_pre_vp, NU(k)) * loc_ele%n_b(:, k)

                 ! Pressure Coeff.
                 Var%Cp(i_wall) = (Var%Vp(i_pre_vp, NU(k)) - P_oo) / &
                                  ( 0.5d0 * rho_oo* U_oo**2 )

#ifdef NAVIER_STOKES

                 !--------------------------------------------------
                 ! Computes the shear stress on the body
                 ! due to the viscosity
                 !----------------------------------------        
                 mu = Viscosity(Var%Vp(:, NU(k)))

                 TT = Stress_Tensor( Var%Vp(:, NU(k)), Var%D_Ua(:, :, NU(k)), mu )

                 SS(:, k) = -MATMUL(TT, loc_ele%n_b(:, k))

                 ! Skin friction Coeff.
                 Var%Cf(i_wall) = SQRT(SUM( SS(:,k)**2 )) / (0.5d0 * rho_oo * U_oo**2)
                 Var%Cf(i_wall) = SIGN( Var%Cf(i_wall), DOT_PRODUCT( SS(:, k), D_dir) )

                 Var%Cf(i_wall) = SIGN( Var%Cf(i_wall), Var%D_Ua(2, i_rv1_ua, NU(k)) )

#endif

              ENDDO

              IF ( MAXVAL(NU) <= Var%NCellsIn) THEN

                 ! Exclude the overlaps in the computaion
                 !---------------------------------------

                 Loc_FP = bInt(loc_ele, NS)
                 Force_P = Force_P + Loc_FP

#ifdef NAVIER_STOKES
                 Loc_FV = bInt(loc_ele, SS)
                 Force_V = Force_V + Loc_FV
#endif

                 !surf = surf + sum(loc_ele%w_q)

              ELSE
             
                 ! Correct behaviour on the overlap
                 !---------------------------------
                 iN_o = 1.d0 / REAL(loc_ele%b_ovlp, 8)

                 Loc_FP = bInt(loc_ele, NS)
                 Force_P = Force_P + iN_o*Loc_Fp

#ifdef NAVIER_STOKES
                 Loc_FV = bInt(loc_ele, SS)
                 Force_V = Force_V + iN_o*Loc_FV
#endif

                 !surf = surf + iN_o*sum(loc_ele%w_q)

              ENDIF
              
              ! Local contribution to the coeff.
              Loc_FP = bInt(loc_ele, NS)
              
              Loc_FV = 0.d0

#ifdef NAVIER_STOKES
              Loc_FV = bInt(loc_ele, SS)
#endif

              Var%C_loc(1, f_wall) = SUM( Loc_FP * L_dir ) +  &
                                     SUM( Loc_FV * L_dir )

              Var%C_loc(2, f_wall) = SUM( Loc_FP * D_dir )
              Var%C_loc(3, f_wall) = SUM( Loc_FV * D_dir )              

              DEALLOCATE( NU, NS, SS )
  
           ENDIF

        ENDDO
    
        DEALLOCATE( TT )

     ENDIF

#ifndef SEQUENTIEL

     CALL Reduce(Com, cs_parall_sum, Force_P)

#ifdef NAVIER_STOKES
     CALL Reduce(Com, cs_parall_sum, Force_V)
#endif

#endif


     ! NOTE: Coefficients MUST be hand-divided by
     !       the reference lenght (2D) or surface (3D)
     !------------------------------------------------
     ! WARNING: These coeff. may be wrong on 3D parallel,
     !          the correct values are computed during
     !          the post-pro only!
     CL   = ( SUM( Force_P * L_dir ) +  &
              SUM( Force_V * L_dir ) ) / q_oo

     CD_P = SUM( Force_P * D_dir ) / q_oo
     CD_V = SUM( Force_V * D_dir ) / q_oo

     ! CM   = ....

     !---------------------------------------------------------------
     ! Output in the file
     !-------------------------------
     IF (Com%Me == 0 ) THEN

        IF (Var%kt < Data%ifre) THEN

           OPEN(81, FILE = 'Aero_Coeff.'//TRIM(ADJUSTL(Data%RootName)), & 
                STATUS = 'REPLACE', IOSTAT = ierror)

           IF(ierror /= 0) THEN
              WRITE(*,*) 'ERROR: Impossible to open the file coeff'
              WRITE(*,*) 'STOP'
              STOP
           ENDIF

           CLOSE(81)

           OPEN(81, FILE = 'Aero_Coeff.'//TRIM(ADJUSTL(Data%RootName)), &
                ACTION = 'WRITE', POSITION = 'APPEND', IOSTAT = ierror)

           WRITE(81, *) 'TITLE = "Aerodynamics coefficients. Reference lenght / surface : 1[m] / 1[m^2]"'
           WRITE(81, *) 'VARIABLES = "Ite", "CL", "CD Pressure", "CD Shear stress"'

           WRITE(81, 22)   Var%Kt, CL, CD_P, CD_V
            
        ELSE

           OPEN(81, FILE = 'Aero_Coeff.'//TRIM(ADJUSTL(Data%RootName)), &
                ACTION = 'WRITE', POSITION = 'APPEND', IOSTAT = ierror)

           WRITE(81, 22)   Var%Kt, CL, CD_P, CD_V

        ENDIF

     ENDIF

     CLOSE(81)

     Var%CL   = CL
     Var%CD_P = CD_P
     Var%CD_V = CD_V
 
22 FORMAT(I6 X2 3(1x,e24.16))
    
  END SUBROUTINE Compute_Aerodynamic_coeff
  !=======================================

END MODULE Compute_Forces
