MODULE MergeVisu
  USE LesTypes
  USE Inputs
  USE Reprise
  USE ReadMesh
  USE GeomGraph
  USE GeomParam

  IMPLICIT NONE


  TYPE Overlap
     INTEGER  :: Element
     INTEGER , DIMENSION(:), POINTER :: pts
     TYPE(Overlap), POINTER :: suivant
  END TYPE Overlap

  TYPE Liste
     INTEGER :: Elt
     TYPE(Liste), POINTER :: suivant
  END TYPE Liste

  TYPE(Overlap) , DIMENSION(:),POINTER :: over
  CHARACTER(LEN=*), PARAMETER :: FichierNum = "Glob2Loc"

CONTAINS

  SUBROUTINE Moulinette(gdata, uMesh, gVar, FileToConvert)
    TYPE(Maillage)    , INTENT(INOUT)   :: uMesh
    TYPE(Variables)   , INTENT(INOUT)   :: gVar
    TYPE(Donnees)     , INTENT(INOUT)   :: gData
    CHARACTER(LEN=*)  , INTENT(INOUT)   :: FileToConvert

    CHARACTER(LEN=*), PARAMETER :: mod_name = "Moulinette"

    ! Variables locales structures
    !----------------------------
    TYPE(MeshDef) :: gMesh
    
    TYPE(MeshCom)     , DIMENSION(:), POINTER :: Com
    TYPE(MeshDef)     , DIMENSION(:), POINTER :: Mesh
    TYPE(Maillage)    , DIMENSION(:), POINTER :: Maill
    TYPE(Variables)   , DIMENSION(:), POINTER :: Var
    TYPE(Donnees)     , DIMENSION(:), POINTER :: DATA


    TYPE(MeshCom)      :: TmpCom
    TYPE(MeshDef)      :: TmpMesh
    TYPE(Donnees)      :: TmpDATA

    ! Variables locales scalaires
    !----------------------------
    INTEGER             :: iv, jt, N_dofs, jf, ele_b, &
                           N_wall_points, k_w, k_b, N_b, &
                           Max_Ns_b, nn_b

    CHARACTER(LEN=1024) :: Hdbfile
    LOGICAL             :: existe , Ok

    CHARACTER(LEN=1024) :: rootname
    INTEGER             :: pos_start, pos_end
    INTEGER             :: iter,nbproc,proc,i,elt
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: Renumerote,Nums
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: EltNum

    pos_end = INDEX(FileToConvert, "-") - 1
    IF (pos_end <= 0) THEN
       PRINT *, "Error: wrong name file - Unable to get the rootname"
       PRINT *, "  Needing something like : rootname-001-000000.save"
       PRINT *, "  Got :", TRIM(FileToConvert)
       CALL Delegate_stop()
    END IF
    rootname = FileToConvert(1:pos_end)

    pos_start = INDEX(FileToConvert, "-", back = .TRUE.) + 1
    pos_end   = INDEX(FileToConvert, ".save") - 1
    IF (pos_end <= pos_start) THEN
       PRINT *,"Error: wrong name file - Unable to get the iteration number"
       PRINT *, "  Needing something like : rootname-001-000000.save"
       PRINT *, "  Got :", TRIM(FileToConvert)
       CALL Delegate_stop()
    END IF

    READ(FileToConvert(pos_start:pos_end),*) iter

    ! Get The number of processors
    TmpCom%Me  =  0
    TmpDATA%rootname = rootname
    TmpCom%NTasks = 2
    TmpCom%GetNTasks = .TRUE.
    CALL DonNumMeth(TmpCom, TmpDATA)
    CALL DonMesh(TmpCom, TmpMesh, TmpDATA)
    !Pour permettre de relire numeth il faut fermer 12
    CLOSE(12)
    ! Now we know the number of processor
    nbproc = TmpCom%NTasks
    PRINT *, mod_name, " ntasks=", nbproc


    ! Allocation des structures
    ALLOCATE(over(nbproc))
    DO proc = 1, nbproc
       NULLIFY(over(proc)%suivant)
    END DO
    !*************************************
    !* Initialisation des com            *
    !************************************* 
    gMesh%Npoint = 0

    N_b = 0; Max_Ns_b = 0

    N_wall_points = 0

    ALLOCATE(Com(nbproc),Mesh(nbproc),Var(nbproc),DATA(nbproc), Maill(nbproc))
    DO proc = 0, nbproc-1
       Com(proc+1)%Me  =  proc
       DATA(proc+1)%rootname = rootname
       Com(proc+1)%NTasks = nbproc  
       Com(proc+1)%GetNTasks = .False.
       !*************************************
       !* Lecture des fichiers de démarrage *
       !************************************* 
       CALL DonNumMeth(Com(proc+1),DATA(proc+1))
       CALL DonMesh(Com(proc+1),Mesh(proc+1),DATA(proc+1))
       !***************************************
       !* Setting de la structure des variables
       !***************************************
       NULLIFY( Var(proc+1)%CellVol )
       !-- initialise var%Ncells pour l'ordre 1 ou 2, appelle GeomSegments2d (entre autres)
       CALL InitVertexCentered(Var(proc+1),Mesh(proc+1),DATA(proc+1))
       !**************************************************************
       
       ! On compte le nombre total de points (somme des points intérieurs)
       gMesh%Npoint = gMesh%Npoint + Mesh(proc+1)%Npoint - Com(proc+1)%NPrcv2tot
       IF (DATA(proc+1)%Nordre == 3) THEN
          PRINT *, " --> Lecture des segments de com <--" 
          CALL ReadSegCom( Com(proc+1) , Mesh(proc+1), DATA(proc+1)%PbName)
       END IF
       Var(proc+1)%NCellsIn = Var(proc+1)%NCells - Com(proc+1)%NPrcv2tot

       WRITE(Hdbfile,"(A,A1,I6.6)") TRIM(DATA(proc+1)%PbName),"-",iter
       INQUIRE(FILE=TRIM(Hdbfile)//'.save', EXIST=existe)
       IF (.NOT. existe) THEN
          PRINT *, "ERREUR : Pas de fichier ", Hdbfile
          CALL Abort()
       END IF
       CALL Reprise_visu(DATA(proc+1), Maill(proc+1), Var(proc+1), Hdbfile) !-- lecture des données

       N_b = N_b + Maill(proc+1)%NfacFr

       N_wall_points = N_wall_points + Data(proc+1)%N_wall_points

       Max_Ns_b = MAX( Max_Ns_b, &
                       SIZE(Mesh(proc+1)%NsFacFr, 1) )
       
       PRINT *, "fin pour proc:", proc

       !Pour permettre de relire numeth il faut fermer 12
       CLOSE(12)
    ENDDO

    ! Bon on a recupere toutes nos donnees C partiiii

    gMesh%Ndim = Mesh(1)%Ndim
    gVar%Temps = Var(1)%Temps
    gData%kt0  = Var(1)%kt
    gVar%CFL   = Var(1)%CFL

    gData%sel_modele = Data(1)%sel_modele

    gVar%Ncells = 0
    DO i = 1, nbproc
       gVar%Ncells = gVar%Ncells + Var(i)%NCellsIn
    END DO

    gData%NVar    = DATA(1)%NVar
    gData%NVarPhy = DATA(1)%NVarPhy +1
    ALLOCATE(gData%VarNames(gData%NVarPhy) )
    gData%VarNames(:DATA(1)%NVarPhy) = DATA(1)%VarNames
    gData%VarNames(gData%NVarPhy) = "dom"
    gData%Ndim = DATA(1)%Ndim

    gData%N_wall_points = N_wall_points

    gData%LoiEtat =     Data(1)%LoiEtat 
    gData%sel_loivisq = Data(1)%sel_loivisq
    gData%LoiTh =       Data(1)%LoiTh

    ALLOCATE( gData%VarPhyInit(gData%NVarPhy-1, 1) )
    gData%VarPhyInit = Data(1)%VarPhyInit

    ALLOCATE( gData%Residus0(gData%NVar) )
    gData%Residus0 = DATA(1)%Residus0

    gMesh%NfacFr = 0
    gdata%Nbord  = 0
    gdata%Nordre = DATA(1)%Nordre

    ALLOCATE(Renumerote(2,gVar%Ncells),Nums(nbproc,MAXVAL(Mesh(:)%NCells)))
    ALLOCATE(EltNum(nbproc,MAXVAL(Mesh(:)%NElemt)))
    EltNum = 0
    Renumerote = 0
    Nums = 0
    gMesh%NdegreMax = MAXVAL(Mesh(:)%NdegreMax)


    CALL ReadNumerotation(OK)
    IF (OK) THEN
       PRINT *, "Renumerotation connue"
    ELSE
       PRINT *, "Renumerotation des DDLs"
       CALL numerote()
       PRINT *, "Renumerotation des Elements"
       CALL numeroteElt(gMesh%Nelemt)
       CALL SaveNumerotation()
    END IF

    PRINT *, "Construction du maillage global"

    ALLOCATE(gMesh%Ndegre(gMesh%Nelemt))
    ALLOCATE(gMesh%Nu(gMesh%NdegreMax ,gMesh%Nelemt))
    ALLOCATE(gMesh%EltType(gMesh%Nelemt))
    ALLOCATE(gMesh%Coor(gMesh%Ndim, gVar%NCells))
    ALLOCATE(gVar%Vp(gData%NvarPhy, gVar%NCells))

    IF(gData%sel_modele == cs_modele_eulerns) THEN
       ALLOCATE(gVar%D_Ua(gData%Ndim,  gData%Nvar, gVar%NCells))
    ENDIF

!!$    ALLOCATE( uMesh%fr(uMesh%NfacFr) )

    ALLOCATE( gVar%Residus(gData%NVar) )
    gVar%Residus = VAr(1)%Residus

    gVar%Resid_1 = Var(1)%Resid_1
    gVar%Resid_2 = Var(1)%Resid_2

    ALLOCATE( gVar%Cp(N_wall_points), &
              gVar%Cf(N_wall_points)  )

    gVar%CL   = Var(1)%CL
    gVar%CD_P = Var(1)%CD_P
    gVar%CD_V = Var(1)%CD_V

    CALL get_Cx()

    k_w = 0; k_b = 0

    gMesh%Ncells = gVar%NCells
    uMesh%Npoint = gVar%NCells

    ALLOCATE( gMesh%NsfacFr(Max_Ns_b, N_b) )
    ALLOCATE( gMesh%LogFr(N_b) )
    ALLOCATE( gMesh%CoordFr(gMesh%Ndim, Max_Ns_b, N_b) )

    gMesh%NsfacFr = 0
    gMesh%LogFr   = 0
    gMesh%CoordFr = 0.d0

    DO proc = 1, nbproc

       DO i = 1, Mesh(proc)%NElemt

          elt = EltNum(proc,i)

          IF (elt > 0) THEN

             gMesh%EltType(elt) = Mesh(proc)%eltType(i)
             gMesh%Ndegre(elt)  = Mesh(proc)%Ndegre(i)
             gMesh%Nu(:,elt)    = Nums(proc,Mesh(proc)%Nu(:,i))
            
          END IF

       END DO

       !----------------------------------------------
       DO jf = 1, Mesh(proc)%NFacFr

          !ele_b = Mesh(proc)%NumElemtFr(jf)

          !elt = EltNum(proc, ele_b)

          !IF(elt > 0) THEN

            k_b = k_b + 1

            nn_b = COUNT( Mesh(proc)%NsfacFr(:, jf) /= 0 )

            gMesh%NsfacFr(1:nn_b, k_b) = Nums( proc, &
                                         Mesh(proc)%NsfacFr(1:nn_b, jf) )
            
            gMesh%LogFr(k_b) = Maill(proc)%fr(jf)%inum

            DO nn_b = 1, SIZE(Maill(proc)%fr(jf)%Coor, 2)
               gMesh%CoordFr(:, nn_b, k_b) = Maill(proc)%fr(jf)%Coor(:, nn_b)
            ENDDO
            
          !ENDIF

       ENDDO
       !----------------------------------------------

       DO i = 1, Data(proc)%N_wall_points

          k_w = k_w + 1

          gVar%Cp(k_w) = Var(proc)%Cp(i)
          gVar%Cf(k_w) = Var(proc)%Cf(i)

       ENDDO

    END DO

    DO i = 1, gVar%Ncells
       proc = Renumerote(1,i)
       elt  = Renumerote(2,i)
       gVar%Vp(:gData%NVarPhy-1,i)  = Var(proc)%Vp(:,elt)     
       gVar%Vp(gData%NVarPhy,i)     = proc
       gMesh%Coor(:,i)              = Mesh(proc)%Coor(:,elt)

       IF (gData%sel_modele == cs_modele_eulerns) THEN
          gVar%D_Ua(:, :, i)  = Var(proc)%D_Ua(:, :, elt)
       ENDIF

    END DO

    uMesh%Nelement = gMesh%Nelemt

    ALLOCATE( uMesh%Maill(uMesh%Nelement) )
    
    DO jt = 1, gMesh%Nelemt

       N_dofs = COUNT( gMesh%Nu(:,jt) /= 0 )

       uMesh%Maill(jt)%Ndegre   = gMesh%Ndegre(jt)
       uMesh%Maill(jt)%Nsommets = N_dofs

       ALLOCATE( uMesh%Maill(jt)%Nu(N_dofs) )
       ALLOCATE( uMesh%Maill(jt)%Coor( DATA(1)%Ndim, N_dofs ) )

       uMesh%Maill(jt)%Nu = gMesh%Nu(1:N_dofs, jt)

       uMesh%Maill(jt)%coor(:, 1:N_dofs) = gMesh%Coor(:, uMesh%Maill(jt)%Nu)

    ENDDO

    uMesh%NfacFr = k_b

    ALLOCATE( uMesh%fr(k_b) )

    DO jf = 1, k_b

       nn_b = COUNT( gMesh%NsfacFr(:, jf) /= 0 )

       ALLOCATE( uMesh%fr(jf)%NU(nn_b) )
       ALLOCATE( uMesh%fr(jf)%Coor(gMesh%Ndim, nn_b) )

       uMesh%fr(jf)%Coor(:, 1:nn_b) = gMesh%CoordFr(:, 1:nn_b, jf)

       uMesh%fr(jf)%Ndegre   = nn_b
       uMesh%fr(jf)%Nsommets = nn_b

       uMesh%fr(jf)%NU(1:nn_b) = gMesh%NsfacFr(1:nn_b, jf)
       
       uMesh%fr(jf)%inum = gMesh%LogFr(jf)

    ENDDO

    WRITE(FileToConvert,"(A,A8,I6.6)") TRIM(Rootname),"-master-", iter
    PRINT *, "Merge Fini Youpiii"

  CONTAINS

    SUBROUTINE SaveNumerotation()
      INTEGER :: i , j , MyUnit
      MyUnit = 10

      OPEN(UNIT = MyUnit, FILE=FichierNum)
      WRITE( MyUnit, *)   gMesh%Npoint, gMesh%Nelemt 
      WRITE( MyUnit, *) (( EltNum(i,j) , i = 1, nbproc) , j = 1,MAXVAL(Mesh(:)%NElemt))

      WRITE( MyUnit, *) (( Renumerote(i,j) , i = 1, 2) , j = 1,gVar%Ncells)

      WRITE( MyUnit, *) (( Nums(i,j) , i = 1, nbproc) , j = 1,MAXVAL(Mesh(:)%NCells))

      CLOSE( MyUnit )

    END SUBROUTINE SaveNumerotation


    SUBROUTINE ReadNumerotation(OK)
      LOGICAL, INTENT(OUT) :: OK
      INTEGER :: i , j , MyUnit


      MyUnit = 10

      INQUIRE(FILE = FichierNum , EXIST = OK)

      IF (OK) THEN
         OPEN(UNIT = MyUnit, FILE = FichierNum , STATUS="OLD")
         READ( MyUnit, *)   gMesh%Npoint, gMesh%Nelemt 
         READ( MyUnit, *) (( EltNum(i,j) , i = 1, nbproc) , j = 1,MAXVAL(Mesh(:)%NElemt))

         READ( MyUnit, *) (( Renumerote(i,j) , i = 1, 2) , j = 1,gVar%Ncells)

         READ( MyUnit, *) (( Nums(i,j) , i = 1, nbproc) , j = 1,MAXVAL(Mesh(:)%NCells))

         CLOSE( MyUnit )
      END IF

    END SUBROUTINE ReadNumerotation



    SUBROUTINE numerote()
      INTEGER :: i,j , pt,DomV,DomV2,jv,is

      pt = 1
      DO i = 1,nbproc
         DO j = 1, Var(i)%NCellsIn
            Renumerote(1,pt) = i
            Renumerote(2,pt) = j
            Nums(i,j) = pt
            pt = pt + 1
         END DO
      END DO

      DO i = 1,nbproc
         DO iv = 1, Com(i)%Ndomv
            DomV = Com(i)%Jdomv(iv)
            DO jv = 1,Com(DomV)%NDomV
               DomV2 = Com(DomV)%Jdomv(jv)            
               IF (DomV2 == i) EXIT
            END DO

            DO  j = 1, Com(i)%NPrcv2(iv) , 1
               is = Com(i)%PTrcv2(j,iv)
               Nums(i,is) = Nums(Domv,Com(DomV)%PTsnd2(j,jv))
            END DO
         END DO
      END DO
    END SUBROUTINE Numerote

    SUBROUTINE numeroteElt(Nelt)
      INTEGER, INTENT(out) :: Nelt
      INTEGER :: i,j
      TYPE(Overlap) , POINTER :: tmp

      Nelt = 0    
      DO i = 1,nbproc
         DO j = 1, Mesh(i)%NElemt
            IF (ALL( Mesh(i)%Nu(1:Mesh(i)%Ndegre(j),j) <= Var(i)%NCellsIn)) THEN
               Nelt = Nelt + 1
               EltNum(i,j) = Nelt
            ELSE
               ALLOCATE(tmp)
               tmp%Element = j
               tmp%suivant => over(i)%suivant
               over(i)%suivant => tmp
            END IF
         END DO
      END DO
      PRINT *, "Suppression des Overlaps"
      CALL supprimeOverlap(Nelt)
    END SUBROUTINE NumeroteElt


    SUBROUTINE supprimeOverlap(nb)
      INTEGER , INTENT(INOUT)   :: nb
      TYPE(Overlap) , POINTER :: tmp
      INTEGER :: degreMax
      INTEGER :: proc
      INTEGER :: compt
      TYPE(Liste) , POINTER :: list,tmplist

      IF (data(1)%Nordre >= 3) THEN
         degreMax = MAXVAL(Mesh(:)%NSommetsMax)
      ELSE
         degreMax = gMesh%NdegreMax
      END IF
      NULLIFY(list)

      ! On compte les elements dans les overlaps + on cree et ordonne des
      ! tableaux contenant les numeros globaux des sommets de ces elements
      compt = 0
      DO proc = 1,nbproc
         tmp => over(proc)%suivant
         DO WHILE(ASSOCIATED(tmp))
            ALLOCATE(tmp%pts(degreMax))
            ! Numeros globaux
            tmp%pts = Nums( proc, Mesh(proc)%Nu(1:degreMax,tmp%Element))
            CALL trie(tmp%pts)
            tmp => tmp%suivant
            ! Hop un element de plus
            compt = compt + 1
         END DO
      END DO

      PRINT *, "FIN Tri : ",compt,"Elements dans les overlaps"
      PRINT *, "Debut supression doublons"

      compt = 0
      ! On supprime les doublons en comparant les tableaux crees au dessus
      DO proc = 1,nbproc
         tmp => over(proc)%suivant
         DO WHILE(ASSOCIATED(tmp))
            compt = compt + 1
            ! Pour optimiser la recherche on cree la liste 
            ! des domaines voisins de cet element
            CALL Libere(List)
            CALL Cree(List,proc,Renumerote(1,tmp%pts))
            tmplist => list
            ! Puis on supprime les doublons uniquement dans 
            ! les overlaps de ces domaines
            DO WHILE(ASSOCIATED(tmplist))
               CALL Supr(tmp%pts,over(tmplist%Elt)%suivant)
               tmplist => tmplist%suivant
            END DO
            ! Maintenant l'element d'overlap est unique : on l'enregistre
            ! dans le bon tableau
            nb = nb + 1
            EltNum( proc , tmp%Element) = nb
            ! Cet element n'existe plus en double, pour accelerer les futurs 
            ! calculs on le supprime de l'overlap
            over(proc)%suivant =>tmp%suivant
            DEALLOCATE(tmp%pts)
            DEALLOCATE(tmp)
            tmp =>over(proc)%suivant
         END DO
      END DO
      PRINT *
      PRINT *,"Au final :", compt ,"Elements dans les overlaps"
    END SUBROUTINE supprimeOverlap

    ! Pour vider une liste
    SUBROUTINE Libere(List)
      TYPE(Liste) , POINTER :: list,tmplist
      DO WHILE(ASSOCIATED(list))
         tmplist => list%suivant
         DEALLOCATE(list)
         list => tmplist
      END DO
    END SUBROUTINE Libere

    ! Cree une liste qui contient les elements de tab, differents de proc
    ! en plus ces elements ne seront presents qu'une fois
    SUBROUTINE Cree(List,proc,tab)
      TYPE(Liste) , POINTER :: list
      INTEGER :: proc
      INTEGER , DIMENSION(:) , INTENT(IN) :: tab

      INTEGER :: i

      NULLIFY(list)
      DO i = 1 , SIZE(tab)
         IF (tab(i) /= proc) CALL Insert(tab(i),list)
      END DO

    END SUBROUTINE Cree

    ! Insert un element dans une liste uniquement s'il n'est deja pas present
    RECURSIVE SUBROUTINE Insert(pt, list)
      TYPE(Liste) , POINTER :: list
      INTEGER , INTENT(in) :: pt
      IF (.NOT. ASSOCIATED(list)) THEN
         ALLOCATE(list)
         NULLIFY(list%suivant )
         list%elt = pt
      ELSE
         IF (pt /= list%elt)  CALL Insert(pt, list%suivant)
      END IF
    END SUBROUTINE Insert

    ! Recherche recursivement si le tableau pts1 n'est pas 
    ! present dans list, s'il le trouve le supprime
    ! la recherche s'arrete la car du fait de la structure utilisee
    ! (une liste d'overlaps par domaine) un tableau ne peut etre
    ! present qu'une seule fois dans la liste
    RECURSIVE SUBROUTINE Supr(pts1, list)
      TYPE(Overlap) , POINTER :: list, tmp
      INTEGER , DIMENSION(:), INTENT(in) :: pts1

      IF (.NOT. ASSOCIATED(list)) RETURN

      IF (ALL(pts1==list%pts)) THEN
         tmp => list%suivant
         DEALLOCATE(list%pts)
         DEALLOCATE(list)
         list => tmp
         ! Plus besoin de continuer la recherche : temps d'exec / 2 !!!
         !! Call Supr(pts1,list)
      ELSE
         CALL Supr(pts1,list%suivant)
      END IF
    END SUBROUTINE Supr

    ! Un simple tri a bulle (les tableaux a trier sont de taille <= 4)
    SUBROUTINE trie(tab)
      INTEGER ,DIMENSION(:) :: tab
      INTEGER :: tmp , i , j

      DO i = SIZE(tab)-1,1,-1
         DO j = 1,i
            tmp = tab(j)
            IF (tmp > tab(j+1)) THEN
               tab(j)   = tab(j+1)
               tab(j+1) = tmp
            END IF
         END DO
      END DO
    END SUBROUTINE trie


    SUBROUTINE InitVertexCentered(Var, Mesh, Data)
      TYPE(Variables) , INTENT(INOUT) :: Var
      TYPE(MeshDef)   , INTENT(INOUT) :: Mesh
      TYPE(Donnees)   , INTENT(INOUT) :: DATA

      CHARACTER(LEN=*), PARAMETER     :: mod_name = "InitVertexCentered"

      Var%Ncells  = Mesh%Npoint !-- c'est pour l'ordre 1 ou 2, sera modifié dans GeomSegments2d
      Mesh%Ncells = Var%Ncells

      SELECT CASE(DATA%sel_geotype)
      CASE(cs_geotype_2dplan)
         CALL GeomSegments2d( Mesh, data%Impre)
         CALL MeshOrderUpdate(Mesh, Data)
         Var%Ncells = Mesh%NCells
!         Var%NCells = Mesh%Npoint

      CASE(cs_geotype_3d)
         CALL GeomSegments3d(DATA,  Mesh)
         CALL GeomFace3D(Mesh)
         CALL MeshOrderUpdate(Mesh, Data)
         Var%Ncells = Mesh%NCells
!         Var%NCells = Mesh%Npoint
      END SELECT
      Var%CellVol => Mesh%Vols
    END SUBROUTINE InitVertexCentered

    !==================
    SUBROUTINE get_Cx()
    !==================
    !
    ! Correct treatment at the overlaps
    ! for the computation of the aerodynamic coeff.
    !
      IMPLICIT NONE

      INTEGER :: jf, j_proc, j_proc_v, j_dom, jf_v, k, k_
      INTEGER :: NU_max, NU_max_v, NCellsIn, NCellsIn_v
      INTEGER :: N_p, N_p_v, N_ovlp, nn, ilog, ilog_v, j_wall
      
      INTEGER, DIMENSION(:), ALLOCATABLE :: NU, NU_v

      REAL :: CL, CD_P, CD_V, rho_oo, U2_oo, q_oo

      CL = 0.d0; CD_P = 0.d0; CD_V = 0.d0 

      DO j_proc = 1, Nbproc

          j_wall = 0

          DO jf = 1, Maill(j_proc)%NfacFr

             ilog = Maill(j_proc)%fr(jf)%inum

             IF (ilog == lgc_paroi_adh_adiab_flux_nul .OR. &
                 ilog == lgc_paroi_glissante ) THEN

                j_wall = j_wall + 1

                Nu_max = MAXVAL(Maill(j_proc)%fr(jf)%NU)
                
                NCellsIn = Var(j_proc)%NCellsIn

                IF( Nu_max <= NCellsIn ) THEN

                   CL = CL + Var(j_proc)%C_loc(1, j_wall)

                   CD_P = CD_P + Var(j_proc)%C_loc(2, j_wall)
                   CD_V = CD_V + Var(j_proc)%C_loc(3, j_wall)

                ELSE 
                   
                   ! The overlaps are identified by the
                   ! elements of different proc.s which 
                   ! have all the vertices with the same coordinates

                   N_p = Maill(j_proc)%fr(jf)%Nsommets

                   ALLOCATE( NU(N_p) )

                   NU = Maill(j_proc)%fr(jf)%NU(1:N_p)

                   N_ovlp = 1

                   DO j_dom = 1, Com(j_proc)%Ndomv

                      ! Neighbouring proc
                      j_proc_v = Com(j_proc)%Jdomv(j_dom)

                      !--- 
                      DO jf_v = 1, Maill(j_proc_v)%NfacFr
                         
                         ilog_v = Maill(j_proc_v)%fr(jf_v)%inum

                         IF (ilog_v == lgc_paroi_adh_adiab_flux_nul .OR. &
                             ilog_v == lgc_paroi_glissante ) THEN

                            Nu_max_v = MAXVAL(Maill(j_proc_v)%fr(jf_v)%NU)

                            NCellsIn_v = Var(j_proc_v)%NCellsIn

                            IF( Nu_max_v > NCellsIn_v ) THEN

                               N_p_v = Maill(j_proc_v)%fr(jf_v)%NSommets

                               IF(N_p_v == N_p) THEN

                                  ALLOCATE( NU_v(N_p_v) )

                                  NU_v = Maill(j_proc_v)%fr(jf_v)%NU(1:N_p)

                                  nn = 0

                                  DO k = 1, N_p

                                     DO k_ = 1, N_p_v

                                        IF( SUM(   Maill(j_proc)%fr(jf)%Coor(:, k) - &
                                                 Maill(j_proc_v)%fr(jf_v)%Coor(:, k_) ) == 0) THEN

                                           nn = nn + 1

                                        ENDIF
                                        
                                     ENDDO
                                     
                                  ENDDO
                                  
                                  IF( nn == N_p_v ) N_ovlp = N_ovlp + 1

                                  DEALLOCATE(NU_v)                                  

                               ENDIF

                            ENDIF

                         ENDIF

                      ENDDO
                      !--- 
                         
                   ENDDO

                   CL = CL + (1.d0/REAL(N_ovlp, 8))*Var(j_proc)%C_loc(1, j_wall)

                   CD_P = CD_P + (1.d0/REAL(N_ovlp, 8))*Var(j_proc)%C_loc(2, j_wall)
                   CD_V = CD_V + (1.d0/REAL(N_ovlp, 8))*Var(j_proc)%C_loc(3, j_wall)

                   DEALLOCATE(NU)

                ENDIF

             ENDIF
             
          ENDDO

       ENDDO

       rho_oo = Data(1)%VarPhyInit(1, 1)
        U2_oo = SUM(Data(1)%VarPhyInit(2:Data(1)%Ndim+1, 1)**2)

       q_oo = 0.5d0*rho_oo*U2_oo

       gVar%CL =   CL   / q_oo
       gVar%CD_P = CD_P / q_oo
       gVar%CD_V = CD_V / q_oo 

    END SUBROUTINE get_Cx
    


  END SUBROUTINE Moulinette
END MODULE MergeVisu
