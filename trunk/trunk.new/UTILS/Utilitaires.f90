!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga, C.Tav�                                  */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MODULE utils
  USE types_enumeres
  USE Lestypes
  USE PLinAlg
  IMPLICIT NONE

!!$  INTERFACE  GradientsVertexCentered
!!$     MODULE PROCEDURE GradientsVertexCentered2DHyb
!!$  END INTERFACE

  REAL, PARAMETER, PRIVATE :: kepsi = 1.0e-9
  REAL, PARAMETER, PRIVATE :: cte0 = 0.0

CONTAINS
  !
  ! Fonctions de base : ici P2
  !



  FUNCTION phip_arete(i,x,N) RESULT(y)
    ! but calculer la valeur de la jieme fonction de base P2 sur les arete a la coordonnee x
    INTEGER, INTENT(in):: i ! indice de la fonction de base, j: indice du point
    INTEGER, INTENT(in):: N ! Nombre de points sur l'arete (3-->ordre3, 4-->Ordre4)
    REAL, INTENT(in):: x!=(/0.,1.,0.5/)
    REAL:: y
    
    y = 0.0 
    SELECT CASE(N)
    CASE(3)
       SELECT CASE(i)
       CASE(1)
          !PHI= (1-x)*(1-2*x) premier point
          y=4.0*x-3.0!3.+4.*x(j)!( (1-2.*x(j)) -2.*( 1-x(j)) )
       CASE(2)
          !phi= x(2*x-1) Extremite
          y=4.0*x-1.! -1+4.*x(j)
       CASE(3)
          !phi= 4.*x*(1-x) milieu
          y=4.0*(1-2.0*x)
       END SELECT
    CASE(4)
       SELECT CASE(i)
       CASE(1)
          !PHI= 9/2*(1-x)*(x-1/3)*(x-2/3) premier point
          y=-11./2.+x*(18.-27/2.*x)!1+x*(-9.+27./2.*x)
       CASE(2)
          !phi= 9/2*x*(x-1/3)*(x-2/3) Extremite
          y=1+x*(-9.+27./2.*x)!-11./2.+x*(18.-27./2.*x)
       CASE(3)
          !phi= -27/2*x*(x-2/3)*(1-x) point a 1/3
          y=9.+(-45.+81./2.*x)*x!-9./2.+(36.-81./2.*x)*x
       CASE(4)
          !phi= 27/2*x*(x-1/3)*(1-x) point a 2/3
          y=-9./2.+(36.-81./2.*x)*x!9.+(-45.+81./2.*x)*x
       END SELECT
    END SELECT
  END FUNCTION phip_arete


  FUNCTION phi_arete(j,x,N) RESULT (basis)
    IMPLICIT NONE
    INTEGER, INTENT(in):: j,N
    REAL, INTENT(in):: x
    REAL:: basis
    
    basis = 0.0
    SELECT CASE(N)
    CASE(3)
       SELECT CASE(j)
       CASE(1)
          basis=(1.0-x)*( 2.0*(1.0-x) -1.0)
       CASE(2)
          basis= x*(2.0* x- 1.0)
       CASE(3)
          basis= 4.0* x*(1.0-x)
       END SELECT
    CASE(4)
       SELECT CASE(j)
       CASE(2)
          basis= 9./2.*(1.0-x)*(x-1./3.)*(x-2./3.)
       CASE(1)
          basis= 9./2.*x*(x-1./3.)*(x-2./3.) ! extremite
       CASE(4)
          basis= -27./2.*x*(1.-x)*(x-2./3.) ! pt 1/3
       CASE(3)
          basis=  27./2.*x*(1.-x)*(x-1./3.) ! pt 2/3
       END SELECT
    END SELECT
  END FUNCTION phi_arete



  FUNCTION basis_function(x,n) RESULT(basis)
    ! version OK pourSRC/MODELS/OrdreTresEleve
    IMPLICIT NONE
    INTEGER:: n, k ! n : ordre=degre+1
    REAL, DIMENSION(3), INTENT(in):: x
    REAL, DIMENSION(10):: basis
    basis=0.
    SELECT CASE(n)
    CASE(3)
       basis(1)= x(1)*( 2.* x(1)-1.)
       basis(2)= x(2)*( 2.* x(2)-1.)
       basis(3)= x(3)*( 2.* x(3)-1.)
       basis(4)= 4.0*x(2)*x(1)
       basis(5)= 4.0*x(3)*x(2)
       basis(6)= 4.0*x(3)*x(1)
    CASE(4)
       DO k=1,3
          basis(k)=9./2.*(x(k)-1./3.)*(x(k)-2./3.)*x(k)
       ENDDO
       basis(4)=x(2)*x(1)*(x(1)-1./3.)*27./2.
       basis(5)=x(2)*x(1)*(x(2)-1./3.)*27./2.
       basis(6)=x(2)*x(3)*(x(2)-1./3.)*27./2.
       basis(7)=x(2)*x(3)*(x(3)-1./3.)*27./2.
       basis(8)=x(3)*x(1)*(x(3)-1./3.)*27./2.
       basis(9)=x(3)*x(1)*(x(1)-1./3.)*27./2.
       basis(10)= x(1)*x(2)*x(3)*27.
    END SELECT
  END FUNCTION basis_function

  FUNCTION normalfunction(l, bary, coor, nordre) RESULT (normal)
    IMPLICIT NONE
    INTEGER:: nordre
    REAL, DIMENSION(3), INTENT(in):: bary   ! coordonnees barycentriques ou on veut calculer
    REAL, DIMENSION(2,10), INTENT(in):: coor ! coordonnees des sommets de points
    REAL, DIMENSION(2):: norm, grad, normal
    INTEGER:: l ! donne le numero de l'arete (redondant avec bary)

    norm = 0.0
    SELECT CASE(nordre)
    CASE(3) ! Ordre 3
       SELECT CASE(l)
       CASE(1) ! on est sur l'arete 1 : sommets 1,4,2, la 3ieme coordonnee bary doit etre nulle.
          ! La convention est que bary(1)=1 en 1
          IF (ABS(bary(3))>0.) THEN
             PRINT*, "erreur",l
             STOP
          ENDIF
          norm=(/-( coor(2,2)-coor(2,1)), coor(1,2)-coor(1,1)/) ! normale de reference
          !         grad = coor(:,1)* gr(1,bary(1),3)+ coor(:,2)* gr(2,bary(1),3) + coor(:,4) * gr(3, bary(1),3)
          grad = coor(:,1)* phip_arete(1,bary(1),3)+ coor(:,2)* phip_arete(2,bary(1),3) + coor(:,4) * phip_arete(3, bary(1),3)
       CASE(2) ! on est sur l'arete 2 : sommets 2,5,3, la 1ere coordonnee bary doit etre nulle.
          ! La convention est que bary(2)=1 en 2
          IF (ABS(bary(1))>0.) THEN
             PRINT*, "erreur",l
             STOP
          ENDIF
          norm=(/-( coor(2,3)-coor(2,2)), coor(1,3)-coor(1,2)/) ! normale de reference
          !          grad = coor(:,2)* gr(1,bary(2),3)+ coor(:,3)* gr(2,bary(2),3) + coor(:,5) * gr(3, bary(2),3)
          grad = coor(:,2)* phip_arete(1,bary(2),3)+ coor(:,3)* phip_arete(2,bary(2),3) + coor(:,5) * phip_arete(3, bary(2),3)
       CASE(3) ! on est sur l'arete 3 : sommets 3,6,1, la 2ieme coordonnee bary doit etre nulle.
          ! La convention est que bary(3)=1 en 3
          IF (ABS(bary(2))>0.) THEN
             PRINT*, "erreur",l
             STOP
          ENDIF
          norm=(/-( coor(2,1)-coor(2,3)), coor(1,1)-coor(1,3)/) ! normale de reference
          !          grad = coor(:,3)* gr(1,bary(3),3)+ coor(:,1)* gr(2,bary(3),3) + coor(:,6) * gr(3, bary(3),3)
          grad = coor(:,3)* phip_arete(1,bary(3),3)+ coor(:,1)* phip_arete(2,bary(3),3) + coor(:,6) * phip_arete(3, bary(3),3)
       END SELECT


    CASE(4)  ! Ordre 4
       SELECT CASE(l)
       CASE(1) ! on est sur l'arete 1 : sommets 1,4,2, la 3ieme coordonnee bary doit etre nulle.
          ! La convention est que bary(1)=1 en 1
          IF (ABS(bary(3))>0.) THEN
             PRINT*, "erreur",l,bary
             STOP
          ENDIF
          norm=(/-( coor(2,2)-coor(2,1)), coor(1,2)-coor(1,1)/) ! normale de reference
          !          grad = coor(:,1)* gr(1,bary(1),4)+ coor(:,2)* gr(2,bary(1),4) + coor(:,4) * gr(3, bary(1),4)+coor(:,5)*gr(4,bary(1),4)
          grad = coor(:,1)* phip_arete(1,bary(1),4)+ coor(:,2)* phip_arete(2,bary(1),4) + &
               & coor(:,4) * phip_arete(3, bary(1),4)+coor(:,5)*phip_arete(4,bary(1),4)
       CASE(2) ! on est sur l'arete 2 : sommets 2,5,3, la 1ere coordonnee bary doit etre nulle.
          ! La convention est que bary(2)=1 en 1
          IF (ABS(bary(1))>0.) THEN
             PRINT*, "erreur",l
             STOP
          ENDIF
          norm=(/-( coor(2,3)-coor(2,2)), coor(1,3)-coor(1,2)/) ! normale de reference
          !         grad = coor(:,2)* gr(1,bary(2),4)+ coor(:,3)* gr(2,bary(2),4) + coor(:,6) * gr(3, bary(2),4)+coor(:,7)*gr(4,bary(2),4)
          grad = coor(:,2)* phip_arete(1,bary(2),4)+ coor(:,3)* phip_arete(2,bary(2),4) + &
               & coor(:,6) * phip_arete(3, bary(2),4)+coor(:,7)*phip_arete(4,bary(2),4)
       CASE(3) ! on est7 sur l'arete 3 : sommets 3,6,1, la 2ieme coordonnee bary doit etre nulle.
          ! La convention est que bary(3)=1 en 1
          IF (ABS(bary(2))>0.) THEN
             PRINT*, "erreur",l,bary
             STOP
          ENDIF
          norm=(/-( coor(2,1)-coor(2,3)), coor(1,1)-coor(1,3)/) ! normale de reference
          !          grad = coor(:,3)* gr(1,bary(3),4)+ coor(:,1)* gr(2,bary(3),4) + coor(:,8) * gr(3, bary(3),4)+coor(:,9)*gr(4,bary(3),4)
          grad = coor(:,3)* phip_arete(1,bary(3),4)+ coor(:,1)* phip_arete(2,bary(3),4) + &
               & coor(:,8) * phip_arete(3, bary(3),4)+coor(:,9)*phip_arete(4,bary(3),4)
       END SELECT
    END SELECT

    normal(1)=-grad(2); normal(2)=grad(1)
    IF (norm(1)*normal(1)+norm(2)*normal(2)>0) THEN
       normal=-normal
    ENDIF

  END FUNCTION normalfunction


  ! Ici on d�finit le gradient sur les lignes qui definissent le bord du "triangle"
  ! La convention est que le point "1" d'efinit l'origine
  ! Dans le cas P2 on  a 1-3-2
  ! Dans le cas P3 on a 1-3-4-2
  REAL FUNCTION gr(l,x,nordre)
    REAL:: x
    INTEGER:: l,nordre

    gr = 0.0
    SELECT CASE(nordre)
    CASE(3) ! ordre 3, poly cubique
       SELECT CASE(l)
       CASE(1)
          gr=4*x-1
       CASE(2)
          gr=-3.0+4.0*x
       CASE(3)
          gr=4.0-8.0*x
       END SELECT
    CASE(4) ! ordre 4, poly cubique
       SELECT CASE(l)
       CASE(1)
          !          gr=1.+(-9.+27./2.*x)*x!-11./2.+(18.-27./2.*x)*x
          gr=-11./2.+(18.-27./2.*x)*x
       CASE(2)
          !          gr=-11./2.+(18.-27./2.*x)*x!1.+(-9.+27./2.*x)*x
          gr=1.+(-9.+27./2.*x)*x
       CASE(4)
          gr=9.+(-45.+81./2.*x)*x
       CASE(3)
          gr=-9./2.+(36.-81./2.*x)*x
       END SELECT
    END SELECT
  END FUNCTION gr

  ! Fin fonctions de base P2
  !-------------------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------------------
  !
  !!  Calcul des Gradients de Fonctions de Base
  !!         Sommets(1:Ndim, 1:Nsmplx)     --> Liste des Sommets de l'Element
  !!         TypeElem                      --> 1  = TRIANGLE P1
  !!                                           2  = TRIANGLE P2
  !!                                           11 = QUADRANGLE Q1
  !!                                           12 = QUADRANGLE Q2
  !!         Point                         --> Points ou le calcul est realise
  !!         GrdPhi(1:Ndim, 1:Nmplx, Q_NP) --> Tableau de Sortie
  !
  !-------------------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------------------
  !
  SUBROUTINE GradPhiGeneric( Sommets, TypeElem, Points, GrdPhi )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GradPhiGeneric"
    !
    !
    REAL, DIMENSION(:, : ), INTENT(IN)      :: Sommets
    REAL, DIMENSION(:, : ), INTENT(IN)      :: Points
    INTEGER,                INTENT(IN)      :: TypeElem
    REAL, DIMENSION(:, :, : ), INTENT(OUT)  :: GrdPhi
    !
    REAL                                            :: x12, y12, x13, y13, x23, y23
    REAL                                            :: Airt
    REAL, DIMENSION(1: 4, 1: 4)                        :: a
    REAL, DIMENSION(4)                              :: s1, s2, s3
    INTEGER                                         :: q_np, k, Nsmplx, l
    !-------------------------------------------
    !  SWITCH SUR TYPE D ELEMENT
    !-------------------------------------------
    !
    q_np   = SIZE( GrdPhi, 3 )
    Nsmplx = SIZE( GrdPhi, 2 )

    s1 = -9.99e99 !-- fausses initialisations pour faire taire l'analyse de flot
    s2 = -9.99e99
    s3 = -9.99e99
    PRINT *, mod_name, " ERREUR : s1, s2, s3 non initialises"
    CALL delegate_stop()

    SELECT CASE( TypeElem )

    CASE( 1  ) ! --- TRIANGLE P1
       x13         = s1(1) - s3(1)
       y13         = s1(2) - s3(2)
       x12         = s1(1) - s2(1)
       y12         = s1(2) - s2(2)
       x23         = s2(1) - s3(1)
       y23         = s2(2) - s3(2)
       Airt        = ABS( x13 * y23 - x23 * y13 )
       GrdPhi(1, 1, : ) =   y23 / Airt
       GrdPhi(2, 1, : ) = - x23 / Airt
       GrdPhi(1, 2, : ) = - y13 / Airt
       GrdPhi(2, 2, : ) =   x13 / Airt
       GrdPhi(1, 3, : ) =   y12 / Airt
       GrdPhi(2, 3, : ) = - x12 / Airt

    CASE ( 11 ) ! --- QUADRANGLE Q1
       a(:, 1) = 1.0
       a(:, 2) = Sommets(1, : )
       a(:, 3) = Sommets(2, : )
       a(:, 4) = Sommets(1, : ) * Sommets(2, : )
       a      = InverseLu( a )
       DO k = 1, q_np
          DO l = 1, Nsmplx
             GrdPhi(1, l, k) = a(2, l) + a(4, l) * Points(2, k)
             GrdPhi(2, l, k) = a(3, l) + a(4, l) * Points(1, k)
          END DO
       END DO

    CASE DEFAULT
       PRINT *, mod_name, " ERREUR : Quadrature: element inconnu  ---> Utilitaires.f90"
       CALL delegate_stop()
    END SELECT

  END SUBROUTINE GradPhiGeneric


  ! ----------------------------------------------------------------------
  !!-- r�le :  calcul des gradients des fonctions de Base P1 (Triangle)
  !!--   Par convention P(1:3, i) = X_{i}
  !!--   et la suite ( X_{1}, X_{2}, X_{3}, X_{4} ) est ORIENTEE!!!
  !--------------------------------------------------------------------
  SUBROUTINE GradPhib2d_hp(s1, s2, s3, GrdPhi )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GradPhib2d_hp"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    REAL(KIND = knd10), DIMENSION(: ), INTENT(IN)     :: s1, s2, s3
    REAL(KIND = knd10), DIMENSION(:, : ), INTENT(OUT)     :: GrdPhi
    ! Variables locales scalaires
    !----------------------------
    REAL(KIND = knd10)     :: x12, y12, x13, y13, x23, y23
    REAL(KIND = knd10)     :: Airt
    INTEGER :: ii, jj
    !--------------------------------------------------------------------
    !                Partie ex�cutable de la proc�dure
    !--------------------------------------------------------------------
    x13         = s1(1) - s3(1)
    y13         = s1(2) - s3(2)
    x12         = s1(1) - s2(1)
    y12         = s1(2) - s2(2)
    x23         = s2(1) - s3(1)
    y23         = s2(2) - s3(2)
    !
    GrdPhi(1, 1) = s2(2) - s3(2)
    GrdPhi(2, 1) = s3(1) - s2(1)
    GrdPhi(1, 2) = s3(2) - s1(2)
    GrdPhi(2, 2) = s1(1) - s3(1)
    GrdPhi(1, 3) = s1(2) - s2(2)
    GrdPhi(2, 3) = s2(1) - s1(1)
    Airt = ABS( x13 * y23 - x23 * y13 )
    GrdPhi = GrdPhi / Airt
    IF (.FALSE. .AND. debug_precision) THEN
       WRITE(6, fmt_prec1) mod_name, "Airt", Airt
       DO jj = 1, SIZE(GrdPhi, 2)
          DO ii = 1, SIZE(GrdPhi, 1)
             PRINT *, mod_name, "ii, jj", ii, jj
             WRITE(6, fmt_prec1) mod_name, "grdphi", GrdPhi(ii, jj)
          END DO
       END DO
    END IF
  END SUBROUTINE GradPhib2d_hp
  SUBROUTINE GradPhib2d(s1, s2, s3, GrdPhi )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GradPhib2d"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
    REAL, DIMENSION(: ), INTENT(IN)     :: s1, s2, s3
    REAL, DIMENSION(:, : ), INTENT(OUT)     :: GrdPhi
    ! Variables locales scalaires
    !----------------------------
    REAL     :: x12, y12, x13, y13, x23, y23
    REAL     :: Airt
    INTEGER :: ii, jj
    !--------------------------------------------------------------------
    !                Partie ex�cutable de la proc�dure
    !--------------------------------------------------------------------
    x13         = s1(1) - s3(1)
    y13         = s1(2) - s3(2)
    x12         = s1(1) - s2(1)
    y12         = s1(2) - s2(2)
    x23         = s2(1) - s3(1)
    y23         = s2(2) - s3(2)
    !
    GrdPhi(1, 1) = s2(2) - s3(2)
    GrdPhi(2, 1) = s3(1) - s2(1)
    GrdPhi(1, 2) = s3(2) - s1(2)
    GrdPhi(2, 2) = s1(1) - s3(1)
    GrdPhi(1, 3) = s1(2) - s2(2)
    GrdPhi(2, 3) = s2(1) - s1(1)
    Airt = ABS( x13 * y23 - x23 * y13 )
    GrdPhi = GrdPhi / Airt
    IF (.FALSE. .AND. debug_precision) THEN
       WRITE(6, fmt_prec1) mod_name, "Airt", Airt
       DO jj = 1, SIZE(GrdPhi, 2)
          DO ii = 1, SIZE(GrdPhi, 1)
             PRINT *, mod_name, "ii, jj", ii, jj
             WRITE(6, fmt_prec1) mod_name, "grdphi", GrdPhi(ii, jj)
          END DO
       END DO
    END IF
  END SUBROUTINE GradPhib2d

  ! ----------------------------------------------------------------------
  !!-- role :  calcul des gradients des fonctions de Base P1 (Triangle) multipli�s par l'aire
  !!--   Par convention P(1:3, i) = X_{i}
  !!--   et la suite ( X_{1}, X_{2}, X_{3}, X_{4} ) est ORIENTEE!!!
!!$  SUBROUTINE GradPhib2DfoisAIRe(s1, s2, s3, GrdPhi )
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "GradPhib2DfoisAIRe"
!!$    !--------------------------------------------------------------------
!!$    ! Type et vocations des Variables d'appel
!!$    !----------------------------------------
!!$    REAL, DIMENSION(: ), INTENT(IN)     :: s1, s2, s3
!!$    REAL, DIMENSION(:, : ), INTENT(OUT)     :: GrdPhi
!!$    ! Variables locales scalaires
!!$    !----------------------------
!!$    REAL     :: x12, y12, x13, y13, x23, y23
!!$
!!$    !--------------------------------------------------------------------
!!$    !                Partie executable de la procedure
!!$    !----------------------------------------------------------------
!!$    x13      = s1(1) - s3(1)
!!$    y13      = s1(2) - s3(2)
!!$    x12      = s1(1) - s2(1)
!!$    y12      = s1(2) - s2(2)
!!$    x23      = s2(1) - s3(1)
!!$    y23      = s2(2) - s3(2)
!!$    !
!!$    GrdPhi(1, 1) =   0.5 * y23
!!$    GrdPhi(2, 1) = - 0.5 * x23
!!$    GrdPhi(1, 2) = - 0.5 * y13
!!$    GrdPhi(2, 2) =   0.5 * x13
!!$    GrdPhi(1, 3) =   0.5 * y12
!!$    GrdPhi(2, 3) = - 0.5 * x12
!!$    !
!!$  END SUBROUTINE GradPhib2DfoisAIRe

  !!-- role :  calcul des gradients des fonctions de base d'un tetraedre
  !!-- Par convention P(1:3, i) = X_{i}
  !!-- et la suite ( X_{1}, X_{2}, X_{3}, X_{4} ) est ORIENTEE!!!
  !----------------------------------------------------
  !!@author     Boniface Nkonga      Universite de Bordeaux 1
  FUNCTION GradPhib3d(x, jt) RESULT(GradPhi)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GradPhib3d"
    !----------------------------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------------------
    REAL, DIMENSION(:, : ), INTENT(IN)     :: x
    INTEGER, INTENT(IN), OPTIONAL :: jt

    REAL, DIMENSION(SIZE(x, dim = 1), SIZE(x, dim = 2))     :: GradPhi
    REAL :: z12, z13, z14, z23, z24, z34
    REAL :: y12, y13, y14, y23, y24, y34
    INTEGER :: icount, ifa
    INTEGER, DIMENSION(3, 4) :: ifac
    REAL, DIMENSION(3) :: fac, g

    z12 = x(3, 2) - x(3, 1)
    y12 = x(2, 2) - x(2, 1)
    z13 = x(3, 3) - x(3, 1)
    y13 = x(2, 3) - x(2, 1)
    z14 = x(3, 4) - x(3, 1)
    y14 = x(2, 4) - x(2, 1)
    z23 = x(3, 3) - x(3, 2)
    y23 = x(2, 3) - x(2, 2)
    z24 = x(3, 4) - x(3, 2)
    y24 = x(2, 4) - x(2, 2)
    z34 = x(3, 4) - x(3, 3)
    y34 = x(2, 4) - x(2, 3)

    GradPhi(1, 1) =  x(2, 2) * z34 - x(2, 3) * z24 + x(2, 4) * z23
    GradPhi(1, 2) = - x(2, 1) * z34 + x(2, 3) * z14 - x(2, 4) * z13
    GradPhi(1, 3) =  x(2, 1) * z24 - x(2, 2) * z14 + x(2, 4) * z12
    GradPhi(1, 4) = - x(2, 1) * z23 + x(2, 2) * z13 - x(2, 3) * z12
    !
    GradPhi(2, 1) = - x(1, 2) * z34 + x(1, 3) * z24 - x(1, 4) * z23
    GradPhi(2, 2) =  x(1, 1) * z34 - x(1, 3) * z14 + x(1, 4) * z13
    GradPhi(2, 3) = - x(1, 1) * z24 + x(1, 2) * z14 - x(1, 4) * z12
    GradPhi(2, 4) =  x(1, 1) * z23 - x(1, 2) * z13 + x(1, 3) * z12
    !
    GradPhi(3, 1) =  x(1, 2) * y34 - x(1, 3) * y24 + x(1, 4) * y23
    GradPhi(3, 2) = - x(1, 1) * y34 + x(1, 3) * y14 - x(1, 4) * y13
    GradPhi(3, 3) =  x(1, 1) * y24 - x(1, 2) * y14 + x(1, 4) * y12
    GradPhi(3, 4) = - x(1, 1) * y23 + x(1, 2) * y13 - x(1, 3) * y12

    !
    ! v�rification
    !
    icount = 0
    g(1) = Sum(x(1, : )) * 0.25
    g(2) = Sum(x(2, : )) * 0.25
    g(3) = Sum(x(3, : )) * 0.25

#ifdef ORDRE_ELEVE
    ifac(:, 1) = (/ 2, 3, 4 /)
    ifac(:, 2) = (/ 4, 3, 1 /)
    ifac(:, 3) = (/ 1, 2, 4 /)
    ifac(:, 4) = (/ 1, 2, 3 /)
#else
    ifac(:, 1) = (/ 1, 2, 3 /)
    ifac(:, 2) = (/ 2, 3, 4 /)
    ifac(:, 3) = (/ 4, 3, 1 /)
    ifac(:, 4) = (/ 1, 2, 4 /)
#endif
    DO ifa = 1, 4
       fac(1) = Sum(x(1, ifac(:, ifa))) / 3.0
       fac(2) = Sum(x(2, ifac(:, ifa))) / 3.0
       fac(3) = Sum(x(3, ifac(:, ifa))) / 3.0
       fac = fac - g
       IF (Sum(fac(: ) * GradPhi(:, ifa)) > 0.0) THEN
          icount = icount + 1
       END IF
    END DO

    IF (icount >= 1 .AND. icount < 4) THEN! si une face est mal orient�e les autres doivent l'�tre aussi
       WRITE(message_log, FMT = *) &
            &"UTILS/Utilitaires.f90/GrapPhib3d : ATTENTION : ALG-ERR : pb non regle %%%%%% Utilitaire", icount
       CALL log_record(message_log)
       PRINT *, Sum(GradPhi(1, : )), Sum(GradPhi(2, : )), Sum(GradPhi(3, : ))
    END IF
    IF (icount >= 1) THEN ! une face est mal orient�e, donc toutes sont mal orient�es, donc on retourne les gradients
       IF (PRESENT(jt)) THEN
          PRINT *, "les faces sont mal orientees au triangle : ", jt
       END IF
       GradPhi = - GradPhi
    END IF
  END FUNCTION GradPhib3d

!!$  !!-- role :  volume d'un tetraedre
!!$  !!-- Par convention P(1:3, i) = X_{i}
!!$  !!-- et la suite ( X_{1}, X_{2}, X_{3}, X_{4} ) est ORIENTEE!!!
!!$  !----------------------------------------------------
!!$  !!@author     Boniface Nkonga      Universite de Bordeaux 1
  SUBROUTINE VolTGradPhib3d(x, Volt, GradPhi)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "VolTGradPhib3d"
    !----------------------------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------------------
    REAL, DIMENSION(:, : ), INTENT(IN)      :: x
    REAL,                    INTENT(OUT)     :: Volt
    REAL, DIMENSION(:, : ), INTENT(OUT)     :: GradPhi
    REAL :: z12, z13, z14, z23, z24, z34
    REAL :: y12, y13, y14, y23, y24, y34

    z12 = x(3, 2) - x(3, 1)
    y12 = x(2, 2) - x(2, 1)
    z13 = x(3, 3) - x(3, 1)
    y13 = x(2, 3) - x(2, 1)
    z14 = x(3, 4) - x(3, 1)
    y14 = x(2, 4) - x(2, 1)
    z23 = x(3, 3) - x(3, 2)
    y23 = x(2, 3) - x(2, 2)
    z24 = x(3, 4) - x(3, 2)
    y24 = x(2, 4) - x(2, 2)
    z34 = x(3, 4) - x(3, 3)
    y34 = x(2, 4) - x(2, 3)

    GradPhi(1, 1) =  x(2, 2) * z34 - x(2, 3) * z24 + x(2, 4) * z23
    GradPhi(1, 2) = - x(2, 1) * z34 + x(2, 3) * z14 - x(2, 4) * z13
    GradPhi(1, 3) =  x(2, 1) * z24 - x(2, 2) * z14 + x(2, 4) * z12
    GradPhi(1, 4) = - x(2, 1) * z23 + x(2, 2) * z13 - x(2, 3) * z12
    !
    GradPhi(2, 1) = - x(1, 2) * z34 + x(1, 3) * z24 - x(1, 4) * z23
    GradPhi(2, 2) =  x(1, 1) * z34 - x(1, 3) * z14 + x(1, 4) * z13
    GradPhi(2, 3) = - x(1, 1) * z24 + x(1, 2) * z14 - x(1, 4) * z12
    GradPhi(2, 4) =  x(1, 1) * z23 - x(1, 2) * z13 + x(1, 3) * z12
    !
    GradPhi(3, 1) =  x(1, 2) * y34 - x(1, 3) * y24 + x(1, 4) * y23
    GradPhi(3, 2) = - x(1, 1) * y34 + x(1, 3) * y14 - x(1, 4) * y13
    GradPhi(3, 3) =  x(1, 1) * y24 - x(1, 2) * y14 + x(1, 4) * y12
    GradPhi(3, 4) = - x(1, 1) * y23 + x(1, 2) * y13 - x(1, 3) * y12

    Volt = ABS(x(1, 1) * GradPhi(1, 1) + x(1, 2) * GradPhi(1, 2)&
         & + x(1, 3) * GradPhi(1, 3) + x(1, 4) * GradPhi(1, 4)) / 6.0
  END SUBROUTINE VolTGradPhib3d

  !---------------------------------------------------------------------------------
  !---------------------------------------------------------------------------------
  !
  !!  Poids et Position des points de quadrature sur triangle (%% approxim�s)
  !!@author  22/01/07 Cedric
  !
  !---------------------------------------------------------------------------------
  !---------------------------------------------------------------------------------
  !
  SUBROUTINE Quadrature2D_TRi( qploc, qpwght, np )
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Quadrature2D_TRi"
    REAL, DIMENSION(: ), INTENT(INOUT)          :: qploc
    REAL, DIMENSION(: ), INTENT(INOUT)          :: qpwght
    INTEGER,            INTENT(INOUT)          :: np


    SELECT CASE( np )

       ! Quadrature a   1 POINT
       !
    CASE( 1 )
       qploc   = (/  0.33333333333333d0, 0.33333333333333d0, 0.33333333333333d0 /)
       qpwght  =  1.0d0

       ! Quadrature a   3 POINTS
       !
    CASE( 3 )
       qploc   = (/ 0.5000000000000d0, 0.50000000000000d0, 0.00000000000000d0, &
            &      0.0000000000000d0, 0.50000000000000d0, 0.50000000000000d0, &
            &      0.5000000000000d0, 0.00000000000000d0, 0.50000000000000d0  /)
       qpwght  = (/ 0.33333333333333d0, 0.33333333333333d0, 0.33333333333333d0 /)

       ! Quadrature a   4 POINTS
       !
    CASE( 4 )
       qploc   = (/ 0.3333333333333d0, 0.33333333333333d0, 0.33333333333333d0, &
            &      0.6000000000000d0, 0.20000000000000d0, 0.20000000000000d0, &
            &      0.2000000000000d0, 0.60000000000000d0, 0.20000000000000d0, &
            &      0.2000000000000d0, 0.20000000000000d0, 0.60000000000000d0  /)
       qpwght  = (/ -0.56250000000000d0, 0.52083333333333d0, 0.52083333333333d0, 0.52083333333333d0 /)

       ! Quadrature a   6 POINTS
       !
    CASE( 6 )
       qploc   = (/ 0.816847572980459d0, 0.091576213509771d0, 0.091576213509771d0, &
            &     0.091576213509771d0, 0.816847572980459d0, 0.091576213509771d0, &
            &     0.091576213509771d0, 0.091576213509771d0, 0.816847572980459d0, &
            &     0.108103018168070d0, 0.445948490915965d0, 0.445948490915965d0, &
            &     0.445948490915965d0, 0.108103018168070d0, 0.445948490915965d0, &
            &     0.445948490915965d0, 0.445948490915965d0, 0.108103018168070d0 /)
       qpwght  = (/ 0.109951743655322d0, 0.109951743655322d0, 0.109951743655322d0, &
            &     0.223381589678011d0, 0.223381589678011d0, 0.223381589678011d0 /)

       ! Quadrature a   7 POINTS
       !
    CASE( 7 )
       qploc   = (/ 0.333333333333333d0, 0.333333333333333d0, 0.333333333333333d0, &
            &     0.797426985353087d0, 0.101286507323456d0, 0.101286507323456d0, &
            &     0.101286507323456d0, 0.797426985353087d0, 0.101286507323456d0, &
            &     0.101286507323456d0, 0.101286507323456d0, 0.797426985353087d0, &
            &     0.470142064105115d0, 0.470142064105115d0, 0.059715871789770d0, &
            &     0.059715871789770d0, 0.470142064105115d0, 0.470142064105115d0, &
            &     0.470142064105115d0, 0.059715871789770d0, 0.470142064105115d0 /)
       qpwght  = (/ 0.225000000000000d0, &
            &     0.125939180544827d0, 0.125939180544827d0, 0.125939180544827d0, &
            &     0.132394152788506d0, 0.132394152788506d0, 0.132394152788506d0 /)

       ! Quadrature a   16 POINTS
       !
    CASE( 16 )
       qploc   = (/ 0.5710419610d-01, 0.6546699267d-01, 0.8774288112d+00, &
            &  0.2768430136d+00, 0.5021012177d-01, 0.6729468646d+00, &
            &  0.5835904324d+00, 0.2891208339d-01, 0.3874974842d+00, &
            &  0.8602401357d+00, 0.9703784844d-02, 0.1300560795d+00, &
            &  0.5710419610d-01, 0.3111645522d+00, 0.6317312517d+00, &
            &  0.2768430136d+00, 0.2386486597d+00, 0.4845083267d+00, &
            &  0.5835904324d+00, 0.1374191041d+00, 0.2789904635d+00, &
            &  0.8602401357d+00, 0.4612207989d-01, 0.9363778441d-01, &
            &  0.5710419610d-01, 0.6317312517d+00, 0.3111645522d+00, &
            &  0.2768430136d+00, 0.4845083267d+00, 0.2386486597d+00, &
            &  0.5835904324d+00, 0.2789904635d+00, 0.1374191041d+00, &
            &  0.8602401357d+00, 0.9363778441d-01, 0.4612207989d-01, &
            &  0.5710419610d-01, 0.8774288093d+00, 0.6546699455d-01, &
            &  0.2768430136d+00, 0.6729468632d+00, 0.5021012321d-01, &
            &  0.5835904324d+00, 0.3874974834d+00, 0.2891208422d-01, &
            &  0.8602401357d+00, 0.1300560792d+00, 0.9703785123d-02 /)
       qpwght  = (/ 0.4713673638d-01, 0.7077613581d-01, 0.4516809857d-01, 0.1084645181d-01, &
            &  0.8837017702d-01, 0.1326884322d+00, 0.8467944903d-01, 0.2033451909d-01, &
            &  0.8837017702d-01, 0.1326884322d+00, 0.8467944903d-01, 0.2033451909d-01, &
            &  0.4713673638d-01, 0.7077613581d-01, 0.4516809857d-01, 0.1084645181d-01 /)

    CASE DEFAULT
       PRINT *, mod_name, " Quadrature NON DISPONIBLE !! -------> (Utilitaires.f90)  "
       CALL delegate_stop()
    END SELECT

  END SUBROUTINE Quadrature2D_TRi




  !!-- Calcule var%grad, le gradient approxim� de chaque variable dans chaque direction
!!$  SUBROUTINE GradientsVertexCentered2DHyb(DATA, Mesh, Var)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "GradientsVertexCentered2DHyb"
!!$    ! Type et vocations des Variables d'appel
!!$    !----------------------------------------
!!$    TYPE(MeshDef),    INTENT(IN)        :: Mesh
!!$    TYPE(Donnees),    INTENT(IN)        :: DATA
!!$    TYPE(variables),  INTENT(INOUT)     :: Var
!!$    ! Variables locales scalaires
!!$    !----------------------------
!!$    INTEGER  :: Jt, Nt, j
!!$    INTEGER  :: is, Ns, is1, is2, isg, ifr
!!$    ! Variables locales tableaux
!!$    !----------------------------
!!$    REAL, DIMENSION(data%Ndim, Mesh%NdegreMax + 1)  :: Coor
!!$    INTEGER, DIMENSION( Mesh%NdegreMax  )           :: Nuloc
!!$    REAL, DIMENSION(data%NvarPhy, Mesh%NdegreMax + 1)  :: Vp
!!$    REAL, DIMENSION(data%NvarPhy)                   :: Vmoy
!!$    REAL, DIMENSION(1: 2)                            :: s1, s2, pm, n, g
!!$    INTEGER :: Nsmplx
!!$    INTEGER :: ii
!!$    !--------------------------------------------------------------------
!!$    !                Partie executable de la procedure
!!$    !--------------------------------------------------------------------
!!$    Nt     = Mesh%Nelemt
!!$    Ns     = Mesh%Npoint
!!$    Var%Grad   = 0.0
!!$
!!$    Nt   = Mesh%Nelemt
!!$
!!$    !
!!$    ! grad X \approx 1/Ci int_Ci grad X dCi = 1/Ci int_Gi X N
!!$    !
!!$    elements: DO  Jt = 1, Nt
!!$       ! On parcours des �l�ments
!!$       Nsmplx = Mesh%Ndegre(Jt)
!!$       isg = Nsmplx + 1
!!$       g = 0.0
!!$       Vp(:, isg) = 0.0
!!$       ! R�cup�ration des valeurs primales aux noeuds
!!$       segments_init: DO j = 1, Nsmplx
!!$          is         = Mesh%Nu(j, Jt)
!!$          Nuloc(j)   = is
!!$          Coor(:, j) = Mesh%Coon(:, is)
!!$          ! On travaille sur les variables primaires  r, u, v, (w, ), ei, p, c, T...
!!$          Vp(:, j)      = Var%Vp(:, is)
!!$          ! Interpolation au "centre" (suppos� etre le barycentre).
!!$          ! Ce qui permet de retrouver les EF P1 sur des triangles, et les DF centr�s
!!$          ! d'ordre 2 sur  des quadrangles cart�siens (mm pas en x, mm pas en y, angles droits).
!!$          !    ---> changer les coeffs si ce n'est pas le cas.
!!$          g(: ) = g(: ) + Coor(:, j)
!!$          Vp(:, isg) = Vp(:, isg) + Vp(:, j)
!!$       END DO segments_init
!!$       g(: ) = g(: ) / Nsmplx
!!$       DO ii = 1, isg -1
!!$          Vp(:, ii) = Vp(:, ii) * 0.25
!!$       END DO
!!$       Vp(:, isg  ) = Vp(:, isg  ) / Nsmplx * 0.50
!!$
!!$       segments_grad: DO j = 1, Nsmplx
!!$          ! sommets du segment et point milieu
!!$          is1 = j
!!$          is2 = MODULO(j, Nsmplx) + 1
!!$          s1 = Coor(:, is1)
!!$          s2 = Coor(:, is2)
!!$          pm = 0.5 * (s1 + s2)
!!$          ! Normale de S1 vers S2 --> N.(S2-S1)
!!$          n = (/ g(2) - pm(2), pm(1) - g(1) /)
!!$          !          Vmoy = 0.5*Vp(:, isg)+0.25*(Vp(:, is1)+Vp(:, is2))
!!$          Vmoy =      Vp(:, isg) + Vp(:, is1) + Vp(:, is2)
!!$          Var%Grad(1, :, Nuloc(is1)) = Var%Grad(1, :, Nuloc(is1)) + Vmoy * n(1) ! Valeur au milieu du segment
!!$          Var%Grad(2, :, Nuloc(is1)) = Var%Grad(2, :, Nuloc(is1)) + Vmoy * n(2) ! Valeur au milieu du segment
!!$          Var%Grad(1, :, Nuloc(is2)) = Var%Grad(1, :, Nuloc(is2)) - Vmoy * n(1) ! Valeur au milieu du segment
!!$          Var%Grad(2, :, Nuloc(is2)) = Var%Grad(2, :, Nuloc(is2)) - Vmoy * n(2) ! Valeur au milieu du segment
!!$       END DO segments_grad
!!$    END DO elements
!!$
!!$    ! Boucle sur les fronti�res
!!$    DO ifr = 1, Mesh%NFacFr
!!$       is1   = Mesh%NsFacFr(1, ifr)
!!$       is2   = Mesh%NsFacFr(2, ifr)
!!$       Vp(:, 1) = Var%Vp(:, is1)
!!$       Vp(:, 2) = Var%Vp(:, is2)
!!$       n = Mesh%VnoFr(:, ifr)
!!$       Vmoy =  0.375 * Vp(:, 1) + 0.125 * Vp(:, 2)
!!$       Var%Grad(1, :, is1) = Var%Grad(1, :, is1) + Vmoy * n(1) ! Valeur au milieu du segment
!!$       Var%Grad(2, :, is1) = Var%Grad(2, :, is1) + Vmoy * n(2) ! Valeur au milieu du segment
!!$       Vmoy =  0.125 * Vp(:, 1) + 0.375 * Vp(:, 2)
!!$       Var%Grad(1, :, is2) = Var%Grad(1, :, is2) + Vmoy * n(1) ! Valeur au milieu du segment
!!$       Var%Grad(2, :, is2) = Var%Grad(2, :, is2) + Vmoy * n(2) ! Valeur au milieu du segment
!!$    END DO
!!$
!!$    ! Normalisation
!!$    DO is = 1, Var%NCells
!!$       Var%Grad(:, :, is) = Var%Grad(:, :, is) / Var%CellVol(is)
!!$    END DO
!!$  END SUBROUTINE GradientsVertexCentered2DHyb




  !!-- Calcule var%grad
!!$  SUBROUTINE GradientsVertexCentered3Dsplx(Mesh, Var)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "GradientsVertexCentered3Dsplx"
!!$    ! Type et vocations des Variables d'appel
!!$    !----------------------------------------
!!$    TYPE(MeshDef),    INTENT(IN)        :: Mesh
!!$    TYPE(variables),  INTENT(INOUT)     :: Var
!!$    ! Variables locales scalaires
!!$    !----------------------------
!!$    INTEGER  :: Jt, Nt, k, Ndim
!!$    INTEGER  :: is, Ns, is1, is2, is3, is4
!!$    REAL     :: Volt
!!$    ! Variables locales tableaux
!!$    !----------------------------
!!$    REAL, DIMENSION(1: SIZE(Var%Grad, 2))   :: Gradl
!!$    REAL, DIMENSION(1: 3, 1: 4)              :: x
!!$    REAL, DIMENSION(1: 3, 1: 4)              :: Cq
!!$    REAL, DIMENSION(1: SIZE(Var%Grad, 3))   :: Vnorm
!!$    !--------------------------------------------------------------------
!!$    !                Partie executable de la procedure
!!$    !--------------------------------------------------------------------
!!$    Nt     = Mesh%Nelemt
!!$    Ns     = Mesh%Npoint
!!$    Var%Grad   = 0.0
!!$    Vnorm      = 0.0
!!$    Ndim   = Mesh%Ndim
!!$
!!$    DO Jt = 1, Nt
!!$       is1 = Mesh%Nu(1, Jt)
!!$       is2 = Mesh%Nu(2, Jt)
!!$       is3 = Mesh%Nu(3, Jt)
!!$       is4 = Mesh%Nu(4, Jt)
!!$
!!$       x(:, 1)  = Mesh%Coor(:, is1)
!!$       x(:, 2)  = Mesh%Coor(:, is2)
!!$       x(:, 3)  = Mesh%Coor(:, is3)
!!$       x(:, 4)  = Mesh%Coor(:, is4)
!!$
!!$       CALL VolTGradPhib3d(x, Volt, Cq)
!!$
!!$       DO k = 1, Ndim
!!$          Gradl  =   Cq(k, 1) * Var%Vp(:, is1) + Cq(k, 2) * Var%Vp(:, is2) &
!!$               &   + Cq(k, 3) * Var%Vp(:, is1) + Cq(k, 4) * Var%Vp(:, is4)
!!$
!!$          Var%Grad(k, :, is1) =  Var%Grad(k, :, is1) + Gradl
!!$          Var%Grad(k, :, is2) =  Var%Grad(k, :, is2) + Gradl
!!$          Var%Grad(k, :, is3) =  Var%Grad(k, :, is3) + Gradl
!!$          Var%Grad(k, :, is4) =  Var%Grad(k, :, is4) + Gradl
!!$       END DO
!!$       Vnorm(is1)   = Vnorm(is1) + Volt
!!$       Vnorm(is2)   = Vnorm(is2) + Volt
!!$       Vnorm(is3)   = Vnorm(is3) + Volt
!!$       Vnorm(is4)   = Vnorm(is4) + Volt
!!$    END DO
!!$    !
!!$    ! Normalisation
!!$    !
!!$    DO is = 1, Ns
!!$       Var%Grad(:, :, is) = Var%Grad(:, :, is) / Vnorm(is)
!!$    END DO
!!$    !
!!$  END SUBROUTINE GradientsVertexCentered3Dsplx


!!$
!!$  SUBROUTINE DistanceAuBord(TheFlux, Mesh, Phi, Dtmax, Dh, ResMax, Ktmax, DATA)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "DistanceAuBord"
!!$    INTEGER, INTENT(IN)                    :: TheFlux
!!$    TYPE(MeshDef),    INTENT(IN)           :: Mesh
!!$    REAL,     INTENT(INOUT), DIMENSION(: ) :: Phi
!!$    REAL,     INTENT(IN)                   :: Dtmax, Dh, ResMax
!!$    INTEGER,          INTENT(IN)           :: Ktmax
!!$    TYPE(Donnees),    INTENT(IN)           :: DATA
!!$
!!$    INTEGER                                 :: kt
!!$    REAL                                    :: Dt, res
!!$    REAL,     DIMENSION(SIZE(Phi))  :: Dist
!!$    ! ---------------------------------------------------------------------
!!$    Phi  = Dh
!!$#ifdef TURBULENCE_SA
!!$    Dt   = MIN(Dtmax, 0.5 * Dh)
!!$#else
!!$    Dt   = Dtmax ! Pour suppression de warning
!!$    Dt   = 0.5 * Dh
!!$#endif
!!$
!!$    DO kt = 1, Ktmax
!!$       Dist = Phi
!!$       CALL Distance(TheFlux, Mesh, Phi, Dt)
!!$       CALL BordFixe(Mesh, Phi, DATA)
!!$       res = SQRT( Sum( (Dist - Phi) ** 2 ) )
!!$       IF (MOD(kt, 100) == 0) THEN
!!$          WRITE(6, *) mod_name, kt, MAXVAL((Phi)), MINVAL((Phi))
!!$          WRITE(6, *) " residu = ", res
!!$       END IF
!!$       IF (res < ResMax ) THEN
!!$          EXIT
!!$       END IF
!!$    END DO
!!$
!!$    CALL medit2(Mesh, Phi, "Dist")
!!$
!!$  END SUBROUTINE DistanceAuBord
!!$
!!$  SUBROUTINE Distance(TheFlux, Mesh, Phi, Dt, ThePhis)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "Distance"
!!$    INTEGER, INTENT(IN)                           :: TheFlux
!!$    TYPE(MeshDef),    INTENT(IN)                  :: Mesh
!!$    REAL,     INTENT(INOUT), DIMENSION(: )        :: Phi
!!$    REAL,     INTENT(IN), DIMENSION(: ), OPTIONAL :: ThePhis
!!$    REAL,          INTENT(IN)                     :: Dt
!!$
!!$    INTEGER                                   :: is, is1, is2, is3, Jt
!!$    REAL                                      :: Airt, Norm, ray
!!$    REAL, DIMENSION(Mesh%Ndim)                :: x1, x2, x3, v12, v13, v23, DxPhi
!!$    REAL, DIMENSION(Mesh%Ndim + 1)            ::  Al, Fm
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%Ndim + 1) ::  Cq, Vn
!!$    REAL, DIMENSION(SIZE(Phi))                :: Phis, Flux, Vol
!!$    ! ----------------------------------------------------------------
!!$    IF (PRESENT(ThePhis) ) THEN
!!$       Phis = ThePhis
!!$    ELSE
!!$       Phis = 1.0
!!$    END IF
!!$
!!$    SELECT CASE(TheFlux)
!!$    CASE(cs_dist_SchemaN, cs_dist_LaxF) !-- %%%% cela serait mieux en entier, plus loin c'est m�me dans une boucle
!!$       Flux = 0.0
!!$       Vol  = 0.0
!!$    CASE(cs_dist_Godunov)
!!$       Flux = - 1.0d+30
!!$       Vol = 0.0d0
!!$       DO is = 1, Mesh%Npoint
!!$          IF (Phis(is) < 0 ) THEN
!!$             Flux(is) = 1.0d+30
!!$          END IF
!!$       END DO
!!$    END SELECT
!!$
!!$
!!$    DO Jt = 1, Mesh%Nelemt
!!$
!!$       is1 = Mesh%Nu(1, Jt)
!!$       is2 = Mesh%Nu(2, Jt)
!!$       is3 = Mesh%Nu(3, Jt)
!!$
!!$       x1  = Mesh%Coor(:, is1)
!!$       x2  = Mesh%Coor(:, is2)
!!$       x3  = Mesh%Coor(:, is3)
!!$
!!$
!!$       v12 = x1 - x2
!!$       v13 = x1 - x3
!!$       v23 = x2 - x3
!!$
!!$       Airt  = 0.5 * ABS( v13(1) * v23(2) - v13(2) * v23(1) )
!!$
!!$
!!$       ! Gradients des fonctions de base P1
!!$       ! -------------------------------------
!!$       Cq(:, 1) = 0.5 * (/   v23(2), - v23(1) /) / Airt
!!$       Cq(:, 2) = 0.5 * (/ - v13(2), v13(1) /) / Airt
!!$       Cq(:, 3) = 0.5 * (/   v12(2), - v12(1) /) / Airt
!!$
!!$       ray = Airt
!!$       !
!!$       ! Calcul de la fonction moyenne F
!!$       ! -----------------------------------------
!!$
!!$       ! Gradient de la fonction "level set " (P1)
!!$       ! -----------------------------------------
!!$       DxPhi  = Phi(is1) * Cq(:, 1) + Phi(is2) * Cq(:, 2) + Phi(is3) * Cq(:, 3)
!!$
!!$       ! Normales aux faces du triangle
!!$       ! ------------------------------
!!$       Vn    = 2.0 * Airt * Cq
!!$
!!$       Norm  = SQRT( Sum(DxPhi ** 2) )
!!$
!!$       SELECT CASE(TheFlux)
!!$       CASE(cs_dist_laxf)
!!$          Fm(1) = hamlxf(DxPhi, Phi(is1), Phi(is2), Phi(is3), &
!!$               & Vn(:, 1), Vn(:, 2), Vn(:, 3), Airt, Phis(is1))
!!$          Fm(2) = hamlxf(DxPhi, Phi(is2), Phi(is3), Phi(is1), &
!!$               & Vn(:, 2), Vn(:, 3), Vn(:, 1), Airt, Phis(is2))
!!$          Fm(3) = hamlxf(DxPhi, Phi(is3), Phi(is1), Phi(is2), &
!!$               & Vn(:, 3), Vn(:, 1), Vn(:, 2), Airt, Phis(is3))
!!$          Al    = 1.0
!!$       CASE(cs_dist_Godunov)
!!$          Fm(1) = ham(Vn(:, 2), Vn(:, 3), DxPhi, Phis(is1))
!!$          Fm(2) = ham(Vn(:, 3), Vn(:, 1), DxPhi, Phis(is2))
!!$          Fm(3) = ham(Vn(:, 1), Vn(:, 2), DxPhi, Phis(is3))
!!$          Al    = 1.0
!!$       END SELECT
!!$
!!$       CALL Miseajour(is1, 1)
!!$       CALL Miseajour(is2, 2)
!!$       CALL Miseajour(is3, 3)
!!$    END DO
!!$
!!$    ! mise a jour de la  fonction level Set
!!$    ! -------------------------------------
!!$    WHERE(Vol > 0) Phi = Phi  -  Dt * Flux / Vol
!!$
!!$  CONTAINS
!!$
!!$    SUBROUTINE Miseajour(js, ks)
!!$      CHARACTER(LEN = *), PARAMETER :: mod_name = "Miseajour"
!!$      INTEGER, INTENT(IN) :: js, ks
!!$
!!$      SELECT CASE(TheFlux)
!!$
!!$      CASE(cs_dist_laxf)
!!$         Flux(js) = Flux(js) +  Fm(ks)
!!$         Vol(js)  = Vol(js)  +  Al(ks) * Airt
!!$
!!$      CASE(cs_dist_Godunov)
!!$         IF (Phis(js) >= 0 ) THEN
!!$            Flux(js) = MAX( Flux(js), Phis(js) * (Fm(ks) - 1.0e0) )
!!$         ELSE
!!$            Flux(js) = MIN( Flux(js), Phis(js) * (Fm(ks) - 1.0e0) )
!!$         END IF
!!$         Vol(js)  = 1.0
!!$
!!$      END SELECT
!!$    END SUBROUTINE Miseajour
!!$  END SUBROUTINE Distance
!!$
!!$  SUBROUTINE BordFixe(Mesh, Phi, DATA)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "BordFixe"
!!$    TYPE(MeshDef),    INTENT(IN)        :: Mesh
!!$    REAL,  DIMENSION(: ), INTENT(INOUT)  :: Phi
!!$    TYPE(Donnees),    INTENT(IN)        :: DATA
!!$    INTEGER :: ifr, inum, ilog, i, is, Fact
!!$
!!$    !     Boucle sur les segments frontieres
!!$    !     **********************************
!!$    DO ifr = 1, Mesh%NFacFr
!!$       inum  = Mesh%LogFr(ifr)
!!$       ilog  = data%FacMap(inum)
!!$       !
!!$       ! IF (ilog ) ici pour filtrer les bords
!!$       ! -----------------------------------
!!$       Fact  = SIZE(Mesh%NsFacFr(:, ifr))
!!$       DO i = 1, Fact
!!$          is = Mesh%NsFacFr(i, ifr)
!!$#ifdef TURBULENCE_SA
!!$          SELECT CASE (ilog)
!!$             !                 CASE (lgc_entree_sa, lgc_sortie_sa) !-- rien
!!$          CASE (lgc_paroi_glissante, lgc_paroi_adh_adiab_flux_nul, lgc_paroi_misc)
!!$             Phi(is) = 0.0
!!$          END SELECT
!!$#else
!!$          Phi(is) = 0.0
!!$#endif
!!$       END DO
!!$    END DO
!!$  END SUBROUTINE BordFixe


  !-----------------------------------------------------------------
  !!--  Role : Calcule les flux moyens d'un champ de vecteurs
  !!--  sur chaque cellule duale en VertexCentered
  !!--  (sert par exemple � estimer la divergence de B pour la MHD)
  !!--  NB : Exact pour les champs lin�aires seulement
  !!--       (pas d'ordre �lev�)
  !-----------------------------------------------------------------
!!$  SUBROUTINE Eval_DualFluxVc(Mesh, VecPt, DualFlux)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "Eval_DualFluxVc"
!!$    ! Variables d'appel
!!$    TYPE(MeshDef),        INTENT(IN)    :: Mesh
!!$    REAL, DIMENSION(:, : ), INTENT(IN)  :: VecPt
!!$    REAL, DIMENSION(: ), INTENT(INOUT) :: DualFlux
!!$    ! Variables locales
!!$    REAL :: Fe
!!$    REAL, DIMENSION(1: Mesh%Ndim)    :: Vno
!!$
!!$    INTEGER              :: is1, is2, iseg, is, Nseg, i, ifr, Nsegmtfr, Fact
!!$
!!$    DualFlux = 0.0
!!$    Nseg     = Mesh%Nsegmt
!!$
!!$    DO iseg = 1, Nseg
!!$       is1 = Mesh%Nubo(1, iseg)
!!$       is2 = Mesh%Nubo(2, iseg)
!!$       Vno = Mesh%Vno(:, iseg)
!!$
!!$
!!$       fe = 0.5 * Sum( (VecPt(:, is1) + VecPt(:, is2)) * Vno )
!!$
!!$       ! de mani�re � assurer la conservativit� du flux � l'interface.
!!$       DualFlux(is1) = DualFlux(is1) + fe
!!$       DualFlux(is2) = DualFlux(is2) - fe
!!$    END DO
!!$
!!$
!!$    Nsegmtfr = Mesh%NFacFr
!!$    DO ifr = 1, Nsegmtfr
!!$
!!$       Vno   = Mesh%VnoFr(:, ifr)
!!$       Fact  = SIZE(Mesh%NsFacFr(:, ifr))
!!$
!!$       DO i = 1, Fact
!!$          is = Mesh%NsFacFr(i, ifr)
!!$          DualFlux(is) = DualFlux(is) + Sum( VecPt(:, is) * Vno ) / Fact
!!$       END DO
!!$    END DO
!!$
!!$    ! Moyennage
!!$    DO is = 1, Mesh%Npoint
!!$       DualFlux(is) = DualFlux(is) / Mesh%Vols(is)
!!$    END DO
!!$
!!$  END SUBROUTINE Eval_DualFluxVc
!!$
!!$  !-----------------------------------------------------------------
!!$  !!--  Role : Calcule les flux moyens d'un champ de vecteurs
!!$  !!--  sur chaque cellule duale en VertexCentered
!!$  !!--  (sert par exemple � estimer la divergence de B pour la MHD)
!!$  !!--  NB : Exact pour les champs lin�aires seulement
!!$  !!--       (pas d'ordre �lev�)
!!$  !-----------------------------------------------------------------
!!$  SUBROUTINE Eval_DualFluxVCOld(Mesh, VecPt, DualFlux)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "Eval_DualFluxVCOld"
!!$    ! Variables d'appel
!!$    TYPE(MeshDef),        INTENT(IN)    :: Mesh
!!$    REAL, DIMENSION(:, : ), INTENT(IN)    :: VecPt
!!$    REAL, DIMENSION(: ), INTENT(INOUT) :: DualFlux
!!$    ! Variables locales
!!$    REAL, DIMENSION(Mesh%Ndim)                                              :: Vec_g ! Valeur de VecPt au centre de gravit� (moyenne arithm�tique)
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%NdegreMax)                               :: Coor
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%NdegreMax, Mesh%NdegreMax)                :: MilSeg ! Milieu du segment ij (mais si ij calcul�, ji ne l'est pas)
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%NdegreMax, Mesh%NdegreMax)                :: VecSeg ! VecSeg(:, i, j) == valeur de Vecteur au milieu du segment ij (idem)
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%NdegreMax, Mesh%NdegreMax, Mesh%NdegreMax) :: MilFac
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%NdegreMax, Mesh%NdegreMax, Mesh%NdegreMax) :: VecFac
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%NdegreMax, Mesh%NdegreMax)                :: VnoDc, VecDc
!!$    REAL, DIMENSION(Mesh%Ndim, Mesh%NdegreMax)                               :: Vectm
!!$    INTEGER, DIMENSION(Mesh%NdegreMax)                                      :: Num
!!$    INTEGER                                           :: Deg, jt, js, js1, js2, jss, is, is1, is2, ifr
!!$    LOGICAL, SAVE :: said = .FALSE.
!!$    LOGICAL :: eseen
!!$
!!$    DualFlux = 0.0
!!$    Deg = 0
!!$
!!$    MilSeg = 0.0
!!$    VecSeg = 0.0
!!$    MilFac = 0.0
!!$    VecFac = 0.0
!!$    VnoDc  = 0.0
!!$    Vectm  = 0.0
!!$    Vec_g  = 0.0
!!$    Num    = 0
!!$
!!$    DO jt = 1, Mesh%Nelemt !-- Boucle sur les �l�ments
!!$
!!$       Deg = Mesh%Ndegre(jt)
!!$       DO js = 1, Deg
!!$          Num(js)    = Mesh%Nu(js, jt)
!!$          Coor(:, js) = Mesh%Coon(:, Num(js))
!!$          Vec_g(: ) = Vec_g(: ) + VecPt(:, Num(js))
!!$       END DO
!!$       Vec_g(: ) = Vec_g(: ) / REAL(Deg)
!!$
!!$       ! Calcul des coordonn�es et valeurs de VecPt aux mileux des segments
!!$       ! (ou ar�tes en 3D) et aux centres des facettes
!!$       SELECT CASE(Mesh%Ndim)
!!$       CASE(2) !-- ndim
!!$          SELECT CASE(Deg)
!!$          CASE(3) !-- 3 segments : 12, 23, 31
!!$             DO js = 1, 3
!!$                js1 = MODULO(js, 3) + 1
!!$                MilSeg(:, js, js1) = 0.5 * (Coor(:, js) + Coor(:, js1))
!!$                VecSeg(:, js, js1) = 0.5 * (VecPt(:, Num(js)) + VecPt(:, Num(js1)))
!!$             END DO
!!$          CASE(4) !-- 4 segments : 12, 23, 34, 41
!!$             DO js = 1, 4
!!$                js1 = MODULO(js, 4) + 1
!!$                MilSeg(:, js, js1) = 0.5 * (Coor(:, js) + Coor(:, js1))
!!$                VecSeg(:, js, js1) = 0.5 * (VecPt(:, Num(js)) + VecPt(:, Num(js1)))
!!$             END DO
!!$          END SELECT
!!$       CASE(3) !-- ndim
!!$          SELECT CASE(Deg)
!!$          CASE(4) !-- 6 segments : 12, 23, 34, 41, 13, 24 et 4 facettes : 123, 412, 423, 431
!!$             DO js = 1, 3 ! 12, 23, 31 et 41, 42, 43
!!$                js1 = MODULO(js, 3) + 1
!!$                MilSeg(:, js, js1) = 0.5 * (Coor(:, js) + Coor(:, js1))
!!$                VecSeg(:, js, js1) = 0.5 * (VecPt(:, Num(js)) + VecPt(:, Num(js1)))
!!$                MilSeg(:, 4, js) = 0.5 * (Coor(:, 4) + Coor(:, js))
!!$                VecSeg(:, 4, js) = 0.5 * (VecPt(:, Num(4)) + VecPt(:, Num(js)))
!!$             END DO
!!$             ! 123
!!$             MilFac(:, 1, 2, 3) = (Coor(:, 1) + Coor(:, 2) + Coor(:, 3)) / 3.0
!!$             DO js = 1, 3 ! 412, 423, 431
!!$                js1 = MODULO(js, 3) + 1
!!$                MilFac(:, 4, js, js1) = (Coor(:, 4) + Coor(:, js) + Coor(:, js1)) / 3.0
!!$                VecFac(:, 4, js, js1) = (VecPt(:, Num(4)) + VecPt(:, Num(js)) + VecPt(:, Num(js1))) / 3.0
!!$             END DO
!!$          CASE DEFAULT
!!$             PRINT *, mod_name, " ERREUR : Type d'element non prevu -> Eval_DualFluxVCOld", Deg
!!$             CALL delegate_stop()
!!$          END SELECT
!!$       CASE DEFAULT
!!$          PRINT *, mod_name, " ERREUR : ndim invalide", Mesh%Ndim
!!$          CALL delegate_stop()
!!$       END SELECT
!!$
!!$       ! Calcul des normales aux cellules duales (ar�tes en 2D
!!$       ! ou facettes en 3D), et de la contribution de l'element jt
!!$       ! au flux dual de chaque sommet
!!$       SELECT CASE(Mesh%Ndim)
!!$       CASE(2) !-- ndim
!!$          DO js = 1, Deg
!!$             js1 = MODULO(js, Deg) + 1
!!$             VnoDc(:, js, js1) = (/ Mesh%CentrEl(2, jt) - MilSeg(2, js, js1), MilSeg(1, js, js1) - Mesh%CentrEl(1, jt) /)
!!$             VnoDc(:, js1, js) = - VnoDc(:, js, js1)
!!$             Vectm(:, js)     = 0.5 * (VecSeg(:, js, js1) + Vec_g(: ))
!!$          END DO
!!$          DO js = 1, Deg
!!$             js1 = MODULO(js, Deg) + 1
!!$             js2 = MODULO(js + Deg -2, Deg) + 1
!!$             DualFlux(Num(js)) = DualFlux(Num(js)) + Sum(Vectm(:, js) * VnoDc(:, js, js1)) &
!!$                  & + Sum(Vectm(:, js2) * VnoDc(:, js, js2))
!!$          END DO
!!$       CASE(3) !-- ndim
!!$          DO js = 1, 3
!!$             js1 = MODULO(js, 3) + 1
!!$             js2 = MODULO(js + 1, 3) + 1
!!$             DO is = 1, 3
!!$                is1 = MODULO(is, 3) + 1
!!$                is2 = MODULO(is + 1, 3) + 1
!!$                VnoDc(is, js, js1) = (MilSeg(is1, js, js1) - MilFac(is1, 4, js, js1)) &
!!$                     & * (Mesh%CentrEl(is2, jt) + MilFac(is2, 1, 2, 3) &
!!$                     & -2.0 * MilFac(is2, 4, js, js1)) - (MilSeg(is2, js, js1) - MilFac(is2, 4, js, js1)) * &
!!$                     & (Mesh%CentrEl(is1, jt) + MilFac(is1, 1, 2, 3) -2.0 * MilFac(is1, 4, js, js1))
!!$                VnoDc(is, js, js1) = (MilSeg(is1, 4, js1) - MilFac(is1, 4, js, js1)) &
!!$                     & * (Mesh%CentrEl(is2, jt) + MilFac(is2, 4, js1, js2) &
!!$                     & -2.0 * MilFac(is2, 4, js, js1)) - (MilSeg(is2, 4, js1) - MilFac(is2, 4, js, js1)) * &
!!$                     & (Mesh%CentrEl(is1, jt) + MilFac(is1, 4, js1, js2) -2.0 * MilFac(is1, 4, js, js1))
!!$             END DO
!!$             VnoDc(:, js1, js) = - VnoDc(:, js, js1)
!!$             VnoDc(:, js1, 4)  = - VnoDc(:, 4, js1)
!!$             VecDc(:, js, js1) = Vec_g(: ) + VecSeg(:, js, js1) + VecFac(:, 1, 2, 3) + VecFac(:, 4, js, js1)
!!$             VecDc(:, js1, js) = VecDc(:, js, js1)
!!$             VecDc(:, 4, js1)  = Vec_g(: ) + VecSeg(:, 4, js1) + VecFac(:, 4, js, js1) + VecFac(:, 4, js1, js2)
!!$             VecDc(:, js1, 4)  = VecDc(:, 4, js1)
!!$          END DO
!!$          VnoDc(:, :, : ) = 0.5 * VnoDc(:, :, : )
!!$          VecDc(:, :, : ) = VecDc(:, :, : ) / 4.0
!!$          DO js = 1, 4
!!$             DO jss = 1, 3
!!$                js1 = MODULO(js + jss -1, 4)
!!$                DualFlux(Num(js)) = DualFlux(Num(js)) + Sum(VnoDc(:, js, js1) * VecDc(:, js, js1))
!!$             END DO
!!$          END DO
!!$       END SELECT
!!$    END DO
!!$
!!$    ! Calculs sur les fronti�res du domaine
!!$    eseen = .FALSE.
!!$    DO ifr = 1, Mesh%NFacFr
!!$       SELECT CASE(Mesh%Ndim)
!!$       CASE(2) !-- ndim
!!$          js   = Mesh%NsFacFr(1, ifr)
!!$          js1  = Mesh%NsFacFr(2, ifr)
!!$          IF (js > 0 .AND. js1 > 0 .AND. js <= SIZE(VecPt, 2) .AND. js1 <= SIZE(VecPt, 2)) THEN !-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!$             Vec_g(: ) = 0.5 * (VecPt(:, js) + VecPt(:, js1)) ! Vec_G n'est ici qu'un interm�diaire
!!$             DualFlux(js)  = DualFlux(js) + 0.25 * Sum(Mesh%VnoFr(:, ifr) * (Vec_g(: ) + VecPt(:, js))) !-- VnoFr est la taille/surface du segment/facette fronti�re
!!$             DualFlux(js1) = DualFlux(js1) + 0.25 * Sum(Mesh%VnoFr(:, ifr) * (Vec_g(: ) + VecPt(:, js1)))
!!$          ELSE
!!$             IF (.NOT. said) THEN
!!$                PRINT *, "ERREUR : ifr==", ifr, "js==", js, js1, SIZE(VecPt, 2)
!!$             END IF
!!$             eseen = .TRUE.
!!$          END IF
!!$       CASE(3) !-- ndim
!!$          PRINT *, mod_name, " ERREUR : NIP ndim==3, TODO" ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!!$          CALL delegate_stop()
!!$       END SELECT
!!$    END DO
!!$    IF (eseen) THEN
!!$       said = .TRUE.
!!$    END IF
!!$
!!$    ! Moyennage
!!$    DO is = 1, Mesh%Npoint
!!$       DualFlux(is) = DualFlux(is) / Mesh%Vols(is)
!!$    END DO
!!$
!!$  END SUBROUTINE Eval_DualFluxVCOld

  FUNCTION pdt_vectoriel(ns, v1, v2) RESULT(Res)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "pdt_vectoriel"
    INTEGER, INTENT(IN) :: ns
    REAL, DIMENSION(: ), INTENT(IN)  :: v1, v2
    REAL, DIMENSION(ns) :: Res !-- On ne peut mettre Res ALLOCTABLE, non standard f90, ne compile pas sur Mirabelle/gfortran

    IF ((SIZE(v1) == 2) .AND. (SIZE(v2) == 2)) THEN
       IF (ns /= 1) THEN
          PRINT *,  mod_name, ' ERREUR : Dimensions incompatibles/1 pour le resultat du produit vectoriel', ns
          CALL delegate_stop()
       END IF
       Res = v1(1) * v2(2) - v1(2) * v2(1)
    ELSE IF ((SIZE(v1) == 3) .AND. (SIZE(v2) == 3)) THEN
       IF (ns /= 3) THEN
          PRINT *,  mod_name, ' ERREUR : Dimensions incompatibles/3 pour le resultat du produit vectoriel', ns
          CALL delegate_stop()
       END IF
       Res(1) = v1(2) * v2(3) - v1(3) * v2(2)
       Res(2) = v1(3) * v2(1) - v1(1) * v2(3)
       Res(3) = v1(1) * v2(2) - v1(2) * v2(1)
    ELSE
       PRINT *,  mod_name, ' ERREUR : Dimensions incompatibles pour le produit vectoriel', SIZE(v1), SIZE(v2)
       CALL delegate_stop()
    END IF

  END FUNCTION pdt_vectoriel

!!$  FUNCTION hamlxf(p, v1, v2, v3, n1, n2, n3, aire, Signe) RESULT(ham)
!!$    !
!!$    !  Hamiltonien de Lax Friedrichs
!!$    !
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "hamlxf"
!!$    REAL, DIMENSION(: ), INTENT(IN) :: p, n1, n2, n3 !-- sans KIND=8, le programme ne peut �tre compil� (sauf -r8)
!!$    REAL, INTENT(IN) :: v1, v2, v3, aire !-- sans KIND=8, le programme ne peut �tre compil� (sauf -r8)
!!$    REAL, INTENT(IN) :: Signe !-- id.
!!$    REAL :: ham
!!$
!!$    REAL :: hh, alpha, hmax
!!$
!!$    hh    = Signe * ( SQRT( p(1) ** 2 + p(2) ** 2 ) - 1.0 )
!!$    hmax  = 1.10 * 6.0
!!$    alpha = MAX( SQRT(n1(1) ** 2 + n1(2) ** 2), &
!!$         & SQRT(n2(1) ** 2 + n2(2) ** 2), &
!!$         & SQRT(n3(1) ** 2 + n3(2) ** 2)) * hmax
!!$    ! inutile de prendre le 3�me en th�orie, mais ca rompt la sym�trie
!!$
!!$    ham = aire * hh + alpha * ( v1 - v2 + v1 - v3)
!!$
!!$  END FUNCTION hamlxf
!!$
!!$  ! --------------------------------------------
!!$  !! Hamiltonien de Godunov  pour ||p||
!!$  ! --------------------------------------------
!!$  FUNCTION ham(Sn2, Sn3, Sp, Signe)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "ham"
!!$    !
!!$    REAL, DIMENSION(: ), INTENT(IN) :: Sp
!!$    REAL, DIMENSION(: ), INTENT(IN) ::  Sn2, Sn3
!!$    REAL, INTENT(IN)  :: Signe
!!$
!!$    REAL              :: ham
!!$    REAL              :: r1, r2
!!$
!!$    IF (Signe > 0 ) THEN
!!$       r1 =  Sp(1) * Sn2(1) + Sp(2) * Sn2(2)
!!$       r2 =  Sp(1) * Sn3(1) + Sp(2) * Sn3(2)
!!$       IF ( r1 < 0.0 .AND. r2 < 0.0 ) THEN
!!$          ham = SQRT( Sp(1) ** 2 + Sp(2) ** 2)
!!$       ELSE
!!$          r1 = ( - Sp(1) * Sn3(2) + Sp(2) * Sn3(1)) / SQRT(Sn3(1) ** 2 + Sn3(2) ** 2)
!!$          r2 = ( - Sp(1) * Sn2(2) + Sp(2) * Sn2(1)) / SQRT(Sn2(1) ** 2 + Sn2(2) ** 2)
!!$          ham = MAX(cte0, r1, - r2)
!!$       END IF
!!$    ELSE
!!$       r1 =  Signe * (Sp(1) * Sn2(1) + Sp(2) * Sn2(2))
!!$       r2 =  Signe * (Sp(1) * Sn3(1) + Sp(2) * Sn3(2))
!!$       IF ( r1 < 0.0 .AND. r2 < 0.0 ) THEN
!!$          ham = SQRT( Sp(1) ** 2 + Sp(2) ** 2)
!!$       ELSE
!!$          r1 = ( - Sp(1) * Sn3(2) + Sp(2) * Sn3(1)) / SQRT(Sn3(1) ** 2 + Sn3(2) ** 2)
!!$          r2 = ( - Sp(1) * Sn2(2) + Sp(2) * Sn2(1)) / SQRT(Sn2(1) ** 2 + Sn2(2) ** 2)
!!$          ham = MAX(cte0, - r1, r2)
!!$       END IF
!!$    END IF
!!$  END FUNCTION ham
!!$
!!$  SUBROUTINE medit2(Mesh, VarPhi, RootName)
!!$    CHARACTER(LEN = *), PARAMETER :: mod_name = "medit2"
!!$    TYPE(MeshDef), INTENT(IN)     :: Mesh
!!$    CHARACTER(LEN = *), INTENT(IN)     ::RootName
!!$    REAL, DIMENSION(: ), INTENT(IN) :: VarPhi
!!$
!!$    INTEGER :: MyUnit, Ns, Nt, Ndim, ntypg, i, is, Jt, k, Nsmplx
!!$    INTEGER :: itypelnu, Nvart, ios
!!$    CHARACTER(LEN = 80) :: PbName, Hdbdesc, Hdbfile
!!$
!!$    PbName = "Final"
!!$    Hdbdesc = " "
!!$    Hdbfile = " "
!!$    MyUnit                         = 97
!!$    Ndim = 2
!!$    ntypg = 1
!!$    Nsmplx = 3
!!$
!!$    Ns = Mesh%Npoint
!!$    Nt = Mesh%Nelemt
!!$    PbName = RootName
!!$    Hdbfile = TRIM(PbName) // ".mesh"
!!$    OPEN( UNIT = MyUnit, FILE = TRIM(Hdbfile), FORM = 'formatted', IOSTAT = ios)
!!$
!!$    IF (ios /= 0) THEN
!!$       PRINT *, mod_name, " ERREUR : impossible d'ouvrir en formatted ", TRIM(Hdbfile), ios
!!$       CALL delegate_stop()
!!$    END IF
!!$    REWIND(MyUnit)
!!$    WRITE (MyUnit, *) "MeshVersionFormatted 1"
!!$    WRITE (MyUnit, *) "Dimension ", Ndim
!!$    WRITE (MyUnit, *) "Vertices ", Ns
!!$    DO is = 1, Ns
!!$       WRITE (MyUnit, 100) (Mesh%Coor(k, is), k = 1, Ndim), ntypg
!!$    END DO
!!$    WRITE (MyUnit, *) "Triangles ", Nt
!!$    DO Jt = 1, Nt
!!$       WRITE (MyUnit, *) (Mesh%Nu(i, Jt), i = 1, Nsmplx), ntypg
!!$    END DO
!!$    WRITE (MyUnit, *) "End "
!!$    CLOSE(MyUnit)
!!$
!!$    Hdbdesc = TRIM(PbName) // ".bb"
!!$    MyUnit                         = 8
!!$    OPEN(UNIT = MyUnit, FORM = 'formatted', FILE = TRIM(Hdbdesc), IOSTAT = ios )
!!$    IF (ios /= 0) THEN
!!$       PRINT *, mod_name, " ERREUR : impossible d'ouvrir en formatted ", TRIM(Hdbdesc), ios
!!$       CALL delegate_stop()
!!$    END IF
!!$    Nvart      = 1
!!$    itypelnu   = 2
!!$    WRITE (MyUnit, *) Ndim, Nvart, Ns, itypelnu
!!$    DO is = 1, Ns
!!$       WRITE (MyUnit, 101)   VarPhi(is)
!!$    END DO
!!$
!!$101 FORMAT( 2(e15.8, 2x))
!!$100 FORMAT( 2(e15.8, 2x), i5)
!!$  END SUBROUTINE medit2

END MODULE utils
