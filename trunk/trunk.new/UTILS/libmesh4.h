//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
//*                                                                           */
//*                  This file is part of the FluidBox program                */
//*                                                                           */
//* Copyright (C) 2000-2008 B.Nkonga                                         */
//*                                                                           */
//*                  2000-2008 Institut de Math�matiques de Bordeaux          */
//*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
//*                                                                           */
//*  FluidBox is distributed under the terms of the Cecill-B License.         */
//*                                                                           */
//*  You can find a copy of the Cecill-B License at :                         */
//*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
//*                                                                           */
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef _LIBMESH_4
#define _LIBMESH_4

/*----------------------------------------------------------*/
/*															*/
/*						LIBMESH V 4.0						*/
/*															*/
/*----------------------------------------------------------*/
/*															*/
/*	Description:		handle .meshb file format I/O		*/
/*	Author:				Loic MARECHAL						*/
/*	Creation date:		aug 02 2003							*/
/*	Last modification:	jan 25 2006							*/
/*															*/
/*----------------------------------------------------------*/

/*
Fait la correspondance entre Fortran et C, sur une base a-priori %%%% int == INTEGER, float == REAL*4
Definit un type �num�r� de 79 constantes, mais rien ne prouve qu'elles collent avec leur nom (%% pb. classique)
%%%% Le 79 n'�tant pas d'ailleurs viss� � cette liste de constante (o� il a donc fallu r�server un certain nb. de places).

Du C � grands d�ports, sans commentaires ou tr�s peu. en particulier sur les variables dans les structures. %%
Une certaine unit� de style entre Fortran et C est souhaitable � l'int�rieur d'un m�me projet %%

typedef struct //-- cette structure contient les infos. relatives � une section de donn�es
{
        // Public
        int SolSiz; //-- nombre de donn�es par ligne (initialis� par ExpFmt)
        int NmbLin; //-- nombre de lignes associ�es � cette entr�e
        int NmbTyp; //-- nombre de donn�es associ�es a cette entr�e
        int TypTab[ GmfMaxTyp ]; // 0:NmbTyp-1, vaut :
                                 // GmfSca    : scalaire (1)
                                 // GmfVec    : vecteur (2)
                                 // GmfSymMat : matrice sym�trique (3)
                                 // GmfMat    : matrice (4)

        // Private
        size_t pos; //-- la position dans la ligne courante pour cette entr�e en cours de lecture
        int CurLin; //-- ligne courante pour cette entr�e
        int typ; //-- vaut :
                 //-- InfKwd (si le 1er format est vide dans le tableau de triplet de chaines sur les mots cl�)
                 //-- SolKwd (si le 2eme format n'est pas "sr")
                 //-- FldKwd (si le 2eme format est "sr"
        char fmt[ GmfMaxTyp ]; //-- format de lecture de chaque type (en fait il y en a SolSiz)
}KwdSct;

typedef struct //-- cette structure contient un maillage GMF
{
        // Public

        int dim; //-- dimension du maillage
        int ver; //-- sans doute la version
        int iter; //-- l'it�ration si le maillage bouge
        float angle; //-- l'angle de vue (?)
        float bbox[3][2]; //-- la bounding box
        float time; //-- temps si le maillage bouge (?)
        KwdSct KwdTab[ GmfMaxKwd + 1 ]; //-- chacune des sections (ou NULL) pr�sente dans le maillage

        // Private

        int mod; //-- mode lecture ou �criture
        int typ; //-- combinaison de 2 des flags suivants
                 // #define Asc 1
                 // #define Bin 2
                 // #define MshFil 4
                 // #define SolFil 8
        int cod; //-- deux codes sont autoris�s : 1 && 16777216, pour diff�rencier BigEndian et LittleEndian
        FILE *hdl; //-- le fichier C qui est ouvert sur le maillage courant
        char FilNam[ GmfStrSiz ]; //-- nom du maillage, taille fixe (%%%%)
}GmfMshSct;
*/

/*----------------------------------------------------------*/
/* Includes													*/
/*----------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


/*----------------------------------------------------------*/
/* Defines													*/
/*----------------------------------------------------------*/

#define GmfStrSiz 1024
#define GmfMaxTyp 20
#define GmfMaxKwd 79
#define GmfMshVer 1
#define GmfRead 1
#define GmfWrite 2
#define GmfSca 1
#define GmfVec 2
#define GmfSymMat 3
#define GmfMat 4

enum GmfKwdCod
{
	GmfReserved1, \
	GmfVersionFormatted, \
	GmfReserved2, \
	GmfDimension, \
	GmfVertices, \
	GmfEdges, \
	GmfTriangles, \
	GmfQuadrilaterals, \
	GmfTetrahedra, \
	GmfPentahedra, \
	GmfHexahedra, \
	GmfReserved3, \
	GmfReserved4, \
	GmfCorners, \
	GmfRidges, \
	GmfRequiredVertices, \
	GmfRequiredEdges, \
	GmfRequiredTriangles, \
	GmfRequiredQuadrilaterals, \
	GmfTangentAtEdgeVertices, \
	GmfNormalAtVertices, \
	GmfNormalAtTriangleVertices, \
	GmfNormalAtQuadrilateralVertices, \
	GmfAngleOfCornerBound, \
	GmfReserved5, \
	GmfReserved6, \
	GmfReserved7, \
	GmfReserved8, \
	GmfReserved9, \
	GmfReserved10, \
	GmfReserved11, \
	GmfReserved12, \
	GmfReserved13, \
	GmfReserved14, \
	GmfReserved15, \
	GmfReserved16, \
	GmfReserved17, \
	GmfReserved18, \
	GmfReserved19, \
	GmfReserved20, \
	GmfReserved21, \
	GmfReserved22, \
	GmfReserved23, \
	GmfReserved24, \
	GmfReserved25, \
	GmfReserved26, \
	GmfReserved27, \
	GmfReserved28, \
	GmfReserved29, \
	GmfReserved30, \
	GmfBoundingBox, \
	GmfReserved31, \
	GmfReserved32, \
	GmfReserved33, \
	GmfEnd, \
	GmfReserved34, \
	GmfReserved35, \
	GmfReserved36, \
	GmfReserved37, \
	GmfTangents, \
	GmfNormals, \
	GmfTangentAtVertices, \
	GmfSolAtVertices, \
	GmfSolAtEdges, \
	GmfSolAtTriangles, \
	GmfSolAtQuadrilaterals, \
	GmfSolAtTetrahedra, \
	GmfSolAtPentahedra, \
	GmfSolAtHexahedra, \
	GmfDSolAtVertices, \
	GmfISolAtVertices, \
	GmfISolAtEdges, \
	GmfISolAtTriangles, \
	GmfISolAtQuadrilaterals, \
	GmfISolAtTetrahedra, \
	GmfISolAtPentahedra, \
	GmfISolAtHexahedra, \
	GmfIterations, \
	GmfTime, \
	GmfVertexHack
};

/********************************************************************/
/*  SI VOTRE COMPILATEUR FORTRAN PREFERE GENERE UN _ APRES          */
/*  LES NOMS DES SUBROUTINES, ALORS PRENEZ CETTE DEFINITION DE proc */
/* #define call(x)  name2(x,_)                                      */
/*  SINON PRENEZ CELLE CI                                           */
/* #define call(x)  x                                               */
/********************************************************************/

#ifndef _UNDERSCORE_H_
#define _UNDERSCORE_H_

#  if defined(_NOUVELLE_FACON_)

#      define chooser(x) x
#      define chooser_(x) x ## _
#      define _chooser(x) _ ## x
#      define _chooser_(x) _ ## x ## _

#      define call(x) _NOUVELLE_FACON_(x)

#  else //-- ancienne fa�on

#  if defined(__ANSI_CPP__) || defined(__linux__) || defined(_HPUX_SOURCE) || defined(__macos__) || defined (__APPLE__)
#    define name2(a,b) a ## b
#  else
#    ifdef BSD  /* BSD way: ok pour dn10000 */
#      define name2(a,b) a\
b

#    else /* System V way: */
#      define name2(a,b) a/**/b
#    endif 
#  endif

#  ifdef F77_NO_UNDER_SCORE 
#    define call(x) x
#  else
#    if defined(__linux__)  || defined(__macos__) || defined (__APPLE__)
#      define call(x) name2(x,_)
#    else
#      define call(x) x
#    endif
#  endif

#  endif //-- ancienne fa�on

#endif // _UNDERSCORE_H_


/*----------------------------------------------------------*/
/* Structures												*/
/*----------------------------------------------------------*/

typedef struct
{
	/* Public */
	int SolSiz, NmbLin, NmbTyp, TypTab[ GmfMaxTyp ];

	/* Private */
	size_t pos;
	int CurLin, typ;
	char fmt[ GmfMaxTyp ];
}KwdSct;

typedef struct
{
	/* Public */

	int dim, ver, iter;
	float angle, bbox[3][2], time;
	KwdSct KwdTab[ GmfMaxKwd + 1 ];

	/* Private */

	int mod, typ, cod;
	FILE *hdl;
	char FilNam[ GmfStrSiz ];
}GmfMshSct;


/*----------------------------------------------------------*/
/* External procedures										*/
/*----------------------------------------------------------*/

extern GmfMshSct *GmfOpenMesh(char *, int, ...);
extern int GmfCloseMesh(GmfMshSct *);
extern int GmfReadField(GmfMshSct *, int, float *, int *);
extern int GmfWriteField(GmfMshSct *, int, int, float *, int *, ...);
extern int GmfReadLine(GmfMshSct *, int, ...);
extern int GmfWriteLine(GmfMshSct *, int, ...);

/* Fortran 77 API */

int call(gmfopenmeshf77)(char *, int *, int *, int [ GmfMaxKwd ][ GmfMaxTyp + 2 ], int);
int call(gmfclosemeshf77)(int *);
int call(gmfreadfieldf77)(int *, int *, float *, int *);
int call(gmfwritefieldf77)(int *, int *, int *, int *, int *, float *, int *);

#endif
