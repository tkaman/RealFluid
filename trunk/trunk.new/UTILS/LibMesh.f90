!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

! --------------------------------------------------------------------------------
! --------------------------------------------------------------------------------
! --------------------------------------------------------------------------------

   MODULE LibMESh

      USE LesTypes
      USE LibComm

      IMPLICIT NONE

      INTERFACE loadSol
         MODULE PROCEDURE  loadSolMat, loadSolMatDbl
      END INTERFACE

      INTERFACE writeSol
         MODULE PROCEDURE writeSolMat, writeSolMatDbl, writeSolVec, writeSolVecDbl
      END INTERFACE

      INTERFACE writeSolTria
         MODULE PROCEDURE writeSolMatTria, writeSolMatDblTRIa
      END INTERFACE

      INTEGER :: verbose

      TYPE t_io

         INTEGER :: verbose = 0
         INTEGER :: depth = 0
         INTEGER :: mem_alloc = 0
         REAL, DIMENSION(4)  :: cpu
         REAL                :: cpul
         CHARACTER(LEN = 512)  :: subr
         REAL, DIMENSION(50) :: cputime

      END TYPE t_io

      TYPE(t_io), SAVE :: out

      INTEGER, PARAMETER :: GmfRead = 1
      INTEGER, PARAMETER :: GmfSca = 1
      INTEGER, PARAMETER :: GmfVec = 2
      INTEGER, PARAMETER :: GmfWrite = 2
      INTEGER, PARAMETER :: GmfSymMat = 3
      INTEGER, PARAMETER :: GmfMat = 4
      INTEGER, PARAMETER :: GmfVertices = 4
      INTEGER, PARAMETER :: GmfEdges = 5
      INTEGER, PARAMETER :: GmfTriangles = 6
      INTEGER, PARAMETER :: GmfQuadrilaterals = 7
      INTEGER, PARAMETER :: GmfTetrahedra = 8
      INTEGER, PARAMETER :: GmfPentahedra = 9
      INTEGER, PARAMETER :: GmfHexahedra = 10
      INTEGER, PARAMETER :: GmfCorners = 13
      INTEGER, PARAMETER :: GmfRidges = 14
      INTEGER, PARAMETER :: GmfRequiredVertices = 15
      INTEGER, PARAMETER :: GmfRequiredEdges = 16
      INTEGER, PARAMETER :: GmfRequiredTriangles = 17
      INTEGER, PARAMETER :: GmfRequiredQuadrilaterals = 18
      INTEGER, PARAMETER :: GmfTangentAtEdgeVertices = 19
      INTEGER, PARAMETER :: GmfMaxSol = 20
      INTEGER, PARAMETER :: GmfNormalAtVertices = 20
      INTEGER, PARAMETER :: GmfNormalAtTriangleVertices = 21
      INTEGER, PARAMETER :: GmfNormalAtQuadrilateralVertices = 22
      INTEGER, PARAMETER :: GmfTangents = 59
      INTEGER, PARAMETER :: GmfNormals = 60
      INTEGER, PARAMETER :: GmfTangentAtVertices = 61
      INTEGER, PARAMETER :: GmfSolAtVertices = 62
      INTEGER, PARAMETER :: GmfSolAtEdges = 63
      INTEGER, PARAMETER :: GmfSolAtTriangles = 64
      INTEGER, PARAMETER :: GmfSolAtQuadrilaterals = 65
      INTEGER, PARAMETER :: GmfSolAtTetrahedra = 66
      INTEGER, PARAMETER :: GmfSolAtPentahedra = 67
      INTEGER, PARAMETER :: GmfSolAtHexahedra = 68
      INTEGER, PARAMETER :: GmfDSolAtVertices = 69
      INTEGER, PARAMETER :: GmfISolAtVertices = 70
      INTEGER, PARAMETER :: GmfISolAtEdges = 71
      INTEGER, PARAMETER :: GmfISolAtTriangles = 72
      INTEGER, PARAMETER :: GmfISolAtQuadrilaterals = 73
      INTEGER, PARAMETER :: GmfISolAtTetrahedra = 74
      INTEGER, PARAMETER :: GmfISolAtPentahedra = 75
      INTEGER, PARAMETER :: GmfISolAtHexahedra = 76
      INTEGER, PARAMETER :: GmfIterations = 77
      INTEGER, PARAMETER :: GmfTime = 78
      INTEGER, PARAMETER :: GmfMaxKwd = 79

      EXTERNAL GmfOpenMeshF77, GmfCloseMeshF77, GmfReadFieldF77&  !-- BIND(C,NAME="gmfopenmeshf77")
 ,           GmfWriteFieldF77
      INTEGER :: GmfOpenMeshF77, GmfCloseMeshF77, GmfReadFieldF77&
 ,           GmfWriteFieldF77



   CONTAINS



!!-- teste l'existence d'un fichier *.mesh ou *.meshb,
!!-- et l'ouvre avec GMFOPENMESHF77 (lit tout le maillage dans une structure interne, variable globale cach�e,
!!-- que l'on peut ensuite consulter par les autres appels a GMF*)
!!-- S'il peut l'ouvrir, affiche un message OPENED et retourne l'information sur le nombre de triangles..pentagones
      SUBROUTINE getMeshInfo(MshNam, Nfic, dim, KwdTab)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "getMeshInfo"

         CHARACTER(LEN = *), INTENT(IN)    :: MshNam
         INTEGER, INTENT(OUT)            :: Nfic
         INTEGER, INTENT(IN)             :: dim
         INTEGER, DIMENSION(:, : ), INTENT(OUT) :: KwdTab

    !-- dec local
         INTEGER :: ind, Len
         LOGICAL :: mesh, meshb
         CHARACTER(LEN = 1024) :: fulNam
         INTEGER :: lnam

    !-- end dec loc

         IF (Size(KwdTab, 1) /= GmfMaxSol + 2 .OR. Size(KwdTab, 2) /= GmfMaxKwd) THEN
            PRINT *, mod_name, " ERREUR : size(KwdTab)==", Size(KwdTab, 1), Size(KwdTab, 2), GmfMaxSol, GmfMaxKwd
            CALL delegate_stop()
         END IF

         Len = Len_trim(MshNam)
         ind = Index( MshNam(1: Len), '.mesh')

         IF (ind <= 0) THEN
            INQUIRE(FILE = MshNam(1: Len) // '.mesh', EXIST = mesh)
            INQUIRE(FILE = MshNam(1: Len) // '.meshb', EXIST = meshb)

            IF (meshb) THEN
               fulNam = MshNam(1: Len) // '.meshb'
               lnam = Len + 6
            ELSE IF (mesh) THEN
               fulNam = MshNam(1: Len) // '.mesh'
               lnam =  Len + 5
            ELSE
               lnam = Len
               PRINT *, mod_name, ' ERREUR : CANNOT OPEN MESH  FILE', MshNam(1: lnam), '.mesh[b]'
               CALL stop_run()
            END IF

         ELSE

            fulNam = MshNam(1: Len)
            lnam   = Len

         END IF

         Nfic = GmfOpenMeshF77( fulNam(1: lnam), GmfRead, dim, KwdTab)

         IF (Nfic <= 0) THEN
            PRINT *, mod_name, ' ERREUR : (2) CANNOT OPEN MESH  FILE', Trim( fulNam(1: lnam))
            CALL stop_run()
         END IF

         PRINT *, mod_name, '  %', fulNam(1: lnam), ' OPENED'


      END SUBROUTINE getMeshInfo


!!-- lit des maillages compos�s de triangles .. pentagones, avec gmf* (C)
      SUBROUTINE loadMesh(Nfic, Ver, VerRef, Tri, Quad, Tet, Hexa, Edg)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "loadMesh"

         REAL, DIMENSION(:, : ), INTENT(OUT) :: Ver
         INTEGER, INTENT(IN)         :: Nfic
         INTEGER, DIMENSION(: ), INTENT(INOUT) :: VerRef

         INTEGER, DIMENSION(:, : ),    OPTIONAL, INTENT(INOUT)  :: Tri, Quad
         INTEGER, DIMENSION(:, : ),    OPTIONAL, INTENT(INOUT)  :: Tet, Hexa
         INTEGER, DIMENSION(:, : ),    OPTIONAL, INTENT(INOUT)  :: Edg

    !-- dec local
         INTEGER :: res
         INTEGER :: NbrVer, NbrTri, NbrQuad, NbrTet, NbrHexa, NbrEdg

         REAL, DIMENSION(Size(Ver, 1), Size( Ver, 2))  :: Vers

    !-- end dec loc

         NbrVer = Size(Ver, 2)
         res = GmfReadFieldF77(Nfic, GmfVertices, Vers, VerRef)
         Ver = Vers
         IF (res /= NbrVer) THEN
            PRINT *, mod_name, ' : ERREUR : Number of read Vertices', res, '/=', NbrVer
            CALL stop_run()
         END IF

         IF (Present(Tri)) THEN
            NbrTri = Size(Tri, 2)
            res = GmfReadFieldF77(Nfic, GmfTriangles, 0, Tri)
            IF (res /= NbrTri) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Triangles', res, '/=', NbrTri
               CALL stop_run()
            END IF
         END IF

         IF (Present(Quad)) THEN
            NbrQuad = Size(Quad, 2)
            res = GmfReadFieldF77(Nfic, GmfQuadrilaterals, 0, Quad)
            IF (res /= NbrQuad) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Quadrilaterals', res, '/=', NbrQuad
               CALL stop_run()
            END IF
         END IF

         IF (Present(Tet)) THEN
            NbrTet = Size(Tet, 2)
            res = GmfReadFieldF77(Nfic, GmfTetrahedra, 0, Tet)
            IF (res /= NbrTet) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Tetrahedra', res, '/=', NbrTet
               CALL stop_run()
            END IF
         END IF
         IF (Present(Hexa)) THEN
            NbrHexa = Size(Hexa, 2)
            res = GmfReadFieldF77(Nfic, GmfHexahedra, 0, Hexa)
            IF (res /= NbrHexa) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Hexahedra', res, '/=', NbrHexa
               CALL stop_run()
            END IF
         END IF

         IF (Present(Edg)) THEN
            NbrEdg = Size(Edg, 2)
            res = GmfReadFieldF77(Nfic, GmfEdges, 0, Edg)
            IF (res /= NbrEdg) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Boundary Edges', res, '/=', NbrEdg
               CALL stop_run()
            END IF
         END IF


         res = GmfCloseMeshF77(Nfic)


      END SUBROUTINE loadMesh


!!-- Ecrit un maillage via gmf*
      SUBROUTINE WriteMesh(MshNam, dim, Ver, VerRef, Tri, TriRef, Quad, QuadRef, Tet, TetRef, Edg, EdgRef)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "WriteMesh"

         CHARACTER(LEN = *), INTENT(IN)     :: MshNam
         REAL, DIMENSION(:, : ), INTENT(IN) :: Ver
         INTEGER, INTENT(IN)              :: dim
         INTEGER, DIMENSION(: ), INTENT(IN) :: VerRef
         INTEGER, DIMENSION(:, : ), INTENT(IN), OPTIONAL  :: Tri, Quad, Tet, Edg
         INTEGER, DIMENSION(: ), INTENT(IN),     OPTIONAL  :: TriRef, QuadRef, TetRef, EdgRef

         INTEGER, DIMENSION(:, : ),  POINTER     :: Itab

    !-- dec local
         INTEGER :: res,  Nfic, ii, jj, istat
         INTEGER :: NbrVer, NbrTri, NbrQuad, NbrTet, NbrEdg

         REAL, DIMENSION(:, : ), POINTER  :: Vers

    !-- end dec loc

         ALLOCATE(Vers(Size(Ver, 1), Size( Ver, 2))) !-- trop gros pour �tre allou� sur le stack par ifort en standard
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "vers", Size(Vers))
         END IF
         Vers = Ver
         Nfic = GmfOpenMeshF77( Trim(MshNam), GmfWrite, dim, 0)
         IF (Nfic == 0) THEN
            PRINT *, mod_name, " : ERREUR : Impossible d'ouvrir le fichier C ", Trim( MshNam)
            CALL stop_run()
         END IF
         NbrVer = Size(VerRef)

         res = GmfWriteFieldF77(Nfic, GmfVertices, NbrVer, 0, 0, Vers, VerRef)

         IF (res /= NbrVer) THEN
            PRINT *, mod_name, ' : ERREUR : Number of read Vertices', res, '/=', NbrVer
            CALL stop_run()
         END IF

         IF (Present(Tri)) THEN
            NbrTri = Size(Tri, 2)
            IF (Present(TriRef) ) THEN
               ALLOCATE( Itab(Size(Tri, 1) + 1, NbrTri) )
               IF (see_alloc) THEN
                  CALL alloc_allocate(mod_name, "itab", Size(Itab))
               END IF
               Itab(1: Size(Tri, 1), 1: NbrTri) = Tri
               Itab(1 + Size(Tri, 1), 1: NbrTri) = TriRef
               res = GmfWriteFieldF77(Nfic, GmfTriangles, NbrTri, 0, 0, 0, Itab)
               DEALLOCATE(Itab)
               IF (see_alloc) THEN
                  CALL alloc_deallocate(mod_name, "itab")
               END IF
            ELSE
               res = GmfWriteFieldF77(Nfic, GmfTriangles, NbrTri, 0, 0, 0, Tri)
            END IF
            IF (res /= NbrTri) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Triangles', res, '/=', NbrTri
               CALL stop_run()
            END IF
         END IF

         IF (Present(Quad)) THEN
            NbrQuad = Size(Quad, 2)
            IF (Present(QuadRef) ) THEN
               ALLOCATE( Itab(Size(Quad, 1) + 1, NbrQuad) )
               IF (see_alloc) THEN
                  CALL alloc_allocate(mod_name, "itab", Size(Itab))
               END IF
               Itab(1: Size(Quad, 1), 1: NbrQuad) = Quad
               Itab(1 + Size(Quad, 1), 1: NbrQuad) = QuadRef
               res = GmfWriteFieldF77(Nfic, GmfQuadrilaterals, NbrQuad, 0, 0, 0, Itab)
               DEALLOCATE(Itab)
               IF (see_alloc) THEN
                  CALL alloc_deallocate(mod_name, "itab")
               END IF
            ELSE
               res = GmfWriteFieldF77(Nfic, GmfQuadrilaterals, NbrQuad, 0, 0, 0, Quad)
            END IF
            IF (res /= NbrQuad) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Quadrilaterals', res, '/=', NbrQuad
               CALL stop_run()
            END IF
         END IF

         IF (Present(Tet)) THEN
            NbrTet = Size(Tet, 2)
            IF (Present(TetRef) ) THEN
               ALLOCATE( Itab(Size(Tet, 1) + 1, NbrTet) )
               IF (see_alloc) THEN
                  CALL alloc_allocate(mod_name, "itab", Size(Itab))
               END IF
               Itab(1: Size(Tet, 1), 1: NbrTet) = Tet
               Itab(1 + Size(Tet, 1), 1: NbrTet) = TetRef
               IF (Size(Tet, 1) == 4 ) THEN
                  res = GmfWriteFieldF77(Nfic, GmfTetrahedra, NbrTet, 0, 0, 0, Itab)
               END IF
               IF (Size(Tet, 1) == 8 ) THEN
                  res = GmfWriteFieldF77(Nfic, GmfHexahedra, NbrTet, 0, 0, 0, Itab)
               END IF
               DEALLOCATE(Itab)
               IF (see_alloc) THEN
                  CALL alloc_deallocate(mod_name, "itab")
               END IF
            ELSE
               IF (Size(Tet, 1) == 4 ) THEN
                  res = GmfWriteFieldF77(Nfic, GmfTetrahedra, NbrTet, 0, 0, 0, Tet)
               END IF
               IF (Size(Tet, 1) == 8 ) THEN
                  res = GmfWriteFieldF77(Nfic, GmfHexahedra, NbrTet, 0, 0, 0, Tet)
               END IF
            END IF
            IF (res /= NbrTet) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Tetrahedra', res, '/=', NbrTet
               CALL stop_run()
            END IF
         END IF

         IF (Present(Edg)) THEN
            NbrEdg = Size(Edg, 2)
            IF (Present(EdgRef) ) THEN
               ALLOCATE( Itab(Size(Edg, 1) + 1, NbrEdg), STAT = istat )
               IF (istat /= 0) THEN
                  PRINT *, mod_name, ' : ERREUR : Cannot allocate Itab', Size(Edg, 1), Size(Edg, 2)
                  CALL stop_run()
               END IF
               IF (see_alloc) THEN
                  CALL alloc_allocate(mod_name, "itab", Size(Itab))
               END IF
               PRINT *, mod_name, " NbrEdg==", NbrEdg, Size(Edg, 1)
               IF (.FALSE.) THEN
                  Itab(1: Size(Edg, 1), 1: NbrEdg) = Edg !-- en principe ce sont des tableaux de m�me taille
               ELSE
                  DO jj = 1, NbrEdg
                     DO ii = 1, Size(Edg, 1)
!                        PRINT *, "II,...=", ii, jj, Size(Edg, 1), Size(Edg, 2), Size(Itab, 1), Size(Itab, 2)
!                        PRINT *, "Edg", Edg(ii, jj)
!                        PRINT *, "Itab", Itab(ii, jj)
                        Itab(ii, jj) = Edg(ii, jj) !-- en principe ce sont des tableaux de m�me taille
                     END DO
                  END DO
                  PRINT *, mod_name, " Done"
               END IF
               Itab(1 + Size(Edg, 1), 1: NbrEdg) = EdgRef
               res = GmfWriteFieldF77(Nfic, GmfEdges, NbrEdg, 0, 0, 0, Itab)
               DEALLOCATE(Itab)
               IF (see_alloc) THEN
                  CALL alloc_deallocate(mod_name, "itab")
               END IF
            ELSE
               res = GmfWriteFieldF77(Nfic, GmfEdges, NbrEdg, 0, 0, 0, Edg)

            END IF
            IF (res /= NbrEdg) THEN
               PRINT *, mod_name, ' : ERREUR : Number of read Bondary Edges', res, '/=', NbrEdg
               CALL stop_run()
            END IF
         END IF

         res = GmfCloseMeshF77(Nfic)

         DEALLOCATE(Vers)
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "vers")
         END IF

      END SUBROUTINE WriteMesh


!!-- teste l'existence d'un fichier *.sol ou *.solb,
!!-- et l'ouvre avec GMFOPENMESHF77
!!-- S'il peut l'ouvrir, affiche un message OPENED
      SUBROUTINE getSolInfo(SolNam, Nfic, dim, KwdTab)

         CHARACTER(LEN = *), PARAMETER :: mod_name = "LibMesh/getSolInfo"

         CHARACTER(LEN = *), INTENT(IN)               :: SolNam
         INTEGER, INTENT(OUT)                       :: Nfic
         INTEGER, INTENT(OUT)                       :: dim
         INTEGER, DIMENSION(:, : ), INTENT(OUT) :: KwdTab


    !-- loc
         INTEGER                                  :: Len
         INTEGER :: ind
         LOGICAL :: sol, solb

         CHARACTER(LEN = 1024) :: fulNam
         INTEGER :: lnam, ii

         IF (Size(KwdTab, 1) /= GmfMaxSol + 2 .OR. Size(KwdTab, 2) /= GmfMaxKwd) THEN
            PRINT *, mod_name, " ERREUR : size(KwdTab)==", Size(KwdTab, 1), Size(KwdTab, 2), GmfMaxSol, GmfMaxKwd
            CALL delegate_stop()
         END IF

         Len = Len_trim(SolNam)


         ind = Index( SolNam(1: Len), '.sol')

         IF (ind <= 0) THEN
            INQUIRE(FILE = SolNam(1: Len) // '.sol', EXIST = sol)
            INQUIRE(FILE = SolNam(1: Len) // '.solb', EXIST = solb)

            IF (solb) THEN
               fulNam = SolNam(1: Len) // '.solb'
               lnam = Len + 5
            ELSE IF (sol) THEN
               fulNam = SolNam(1: Len) // '.sol'
               lnam =  Len + 4
            ELSE
               lnam = 0
               PRINT *, mod_name, " :: ERREUR : cas non prevu : ni sol, ni solb `" // SolNam(1: Len) // "'", Len
               CALL stop_run()
            END IF

         ELSE

            fulNam = SolNam(1: Len)
            lnam   = Len

         END IF


         Nfic = GmfOpenMeshF77( fulNam(1: lnam), GmfRead, dim, KwdTab)

         IF (.FALSE.) THEN
            DO ii = 1, GmfMaxKwd
               PRINT *, mod_name, " KwdTab==", ii, "->", KwdTab(1, ii)
            END DO
         END IF


         IF (Nfic <= 0) THEN
            PRINT *, mod_name, " :: ERREUR : NO SOLUTION FILE FOUND ", Trim(fulNam)
            CALL stop_run()
         END IF

         PRINT *, mod_name, ' %', fulNam(1: lnam), ' OPENED'



      END SUBROUTINE getSolInfo



      SUBROUTINE loadSolMat(Nfic, SolMat, Time, Iter)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "loadSolMat"


         REAL, DIMENSION(:, : ), INTENT(OUT)                 :: SolMat
         REAL, OPTIONAL, INTENT(OUT)    :: Time
         INTEGER, OPTIONAL, INTENT(OUT) :: Iter
         INTEGER, INTENT(IN) :: Nfic

    !-- loc
         INTEGER :: res
         INTEGER :: NbrSol, SizTot

    !-- end dec loc


         NbrSol  = Size(SolMat, 2)
         SizTot  = Size(SolMat, 1)

         res = GmfReadFieldF77(Nfic, GmfSolAtVertices, SolMat, 0)
         IF (res /= NbrSol) THEN
            PRINT *, mod_name, ' : ERREUR : NUMBER OF READ SOLUTIONS', res, '/=', NbrSol
         END IF


         IF (Present(Time) ) THEN
            res = GmfReadFieldF77(Nfic, GmfTime, Time, 0)
         END IF

         IF (Present(Iter) ) THEN
            res = GmfReadFieldF77(Nfic, GmfIterations, 0, Iter)
         END IF

         res = GmfCloseMeshF77(Nfic)

      END SUBROUTINE loadSolMat


!!-- Lit, au format GMF, dans un fichier .sol/.solb donn� par l'unit� Nfic, la solution pour le temps Time (option.) et/ou l'iter Iter (option.),
!!-- dans la matrice SolMat
      SUBROUTINE loadSolMatDbl(Nfic, SolMat, Time, Iter)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "loadSolMatDbl"


         REAL, DIMENSION(:, : ), INTENT(INOUT)    :: SolMat
         REAL, OPTIONAL, INTENT(OUT)    :: Time
         INTEGER, OPTIONAL, INTENT(OUT) :: Iter
         INTEGER, INTENT(IN) :: Nfic

    !-- loc
         INTEGER :: res
         INTEGER :: NbrSol, SizTot
         REAL,  DIMENSION(:, : ),  ALLOCATABLE :: SolMats !-- [KADNA-PROTECTED]
         REAL   :: Times !-- [KADNA-PROTECTED]

         ALLOCATE(SolMats(Size(SolMat, 1), Size( SolMat, 2)))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "solmats", Size(SolMats))
         END IF

    !-- end dec loc


         NbrSol  = Size(SolMat, 2)
         SizTot  = Size(SolMat, 1)

         res = GmfReadFieldF77(Nfic, GmfSolAtVertices, SolMats, 0)
         IF (res /= NbrSol) THEN
            PRINT *, mod_name, ' : ERREUR : NUMBER OF READ SOLUTIONS', res, '/=', NbrSol
         END IF

         PRINT *, Size(Solmat, 1), Size(Solmat, 2)
         PRINT *, SolMat(1, 1)
         PRINT *, Size(Solmats, 1), Size(Solmats, 2)
         PRINT *, SolMats(1, 1)

         SolMat = SolMats
         IF (Present(Time) ) THEN
            res = GmfReadFieldF77(Nfic, GmfTime, Times, 0)
            Time = Times
         END IF

         IF (Present(Iter) ) THEN
            res = GmfReadFieldF77(Nfic, GmfIterations, 0, Iter)
         END IF

         res = GmfCloseMeshF77(Nfic)



      END SUBROUTINE loadSolMatDbl




!!-- Ecriture du maillage au format GMF
      SUBROUTINE writeSolMat(SolNam, dim, NbrFld, SizFld, SolMat, Err, Iter, Time)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "writeSolMat"

         CHARACTER(LEN = *), INTENT(IN)          :: SolNam
         REAL, DIMENSION(:, : ), INTENT(IN)    :: SolMat !-- [KADNA-PROTECTED]
         INTEGER, INTENT(IN)                   :: NbrFld
         INTEGER, DIMENSION(: ), INTENT(INOUT) :: SizFld
         INTEGER, OPTIONAL, INTENT(OUT)        :: Err
         INTEGER, INTENT(IN)                   :: dim
         REAL, OPTIONAL, INTENT(IN)          :: Time
         INTEGER, OPTIONAL, INTENT(IN)         :: Iter

         INTEGER                            :: NbrSol
         INTEGER :: Nfic, i,  res
         INTEGER :: Len
         INTEGER :: zero, un
         REAL           :: copy_Time !-- [KADNA-PROTECTED]

         zero = 0
         un   = 1

         IF ( Present(Err) ) THEN
            Err = 0
         END IF

         Len = Len_trim(SolNam)

         Nfic = GmfOpenMeshF77( SolNam(1: Len), GmfWrite, dim, 0)

         IF (Nfic <= 0) THEN
            PRINT *, mod_name, ' ** CANNOT CREATE SOLUTION FILE'
            IF ( Present(Err) ) THEN
               Err = -1
            END IF
            RETURN
         END IF

         PRINT *, mod_name, '  %', SolNam(1: Len), ' CREATED'



         DO i = 1, NbrFld
            IF (SizFld(i) == 1) THEN  !scalar
               SizFld(i) = GmfSca
            ELSE IF (SizFld(i) == dim) THEN ! Vector
               SizFld(i) = GmfVec
            ELSE IF (SizFld(i) == dim * (dim + 1) / 2 ) THEN ! Sym SolMat
               SizFld(i) = GmfSymMat
            ELSE IF ( SizFld(i) == dim * dim   ) THEN ! SolMat (pleine)
               SizFld(i) = GmfMat
            ELSE
               PRINT *, mod_name, ' : ERREUR : UNVALID SOLUTION TYPE', SizFld(i)
               PRINT *, '    MUST BE Scalar(1), Vector(2), Tensor(3), Matrix(4)'
               CALL stop_run()
            END IF

         END DO

         NbrSol = Size(SolMat, 2)
         res = GmfWriteFieldF77(Nfic, GmfSolAtVertices, NbrSol, NbrFld, SizFld, SolMat, 0)
         IF (res /= NbrSol) THEN
            PRINT *, mod_name, ' : ERREUR : NUMBER OF WRITTEN SOLUTIONS', res, '/=', NbrSol
            IF ( Present(Err) ) THEN
               Err = -2
            END IF
         END IF


         IF (Present(Time) ) THEN
            copy_Time = Time
            res = GmfWriteFieldF77(Nfic, GmfTime, 1, 0, 0, copy_Time, 0)
         END IF

         IF (Present(Iter) ) THEN
            res = GmfWriteFieldF77(Nfic, GmfIterations, 1, 0, 0, 0, Iter)
         END IF


         res = GmfCloseMeshF77(Nfic)


      END SUBROUTINE writeSolMat


!!-- Interface pour passer de REAL(KIND=8) � REAL(KIND=4) (seul format accept� par gmf) %%; appelle writeSolMat
      SUBROUTINE writeSolMatDbl(SolNam, dim, NbrFld, SizFld, SolMat, Err, Iter, Time)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "writeSolMatDbl"

         CHARACTER(LEN = *), INTENT(IN)          :: SolNam
         REAL, DIMENSION(:, : ), INTENT(IN)    :: SolMat
         INTEGER, INTENT(IN)                   :: NbrFld
         INTEGER, DIMENSION(: ), INTENT(INOUT) :: SizFld
         INTEGER, OPTIONAL, INTENT(INOUT)     :: Err !-- %% no use
         INTEGER, INTENT(IN)                   :: dim
         REAL, OPTIONAL, INTENT(IN)          :: Time
         INTEGER, OPTIONAL, INTENT(IN)         :: Iter

         REAL,  DIMENSION(:, : ),  ALLOCATABLE :: temp !-- [KADNA-PROTECTED]
         INTEGER    :: Iters, errs
         REAL    :: Times

         ALLOCATE(temp(Size(SolMat, 1), Size( SolMat, 2)))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "temp", Size(temp))
         END IF

         Times = 0.0
         IF ( Present(Time) ) THEN
            Times = Time
         END IF
         Iters = 0
         IF ( Present(Iter) ) THEN
            Iters = Iter
         END IF

         temp  = SolMat
         CALL writeSolMat(SolNam, dim, NbrFld, SizFld, temp, errs, Iters, Times)

         DEALLOCATE(temp)
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "temp")
         END IF

         IF (.FALSE.) THEN ! Pour suppression de warning (pas top)
            Err = 1
         END IF
      END SUBROUTINE writeSolMatDbl

      SUBROUTINE writeSolMatTria(SolNam, dim, NbrFld, SizFld, SolMat, Err, Iter, Time, sel_approche)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "writeSolMatTria"

         CHARACTER(LEN = *), INTENT(IN)          :: SolNam
         REAL, DIMENSION(:, : ), INTENT(IN)    :: SolMat !-- [KADNA-PROTECTED]
         INTEGER, INTENT(IN)                   :: NbrFld
         INTEGER, DIMENSION(: ), INTENT(INOUT) :: SizFld
         INTEGER, OPTIONAL, INTENT(OUT)        :: Err
         INTEGER, INTENT(IN)                   :: dim
         REAL, OPTIONAL, INTENT(IN)          :: Time !-- [KADNA-PROTECTED]
         INTEGER, OPTIONAL, INTENT(IN)         :: Iter
         INTEGER, OPTIONAL, INTENT(IN)         :: sel_approche

         INTEGER                            :: NbrSol
         INTEGER :: Nfic, i, res
         INTEGER :: Len
         INTEGER :: zero, un
         REAL :: copy_Time !-- [KADNA-PROTECTED]

         zero = 0
         un   = 1

         IF ( Present(Err) ) THEN
            Err = 0
         END IF

         Len = Len_trim(SolNam)

         Nfic = GmfOpenMeshF77( SolNam(1: Len), GmfWrite, dim, 0)

         IF (Nfic <= 0) THEN
            PRINT *, mod_name, ' ** CANNOT CREATE SOLUTION FILE'
            IF ( Present(Err) ) THEN
               Err = -1
            END IF
            RETURN
         END IF

         PRINT *, mod_name, ' %', SolNam(1: Len), ' CREATED'

         DO i = 1, NbrFld
            IF (SizFld(i) == 1) THEN  !scalar
               SizFld(i) = GmfSca
            ELSE IF (SizFld(i) == dim) THEN ! Vector
               SizFld(i) = GmfVec
            ELSE IF (SizFld(i) == dim * (dim + 1) / 2 ) THEN ! Sym SolMat
               SizFld(i) = GmfSymMat
            ELSE IF ( SizFld(i) == dim * dim   ) THEN ! SolMat (pleine)
               SizFld(i) = GmfMat
            ELSE
               PRINT *, mod_name, ' : ERREUR : UNVALID SOLUTION TYPE', SizFld(i)
               PRINT *, '    MUST BE Scalar(1), Vector(2), Tensor(3), Matrix(4)'
               CALL stop_run()
            END IF

         END DO

         NbrSol = Size(SolMat, 2)
         IF (dim == 2 ) THEN
            res = GmfWriteFieldF77(Nfic, GmfSolAtTriangles, NbrSol, NbrFld, SizFld, SolMat, 0)
         ELSE IF (dim == 3 ) THEN
            IF (Present(sel_approche)) THEN
               SELECT CASE(sel_approche)
                  CASE(cs_approche_vertexcentered)
                     res = GmfWriteFieldF77(Nfic, GmfSolAtVertices, NbrSol, NbrFld, SizFld, SolMat, 0)
                  CASE(cs_approche_cellcentered)
                     res = GmfWriteFieldF77(Nfic, GmfSolAtTetrahedra, NbrSol, NbrFld, SizFld, SolMat, 0) !-- en 1ere approximation %%%%
               END SELECT
            ELSE
               res = GmfWriteFieldF77(Nfic, GmfSolAtVertices, NbrSol, NbrFld, SizFld, SolMat, 0)
            END IF
         END IF
         IF (res /= NbrSol) THEN
            PRINT *, mod_name, ' : ERREUR : NUMBER OF READ SOLUTIONS', res, '/=', NbrSol
            IF ( Present(Err) ) THEN
               Err = -2
            END IF
         END IF


         IF (Present(Time) ) THEN
            copy_time = Time
            res = GmfWriteFieldF77(Nfic, GmfTime, 1, 0, 0, copy_Time, 0)
         END IF

         IF (Present(Iter) ) THEN
            res = GmfWriteFieldF77(Nfic, GmfIterations, 1, 0, 0, 0, Iter)
         END IF


         res = GmfCloseMeshF77(Nfic)


      END SUBROUTINE writeSolMatTria


      SUBROUTINE writeSolMatDblTRIa(SolNam, dim, NbrFld, SizFld, SolMat, Err, Iter, Time, sel_approche)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "writeSolMatDblTRIa"

         CHARACTER(LEN = *), INTENT(IN)          :: SolNam
         REAL, DIMENSION(:, : ), INTENT(IN)    :: SolMat
         INTEGER, INTENT(IN)                   :: NbrFld
         INTEGER, DIMENSION(: ), INTENT(INOUT) :: SizFld
         INTEGER, OPTIONAL, INTENT(IN)         :: Err
         INTEGER, INTENT(IN)                   :: dim
         REAL, OPTIONAL, INTENT(IN)            :: Time
         INTEGER, OPTIONAL, INTENT(IN)         :: Iter
         INTEGER, OPTIONAL, INTENT(IN)         :: sel_approche

         INTEGER :: i, j
         REAL,  DIMENSION(:, : ),  ALLOCATABLE :: temp !-- [KADNA-PROTECTED]
         INTEGER    :: Iters, errs

         ALLOCATE(temp(Size(SolMat, 1), Size( SolMat, 2)))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "temp", Size(temp))
         END IF

         Iters = 0
         IF ( Present(Iter) ) THEN
            Iters = Iter
         END IF

         ! Cette transformation n'a pas l'air de bien marcher avec
         ! les options de v�rification de d�bordement de tableaux.
         DO i = 1, Size(SolMat, 1)
            DO j = 1, Size(SolMat, 2)
!WAIT               IF (SolMat(i,j) > Huge(1.0_4)) THEN
!WAIT                  PRINT *, mod_name, " ERREUR : i=", i, "j=", j, "le champ sorti depasse Huge(1.0_4)"
!WAIT                  PRINT *, "HINT : manque d'initialisation du champ ?"
!WAIT                  CALL stop_run()
!WAIT               END IF
               temp(i, j)  = SolMat(i, j)
            END DO
         END DO
         IF (Present(sel_approche)) THEN
            CALL writeSolMatTria(SolNam, dim, NbrFld, SizFld, temp, errs, Iters, Time, sel_approche)
         ELSE
            CALL writeSolMatTria(SolNam, dim, NbrFld, SizFld, temp, errs, Iters, Time)
         END IF

         DEALLOCATE(temp)
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "temp")
         END IF
         IF (.FALSE.) THEN  ! Pour suppression de warning
            PRINT *,Err 
         END IF
 
      END SUBROUTINE writeSolMatDblTRIa




      SUBROUTINE writeSolVec(SolNam, dim, SolMat, Err, Iter, Time)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "writeSolVec"

         CHARACTER(LEN = *), INTENT(IN)     :: SolNam
         INTEGER, INTENT(IN)              :: dim
         REAL, DIMENSION(: ), INTENT(IN) :: SolMat !-- [KADNA-PROTECTED]
         INTEGER, OPTIONAL, INTENT(OUT)   :: Err
         INTEGER, OPTIONAL, INTENT(IN)    :: Iter
         REAL, OPTIONAL, INTENT(IN)       :: Time

    !-- loc
         INTEGER                          :: NbrSol
         INTEGER :: Nfic, res
    !  integer :: KwdTab(GmfMaxSol + 2, GmfMaxKwd)
         INTEGER :: Len
         INTEGER, DIMENSION(1) :: SizFld
         SizFld = (/ 1 /)


    !-- end dec loc

         IF ( Present(Err) ) THEN
            Err = 0
         END IF

         Len = Len_trim(SolNam)

         Nfic = GmfOpenMeshF77( SolNam(1: Len), GmfWrite, dim, 0)

         IF (Nfic <= 0) THEN
            PRINT *, mod_name, ' ** CANNOT CREATE SOLUTION FILE'
            IF ( Present(Err) ) THEN
               Err = -1
            END IF
            RETURN
         END IF

         PRINT *, mod_name, ' %', SolNam(1: Len), ' CREATED'

         NbrSol = Size(SolMat, 1)
         res = GmfWriteFieldF77(Nfic, GmfSolAtVertices, NbrSol, 1, SizFld, SolMat, 0)
         IF (res /= NbrSol) THEN
            PRINT *, mod_name, ' : ERREUR : NUMBER OF WRITTEN SOLUTIONS', res, '/=', NbrSol
            IF ( Present(Err) ) THEN
               Err = -2
            END IF
         END IF

         IF (Present(Time) ) THEN
            res = GmfWriteFieldF77(Nfic, GmfTime, 1, 0, 0, Time, 0)
         END IF

         IF (Present(Iter) ) THEN
            res = GmfWriteFieldF77(Nfic, GmfIterations, 1, 0, 0, 0, Iter)
         END IF


         res = GmfCloseMeshF77(Nfic)


      END SUBROUTINE writeSolVec

      SUBROUTINE writeSolVecDbl(SolNam, dim, SolMat, Err, Iter, Time)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "writeSolVecDbl"

         CHARACTER(LEN = *), INTENT(IN)       :: SolNam
         REAL, DIMENSION(: ), INTENT(IN) :: SolMat
         INTEGER, INTENT(OUT), OPTIONAL                  :: Err
         INTEGER, INTENT(IN)                :: dim
         REAL, INTENT(IN), OPTIONAL    :: Time
         INTEGER, INTENT(IN), OPTIONAL :: Iter

    !-- loc
         INTEGER :: i
         REAL,  DIMENSION(: ), ALLOCATABLE  :: temp !-- [KADNA-PROTECTED]

         ALLOCATE(temp(Size(SolMat, 1)))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "temp", Size(temp))
         END IF
         DO i = 1, Size( SolMat, 1)
            temp(i) = SolMat(i)
         END DO

         CALL writeSolVec(SolNam, dim, temp, Err, Iter, Time)

      END SUBROUTINE writeSolVecDbl





      SUBROUTINE  GetVer2Tet(NbrVer, Tet, Head, Ver2Tet)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "GetVer2Tet"


         INTEGER, INTENT(IN)              :: NbrVer
         INTEGER, DIMENSION(:, : ), INTENT(IN) :: Tet
         INTEGER, DIMENSION(: ), INTENT(OUT) :: Ver2Tet
         INTEGER, DIMENSION(: ), INTENT(OUT) :: Head

    !---- local
         INTEGER :: i, iTet, iVer
         INTEGER :: NbrTet, SzV2t
         INTEGER :: nvmax
         INTEGER, DIMENSION(: ), ALLOCATABLE    :: current
         INTEGER :: fact


         fact = Size(Tet, 1)
         NbrTet = Size(Tet, 2)

    !-- Alloc
         SzV2t = fact * NbrTet
    ! call myallocate( Head, NbrVer+1)
    !
    ! call myallocate(Ver2Tet,SzV2T)
         ALLOCATE( current(NbrVer + 1))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "current", Size(current))
         END IF

    ! calclul nvmax : nb max de tet attache a un vertex
    !------------------
         DO iVer = 1, NbrVer + 1
            Head(iVer) = 0
            current(iVer) = 0
         END DO

         nvmax = 0
         DO iTet = 1, NbrTet
            DO i = 1, fact
               iVer =  Tet(i, iTet)
          !    print*,'iVer ',iVer
               current(iVer) =  current(iVer) + 1
               IF (current(iVer) > nvmax) THEN
                  nvmax =  current(iVer)
               END IF
            END DO
         END DO

         Head(1) = 1
         DO iVer = 2, NbrVer
            Head(iVer) = Head(iVer -1) + current(iVer -1)
            current(iVer -1) = 0
         END DO
         current(NbrVer) = 0
         Head(NbrVer + 1)  = SzV2t + 1


         IF (out%verbose >= 4) THEN
            PRINT *, mod_name, ' NVMAX :',  nvmax
         END IF

         DO iTet = 1, NbrTet
            DO i = 1, fact
               iVer =  Tet(i, iTet)
               Ver2Tet(Head(iVer) + current(iVer)) = iTet
               current(iVer) = current(iVer) + 1
            END DO
         END DO


      END SUBROUTINE GetVer2Tet







      SUBROUTINE GetNbrEdg(NbrVer, Tet, Head, Ver2Tet, NbrEdg)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "GetNbrEdg"


         INTEGER, INTENT(IN)               :: NbrVer
         INTEGER, DIMENSION(:, : ), INTENT(INOUT) :: Tet
         INTEGER, DIMENSION(: ), INTENT(OUT) :: Ver2Tet
         INTEGER, DIMENSION(: ), INTENT(OUT) :: Head
         INTEGER, INTENT(OUT)              :: NbrEdg

    !-- local
         INTEGER, DIMENSION(: ), ALLOCATABLE :: color
         INTEGER :: iVer, iv, iTet, k, vid, nfac


         CALL GetVer2Tet(NbrVer, Tet, Head, Ver2Tet)


         ALLOCATE(color(NbrVer))
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "color", Size(color))
         END IF

         nfac = Size(Tet, 1)
         color = 0
         NbrEdg = 0

         DO iVer = 1, NbrVer
            DO iv = 1, Head(iVer + 1) - Head(iVer)
               iTet =  Ver2Tet(Head(iVer) + iv - 1)
               DO k = 1, nfac
                  vid =  Tet(k, iTet)
                  IF (iVer < vid .AND. color(vid) /= iVer) THEN
                     NbrEdg =  NbrEdg + 1
                     color(vid) = iVer
                  END IF
               END DO
            END DO
         END DO

      END SUBROUTINE GetNbrEdg






   END MODULE LibMESh

