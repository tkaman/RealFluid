Module Quadrature

   USE LesTypes

   IMPLICIT NONE

   INTERFACE Int_d
      MODULE PROCEDURE Int_d_scalar
      MODULE PROCEDURE Int_d_vectorial
   END INTERFACE   

 CONTAINS

  !====================================
  FUNCTION oInt_n(ele, ff) RESULT(i_ff)
  !====================================
  !
  ! \oint_{\partial E} {ff * n}
  !
    IMPLICIT NONE

    TYPE(element_str),      INTENT(IN) :: ele
    REAL, DIMENSION(:,:,:), INTENT(IN) :: ff

    REAL, DIMENSION(SIZE(ff, 1)) :: i_ff
    !----------------------------------------------

    INTEGER,                 POINTER :: N_quad
    INTEGER,                 POINTER :: N_points
    INTEGER, DIMENSION(:),   POINTER :: loc
    REAL,    DIMENSION(:,:), POINTER :: p
    REAL,    DIMENSION(:,:), POINTER :: n
    REAL,    DIMENSION(:),   POINTER :: w
    
    REAL, DIMENSION(:,:), ALLOCATABLE :: ff_q

    INTEGER :: l, j, iq, id, k
    !----------------------------------------------

    i_ff = 0.d0

    ALLOCATE( ff_q(SIZE(ff,1),SIZE(ff,2)) )

    DO j = 1, ele%N_faces

       N_quad   => ele%faces(j)%f%N_quad
       N_points => ele%faces(j)%f%N_points
       loc      => ele%faces(j)%f%l_nu
       p        => ele%faces(j)%f%phi_q
       w        => ele%faces(j)%f%w_q
       n        => ele%faces(j)%f%n_q

       DO iq = 1, N_quad

          ff_q = 0.d0
          DO k = 1, N_points

             DO id = 1, SIZE(ff, 2)

                ff_q(:, id) = ff_q(:, id) + ff(:, id, loc(k)) * p(k, iq)
                
             ENDDO             

          ENDDO 

          DO l = 1, SIZE(ff, 1)
             i_ff(l) = i_ff(l) + w(iq) * DOT_PRODUCT(ff_q(l,:), n(:, iq) )
          ENDDO
          
       ENDDO
       
       NULLIFY(N_quad, N_points, loc, p, w, n)
       
    ENDDO

    DEALLOCATE( ff_q )
    
  END FUNCTION oInt_n
  !==================

  !========================================
  FUNCTION bInt_i(ele, ii, f_n) RESULT(i_f)
  !========================================
  !
  ! \int_{\partial\Omega_i /\ \partial\Omega} {\phi_i f_n}
  !
    IMPLICIT NONE

    TYPE(face_str),       INTENT(IN) :: ele
    INTEGER,              INTENT(IN) :: ii
    REAL, DIMENSION(:,:), INTENT(IN) :: f_n

    REAL, DIMENSION(SIZE(f_n, 1)) :: i_f
    !---------------------------------------

    REAL, DIMENSION(:,:), POINTER :: p
    REAL, DIMENSION(:),   POINTER :: w
 
    REAL, DIMENSION(SIZE(f_n, 1)) :: f_q

    INTEGER :: iq, k
    !---------------------------------------

    i_f = 0.d0
  
    p  => ele%phi_q
    w  => ele%w_q
    
    DO iq = 1, ele%N_quad

       f_q = 0.0
       DO k = 1, ele%N_points
          f_q = f_q + f_n(:, k) * p(k, iq)
       ENDDO
              
       i_f = i_f + w(iq) * p(ii, iq) * f_q

    ENDDO

    NULLIFY(p, w)

  END FUNCTION bInt_i
  !==================  

  !=================================
  FUNCTION bInt(ele, f) RESULT(i_fn)
  !=================================
  !
  ! \int_{\partial\Omega} {f}
  !
    IMPLICIT NONE

    TYPE(face_str),       INTENT(IN) :: ele
    REAL, DIMENSION(:,:), INTENT(IN) :: f

    REAL, DIMENSION(SIZE(f,1)) :: i_fn
    !-----------------------------------------------

    REAL, DIMENSION(:,:), POINTER :: p
    REAL, DIMENSION(:),   POINTER :: w
 
    REAL, DIMENSION(SIZE(f,1)) :: f_q

    INTEGER :: iq, k
    !---------------------------------------

    i_fn = 0.d0
  
    p  => ele%phi_q
    w  => ele%w_q
    
    DO iq = 1, ele%N_quad

       f_q = 0.0
       DO k = 1, ele%N_points
          f_q = f_q + f(:, k) * p(k, iq)
       ENDDO
              
       i_fn = i_fn + w(iq) * f_q

    ENDDO
    
    NULLIFY(p, w)

  END FUNCTION bInt
  !================  
  
  !==========================================
  FUNCTION Int_d_scalar(ele, ff) RESULT(i_ff)
  !==========================================
  !
  ! \int_{E} {ff}
  !
    IMPLICIT NONE

    TYPE(element_str),          INTENT(IN) :: ele
    REAL(KIND=8), DIMENSION(:), INTENT(IN) :: ff

    REAL(KIND=8) :: i_ff
    !----------------------------------------------

    INTEGER :: N_quad,  N_points
   
    REAL(KIND=8), DIMENSION(:,:), POINTER :: p
    REAL(KIND=8), DIMENSION(:),   POINTER :: w
        
    REAL(KIND=8) :: ff_q

    INTEGER :: iq
    !----------------------------------------------

    i_ff = 0.d0

    N_quad   =  ele%N_quad
    N_points =  ele%N_points
    p        => ele%phi_q
    w        => ele%w_q

    DO iq = 1, N_quad

       ff_q = SUM( ff * p(:, iq) )

       i_ff = i_ff + w(iq) * ff_q

    ENDDO
       
    NULLIFY( p, w )
       
  END FUNCTION Int_d_scalar
  !========================

  !=============================================
  FUNCTION Int_d_vectorial(ele, ff) RESULT(i_ff)
  !=============================================
  !
  ! \int_{E} {ff}
  !
    IMPLICIT NONE

    TYPE(element_str),            INTENT(IN) :: ele
    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: ff

    REAL(KIND=8), DIMENSION(SIZE(ff, 1)) :: i_ff
    !----------------------------------------------

    INTEGER :: N_quad,  N_points
   
    REAL(KIND=8), DIMENSION(:,:), POINTER :: p
    REAL(KIND=8), DIMENSION(:),   POINTER :: w
        
    REAL(KIND=8), DIMENSION(SIZE(ff, 1)) :: ff_q

    INTEGER :: iq, k
    !----------------------------------------------

    i_ff = 0.d0

    N_quad   =  ele%N_quad
    N_points =  ele%N_points
    p        => ele%phi_q
    w        => ele%w_q

    DO iq = 1, N_quad

       DO  k = 1, SIZE(ff, 1)
          ff_q(k) = SUM( ff(k, :) * p(:, iq) )
       ENDDO
       
       i_ff = i_ff + w(iq) * ff_q

    ENDDO
       
    NULLIFY( p, w )
       
  END FUNCTION Int_d_vectorial
  !===========================

END Module Quadrature
