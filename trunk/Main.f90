!> \mainpage FluidBox Documentation Page
!! \image html FBxLogo.png
!! \image latex FBxLogo.png
!! \section intro_sec Introduction
!!
!! FluidBox provides tools for solving CFD problems
!!
!! \section start Getting started
!!
!! \subsection main_program Program
!! The entry point of the software is in \ref Main.f90
!!
!! Here is a Todo list \ref todo
!!
!! You can also look for the known Bugs, we'll be glad you solved them: \ref bug

!> \brief Main program
!!
!! MainLag is the entry point of the program
!! It reads the arguments given on the command line
!! and send them to the main routine
!! 
!! \see The main routine is fluidbox::FBxMain
!!
!! \todo 
!! - Documenting all the code
!! - Cleaning the warnings (unused or uninitialised values)
!! - Cleaning the compilations flags in order to gain visibility
!!
!! \bug Many Many
   PROGRAM MainLag

      USE FluidBox

      IMPLICIT NONE
      CHARACTER(LEN = *), PARAMETER :: mod_name = "MainLag"
      CHARACTER(LEN = 1024) :: exec
      CHARACTER(LEN = 1024) :: rootname
      INTEGER               :: nb_args
      !INTEGER, EXTERNAL     :: iargc 

      CALL getarg(0, exec)

      nb_args = iargc()

      IF (nb_args < 1) THEN
         PRINT *, mod_name, " ERREUR : donner le nom du cas en argument (Data%RootName), argc==", nb_args
         PRINT *, "ex.: ", Trim(exec), " toto"
         STOP
      END IF

      CALL getarg(1, rootname)

      CALL FBxMain(rootname)

   END PROGRAM MainLag

