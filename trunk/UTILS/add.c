#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void makenan_(void *anonyme, long *id) { /*BIND(FORTRAN)*/
  assert(sizeof(long long) == sizeof(double));
//  *(long long *)anonyme = *id;
//  *(long long *)anonyme = 0xffffffffffffffff;
//  *(long long *)anonyme = 0xbeefbeefbeefbeef;
  *(long long *)anonyme = 0x7ff7ffffffffffff; // C'est celui l� qui plante le mieux (c'est un Snan Signaling Not a Number)
} /* makenan_ */

void printnan_(double *xx) { /*BIND(FORTRAN)*/
  long long *decode = (long long *)xx;
  int res = isnan(*xx);
  fprintf(stdout, "isnan : val==%35.20g hint==%Ld isnan==%d\n", *xx, *decode, res);
}

int isnan_(double *xx) { /*BIND(FORTRAN)*/
  int res = isnan(*xx);
  if (res) {
    fprintf(stdout, "isnan : %g %d %d\n", *xx, isnan(*xx), isnan(*xx) != 0);
    fprintf(stderr, "isnan : %g %d %d\n", *xx, isnan(*xx), isnan(*xx) != 0);
  };
  return res;
}

int isinf_(double *xx) { /*BIND(FORTRAN)*/
  int res = isinf(*xx) != 0;
  if (res) {
    fprintf(stdout, "isinf : %g %d %d\n", *xx, isinf(*xx), isinf(*xx) != 0);
    fprintf(stderr, "isinf : %g %d %d\n", *xx, isinf(*xx), isinf(*xx) != 0);
  };
  return res;
}

void print_logical_(int *xx) { /*BIND(FORTRAN)*/
  fprintf(stdout, "print_logical_ : %d %x\n", *xx, *xx);
}

void errno_(const char *mssg, int len) {
  fprintf(stderr, "errno==%d\n", errno);
  perror(mssg);
}

void get_set_prec_(int *new_prec, int *new_round, int *old_stat) { /*BIND(FORTRAN)*/

  const char const* mod_name = "get_set_prec_";
  unsigned int _fp_oldcw, _finter, _fp_cw;

  __asm__ __volatile__ ("fnstcw %0" : "=m" (*&_fp_oldcw));

  *old_stat = _fp_oldcw;

//  fprintf(stdout, "%s new_prec==%d, new_round==%d, old_stat<-%d\n", mod_name, *new_prec, *new_round, *old_stat);

  _fp_oldcw = 0x10ff;

  if (*new_prec >= 0) { //-- 8 et 9 : x300

    _finter = (_fp_oldcw & ~0x300) | ((int)*new_prec << 8);

    __asm__ __volatile__ ("fldcw %0" : : "m" (*&_finter));

  } else {
    _finter = _fp_oldcw;
  }

  if (*new_round >= 0) { //-- 10 et 11 : xa00

    _fp_cw = (_finter & ~0xc00) | ((int)*new_round << 10);

    __asm__ __volatile__ ("fldcw %0" : : "m" (*&_fp_cw));

  }

}
#define FP_DECLARE() \
            unsigned int _xpfpa_fpu_oldcw, _xpfpa_fpu_cw;

#define FP_SWITCH() \
            __asm__ __volatile__ ("fnstcw %0" : "=m" (*&_xpfpa_fpu_oldcw)); \
            _xpfpa_fpu_cw = _xpfpa_fpu_oldcw & 0x100; \
            __asm__ __volatile__ ("fldcw %0" : : "m" (*&_xpfpa_fpu_cw));

void control_() {
  FP_DECLARE()
  FP_SWITCH()
}

void ecrire_en_c_(void) { /*BIND(FORTRAN)*/
  const char *fname = "fort.97";
  int file;
  ssize_t nb;
  double toto = 1.7;
  file = open(fname, O_WRONLY);
  assert(file);
  nb = write(file, &toto, sizeof(toto));
  if (nb == 0) {
    fprintf(stderr, "nb==0\n");
    exit(1);
  };
  close(file);
}

void ecrire_en_c(void) { /*BIND(FORTRAN)*/
  const char *fname = "fort.97";
  int file;
  ssize_t nb;
  double toto = 1.7;
  file = open(fname, O_WRONLY);
  assert(file);
  nb = write(file, &toto, sizeof(toto));
  if (nb == 0) {
    fprintf(stderr, "nb==0\n");
    exit(1);
  };
  close(file);
}

