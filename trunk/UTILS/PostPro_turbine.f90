MODULE PostPro_turbine

  USE LesTypes
  USE Visc_Laws
  USE EOSLaws

  IMPLICIT NONE

CONTAINS

  !=================================
  SUBROUTINE efficiency_turbine(Var)
    !=================================
    !> \brief
    !! Routine: Efficiency_turbine
    !! The implementation of the GP EOS is a bit schetcy
    !!
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "efficiency_turbine"
    TYPE(Variables), INTENT(IN) :: Var
    !------------------------------------

    TYPE(face_str) :: loc_ele

    INTEGER, DIMENSION(Var%NCells) :: nn_b

    REAL, DIMENSION(10) :: Vb_in, Vb_out

    REAL :: P_mean_out, S_mean_in, T_s, rho_s, h_s, T_bar

    REAL :: rho_in,  P_in,  T_in,  q_in,  c_in,  M_in,  h_in,  ht_in,  S_in
    REAL :: rho_out, P_out, T_out, q_out, c_out, M_out, h_out, ht_out, S_out

    REAL :: h, ht, s, M, theta, hb_in, hb_out, rho, T, RR, Z_c, T_c

    REAL :: eta_1, eta_2, eta_3, Dh, DT

    INTEGER :: N_in, N_out, ite, ierror

    INTEGER :: i, jf, k, bc_idx, bc_type

    REAL,    PARAMETER :: Pi = 4.d0*ATAN(1.d0)
    REAL,    PARAMETER :: toll = 1.0E-8
    INTEGER, PARAMETER :: ite_max = 40
    !------------------------------------
    SELECT CASE (sel_EosLaw) ! close at the end of the routine
    CASE(cs_loietat_GP)
       PRINT*, mod_name, "GP"
    CASE default
       RR = 8.1d0

       T_c = Data%Tcrit

       IF( sel_EosLaw == cs_loietat_prsv ) THEN
          Z_c = 0.311155425170673d0  ! Constant
       ELSEIF( sel_EosLaw == cs_loietat_sw ) THEN
          Z_c = 0.285883427d0        ! Should be read from the eos file
       ELSE
          WRITE(*,*) 'ERROR: EOS ZC'
       ENDIF

       nn_b = 0;
       DO jf = 1, SIZE(b_element)

          loc_ele = b_element(jf)%b_f

          bc_idx  = loc_ele%bc_type
          bc_type = Data%FacMap(bc_idx)

          IF( bc_type == lgc_entree_turbine_subsonique ) THEN

             DO k = 1, loc_ele%N_points

                nn_b( loc_ele%NU(k) ) = -1

             ENDDO

          ELSEIF( bc_type == lgc_sortie_turbine_subsonique ) THEN

             DO k = 1, loc_ele%N_points

                nn_b( loc_ele%NU(k) ) = 1

             ENDDO

          ENDIF

       ENDDO



       N_in = 0; N_out = 0

       Vb_in = 0.0; Vb_out = 0.0

       DO i = 1, SIZE(nn_b)

          IF( nn_b(i) == -1 ) THEN

             h = Var%Vp(i_eps_vp, i) + &
                  Var%Vp(i_pre_vp, i)/Var%Vp(i_rho_vp, i)

             s = s__rho_T( Var%Vp(i_rho_vp, i),  Var%Vp(i_tem_vp, i) )

             M = SQRT( SUM(Var%Vp(i_vi1_vp:i_vi2_vp, i)**2) ) / Var%Vp(i_son_vp, i)

             theta = ATAN( Var%Vp(i_vi2_vp, i)/Var%Vp(i_vi1_vp, i) )

             Vb_in(1) = Vb_in(1) + Var%Vp(i_rho_vp, i)
             Vb_in(2) = Vb_in(2) + Var%Vp(i_vi1_vp, i)
             Vb_in(3) = Vb_in(3) + Var%Vp(i_vi2_vp, i)
             Vb_in(4) = Vb_in(4) + Var%Vp(i_tem_vp, i)
             Vb_in(5) = Vb_in(5) + h
             Vb_in(6) = Vb_in(6) + h + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
             Vb_in(7) = Vb_in(7) + s
             Vb_in(8) = Vb_in(8) + M
             Vb_in(9) = Vb_in(9) + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
             Vb_in(10)= Vb_in(10) + theta

             N_in = N_in + 1

          ELSEIF( nn_b(i) == 1 ) THEN

             h = Var%Vp(i_eps_vp, i) + &
                  Var%Vp(i_pre_vp, i)/Var%Vp(i_rho_vp, i)

             s = s__rho_T( Var%Vp(i_rho_vp, i),  Var%Vp(i_tem_vp, i) )

             M = SQRT( SUM(Var%Vp(i_vi1_vp:i_vid_vp, i)**2) ) / Var%Vp(i_son_vp, i)

             theta = ATAN( Var%Vp(i_vi2_vp, i)/Var%Vp(i_vi1_vp, i) )

             Vb_out(1) = Vb_out(1) + Var%Vp(i_rho_vp, i)
             Vb_out(2) = Vb_out(2) + Var%Vp(i_vi1_vp, i)
             Vb_out(3) = Vb_out(3) + Var%Vp(i_vi2_vp, i)
             Vb_out(4) = Vb_out(4) + Var%Vp(i_tem_vp, i)
             Vb_out(5) = Vb_out(5) + h
             Vb_out(6) = Vb_out(6) + h + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
             Vb_out(7) = Vb_out(7) + s
             Vb_out(8) = Vb_out(8) + M
             Vb_out(9) = Vb_out(9) + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
             Vb_out(10)= Vb_out(10) + theta

             N_out = N_out + 1

          ENDIF

       ENDDO

       Vb_in  = Vb_in  / REAL(N_in,  8)
       Vb_out = Vb_out / REAL(N_out, 8)


       P_mean_out = P__rho_T( Vb_out(1), Vb_out(4) )
       S_mean_in  = s__rho_T( Vb_in(1),  Vb_in(4)  )

       ite = 0; T_bar = 1.01d0

       DO

          T_s = T_bar

          rho_s = rho__P_T(P_mean_out, T_s)

          T_bar = T__rho_s(rho_s, S_mean_in)

          IF( ABS(T_bar - T_s)/T_bar <= toll ) THEN

             rho_s = rho__P_T(P_mean_out, T_bar)
             T_s = T_bar

             EXIT

          ENDIF

          ite = ite + 1

          IF( ite > ite_max ) THEN
             WRITE(*,*) 'ERROR: not converged in efficiency turbine'
             STOP
          ENDIF

       ENDDO

       h_s = e__rho_T(rho_s, T_s) + P_mean_out/rho_s

       rho = Vb_in(1); T = Vb_in(4)
       hb_in = e__rho_T(rho, T) + P__rho_T(rho, T)/rho

       rho = Vb_out(1); T = Vb_out(4)
       hb_out = e__rho_T(rho, T) + P__rho_T(rho, T)/rho

       !     eta_1 = (hb_in - hb_out)/( hb_in - h_s) 
       eta_1 = ( Vb_in(5) -  Vb_out(5) ) / (Vb_in(5) - h_s)
       eta_2 = ( Vb_out(9) - Vb_in(9)  ) / (Vb_in(5) - h_s)
       eta_3 = ( eta_1 + eta_2 ) / 2.d0

       Dh = (hb_in - hb_out) * Z_c * T_c * RR

       DT = ( Vb_in(4) - Vb_out(4) ) / Vb_in(4)

       IF (Var%kt < Data%ifre) THEN

          OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), & 
               STATUS = 'REPLACE', IOSTAT = ierror)

          IF(ierror /= 0) THEN
             WRITE(*,*) 'ERROR: Impossible to open the file effic'
             WRITE(*,*) 'STOP'
             STOP
          ENDIF

          CLOSE(81)

          OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), &
               ACTION = 'WRITE', POSITION = 'APPEND', IOSTAT = ierror)

          WRITE(81, *) 'TITLE = "Efficiency turbine"'
          WRITE(81, *) 'VARIABLES = "Ite", "eta", "Dh", "DT", "theta"'

          PRINT*,"AMANDA CHE CAZZO FAI ? "
          !        WRITE(81, 22) Var%Kt, Eta, Dh, DT, Vb_out(9)*180.d0/Pi

       ELSE

          OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), & 
               STATUS = 'REPLACE', IOSTAT = ierror)

          IF(ierror /= 0) THEN
             WRITE(*,*) 'ERROR: Impossible to open the file effic'
             WRITE(*,*) 'STOP'
             STOP
          ENDIF

          CLOSE(81)


          OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), &
               ACTION = 'WRITE', POSITION = 'APPEND', IOSTAT = ierror)

          rho_in = Vb_in(1); T_in = Vb_in(4)

          P_in = P__rho_T( rho_in, T_in )

          q_in = Vb_in(9)

          c_in = c__rho_T( rho_in, T_in )

          M_in = SQRT(2.d0 * q_in) / c_in

          h_in = e__rho_T(rho_in, T_in) +  P_in / rho_in

          ht_in = h_in + q_in

          S_in = s__rho_T( rho_in, T_in )

          !---------------------------------

          rho_out = Vb_out(1); T_out = Vb_out(4)

          P_out = P__rho_T( rho_out, T_out )

          q_out = Vb_out(9)

          c_out = c__rho_T( rho_out, T_out )

          M_out = SQRT(2.0*q_out) / c_out

          h_out = e__rho_T(rho_out, T_out) +  P_out / rho_out

          ht_out = h_out + q_out

          S_out = s__rho_T( rho_out, T_out )

          WRITE(81, *) 'P, rho, T, ek, M, c, h, ht, S'
          WRITE(81, *) 'IN: '
          WRITE(81, 23) P_in, rho_in, T_in, q_in, M_in, c_in, h_in, ht_in, S_in
          WRITE(81, *) 'OUT: '
          WRITE(81, 23) P_out, rho_out, T_out, q_out, M_out, c_out, h_out, ht_out, S_out
          WRITE(81, *) 'h_id, T_id, rho_id, eta 1, eta 2, eta 3, Dh, DT, angle'
          WRITE(81, 23) h_s, T_s, rho_s, eta_1, eta_2, eta_3, Dh, Dt,  Vb_out(10)*180.d0/Pi

       ENDIF

       CLOSE(81)
    END SELECT ! end of select case of the beginning
22  FORMAT(I6 X2 4(1x,e24.16))
23  FORMAT( 9(1x,e15.7))
  END SUBROUTINE efficiency_turbine
  !================================  



  !====================================
  SUBROUTINE efficiency_turbine_XL(Var)
    !====================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(IN) :: Var
    !------------------------------------

    TYPE(face_str) :: loc_ele

    INTEGER, DIMENSION(Var%NCells) :: nn_b

    REAL, DIMENSION(10) :: Vb_in, Vb_out

    REAL :: P_mean_out, S_mean_in, T_s, rho_s, h_s, T_bar

    REAL :: rho_in,  P_in,  T_in,  q_in,  c_in,  M_in,  h_in,  ht_in,  S_in
    REAL :: rho_out, P_out, T_out, q_out, c_out, M_out, h_out, ht_out, S_out

    REAL :: h, ht, s, M, theta, hb_in, hb_out, rho, T, RR, Z_c, T_c

    REAL :: eta_1, eta_2, eta_3, Dh, DT

    INTEGER :: N_in, N_out, ite, ierror

    INTEGER :: i, je, k, bc_idx, bc_type

    REAL,    PARAMETER :: Pi = 4.d0*ATAN(1.d0)
    REAL,    PARAMETER :: toll = 1.0E-8
    INTEGER, PARAMETER :: ite_max = 40
    !------------------------------------

    RR = 8.1d0

    T_c = Data%Tcrit

    IF( sel_EosLaw == cs_loietat_prsv ) THEN
       Z_c = 0.311155425170673d0  ! Constant
    ELSEIF( sel_EosLaw == cs_loietat_sw ) THEN
       Z_c = 0.285883427d0        ! Should be read from the eos file
    ELSE
       WRITE(*,*) 'ERROR: EOS ZC'
    ENDIF

    nn_b = 0;
    DO je = 1, SIZE(d_element)

       DO k = 1, d_element(je)%e%N_points

          IF(     ABS(d_element(je)%e%Coords(1, k) + 0.8d0) < 1.0E-8 ) THEN

             nn_b( d_element(je)%e%NU(k) ) = -1

          ELSEIF( ABS(d_element(je)%e%Coords(1, k) - 2.d0) < 1.0E-8 ) THEN

             nn_b( d_element(je)%e%NU(k) ) = 1

          ENDIF

       ENDDO

    ENDDO

    N_in = 0; N_out = 0

    Vb_in = 0.0; Vb_out = 0.0

    DO i = 1, SIZE(nn_b)

       IF( nn_b(i) == -1 ) THEN

          h = Var%Vp(i_eps_vp, i) + &
               Var%Vp(i_pre_vp, i)/Var%Vp(i_rho_vp, i)

          s = s__rho_T( Var%Vp(i_rho_vp, i),  Var%Vp(i_tem_vp, i) )

          M = SQRT( SUM(Var%Vp(i_vi1_vp:i_vi2_vp, i)**2) ) / Var%Vp(i_son_vp, i)

          theta = ATAN( Var%Vp(i_vi2_vp, i)/Var%Vp(i_vi1_vp, i) )

          Vb_in(1) = Vb_in(1) + Var%Vp(i_rho_vp, i)
          Vb_in(2) = Vb_in(2) + Var%Vp(i_vi1_vp, i)
          Vb_in(3) = Vb_in(3) + Var%Vp(i_vi2_vp, i)
          Vb_in(4) = Vb_in(4) + Var%Vp(i_tem_vp, i)
          Vb_in(5) = Vb_in(5) + h
          Vb_in(6) = Vb_in(6) + h + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
          Vb_in(7) = Vb_in(7) + s
          Vb_in(8) = Vb_in(8) + M
          Vb_in(9) = Vb_in(9) + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
          Vb_in(10)= Vb_in(10) + theta

          N_in = N_in + 1

       ELSEIF( nn_b(i) == 1 ) THEN

          h = Var%Vp(i_eps_vp, i) + &
               Var%Vp(i_pre_vp, i)/Var%Vp(i_rho_vp, i)

          s = s__rho_T( Var%Vp(i_rho_vp, i),  Var%Vp(i_tem_vp, i) )

          M = SQRT( SUM(Var%Vp(i_vi1_vp:i_vid_vp, i)**2) ) / Var%Vp(i_son_vp, i)

          theta = ATAN( Var%Vp(i_vi2_vp, i)/Var%Vp(i_vi1_vp, i) )

          Vb_out(1) = Vb_out(1) + Var%Vp(i_rho_vp, i)
          Vb_out(2) = Vb_out(2) + Var%Vp(i_vi1_vp, i)
          Vb_out(3) = Vb_out(3) + Var%Vp(i_vi2_vp, i)
          Vb_out(4) = Vb_out(4) + Var%Vp(i_tem_vp, i)
          Vb_out(5) = Vb_out(5) + h
          Vb_out(6) = Vb_out(6) + h + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
          Vb_out(7) = Vb_out(7) + s
          Vb_out(8) = Vb_out(8) + M
          Vb_out(9) = Vb_out(9) + 0.5d0*SUM( Var%Vp(i_vi1_vp:i_vid_vp, i)**2 )
          Vb_out(10)= Vb_out(10) + theta

          N_out = N_out + 1

       ENDIF

    ENDDO

    Vb_in  = Vb_in  / REAL(N_in,  8)
    Vb_out = Vb_out / REAL(N_out, 8)


    P_mean_out = P__rho_T( Vb_out(1), Vb_out(4) )
    S_mean_in  = s__rho_T( Vb_in(1),  Vb_in(4)  )

    ite = 0; T_bar = 1.01d0

    DO

       T_s = T_bar

       rho_s = rho__P_T(P_mean_out, T_s)

       T_bar = T__rho_s(rho_s, S_mean_in)

       IF( ABS(T_bar - T_s)/T_bar <= toll ) THEN

          rho_s = rho__P_T(P_mean_out, T_bar)
          T_s = T_bar

          EXIT

       ENDIF

       ite = ite + 1

       IF( ite > ite_max ) THEN
          WRITE(*,*) 'ERROR: not converged in efficiency turbine'
          STOP
       ENDIF

    ENDDO

    h_s = e__rho_T(rho_s, T_s) + P_mean_out/rho_s

    rho = Vb_in(1); T = Vb_in(4)
    hb_in = e__rho_T(rho, T) + P__rho_T(rho, T)/rho

    rho = Vb_out(1); T = Vb_out(4)
    hb_out = e__rho_T(rho, T) + P__rho_T(rho, T)/rho

    !     eta_1 = (hb_in - hb_out)/( hb_in - h_s) 
    eta_1 = ( Vb_in(5) -  Vb_out(5) ) / (Vb_in(5) - h_s)
    eta_2 = ( Vb_out(9) - Vb_in(9)  ) / (Vb_in(5) - h_s)
    eta_3 = ( eta_1 + eta_2 ) / 2.d0

    Dh = (hb_in - hb_out) * Z_c * T_c * RR

    DT = ( Vb_in(4) - Vb_out(4) ) / Vb_in(4)

    IF (Var%kt < Data%ifre) THEN

       OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), & 
            STATUS = 'REPLACE', IOSTAT = ierror)

       IF(ierror /= 0) THEN
          WRITE(*,*) 'ERROR: Impossible to open the file effic'
          WRITE(*,*) 'STOP'
          STOP
       ENDIF

       CLOSE(81)

       OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), &
            ACTION = 'WRITE', POSITION = 'APPEND', IOSTAT = ierror)

       WRITE(81, *) 'TITLE = "Efficiency turbine"'
       WRITE(81, *) 'VARIABLES = "Ite", "eta", "Dh", "DT", "theta"'

       !        WRITE(81, 22) Var%Kt, Eta, Dh, DT, Vb_out(9)*180.d0/Pi

    ELSE

       OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), & 
            STATUS = 'REPLACE', IOSTAT = ierror)

       IF(ierror /= 0) THEN
          WRITE(*,*) 'ERROR: Impossible to open the file effic'
          WRITE(*,*) 'STOP'
          STOP
       ENDIF

       CLOSE(81)


       OPEN(81, FILE = 'effic_turbine.'//TRIM(ADJUSTL(Data%RootName)), &
            ACTION = 'WRITE', POSITION = 'APPEND', IOSTAT = ierror)

       rho_in = Vb_in(1); T_in = Vb_in(4)

       P_in = P__rho_T( rho_in, T_in )

       q_in = Vb_in(9)

       c_in = c__rho_T( rho_in, T_in )

       M_in = SQRT(2.d0 * q_in) / c_in

       h_in = e__rho_T(rho_in, T_in) +  P_in / rho_in

       ht_in = h_in + q_in

       S_in = s__rho_T( rho_in, T_in )

       !---------------------------------

       rho_out = Vb_out(1); T_out = Vb_out(4)

       P_out = P__rho_T( rho_out, T_out )

       q_out = Vb_out(9)

       c_out = c__rho_T( rho_out, T_out )

       M_out = SQRT(2.0*q_out) / c_out

       h_out = e__rho_T(rho_out, T_out) +  P_out / rho_out

       ht_out = h_out + q_out

       S_out = s__rho_T( rho_out, T_out )

       WRITE(81, *) 'P, rho, T, ek, M, c, h, ht, S'
       WRITE(81, *) 'IN: '
       WRITE(81, 23) P_in, rho_in, T_in, q_in, M_in, c_in, h_in, ht_in, S_in
       WRITE(81, *) 'OUT: '
       WRITE(81, 23) P_out, rho_out, T_out, q_out, M_out, c_out, h_out, ht_out, S_out
       WRITE(81, *) 'h_id, T_id, rho_id, eta 1, eta 2, eta 3, Dh, DT, angle'
       WRITE(81, 23) h_s, T_s, rho_s, eta_1, eta_2, eta_3, Dh, Dt,  Vb_out(10)*180.d0/Pi

    ENDIF

    CLOSE(81)

22  FORMAT(I6 X2 4(1x,e24.16))
23  FORMAT( 9(1x,e15.7))
    STOP
  END SUBROUTINE efficiency_turbine_XL
  !===================================


END MODULE PostPro_turbine

