MODULE Ringleb_flow

  USE LesTypes  
  USE EOSlaws
  USE PLinAlg

  IMPLICIT NONE
  
CONTAINS
  
  !==========================================
  FUNCTION Exact_Ringleb_sol(x, y) RESULT(Vp)
  !==========================================

    IMPLICIT NONE

    REAL(KIND=8), INTENT(IN) :: x, y

    REAL(KIND=8), DIMENSION(Data%NVarPhy) :: Vp
    !--------------------------------------------

    REAL :: x_, y_, V, b, rho, p, L, psi, Vpsi, theta, vx, vy
    real, parameter :: zero=0.0, fifth=0.2, half=0.5
    real, parameter ::  one=1.0, three=3.0, five=5.0
    !----------------------------------------------

    x_ = -x
    y_ = y
  
    V = V_from_xy_fpitr(x_, y_, 0.d0, 2.0d0)
    
    b = sqrt(one - fifth*V*V)
    rho = b**five
    p = b**7
    L = one/b + one/(three*b**three) + one/(five*b**five) &
        - half*log( (one+b) / (one-b) )

    psi = sqrt( half/V**2-(x_-half*L)*rho )
    Vpsi = sqrt( half-V*V*(x_-half*L)*rho )

    if ( abs( Vpsi - one ) < 1.0e-15  ) then
       theta = half*3.141592653589793238 ! Do not rely on `asin' here.
    else
       theta = asin( psi*V )
    endif

    vx = -V*cos(theta)
    vy = -V*sin(theta)

    IF(y_ > 0.d0) vx = -vx

    Vp(i_rho_vp) = rho
    Vp(i_vi1_vp) = vx
    Vp(i_vi2_vp) = vy
    Vp(i_pre_vp) = P
    Vp(i_son_vp) = b
    Vp(i_tem_vp) = T__rho_c(rho, b)
    Vp(i_eps_vp) = EOSEpsilon(Vp) 

  END FUNCTION Exact_Ringleb_sol  
  !=============================

  !=========================================
  function V_from_xy_fpitr(x, y, Vmin, Vmax)
  !=========================================

    implicit none
  
    real, parameter :: fifth=0.2, half=0.5
    real, parameter :: one=1.0, two=2.0, three=3.0, five=5.0
    !Input and output
    real, intent(in) :: x, y, Vmin, Vmax ! Iput
    real             :: V_from_xy_fpitr  ! Output
    !Local variables
    integer  :: i, imax
    real :: b, rho,L, V, VP, tol

    imax = 500
    tol = 1.0e-15

    !Initial guess
    Vp = half*( Vmin + Vmax )

    !Start iterations
    fp_itertions : do i=1, imax

       Vp = MIN(Vp, Vmax)

       b  = sqrt(one - fifth*Vp*Vp)
       rho = b**five
       L  =   one/b + one/(three*b**three) + one/(five*b**five) &
            - half*log((one+b)/(one-b))
       V  = sqrt(one/sqrt( abs((x-half*L)**two + y**two) )/two/rho) ! Eq.(7.11.76)

       if (abs(V-Vp) < tol) exit
       if (i == imax) then
          write(*,*) "did not converge... Sorry.", i
          stop
       endif
       
       Vp = V

    end do fp_itertions

    V_from_xy_fpitr = V

  end function V_from_xy_fpitr
  !============================

END MODULE Ringleb_flow
