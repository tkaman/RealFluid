MODULE mmg3d

  USE LesTypes 

  IMPLICIT NONE

CONTAINS 

  !===============================================
  SUBROUTINE mmg3d_infile(Data, Mesh, Var, PbName)
  !===============================================

    IMPLICIT NONE
        
    TYPE(Donnees),       INTENT(IN)    :: Data
    TYPE(Maillage),      INTENT(INOUT) :: Mesh
    TYPE(Variables),     INTENT(IN)    :: Var
    CHARACTER(LEN = 70), INTENT(IN)    :: PbName
    !----------------------------------------------

    CHARACTER(LEN = 75)  :: Mesh_file, Sol_file
    CHARACTER(LEN = 70)  :: tmp, tmp_, p_crt, Mesh_out
    CHARACTER(LEN = 200) :: lunch

    INTEGER             :: Nordre, istat, ivar, id, &
                           is, jt, k , js, N_nodes
    INTEGER             :: Unit_M, Unit_S, Unit_a
    !----------------------------------------------
    REAL   , DIMENSION(:,:), POINTER :: Vp
    REAL   , DIMENSION(:,:), POINTER :: Coor
    INTEGER, DIMENSION(:)  , POINTER :: Nu
    !----------------------------------------------

    REAL, DIMENSION(3) :: ww

    REAL :: p_eps, p_hmin, p_hmax
    INTEGER :: p_Cycle
    REAL :: SS

    INTEGER :: nt, ns, nn, polyType, N_Pyr, N_dofs
    INTEGER, DIMENSION(2) :: NbOfPolys, Degre
    
    INTEGER, DIMENSION(:), ALLOCATABLE :: Nv

    INTEGER, DIMENSION(:), POINTER :: Glob2Loc
    !-----------------------------------------------

    Nordre = Data%Nordre

    IF( Data%Ndim /= 3 ) THEN
       WRITE(*,*) 'ERROR: only 3D mesh allowed'
    ENDIF

    IF( Data%Nordre /= 2 ) THEN
       WRITE(*,*) 'ERROR: only 2nd mesh order allowed'
    ENDIF

    Unit_M = 11; Unit_S = 12; Unit_a = 15

    nt = Mesh%Nelement

    OPEN(UNIT = Unit_a, file='Adapt.data', ACTION = 'READ', IOSTAT = istat )
    IF (istat /= 0) THEN
       WRITE(*,*) 'ERROR opening Adapt.data'
       STOP
    END IF

    READ(Unit_a, *) p_eps
    READ(Unit_a, *) p_crt
    READ(Unit_a, *) p_hmin
    READ(Unit_a, *) p_hmax
    READ(Unit_a, *) p_Cycle
    READ(Unit_a, *) Mesh_out

    Mesh_file(1: ) = TRIM(ADJUSTL(PbName)) // ".mesh"
     SOL_file(1: ) = TRIM(ADJUSTL(PbName)) // ".sol"

    OPEN(UNIT = Unit_M, FILE = TRIM(Mesh_file), IOSTAT = istat, STATUS = "REPLACE" )
    IF (istat /= 0) THEN
       WRITE(*,*) 'ERROR opening TRIM(Mesh_file)'
       STOP
    END IF

    OPEN(UNIT = Unit_S, FILE = TRIM(Sol_file), IOSTAT = istat, STATUS = "REPLACE" )
    IF (istat /= 0) THEN
       WRITE(*,*) 'ERROR opening TRIM(Sol_file)'
       STOP
    END IF
    
    REWIND(Unit_M); REWIND(Unit_S)

    WRITE(Unit_M, '("MeshVersionFormatted 2")')
    WRITE(Unit_M, *)

    WRITE(Unit_M, '("Dimension", I2)') Data%Ndim
    WRITE(Unit_M, *)

    WRITE(Unit_M, '("Vertices")')

    WRITE(Unit_S, '("MeshVersionFormatted")')
    WRITE(Unit_S, '("2")')
    WRITE(Unit_S, *)

    WRITE(Unit_S, '("Dimension")')
    WRITE(Unit_S,'(I2)') Data%Ndim
    WRITE(Unit_S, *)
    
    WRITE(Unit_S, '("SolAtVertices")')
    
    Degre(1) = MINVAL(Mesh%maill(:)%Ndegre)
    Degre(2) = MAXVAL(Mesh%maill(:)%Ndegre)

    ! NbOfPolys(1) == nb of triangles/Tetra
    ! NbOfPolys(2) == nb of quadrangles/Hexa
    NbOfPolys = 0
    N_Pyr = 0

    DO jt = 1, Mesh%Nelement

       SELECT CASE(Data%Ndim)

!!$       CASE(2)
!!$
!!$          SELECT CASE(Mesh%Maill(jt)%Nsommets)
!!$          CASE(3) ! Triangles P1
!!$             NbOfPolys(1) = NbOfPolys(1) + 1
!!$          CASE(4) ! Quadrangles P1
!!$             NbOfPolys(2) = NbOfPolys(2) + 1
!!$          CASE DEFAULT
!!$            WRITE(*,*) 'ERROR: element type'
!!$            STOP
!!$          END SELECT

       CASE(3)
          
          SELECT CASE(Mesh%Maill(jt)%Nsommets)

          CASE(4) ! Tétrahèdres P1
             NbOfPolys(1) = NbOfPolys(1) + 1

!!$          CASE(8) ! Hexahedres P1
!!$             NbOfPolys(2) = NbOfPolys(2) + 1
!!$          CASE(5) ! Pyramid P1
!!$             NbOfPolys(2) = NbOfPolys(2) + 1
!!$             N_Pyr = N_Pyr + 1

          CASE DEFAULT
             WRITE(*,*) 'ERROR: element type'
             STOP
          END SELECT

       END SELECT
       
    END DO

    N_dofs = Var%Ncells

    WRITE(tmp, '(I9)') N_dofs

    WRITE(Unit_M, *) TRIM(ADJUSTL(tmp))
    WRITE(Unit_S, *) TRIM(ADJUSTL(tmp))
    WRITE(Unit_S, *)
    WRITE(Unit_S, '("1 1")')

    ALLOCATE(        Glob2Loc(N_dofs) )
    ALLOCATE( coor(Data%Ndim, N_dofs) )

    N_pyr = 0
    nn = Var%Ncells

    DO jt = 1, Mesh%Nelement

       DO k = 1, Mesh%Maill(jt)%Nsommets
          coor(:, Mesh%Maill(jt)%Nu(k)) = Mesh%Maill(jt)%coor(:,k)           
       ENDDO

    ENDDO
   
    DO polyType = 1, 2

       IF (NbOfPolys(polyType) > 0) THEN

          SELECT CASE(polyType)

          CASE(1)
             IF (Data%Ndim==2) THEN
                WRITE(*,*) 'traitement des triangles'
             ELSE
                WRITE(*,*) 'traitement des tetrahedres'
             END IF
          CASE(2)
             IF (Data%Ndim==2) THEN
                WRITE(*,*) 'traitement des quadrangles'
             ELSE
                WRITE(*,*) 'traitement des hexahedres'
             END IF
          CASE DEFAULT
                WRITE(*,*) 'Element inconnu'
             CALL delegate_stop()
          END SELECT

          ! Compte le nombre de sommets locaux (les sommets des tri/tetra) et les ordonne
          Glob2Loc = 0; ns = 0

          DO jt = 1, Mesh%Nelement

             ! Identifie les elements qui nous interessent
             IF ( Mesh%Maill(jt)%NDegre == Degre(polyType)) THEN

                ! Parcours des sommets de l'element
                DO k = 1,  Mesh%Maill(jt)%Nsommets

                   js = Mesh%Maill(jt)%Nu(k)

                   ! Si on ne connait pas le sommet on l'ajoute a notre tableau
                   IF ( Glob2Loc( js ) == 0 ) THEN
                      ns = ns + 1 
                      Glob2Loc( js ) = ns
                   END IF

                END DO

             END IF
             
          END DO

          ns = 0 
          DO is = 1, N_dofs ! ecriture coord des noeuds

             IF ( Glob2Loc(is) > 0) THEN
                ns = ns + 1
                Glob2Loc(is) = ns

                tmp  = ""

                DO id = 1, Data%Ndim

                   tmp_ = ""

                   WRITE(tmp_, '(F24.12)') Coor(id, is)

                   tmp(1 :) = TRIM(ADJUSTL(tmp)) // " " // TRIM(ADJUSTL(tmp_))

                ENDDO

                tmp(1 :) =  TRIM(ADJUSTL(tmp)) // " 0"

                WRITE(Unit_M, *) TRIM(ADJUSTL(tmp))

                ! Adaptation criterion
                !----------------------------------------------
                IF( TRIM(ADJUSTL(p_crt)) == "vorticity" ) THEN

                   ww = Vorticity_vector( Var%Vp(:,is),      &
                                          Var%D_Ua(:, :, is) )
                
                   SS = SQRT( SUM(ww**2) )

                ELSEIF( TRIM(ADJUSTL(p_crt)) == "mach" ) THEN

                   SS = SQRT(SUM(Var%Vp(2:Data%Ndim+1, is)**2)) / &
                        Var%Vp(Data%NvarPhy-1, is)

                ELSE
                   
                   WRITE(*,*) 'ERROR: unknown adaptation criterion...'
                   WRITE(*,*) TRIM(ADJUSTL(p_crt))
                   STOP

                ENDIF
                !----------------------------------------------

                WRITE(tmp_, '(F24.12)') SS

                WRITE(Unit_S, *) TRIM(ADJUSTL(tmp_))
                
             END IF
             
          END DO

          WRITE(Unit_M, *)
          WRITE(Unit_M, '("Triangles")')

          WRITE(tmp, '(I9)') Mesh%NFacFr
          WRITE(Unit_M, *) TRIM(ADJUSTL(tmp))

          DO jt = 1, Mesh%NFacFr

             Nu => Mesh%fr(jt)%Nu(:)

             nn = SIZE(Nu)

             tmp  = ""

             DO k = 1, nn

                tmp_ = ""

                WRITE(tmp_, '(I6)')  Glob2Loc(Nu(k))

                tmp(1 :) = TRIM(ADJUSTL(tmp)) // " " // TRIM(ADJUSTL(tmp_))

             ENDDO

             WRITE(tmp_, '(I9)')  Mesh%fr(jt)%inum

             
             tmp(1 :) =  TRIM(ADJUSTL(tmp)) // " " // TRIM(ADJUSTL(tmp_))

             WRITE(Unit_M, *) TRIM(ADJUSTL(tmp)) 

          ENDDO

          WRITE(Unit_M, *)
          WRITE(Unit_M, '("Tetrahedra")')

          WRITE(tmp, '(I9)') Mesh%Nelement
          WRITE(Unit_M, *) TRIM(ADJUSTL(tmp))

          DO jt = 1, Mesh%Nelement! ecriture connectivites
             IF (Mesh%Maill(jt)%Ndegre == Degre(polyType)) THEN

                Nu => Mesh%Maill(jt)%Nu(:)

                nn = SIZE(Nu)

                tmp  = ""

                DO k = 1, nn

                   tmp_ = ""

                   WRITE(tmp_, '(I6)')  Glob2Loc(Nu(k))

                   tmp(1 :) = TRIM(ADJUSTL(tmp)) // " " // TRIM(ADJUSTL(tmp_))

                ENDDO

                tmp(1 :) =  TRIM(ADJUSTL(tmp)) // " 0"
                WRITE(Unit_M, *) TRIM(ADJUSTL(tmp))          

             END IF

          END DO

       END IF
    END DO
    
    WRITE(Unit_M, *)

    DEALLOCATE(Glob2Loc, coor)

    WRITE(Unit_M, *)
    WRITE(Unit_M, '("END")')

    WRITE(Unit_S, *)
    WRITE(Unit_S, '("END")')

    CLOSE(Unit_M)
    CLOSE(Unit_S)

    OPEN(2, file='jj.sh', ACTION = 'WRITE', IOSTAT = istat )

    WRITE(tmp_, '(I2)') p_Cycle

    WRITE(2, *) "#!/bin/bash"
    WRITE(2, *) "n=0"
    WRITE(2, *) "while [ $n -le" // " " // TRIM(ADJUSTL(tmp_)) // " ]" 
    WRITE(2, *) "do"
    WRITE(2, *) "   if [ ! -d Adapt_$n ]"
    WRITE(2, *) "   then"
    WRITE(2, *) "   mkdir Adapt_$n"
    WRITE(2, *) "   chmod ugo+wr Adapt_$n"
    WRITE(2, *) "   break"
    WRITE(2, *) "   fi"
    WRITE(2, *) "  n=$(( $n + 1 ))"
    WRITE(2, *) "done"
    WRITE(2, *) "cp * Adapt_$n"
    CLOSE(2)

    CALL SYSTEM('chmod 777 jj.sh')    
    CALL SYSTEM('./jj.sh')


    lunch = ""

    lunch(1 :) = "$TFB/../trunk/UTILS/mshmet"
    lunch(1 :) = TRIM(ADJUSTL(lunch))  // " " // TRIM(ADJUSTL(PbName))
    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " " // " temp.sol"

    lunch(1 :) = TRIM(ADJUSTL(lunch))  // " " // " -eps"
    tmp_ = ""
    WRITE(tmp_, '(F13.6)') p_eps    
    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " " // TRIM(ADJUSTL(tmp_))

    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " -hmin"
    tmp_ = ""
    WRITE(tmp_, '(F13.6)') p_hmin
    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " " // TRIM(ADJUSTL(tmp_))
    
    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " -hmax"    
    tmp_ = ""
    WRITE(tmp_, '(F13.6)') p_hmax
    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " " // TRIM(ADJUSTL(tmp_))
    
    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " -iso"   

    CLOSE(Unit_a)

    WRITE(*, *) TRIM(ADJUSTL(lunch))

    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " > log_mshmet"

    CALL SYSTEM(lunch)
  
    tmp = ""
    tmp = "mv temp.sol" //  " " // TRIM(ADJUSTL(SOL_file))

    CALL SYSTEM(tmp)

    lunch = ""
    lunch(1 :) = "$TFB/../trunk/UTILS/mmg3d4.0 -O 1 " &
                  // " -in " // TRIM(ADJUSTL(PbName)) &
                  // " -out  temp.mesh "

    WRITE(*, *) TRIM(ADJUSTL(lunch))

    WRITE(*, *) 'Adapting mesh...'

    lunch(1 :) = TRIM(ADJUSTL(lunch)) // " > log_mmg3d"

    CALL SYSTEM(lunch)

    WRITE(*,*) '...done'

    OPEN(UNIT = Unit_M, file='temp.mesh', ACTION = 'READ', IOSTAT = istat )
    IF (istat /= 0) THEN
       WRITE(*,*) 'ERROR opening temp.mesh'
       STOP
    END IF

    OPEN(UNIT = Unit_a, file=TRIM(ADJUSTL(Mesh_out)), ACTION = 'WRITE', IOSTAT = istat )
    IF (istat /= 0) THEN
       WRITE(*,*) 'ERROR opening ', TRIM(ADJUSTL(Mesh_out))
       STOP
    END IF

    CALL meshTOgmsh(Unit_M, Unit_a, N_nodes)

    CLOSE(Unit_M)
    CLOSE(Unit_a)

    CALL SYSTEM('rm *.mesh *.sol *.save')

  END SUBROUTINE mmg3d_infile
  !==========================
  
  !=============================================
  SUBROUTINE meshTOgmsh(Unit_M, Unit_G, N_nodes)
  !=============================================

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: Unit_M
    INTEGER, INTENT(IN) :: Unit_G

    INTEGER, INTENT(OUT) :: N_nodes

    INTEGER, DIMENSION(:,:), ALLOCATABLE :: face
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: ele

    REAL(KIND=8) :: x, y, z

    INTEGER :: i, k, n, N_dofs, &
               N_ele, N_face, ierror, dummy
    !--------------------------------------

    READ(Unit_M, *);  READ(Unit_M, *)
    READ(Unit_M, *);  READ(Unit_M, *)
    READ(Unit_M, *);  READ(Unit_M, *); READ(Unit_M, *)

    WRITE(Unit_G, '("$MeshFormat")') 
    WRITE(Unit_G, '("2.2 0 8")')
    WRITE(Unit_G, '("$EndMeshFormat")')
    WRITE(Unit_G, '("$Nodes")')

    READ(Unit_M, *) N_dofs
    WRITE(Unit_G, *) N_dofs

    N_nodes = N_dofs

    DO i = 1, N_dofs
     
       READ(Unit_M, *) x, y, z, n

       WRITE(Unit_G, '(I9, 3F20.12)') i, x, y, z

    ENDDO
    WRITE(Unit_G, '("$EndNodes")')

    READ(Unit_M, *); READ(Unit_M, *);  READ(Unit_M, *)
    READ(Unit_M, *) N_face

    ALLOCATE( face(4, N_face) )

    DO i = 1, N_face
       READ(Unit_M, *) face(:, i)
    ENDDO

    READ(Unit_M, *); READ(Unit_M, *); READ(Unit_M, *)
    READ(Unit_M, *) N_ele

    ALLOCATE( ele(4, N_ele) )

    DO i = 1, N_ele
       READ(Unit_M, *) ele(:, i), dummy
    ENDDO

    WRITE(Unit_G, '("$Elements")')
    WRITE(Unit_G, *) N_ele + N_face

    n = 0
    DO i = 1, N_ele
       n = n + 1
       WRITE(Unit_G, '(I9, 5I2,  4I9 )') n, 4, 3, 0, 0, 0, ele(:, i)
    ENDDO
    DO i = 1, N_face
       n = n + 1
       WRITE(Unit_G, '(I9, 3I2, I4, I2, 3I9)') n, 2, 3, &
                                               face(4, i), face(4, i)*100, 0, face(1:3, i)
    ENDDO

    WRITE(Unit_G, '("$EndElements")')

    DEALLOCATE( ele, face )

  END SUBROUTINE meshTOgmsh
  !========================

  !============================================
  FUNCTION Vorticity_vector(Vp, G_Vc) Result(w)
  !============================================
  !
  ! Compute the components of the vorticity vector
  ! w(3) = w_z, w(2) = w_y, w(1) = w_x
  !
  ! In 2D, only w_z /= 0
  !
  ! Variables ordering
  ! Vp: rho,u,v,w;   G_Vc: G_rho,G_mx,G_my,G_mz
    
    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_Vc

    REAL, DIMENSION(3) :: w
    !----------------------------------------

    INTEGER, DIMENSION(3), PARAMETER :: perm = (/3, 1, 2/)

    REAL :: dui_dxj, duj_dxi

    INTEGER :: i, j, k, N_dim
    !----------------------------------------

    N_dim = SIZE(G_Vc, 1)

    w = 0.0

    DO k = 3, 1, -1

       i = perm(k); j= perm(perm(k))

       dui_dxj = (G_Vc(j, i+1) - Vp(i+1)*G_Vc(j, 1)) / Vp(1)
       duj_dxi = (G_Vc(i, j+1) - Vp(j+1)*G_Vc(i, 1)) / Vp(1)

       w(k) = dui_dxj - duj_dxi

       IF(N_dim == 2) EXIT
     
    ENDDO    
    
  END FUNCTION Vorticity_vector  
  !============================

END MODULE mmg3d

