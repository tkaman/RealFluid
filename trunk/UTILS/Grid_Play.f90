MODULE Grid_Play

  USE LesTypes
  USE GeomGraph

  IMPLICIT NONE

CONTAINS

  !===========================
  SUBROUTINE find_bfaces(Mesh)
  !===========================

    IMPLICIT NONE 

    TYPE(Meshdef), INTENT(INOUT) :: Mesh

    REAL, DIMENSION(3) :: GG
    REAL :: x_s, x_e, x, y, z

    REAL(KINd=8), PARAMETER :: PI = DACOS(-1.d0)

    INTEGER, DIMENSION(4) :: nu_face
    INTEGER, DIMENSION(6) :: NU

    INTEGER, DIMENSION(4, 3), PARAMETER :: F_TET_P1 = &
                          RESHAPE( (/ 2, 1, 4, 2, &
                                      3, 4, 1, 1, &
                                      4, 3, 2, 3 /), (/4, 3/))

    INTEGER, DIMENSION(4, 6), PARAMETER :: F_TET_P2 = &
                          RESHAPE( (/ 2, 1, 4,  2, &
                                      3, 4, 1,  1, &
                                      4, 3, 2,  3, &
                                      6, 8, 8,  5, &
                                      9, 9, 5,  7, & 
                                     10, 7, 10, 6 /), (/4, 6/))

    CHARACTER(len=64) :: out_file
    INTEGER :: je, i, k, is, c_ele, n_ele_b, bc, l
    INTEGER :: ierror
    !--------------------------------------

    OPEN(15, FILE = 'temp', ACTION = 'WRITE', IOSTAT = ierror)
    IF(ierror /= 0) THEN
       WRITE(*,*) 'ERROR: cannnot open the output file'
       STOP
    ENDIF

    n_ele_b = 0

    DO je = 1, Mesh%Nelemt

       nu_face = Mesh%NuFacette(:, je)

       DO k = 1, SIZE(nu_face)

          ! Adjacent element with share the same face             
          IF( Mesh%EleVois(1, nu_face(k)) == je ) THEN
             c_ele = Mesh%EleVois(2, nu_face(k))
          ELSE
             c_ele = Mesh%EleVois(1, nu_face(k))
          ENDIF

          ! Boundary face
          IF( c_ele == 0 ) THEN

             n_ele_b = n_ele_b + 1

             GG(1) = SUM( Mesh%coor(1,  Mesh%Nu(F_Tet_P1(k, :), je)) ) / 3.d0
             GG(2) = SUM( Mesh%coor(2,  Mesh%Nu(F_Tet_P1(k, :), je)) ) / 3.d0
             GG(3) = SUM( Mesh%coor(3,  Mesh%Nu(F_Tet_P1(k, :), je)) ) / 3.d0


             bc = 1
             IF( SQRT(SUM(GG**2)) > 10.d0 ) bc = 2 ! FAR FIELD
             IF( ABS(GG(2)) < 1.0E-9 ) bc = 3 ! SYMMETRY PLANE

!!$             l = 0
!!$             DO is = 1, 3
!!$
!!$                x = Mesh%coor(1,  Mesh%Nu(F_Tet(k, is), je))
!!$                y = Mesh%coor(2,  Mesh%Nu(F_Tet(k, is), je))
!!$                z = Mesh%coor(3,  Mesh%Nu(F_Tet(k, is), je))
!!$
!!$                x_s = 0.3d0 * SIN(PI * y)**4
!!$                x_e = 1.5d0 + 0.3d0 * SIN(PI * y)**4
!!$               
!!$                IF( x >= x_s .AND. x <= x_e ) l = l + 1
!!$
!!$             ENDDO
!!$
!!$             IF(     GG(1) == -25.d0 ) THEN
!!$                bc = 1 ! INFLOW
!!$             ELSEIF( GG(1) == 26.5d0 ) THEN
!!$                bc = 2 ! OUTFLOW
!!$             ELSEIF( l == 3 .AND. &                    
!!$                  GG(2) > -1.d0 .AND. GG(2) <  0.0d0 .AND. &
!!$                  GG(3) <  1.d0) THEN
!!$                bc = 3 ! WALLBUMP
!!$             ELSE
!!$                bc = 4 ! SYMMETRY
!!$             ENDIF
     
             WRITE(15, '(I10, 6I10, I3)') n_ele_b, Mesh%Nu( F_Tet_P2(k, :), je ), bc
             
          ENDIF

       ENDDO

    ENDDO
    CLOSE(15)

    WRITE(*,*) n_ele_b

    !------------------
    ! Write outpu mesh
    !-------------------------------------------------
    out_file = "new-mesh.msh"

    OPEN(15, FILE = TRIM(ADJUSTL(out_file)), ACTION = 'WRITE', IOSTAT = ierror)
  
    IF(ierror /= 0) THEN
       WRITE(*,*) 'ERROR: cannnot open the output file'
       STOP
    ENDIF

    WRITE(15, '("$MeshFormat")') 
    WRITE(15, '("2.2 0 8")')
    WRITE(15, '("$EndMeshFormat")')
    WRITE(15, '("$Nodes")')
    
    WRITE(15, '(I9)') Mesh%Npoint
   
    DO i = 1, Mesh%Npoint
       WRITE(15, '(I9, 3E24.16)') i, Mesh%Coor(:, i)
    ENDDO

    WRITE(15, '("$EndNodes")')
    WRITE(15, '("$Elements")')
   
    WRITE(15, '(I10)') Mesh%Nelemt + N_ele_b
    
    DO i = 1, Mesh%Nelemt
       WRITE(15, '(I10, I3, 4I3, 10I10 )') i, 11, 3, 0, 0, 0, Mesh%Nu(:, i)
    ENDDO

    OPEN(25, FILE = 'temp', ACTION = 'READ', IOSTAT = ierror)
    IF(ierror /= 0) THEN
       WRITE(*,*) 'ERROR: cannnot open the temp file'
       STOP
    ENDIF

    DO i = 1, N_ele_b
       READ(25, *) is, NU, bc
!       write(*,*) is
       WRITE(15, '(I10, 5I3, 6I10 )') i+Mesh%Nelemt, 9, 3, bc, bc, 0, NU
    ENDDO

    WRITE(15, '("$EndElements")')
    CLOSE(15)
    CLOSE(25)

  END SUBROUTINE find_bfaces
 !=========================

  !==========================
  SUBROUTINE write_gmsh(Mesh)
  !==========================

    IMPLICIT NONE 

    TYPE(Meshdef), INTENT(INOUT) :: Mesh

    CHARACTER(len=64) :: out_file
    INTEGER :: i, ierror

    out_file = "P2-mesh.msh"

    OPEN(15, FILE = TRIM(ADJUSTL(out_file)), ACTION = 'WRITE', IOSTAT = ierror)
  
    IF(ierror /= 0) THEN
       WRITE(*,*) 'ERROR: cannnot open the output file'
       STOP
    ENDIF

    WRITE(15, '("$MeshFormat")') 
    WRITE(15, '("2.2 0 8")')
    WRITE(15, '("$EndMeshFormat")')
    WRITE(15, '("$Nodes")')

    WRITE(15, '(I9)') SIZE( Mesh%Coor, 2)
    DO i = 1, SIZE( Mesh%Coor, 2)
       WRITE(15, '(I9, 3E24.16)') i, Mesh%Coor(:, i)
    ENDDO

    WRITE(15, '("$EndNodes")')
    WRITE(15, '("$Elements")')

    WRITE(15, '(I10)') Mesh%Nelemt
    DO i = 1, Mesh%Nelemt
       WRITE(15, '(I10, I3, 4I3, 10I10 )') i, 11, 3, 0, 0, 0, Mesh%Nu(:, i)
    ENDDO
    WRITE(15, '("$EndElements")')
    CLOSE(15)

  END SUBROUTINE write_gmsh
  !========================


  !======================
  SUBROUTINE check_mesh()
  !======================

    IMPLICIT NONE

    INTEGER :: n_ele, je, k, iq
    REAL :: tot_V

    tot_V = 0.d0

    DO je = 1, SIZE(d_element)

       WRITE(*, '("ELE: " I6, "  Vol: ", E12.6)') je, SUM(d_element(je)%e%w_q)

       DO k = 1, d_element(je)%e%N_points

          WRITE(*, '(I6, ": " 3(E13.6, ", "))') d_element(je)%e%Nu(k), d_element(je)%e%Coords(:, k)

       ENDDO

       WRITE(*,*) 'RD n'
       WRITE(*,*) '----------'
       DO k = 1, d_element(je)%e%N_verts
          WRITE(*, '(3(F12.6, ", "))' ) d_element(je)%e%rd_n(:, k)
       ENDDO

       tot_V = tot_v + SUM(d_element(je)%e%w_q)

       WRITE(*,*) 'Quadrature'
       write(*,*) '----------'     
       DO iq = 1, d_element(je)%e%N_quad
          
          write(*, '(3(F12.6, ", "))') d_element(je)%e%xx_q(:, iq)

       ENDDO

       WRITE(*,*) 'FACES'
   
       DO k = 1,  d_element(je)%e%N_Faces

          WRITE(*, '("G_f ", I6,  " C_E ", I6)' ) d_element(je)%e%Faces(k)%f%g_face, &
                                                  d_element(je)%e%Faces(k)%f%c_ele

          DO iq = 1, d_element(je)%e%Faces(k)%f%N_quad

              WRITE(*, '(6(F12.6, ", "))') d_element(je)%e%Faces(k)%f%xx_q(:, iq), &
                                           d_element(je)%e%Faces(k)%f%n_q(: , iq)
             
          ENDDO

          WRITE(*,*) '-----------------------'

       ENDDO
       
       WRITE(*,*)

    ENDDO

    WRITE(*, '("Volume ", F24.12)') tot_V

  END SUBROUTINE check_mesh
  !========================

  !=======================
  SUBROUTINE check_bmesh()
  !=======================

    IMPLICIT NONE

    INTEGER :: jf, k, iq

    REAL :: tot_S

    tot_S = 0.d0

    IF(SIZE(b_element) == 0) RETURN

    WRITE(1, '("$ DATA=VECTOR")')
    WRITE(1, '("% vscale = 0.1")')

    DO jf = 1, SIZE(b_element)

!       WRITE(*, '("b_FACE: " I6, I2, F12.6)') jf, b_element(jf)%b_f%bc_type, &
!                                              SUM(b_element(jf)%b_f%w_q)

       DO k = 1, b_element(jf)%b_f%N_points

!          WRITE(*, '(I6, ": " 2(F10.5))') b_element(jf)%b_f%Nu(k), b_element(jf)%b_f%Coords(:, k)

       ENDDO

       tot_S = tot_S + SUM(b_element(jf)%b_f%w_q)

!       WRITE(*,*) 'Normals'

       DO k = 1, b_element(jf)%b_f%N_points

          WRITE(1, '(6(F10.5))')  b_element(jf)%b_f%Coords(:, k), 0.0, &
                                  b_element(jf)%b_f%n_b(: , k), 0.0

       ENDDO

!       WRITE(*,*)

    END DO
    
    WRITE(1, '("$END")')

    STOP

!    WRITE(*, '("Surface ", F24.12)') tot_S

  END SUBROUTINE check_bmesh
  !=========================

  !===================================
  SUBROUTINE Jacobian_Sparsity(Mat_fb)
  !===================================

    IMPLICIT NONE

    TYPE(Matrice), INTENT(IN) :: Mat_fb
    !----------------------------------
    INTEGER, DIMENSION(:), ALLOCATABLE :: Id_col, col_dof
    INTEGER, DIMENSION(:), ALLOCATABLE :: col_ext      
    INTEGER, DIMENSION(:), ALLOCATABLE :: idx
      
    INTEGER :: Nvar, row_p, nnz

    INTEGER :: i, i_dof, j, k, row, l, &
               col_s, col_e, id_e, id_s, &
               N_syst, N_dof, nnz_dof
    !--------------------------------------

    N_dof = Mat_fb%NNin
    Nvar = Mat_fb%blksize

    write(1,*) 0, N_dof

    DO i_dof = 1, N_dof

       nnz_dof = Mat_fb%JPosi(i_dof+1) - Mat_fb%JPosi(i_dof)

       nnz = nnz_dof

       row = (i_dof - 1) + 1

       ALLOCATE( Id_col(nnz_dof), col_dof(nnz_dof) )         
       ALLOCATE( col_ext(nnz) ) 

       Id_col = (/ (i, i = Mat_fb%Jposi(i_dof), &
                           Mat_fb%Jposi(i_dof+1)-1) /)

       col_dof =  Mat_fb%Jvcell(Id_col)

       DO j = 1, SIZE(Id_col)

            col_s = (col_dof(j)-1) + 1
            col_e = col_s

            id_s = (j - 1) + 1
            id_e = id_s

            col_ext(id_s:id_e) = (/ (i, i = col_s, col_e) /)

         ENDDO

         row_p = row

         DO j = 1, SIZE(col_ext)
            write(1,*) row_p, col_ext(j)
         ENDDO

         DEALLOCATE( Id_col, col_dof, col_ext )

    ENDDO

    CALL Greddy_coloring(N_dof)

    DO j = 1, SIZE(ColorMap)

       DO i = 1, SIZE(ColorMap(j)%NU)
          k = j*10
          
          write(k, *) colorMap(j)%NU(i)
       ENDDO

    ENDDO

  END SUBROUTINE Jacobian_Sparsity
  !===============================  


END MODULE Grid_Play
