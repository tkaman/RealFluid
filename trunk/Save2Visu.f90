!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Mathématiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Université Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!>\bug : il faut adapter les IO en fonction de la nlle structure
   PROGRAM Save2Visu

      USE LesTypes 
      USE Reprise 
      USE Visu
      USE Inputs
      USE ReadMEsh
      USE MergeVisu
      USE mmg3d

      IMPLICIT NONE

      CHARACTER(LEN = *), PARAMETER :: mod_name = "Save2Visu"
      TYPE(Maillage)      :: Mesh
      TYPE(Variables)     :: Var
      !TYPE(Donnees)       :: data
      INTEGER             :: Stat, ind, iargc, i
      CHARACTER(LEN = 70) :: FileToConvert, type, type_, exec 
      CHARACTER(LEN = 2), DIMENSION(: ), ALLOCATABLE :: tab

      CALL info_ifdef()

      Stat = iargc()
      CALL getarg(0, exec)
      IF (Stat == 0) THEN
         CALL Infos()
      END IF
      IF (Stat < 2) THEN
          GO TO 999 
      END IF!-- va imprimer exec
      CALL getarg(1, type)

      IF (TRIM(type) == "merge") THEN

         IF (Stat < 3) THEN
            GO TO 999 
         END IF

         CALL getarg(2, type)
         CALL getarg(3, FileToConvert)

         CALL Moulinette(Data, Mesh, Var, FileToConvert) 

         type_ = "merge"

      ELSE

         CALL getarg(2, FileToConvert)

         ind = Index(FileToConvert, " ")
         FileToConvert(ind-5: ind-1) = Repeat(" ", 5)
         ind = Index(FileToConvert, "-")
         Data%RootName = FileToConvert(1: ind-1)

         CALL Reprise_visu(Data, Mesh, Var, FileToConvert)

      END IF

      ! Calcul des flux parietaux
      SELECT CASE(Trim(Adjustl(type)))

         CASE("Tecplot", "tecplot", "TECPLOT")
         !------------------------------------

            CALL VisuTecplotAscii(Data, Mesh, Var, FileToConvert )

         CASE("cp", "cf", "cx")
         !---------------------

            CALL VisuCx(Data, Mesh, Var, FileToConvert )

         CASE("save")
         !-----------

            IF( Trim(Adjustl(type_)) /= "merge") THEN
               WRITE(*,*) 'ERROR: creating aoutput'
               STOP
            ELSE
               CALL Reprise_Merge(Data, Mesh, Var, FileToConvert)
            ENDIF

         CASE("adapt")

            CALL mmg3d_infile(Data, Mesh, Var, FileToConvert)

         CASE("gmsh")

            CALL VisuGmsh(Data, Mesh, Var,  FileToConvert)

         CASE DEFAULT
            CALL Infos()
      END SELECT

      STOP "Fin du programme principal"

999 CONTINUE
      CALL Infos()
    CONTAINS

      SUBROUTINE Infos()
        PRINT *, Trim(exec), " prend 2 arguments OU PLUS (pour medit) : le type de visualisation, le fichier a convertir"
        PRINT *, "Le nom du fichier a convertir doit se terminer par .save (ou par .<qqch faisant 4 caracteres>)"
        PRINT *, "    et doit contenir un -, separateur du type de run et du pas de temps"
        PRINT *, "Les types de visualisation disponibles sont :"
        PRINT *, "  Tecplot, tecplot, TECPLOT"
        PRINT *, "  Cp"
        PRINT *, "  ... ou merge suivi d'un des types de visu precedent"
        CALL delegate_stop()
     END SUBROUTINE Infos
   END PROGRAM Save2Visu
