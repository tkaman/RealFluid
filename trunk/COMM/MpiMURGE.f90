!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga, M.Papin                                 */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  !!@author Module initialement Cod� par B. NKonga, Univ. Bx I, LaMAB
  !!
  !!@author Revisit� par M. Papin, Univ. Bx I, LaMAB le 05/09/2003
  !!@author Revisit� par M. Papin, INRIA, Univ. Bx I le 11/01/2006
  !!
  !! Role: interface parall�le MPI pour Fluids/FluidBox

#define FBXDEBUG PRINT *, mod_name ,":", 

   MODULE  LibComm
      USE Lestypes

#ifdef MPICH2
      USE mpi
#endif
      IMPLICIT NONE

      INTERFACE Reduce
         MODULE PROCEDURE VR_Reduce  !! Vecteur de real
         MODULE PROCEDURE VI_Reduce  !! Vecteur de integer
         MODULE PROCEDURE SR_Reduce  !! Simple real
         MODULE PROCEDURE SI_Reduce  !! simple integer
      END INTERFACE

#ifndef MPICH2
      INCLUDE "mpif.h"
#endif

      INTEGER, PRIVATE            :: StatInfo
      INTEGER, PRIVATE, SAVE      :: FluidBox_Comm = 0

  ! Variables locales tableaux
  !----------------------------
!--OFF      INTEGER, DIMENSION( mpi_status_size ) ::  Status

      INTEGER, PRIVATE, SAVE :: type_real, type_int = -100

      INTEGER, PRIVATE, SAVE :: MsgTag = 0

   CONTAINS

      SUBROUTINE stop_run(ident)
         CHARACTER(LEN=*), PARAMETER :: mod_name = "stop_run"
         INTEGER, INTENT(IN), OPTIONAL :: ident
         PRINT *, mod_name, "MPI_Abort va demarrer"
         IF (Present(ident)) THEN
            CALL delegate_stop(ident)
         ELSE
            CALL delegate_stop(-1)
         END IF
      END SUBROUTINE stop_run


    !     -----------------------------------------------
    !!     r�le : Phase d'initilisation des communications
    !!
    !!     les variables de la structure "Com" initialis�es dans le cas parall�le sont :
    !!
    !!         Ntasks    = nombre total de t�ches
    !!
    !!         MyId      = identification de la t�che courante
    !!
    !!         Me        =  num�ro de la t�che courante
    !!
    !!         GroupName = Non du groupe de la t�che courante
    !
    !     -----------------------------------------------
    !!@author     Boniface Nkonga      Universite de Bordeaux 1
    !     -----------------------------------------------
    !!
    !!@author     Modif M. PAPIN pour Fluids
    !!@author     Modif P.Jacq pour le couplage

      SUBROUTINE IniCom(Com)

         CHARACTER(LEN=*), PARAMETER :: mod_name = "IniCom"

    !--------------------------------------------------------------------
    !                 Sp�cifications et D�clarations
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------

         TYPE(MeshCom), INTENT(IN OUT)       :: Com

    ! Variables locales scalaires
    !----------------------------

         INTEGER             :: Me, MyTid, BarrierOpt, iodbar, NTasks
         INTEGER             :: Nspawn, Nprocs, itask
    !--------------------------------------------------------------------
    !                Partie ex�cutable de la proc�dure
    !--------------------------------------------------------------------

         BarrierOpt               = 0

    ! Enr�le dans MPI

         CALL mpi_init(StatInfo)

         IF (StatInfo /= 0) THEN
            PRINT *, mod_name, " : ERREUR : StatInfo(Mpi_init)==", StatInfo
            CALL stop_run()
         END IF

         ! P. JACQ : Cr�ation d'un communicateur priv� (important pour couplage)
         FluidBox_Comm = mpi_comm_world

         CALL mpi_comm_size(FluidBox_Comm, NTasks, StatInfo)
         CALL mpi_comm_rank(FluidBox_Comm, Me, StatInfo)
         MyTid = Me
         PRINT *, mod_name, " Me==", Me, " NTasks==", NTasks


         ! Remplissage de la structure "COM"

         !  Defines the direct and inverse mapping

         !if (ntasks==1) return

         ALLOCATE( Com%Tids(0: NTasks -1) )
         Com%Tids = 0
         ALLOCATE( Com%MeInGr(1: NTasks) )
         Com%MeInGr = 0
         ALLOCATE( Com%MapDom(1: NTasks) )
         ALLOCATE( Com%InvMap(0: NTasks-1) )
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "com%tids", SIZE(Com%Tids))
            CALL alloc_allocate(mod_name, "com%meingr", SIZE(Com%MeInGr))
            CALL alloc_allocate(mod_name, "com%mapdom", SIZE(Com%MapDom))
            CALL alloc_allocate(mod_name, "com%invmap", SIZE(Com%InvMap))
         END IF
         Com%MapDom      = (/ (itask - 1, itask=1, NTasks) /)
         Com%InvMap      = (/ (itask, itask=1, NTasks) /)
         Com%NTasks      = NTasks
         Com%MyTid       = MyTid
         Com%Me          = Me
         Com%Tids(Me)    = MyTid
         Com%Ngroup      =  1
         Com%MeInGr( 1 ) = Me

         Com%GroupName   = "Tous"
    !
    ! IENCOD = 0 : MPIDEFAULT
    !          1 : MPIRAW
    !          2 : MPIINPLACE
    !
         Com%iencod        = 1

         Com%Tcom          = 0.0
         Com%Twait         = 0.0
         Com%TechgSolu     = 0.0
         Com%TechgGrad     = 0.0
         Com%Treduce       = 0.0
         IF (KIND(1.0)==SELECTED_REAL_KIND(14)) THEN
            type_real = mpi_double_precision
         ELSE IF (KIND(1.0)==SELECTED_REAL_KIND(6)) THEN
            type_real = mpi_real
         ELSE
            PRINT *, mod_name, " : ERREUR : kind(1.0)==", KIND(1.0), " cas non pris en compte"
            CALL stop_run()
         END IF
         IF (KIND(1) ==SELECTED_INT_KIND(14) .OR.  KIND(1) ==SELECTED_INT_KIND(6)) THEN
            type_int  = mpi_integer
         END IF
         IF (type_int < 0) THEN
            PRINT *, mod_name, " : ERREUR : kind(1)==", KIND(1), " cas non pris en compte"
            CALL stop_run()
         END IF

      END SUBROUTINE IniCom

      SUBROUTINE bcast(un_entier)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "bcast"
         INTEGER, INTENT(IN) :: un_entier
         INTEGER :: pst

         CALL mpi_bcast(un_entier, 1, mpi_integer, 0, fluidbox_comm, pst)

         IF (pst < 0) THEN
            PRINT *, mod_name, ' ERREUR dans bcast : ', pst
            CALL stop_run()
         END IF

      END SUBROUTINE bcast

      SUBROUTINE  VR_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN = *), PARAMETER :: mod_name = "VR_Reduce"
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         INTEGER, INTENT(IN)                  ::  FCas
         REAL, DIMENSION(: ), INTENT(INOUT)   ::  VecToRdc
         REAL, DIMENSION(Size(VecToRdc))   :: buffer
         INTEGER   :: Length
         REAL      :: Temps_debut, Temps_final

    !
           IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()
    !
         Length           = SIZE(VecToRdc)

         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_real, mpi_min, FluidBox_Comm, StatInfo)
            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_real, mpi_max, FluidBox_Comm, StatInfo)
            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_real, mpi_sum, FluidBox_Comm, StatInfo)
            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans VR8 Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT
         VecToRdc  = buffer
    !
         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans VR8 MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF
    !
    !****************************************
         Temps_final     = CpuTime()
         Com%Treduce     = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE VR_Reduce


      SUBROUTINE  VI_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN=*), PARAMETER :: mod_name = "VI_Reduce"
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         INTEGER, INTENT(IN)      ::  FCas
         INTEGER, DIMENSION(: ), INTENT(IN OUT)   ::  VecToRdc
         INTEGER, DIMENSION(SIZE(VecToRdc)) ::  buffer
         INTEGER   ::  MsgType, Length
         REAL      :: Temps_debut, Temps_final
         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()
    !
         Length           = Size(VecToRdc)
         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_int, mpi_min, FluidBox_Comm, StatInfo)
            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_int, mpi_max, FluidBox_Comm, StatInfo)
            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, Length, type_int, mpi_sum, FluidBox_Comm, StatInfo)
            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans VI4 Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT
    !
         VecToRdc = buffer

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans VI4, MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF
    !****************************************
         Temps_final     = CpuTime()
         Com%Treduce      = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE VI_Reduce


      SUBROUTINE  SR_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN=*), PARAMETER :: mod_name = "SR_Reduce"
         TYPE(MeshCom), INTENT(IN OUT)   ::  Com
         INTEGER, INTENT(IN)      ::  FCas
         REAL, INTENT(IN OUT)   ::  VecToRdc
         REAL                                ::  buffer
         REAL      :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_real, mpi_min, FluidBox_Comm, StatInfo)
            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_real, mpi_max, FluidBox_Comm, StatInfo)
            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_real, mpi_sum, FluidBox_Comm, StatInfo)
            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans SR Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT

         VecToRdc = buffer

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans SR, MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF
    !
    !****************************************
         Temps_final      = CpuTime()
         Com%Treduce      = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE SR_Reduce

      SUBROUTINE  SI_Reduce(Com, FCas, VecToRdc )
         CHARACTER(LEN=*), PARAMETER :: mod_name = "SI_Reduce"
         TYPE(MeshCom), INTENT(IN OUT)  :: Com
         INTEGER, INTENT(IN)     ::  FCas
         INTEGER(KIND=4), INTENT(IN OUT)  ::  VecToRdc
         INTEGER(KIND=4) :: buffer
         REAL      :: Temps_debut, Temps_final
         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()
    !
         SELECT CASE( FCas )
            CASE(cs_parall_min)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_int, mpi_min, FluidBox_Comm, StatInfo)

            CASE(cs_parall_max)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_int, mpi_max, FluidBox_Comm, StatInfo)

            CASE(cs_parall_sum)
               CALL mpi_allreduce(VecToRdc, buffer, 1, type_int, mpi_sum, FluidBox_Comm, StatInfo)

            CASE DEFAULT
               PRINT *, mod_name, " ERREUR dans SI4 Reduce ", FCas, " non traite"
               CALL stop_run()
         END SELECT

         VecToRdc = buffer
    !
         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans SI4, MPI_REDUCE: ', StatInfo
            CALL stop_run()
         END IF

    !****************************************
         Temps_final      = CpuTime()
         Com%Treduce      = Com%Treduce + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE SI_Reduce

    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeInit(Com, DATA)
         CHARACTER(LEN=*), PARAMETER :: mod_name = "EchangeInit"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         TYPE(Donnees), INTENT(IN)      :: DATA
         INTEGER    :: MaxLengthRcv, MaxLengthSnd
         REAL       :: Temps_debut, Temps_final
         INTEGER    :: Nvar, Ndomv, Ndim

      END SUBROUTINE EchangeInit


    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeSolRecv( Com, Ua )
         CHARACTER(LEN=*), PARAMETER :: mod_name = "EchangeSolRecv"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         REAL, DIMENSION(:,   : ), INTENT(IN OUT)   :: Ua
    !REAL, DIMENSION(:, :), pointer   :: Ua
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: iv, ivv, j, is, ipos, Nvar, Domv, Ndomv
         REAL       :: Temps_debut, Temps_final
!    INTEGER, dimension(MPI_STATUS_SIZE, 1) :: Stat

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

         Ndomv   = Com%Ndomv
         Nvar    = SIZE(Ua, 1)

         CALL mpi_waitall(2*Ndomv, Com%Request, Com%Stat, StatInfo) ! On attend tout

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
!       DO ivv = 1, NdomV  ! On laisse tomber, ca fout le b... dans la continuit� m�moire
!          CALL MPI_WAITANY( NdomV, Com%Request(1:NdomV), iv, Stat, StatInfo )
                  DO  j = 1, Com%NPrcv2(iv)
                     is                   = Com%PTrcv2(j, iv)
                     ipos                 = (j-1)*Nvar + 1
                     Ua(1: Nvar, is)        = Com%RcvData(ipos: ipos+Nvar-1, iv)
                  END DO
               END DO
         END SELECT

         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
      END SUBROUTINE EchangeSolRecv

    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeSolPrepSend( Com, Ua )
         CHARACTER(LEN=*), PARAMETER :: mod_name = "EchangeSolPrepSend"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         REAL, DIMENSION(:,   : ), INTENT(IN OUT)   :: Ua
    !REAL, DIMENSION(:, :), pointer   :: Ua
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: iv, j, is, ipos
         INTEGER    :: Nvar, MyTid, Domv
         INTEGER    :: MsgLength, FromId, DestId, NumbMsg, Ndomv, RcvNode
         REAL       :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF

         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag=MsgTag+1

         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv
         Nvar    = SIZE(Ua, 1)

         Com%Request = mpi_request_null
    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            FromId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPrcv2(iv)  !-- Vertex centered !
            MsgLength = Nvar*NumbMsg
            CALL mpi_irecv( Com%RcvData(1, iv), MsgLength, type_real, &
                         &  FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO

    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv, 1, -1
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            DestId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPsnd2(iv)
            DO  j = 1, NumbMsg
               is                            = Com%PTsnd2(j, iv)
               ipos                          = (j-1)*Nvar + 1
               Com%SndData(ipos: ipos+Nvar-1, iv) = Ua(:,   is)
            END DO
            MsgLength = Nvar*NumbMsg
            CALL mpi_isend(Com%SndData(1, iv), MsgLength, type_real, DestId, MsgTag, FluidBox_Comm, Com%Request(Ndomv+iv), &
                          & StatInfo)
         END DO

         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
      END SUBROUTINE EchangeSolPrepSend


    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeSol( Com, Ua)
         CHARACTER(LEN=*), PARAMETER :: mod_name = "EchangeSol"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         REAL, DIMENSION(:,   : ), INTENT(IN OUT)   :: Ua
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: k, iv, j, is, ipos
         INTEGER    :: Nvar, MyTid, Domv
         INTEGER    :: MsgLength, FromId, DestId, NumbMsg, Ndomv, RcvNode
         REAL       :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag=MsgTag+1
    !
         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv
         Nvar    = SIZE(Ua, 1)

         Com%Request = mpi_request_null

    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            FromId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPrcv2(iv)  !-- Vertex centered !
            MsgLength = Nvar*NumbMsg
            CALL mpi_irecv( Com%RcvData(1, iv), MsgLength, type_real, &
                          &  FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO
    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv, 1, -1
            Domv  =  Com%Jdomv(iv)        ! Numero du (iv)ieme sous-domaine voisin
            DestId =  Com%MapDom(Domv)    ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPsnd2(iv)
            DO  j = 1, NumbMsg
               is                                   = Com%PTsnd2(j, iv)
               ipos                                 = (j-1)*Nvar + 1
               Com%SndData(ipos: ipos+Nvar-1, iv) = Ua(:,   is)
            END DO
            MsgLength = Nvar*NumbMsg
            CALL mpi_isend(Com%SndData(1, iv), MsgLength, type_real, DestId, &
                          &  MsgTag, FluidBox_Comm, Com%Request(Ndomv+iv), StatInfo)
         END DO

         CALL mpi_waitall(2*Ndomv, Com%Request, Com%Stat, StatInfo)

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
                  DO  j = 1, Com%NPrcv2(iv)
                     is                   = Com%PTrcv2(j, iv)
                     ipos                 = (j-1)*Nvar + 1
                     Ua(1: Nvar, is)        = Com%RcvData(ipos: ipos+Nvar-1, iv)
                  END DO
               END DO
         END SELECT
    !****************************************
         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE EchangeSol


    !     -----------------------------------------------
    !!@author     Pascal JACQ (adaptation de EchangeSol de Mikael Papin)
    !     -----------------------------------------------
      SUBROUTINE EchangeGrad( Com, Grad)
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom)       , INTENT(IN OUT)   :: Com
         REAL, DIMENSION(:,:,:), INTENT(IN OUT)   :: Grad
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: k, iv, j, is, ipos,i
         INTEGER    :: Nvar, MyTid, Domv, Dim
         INTEGER    :: MsgLength, FromId, DestId, NumbMsg, Ndomv, RcvNode
         REAL       :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) RETURN
         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag=MsgTag+1
    !
         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv
         Dim     = SIZE(Grad,1)
         Nvar    = SIZE(Grad,2)

         Com%Request = mpi_request_null

    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)       ! Numero du (iv)ieme sous-domaine voisin
            FromId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPrcv2(iv)  !-- Vertex centered !
            MsgLength = Dim*Nvar*NumbMsg
            CALL mpi_irecv( Com%RcvData(1, iv), MsgLength, type_real, &
            &  FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO
    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv,1,-1
            Domv  =  Com%Jdomv(iv)        ! Numero du (iv)ieme sous-domaine voisin
            DestId =  Com%MapDom(Domv)    ! Numero du processeur associe au  (iv)ieme sous-domaine
       ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
            NumbMsg   = Com%NPsnd2(iv)
            DO  j = 1, NumbMsg , 1
               is                                   = Com%PTsnd2(j,iv)
               ipos                                 = (j-1)*Nvar*Dim + 1
               DO i = 1,Nvar
                  Com%SndData(ipos:ipos+dim-1, iv) = Grad(1:dim,i,is)
                  ipos = ipos + dim
               END DO
            END DO
            MsgLength = Dim*Nvar*NumbMsg
            CALL mpi_isend(Com%SndData(1,iv), MsgLength , type_real, DestId,&
            &  MsgTag, FluidBox_Comm,Com%Request(Ndomv+iv),StatInfo)
         END DO

         CALL mpi_waitall(2*Ndomv, Com%Request,Com%Stat,StatInfo)

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
                  DO  j = 1, Com%NPrcv2(iv) , 1
                     is                   = Com%PTrcv2(j,iv)
                     ipos                 = (j-1)*Nvar*Dim + 1
                     DO i = 1,Nvar
                        Grad(1:dim,i,is) = Com%RcvData(ipos:ipos+Dim-1,iv)
                        ipos = ipos + dim
                     END DO
                  END DO
               END DO
         END SELECT
    !****************************************
         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE EchangeGrad


    !     -----------------------------------------------
    !!@author     Mikael Papin INRIA / Univ. de Bordeaux 1
    !     -----------------------------------------------
      SUBROUTINE EchangeVec( Com, Vec, DATA )
         CHARACTER(LEN=*), PARAMETER :: mod_name = "EchangeVec"
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         TYPE(Donnees), INTENT(IN)      :: DATA
         REAL, DIMENSION(: ), INTENT(IN OUT)   :: Vec
    ! Variables locales scalaires
    !----------------------------
         INTEGER    :: k, iv, j, is, ipos
         INTEGER    :: MyTid, Domv
         INTEGER    :: MsgLength, FromId, DestId, Ndomv, RcvNode
         REAL       :: Temps_debut, Temps_final
         INTEGER :: MaxLengthSnd, MaxLengthRcv
    ! Variables locales tableaux
    !----------------------------
         REAL, DIMENSION(:,   : ), ALLOCATABLE    :: SndData, RcvData
         INTEGER :: istat

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

    ! Special treatment flag to prevent overwritting of messages
         MsgTag=MsgTag+1
    !
         MyTid   = Com%MyTid
         Ndomv   = Com%Ndomv

         MaxLengthSnd = 0
         MaxLengthRcv = 0
         SELECT CASE (Data%sel_approche)
            CASE(cs_approche_vertexcentered)      !-- Vertex centered !
               MaxLengthSnd = Com%MaxLengthSnd2
               MaxLengthRcv = Com%MaxLengthRcv2
               Com%LNiveau  = 2
            CASE DEFAULT
               WRITE(6, *) mod_name, " ERREUR : L'approche ", Data%Approche, " n'est pas traitee"
               CALL stop_run()
         END SELECT

         IF (.FALSE.) THEN
            WRITE(7+MyTid, *) mod_name, " MaxLengthSnd==", MaxLengthSnd, Ndomv, MyTid
            WRITE(7+MyTid, *) mod_name, " MaxLengthRcv==", MaxLengthRcv, Ndomv, MyTid
         END IF

         ALLOCATE(SndData(MaxLengthSnd, Ndomv ), STAT=istat )
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot allocate SndData ", istat
            CALL stop_run()
         END IF
         ALLOCATE(RcvData(MaxLengthRcv, Ndomv ), STAT=istat )
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot allocate RcvData ", istat
            CALL stop_run()
         END IF
         IF (see_alloc) THEN
            CALL alloc_allocate(mod_name, "snddata", SIZE(SndData))
            CALL alloc_allocate(mod_name, "rcvdata", SIZE(RcvData))
         END IF
         Com%Request = mpi_request_null

    ! On pr�pare � recevoir
         DO iv = 1, Ndomv
            Domv  =  Com%Jdomv(iv)
            FromId =  Com%MapDom(Domv)
            MsgLength  = Com%NPrcv2(iv)  !-- Vertex centered !!!
            CALL mpi_irecv( RcvData(1, iv), MsgLength, type_real, FromId, MsgTag, FluidBox_Comm, Com%Request(iv), StatInfo )
         END DO

    ! On envoi les infos
!    DO iv = 1, NdomV
         DO iv = Ndomv, 1, -1
            Domv  =  Com%Jdomv(iv)
            DestId =  Com%MapDom(Domv)
            MsgLength = Com%NPsnd2(iv) !-- Vertex centered !!!
            DO  j = 1, MsgLength
               is             = Com%PTsnd2(j, iv)
               IF (is > SIZE(Vec)) THEN
                  PRINT *, mod_name, " : ERREUR : iv=", iv, "j=", j, "is=", is, "size(vec)=", SIZE(Vec)
                  CALL stop_run()
               END IF
               SndData(j, iv) = Vec(is)
            END DO
            CALL mpi_isend(SndData(1, iv), MsgLength, type_real, DestId, MsgTag, FluidBox_Comm, Com%Request(Ndomv+iv), StatInfo)
         END DO

         CALL mpi_waitall(2*Ndomv, Com%Request, Com%Stat, StatInfo)

         SELECT CASE (Com%LNiveau)
            CASE(2)
               DO iv = 1, Ndomv
!          CALL MPI_WAIT(Com%Request(iv), Status,  StatInfo)
                  DO  j = 1, Com%NPrcv2(iv)
                     is             = Com%PTrcv2(j, iv)
                     Vec(is)        = RcvData(j, iv)
                  END DO
               END DO
         END SELECT

         DEALLOCATE(SndData, STAT=istat)
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot deallocate SndData ", istat
            CALL stop_run()
         END IF
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "snddata")
         END IF
         DEALLOCATE(RcvData, STAT=istat)
         IF (istat /= 0) THEN
            PRINT *, mod_name, " :: ERREUR : Cannot deallocate SndData ", istat
            CALL stop_run()
         END IF
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "rcvdata")
         END IF
    !****************************************
         Temps_final     = CpuTime()
         Com%TechgSolu   = Com%TechgSolu + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE EchangeVec



    !       -------------------------------------------------
    !!--      Role: Synchronise tous les noeuds du standard central
    !       -----------------------------------------------

      SUBROUTINE WaitGroup(Com, GroupName)
         CHARACTER(LEN=*), PARAMETER :: mod_name = "WaitGroup"
         TYPE(MeshCom), INTENT(IN OUT)   :: Com
         CHARACTER(LEN=*), INTENT(IN)      :: GroupName
         REAL                        :: Temps_debut, Temps_final

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF
         Temps_debut = CpuTime()

         CALL mpi_barrier(FluidBox_Comm, StatInfo )

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_BARRIER: ', StatInfo
            CALL stop_run()
         END IF
    !****************************************
         Temps_final  = CpuTime()
         Com%Twait    = Com%Twait + Temps_final - Temps_debut
    !****************************************
      END SUBROUTINE WaitGroup


    !       -------------------------------------------------
    !!--      Role:
    !!--                   Synchronise tous les noeuds
    !!--                   du standard central et le Desactive
    !       -----------------------------------------------
      SUBROUTINE EndCom(Com, GroupName)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "EndCom"
         TYPE(MeshCom), INTENT(INOUT)   :: Com
         CHARACTER(LEN = *), INTENT(IN)      :: GroupName

         IF (Com%NTasks == 1) THEN
            RETURN
         END IF

         CALL mpi_barrier(FluidBox_Comm, StatInfo )

         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_BARRIER: ', StatInfo
            CALL stop_run()
         END IF

         DEALLOCATE(Com%SndData, Com%RcvData)
         DEALLOCATE(Com%Stat, Com%Request)
         IF (see_alloc) THEN
            CALL alloc_deallocate(mod_name, "com%snddata")
            CALL alloc_deallocate(mod_name, "com%rcvdata")
            CALL alloc_deallocate(mod_name, "com%stat")
            CALL alloc_deallocate(mod_name, "com%request")
         END IF

         CALL mpi_finalize(StatInfo)
         IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_FINALIZE: ', StatInfo
            CALL stop_run()
         END IF

      END SUBROUTINE EndCom

      REAL FUNCTION CpuTime()
         CHARACTER(LEN=*), PARAMETER :: mod_name = "CpuTime"
    !
    !       -------------------------------------------------
    !!      Role: remplacer la fonction intrins�que Cputime par la fonction MPI
    !!
    !       -----------------------------------------------
    !       -----------------------------------------------
    !!@author      Boniface Nkonga      Universite de Bordeaux 1
    !       -----------------------------------------------
    !--------------------------------------------------------------------
    !                 Specifications et Declarations
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------


    ! Variables locales scalaires
    !----------------------------
    !
         REAL, EXTERNAL    :: second, tsecnd
    !
    !       -----------------------
    !       debut de la procedure
    !       -----------------------
    !

    !  CpuTime = SECOND( )
    ! CpuTime = TSECND( )

         CpuTime = mpi_wtime()

      END FUNCTION CpuTime




      ! Create Global ".save" files : Gather on proc 0
    SUBROUTINE PrepGather2Visu(Com, DATA, localnodes, NbNodes)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "PrepGather2Visu"
      TYPE(MeshCom)         , INTENT( INOUT ) :: Com
      TYPE(Donnees)         , INTENT( IN )    :: DATA
      INTEGER, DIMENSION(:) , INTENT( IN )    :: localnodes
      INTEGER               , INTENT( IN )    :: NbNodes

      INTEGER , DIMENSION( : ) , ALLOCATABLE  :: echange
      INTEGER :: Domv, MsgLength, Tag  , MaxCom , StatInfo
      INTEGER , DIMENSION(mpi_status_size)   :: status

      
      ! Send the number of Local nodes to proc 0
      ALLOCATE( echange(Com%NTasks - 1) )
      echange = 0
      IF (Com%Me /= 0) THEN
         echange( Com%Me ) = size(localnodes)
      END IF
      CALL Reduce(Com, cs_parall_max, echange )


      ! ALLOCATE the communication's arrays
      IF (Com%Me == 0) THEN
         MaxCom = MAXVAL( echange )
         ALLOCATE( Com%MURGEGatherRcvNum(MaxCom,Com%NTasks - 1) )
         ALLOCATE( Com%MURGEGatherRcv(MaxCom * Data%NVarPhy, Com%NTasks - 1 ) )
         ALLOCATE( Com%MURGEGatherNbRcv(Com%NTasks - 1) )
         Com%MURGEGatherNbRcv = echange
      ELSE
         ALLOCATE( Com%MURGEGatherSnd( Nbnodes * Data%NVarPhy ) )
      END IF

      DEALLOCATE( echange )

      IF (Com%Me == 0) THEN
         ! Get the global numerotation of the local nodes for the other domains
         DO DomV = 1, Com%NTasks - 1
            MsgLength  = Com%MURGEGatherNbRcv( DomV )
            Tag = 1000 + DomV
            CALL mpi_recv( Com%MURGEGatherRcvNum(1, DomV), MsgLength, type_int, DomV, Tag, FluidBox_Comm, Status, StatInfo )
            FBXDEBUG minval( Com%MURGEGatherRcvNum(1:MsgLength, DomV)),maxval( Com%MURGEGatherRcvNum(1:MsgLength, DomV)),DomV
         END DO
      ELSE
         ! Send the global numerotation of the local nodes to proc 0
         MsgLength  = NbNodes
         Tag = 1000 + Com%Me
         FBXDEBUG Com%Me,minval(localnodes),maxval(localnodes),size(localnodes),NbNodes
         CALL mpi_send( localnodes, MsgLength, type_int, 0, Tag, FluidBox_Comm, StatInfo )
      END IF
      

    END SUBROUTINE PrepGather2Visu

    
    SUBROUTINE Gather2Visu(Com, DATA,Var, Mesh, SaveVar, SaveMesh)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "Gather2Visu"
      TYPE(MeshCom)         , INTENT( INOUT ) :: Com
      TYPE(Donnees)         , INTENT( IN )    :: DATA
      TYPE(Variables)       , INTENT( INOUT ) :: Var
      TYPE(MeshDef)         , INTENT( INOUT ) :: Mesh
      TYPE(Variables)       , INTENT( OUT   ) :: SaveVar
      TYPE(MeshDef)         , INTENT( OUT   ) :: SaveMesh

      INTEGER :: Domv, MsgLength, Statinfo
      INTEGER :: step , pos , ii , Tag
      INTEGER , DIMENSION(mpi_status_size)   :: status

      IF (Com%Me == 0 ) THEN
         ! Save Local Mesh and Var struct

         SaveMesh%Npoint = Mesh%Npoint
         SaveMesh%NCells = Mesh%NCells
         SaveMesh%Coon   => Mesh%Coon
         SaveMesh%Coor   => Mesh%Coor
         SaveMesh%NElemt = Mesh%NElemt
         SaveMesh%NFacFr = Mesh%NFacFr
         SaveMesh%LogFr => Mesh%LogFr
         SaveMesh%NsFacFr => Mesh%NsFacFr
         SaveMesh%Ndegre => Mesh%Ndegre
         SaveMesh%Nu => Mesh%Nu

         SaveVar%Ncells = Var%Ncells
         SaveVar%Vp     => Var%Vp


         !*** ***!
         ! Set Mesh and Var to match the global problem

         Mesh%Npoint = Mesh%GlobNpoint
         Mesh%NCells = Mesh%GlobNpoint !!--%%%%%%%%%%%
         Mesh%Nelemt = Mesh%GlobNelemt
         Mesh%NFacFr = Mesh%GlobNFacFr
         Mesh%Coon => Mesh%GlobCoon
         Mesh%Coor => Mesh%GlobCoon     !!--%%%%%%%%%%%
         Mesh%Nu => Mesh%GlobNu

         Mesh%NsFacFr => Mesh%GlobNsFacFr
         Mesh%Ndegre => Mesh%GlobNdegre
         Mesh%LogFr => Mesh%GlobLogFr
 

         Var%Ncells = Mesh%GlobNpoint  !!--%%%%%%%%%%%
         Var%Vp     => Var%GlobalVp

         !*** ***!

         ! Put the local data in Vp
         Var%Vp(:,Mesh%nodelist) = SaveVar%Vp

         step = Data%NVarPhy
         ! Gather data from the others domains
         DO DomV = 1, Com%NTasks - 1
            MsgLength  = Com%MURGEGatherNbRcv( DomV ) * step
            Tag = 10000 + DomV
            CALL mpi_recv( Com%MURGEGatherRcv(1, DomV), MsgLength, type_real, DomV, Tag, FluidBox_Comm, Status,StatInfo )
         END DO

         ! Put theses datas in Vp
         DO DomV = 1, Com%NTasks - 1
            pos = 1
            DO ii = 1, Com%MURGEGatherNbRcv(DomV)
!               print *,Com%MURGEGatherRcvNum(ii,DomV),ii,Com%MURGEGatherNbRcv(DomV),DomV,Com%NTasks
               Var%Vp(:, Com%MURGEGatherRcvNum(ii,DomV) ) = Com%MURGEGatherRcv( pos : pos+step-1 ,DomV)
               pos = pos + step
            END DO
         END DO

      ELSE
         ! Send the local data to proc 0
         step = Data%NVarPhy
         pos  = 1 
         DO ii = 1, Var%NCellsIn
            Com%MURGEGatherSnd( pos : pos+step-1 ) = Var%Vp(:,ii)
            pos = pos + step
         END DO
         MsgLength  = Var%NCellsIn * step
         Tag = 10000 + Com%Me
         CALL mpi_send( Com%MURGEGatherSnd, MsgLength, type_real, 0, Tag, FluidBox_Comm, StatInfo )
      END IF

    END SUBROUTINE Gather2Visu



    SUBROUTINE ReverseGather2Visu(Com,Var, Mesh, SaveVar, SaveMesh)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "ReverseGather2Visu"
      TYPE(MeshCom)         , INTENT( INOUT ) :: Com
      TYPE(Variables)       , INTENT( INOUT ) :: Var
      TYPE(MeshDef)         , INTENT( INOUT ) :: Mesh
      TYPE(Variables)       , INTENT( IN    ) :: SaveVar
      TYPE(MeshDef)         , INTENT( IN    ) :: SaveMesh

      IF (Com%Me == 0 ) THEN
         ! Give me my local data back!!!
         Mesh%Npoint  = SaveMesh%Npoint
         Mesh%NCells  = SaveMesh%NCells
         Mesh%Coon    => SaveMesh%Coon
         Mesh%Coor    => SaveMesh%Coor
         Mesh%NElemt  = SaveMesh%NElemt
         Mesh%NFacFr  = SaveMesh%NFacFr
         Mesh%LogFr   => SaveMesh%LogFr
         Mesh%NsFacFr => SaveMesh%NsFacFr
         Mesh%Ndegre  => SaveMesh%Ndegre
         Mesh%Nu      => SaveMesh%Nu

         Var%Ncells   = SaveVar%Ncells
         Var%Vp       => SaveVar%Vp
      END IF

    END SUBROUTINE ReverseGather2Visu


    SUBROUTINE CalculeCom(Com,DATA,Mesh,localnodes, NbNodes)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "CalculeCom"
      TYPE(MeshDef)         , INTENT( INOUT ) :: Mesh
      TYPE(MeshCom)         , INTENT( INOUT ) :: Com
      TYPE(Donnees)         , INTENT( INOUT ) :: DATA
      INTEGER, DIMENSION(:) , INTENT( IN )    :: localnodes
      INTEGER               , INTENT( IN )    :: NbNodes

      INTEGER , DIMENSION( NbNodes)           :: owner
      INTEGER , DIMENSION( : ) , ALLOCATABLE  :: localRecv
      INTEGER , DIMENSION( : ) , ALLOCATABLE  :: nbRecv
      INTEGER , DIMENSION( : ) , ALLOCATABLE  :: echange
      INTEGER , DIMENSION(:,:) , ALLOCATABLE  :: buffer
      INTEGER :: ii, jj , k, l, nboverlap,nbinterieur

      INTEGER :: NdomV, NVar,MaxLengthSnd ,MaxLengthRcv

      INTEGER :: Domv, FromId, MsgLength, Statinfo,DestId

      IF ( Com%Ntasks == 1 ) THEN
         RETURN
      END IF

      ! Qui a quel noeud (unicite on prend le plus petit numero)
      nbinterieur = SIZE(localnodes)

      owner = Com%Ntasks
      owner(localnodes) = Com%Me        
      CALL Reduce(Com, cs_parall_min, owner )

      ! qui a donc mes overlaps
      nbOverlap = SIZE(Mesh%overlap)
      ALLOCATE( localRecv(nbOverlap) )

      localRecv = owner(Mesh%overlap)
      ! On peut calculer les recv
      ALLOCATE(nbRecv(Com%Ntasks**2))
      nbRecv = 0
      DO ii=1, Com%Ntasks
         nbRecv(ii+(Com%Me*Com%Ntasks)) = COUNT(localRecv == (ii-1))
      END DO

      ! Tout les procs connaissent la taille de leurs echanges
      CALL Reduce(Com, cs_parall_sum, nbRecv )

      ! On a les receive des proc n sur me*ntasks+n, les sends vers n sur n*ntasks+me

      ! on alloue les structures
      ALLOCATE( echange(Com%Ntasks) )
      echange = nbRecv(Com%Me*Com%Ntasks+1:(Com%Me+1)*Com%Ntasks) + nbRecv(Com%Me+1::Com%Ntasks)

      NdomV= COUNT( echange /= 0 )
      Com%Ndomv = Ndomv
      Nvar    = Data%NvarPhy 

      MaxLengthSnd = MAXVAL(nbRecv(Com%Me+1::Com%Ntasks))
      MaxLengthRcv = MAXVAL(nbRecv(Com%Me*Com%Ntasks+1:(Com%Me+1)*Com%Ntasks))

      SELECT CASE (Data%sel_approche)
      CASE(cs_approche_vertexcentered)      !-- Vertex centered !
         Com%MaxLengthSnd2 = MaxLengthSnd
         Com%MaxLengthRcv2 = MaxLengthRcv 
         Com%LNiveau  = 2
      CASE DEFAULT
         WRITE(6, *) mod_name, " ERREUR : L'approche ", Data%Approche, " n'est pas traitee"
         CALL stop_run()
      END SELECT

      ALLOCATE(Buffer(MAX(MaxLengthRcv,MaxLengthSnd), Ndomv ) )

      ALLOCATE(Com%SndData(Mesh%Ndim *Nvar*MaxLengthSnd, Ndomv ) )
      ALLOCATE(Com%RcvData(Mesh%Ndim *Nvar*MaxLengthRcv, Ndomv ) )
      ALLOCATE(Com%Request(2*Ndomv), Com%Stat(mpi_status_size, 2*Ndomv))
      IF (see_alloc) THEN
         CALL alloc_allocate(mod_name, "com%snddata", SIZE(Com%SndData))
         CALL alloc_allocate(mod_name, "com%rcvdata", SIZE(Com%RcvData))
         CALL alloc_allocate(mod_name, "com%request", SIZE(Com%Request))
         CALL alloc_allocate(mod_name, "com%stat", SIZE(Com%Stat))
      END IF

      ALLOCATE(Com%NPrcv2(NdomV))
      ALLOCATE(Com%NPsnd2(NdomV))
      ALLOCATE(Com%PTrcv2(MaxLengthRcv,NdomV))
      ALLOCATE(Com%PTsnd2(MaxLengthSnd,NdomV))
      ALLOCATE(Com%JDomv(NdomV))

      Com%JdomV = 0

      jj = 1
      echange = nbRecv(Com%Me*Com%Ntasks+1:(Com%Me+1)*Com%Ntasks) + nbRecv(1+Com%Me::Com%Ntasks)
      DO ii = 1,Com%Ntasks
         IF (echange(ii) /= 0) THEN
            Com%JDomv(jj) = ii
            Com%NPrcv2(jj) = nbRecv(Com%Me*Com%Ntasks+ii)
            Com%NPsnd2(jj) = nbRecv(1+Com%Me+(ii-1)*Com%Ntasks)
            jj = jj + 1
         END IF
      ENDDO

      IF (jj /= NdomV + 1) THEN
         PRINT *, 'Abort domV',Com%Me,Ndomv,jj-1,echange
         CALL delegate_stop()
      END IF

      ! Calcul des pts de recv
      DO ii = 1,Com%NDomV
         jj = Com%JDomv(ii)
         k  = 1
         DO l = 1, nbOverlap
            IF (localRecv(l) == (jj-1)) THEN
               Com%PTrcv2(k,ii) = l + nbinterieur
               Buffer(k,ii)     = Mesh%Overlap(l)
               k = k + 1
            END IF
         END DO
      ENDDO

      MsgTag = 100

      ! prepare a recevoir
      DO ii = 1, Ndomv
         Domv  =  Com%Jdomv(ii)
         FromId =  Com%MapDom(Domv)
         MsgLength  = Com%NPsnd2(ii)  !-- Vertex centered !!!
         CALL mpi_irecv( Com%PTsnd2(1, ii), MsgLength, type_int, FromId, MsgTag, Fluidbox_comm, Com%Request(ii), StatInfo )
      END DO

      DO ii = Ndomv, 1, -1
         Domv  =  Com%Jdomv(ii)       ! Numero du (iv)ieme sous-domaine voisin
         DestId =  Com%MapDom(Domv)   ! Numero du processeur associe au  (iv)ieme sous-domaine
         ! Boucle sur les Sommets a transmettre au  (iv)ieme sous-domaine
         MsgLength = Com%NPrcv2(ii)
         CALL mpi_isend(Buffer(1, ii), MsgLength, type_int, DestId, MsgTag, Fluidbox_comm, Com%Request(Ndomv+ii), &
              & StatInfo)
      END DO

      CALL mpi_waitall(2*Ndomv, Com%Request, Com%Stat, StatInfo);


      ! Update des pts snd
      DO ii = 1,Com%NDomV
         DO l = 1, Com%NPsnd2(ii)
            DO k = 1, nbinterieur
               IF (localnodes(k) == Com%PTsnd2(l,ii)) THEN
                  Com%PTsnd2(l,ii) = k
                  EXIT
               END IF
            END DO
         END DO
      ENDDO

      ! On fait connaitre nos infos aux autres procs (recv local = send distant)

      ! Menage
      DEALLOCATE( localRecv ,nbRecv,Buffer,echange)

    END SUBROUTINE CalculeCom

    SUBROUTINE CleanCom(com)
      TYPE(MeshCom), INTENT( IN OUT)   :: Com
   
      DEALLOCATE(Com%JDomv)
      IF (Com%Me == 0) THEN
         DEALLOCATE(Com%MURGEGatherRcv)
         DEALLOCATE(Com%MURGEGatherNbRcv)
         DEALLOCATE(Com%MURGEGatherRcvNum)
      ELSE
         DEALLOCATE(Com%MURGEGatherSnd)
      END IF
      DEALLOCATE(Com%NPsnd2)
      DEALLOCATE(Com%NPrcv2)
      DEALLOCATE(Com%PTrcv2)
      DEALLOCATE(Com%PTsnd2)
      DEALLOCATE(Com%InvMap)
      DEALLOCATE(Com%MapDom)
      DEALLOCATE(Com%MeInGr)
      DEALLOCATE(Com%Tids)  

    END SUBROUTINE CleanCom
   END MODULE LibComm

