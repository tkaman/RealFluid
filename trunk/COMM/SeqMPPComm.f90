!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga, M.Papin                                 */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   MODULE  LibComm
      USE Lestypes
      IMPLICIT NONE
   CONTAINS
  !
      SUBROUTINE stop_run()
         CHARACTER(LEN = *), PARAMETER :: mod_name = "stop_run"
         CALL Abort() !-- un probl�me s'est produit, il faut arr�ter le job avec la pile d'appels si possible
      END SUBROUTINE stop_run


      SUBROUTINE IniCom(Com)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IniCom"
    !     -----------------------------------------------
    !!     role: Phase d'initilisation des communications
    !!
    !!     les variables de la structure "Com" initialis�es sont, dans le cas s�quentiel :
    !!
    !!          Ntasks    = nombre total de t�ches, � 1
    !          MyId      = identification de la tache courante
    !!
    !!          Me        =  numero de la t�che courante, � 0
    !          GroupName = Non du groupe de la tache courante
    !
    !     -----------------------------------------------
    !!@author     Boniface Nkonga      Universite de Bordeaux 1
    !     -----------------------------------------------
    !!@author     Modif M. PAPIN pour Fluids
    !--------------------------------------------------------------------
    !                 Specifications et Declarations
    !--------------------------------------------------------------------
    !--------------------------------------------------------------------
    ! Type et vocations des Variables d'appel
    !----------------------------------------
         TYPE(MeshCom), INTENT(INOUT)       :: Com
    ! Variables locales scalaires
    !----------------------------
         Com%Ntasks      = 1
         Com%Me          = 0
         ! P.J : Set to True when used with the moulinette to get the number of process
         Com%GetNTasks = .FALSE.
         PRINT *, mod_name, " JOB SEQUENTIEL"

      END SUBROUTINE IniCom


   END MODULE LibComm

