MODULE ModelInterfaces

  USE LesTypes

  USE EosLaws

  USE Visc_laws,         ONLY: EOSIviscInit

  USE Space_integration
  USE Num_schemes

  USE Contrib_Boundary

  USE Advection,         ONLY: Prim_to_Cons, Cons_to_Prim

  USE Turbulence_models, ONLY: Init_Turbulence, Turbulent, N_eqn_turb, &
                               Turb_Set_VarVP, Turb_Set_VarUa,         &
                               Turb_Read_InitVals, Turb_init_var,      &
                               Turb_Read_bc, Turb_Init_Wall_bc,        &
                               turb_cons_variables, turb_prim_variables

  USE GeomParam,         ONLY: Near_Wall_Distance_seq, &
                               Near_Wall_Distance_par, &
                               Near_Wall_Distance_parPtoP

  USE GeomGraph

  IMPLICIT NONE

CONTAINS

  !==============================================
  SUBROUTINE Model_compute_LHS_RHS(Com, Var, Mat)
  !==============================================

    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Mat
    !------------------------------------------------

    SELECT CASE(DATA%sel_GeoType)

    !-------------------------       
    !  Domain contribution
    !--------------------------------------------------
    CASE(cs_geotype_2dplan, cs_geotype_3d)

       CALL imp_RD_fuq(Com, Var, Mat)

    CASE DEFAULT

       STOP

    END SELECT

    !-----------------------
    ! Boundary conditions
    !-----------------------------------------------------
    CALL imp_Boundary_Conditions(Var, Mat)

  END SUBROUTINE Model_compute_LHS_RHS
  !===================================

  !==============================================
  SUBROUTINE Model_compute_RHS(Com, Var, with_Dt)
    !==============================================
    !>\brief
    !> One assumes that this routine is called for explicit run
    !> hence it assumes that Var%Ua, Ua_0, Vp, VP_0 exists and are filled
    !>
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Model_compute_RHS"

    TYPE(MeshCom),     INTENT(INOUT) :: Com
    TYPE(Variables),   INTENT(INOUT) :: Var
    LOGICAL, OPTIONAL, INTENT(IN)    :: with_Dt
    !-------------------------------------------

    SELECT CASE(DATA%sel_GeoType)

       !-------------------------
       !  Domain contribution
       !--------------------------------------------------

    CASE(cs_geotype_2dplan, cs_geotype_3d)
       SELECT CASE(DATA%sel_methode)
       CASE(cs_methode_exp_rk_Mario_rd)


          CALL exp_RD_fuq_exp(Com, Var, with_Dt)
       CASE DEFAULT
          CALL exp_RD_fuq(Com, Var, with_Dt)


       END SELECT
    CASE DEFAULT
       WRITE(*,*) mod_name, "Wrong (Data%sel_GeoType", DATA%sel_GeoType
       STOP

    END SELECT
    !-----------------------
    ! Boundary conditions
    !-----------------------------------------------------
    CALL Boundary_Conditions(Var)

  END SUBROUTINE Model_compute_RHS
  !===============================

  !=================================================
  FUNCTION Model_compute_RHS_i(Nu_i, Vc) RESULT(R_i)
  !=================================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN)    :: Nu_i
    REAL, DIMENSION(:,:), INTENT(INOUT) :: Vc

    REAL, DIMENSION(DATA%Nvar) :: R_i
    !-----------------------------------------

    R_i = exp_RD_i(Nu_i, Vc)

    CALL Boundary_Conditions_i(Nu_i, Vc, R_i)

  END FUNCTION Model_compute_RHS_i
  !===============================

  !==================================================================
  FUNCTION Model_approx_dRHS(Nu_i, Nu_j, Var, du_j, R_i) RESULT(dR_i)
  !==================================================================

    IMPLICIT NONE

    INTEGER,            INTENT(IN) :: Nu_i
    INTEGER,            INTENT(IN) :: Nu_j
    TYPE(Variables),    INTENT(IN) :: Var
    REAL, DIMENSION(:), INTENT(IN) :: du_j
    REAL, DIMENSION(:), INTENT(IN) :: R_i

    REAL, DIMENSION(SIZE(R_i)) :: dR_i
    !-----------------------------------------

!!$    TYPE(element_str) :: ele
!!$    TYPE(face_str)    :: ele_b
!!$
!!$    REAL, DIMENSION(:,:), ALLOCATABLE :: Vc_0, Vc
!!$    REAL, DIMENSION(:,:), ALLOCATABLE :: Vp_0, Vp
!!$    REAL, DIMENSION(:),   ALLOCATABLE :: Phi_tot_0, Phi_tot
!!$    REAL, DIMENSION(:,:), ALLOCATABLE :: Phi_i_0, Phi_i
!!$
!!$    REAL :: alpha, dummy
!!$
!!$    INTEGER :: je, jf, i, k, l, N_var, N_dofs
!!$
!!$    LOGICAL :: found
!!$    !-----------------------------------------
!!$
!!$    N_var = Data%NVar
!!$
!!$    dR_i = R_i
!!$
!!$    alpha = 10.E-3
!!$
!!$    !
!!$    ! Domain Residual
!!$    !
!!$    DO l = 1, SIZE(Connect(Nu_j)%cn_ele)
!!$
!!$       je = Connect(Nu_j)%cn_ele(l)
!!$
!!$       found = .FALSE.
!!$       DO k = 1, d_element(je)%e%N_points
!!$
!!$          IF( d_element(je)%e%Nu(k) == Nu_i ) THEN
!!$             found = .TRUE.
!!$             EXIT
!!$          ENDIF
!!$
!!$       ENDDO
!!$
!!$       IF( found ) THEN
!!$
!!$          ele = d_element(je)%e
!!$
!!$          N_dofs = ele%N_points
!!$
!!$          ALLOCATE( Vc_0(Data%NVar,    N_dofs), &
!!$               Vc(Data%NVar,    N_dofs) )
!!$
!!$          ALLOCATE( Vp_0(Data%NVarPhy, N_dofs), &
!!$               Vp(Data%NVarPhy, N_dofs) )
!!$
!!$          ALLOCATE( Phi_tot_0(Data%NVar), &
!!$               Phi_tot(Data%NVar) )
!!$
!!$          ALLOCATE( Phi_i_0(Data%NVar, N_dofs), &
!!$               Phi_i(Data%NVar, N_dofs) )
!!$
!!$          DO k = 1, N_dofs
!!$
!!$             Vc_0(:, k) = Var%Ua(:, ele%Nu(k))
!!$             Vc(:, k) = Var%Ua(:, ele%Nu(k))
!!$
!!$             IF( ele%Nu(k) == Nu_j ) THEN
!!$
!!$                Vc(:, k) = Var%Ua(:, ele%Nu(k)) + alpha*du_j
!!$
!!$             ENDIF
!!$
!!$             CALL ModelU2VpPt(Vc_0(:, k), Vp_0(:, k))
!!$             CALL ModelU2VpPt(  Vc(:, k),   Vp(:, k))
!!$
!!$          ENDDO
!!$
!!$          CALL Total_Residual_exp(ele, Vp_0, Vc_0, Phi_tot_0)
!!$          CALL Total_Residual_exp(ele, Vp,   Vc,   Phi_tot)
!!$
!!$          CALL Distribute_residual_exp(ele, Vp_0, Vc_0, dummy, Phi_tot_0, Phi_i_0, dummy)
!!$          CALL Distribute_residual_exp(ele, Vp,   Vc,   dummy, Phi_tot,   Phi_i,   dummy)
!!$
!!$          DO i = 1, N_dofs
!!$
!!$             IF( ele%Nu(i) == Nu_i ) THEN
!!$
!!$                dR_i = dR_i - Phi_i_0(:, i) + Phi_i(:, i)
!!$                EXIT
!!$
!!$             ENDIF
!!$
!!$          ENDDO
!!$
!!$          DEALLOCATE( Vc_0, Vc, Vp_0, Vp ) 
!!$          DEALLOCATE( Phi_tot_0, Phi_tot, Phi_i_0, Phi_i )
!!$
!!$       ENDIF ! ele \ni Nu_i
!!$
!!$    ENDDO ! ele \ni Nu_j
!!$
!!$    !
!!$    ! Boundary Residual
!!$    !
!!$    IF( ALLOCATED(Connect(Nu_j)%cn_bele) ) THEN
!!$
!!$       DO l = 1, SIZE(Connect(Nu_j)%cn_bele)
!!$
!!$          jf = Connect(Nu_j)%cn_bele(l)
!!$
!!$          found = .FALSE.
!!$          DO k = 1, b_element(jf)%b_f%N_points
!!$
!!$             IF( b_element(jf)%b_f%Nu(k) == Nu_i ) THEN
!!$                found = .TRUE.
!!$                EXIT
!!$             ENDIF
!!$
!!$          ENDDO
!!$
!!$          IF( found ) THEN
!!$
!!$             ele_b = b_element(jf)%b_f
!!$
!!$             N_dofs = ele_b%N_points
!!$
!!$             ALLOCATE( Vc_0(Data%NVar,    N_dofs), &
!!$                  Vc(Data%NVar,    N_dofs) )
!!$
!!$             ALLOCATE( Vp_0(Data%NVarPhy, N_dofs), &
!!$                  Vp(Data%NVarPhy, N_dofs) )
!!$
!!$             ALLOCATE( Phi_i_0(Data%NVar, N_dofs), &
!!$                  Phi_i(Data%NVar, N_dofs) )
!!$
!!$             DO k = 1, N_dofs
!!$
!!$                Vc_0(:, k) = Var%Ua(:, ele_b%Nu(k))
!!$                Vc(:, k) = Var%Ua(:, ele_b%Nu(k))
!!$
!!$                IF( ele_b%Nu(k) == Nu_j ) THEN
!!$
!!$                   Vc(:, k) = Vc(:, k) + alpha*du_j
!!$
!!$                ENDIF
!!$
!!$                CALL ModelU2VpPt(Vc_0(:, k), Vp_0(:, k))
!!$                CALL ModelU2VpPt(  Vc(:, k),   Vp(:, k))
!!$
!!$             ENDDO
!!$
!!$             Phi_i_0 = impose_WeakBC_exp(ele_b, Vc_0, Vp_0)
!!$             Phi_i   = impose_WeakBC_exp(ele_b, Vc,   Vp)
!!$
!!$             !#ifdef NAVIER_STOKES
!!$             !             CALL impose_StrongBC_exp(ele_b, Vc, Vp, Phi_i, D_Vc ) ....
!!$             !#else
!!$             !             CALL impose_StrongBC_exp(ele_b, Vc, Vp, Phi_i)
!!$             !#endif
!!$
!!$             DO i = 1, N_dofs
!!$
!!$                IF( ele_b%Nu(i) == Nu_i ) THEN
!!$
!!$                   dR_i = dR_i - Phi_i_0(:, i) + Phi_i(:, i)
!!$                   EXIT
!!$
!!$                ENDIF
!!$
!!$             ENDDO
!!$
!!$             DEALLOCATE( Vc_0, Vc, Vp_0, Vp ) 
!!$             DEALLOCATE( Phi_i_0, Phi_i )
!!$
!!$          ENDIF
!!$
!!$       ENDDO
!!$
!!$    ENDIF
!!$
!!$    dR_i = (dR_i - R_i) / alpha

  END FUNCTION Model_approx_dRHS
  !=============================  

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INITIALIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  !==============================
  SUBROUTINE Model_Init_Var(DATA)
  !==============================

    TYPE(Donnees), INTENT(IN OUT) :: DATA
    !-------------------------------------

    DATA%Nvar = 2 + DATA%Ndim ! r,ru,rv,rw,re

#ifdef NAVIER_STOKES   
    IF(Turbulent) THEN
       DATA%Nvar = DATA%Nvar + N_eqn_turb
    ENDIF
#endif

    DATA%Nvart = 3  ! p,c,T

    DATA%NvarPhy = DATA%Nvar + DATA%Nvart

    !--------------------
    ! Primitive variables
    !------------------------------
    i_rho_vp = 1                  ! rho
    i_vi1_vp = 2                  ! u
    i_vi2_vp = 3                  ! v
    i_vid_vp = DATA%Ndim + 1      ! v (2D) or w (3D)
    i_eps_vp = i_vid_vp  + 1      ! e
#ifdef NAVIER_STOKES
    IF (Turbulent) THEN
       CALL Turb_Set_VarVP(DATA%Nvar)
    END IF
#endif
    i_pre_vp = DATA%Nvar + 1      ! P
    i_son_vp = DATA%NVar + 2      ! c
    i_tem_vp = DATA%NVar + 3      ! T

    !------------------------
    ! Conservatives variables
    !------------------------------
    i_rho_ua = 1                  ! rho
    i_rv1_ua = 2                  ! rho*u
    i_rv2_ua = 3                  ! rho*v
    i_rvd_ua = DATA%Ndim + 1      ! rho*v (2D) or rho*w (3D)
    i_ren_ua = i_rvd_ua + 1       ! rho*et, with et = rho*e + 0.5*rho*Vel**2
#ifdef NAVIER_STOKES
    IF (Turbulent) THEN 
       CALL Turb_Set_VarUa(DATA%Nvar)
    END IF
#endif

    !------------------------
    ! Plotted variables names
    !------------------------------------
    ALLOCATE(DATA%VarNames(DATA%NvarPhy))

    DATA%VarNames(i_rho_vp)  = "rho"
    DATA%VarNames(i_vi1_vp)  = " u "
    DATA%VarNames(i_vi2_vp)  = " v "
    IF (DATA%Ndim==3) THEN 
       DATA%VarNames(i_vid_vp)  = " w "
    END IF
    DATA%VarNames(i_eps_vp)  = " e"
#ifdef NAVIER_STOKES
    IF (Turbulent) THEN 
       DATA%VarNames(i_nuts_vp) = "mut" ! eddy viscosity
    END IF
#endif    
    DATA%VarNames(i_pre_vp)  = " p "
    DATA%VarNames(i_son_vp)  = " C "
    DATA%VarNames(i_tem_vp)  = " T "

  END SUBROUTINE Model_Init_Var
  !============================

  !==========================================
  SUBROUTINE Model_Alloc_Var(DATA, Mesh, Var)
  !==========================================

    TYPE(Donnees),   INTENT(INOUT) :: DATA
    TYPE(MeshDef),   INTENT(IN)    :: Mesh
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    INTEGER :: N_dim, N_var_c, N_var_P, N_dofs, jt, k, is
    !--------------------------------------

#ifdef NAVIER_STOKES

    IF( DATA%sel_Modele /= cs_modele_eulerns ) THEN

       WRITE(*,*) 'ERROR: inconsistent model with the Navier-Stokes equations'
       WRITE(*,*) 'STOP'

       STOP

    ENDIF

#endif

    N_dofs  = Mesh%NCells
    N_dim   = DATA%Ndim
    N_Var_C = DATA%Nvar
    N_Var_P = DATA%NvarPhy

    Var%Ncells = N_dofs

    ALLOCATE( Var%Vp  (N_Var_P, N_dofs) ) 
    ALLOCATE( Var%Ua  (N_Var_C, N_dofs) )
    ALLOCATE( Var%Flux(N_var_C, N_dofs) )
    ALLOCATE( Var%Dtloc(        N_dofs) )

    ALLOCATE( Var%Vp_0 (N_Var_P, N_dofs) )  ! For unsteady
    ALLOCATE( Var%ua_0(N_var_C, N_dofs) )
    ALLOCATE(Var%CellVol(N_dofs))

#ifdef NAVIER_STOKES

    ALLOCATE( Var%D_Ua(N_dim, N_var_C, N_dofs) )
    ALLOCATE( Var%Re_loc(N_dofs) )

#endif  

#ifdef INSTAT_RD /* allocate source_instat, ua_0, ua_1 */

    ALLOCATE ( Var%source_instat(N_var_C, N_dofs) )
    ALLOCATE ( Var%ua_0         (N_var_C, N_dofs) )
    ALLOCATE ( Var%ua_1         (N_var_C ,N_dofs) )

#endif

    ! Strategie de sensure des chocs
!!$    Var%Avec_h = .FALSE. !?
!!$
!!$    IF (Var%avec_h) THEN
!!$       ALLOCATE(Var%h(2, N_dofs))
!!$    END IF

    ALLOCATE(   Var%Residus(N_var_C) )    
    ALLOCATE( DATA%Residus0(N_var_C) )
    ALLOCATE(    Var%Res_oo(N_var_C) )    
    ALLOCATE(  DATA%Res_oo0(N_var_C) )

    Var%CellVol=0.
    DO jt = 1, SIZE(d_element)

       DO k = 1, d_element(jt)%e%N_points

          is = d_element(jt)%e%NU(k)
          Var%CellVol(is) = Var%CellVol(is) &
               & + d_element(jt)%e%Volume/REAL( d_element(jt)%e%N_points)
       ENDDO
    ENDDO
  END SUBROUTINE Model_Alloc_Var
  !=============================

  !==========================================
  SUBROUTINE Model_Init_boundaries(Var, DATA)
  !==========================================
    CHARACTER(LEN = *), PARAMETER  :: mod_name = "Model_Init_boundaries"

    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Donnees),   INTENT(INOUT) :: DATA
    !--------------------------------------

    TYPE(face_str) :: loc_ele

    INTEGER, DIMENSION(:), ALLOCATABLE :: NU

    INTEGER :: jf, j_dom, i, k
    INTEGER :: N_dofs, bc_idx, bc_type
    LOGICAL :: Periodic_bc
    !-------------------------------------------

    DATA%N_wall_points = 0
    DATA%N_wall_faces = 0
    Periodic_bc = .FALSE.

#ifdef NAVIER_STOKES

    IF(Turbulent) THEN

       WRITE(*,*) 'Computing wall distance...'

#ifdef SEQUENTIEL
       CALL Near_Wall_Distance_seq()
#else
       !CALL Near_Wall_Distance_par()
       CALL Near_Wall_Distance_parPtoP(Var%NCells)
#endif

       WRITE(*,*) '...done'

    ENDIF

#endif
    ! No boundary    
    IF (.NOT. ALLOCATED(b_element) ) RETURN

    DO jf = 1, SIZE(b_element)

       !loc_ele = b_element(jf)%b_f

       N_dofs = b_element(jf)%b_f%N_points

       ALLOCATE( NU(N_dofs) )

       NU = b_element(jf)%b_f%NU

       bc_idx  = b_element(jf)%b_f%bc_type
       bc_type = DATA%FacMap(bc_idx)

       SELECT CASE (bc_type)

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_dirichlet,                 &
            lgc_gradient_nul_exterieur,    &
            lgc_sortie_subsonique,         &
            lgc_sortie_mixte,              &
            lgc_ns_steger_warming,         &
            lgc_flux_de_steger,            &
            lgc_entree_turbine_subsonique, &
            lgc_sortie_turbine_subsonique  )
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          IF( .NOT. DATA%Reprise ) THEN

             DO i = 1, N_dofs

                Var%Vp(:, NU(i)) = DATA%Fr(bc_idx)%Vp

             END DO

          ENDIF

       END SELECT

       DEALLOCATE( NU )
    ENDDO

    DO jf = 1, SIZE(b_element)

       !loc_ele = b_element(jf)%b_f

       N_dofs = b_element(jf)%b_f%N_points

       ALLOCATE( NU(N_dofs) )

       NU = b_element(jf)%b_f%NU

       bc_idx  = b_element(jf)%b_f%bc_type
       bc_type = DATA%FacMap(bc_idx)

       SELECT CASE (bc_type)

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_paroi_adh_adiab_flux_nul) 
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          DO i = 1, N_dofs  

             IF( .NOT. DATA%Reprise ) THEN

                Var%Vp(i_vi1_vp:i_vid_vp, NU(i)) = DATA%Fr(bc_idx)%Vp(i_vi1_vp:i_vid_vp)

                IF(turbulent) CALL Turb_Init_Wall_bc( Var%Vp(:, NU(i)) )

             END IF

             DATA%N_wall_points = DATA%N_wall_points + 1

          ENDDO

          DATA%N_wall_faces = DATA%N_wall_faces + 1

       !>>>>>>>>>>>>>>>>>>>
       CASE(lgc_paroi_misc)
       !>>>>>>>>>>>>>>>>>>>

          DO i = 1, N_dofs

             IF( .NOT. DATA%Reprise ) THEN

                ! Density from the domain initialization

                Var%Vp(i_vi1_vp:i_vid_vp, NU(i)) = DATA%Fr(bc_idx)%Vp(i_vi1_vp:i_vid_vp)

                Var%Vp(i_tem_vp, NU(i)) = DATA%Fr(bc_idx)%Vp(i_tem_vp)

                Var%Vp(i_eps_vp, NU(i)) = e__rho_T( Var%Vp(i_rho_vp, NU(i)), &
                                                    DATA%Fr(bc_idx)%Vp(i_tem_vp) )

                Var%Vp(i_pre_vp, NU(i)) = P__rho_T( Var%Vp(i_rho_vp, NU(i)), &
                                                    DATA%Fr(bc_idx)%Vp(i_tem_vp) )

                Var%Vp(i_son_vp, NU(i)) = c__rho_T( Var%Vp(i_rho_vp, NU(i)), &
                                                     DATA%Fr(bc_idx)%Vp(i_tem_vp) )

                IF(turbulent) CALL Turb_Init_Wall_bc( Var%Vp(:, NU(i)) )

             ENDIF

             DATA%N_wall_points = DATA%N_wall_points + 1

          END DO

          DATA%N_wall_faces = DATA%N_wall_faces + 1

       !>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_paroi_glissante)
       !>>>>>>>>>>>>>>>>>>>>>>>>

          DO i = 1, N_dofs

             DATA%N_wall_points = DATA%N_wall_points + 1

          END DO

          DATA%N_wall_faces = DATA%N_wall_faces + 1

       !>>>>>>>>>>>>>>>>>>>
       CASE(lgc_periodique)
       !>>>>>>>>>>>>>>>>>>>

          Periodic_bc = .TRUE.

       END SELECT

       DEALLOCATE(NU)
    ENDDO

    ALLOCATE( Var%Cp( DATA%N_wall_points) )
    ALLOCATE( Var%Cf( DATA%N_wall_points) )
    ALLOCATE( Var%C_LOC(3, DATA%N_wall_faces))

    IF(Periodic_bc) CALL Periodic_connect(Var%NCells)

  END SUBROUTINE Model_Init_boundaries
  !===================================


  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INPUTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !====================================
  SUBROUTINE Model_Get_Data(DATA, Mesh)
  !====================================

    TYPE(Donnees), INTENT(INOUT) :: DATA
    TYPE(MeshDef), INTENT(INOUT) :: Mesh
    !-----------------------------------

    REAL :: rho_oo, M_oo, AoA, AoSS, c_oo, T_oo, Re_oo, P_oo
    REAL :: Re_adim, mu_oo

    INTEGER , DIMENSION(:), ALLOCATABLE :: New_Log_Cell
    INTEGER , DIMENSION(:), POINTER     :: LogCell

    INTEGER :: MyUnit, maxlogcell, dominf, domsup
    INTEGER :: i, is

    LOGICAL :: test
    CHARACTER(LEN=256) :: essai
    INTEGER :: ios

    CHARACTER(LEN=20) :: fluid, fluid_model
    CHARACTER(LEN=20) :: turbulence, turbulence_model

    REAL, PARAMETER :: Pi = 4.0*ATAN(1.0)
    !--------------------------------------

    IF (DATA%sel_Modele == cs_modele_euler) THEN

       DATA%NavierStokes =.FALSE.

    ELSEIF (DATA%sel_Modele == cs_modele_eulerns) THEN

       DATA%NavierStokes =.TRUE.

    ENDIF

    MyUnit = 12

    ! Thermodynamics and Viscous Laws
    READ(MyUnit,*,ERR=996,END=996) DATA%LoiEtat, &
         DATA%LoiVisq, &
         DATA%LoiTh    ! Not used

    !Initialisation Modèle Thermodynamique
    CALL decoder_data_loivisq(DATA%LoiVisq, DATA%sel_LoiVisq)
    CALL decoder_data_loietat(DATA%LoiEtat, DATA%sel_LoiEtat)
    CALL decoder_data_loith(  DATA%LoiTh,   DATA%sel_LoiTh)

    ! Fluid model (Laminar or Turbulent)
    ! and the turbulent model
    IF(DATA%NavierStokes) THEN

       READ(MyUnit, *, ERR=996,END=996) Fluid,     &
            Turbulence

       CALL en_minuscules(Fluid, Fluid_model)
       CALL en_minuscules(Turbulence, Turbulence_model) 

       CALL Init_Turbulence(Fluid_model, Turbulence_Model)

    ENDIF

    CALL Model_Init_Var(DATA)

    READ(MyUnit, *) ! empty line

    READ(MyUnit,*,ERR=996,END=996) maxlogcell

    ALLOCATE(DATA%VarPhyInit(DATA%NvarPhy, maxlogcell))

    SELECT CASE(DATA%sel_approche)

    CASE (cs_approche_vertexcentered)

       ALLOCATE( New_Log_Cell(SIZE(Mesh%coor, 2)) )

       LogCell => Mesh%LogPt

    END SELECT

    New_Log_Cell = 1 

    LogCell = New_Log_Cell

    !----------------------------------
    ! Domain's variables initialization
    !-----------------------------------------------------------
    DO i = 1, maxlogcell

       READ(MyUnit,"(A)",IOSTAT=ios) essai

       IF (ios /= 0) THEN
          PRINT *, "ios/0==", ios
          GO TO 996
       END IF

       Re_oo = 0.0

       !-------------------------------
       ! Navier-Stokes or RANS model
       IF (DATA%sel_modele == cs_modele_eulerns) THEN
          
          ! Perfect gas
          IF (DATA%sel_LoiEtat == cs_loietat_gp .OR. &
              DATA%sel_LoiEtat == cs_loietat_ig ) THEN

             IF(DATA%NDim == 2) THEN

                READ(essai, *, IOSTAT=ios) dominf, domsup, rho_oo, M_oo, AoA, Re_oo
                IF(Turbulent) CALL Turb_Read_InitVals(essai, 6)

             ELSEIF(DATA%NDim == 3) THEN

                READ(essai, *, IOSTAT=ios) dominf, domsup, rho_oo, M_oo, AoA, AoSS, Re_oo
                IF(Turbulent) CALL Turb_Read_InitVals(essai, 7)

             ELSE

                WRITE(*,*) 'ERROR: number space dimensions'
                STOP

             ENDIF
             
          ! Real gas
          ELSE

             IF(DATA%NDim == 2) THEN
                
                READ(essai, *, IOSTAT=ios) dominf, domsup, rho_oo, M_oo, AoA, P_oo, Re_oo
                IF(Turbulent) CALL Turb_Read_InitVals(essai, 7)

             ELSEIF(DATA%NDim == 3) THEN

                READ(essai, *, IOSTAT=ios) dominf, domsup, rho_oo, M_oo, AoA, AoSS, P_oo, Re_oo
                IF(Turbulent) CALL Turb_Read_InitVals(essai, 8)

             ELSE

                WRITE(*,*) 'ERROR: number space dimensions'
                STOP

             ENDIF

          ENDIF

       ! Euler model
       ELSE

          IF(DATA%sel_LoiEtat == cs_loietat_gp .OR. &
             DATA%sel_LoiEtat == cs_loietat_ig ) THEN

             ! Perfect gas
             READ(essai, *, IOSTAT=ios) dominf, domsup, rho_oo, M_oo, AoA
             Aoss = 0.
          ELSE

             ! Real gas
             READ(essai, *, IOSTAT=ios) dominf, domsup, rho_oo, M_oo, AoA, P_oo
             Aoss = 0.
          ENDIF

       ENDIF
       !-------------------------------

       LogCell = New_Log_Cell

       DEALLOCATE(New_Log_Cell)

       IF(DATA%sel_LoiEtat == cs_loietat_sw) THEN

          !Lecture des données à l'infini et Tc
          READ(MyUnit,*,ERR=996,END=996) DATA%RhoRef, &
                                         DATA%VRef,   &
                                         DATA%LRef,   &
                                         DATA%TRef,   &
                                         DATA%Tcrit

       ELSE

          !Lecture des données à l'infini
          READ(MyUnit,*,ERR=996,END=996) DATA%RhoRef, &
                                         DATA%VRef,  &
                                         DATA%LRef,  &
                                         DATA%TRef

       ENDIF

       CALL EOSInit(DATA%Ndim, DATA%Nvar, DATA%LoiEtat, 1, DATA)

       READ(MyUnit,*,ERR=996,END=996) DATA%Gravite(1:DATA%Ndim)

       READ(MyUnit,*,ERR=996,END=996)

       IF (ios /= 0) THEN
          STOP
          WRITE(message_log, FMT=*) "ATTENTION : ios/1==", ios, TRIM(iostat_string(ios))
          CALL log_record(message_log)
          WRITE(message_log, FMT=*) "essai==", TRIM(essai)
          CALL log_record(message_log)
          READ(essai,*,IOSTAT=ios) rho_oo, M_oo, AoA, P_oo !RE_oo
       END IF

       IF (ios /= 0) THEN
          PRINT *, "ios/2==", ios
          GO TO 996
       END IF

       WRITE(*,*)
       WRITE(*,*) "En entrée : "
       WRITE(*,'("  rho_oo = ", F10.5)') rho_oo
       WRITE(*,'("  M_oo   = ", F10.5)') M_oo
       WRITE(*,'("  Alpha  = ", F10.5, " deg")') AoA
print*, "ZZZZZZZZZZZZZOOOOOOOOOOOOBBBBBB"
       IF(DATA%NDim == 3 .and. DATA%sel_modele == cs_modele_eulerns) THEN
          WRITE(*,'("   Beta  = ", F10.5, " deg")') AoSS
       ENDIF
       WRITE(*,'("  RE_oo  = ", E12.5)') Re_oo
       IF (DATA%sel_LoiEtat /= cs_loietat_gp .AND. &
           DATA%sel_LoiEtat /= cs_loietat_ig ) THEN
       WRITE(*,'("  P_oo   = ", F10.5)') P_oo
       ENDIF
       WRITE(*,*)

       ! Perfect gass
       IF (DATA%sel_LoiEtat == cs_loietat_gp .OR. &
           DATA%sel_LoiEtat == cs_loietat_ig ) THEN

          ! Desnity
          DATA%VarPhyInit(i_rho_vp, i) = rho_oo

          ! Incidence (rad)
          AoA = AoA * Pi/180.d0
          DATA%AoA = AoA

          ! Side slip
          IF(DATA%Ndim == 3) THEN
             AoSS = AoSS * Pi/180.d0
             DATA%AoSS = AoSS
          ENDIF
          
          ! Speed of sound
          c_oo = 1.d0
          DATA%VarPhyInit(i_son_vp, i) = c_oo

          !u,v,w; Component w always assumed 0
          DATA%VarPhyInit(i_vi1_vp:i_vid_vp, i) = 0.d0

          IF(DATA%NDim == 2) THEN

             DATA%VarPhyInit(i_vi1_vp, i) = M_oo*c_oo *COS(AoA)
             DATA%VarPhyInit(i_vi2_vp, i) = M_oo*c_oo *SIN(AoA)

          ELSEIF(DATA%NDim == 3) THEN

             DATA%VarPhyInit(i_vi1_vp, i) = M_oo*c_oo *COS(AoA)*COS(AoSS)
             DATA%VarPhyInit(i_vi2_vp, i) = M_oo*c_oo *SIN(AoSS)
             DATA%VarPhyInit(i_vid_vp, i) = M_oo*c_oo *SIN(AoA)*COS(AoSS)

          ELSE

             WRITE(*,*) 'Eorror: number space dimensions'
             STOP

          ENDIF

          ! Temperature
          T_oo = T__rho_c(rho_oo, c_oo)
          DATA%VarPhyInit(i_tem_vp, i) = T_oo

          ! Pressure
          DATA%VarPhyInit(i_pre_vp, i) = P__rho_T(rho_oo, T_oo)

          ! Internal energy
          DATA%VarPhyInit(i_eps_vp, i) = EOSEpsilon(DATA%VarPhyInit(:,i))

       ! Real gas
       ELSE

          ! Pressure
          DATA%VarPhyInit(i_pre_vp, i) = P_oo

          ! Desnity
          DATA%VarPhyInit(i_rho_vp, i) = rho_oo

          ! Incidence (rad)
          AoA = AoA * Pi/180.d0
          DATA%AoA = AoA

          ! Side slip
          IF(DATA%Ndim == 3) THEN
             AoSS = AoSS * Pi/180.d0
             DATA%AoSS = AoSS
          ENDIF

          ! Temperature     : T = T(rho,P)  []
          T_oo                         = T__rho_P(rho_oo, P_oo)
          DATA%VarPhyInit(i_tem_vp, i) = T_oo
          DATA%Tinf                    = T_oo

          ! Internal energy : e = e(rho,T)
          DATA%VarPhyInit(i_eps_vp, i) = e__rho_T(rho_oo, T_oo)

          ! Speed of sound  : c = c(rho,T)
          c_oo                         = c__rho_T(rho_oo, T_oo)
          DATA%VarPhyInit(i_son_vp, i) = c_oo

          !u,v,w; Component w always assumed 0
          DATA%VarPhyInit(i_vi1_vp:i_vid_vp, i) = 0.d0

          IF(DATA%NDim == 2) THEN

             DATA%VarPhyInit(i_vi1_vp, i) = M_oo*c_oo *COS(AoA)
             DATA%VarPhyInit(i_vi2_vp, i) = M_oo*c_oo *SIN(AoA)

          ELSEIF(DATA%NDim == 3) THEN

             DATA%VarPhyInit(i_vi1_vp, i) = M_oo*c_oo *COS(AoA)*COS(AoSS)
             DATA%VarPhyInit(i_vi2_vp, i) = M_oo*c_oo *SIN(AoSS)
             DATA%VarPhyInit(i_vid_vp, i) = M_oo*c_oo *SIN(AoA)*COS(AoSS)

          ELSE

             WRITE(*,*) 'Eorror: number space dimensions'
             STOP

          ENDIF

       ENDIF

       !Printing Values
       WRITE(*,*) "Initialization : "
       WRITE(*,'("  rho_oo = ", F10.5)') DATA%VarPhyInit(i_rho_vp, i)
       WRITE(*,'("    P_oo = ", F10.5)') DATA%VarPhyInit(i_pre_vp, i)
       WRITE(*,'("    T_oo = ", F10.5)') DATA%VarPhyInit(i_tem_vp, i)
       WRITE(*,'("    e_oo = ", F10.5)') DATA%VarPhyInit(i_eps_vp, i)
       WRITE(*,'("    G_oo = ", F10.5)') GAMMA__rho_T(rho_oo, T_oo)
       WRITE(*,'("    M_oo = ", F10.5)') M_oo 
       WRITE(*,'("    c_oo = ", F10.5)') DATA%VarPhyInit(i_son_vp, i)
       WRITE(*,'("     AoA = ", F10.5, " rad")') AoA
       IF(DATA%NDim == 3 ) THEN
       WRITE(*,'("    AoSS = ", F10.5, " rad")') AoSS
       ENDIF
       IF(DATA%NDim == 2 ) THEN
       WRITE(*,'("    v_oo = ",2(F10.5, 2X))') DATA%VarPhyInit(i_vi1_vp:i_vid_vp, i)
       ELSE
       WRITE(*,'("    v_oo = ",3(F10.5, 2x))') DATA%VarPhyInit(i_vi1_vp:i_vid_vp, i)
       ENDIF
       WRITE(*,'("   Re_oo = ", E12.5)') Re_oo
       WRITE(*,*)

       !Initialisation Modele Visqueux
       IF (DATA%sel_modele == cs_modele_eulerns) THEN
          CALL EOSIviscInit(DATA, M_oo, Re_oo, Re_adim, mu_oo)
       END IF

#ifdef NAVIER_STOKES
       IF(Turbulent) CALL Turb_init_var(DATA%VarPhyInit(:, i), Re_adim, mu_oo)
#endif

    END DO


    CLOSE(MyUnit)

    Mesh%Coor = Mesh%Coor/DATA%LRef

    RETURN

996 CONTINUE

    WRITE(*,*) 'ERREUR DANS NumMeth. LA SECTION 5 DOIT CONTENIR:'
    WRITE(*,*) 'LoiEtat, LoiVisq, LoiTh'
    IF(DATA%NavierStokes) WRITE(*,*)  'Fluid_Model, Turbulence_Model'
    WRITE(*,*)
    WRITE(*,*) 'maxlogcell'
    WRITE(*,*) '[dominf,domsup,]RhoIn, UIn(1:Data%Ndim), PIn'
    WRITE(*,*) 'Data%RhoRef , Data%VRef   , Data%LRef   ,   Data%Tref'
    WRITE(*,*) 'Data%Gravite(1:Data%Ndim)'
    WRITE(*,*)

    STOP

  END SUBROUTINE Model_Get_Data
  !============================

  !==================================
  SUBROUTINE Model_Get_bc(DATA, Mesh)
  !==================================

    TYPE(Donnees), INTENT(INOUT) :: DATA
    TYPE(MeshDef), INTENT(INOUT) :: Mesh
    !------------------------------------

    CHARACTER(LEN=256) :: essai

    LOGICAL :: Found , existe
    INTEGER :: MyUnit, ibord, MaxLogFac, Facmax, ilog, ifac

    INTEGER :: statut, ios

    REAL, DIMENSION(:,:), POINTER :: VpFrl

    CHARACTER(LEN=70) :: FileName, composition

    INTEGER, DIMENSION(:), ALLOCATABLE :: lgn, lgf1, lgf2,lgc
    !-----------------------------------------

    REAL :: rho_b, M_b, AoA_b, AoSS_b, P_b, c_b, T_b, mod_v
    !-----------------------------------------

    DATA%Nbord = 0

    IF( Mesh%NFacFr <= 0 ) RETURN

    MaxLogFac = 0

    MyUnit = 11

    FileName = "Climites_"//TRIM(DATA%RootName)//'.data'

    INQUIRE(FILE=TRIM(FileName), EXIST=existe)

    IF (.NOT. existe) THEN
       WRITE(*,*) 'ERROR: file', TRIM(FileName), ' not found'
       STOP
    END IF

    OPEN(UNIT = MyUnit, FILE=TRIM(FileName), &
         FORM = "formatted", STATUS = "old", IOSTAT=statut )

    READ(MyUnit,*, IOSTAT=statut) DATA%Nbord

    IF( DATA%Nbord <= 0 ) RETURN

    ALLOCATE( lgn (DATA%Nbord), &
         lgf1(DATA%Nbord), &
         lgf2(DATA%Nbord), &
         lgc (DATA%Nbord)  )
    lgc = 0

    ALLOCATE( VpFrl(DATA%NvarPhy, DATA%Nbord) )

    VpFrl = 0.0

    DO ibord = 1 , DATA%Nbord !, 1

       READ(MyUnit,*) lgn(ibord),  &
            lgf1(ibord), &                      
            lgf2(ibord), &
            lgc(ibord)

       CALL encoder_Data_logfr(lgc(ibord), composition)


       WRITE(*,*) ibord, ' --> type =', lgc(ibord), TRIM(composition)

       SELECT CASE(lgc(ibord))

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_dirichlet,              &
            lgc_gradient_nul_exterieur, &
            lgc_sortie_subsonique,      &
            lgc_sortie_mixte,           &
            lgc_ns_steger_warming,      &
            lgc_flux_de_steger          )
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          READ(MyUnit,"(A)", IOSTAT=ios) essai

          IF(DATA%NDim == 2) THEN

             READ(essai, *, IOSTAT=ios) rho_b, M_b, AoA_b, P_b

             AoA_b = AoA_b * ACOS(-1.d0) / 180.d0

          ELSEIF(DATA%NDim == 3) THEN

             READ(essai, *, IOSTAT=ios) rho_b, M_b, AoA_b, AoSS_b, P_b

             AoA_b  = AoA_b  * ACOS(-1.d0) / 180.d0
             AoSS_b = AoSS_b * ACOS(-1.d0) / 180.d0

          ELSE
             WRITE(*,*) 'ERROR: number space dimensions'
             STOP
          ENDIF

          T_b = T__rho_P(rho_b, P_b)
          c_b = c__rho_T(rho_b, T_b)
          
          mod_v = M_b * c_b

          VpFrl(i_rho_vp,          ibord) = rho_b
          
          VpFrl(i_vi1_vp:i_vid_vp, ibord) = 0.d0

          IF(DATA%NDim == 2) THEN

             VpFrl(i_vi1_vp,       ibord) = mod_v * COS(AoA_b)
             VpFrl(i_vi2_vp,       ibord) = mod_v * SIN(AoA_b)

          ELSEIF(DATA%NDim == 3) THEN

             VpFrl(i_vi1_vp,       ibord) = mod_v * COS(AoA_b) * COS(AoSS_b)
             VpFrl(i_vi2_vp,       ibord) = mod_v * SIN(AoSS_b)
             VpFrl(i_vid_vp,       ibord) = mod_v * SIN(AoA_b) * COS(AoSS_b)

          ELSE

             WRITE(*,*) 'ERROR: number space dimensions'
             STOP

          ENDIF

          VpFrl(i_pre_vp,          ibord) = P_b

#ifdef NAVIER_STOKES
          IF(DATA%NDim == 2) THEN

             IF(Turbulent) CALL Turb_Read_bc(essai, 4, VpFrl(:, ibord))

          ELSEIF(DATA%NDim == 3) THEN

             IF(Turbulent) CALL Turb_Read_bc(essai, 5, VpFrl(:, ibord))

          ELSE
             WRITE(*,*) 'ERROR: number space dimensions'
             STOP
          ENDIF
#endif

       !>>>>>>>>>>>>>>>>>>>
       CASE(lgc_paroi_misc)
       !>>>>>>>>>>>>>>>>>>>

          IF (.NOT. DATA%NavierStokes) THEN 
             WRITE(*,*) 'ERROR: no-slip wall not supported by inviscid computations,'
             WRITE(*,*) '       use slip wall boundary conditions. STOP'
             STOP
          ENDIF

          READ(MyUnit,*) VpFrl(i_vi1_vp:i_vid_vp, ibord), &
                         VpFrl(i_tem_vp,          ibord)

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_paroi_adh_adiab_flux_nul)
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          IF (.NOT. DATA%NavierStokes) THEN 
             WRITE(*,*) 'ERROR: no-slip wall not supported by inviscid computations,'
             WRITE(*,*) '       use slip wall boundary conditions. STOP'
             STOP
          ENDIF

          READ(MyUnit,*) VpFrl(i_vi1_vp:i_vid_vp,ibord)

       !>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_paroi_glissante)
       !>>>>>>>>>>>>>>>>>>>>>>>>

          IF (DATA%NavierStokes) THEN
             WRITE(*,*) 'ERROR: slip wall not supported by viscous computations,'
             WRITE(*,*) '       use symmetry plane boundary conditions. STOP'
             STOP
          ENDIF

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_gradient_nul_interieur)
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

       !Lecture données : Pression totale, densité totale, angle d'attaque
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_entree_turbine_subsonique)
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          IF (DATA%Ndim /= 2) THEN 
             WRITE(*,*) 'ERROR: Boundary Condition -> '
             WRITE(*,*) 'Entrée Turbine Subsonique = 91'
             WRITE(*,*) 'Spatial Dimension must be equal to 2. STOP'
             STOP
          ENDIF

          IF (DATA%sel_LoiEtat == cs_loietat_gp .OR. &
               DATA%sel_LoiEtat == cs_loietat_vdw ) THEN 
             WRITE(*,*) 'ERROR: Boundary Condition -> '
             WRITE(*,*) 'Entrée Turbine Subsonique = 91'
             WRITE(*,*) 'EOS Law must be SW or PRSV. STOP'
             STOP
          ENDIF

          READ(MyUnit,*) DATA%P_oo_turb, DATA%rho_oo_turb, DATA%alpha_oo_turb
          DATA%alpha_oo_turb = DATA%alpha_oo_turb * ACOS(-1.d0) / 180.d0

          !Lecture données ; Rapport de pression
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_sortie_turbine_subsonique)
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          IF (DATA%Ndim /= 2) THEN 
             WRITE(*,*) 'ERROR: Boundary Condition -> '
             WRITE(*,*) 'Sortie Turbine Subsonique = 92'
             WRITE(*,*) 'Spatial Dimension must be equal to 2. STOP'
             STOP
          ENDIF

          IF (DATA%sel_LoiEtat == cs_loietat_gp .OR. &
              DATA%sel_LoiEtat == cs_loietat_vdw ) THEN 
             WRITE(*,*) 'ERROR: Boundary Condition -> '
             WRITE(*,*) 'Entrée Turbine Subsonique = 91'
             WRITE(*,*) 'EOS Law must be SW or PRSV. STOP'
             STOP
          ENDIF

          READ(MyUnit,*) DATA%r_turb

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_subsonic_inflow_Pt)
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>

          IF (DATA%sel_LoiEtat /= cs_loietat_gp ) THEN 
             WRITE(*,*) 'ERROR: Boundary Condition -> '
             WRITE(*,*) 'stagnation values for subsonic inflow '
             WRITE(*,*) 'available only for perfect gas. STOP!'
             STOP
          ENDIF

          VpFrl(:, ibord) = DATA%VarPhyInit(:, 1)

          READ(MyUnit,"(A)", IOSTAT=ios) essai

          IF( DATA%Ndim == 2 ) THEN

             ! Total Pressure, Total Temperature, Flow direction
             READ(essai, *, IOSTAT=ios) VpFrl(i_pre_vp, ibord), &
                                        VpFrl(i_tem_vp, ibord), &
                                        AoA_b

             AoA_b = AoA_b * ACOS(-1.d0) / 180.d0

             ! Components of the flow direction
             VpFrl(i_vi1_vp, ibord) = COS(AoA_b)
             VpFrl(i_vi2_vp, ibord) = SIN(AoA_b)

#ifdef NAVIER_STOKES
             IF(Turbulent) CALL Turb_Read_bc(essai, 3, VpFrl(:, ibord))
#endif

          ELSE

             READ(essai, *, IOSTAT=ios) VpFrl(i_pre_vp, ibord), &
                                        VpFrl(i_tem_vp, ibord), &
                                        AoA_b, AoSS_b

             AoA_b  = AoA_b  * ACOS(-1.d0) / 180.d0
             AoSS_b = AoSS_b * ACOS(-1.d0) / 180.d0

             ! Components of the flow direction
             VpFrl(i_vi1_vp, ibord) = COS(AoA_b)*COS(AoSS_b)
             VpFrl(i_vi2_vp, ibord) = SIN(AoSS_b)
             VpFrl(i_vid_vp, ibord) = SIN(AoA_b)*COS(AoSS_b)

#ifdef NAVIER_STOKES
             IF(Turbulent) CALL Turb_Read_bc(essai, 4, VpFrl(:, ibord))
#endif

ENDIF 

       !>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_symetrie_plane) 
       !>>>>>>>>>>>>>>>>>>>>>>>

!!$          IF (.NOT. Data%NavierStokes) THEN
!!$             lgc(ibord) = lgc_paroi_glissante
!!$          ENDIF             

       !>>>>>>>>>>>>>>>>>>>
       CASE(lgc_periodique)
       !>>>>>>>>>>>>>>>>>>>

       CASE DEFAULT

          WRITE(*,*) 'ERROR: boundary condition of unknown type'
          STOP

       END SELECT

    END DO

    CLOSE(MyUnit)

    DO ifac = 1, Mesh%NFacFr

       ilog = Mesh%LogFr(ifac)

       IF (ilog == 0) CYCLE

       Found = .FALSE.

       DO ibord = 1, DATA%Nbord !, 1

          IF( ilog >= lgf1(ibord) .AND. ilog <= lgf2(ibord) ) THEN

             Mesh%LogFr(ifac) = ibord

             Found  = .TRUE.

             EXIT

          END IF

       ENDDO

       IF( .NOT. Found ) THEN

          WRITE(*,*) 'ERROR in boudary conditions'
          STOP

       END IF

    ENDDO

    DEALLOCATE(lgn, lgf1, lgf2)

    MaxLogFac = DATA%Nbord

    ALLOCATE( DATA%Fr(MaxLogFac) )

    MaxLogFac = DATA%Nbord

    DO ibord = 1, DATA%Nbord !, 1

       SELECT CASE(lgc(ibord))

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_dirichlet,              &
            lgc_gradient_nul_exterieur, &
            lgc_sortie_subsonique,      &
            lgc_sortie_mixte,           &
            lgc_ns_steger_warming,      &
            lgc_flux_de_steger          ) 
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          !rho et P imposés
          !Calcul T
          VpFrl(i_tem_vp, ibord) = T__rho_P(VpFrl(i_rho_vp, ibord), VpFrl(i_pre_vp, ibord))

          !Calcul e
          VpFrl(i_eps_vp, ibord) = e__rho_T(VpFrl(i_rho_vp, ibord), VpFrl(i_tem_vp, ibord))

          !Calcul c
          VpFrl(i_son_vp, ibord) = c__rho_T(VpFrl(i_rho_vp, ibord), VpFrl(i_tem_vp, ibord))

       !>>>>>>>>>>>>>>>>>>>
       CASE(lgc_paroi_misc)
       !>>>>>>>>>>>>>>>>>>>

          !?

       !>>>>>>>>>>>>>>>>>>>
       CASE(lgc_entree_turbine_subsonique )
       !>>>>>>>>>>>>>>>>>>>
          
          VpFrl(:,ibord) = DATA%VarPhyInit(:, 1)

       !>>>>>>>>>>>>>>>>>>>
       CASE(lgc_sortie_turbine_subsonique )
       !>>>>>>>>>>>>>>>>>>>
          
          ! rho, u, v
          VpFrl(:,ibord) = DATA%VarPhyInit(:, 1)

       !>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CASE(lgc_subsonic_inflow_Pt)
       !>>>>>>>>>>>>>>>>>>>>>>>>>>>

         ! Nothing happens here

       END SELECT

       ALLOCATE(DATA%Fr(ibord)%Vp(DATA%NvarPhy))

       DATA%Fr(ibord)%Vp(:) = VpFrl(:,ibord)

    END DO

    DEALLOCATE(VpFrl)

    Facmax = DATA%Nbord

    ALLOCATE( DATA%FacMap(Facmax))

    DATA%FacMap = lgc

    DEALLOCATE(lgc)

  END SUBROUTINE Model_Get_bc
  !==========================


  !-------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------

  !===========================
  SUBROUTINE ModelVp2u(Vp, Vc)
  !===========================
  !
  ! Vp -> Ua
  !
    IMPLICIT NONE

    REAL,DIMENSION(:), INTENT(IN)  :: Vp
    REAL,DIMENSION(:), INTENT(OUT) :: Vc
    !-----------------------------------

    CALL Prim_to_Cons(Vp, Vc)

  END SUBROUTINE ModelVp2u
  !=======================

  !=============================
  SUBROUTINE ModelU2VpPt(Vc, Vp)
  !=============================
  !
  ! Ua -> Vp
  !
    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: Vc
    REAL, DIMENSION(:), INTENT(OUT) :: Vp   
    !------------------------------------    

    CALL Cons_to_Prim(Vc, Vp)

  END SUBROUTINE ModelU2VpPt
  !=========================

  !===================================
  SUBROUTINE ModelIncrement(Var, DATA)
  !===================================

  !Incrementer les variables transportees et calculer les variables physiques.
  !Eventuellement appliquer un filtre, et dans ce cas mettre en coherence les
  !increments, les variables transportees et les variables physiques.
  !
    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Donnees),   INTENT(IN)    :: DATA
    !--------------------------------------

    REAL, DIMENSION(DATA%Nvar) :: Vc

    INTEGER :: is
    !---------------------------------------

    DO is = 1, Var%NCells

       Vc = Var%ua(:,is) + Var%Flux(:,is)
       !^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

       Var%Ua(:, is) = Vc

       CALL ModelU2VpPt(Vc, Var%Vp(:, is))

    END DO

!    CALL Strong_BC(Var, .TRUE.)

    IF( ALLOCATED(per_conn) ) THEN
       DO is = 1, SIZE(per_conn, 1) 
          Var%Ua(:, per_conn(is,2)) = Var%Ua(:,  per_conn(is,1))
          Var%Vp(:, per_conn(is,2)) = Var%Vp(:,  per_conn(is,1))
       ENDDO
    ENDIF

  END SUBROUTINE ModelIncrement
  !============================

   !========================================
  SUBROUTINE Model_TimeStep(DATA, Var, CFL)
    !========================================

    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER  :: mod_name = "Model_TimeStep"
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Donnees),   INTENT(IN)    :: DATA
    REAL,            INTENT(IN)    :: CFL
    !-------------------------------------
    INTEGER :: is, jt, k, Nvar, istat

    REAL    :: c, Unorm, Dt0, umax
    REAL    :: hnorm

    LOGICAL, SAVE :: initialise = .FALSE.



    REAL :: dt_phys2, dtold

    !----------------------------------------------




    Dt0 = 100.0



    Nvar = DATA%Nvar

    Dt0 = MAX(1.0e-10, MINVAL(Var%Dtloc))


    dt_phys2= HUGE(1.0)

    !
    ! Strategie de pas de temps : Remi--Mario. Dans cette version, il y a un probleme au 
    ! 1er pas de temps car le pas de temps est calculer avec les residus : au premier pas de
    ! temps, dtphys=0, en fait une valeur minimale : 10e-10 ici.
    !

    DO jt = 1, SIZE(d_element)
       Umax=0.
       DO k = 1, d_element(jt)%e%N_points

          is = d_element(jt)%e%NU(k)
          ! Normale sortante de 1 -- vers ---> 2 (normalisee)

          c = Var%Vp(i_son_vp,is)

          unorm = ABS ( SUM( Var%Vp(i_vi1_vp: i_vid_vp,is) * d_element(jt)%e%rd_n(:,k))) &
               +c * SQRT ( SUM( d_element(jt)%e%rd_n(:,k)**2 ) )


          Umax = MAX(umax, unorm)

       END DO
       dt_phys2= MIN( dt_phys2, d_element(jt)%e%volume/MAX(unorm, 1.e-10))
    END DO

    !    dt_phys2 = MINVAL ( Var%CellVol/Um)

    dtold=Var%dt_Phys



    Var%Dt_phys = MAX(MIN( CFL * dt_phys2, DATA%Tmax - Var%Temps),1.e-10)


    Var%Dt=Var%Dt_Phys





  END SUBROUTINE Model_TimeStep
  !============================


!!!===================Inititialisation and restart

  SUBROUTINE Model_Init_Sol(Icas,DATA, Var)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "Model_Init_Sol"
    INTEGER, INTENT(in):: Icas
    TYPE(donnees),   INTENT(IN) :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var

    INTEGER ::  jt, i, N_dofs, bc_idx, bc_type
    INTEGER, DIMENSION(:), ALLOCATABLE :: Nu


    SELECT CASE(Icas)

       !---------------------
    CASE(cs_icas_uniforme)
       !---------------------

       CALL CAS_Uniforme(DATA, Var)

       !--------------------
    CASE(cs_icas_ringleb)
       !--------------------

       CALL CAS_Ringleb(DATA, Var)


       !--------------------
    CASE(cs_icas_supvort) ! 30
       !--------------------

       CALL CAS_SupersonicVortex(DATA, Var)

       !--------------------
    CASE(cs_icas_4etats) ! 31
       !---------------------

       CALL cas_4etats(DATA, Var)

       !--------------------
    CASE(cs_icas_sod) ! 32
       !---------------------


      CALL cas_Sod(DATA, Var)


    CASE(cs_icas_DMR) ! 40
       !---------------------

       CALL cas_dmr(DATA, Var)

    CASE(cs_icas_vortex) ! 50
       !---------------------

       CALL cas_vortex(DATA, Var)

   CASE(cs_icas_test) ! 100
       !---------------------

       CALL cas_sphere(DATA, Var)


    CASE DEFAULT

       WRITE(*,*) 'ERROR: unknown initilization type'
       CALL stop_run()

    END SELECT

  END SUBROUTINE Model_Init_Sol







  !--------------------------------------------------------------------------------------------
  !--------------------------------------------------------------------------------------------
  !--------------------------------------------------------------------------------------------
  !--------------------------------------------------------------------------------------------

  !=================================
  SUBROUTINE CAS_uniforme(DATA, Var)
    !=================================

    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    TYPE(face_str) :: loc_ele         

    INTEGER :: jf, i, is, N_dofs, bc_idx, bc_type

    INTEGER, DIMENSION(:), ALLOCATABLE :: Nu
    !----------------------------------------

    SELECT CASE (DATA%sel_modele)

    CASE (cs_modele_euler, &
         cs_modele_eulerns)

       DO is = 1, Var%NCells
          Var%Vp(:, is) = DATA%VarPhyInit(:, 1)
       END DO

       IF (DATA%NavierStokes) THEN

          ! No boundary
          IF (.NOT. ALLOCATED(b_element) ) RETURN

          DO jf = 1, SIZE(b_element)

             loc_ele = b_element(jf)%b_f                  

             N_dofs = loc_ele%N_points

             ALLOCATE( NU(N_dofs) )
             NU = loc_ele%NU

             bc_idx  = loc_ele%bc_type
             bc_type = DATA%FacMap(bc_idx)

             SELECT CASE(bc_type)

             CASE(lgc_paroi_adh_adiab_flux_nul, &
                  lgc_paroi_misc)

                DO i = 1, N_dofs

                   Var%Vp(i_vi1_vp:i_vid_vp, NU(i)) = DATA%Fr(bc_idx)%Vp(i_vi1_vp:i_vid_vp)

                ENDDO

             END SELECT

             DEALLOCATE( NU )

          END DO

       ENDIF

    CASE DEFAULT

       WRITE(*,*) 'ERROR: unknown model for uniform initialization'
       CALL stop_run()

    END SELECT

  END SUBROUTINE CAS_uniforme
  !==========================

  !================================
  SUBROUTINE CAS_Ringleb(DATA, Var)
    !================================

    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Ringleb"
    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var       
    !--------------------------------------

    REAL :: x, y, rho, u, v, P, c
    INTEGER :: je, is, Nu
    !--------------------------------------

    PRINT*, mod_name//" Pas implemente"
    STOP

!!$    Var%Vp = 0.0
!!$
!!$    SELECT CASE (data%sel_modele)
!!$
!!$    CASE (cs_modele_euler)
!!$
!!$
!!$       CALL Init_Exact_Ringleb_sol(Var)
!!$
!!$       DO je = 1, SIZE(b_element)
!!$
!!$          DO is = 1, b_element(je)%b_f%N_points
!!$
!!$             x = b_element(je)%b_f%Coords(1, is)
!!$             y = b_element(je)%b_f%Coords(2, is)
!!$
!!$             Nu = b_element(je)%b_f%NU(is)
!!$
!!$             IF ( x < 0.0  .AND. ABS(y) < 1.428 ) THEN
!!$
!!$                ! Inner boundary
!!$                CALL Exact_Ringleb_sol(x, y, "in_wall", rho, u, v, P, c)
!!$
!!$             ELSEIF ( x > 0.328865 ) THEN
!!$
!!$                ! Outer boundary
!!$                CALL Exact_Ringleb_sol(x, y, "out_wall", rho, u, v, P, c)
!!$
!!$             ELSEIF( ABS(y) > 1.428 .AND. x < 0.33 ) THEN
!!$
!!$                ! In/Out-flow boundary
!!$                CALL Exact_Ringleb_sol(x, y, "in_out", rho, u, v, P, c)
!!$
!!$             END IF
!!$
!!$             Var%Vp(i_rho_vp, Nu) = rho
!!$             Var%Vp(i_vi1_vp, Nu) = u
!!$             Var%Vp(i_vi2_vp, Nu) = ABS(v)
!!$             Var%Vp(i_pre_vp, Nu) = P
!!$             Var%Vp(i_son_vp, Nu) = c
!!$             Var%Vp(i_tem_vp, Nu) = T__rho_c(rho, c)
!!$             Var%Vp(i_eps_vp, Nu) = EOSEpsilon(Var%Vp(:, Nu))
!!$
!!$          ENDDO
!!$
!!$       ENDDO
!!$
!!$    CASE DEFAULT
!!$
!!$       WRITE(*,*) 'ERROR: unknown model for Ringleb initialization'
!!$       CALL stop_run()
!!$
!!$    END SELECT

  END SUBROUTINE CAS_Ringleb
  !=========================

!!$       !===========================
!!$       SUBROUTINE CAS_MS(Data, Var)
!!$       !============================
!!$  ! manufactured solutions
!!$         IMPLICIT NONE
!!$
!!$         TYPE(donnees),   INTENT(IN)    :: DATA
!!$         TYPE(Variables), INTENT(INOUT) :: Var
!!$         !--------------------------------------
!!$
!!$         TYPE(face_str) :: loc_ele
!!$
!!$         REAL :: x, y
!!$
!!$         INTEGER :: je, is, NU
!!$         !---------------------------------------
!!$
!!$         DO je = 1, SIZE(d_element)
!!$
!!$            DO is = 1, d_element(je)%e%N_points
!!$
!!$               NU = d_element(je)%e%NU(is)
!!$               x = d_element(je)%e%Coords(1, is)
!!$               y = d_element(je)%e%Coords(2, is)
!!$#ifdef NAVIER_STOKES
!!$               Var%Vp(:, NU) = MS_Psolution(x, y)
!!$#endif
!!$            ENDDO
!!$
!!$         ENDDO
!!$         
!!$       END SUBROUTINE CAS_MS
!!$       !====================

  !=========================================
  SUBROUTINE CAS_SupersonicVortex(DATA, Var)
    !=========================================

    IMPLICIT NONE

    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    TYPE(face_str) :: loc_ele

    REAL :: x, y, Mach, r_in, M_in, r, kk

    INTEGER :: je, is, NU
    !---------------------------------------

    M_in = 2.25d0
    r_in = 1.d0

    kk = 1.586d0

    DO je = 1, SIZE(d_element)

       DO is = 1, d_element(je)%e%N_points

          NU = d_element(je)%e%NU(is)
          x = d_element(je)%e%Coords(1, is)
          y = d_element(je)%e%Coords(2, is)

          r = SQRT(x*x + y*y)

          Var%Vp(i_rho_vp, NU) = (1.d0 + 0.2d0*M_in*M_in*(1.d0 - (r_in/r)**2))**(1.d0/0.4d0)

          Var%Vp(i_pre_Vp, NU) = (1.d0/1.40)*(1.d0 + 0.2d0*M_in*M_in*(1.d0 - (r_in/r)**2))**(1.4d0/0.4d0)

          Var%Vp(i_tem_Vp, NU) = 1.4d0*Var%Vp(i_pre_Vp, NU)/Var%Vp(i_rho_vp, NU)

          Var%Vp(i_son_Vp, NU) = DSQRT( Var%Vp(i_tem_Vp, NU) )

          Mach = SQRT( kk**2 / ( ((r/r_in)**2) - 0.2d0*kk*KK) )

          Var%Vp(i_vi1_vp, NU) =  Var%Vp(i_son_Vp, NU)*Mach*y/r
          Var%Vp(i_vi2_vp, NU) = -Var%Vp(i_son_Vp, NU)*Mach*x/r

          Var%Vp(i_eps_vp, NU) = Var%Vp(i_pre_Vp, NU)/(Var%Vp(i_rho_vp, NU)*0.4d0)

       ENDDO

    ENDDO

  END SUBROUTINE CAS_SUPERSONICVORTEX
  !==================================



  !=================================
  SUBROUTINE CAS_DMR(DATA, Var)
    !=================================
    ! assume the wedge is located at (0,0)
    ! before shock (rho,u,v,p)=(8,8.25,0,116.5)
    ! after shock: (rho,u,v,p)=(1.4,0.0.1)
    ! angle of wedge: Pi/6
    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    CLASS(face_str),POINTER :: loc_ele



    INTEGER :: je, is
    REAL :: x, y, rho, u, v, p

    INTEGER :: Nu
    REAL, ALLOCATABLE,DIMENSION(:):: Vp

    !----------------------------------------
    ALLOCATE(Vp(SIZE(Var%Vp,1)))

    DO je = 1, SIZE(d_element)

       DO is = 1, d_element(je)%e%N_points

          x = d_element(je)%e%Coords(1, is)
          y = d_element(je)%e%Coords(2, is)

          NU = d_element(je)%e%NU(is)

          IF (x.GE.-0.0) THEN
             rho=1.4
             u=0.
             v=0.
             p=1.
          ELSE 
             rho=8.
             u=8.25
             v=0.
             p=116.5
          ENDIF

          Vp(i_rho_vp)=rho
          Vp(i_vi1_vp)=u
          Vp(i_vi2_vp)=v
          Vp(i_pre_vp)=p 
          Vp(i_eps_vp)=EOSEpsilon(Vp)
          Vp(i_son_vp)=EOSson(Vp)
          Vp(i_tem_vp)=EOSTemperature(Vp)

          Var%Vp(:, Nu) = Vp

       ENDDO
    ENDDO

    DEALLOCATE(Vp)


  END SUBROUTINE CAS_DMR
  !==========================


  !=================================
  SUBROUTINE CAS_Sphere(DATA, Var)
    !=================================
    ! assume the wedge is located at (0,0)
    ! before shock (rho,u,v,p)=(8,8.25,0,116.5)
    ! after shock: (rho,u,v,p)=(1.4,0.0.1)
    ! angle of wedge: Pi/6
    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    CLASS(face_str),POINTER :: loc_ele



    INTEGER :: je, is
    REAL :: x, y, rho, u, v, w, p

    INTEGER :: Nu
    REAL, ALLOCATABLE,DIMENSION(:):: Vp

    !----------------------------------------
    ALLOCATE(Vp(SIZE(Var%Vp,1)))

    DO je = 1, SIZE(d_element)

       DO is = 1, d_element(je)%e%N_points

!          x = d_element(je)%e%Coords(1, is)
!          y = d_element(je)%e%Coords(2, is)
x=sum(d_element(je)%e%coords(:,is)**2)
          NU = d_element(je)%e%NU(is)

!!$          IF (x.GE.-1.250) THEN
!!$             rho=1.4
!!$             u=0.
!!$             v=0.
!!$             w=0.
!!$             p=1.
!!$          ELSE 
!!$             rho=8.
!!$             u=8.25
!!$             v=0.
!!$             w=0.
!!$             p=116.5
!!$
!!$          ENDIF
rho=1.4; u=0.; v=0.; w=0.
           if (x.le.0.1*0.1) then
           p=1000
           else
           p=0.1
           endif

          Vp(i_rho_vp)=rho
          Vp(i_vi1_vp)=u
          Vp(i_vi2_vp)=v
          Vp(i_vid_vp)=w
          Vp(i_pre_vp)=p 
          Vp(i_eps_vp)=EOSEpsilon(Vp)
          Vp(i_son_vp)=EOSson(Vp)
          Vp(i_tem_vp)=EOSTemperature(Vp)

          Var%Vp(:, Nu) = Vp

       ENDDO
    ENDDO

    DEALLOCATE(Vp)


  END SUBROUTINE CAS_Sphere
  !==========================


  !=================================
  SUBROUTINE CAS_vortex(DATA, Var)
    !=================================
    ! ----------------------------------------------------------------------

    ! Written by : Dinshaw Balsara

    ! Sets up an isentropic hydrodynamical vortex in 2d. Default is xy-plane
    ! with iz1 = 1. The problem is catalogued in Balsara & Shu (2000) JCP.
    ! Use a domain of [-5,5]x[-5,5] on a range of meshes
    ! going from 64x64 to 512x512 zones for this
    ! problem along with periodic boundaries (boundary condition # 4).
    ! "gamma_euler = 1.4" suggested. Suggested stopping time is
    ! "timestop = 10.0". The vortex propagates along the diagonal of
    ! the computing mesh in a form-preserving fashion. Can analyze the
    ! accuracy of the problem by comparing the numerical solution with
    ! the analytical solution.

    ! ----------------------------------------------------------------------
    REAL,PARAMETER:: pi=ACOS(-1.0), gamma_euler=1.4
    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    CLASS(face_str),POINTER :: loc_ele



    INTEGER :: je, is
    REAL :: x, y, rho, u, v, p

    INTEGER :: Nu
    REAL, ALLOCATABLE,DIMENSION(:):: Vp

    REAL xmin, xmax, ymin, ymax, zmin, zmax,&
         
         dtcur,&
         rho0, prs0, entropy0, vmag,  delta_t,&
         
         radius, tempaa, tempab, tempac,&
         costheta, sintheta, velrms, bfldrms,&
         VLXFN, VLYFN, TEMPFN,&
         vlx0, vly0, &
         xlo, xhi, ylo, yhi,&
         rhouse, prsuse, enguse, vlxuse, vlyuse, vlzuse,&
         prsmin, prsmax
    !----------------------------------------
    ALLOCATE(Vp(SIZE(Var%Vp,1)))

    vmag = 5.0 / ( 2.0 * pi)
    ! Default parameters for isentropic hydrodynamical vortex problem.
    ! Stop this one at a time of 10.0.

    rho0 = 1.0
    prs0 = 1.0
    entropy0 = prs0 / rho0**gamma_euler

    vlx0 = 1.0
    vly0 = 1.0
    xlo=-0.
    ylo=-0.
    DO je = 1, SIZE(d_element)

       DO is = 1, d_element(je)%e%N_points

          x = d_element(je)%e%Coords(1, is)-xlo
          y = d_element(je)%e%Coords(2, is)-ylo

          NU = d_element(je)%e%NU(is)

          vlxfn = - vmag * y * EXP ( 0.5*(1.0-x**2-y**2))+vlx0
          vlyfn =   vmag * x * EXP ( 0.5*(1.0-x**2-y**2))+vly0
          Tempfn= - ( gamma_euler - 1.0) * vmag**2 &
               * EXP (1.0-x**2-y**2) / ( 2.0 * gamma_euler)

          ! ----------------------------------------------------------------------





          rho = ( ( ( prs0 / rho0) + Tempfn)&
               / entropy0)**(1.0/(gamma_euler-1.0) )

          prsuse = rho * ( ( prs0 / rho0) + Tempfn)

!!$          prsmin = MIN ( prsmin, prsuse)
!!$          prsmax = MAX ( prsmax, prsuse)



          Vp(i_rho_vp)=rho
          Vp(i_vi1_vp)=vlxfn
          Vp(i_vi2_vp)=vlyfn
          Vp(i_pre_vp)=prsuse
          Vp(i_eps_vp)=EOSEpsilon(Vp)
          Vp(i_son_vp)=EOSson(Vp)
          Vp(i_tem_vp)=EOSTemperature(Vp)

          Var%Vp(:, Nu) = Vp

       ENDDO
    ENDDO

    DEALLOCATE(Vp)


  END SUBROUTINE CAS_Vortex
  !==========================


  SUBROUTINE CAS_4etats(DATA, Var)
    !=================================
    REAL,PARAMETER:: xlo=-1.0,xup=1.0,ylo=-1.0,yup=1.0
    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------


    INTEGER :: je, is
    REAL :: x, y, rho, u, v, p

    INTEGER :: Nu
    REAL, DIMENSION(SIZE(Var%Vp),4):: A
    REAL, ALLOCATABLE,DIMENSION(:):: Vp
    !----------------------------------------
    !                      3  ! 1
    !                     ____!_____
    !                         !
    !                      4  ! 2
    A(1,1)=0.5313; A(2:3,1)=0.0             ; A(4,1)=0.4!/0.4
    A(1,2)=1.0   ; A(2,2)=0.; A(3,2)=0.7276 ; A(4,2)=1.0!/0.4
    A(1,3)=1.    ; A(2,3)=0.7276; A(3,3)=0.0; A(4,3)=1.0!/0.4
    A(1,4)=0.8   ; A(2:3,4)=0.              ; A(4,4)=1.0!/0.4
!!$    A(1,1)=1.5   ; A(2:3,1)=0.0             ; A(4,1)=1.5!/0.4
!!$    A(1,2)=0.5323; A(2,2)=0.; A(3,2)=1.206  ; A(4,2)=0.3!/0.4
!!$    A(1,3)=0.5323; A(2,3)=1.206; A(3,3)=0.0 ; A(4,3)=0.3!/0.4
!!$    A(1,4)=0.1379; A(2:3,4)=1.206           ; A(4,4)=0.029!/0.4
    ALLOCATE(Vp(SIZE(Var%Vp,1)))

    DO je = 1, SIZE(d_element)

       DO is = 1, d_element(je)%e%N_points

          x = d_element(je)%e%Coords(1, is)
          y = d_element(je)%e%Coords(2, is)

          NU = d_element(je)%e%NU(is)

          IF (x.GT.0) THEN
             IF (y.GT.0) THEN !1
                rho=A(1,1)
                u=A(2,1)
                v=A(3,1)
                p=A(4,1)!*0.4
             ELSE !4
                rho=A(1,2)
                u=A(2,2)
                v=A(3,2)
                p=A(4,2)!*0.4
             ENDIF
          ELSE
             IF (y.GT.0) THEN !2
                rho=A(1,3)
                u=A(2,3)
                v=A(3,3)
                p=A(4,3)!*0.4
             ELSE !3
                rho=A(1,4)
                u=A(2,4)
                v=A(3,4)
                p=A(4,4)
             ENDIF
          ENDIF
          Vp(i_rho_vp)=rho
          Vp(i_vi1_vp)=u
          Vp(i_vi2_vp)=v
          Vp(i_pre_vp)=p
          Vp(i_eps_vp)=EOSEpsilon(Vp)
          Vp(i_son_vp)=EOSson(Vp)
          Vp(i_tem_vp)=EOSTemperature(Vp)

          Var%Vp(:, Nu) = Vp

       ENDDO
    ENDDO

    DEALLOCATE(Vp)

  END SUBROUTINE CAS_4etats
!========================

  SUBROUTINE CAS_sod(DATA, Var)
    !=================================
    REAL,PARAMETER:: xlo=-1.0,xup=1.0,ylo=-1.0,yup=1.0
    TYPE(donnees),   INTENT(IN)    :: DATA
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------


    INTEGER :: je, is
    REAL :: x, y, rho, u, v, p

    INTEGER :: Nu
    REAL, DIMENSION(SIZE(Var%Vp),4):: A
    REAL, ALLOCATABLE,DIMENSION(:):: Vp
    !----------------------------------------
    !                      3  ! 1
    !                     ____!_____
    !                         !
    !                      4  ! 2
    A(1,1)=0.5313; A(2:3,1)=0.0             ; A(4,1)=0.4!/0.4
    A(1,2)=1.0   ; A(2,2)=0.; A(3,2)=0.7276 ; A(4,2)=1.0!/0.4
    A(1,3)=1.    ; A(2,3)=0.7276; A(3,3)=0.0; A(4,3)=1.0!/0.4
    A(1,4)=0.8   ; A(2:3,4)=0.              ; A(4,4)=1.0!/0.4
!!$    A(1,1)=1.5   ; A(2:3,1)=0.0             ; A(4,1)=1.5!/0.4
!!$    A(1,2)=0.5323; A(2,2)=0.; A(3,2)=1.206  ; A(4,2)=0.3!/0.4
!!$    A(1,3)=0.5323; A(2,3)=1.206; A(3,3)=0.0 ; A(4,3)=0.3!/0.4
!!$    A(1,4)=0.1379; A(2:3,4)=1.206           ; A(4,4)=0.029!/0.4
    ALLOCATE(Vp(SIZE(Var%Vp,1)))

    DO je = 1, SIZE(d_element)

       DO is = 1, d_element(je)%e%N_points

          x = d_element(je)%e%Coords(1, is)
          y = d_element(je)%e%Coords(2, is)

          NU = d_element(je)%e%NU(is)

              IF (y.GT.0) THEN !1
                rho=1.
                u=0.0
                v=0.0
                p=1.0
             ELSE !4
                rho=0.125
                u=0.
                v=0.
                p=0.1
             ENDIF
           Vp(i_rho_vp)=rho
          Vp(i_vi1_vp)=u
          Vp(i_vi2_vp)=v
          Vp(i_pre_vp)=p
          Vp(i_eps_vp)=EOSEpsilon(Vp)
          Vp(i_son_vp)=EOSson(Vp)
          Vp(i_tem_vp)=EOSTemperature(Vp)

          Var%Vp(:, Nu) = Vp

       ENDDO
    ENDDO

    DEALLOCATE(Vp)

  END SUBROUTINE CAS_sod

  !==========================
!!$       !=============================
!!$       SUBROUTINE CAS_Test(Data, Var)
!!$       !=============================
!!$
!!$         IMPLICIT NONE
!!$
!!$         TYPE(donnees),   INTENT(IN)    :: DATA
!!$         TYPE(Variables), INTENT(INOUT) :: Var
!!$         !--------------------------------------
!!$
!!$         REAL :: x, y, z, eta, psi, mu
!!$
!!$         INTEGER :: je, is, NU
!!$         !---------------------------------------
!!$         
!!$         mu = 0.01d0
!!$
!!$         DO je = 1, SIZE(d_element)
!!$
!!$            DO is = 1, d_element(je)%e%N_points
!!$
!!$               NU = d_element(je)%e%NU(is)
!!$               x = d_element(je)%e%Coords(1, is)
!!$               y = d_element(je)%e%Coords(2, is)         
!!$!               z = d_element(je)%e%Coords(3, is)
!!$
!!$!!                Var%Vp(:, NU) = 1.d0
!!$
!!$               Var%Vp(i_rho_vp, NU) = 1.d0 + SIN(Pi*x)*COS(Pi*x)*SIN(Pi*y)*COS(Pi*y)!*SIN(Pi*z)*COS(Pi*z)
!!$               Var%Vp(i_vi1_vp, NU) = 0.25d0 + SIN(1.d0*Pi*x)*COS(1.d0*Pi*x)*SIN(1.d0*Pi*y)*COS(1.d0*Pi*y)!*SIN(1.d0*Pi*z)*COS(1.d0*Pi*z)
!!$               Var%Vp(i_vi2_vp, NU) = 0.d0 + SIN(1.d0*Pi*x)*COS(1.d0*Pi*x)*SIN(1.d0*Pi*y)*COS(1.d0*Pi*y)!*SIN(1.d0*Pi*z)*COS(1.d0*Pi*z)
!!$               Var%Vp(i_vid_vp, NU) = 0.d0 + SIN(1.d0*Pi*x)*COS(1.d0*Pi*x)*SIN(1.d0*Pi*y)*COS(1.d0*Pi*y)!*SIN(1.d0*Pi*z)*COS(1.d0*Pi*z)
!!$               Var%Vp(i_pre_Vp, NU) = 1.d0/1.4d0 + SIN(1.d0*Pi*x)*COS(1.d0*Pi*x)*SIN(1.d0*Pi*y)*COS(1.d0*Pi*y)!*SIN(1.d0*Pi*z)*COS(1.d0*Pi*z)
!!$               Var%Vp(i_nuts_Vp, NU) = 0.01d0 + SIN(1.d0*Pi*x)*COS(1.d0*Pi*x)*SIN(1.d0*Pi*y)*COS(1.d0*Pi*y)
!!$
!!$!               Var%Vp(i_eps_vp, NU) = EOSEpsilon( Var%Vp(:, NU) )
!!$!               Var%Vp(i_son_Vp, NU) = EOSson( Var%Vp(:, NU) )
!!$!               Var%Vp(i_tem_vp, Nu) = EOSTemperature(  Var%Vp(:, NU) )
!!$
!!$               Var%Vp(i_tem_vp, Nu) = 1.4d0*Var%Vp(i_pre_Vp, NU)/Var%Vp(i_rho_vp, NU)
!!$               Var%Vp(i_son_Vp, NU) = SQRT( Var%Vp(i_tem_Vp, NU) )                
!!$               Var%Vp(i_eps_vp, NU) = Var%Vp(i_pre_Vp, NU)/(Var%Vp(i_rho_vp, NU)*0.4d0)
!!$
!!$!!               eta = DCOS(45.d0*PI/180.d0)*x - DSIN(45.d0*PI/180.d0)*y
!!$!!               psi = DSIN(45.d0*PI/180.d0)*x + DCOS(45.d0*PI/180.d0)*y
!!$!!
!!$!!               Var%Vp(i_vi1_vp, NU) = -DCOS(2.d0*PI*eta)* & 
!!$!!	                               EXP(0.5d0*psi*( 1.d0 - SQRT(1.d0 + 16.d0*(PI**2)*mu**2) )/mu)
!!$
!!$            ENDDO
!!$
!!$         ENDDO
!!$
!!$!!         CALL Near_Wall_Distance_seq()
!!$!!
!!$!!         
!!$!!DO je = 1, SIZE(d_element)
!!$!!   DO is = 1, d_element(je)%e%N_points
!!$!!
!!$!!      NU = d_element(je)%e%NU(is)
!!$!!      Var%Vp(:, NU) = 1.d0
!!$!!
!!$!!      Var%Vp(i_vi1_vp, NU) = d_element(je)%e%d_min(is)
!!$!!
!!$!!   ENDDO
!!$!!ENDDO
!!$
!!$       END SUBROUTINE CAS_Test       
!!$       !======================
!!!!!

  SUBROUTINE Model_Reprise(DATA,myunit)
    IMPLICIT NONE
    INTEGER, INTENT(in):: myunit
    TYPE(donnees),   INTENT(IN) :: DATA
    INTEGER:: ies

    WRITE(MyUnit) ( DATA%VarNames(ies),  &
         ies = 1, DATA%NvarPhy )


  END SUBROUTINE Model_Reprise

  SUBROUTINE Model_check_variables(Var)
    TYPE(Variables), INTENT(in):: Var
    IF (MINVAL(Var%Vp(i_rho_vp, : )) <= 0.0) THEN
       PRINT *,  " kt == ", Var%kt
       PRINT *,  " :: ERREUR : rho <= 0.0"
       PRINT*, "rho ", MINVAL(Var%Vp(i_rho_vp, : )),MAXVAL(Var%Vp(i_rho_vp, : ))
       CALL stop_run()
    END IF
  END SUBROUTINE Model_check_variables

  SUBROUTINE Model_Check_init(Var)
    TYPE(Variables), INTENT(in):: Var

  END SUBROUTINE Model_Check_Init

  SUBROUTINE Model_Parameter_Special_Read(MyUnit,DATA)
    IMPLICIT NONE
    ! aim: provide the names of variables, adimensionalisation etc
    TYPE(donnees), INTENT(Out):: DATA
    INTEGER, INTENT(in):: MyUnit
    INTEGER:: ies

    READ(MyUnit) ( DATA%VarNames(ies), &
         ies = 1, DATA%NvarPhy)

    READ(MyUnit) DATA%LoiEtat,     &
         DATA%sel_loivisq, &
         DATA%LoiTh

    ! rho_oo, V_oo, T_oo, P_oo
    READ(MyUnit) DATA%VarPhyInit(1,             :), &
         &       DATA%VarPhyInit(2:DATA%Ndim+1, :), &
         &       DATA%VarPhyInit(DATA%Ndim+5,   :), &
         &       DATA%VarPhyInit(DATA%Ndim+3,   :)

    READ(MyUnit) DATA%RhoRef, &
         &       DATA%VRef,   &
         &       DATA%LRef,   &
         &       DATA%TRef

  END SUBROUTINE Model_Parameter_Special_Read


  SUBROUTINE Model_Parameter_Special_Write(MyUnit,DATA)
    IMPLICIT NONE
    ! aim: provide the names of variables, adimensionalisation etc
    TYPE(donnees), INTENT(in):: DATA
    INTEGER, INTENT(in):: MyUnit
    INTEGER:: ies

    WRITE(MyUnit) ( DATA%VarNames(ies),  &
         &         ies = 1, DATA%NvarPhy )

    WRITE(MyUnit) DATA%LoiEtat,     &
         &        DATA%sel_loivisq, &
         &        DATA%LoiTh

    WRITE(MyUnit) DATA%VarPhyInit(i_rho_vp, 1),          &
         &        DATA%VarPhyInit(i_vi1_vp:i_vid_vp, 1), &
         &        DATA%VarPhyInit(i_tem_vp, 1),          &
         &        DATA%VarPhyInit(i_pre_vp, 1)

    WRITE(MyUnit) DATA%RhoRef, &
         &        DATA%VRef,   &
         &        DATA%LRef,   &
         &        DATA%TRef
  END SUBROUTINE Model_Parameter_Special_Write


END MODULE ModelInterfaces

