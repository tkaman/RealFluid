MODULE gradient_recovery

  ! In this version the SPR_ZZ is implemented
  ! with the direct computation on the boundary
  ! and the extrapolation is done only when there
  ! aren't enought points to solve the non-linear problem

  USE LesTypes
  USE PLinAlg
  USE LibComm

  IMPLICIT NONE

  INTEGER, PARAMETER :: GRAD_GG    = cs_RGrad_GG
  INTEGER, PARAMETER :: GRAD_GG_b  = cs_RGrad_GG_b
  INTEGER, PARAMETER :: GRAD_LSQ   = cs_RGrad_LSQ
  INTEGER, PARAMETER :: GRAD_SPRZZ = cs_RGRAD_SPRZZ
  INTEGER, PARAMETER :: GRAD_SPRZN = cs_RGRAD_SPRZN
  !------------------------------------------------

  TYPE :: S_matrix
     REAL, ALLOCATABLE, DIMENSION(:,:) :: MM
     INTEGER :: idx
  END TYPE S_matrix  
  TYPE(S_matrix), DIMENSION(:), ALLOCATABLE :: inv_AA, AA

  TYPE :: S_vector
     REAL, DIMENSION(:,:), ALLOCATABLE :: v
     INTEGER :: idx
  END TYPE S_vector
  TYPE(S_vector), DIMENSION(:), ALLOCATABLE :: b, s_coeff

  TYPE :: Sa_vector
     REAL, DIMENSION(:,:,:), ALLOCATABLE :: v
     INTEGER, DIMENSION(:), ALLOCATABLE :: idx
  END TYPE Sa_vector
  TYPE(Sa_vector), DIMENSION(:), ALLOCATABLE :: b_a, p_coeff

  TYPE :: con_Nu
     INTEGER, DIMENSION(:), ALLOCATABLE :: con
     REAL :: h_z
  END TYPE con_Nu
  TYPE(con_NU), DIMENSION(:), ALLOCATABLE :: nn_Nu

  LOGICAL :: PREPRO_LSQ   = .FALSE.
  LOGICAL :: PREPRO_SPRZZ = .FALSE.
  LOGICAL :: PREPRO_SPRZN = .FALSE.
  !------------------------------------------------

CONTAINS

#ifdef NAVIER_STOKES

  !=================================
  SUBROUTINE Grad_recovery(Com, Var)
  !=================================

    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    !------------------------------------

    SELECT CASE(Data%Rgrad)

    CASE(GRAD_GG)

       CALL Green_Gauss_recovery(Com, Var)

    CASE(GRAD_GG_b)

       CALL Green_Gauss_recovery_b(Var)

    CASE(GRAD_LSQ)

       CALL Least_Square_recovery(Var)

    CASE(GRAD_SPRZZ)

       CALL SPR_ZZ_recovery(Com, Var)

    CASE(GRAD_SPRZN)
       
       ! Don't use it
       STOP
       CALL SPR_ZN_recovery(var)

    CASE DEFAULT

       WRITE(*,*) 'ERROR: unknown gradient recovery method'
       STOP

    END SELECT  

  END SUBROUTINE Grad_recovery
  !============================ 

!---------------------------------------------------------------------
!---------------------------------------------------------------------
!---------------------------------------------------------------------

  !========================================
  SUBROUTINE Green_Gauss_recovery(Com, Var)
  !========================================

    IMPLICIT NONE
    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    !------------------------------------

    TYPE(element_Str) :: loc_ele

    REAL,    DIMENSION(:,:), ALLOCATABLE :: Vc
    INTEGER, DIMENSION(:),   ALLOCATABLE :: Nu
    REAL,    DIMENSION(:),   ALLOCATABLE :: diag

    REAL, DIMENSION(DATA%Ndim) :: D_U
    
    INTEGER :: N_var, N_dofs, N_dim
    INTEGER :: je, i, k, id, jf
    !------------------------------------

    N_dim = Data%NDim
    N_var = Data%NVar

    ALLOCATE( diag(Var%NCells) )

    diag = 0.d0;  Var%D_Ua = 0.d0

    DO je = 1, SIZE(d_element)

       loc_ele = d_element(je)%e
       
       N_dofs = loc_ele%N_Points

       ALLOCATE( NU(N_dofs), Vc(N_var, N_dofs) )

       DO k = 1, N_dofs

          Nu(k) = loc_ele%Nu(k)

          Vc(:, k) = Var%Ua(:, Nu(k))

       ENDDO

       DO k = 1, N_dofs

#ifndef SEQUENTIEL
          ! Exclude nodes which are on the overlaps,
          ! the gradients here are computed on the 
          ! connected sub-domians and then are exchanged
          !IF( Nu(k) > Var%NCellsIN ) CYCLE
#endif

          DO i = 1, N_var
             
             DO id = 1, N_dim
                D_U(id) = SUM( Vc(i, :) * loc_ele%D_phi_k(id, :, k) )
             ENDDO

             Var%D_Ua(:, i, Nu(k)) = Var%D_UA(:, i, Nu(k)) + &
                                     D_U * loc_ele%volume

          ENDDO

       ENDDO

       diag(Nu) = diag(Nu) + loc_ele%volume

       DEALLOCATE( Nu, Vc )

    ENDDO

    DO id = 1, N_dim

       DO i = 1, N_var

          Var%D_Ua(id, i, :) = Var%D_Ua(id, i, :)/diag(:)

       ENDDO

    ENDDO

!    CALL EchangeGrad(Com, Var%D_Ua)

    DEALLOCATE( diag )

#ifndef SEQUENTIEL
    !CALL EchangeGrad(Com, Var%D_Ua)
#endif

  END SUBROUTINE Green_Gauss_recovery
  !=================================== 
 
  !=====================================
  SUBROUTINE Green_Gauss_recovery_b(Var)
  !=====================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    !------------------------------------

    TYPE(element_Str) :: loc_ele

    REAL,    DIMENSION(:,:), ALLOCATABLE :: Vc
    INTEGER, DIMENSION(:),   ALLOCATABLE :: Nu
    REAL,    DIMENSION(:),   ALLOCATABLE :: diag

    INTEGER,                      POINTER :: N_quad 
    INTEGER,      DIMENSION(:),   POINTER :: loc
    REAL(KIND=8), DIMENSION(:,:), POINTER :: p
    REAL(KIND=8), DIMENSION(:,:), POINTER :: n
    REAL(KIND=8), DIMENSION(:),   POINTER :: w
    !-------------------------------------------

    REAL :: V_q

    REAL, DIMENSION(DATA%Ndim) :: D_U
    
    INTEGER :: N_var, N_dofs, N_dim, Nu_i, l_n
    INTEGER :: je, i, k, id, jf, iq
    !--------------------------------------------

    N_dim = Data%NDim
    N_var = Data%NVar

    ALLOCATE( diag(Var%NCells) )

    diag = 0.d0;  Var%D_Ua = 0.d0

    DO je = 1, SIZE(d_element)

       loc_ele = d_element(je)%e
       
       N_dofs = loc_ele%N_Points

       ALLOCATE( NU(N_dofs), Vc(N_var, N_dofs) )

       DO k = 1, N_dofs

          Nu(k) = loc_ele%Nu(k)

          Vc(:, k) = Var%Ua(:, Nu(k))

       ENDDO

       DO k = 1, N_dofs

          DO i = 1, N_var
 
             D_U = 0.d0
             DO jf = 1, loc_ele%N_faces

                N_quad   => loc_ele%faces(jf)%f%N_quad
                loc      => loc_ele%faces(jf)%f%l_nu
                p        => loc_ele%faces(jf)%f%phi_q
                w        => loc_ele%faces(jf)%f%w_q
                n        => loc_ele%faces(jf)%f%n_q

                DO iq = 1, N_quad

                   V_q = SUM( Vc(i, loc)*p(:, iq) )

                   D_U = D_U + V_q * n(:, iq) * w(iq)

                ENDDO                

                NULLIFY( N_quad, loc, p, w, n )

             ENDDO

             D_U = D_U / loc_ele%Volume

             Var%D_Ua(:, i, Nu(k)) = Var%D_UA(:, i, Nu(k)) + &
                                     D_U * loc_ele%volume

          ENDDO

       ENDDO

       diag(Nu) = diag(Nu) + loc_ele%volume

       DEALLOCATE( Nu, Vc )

    ENDDO

    DO id = 1, N_dim

       DO i = 1, N_var

          Var%D_Ua(id, i, :) = Var%D_Ua(id, i, :)/diag(:)

       ENDDO

    ENDDO

    DEALLOCATE( diag )

    DO je = 1, SIZE(d_element)

       DO jf = 1, d_element(je)%e%N_faces

          IF(d_element(je)%e%faces(jf)%f%c_ele == 0) THEN
             
             DO i = d_element(je)%e%faces(jf)%f%N_verts+1, &
                    d_element(je)%e%faces(jf)%f%N_points
                               
                Nu_i = d_element(je)%e%faces(jf)%f%Nu(i)
                
                l_n = d_element(je)%e%faces(jf)%f%l_nu(i)

                DO k = 1, N_var
                   DO id = 1, N_dim
                      Var%D_Ua(id, k, Nu_i) = SUM( Var%Ua(k, d_element(je)%e%NU) * &
                                                   d_element(je)%e%D_phi_k(id, :, l_n) )
                   ENDDO
                ENDDO

             ENDDO             

          ENDIF

       ENDDO

    ENDDO

  END SUBROUTINE Green_Gauss_recovery_b
  !====================================

  !====================================
  SUBROUTINE Least_Square_recovery(Var) 
  !====================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    !------------------------------------

    TYPE(element_Str) :: ele
    !-----------------------------------------------

    REAL, DIMENSION(Data%NDim) :: Dxx_ik
    REAL, DIMENSION(Data%NVar) :: DU_ik

    REAL, DIMENSION(:), ALLOCATABLE :: GG, rhs
    
    REAL :: w_ik

    INTEGER :: Nu_i, NU_k

    INTEGER :: je, i, k, j, l, id
    !------------------------------------

    IF( .NOT. PREPRO_LSQ ) THEN

       ALLOCATE( nn_NU(Var%NCells) )

       ! Once for all
       CALL LSQ_build_matrix(Var%NCells, nn_NU)

       PREPRO_LSQ = .TRUE.

    ENDIF

    b(:)%idx = 0

    DO je = 1, SIZE(d_element)

       ele = d_element(je)%e
       
       DO i = 1, ele%N_points

          Nu_i = ele%NU(i)

           DO k = 1, ele%N_points

              IF( k == i ) CYCLE

              Nu_k = ele%NU(k)

              DO j = 1, SIZE(nn_NU(Nu_i)%con)

                 IF( Nu_k == nn_NU(Nu_i)%con(j) ) THEN

                    ! Easy way to mark a found node
                    nn_NU(Nu_i)%con(j) = -nn_NU(Nu_i)%con(j)

                    Dxx_ik = ele%Coords(:, k) - ele%Coords(:, i)

                    b(Nu_i)%idx = b(Nu_i)%idx + 1

                    w_ik = 1.d0 / DSQRT( SUM(Dxx_ik**2) )

                    DU_ik = Var%Ua(:, Nu_k) - Var%Ua(:, Nu_i)

                    DO l = 1, Data%NVar

                       b(Nu_i)%v(b(Nu_i)%idx, l) = w_ik * DU_ik(l)

                    ENDDO  ! l <= #Var

                 ENDIF

              ENDDO  ! j <= #Con

           ENDDO ! k <= #N_points
          
       ENDDO ! i <= #N_points

    ENDDO ! je <= #elements

    ALLOCATE( rhs(SIZE(AA(1)%MM, 1)), &
               GG(SIZE(AA(1)%MM, 1))  )
    !         dummy ------^

    DO i = 1, Var%NCells

       DO l = 1, Data%NVar

          rhs = MATMUL( AA(i)%MM, b(i)%v(:, l) )
                ! A^T ---^

          GG = MATMUL( inv_AA(i)%MM, rhs )

          Var%D_Ua(:, l, i) = GG(1: Data%NDim)

       ENDDO
       
       ! Back to the positive numeration
       nn_NU(i)%con = -nn_NU(i)%con

    ENDDO

    DEALLOCATE( rhs, GG )

  END SUBROUTINE Least_Square_recovery
  !===================================
  
  !========================================
  SUBROUTINE LSQ_build_matrix(N_DOF, nn_NU)
  !========================================
  !
  ! Construct the Least square matrices for each DOF
  ! Needed to be done just once for all  
  !
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: N_DOF

    TYPE(con_NU), DIMENSION(:), INTENT(OUT) :: nn_Nu
    !-----------------------------------------------

    TYPE(element_Str) :: ele
    !-------------------------------------------
    INTEGER, DIMENSION(:), POINTER :: Nu
   
    INTEGER, DIMENSION(:),  ALLOCATABLE :: temp
    REAL,   DIMENSION(:,:), ALLOCATABLE :: A_temp, AA_TR    

    LOGICAL :: find
    !-------------------------------------------

    REAL, DIMENSION(Data%NDim) :: Dxx_ik
    REAL :: w_ik
    !-------------------------------------------

    INTEGER :: N_dim, Ns, n, N_r, N_c
    INTEGER :: Nu_i, NU_k
    INTEGER :: i, k, je, j, l, m, id
    !---------------------------------------

    N_dim = Data%Ndim

    ! Node-to-bouble connectivity
    !--------------------------------------------------------
    DO je = 1, SIZE(d_element)

       Ns = d_element(je)%e%N_points
       Nu => d_element(je)%e%Nu

       DO i = 1, Ns

          IF( .NOT. ALLOCATED(nn_Nu(Nu(i))%con) ) THEN

             ALLOCATE(nn_Nu(Nu(i))%con(Ns))

             nn_Nu(Nu(i))%con = Nu

          ELSE

             DO k = 1, Ns

                n = SIZE(nn_Nu(Nu(i))%con)

                find = .FALSE.

                DO j = 1, n

                   IF(nn_Nu(Nu(i))%con(j) == Nu(k)) THEN
                      find = .TRUE.
                      EXIT
                   ENDIF

                ENDDO

                IF(.NOT. find) THEN

                   ALLOCATE( temp(SIZE(nn_Nu(Nu(i))%con)) )
                   temp = nn_Nu(Nu(i))%con

                   DEALLOCATE( nn_Nu(Nu(i))%con )
                     ALLOCATE( nn_Nu(Nu(i))%con(SIZE(temp)+1) )

                   nn_Nu(Nu(i))%con = (/ temp, Nu(k) /)

                   DEALLOCATE(temp)                      

                ENDIF

             ENDDO ! k <= #N_points

          ENDIF ! Not allocated

       ENDDO ! i <= #N_points

    ENDDO ! je <= #elements
    !--------------------------------------------------------

    ! Number of column of the matrices
    SELECT CASE(N_dim)    
    CASE(2)
       IF(Data%NOrdre == 2) THEN
          N_c = 2
       ELSEIF(Data%NOrdre == 3) THEN
          N_c = 5
       ENDIF
    CASE(3)
       N_c = 3**(Data%NOrdre - 1)
    END SELECT

    ALLOCATE( AA(N_DOF) )
    
    ! Construct the matrices of the least-square
    ! method, for each DOF
    !---------------------------------------------
    DO je = 1, SIZE(d_element)

       ele = d_element(je)%e

       DO i = 1, ele%N_points

          Nu_i = ele%NU(i)

          N_r = SIZE(nn_NU(Nu_i)%con) - 1

          IF (.NOT. ALLOCATED(AA(Nu_i)%MM) ) THEN
             ALLOCATE( AA(Nu_i)%MM(N_r, N_c) )
             AA(Nu_i)%idx = 0
          ENDIF

          DO k = 1, ele%N_points

             Nu_k = ele%NU(k)

             IF( k == i ) CYCLE

             DO j = 1, SIZE(nn_NU(Nu_i)%con)

                IF( Nu_k == nn_NU(Nu_i)%con(j) ) THEN
                   
                   ! Easy way to mark a node found
                   nn_NU(Nu_i)%con(j) = -nn_NU(Nu_i)%con(j)

                   Dxx_ik = ele%Coords(:, k) - ele%Coords(:, i)

                   AA(Nu_i)%idx = AA(Nu_i)%idx + 1
                   
                   w_ik = 1.d0 / DSQRT( SUM(Dxx_ik**2) )

                   AA(Nu_i)%MM( AA(Nu_i)%idx, 1:N_dim) = w_ik * Dxx_ik

                   IF(Data%NOrdre == 3) THEN
                     
                      AA(Nu_i)%MM( AA(Nu_i)%idx, N_dim+1:2*N_dim) = w_ik * Dxx_ik**2

                      id = 0
                      DO l = 1, N_dim-1
                         DO m = l+1, N_dim
                            id = id + 1
                            AA(Nu_i)%MM( AA(Nu_i)%idx, 2*N_dim+id) = w_ik * Dxx_ik(l)*Dxx_ik(m)
                         ENDDO
                      ENDDO

                   ENDIF

                ENDIF
                
             ENDDO ! j <= #Con

          ENDDO ! k <= #N_points

       ENDDO ! i <= #N_points

    ENDDO ! je <= #elements
    !---------------------------------------------

    ALLOCATE( inv_AA(N_DOF) )
    ALLOCATE(      b(N_DOF) )

    DO i = 1, N_DOF

       N_r = SIZE(AA(i)%MM, 1)
       N_c = SIZE(AA(i)%MM, 2)
       
       ! Back to the positive numeration
       nn_NU(i)%con = -nn_NU(i)%con

       ALLOCATE( AA_TR(N_c, N_r) )

       AA_TR = TRANSPOSE( AA(i)%MM )

       ALLOCATE( A_temp(N_c, N_c) )

       A_temp = MATMUL( AA_TR, AA(i)%MM )

       ALLOCATE( inv_AA(i)%MM(N_c, N_c) )

       inv_AA(i)%MM = InverseLU( A_temp )

       DEALLOCATE( AA(i)%MM )
         ALLOCATE( AA(i)%MM(N_c, N_r) )
         
       ! Store the transpose matrix
       ! in the original matrix
       AA(i)%MM = AA_TR

       DEALLOCATE( AA_TR, A_temp )

       !----------------------------------

       !RHS terms
       ALLOCATE( b(i)%v(N_r, Data%Nvar) )
       b(i)%v = 0.d0
       b(i)%idx = 0

    ENDDO

  END SUBROUTINE LSQ_build_matrix
  !==============================

  !===================================
  SUBROUTINE SPR_ZZ_recovery(Com, Var)
  !===================================

    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    !------------------------------------

    TYPE(element_Str) :: ele
   
    REAL, DIMENSION(:), ALLOCATABLE :: rhs

    REAL, DIMENSION(Data%NDim) :: GG, xx_i, xx_0, d_xx

    REAL :: d_min

    INTEGER :: N_DOF_v
    INTEGER :: je, iq, k, i, l, N_dim, Nu_i, id
    INTEGER :: jb, Nu_k, n, jd, n_e, j, k_min, m
    LOGICAL :: found
    !---------------------------------------

    IF( .NOT. PREPRO_SPRZZ ) THEN
     
       ! Once for all
       CALL SPRZZ_build_matrix(Var%NCells, Var%NCellsIN)

       PREPRO_SPRZZ = .TRUE.

    ENDIF

    N_dim = Data%NDim
    
    ! Reset
    DO i = 1, SIZE(b_a)

       IF( ALLOCATED(b_a(i)%idx) ) THEN
          b_a(i)%idx = 0
       ENDIF

       Var%D_Ua(:,:, i) = 0.d0

    ENDDO

    ! Construction of the RHS terms
    ! for each domain mesh vertex
    !--------------------------------
    DO i = 1, SIZE(b_a)

       IF ( ALLOCATED(b_a(i)%v) ) THEN

          DO j = 1, SIZE(nn_NU(i)%con)

             je = nn_NU(i)%con(j)

             ele = d_element(je)%e

             DO iq = 1, ele%N_sampl
                
                DO l = 1, Data%NVar

                   DO k = 1, N_dim
                      GG(k) = SUM( Var%Ua(l, ele%Nu) * ele%D_phi_R(k, :, iq) )
                   ENDDO

                   b_a(i)%idx(l) = b_a(i)%idx(l) + 1

                   b_a(i)%v( b_a(i)%idx(l), :, l ) = GG

                ENDDO ! l <= # var

             ENDDO ! iq <= # recovery points

          ENDDO ! j <= # ele bubble

       ENDIF

    ENDDO ! i <= # DOF
    !-------------------------------

    ! Polynomial coeff. for the recoverd
    ! gradient at each domain mesh vertex
    !--------------------------------
    DO i = 1, SIZE(b_a)
       
       IF( ALLOCATED(AA(i)%MM) ) THEN

          ALLOCATE( rhs(SIZE(AA(i)%MM, 1)) )
          !  Transpose  ------^

          DO l = 1, Data%NVar

             DO k = 1, N_dim

                rhs = MATMUL( AA(i)%MM, b_a(i)%v(:, k, l) )

                p_coeff(i)%v(:, k, l) = MATMUL( inv_AA(i)%MM, rhs )
                
                ! Gradient at the domain vertex 
                Var%D_Ua(k, l, i) = p_coeff(i)%v(1, k, l)
                      ! At the origin of the ----^
                      ! local reference syst

             ENDDO

          ENDDO

          DEALLOCATE( rhs )

       ENDIF
       
    ENDDO
    !--------------------------------

    !//////////////////////////////////////////
    !// For extra HO DOFs on the domain, the //
    !// gradient is computed extrapolating   //
    !// the value from the nearest vertex    //
    !// on the face                          //
    !//////////////////////////////////////////
    IF ( Data%NOrdre > 2 ) THEN

        DO je = 1, SIZE(d_element)

           ele = d_element(je)%e
           
           DO i = ele%N_Verts+1, ele%N_points

              Nu_i = ele%NU(i)
              xx_i = ele%Coords(:, i)

              ! Find the nearest vertex to Nu_i 
              d_min = HUGE(1.d0)
              DO k = 1, ele%N_verts

                 Nu_k = ele%NU(k)
                 xx_0 = ele%Coords(:, k)
                 d_xx = xx_i - xx_0

                  IF( SQRT(SUM(d_xx**2)) < d_min .AND. &
                       ALLOCATED( p_coeff(Nu_k)%v ) ) THEN
                     d_min = SQRT(SUM(d_xx**2))
                     k_min = k
                  ENDIF

              ENDDO
              !------------

              Nu_k = ele%NU(k_min)
              xx_0 = ele%Coords(:, k_min)
              d_xx = xx_i - xx_0

              IF( .NOT. ALLOCATED(p_coeff(Nu_k)%v) ) CYCLE

              ! Gradient evaluation
              DO l = 1, Data%NVar
       
                 DO jd = 1, N_dim
                    ! 0-th order term
                    Var%D_Ua(jd, l, Nu_i) = p_coeff(Nu_k)%v(1, jd, l)
                    ! 1-st order terms
                    id = 1
                    DO j = 1, N_dim
                       id = id+1
                       Var%D_Ua(jd, l, Nu_i) = Var%D_Ua(jd, l, Nu_i) + &
                                               p_coeff(Nu_k)%v(id, jd, l)*d_xx(j)
                    ENDDO
                    ! 2-nd order terms
                    DO j = 1, N_dim
                       id = id+1
                       Var%D_Ua(jd, l, Nu_i) = Var%D_Ua(jd, l, Nu_i) + &
                                               p_coeff(Nu_k)%v(id, jd, l)*d_xx(j)**2
                    ENDDO
                    ! 2-nd order mixed terms
                    DO n = 1, N_dim-1
                       DO m = n+1, N_dim
                          id = id + 1
                          Var%D_Ua(jd, l, Nu_i) = Var%D_Ua(jd, l, Nu_i) + &
                                                  p_coeff(Nu_k)%v(id, jd, l)*d_xx(n)*d_xx(m)
                       ENDDO
                    ENDDO
                    
                 ENDDO ! id <= #N_dim
                   
              ENDDO ! l <= #N_var

           ENDDO          

        ENDDO ! je <= # ele

    ENDIF ! If HO
    !--------------------------------------------

#ifndef SEQUENTIEL
    CALL EchangeGrad(Com, Var%D_Ua)
#endif

  END SUBROUTINE SPR_ZZ_recovery
  !==============================  

  !========================================
  SUBROUTINE SPRZZ_build_matrix(N_DOF, NIn)
  !========================================

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: N_DOF
    INTEGER, INTENT(IN) :: NIn
    !---------------------------

    TYPE(element_Str) :: ele
    TYPE(face_str)    :: b_ele

    REAL, DIMENSION(Data%NDim) :: xx, xi, xx_i, xx_k

    INTEGER, DIMENSION(:), ALLOCATABLE :: NN_A

    REAL, DIMENSION(:,:), ALLOCATABLE :: temp, AA_Tr

    LOGICAL, DIMENSION(:), ALLOCATABLE :: seen

    REAL :: d_ik, d_ik_min

    LOGICAL :: found

    INTEGER :: c_ele, ke, je, jb, jf, n_r, n_c
    INTEGER :: N_o, N_o_lim, N_e, N_dim, N_Order, Nu_k 
    INTEGER :: i, j, iq, k, id, l, m, n, Nu_i, Nu_k_min
    !------------------------------------------

    ALLOCATE( nn_NU(N_DOF) )

    N_dim = Data%NDim
    N_Order = Data%NOrdre

    ! # polynomial coefficients
    IF( N_dim ==2 ) THEN
       N_o = N_Order*(N_Order + 1)/2
    ELSE
       N_o = N_Order*(N_Order + 1)*(N_Order + 2)/6
    ENDIF

    !----------------------------
    ! # of elements belonging to 
    ! bubble of the vertex i
    !-------------------------------------------------
    ALLOCATE( seen(N_DOF) )
    seen = .FALSE.

    DO je = 1, SIZE(d_element)

       ele = d_element(je)%e

       DO k = 1, ele%N_verts ! Verticies only

          Nu_k = ele%NU(k)

#ifndef SEQUENTIEL
          ! Exclude nodes which are on the overlaps,
          ! the gradients here are computed on the 
          ! connected sub-domians and then are exchanged
          IF( Nu_k > NIn ) CYCLE
#endif

          IF( .NOT. seen(Nu_k) ) THEN

             seen(Nu_k) = .TRUE. 
             
             m = SIZE(ele%b_e_con(k)%cn_ele)

             ALLOCATE( nn_NU(Nu_k)%con(m) )

             nn_NU(Nu_k)%con = ele%b_e_con(k)%cn_ele

          ENDIF
          
       ENDDO
       
    ENDDO
    !-------------------------------------------------

    !////////////////////////////////////////////////////////
    !//     SPECIAL TREATMENT FOR THE BOUNDARY NODES       //
    !//                                                    //
    !// Strategy A: Enlarge connectivity by including      //
    !//             domain elements having common faces    //
    !//             and whith no faces on the boundary     //
    !//                                                    //
    !// Strategy B: If Strategy A doens't generate enough  //
    !//             elements, other elements having common //
    !//             vertices are added                     //
    !////////////////////////////////////////////////////////

    !------------
    ! Strategy A
    !--------------------------------------------------------
    seen = .FALSE.
    DO jb = 1, SIZE(b_element)

       b_ele = b_element(jb)%b_f

       DO i = 1, b_ele%N_verts

          Nu_i = b_ele%NU(i)

#ifndef SEQUENTIEL
          ! Exclude nodes which are on the overlaps,
          ! the gradients here are computed on the 
          ! connected sub-domians and then are exchanged
          IF( Nu_i > NIn ) CYCLE
#endif

          IF( .NOT. seen(Nu_i) ) THEN
             seen(Nu_i) = .TRUE.
          ELSE
             ! Nodes already processed 
             ! must not be touched
             CYCLE
          ENDIF
          
          ! For each elment of the original 
          ! bubble connectivity, find onother
          ! element with common face 
          DO j = 1, SIZE( b_ele%b_e_con(i)%cn_ele )

             je = b_ele%b_e_con(i)%cn_ele(j)

             ele = d_element(je)%e

             DO jf = 1, ele%N_faces

                c_ele = ele%faces(jf)%f%c_ele
                
                found = .TRUE.
               
                DO k = 1, SIZE( b_ele%b_e_con(i)%cn_ele )

                   ke = b_ele%b_e_con(i)%cn_ele(k)

                   ! The processed element must exist (c_ele /= 0)
                   ! and it must not be contained in the original
                   ! bubble connectivity ( c_ele /= ke )
                   IF( c_ele == 0 .OR. c_ele == ke ) THEN
                      found = .FALSE.
                      EXIT
                   ENDIF
          
                ENDDO ! jf <= # cn_ele bubble

                IF(found) EXIT                
                
             ENDDO ! jf <= # N_faces

             IF( found ) THEN

                ! Add the new element to the bubble connectivity
                m = SIZE(nn_NU(Nu_i)%con)

                ALLOCATE( NN_A(m) ); NN_A = nn_NU(Nu_i)%con

                DEALLOCATE(nn_NU(Nu_i)%con)
                  ALLOCATE(nn_NU(Nu_i)%Con(m+1) )

                nn_NU(Nu_i)%Con(1:m) = NN_A
                nn_NU(Nu_i)%Con(m+1) = c_ele

                DEALLOCATE(NN_A)

             ENDIF

          ENDDO ! j <= # cn_ele 

       ENDDO ! i <= # N_verts

    ENDDO ! jb <= # ele_b
    !--------------------------------------------------------

    !------------
    ! Strategy B
    !--------------------------------------------------------
    ! Identify the boundary vertices
    seen = .FALSE.
    DO jb = 1, SIZE(b_element)

       DO k = 1, b_element(jb)%b_f%N_verts

          Nu_k = b_element(jb)%b_f%NU(k)

          IF(.NOT. seen(Nu_k)) seen(Nu_k) = .TRUE.

       ENDDO

    ENDDO

    IF( N_dim ==2 ) THEN
       N_o_lim = 2*(2 + 1)/2
    ELSE
       N_o_lim = 2*(2 + 1)*(2 + 2)/6
    ENDIF

    DO jb = 1, SIZE(b_element)

       b_ele = b_element(jb)%b_f

       DO i = 1, b_ele%N_verts

          Nu_i = b_ele%NU(i)

#ifndef SEQUENTIEL
          ! Exclude nodes which are on the overlaps,
          ! the gradients here are computed on the 
          ! connected sub-domians and then are exchanged
          IF( Nu_i > NIn ) CYCLE
#endif         

          ! check if the elements already found are
          ! enough to construct a well posed system 

          m = SIZE(nn_NU(Nu_i)%con)
          !m = 0
          !DO l = 1, SIZE(nn_NU(Nu_i)%con)
          !   m = m + d_element(nn_NU(Nu_i)%con(l))%e%N_sampl
          !ENDDO

          ! Be conservative: N_o+1 instead N_o
          IF( m > N_o_lim+1 ) CYCLE

          DO j = 1, SIZE( b_ele%b_e_con(i)%cn_ele )

             je = b_ele%b_e_con(i)%cn_ele(j)
             ele = d_element(je)%e

             DO k = 1, ele%N_verts

                Nu_k = ele%Nu(k)

                ! Only domain nodes must be added
                IF( seen(Nu_k) ) CYCLE

                DO m = 1, SIZE(ele%b_e_con(k)%cn_ele)
                    
                   ! Check if the element is already present
                   ! in the bubble connectivity...
                   found = .FALSE.
                   DO n = 1, SIZE( nn_NU(Nu_i)%con )

                      ke = ele%b_e_con(k)%cn_ele(m)

                      IF( ke == nn_NU(Nu_i)%con(n) ) THEN
                         found = .TRUE.
                         EXIT
                      END IF
                       
                   ENDDO
                    
                   ! ... if not, add it
                   IF(.NOT. found) THEN
                      
                      l = SIZE(nn_NU(Nu_i)%con)
                      ALLOCATE( NN_A(l) )

                      NN_A = nn_NU(Nu_i)%con

                      DEALLOCATE(nn_NU(Nu_i)%con)
                        ALLOCATE(nn_NU(Nu_i)%Con(l+1) )
                        
                     nn_NU(Nu_i)%Con(1:l) = NN_A
                     nn_NU(Nu_i)%Con(l+1) = ke

                     DEALLOCATE(NN_A)

                  ENDIF

               ENDDO ! m <= # cn_ele(k)

            ENDDO ! k <= # verts(cn_ele(k))

         ENDDO ! j <= # cn_ele(i)

      ENDDO ! i <= # verts(ele_b)

   ENDDO ! jb <= # ele_b
   !--------------------------------------------------------

   DEALLOCATE( seen )

   ALLOCATE( AA(N_DOF) )
   DO i = 1, N_DOF
       
       IF( ALLOCATED(nn_NU(i)%con) ) THEN

          n = 0
          DO j = 1, SIZE(nn_NU(i)%con)
             n = n + d_element(nn_NU(i)%con(j))%e%N_sampl
          ENDDO

          ! Size(A):
          ! (#recovery points * #element of the bubble) x
          ! #coeff. of the polynomial expansion
          ALLOCATE( AA(i)%MM(n, N_o) )
          AA(i)%MM = 0.d0

       ENDIF

    ENDDO

    ! Construct the local matrices for
    ! the least square problem: A
    !----------------------------------
    DO i = 1, N_DOF

       IF( ALLOCATED(nn_NU(i)%con) ) THEN

          DO j = 1, SIZE(nn_NU(i)%con)

             je = nn_NU(i)%con(j)

             ele = d_element(je)%e

             DO k = 1, ele%N_verts                
                IF( ele%NU(k) == i ) THEN
                   xx = ele%coords(:, k)
                   EXIT
                ENDIF
             ENDDO

             DO iq = 1, ele%N_sampl

                ! Local reference system, 
                ! centered on the node i
                xi = ele%xx_R(:, iq) - xx

                IF( ALLOCATED(AA(i)%MM) ) THEN

                   DO k = 1, SIZE(AA(i)%MM, 1)

                      IF(AA(i)%MM(k, 1) == 0) THEN

                         AA(i)%MM(k, 1)         = 1.d0
                         AA(i)%MM(k, 2:N_dim+1) = xi

                         IF( N_Order == 3 ) THEN

                            AA(i)%MM(k, 2+N_dim : 1+2*N_dim) = xi**2

                            id = 0
                            DO l = 1, N_dim-1
                               DO m = l+1, N_dim
                                  id = id + 1
                                  AA(i)%MM(k, 1+2*N_dim+id) =  xi(l)*xi(m)
                               ENDDO

                            ENDDO

                         ENDIF ! if O3

                         EXIT
                         
                      ENDIF ! find next row

                   ENDDO ! k <= # rows AA

                ENDIF ! if AA alloted
                                
             ENDDO  ! iq <= # recovery points

          ENDDO ! j <= # ele bubble

       ENDIF ! if exists connectity

    ENDDO ! i <= # DOFS
    !----------------------------------

    ALLOCATE( inv_AA(N_DOF) )

    ALLOCATE( b_a(N_DOF), p_coeff(N_DOF) )

    ! A^-1 <= (A^T * A)^-1
    DO i = 1, N_DOF
       
       IF( ALLOCATED(AA(i)%MM) ) THEN

          n_r = SIZE(AA(i)%MM, 1)
          n_c = SIZE(AA(i)%MM, 2)
          
          ALLOCATE( AA_Tr(n_c, n_r), &
                     temp(n_c, n_c) )

          AA_Tr = TRANSPOSE(AA(i)%MM)

          temp = MATMUL(AA_tr, AA(i)%MM)

!!$          ! Exclude  points for which 
!!$          ! the matrix is singular
!!$          ! or ill-conditioned
!!$          IF(SIZE(AA_tr, 2) > N_o) THEN

             ALLOCATE( inv_AA(i)%MM(n_c, n_c) )
             inv_AA(i)%MM = 0.d0

             inv_AA(i)%MM = inverseLU(temp)

!!$          ENDIF
          
          DEALLOCATE( AA(i)%MM )
            ALLOCATE( AA(i)%MM(n_c, n_r) )

          ! Store the transpose matrix
          AA(i)%MM = AA_Tr

          DEALLOCATE( temp, AA_Tr )

          ! RHS
          ALLOCATE( b_a(i)%v(n_r, N_dim, Data%NVar) )
          ALLOCATE( b_a(i)%idx(Data%NVar) )
          b_a(i)%v = 0.d0
          b_a(i)%idx = 0

          ! Polynomial Coeff
          ALLOCATE( p_coeff(i)%v(n_c, N_dim, Data%NVar) )    
          p_coeff(i)%v = 0.d0
       
       ENDIF
        
!!$       ! This will be used to store the 
!!$       ! connectivity between a boundary node
!!$       ! and the nearest domain vertex
!!$       ALLOCATE( p_coeff(i)%idx(1) )
!!$       p_coeff(i)%idx = 0

    ENDDO

    ! *** DONE ****

  END SUBROUTINE SPRZZ_build_matrix
  !================================










  !==============================
  SUBROUTINE SPR_ZN_recovery(Var)
  !==============================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    !------------------------------------

    TYPE(element_Str) :: ele

    REAL, DIMENSION(:), ALLOCATABLE :: rhs

    REAL, DIMENSION(Data%NDim) :: xx_i, xx_0, d_xx

    REAL :: d_min

    INTEGER :: je, k, i, j, l, N_dim, Nu_i, Nu_k
    INTEGER :: jb, n_e, n, m, k_min, id, jd
    LOGICAL :: found
    !---------------------------------------

    IF( .NOT. PREPRO_SPRZN ) THEN

       ALLOCATE( nn_NU(Var%NCells) )

       ! Once for all
       CALL SPRZN_build_matrix(Var%NCells, nn_NU)

       PREPRO_SPRZN = .TRUE.

    ENDIF
    
    N_dim = Data%NDim

    ! Reset
    b%idx = 0
    Var%D_Ua = 0.d0

    ! Construction of the RHS terms
    ! for each domain mesh vertex
    !--------------------------------
    DO je = 1, SIZE(d_element)

       ele = d_element(je)%e

       DO i = 1, ele%N_verts

          Nu_i = ele%NU(i)

          DO k = 1, ele%N_points

             Nu_k = ele%NU(k)

             DO j = 1, SIZE(nn_NU(Nu_i)%con)

                IF( Nu_k == nn_NU(Nu_i)%con(j) ) THEN

                   ! Easy way to mark a found node
                   nn_NU(Nu_i)%con(j) = -nn_NU(Nu_i)%con(j)

                   IF ( ALLOCATED(b(Nu_i)%v) ) THEN

                      b(Nu_i)%idx = b(Nu_i)%idx + 1
                      
                      DO l = 1, Data%NVar

                         b(Nu_i)%v(b(Nu_i)%idx, l) = Var%Ua(l, Nu_k)

                      ENDDO  ! l <= #Var

                   ENDIF

                ENDIF

             ENDDO  ! j <= #Con

          ENDDO  ! k <= #N_points

       ENDDO  ! i <= #N_verts

    ENDDO ! je <= #elements

    DO i = 1, Var%NCells

       IF ( ALLOCATED(b(i)%v) ) THEN

          ALLOCATE( rhs(SIZE(AA(i)%MM, 1)) )

          DO l = 1, Data%NVar

             rhs = MATMUL( AA(i)%MM, b(i)%v(:, l) )
                     ! A^T ---^

             s_coeff(i)%v(:, l) = MATMUL( inv_AA(i)%MM, rhs )

             ! Gradient at the domain vertex 
             Var%D_Ua(:, l, i) = s_coeff(i)%v(2:N_dim+1, l)
             
          ENDDO

          DEALLOCATE( rhs )
          
          ! Back to the positive numeration
          nn_NU(i)%con = -nn_NU(i)%con

       ENDIF

    ENDDO  
    !--------------------------------

    !//////////////////////////////////////////
    !// For extra HO DOFs on the domain, the //
    !// gradient is computed extrapolating   //
    !// the value from the nearest vertex    //
    !// on the face                          //
    !//////////////////////////////////////////
    IF ( Data%NOrdre > 2 ) THEN

       DO je = 1, SIZE(d_element)
       
          ele = d_element(je)%e
          
          ! *** DOF on the faces ***          
          DO jb = 1, ele%N_faces
             
             DO i = ele%faces(jb)%f%N_verts+1, &
                    ele%faces(jb)%f%N_points
             
                Nu_i = ele%faces(jb)%f%NU(i)

                xx_i = ele%faces(jb)%f%Coords(:, i)

                ! Find the nearest node on the face to Nu_i 
                d_min = HUGE(1.d0)
                DO k = 1, ele%faces(jb)%f%N_verts

                   Nu_k = ele%faces(jb)%f%NU(k)
                   xx_0 = ele%faces(jb)%f%Coords(:, k)

                   d_xx = xx_i - xx_0

                   IF( DSQRT(SUM(d_xx**2)) < d_min ) THEN
                      d_min = DSQRT(SUM(d_xx**2))
                      k_min = k
                   ENDIF
                      
                ENDDO
                !---------

                Nu_k = ele%faces(jb)%f%NU(k_min)

                xx_0 = ele%faces(jb)%f%Coords(:, k_min)

                d_xx = xx_i - xx_0
         
                DO l = 1, Data%NVar
                   Var%D_Ua(:, l, Nu_i) = s_coeff(Nu_k)%v(2:N_dim+1, l)
                   Var%D_Ua(:, l, Nu_i) = Var%D_Ua(:, l, Nu_i) + &
                                          2.d0*s_coeff(Nu_k)%v(2+N_dim:1+2*N_dim, l)*d_xx

                   ! WARNING: 2D ONLY
                   Var%D_Ua(1, l, Nu_i) = Var%D_Ua(1, l, Nu_i) + s_coeff(Nu_k)%v(6, l)*d_xx(2)
                   Var%D_Ua(2, l, Nu_i) = Var%D_Ua(2, l, Nu_i) + s_coeff(Nu_k)%v(6, l)*d_xx(1)

!                   DO k = 1, N_dim
!                      n = 0
!                      DO id = 1, N_dim-1
!                         DO jd = id+1, N_dim
!                            DO m = 1, N_dim
!                               n = n+1
!                               IF(m /= k .AND. n <= (N_dim-1) ) THEN
!                                  Var%D_Ua(k, l, Nu_i) = Var%D_Ua(k, l, Nu_i) + &
!                                                         s_coeff(Nu_k)%v(1+2*N_dim+n, l)*d_xx(m)
!                               ENDIF
!                            ENDDO
!                         ENDDO
!                      ENDDO
!                   ENDDO

                ENDDO ! l # Nvar   
       
             ENDDO ! i <= HO dofs face
                                 
          ENDDO ! jb <= # faces
          ! *** DONE ***
          
!          INTERNAL DOFs ????

       ENDDO ! je <= # ele

    ENDIF ! IF HO
    !--------------------------------------------

  END SUBROUTINE SPR_ZN_recovery
  !=============================

  !==========================================
  SUBROUTINE SPRZN_build_matrix(N_DOF, nn_NU)
  !==========================================

    IMPLICIT NONE
    
    INTEGER, INTENT(IN) :: N_DOF

    TYPE(con_NU), DIMENSION(:), INTENT(OUT) :: nn_Nu
    !-----------------------------------------------

    TYPE(element_Str) :: ele
    TYPE(face_str)    :: b_ele
    !-------------------------------------------

    REAL, DIMENSION(Data%NDim) :: xi, xx_0, xx_i, xx_k

    INTEGER, DIMENSION(:), POINTER     :: Nu
    INTEGER, DIMENSION(:), ALLOCATABLE :: temp   

    REAL, DIMENSION(:,:), ALLOCATABLE :: A_temp, AA_TR

    LOGICAL :: find
    !-------------------------------------------

    REAL :: d_ik, d_ik_min, d_max

    INTEGER :: N_dim, N_Order, Ns, N_c, N_r, N_o, Nu_k_min 
    INTEGER :: i, k, je, jb, jf, j, l, m, n, id, Nu_i, Nu_k
    !---------------------------------------

    N_dim   = Data%Ndim
    N_Order = Data%NOrdre

    ! Node-to-bouble connectivity
    !--------------------------------------------------------
    DO je = 1, SIZE(d_element)

       Ns = d_element(je)%e%N_points
       Nu => d_element(je)%e%Nu

       DO i = 1, d_element(je)%e%N_verts

          xx_i = d_element(je)%e%Coords(:,i)

          IF( .NOT. ALLOCATED(nn_Nu(Nu(i))%con) ) THEN

             ALLOCATE(nn_Nu(Nu(i))%con(Ns))

             nn_Nu(Nu(i))%con = Nu

             nn_NU(Nu(i))%h_z = 0.d0             

             DO k = 1, d_element(je)%e%N_verts
                xx_k = d_element(je)%e%Coords(:,k)
                nn_NU(Nu(i))%h_z = MAX( nn_NU(Nu(i))%h_z, &
                                        DSQRT(SUM((xx_i - xx_k)**2)) )

             ENDDO

          ELSE

             DO k = 1, Ns

                n = SIZE(nn_Nu(Nu(i))%con)

                find = .FALSE.

                DO j = 1, n

                   IF(nn_Nu(Nu(i))%con(j) == Nu(k)) THEN
                      find = .TRUE.
                      EXIT
                   ENDIF

                ENDDO

                IF(.NOT. find) THEN

                   ALLOCATE( temp(SIZE(nn_Nu(Nu(i))%con)) )
                   temp = nn_Nu(Nu(i))%con

                   DEALLOCATE( nn_Nu(Nu(i))%con )
                     ALLOCATE( nn_Nu(Nu(i))%con(SIZE(temp)+1) )

                   nn_Nu(Nu(i))%con = (/ temp, Nu(k) /)

                   xx_k = d_element(je)%e%Coords(:,k)

                   nn_NU(Nu(i))%h_z = MAX( nn_NU(Nu(i))%h_z, &
                                           DSQRT(SUM((xx_i - xx_k)**2)) )

                   DEALLOCATE(temp)                      

                ENDIF

             ENDDO ! k <= #N_points

          ENDIF ! Not allocated

       ENDDO ! i <= #N_points

    ENDDO ! je <= #elements
    !--------------------------------------------------------

    ! # polynomial coefficients
    IF( N_dim ==2 ) THEN
       N_o = N_Order*(N_Order + 1)/2
    ELSE
       N_o = 3**(N_Order - 1) + 1
    ENDIF

    ALLOCATE( AA(N_DOF) )
 
    ! Construct the matrices of the least-square
    ! method, for each DOF
    !---------------------------------------------
    DO je = 1, SIZE(d_element)
    
       ele = d_element(je)%e

       DO i = 1, ele%N_verts

          Nu_i = ele%NU(i)

          xx_0 = ele%coords(:, i)

          N_r = SIZE(nn_NU(Nu_i)%con)

          IF (.NOT. ALLOCATED(AA(Nu_i)%MM) ) THEN
             ALLOCATE( AA(Nu_i)%MM(N_r, N_o) )
             AA(Nu_i)%idx = 0
          ENDIF

          DO k = 1, ele%N_points

             Nu_k = ele%NU(k)

             DO j = 1, N_r

                IF( Nu_k == nn_NU(Nu_i)%con(j) ) THEN

                   xi = ele%coords(:, k) - xx_0

                   xi = xi !/ nn_NU(Nu_i)%h_z

                   ! Easy way to mark a node found
                   nn_NU(Nu_i)%con(j) = -nn_NU(Nu_i)%con(j)

                   AA(Nu_i)%idx = AA(Nu_i)%idx + 1

                   n = AA(Nu_i)%idx
                   
                   AA(Nu_i)%MM(n, 1)         = 1.d0
                   AA(Nu_i)%MM(n, 2:N_dim+1) = xi

                   IF( N_Order == 3 ) THEN

                      AA(Nu_i)%MM(n, 2+N_dim : 1+2*N_dim) = xi**2

                      id = 0
                      DO l = 1, N_dim-1
                         DO m = l+1, N_dim
                            id = id + 1
                            AA(Nu_i)%MM(n, 1+2*N_dim+id) =  xi(l)*xi(m)
                         ENDDO
                      ENDDO
                      
                   ENDIF

                ENDIF ! IF Nu_k in Omega_ik

             ENDDO ! j <= #N_rows

          ENDDO ! k <= #N_points

       ENDDO! i <= #N_verts

    ENDDO ! je <= #elements
    !---------------------------------------------

    ALLOCATE( inv_AA(N_DOF) )

    ALLOCATE( b(N_DOF), s_coeff(N_DOF) )
    
    ! A^-1 <= (A^T * A)^-1
    DO i = 1, N_DOF
              
       IF( ALLOCATED(AA(i)%MM) ) THEN

          ! Back to the positive numeration
          nn_NU(i)%con = -nn_NU(i)%con
       
          N_r =  SIZE(AA(i)%MM, 1)
          N_c =  SIZE(AA(i)%MM, 2)

          ALLOCATE( AA_Tr(N_c, N_r), &
                   A_temp(N_c, N_c) )

          AA_Tr = TRANSPOSE(AA(i)%MM)

          A_temp = MATMUL(AA_tr, AA(i)%MM)

          ALLOCATE( inv_AA(i)%MM(N_c, N_c) )

          inv_AA(i)%MM = 0.d0

          ! Exclude  points for which 
          ! the matrix is singular
          ! or ill-conditioned
          IF(SIZE(AA_tr, 2) >= N_o) THEN
             inv_AA(i)%MM = inverseLU(A_temp)
          ENDIF
          
          DEALLOCATE( AA(i)%MM )
            ALLOCATE( AA(i)%MM(N_c, N_r) )

          AA(i)%MM = AA_Tr

          DEALLOCATE( A_temp, AA_Tr )

          ! RHS
          ALLOCATE( b(i)%v(N_r, Data%NVar) )
          b(i)%v = 0.d0
          b(i)%idx = 0

          ! Polynomial Coeff
          ALLOCATE( s_coeff(i)%v(N_c, Data%NVar) )    
          s_coeff(i)%v = 0.d0
       
       ENDIF
        
       ! This will be used to store the 
       ! connectivity between a boundary node
       ! and the nearest domain vertex
       s_coeff(i)%idx = 0

    ENDDO

  END SUBROUTINE SPRZN_build_matrix
  !================================  

  !============================================
  FUNCTION Grad_i__tmp(Nu_i, Vc) RESULT(G_Vc_i)
  !============================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN) :: Nu_i
    REAL, DIMENSION(:,:), INTENT(IN) :: Vc

    REAL, DIMENSION(Data%NDim, Data%NVar) :: G_Vc_i
    !----------------------------------------------

    TYPE(element_str) :: ele

    REAL, DIMENSION(:,:), ALLOCATABLE :: Vc_l
    REAL, DIMENSION(DATA%Ndim) :: D_U
    REAL :: d_i

    INTEGER :: l, je, k, j, id, N_dofs, N_var, N_dim
    !----------------------------------------------
    
    N_Var = Data%NVar
    N_dim = Data%NDim

    G_Vc_i = 0.d0; d_i = 0.d0

    DO l = 1, SIZE(Connect(Nu_i)%cn_ele)

       je = Connect(Nu_i)%cn_ele(l)

       ele = d_element(je)%e

       N_dofs = ele%N_points

       ALLOCATE( Vc_l(N_Var, N_dofs) )

       Vc_l = Vc(:, ele%Nu)

       DO k = 1, N_dofs

          IF( ele%Nu(k) == NU_i ) THEN

             DO j = 1, N_var
             
                DO id = 1, N_dim
                   D_U(id) = SUM( Vc_l(j, :) * ele%D_phi_k(id, :, k) )
                ENDDO

                G_Vc_i(:, j) = G_Vc_i(:, j) + ele%volume *  D_U

             ENDDO

             d_i = d_i + ele%volume

             EXIT

          ENDIF

       ENDDO

       DEALLOCATE( Vc_l )

    ENDDO

    G_Vc_i = G_Vc_i / d_i

  END FUNCTION Grad_i__tmp
  !=======================  

#endif //-Navier-Stokes

END MODULE gradient_recovery


