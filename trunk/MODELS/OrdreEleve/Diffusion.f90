MODULE diffusion

  USE LesTypes

  USE EOSLaws,           ONLY: EOSCv__T_rho

  USE Visc_Laws,         ONLY: EOSvisco, EOSCondTh, Re_mu

  USE Contrib_NS

  USE Advection,         ONLY: Cons_to_Prim

  USE Turbulence_models, ONLY: Turbulent, Turb_eddy_viscosity, &
                               Turb_eddy_thermal_conductivity, &
                               Turb_diffusion_Jacobian,        &
                               Turb_diffusion_flux_function

  IMPLICIT NONE

CONTAINS

#ifdef NAVIER_STOKES

  !=============================================
  SUBROUTINE Diffusion_flux(N_dim, Vp, G_Vc, ff)
  !=============================================

    IMPLICIT NONE

    INTEGER,                  INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),       INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),     INTENT(IN)  :: G_vc
    REAL, DIMENSION(:,:),     INTENT(OUT) :: ff
    !----------------------------------------------

    REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: KK

    INTEGER :: id, jd
    !----------------------------------------------

    ALLOCATE(KK( SIZE(G_Vc,2), SIZE(G_Vc,2), &
                    N_dim, N_dim)    )
 
    CALL Diffusion_Jacobian( N_dim, Vp, KK )
        
    ff = 0.d0

    DO id = 1, N_dim

       DO jd = 1, N_dim

          ff(:, id) = ff(:, id) + & 
                      MATMUL( KK(:,:, id, jd), G_Vc(jd, :) )

       ENDDO 

    ENDDO

    DEALLOCATE( KK )

  END SUBROUTINE Diffusion_flux
  !============================

  !===============================================
  SUBROUTINE Diffusion_flux_function(Vp, G_Vc, ff)
  !===============================================
    
    IMPLICIT NONE

    REAL, DIMENSION(:),       INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),     INTENT(IN)  :: G_vc
    REAL, DIMENSION(:,:),     INTENT(OUT) :: ff
    !----------------------------------------------

    REAL :: mu, mu_v, k_therm
    !----------------------------------------------

    mu = EOSvisco(Vp(i_rho_vp), Vp(i_tem_vp))

    mu_v = Viscosity(Vp)
    
    k_therm = Thermal_conductivity(Vp)

    CALL NS_flux(Vp, G_Vc, mu_v, k_therm, ff)

    IF( Turbulent ) THEN

       CALL Turb_diffusion_flux_function(Vp, G_vc, mu, ff) ! ff in-out

    ENDIF  

  END SUBROUTINE Diffusion_flux_function
  !=====================================

  !=====================================================
  SUBROUTINE Diffusion_flux_n(N_dim, Vp, G_Vc, nn, ff_n)
  !=====================================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:), INTENT(IN)  :: G_vc
    REAL, DIMENSION(:),   INTENT(IN)  :: nn
    REAL, DIMENSION(:),   INTENT(OUT) :: ff_n
    !----------------------------------------------

    REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: KK

    INTEGER :: N_var, id, jd
    !----------------------------------------------

    N_var = SIZE(G_Vc,2)

    ALLOCATE( KK(N_var, N_var, N_dim, N_dim) )
     
    CALL Diffusion_Jacobian( N_dim, Vp, KK )
        
    ff_n = 0.d0

    DO id = 1, N_dim

       DO jd = 1, N_dim

          ff_n = ff_n + &
                 MATMUL( KK(:,:, id, jd), G_Vc(jd, :) ) * nn(id)

       ENDDO

    ENDDO

    DEALLOCATE( KK )

  END SUBROUTINE Diffusion_flux_n
  !==============================

  !===========================================
  SUBROUTINE Diffusion_Jacobian(N_dim, Vp, KK)
  !===========================================

    IMPLICIT NONE

    INTEGER,                  INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),       INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:,:,:), INTENT(OUT) :: KK
    !------------------------------------------------

    REAL :: mu, mu_v, k_th_v
    !------------------------------------------------

    ! Laminar visocisity
    mu = EOSvisco(Vp(i_rho_vp), Vp(i_tem_vp))

    ! Contribution of the eddy viscosity
    ! mu_v = mu (+ mu_eddy)
    mu_v = Viscosity(Vp)

    ! Contribution of the turbulence
    ! k_th_v = k_th (+ k_th_turb)
    k_th_v = Thermal_conductivity(Vp)
        
    CALL NS_Jacobian( N_dim, Vp, mu_v, k_th_v, KK )

    IF( Turbulent ) THEN
       CALL Turb_diffusion_Jacobian( N_dim, Vp, mu, KK ) ! KK in-out
    ENDIF  

  END SUBROUTINE Diffusion_Jacobian
  !================================

  !========================================
  FUNCTION Thermal_flux(Vp, G_Vc) RESULT(q)
  !========================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: G_Vc

    REAL, DIMENSION(SIZE(G_Vc, 1)) :: q
    !----------------------------------------

    REAL :: k_th
    !----------------------------------------
    
    ! Laminar (+ Turbulent)
    k_th = Thermal_conductivity(Vp)

    q = Heat_flux(Vp, G_Vc, k_th)

  END FUNCTION Thermal_flux
  !========================

  !==================================
  FUNCTION Viscosity(Vp) RESULT(mu_v)
  !==================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    
    REAL :: mu_v
    !-------------------------------------

    REAL :: mu_turb
    !-------------------------------------

    mu_v = EOSvisco(Vp(i_rho_vp), Vp(i_tem_vp))
        
    IF( Turbulent ) THEN

       mu_turb = Turb_eddy_viscosity(Vp, mu_v)

       mu_v = mu_v + mu_turb/Re_mu

    ENDIF

  END FUNCTION Viscosity
  !=====================

  !==============================================
  FUNCTION Thermal_conductivity(Vp) RESULT(kth_v)
  !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN) :: Vp

    REAL :: kth_v
    !-------------------------------------

    REAL :: mu, k_th_turb    
    !-------------------------------------    

    kth_v = EOSCondTh(Vp(i_rho_vp), Vp(i_tem_vp))

    IF( Turbulent ) THEN

       mu = EOSvisco(Vp(i_rho_vp), Vp(i_tem_vp))

       k_th_turb = Turb_eddy_thermal_conductivity(Vp, mu)

       kth_v = kth_v + k_th_turb/Re_mu

    ENDIF

  END FUNCTION Thermal_conductivity
  !================================
  
  !==========================================
  FUNCTION alpha_visc(ele, Vp) RESULT(a_visc)
  !==========================================
  !
  !  Viscous time stepping ~ |V|/Dt
  !
    IMPLICIT NONE

    TYPE(element_str),    INTENT(IN) :: ele
    REAL, DIMENSION(:,:), INTENT(IN) :: Vp

    REAL :: a_visc
    !-------------------------------------------

    REAL :: nu_max, kt_max, S_ele, k_th, Cv
    INTEGER :: j
    !-------------------------------------------

    nu_max = 0.d0; kt_max = 0.d0

    DO j = 1, SIZE(Vp, 2)

       k_th = Thermal_conductivity(Vp(:, j))

       Cv  = EOSCv__T_rho( Vp(i_rho_vp, j), Vp(i_tem_vp, j) )

       nu_max = MAX( nu_max, Viscosity(Vp(:, j)) / Vp(i_rho_vp, j) )

       kt_max = MAX( kt_max, k_th / ( Vp(i_rho_vp, j) * Cv ) )

    ENDDO

    !! nu_max = nu_max * (M_oo/Re_oo)
    !! kt_max = kt_max * (M_oo/Re_oo)

    ! Element surface
    S_ele = 0.d0
    DO j = 1, ele%N_faces
       S_ele = S_ele + SUM(ele%faces(j)%f%w_q)
    ENDDO

!    a_visc = ele%N_dim * (nu_max + kt_max) * S_ele*S_ele / ele%Volume
    a_visc = 2.d0* (nu_max + kt_max) / &
                   (ele%N_dim * ele%Volume/S_ele )
        
  END FUNCTION alpha_visc
  !======================
  
  !============================================
  FUNCTION alpha_visc_t(ele, Vp) RESULT(a_visc)
  !============================================
  !
  !  
  !
    IMPLICIT NONE

    TYPE(element_str),    INTENT(IN) :: ele
    REAL, DIMENSION(:,:), INTENT(IN) :: Vp

    REAL :: a_visc
    !-------------------------------------------

    REAL :: nu_max, kt_max, S_ele, Cv, k_th
    INTEGER :: j
    !-------------------------------------------

    nu_max = 0.d0; kt_max = 0.d0

    DO j = 1, SIZE(Vp, 2)

       k_th = Thermal_conductivity(Vp(:, j))

       Cv  = EOSCv__T_rho( Vp(i_rho_vp, j), Vp(i_tem_vp, j) )

       nu_max = MAX( nu_max, Viscosity(Vp(:, j)) / Vp(i_rho_vp, j)    )

       kt_max = MAX( kt_max, k_th / ( Vp(i_rho_vp, j) * Cv ) )

    ENDDO

    !! nu_max = nu_max * (M_oo/Re_oo)
    !! kt_max = kt_max * (M_oo/Re_oo)

    a_visc = 2.d0* (nu_max + kt_max) 
        
  END FUNCTION alpha_visc_t  
  !========================
   
  !====================================
  FUNCTION Loc_Re(ele, Vp) RESULT(l_Re)
  !====================================

    IMPLICIT NONE

    TYPE(element_str),    INTENT(IN)  :: ele
    REAL, DIMENSION(:,:), INTENT(IN)  :: Vp
     
    REAL :: l_Re
    !---------------------------------------
     
    REAL :: u_l, mu_l, rho_l, S_e, h
    INTEGER :: i
    !---------------------------------------
     
    u_l = HUGE(1.d0); mu_l = 0.d0

    DO i = 1, SIZE(Vp, 2)

        u_l = MIN( u_l, DSQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
       mu_l = MAX( mu_l, EOSvisco( Vp(i_rho_vp, i), Vp(i_tem_vp, i) ) ) !!* M_oo/Re_oo

    ENDDO

    rho_l = MAXVAL(Vp(i_rho_vp, :))

    S_e = 0.d0
    DO i = 1, ele%N_faces
       S_e = S_e + SUM(ele%faces(i)%f%w_q)
    ENDDO
    
    h = ele%N_dim * ele%Volume / S_e

    l_Re = u_l * h * rho_l / mu_l
    
    ! To avoid division by zero
    l_Re = MAX(l_Re, 1.0E-12)

  END FUNCTION Loc_Re
  !==================   

  !====================================================
  FUNCTION check_visc_flux(N_dim, Vp, G_Vc) RESULT(idx)
  !====================================================

    IMPLICIT NONE

    INTEGER,                  INTENT(IN)  :: N_dim
    REAL, DIMENSION(:),       INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),     INTENT(IN)  :: G_vc
    INTEGER                               :: idx
    !--------------------------------------------

    REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: KK
    REAL, DIMENSION(:,:),     ALLOCATABLE :: ff, ff_v

    REAL :: err

    INTEGER :: id, jd
    !--------------------------------------------

    idx = 0

    ALLOCATE(KK( SIZE(G_Vc,2), SIZE(G_Vc,2), &
                    N_dim, N_dim)    )

    ALLOCATE( ff  (SIZE(G_Vc,2), N_dim) )
    ALLOCATE( ff_v(SIZE(G_Vc,2), N_dim) )

    CALL Diffusion_Jacobian( N_dim, Vp, KK )
        
    ff = 0.d0

    DO id = 1, N_dim

       DO jd = 1, N_dim

          ff(:, id) = ff(:, id) + & 
                      MATMUL( KK(:,:, id, jd), G_Vc(jd, :) )

       ENDDO 

    ENDDO

    CALL Diffusion_flux_function(Vp, G_Vc, ff_v)

    err = MAXVAL(ABS(ff - ff_v))

IF( SQRT(SUM(G_Vc**2)) > 1.0E-7 ) THEN
WRITE(*, '(4E24.16)') ff(1, :) , ff_v(1, :)
WRITE(*, '(4E24.16)') ff(2, :) , ff_v(2, :)
WRITE(*, '(4E24.16)') ff(3, :) , ff_v(3, :)
WRITE(*, '(4E24.16)') ff(4, :) , ff_v(4, :)
WRITE(*, '(4E24.16)') ff(5, :) , ff_v(5, :)
WRITE(*, *) '-------------'
WRITE(*, *)
ENDIF

WRITE(*, '(E12.6)') err

    IF( err > 1.0E-16 ) THEN
       WRITE(*,*) 'stop'
       STOP
    ENDIF

    DEALLOCATE(KK, ff, ff_v)

  END FUNCTION check_visc_flux
  !===========================

#endif

END MODULE diffusion
