MODULE Contrib_Boundary

  USE LesTypes

  USE EOSLaws

  !!USE Visc_Laws !!,   ONLY: M_oo, Re_oo

  USE Gradient_Recovery

  USE Diffusion

  USE Advection

  USE Assemblage

  USE Turbulence_models, ONLY: Turbulent, Turb_wall_bc, &
       Turb_sym_plane_bc, Turb_Jac_sym_plane

  USE Quadrature, ONLY: bInt_i

  USE PLinAlg,    ONLY: Identity_Matrix

  IMPLICIT NONE

  PRIVATE :: imp_Weak_BC, Weak_BC,                     &
       imp_BC_Slip_Wall, BC_Slip_Wall,                 &
       imp_BC_Sym_Plane, BC_Sym_Plane,                 &
       imp_Steger_Warming_Flux, Steger_Warming_Flux,   &
       imp_BC_entree_turbine, BC_entree_turbine,       &
       imp_BC_sortie_turbine, BC_sortie_turbine        

CONTAINS

  !===========================================
  SUBROUTINE imp_Boundary_Conditions(Var, Mat)
    !===========================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Mat
    !--------------------------------------

    ! No boundary
    IF (.NOT. ALLOCATED(b_element) ) RETURN

    CALL imp_Weak_BC(Var, Mat)

    CALL imp_Strong_BC(Var, Mat)

    CALL imp_Periodic_BC(Var, Mat)

  END SUBROUTINE imp_Boundary_Conditions
  !======================================

  !==================================
  SUBROUTINE Boundary_Conditions(Var)
    !==================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    ! No boundary
    IF (.NOT. ALLOCATED(b_element) ) RETURN

    CALL Weak_BC(Var)

    CALL Strong_BC(Var, .FALSE.)

    CALL exp_Periodic_BC(Var)

  END SUBROUTINE Boundary_Conditions
  !=================================

  !==============================================
  SUBROUTINE Boundary_Conditions_i(Nu_i, Vc, R_i)
    !==============================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN)    :: Nu_i
    REAL, DIMENSION(:,:), INTENT(INOUT) :: Vc
    REAL, DIMENSION(:),   INTENT(INOUT) :: R_i
    !-------------------------------------------

    ! No boundary
    IF (.NOT. ALLOCATED(b_element) ) RETURN

    CALL Weak_BC_i(Nu_i, Vc, R_i)

    CALL Strong_BC_i(Nu_i, Vc, R_i)

  END SUBROUTINE Boundary_Conditions_i
  !===================================

  !//////////////////////////////////////////////////////////////////////////
  !//                                                                      //
  !//                       WEAK BOUNDARY CONDITIONS                       //
  !//                              (implicit)                              //
  !//                                                                      //
  !//////////////////////////////////////////////////////////////////////////

  !===============================
  SUBROUTINE imp_Weak_BC(Var, Mat)
    !===============================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Mat
    !------------------------------------

!    TYPE(face_str) :: loc_ele

    INTEGER, DIMENSION(:),   ALLOCATABLE :: NU
    REAL,    DIMENSION(:,:), ALLOCATABLE :: fb_n
    REAL,    DIMENSION(:,:), ALLOCATABLE :: Vc
    REAL,    DIMENSION(:,:), ALLOCATABLE :: Vp
    REAL,    DIMENSION(:),   ALLOCATABLE :: Vp_ext
    REAL,    DIMENSION(:),   ALLOCATABLE :: ff_NS_n
    REAL,    DIMENSION(:),   ALLOCATABLE :: ff_NSw_n

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: JJ_b
    REAL, DIMENSION(:,:),   ALLOCATABLE :: MM

    REAL :: m_ik

    LOGICAL :: is_WeakBC

    INTEGER :: bc_idx, bc_type, N_dofs, N_var, N_dim
    INTEGER :: jf, i, j, k, iq
    !---------------------------------------------

    N_var = DATA%Nvar
    N_dim = DATA%Ndim

    ALLOCATE( Vp_ext(SIZE(Var%Vp, 1)) )

    ALLOCATE( MM(N_var, N_var) )

    DO jf = 1, SIZE(b_element)

!       loc_ele = b_element(jf)%b_f

       bc_idx  = b_element(jf)%b_f%bc_type
       bc_type = DATA%FacMap(bc_idx)

       N_dofs = b_element(jf)%b_f%N_points

       ALLOCATE( fb_n(N_var, N_dofs) )

       ALLOCATE( NU(N_dofs) )
       NU = b_element(jf)%b_f%NU

       ALLOCATE( Vc(SIZE(Var%Ua,1), N_dofs) )
       Vc = Var%Ua(:,Nu)!0.5*(Var%Ua(:, NU)    +Var%Ua_0(:, NU)    )

       ALLOCATE( Vp(SIZE(Var%Vp,1), N_dofs) )
       Vp = Var%Vp(:,Nu)!0.5*( Var%Vp(:, NU) + Var%Vp_0(:, NU) )

       ALLOCATE( JJ_b(N_var, N_var, N_dofs) )

       is_WeakBC = .TRUE.

#ifdef NAVIER_STOKES
       ! Boundary integral of the viscous flux
       IF(DATA%Iflux == 4) THEN
          DO i = 1, N_dofs
             CALL Diffusion_flux_n(N_dim, Vp(:, i), Var%D_Ua(:,:, NU(i)), &
                  b_element(jf)%b_f%n_b(:, i), fb_n(:, i))
          ENDDO

          DO i = 1, N_dofs
             Var%Flux(:, NU(i)) = Var%Flux(:, NU(i)) - &
                  bInt_i(b_element(jf)%b_f, i, fb_n)
          ENDDO
       ENDIF
#endif

       SELECT CASE(bc_type)

          !>>>  Slip wall <<<
          !########################
       CASE(lgc_paroi_glissante) ! Code 21
          !########################

          DO i = 1, N_dofs
             CALL imp_BC_Slip_Wall( b_element(jf)%b_f%n_b(:,i), &
                  Vp(:,i), fb_n(:,i), JJ_b(:,:, i) )
          END DO

          !>>> Symmetry plane <<<
          !####################### 
       CASE(lgc_symetrie_plane)
          !#######################

          DO i = 1, N_dofs
             CALL imp_BC_Sym_Plane( b_element(jf)%b_f%n_b(:,i), &
                  Vp(:,i), fb_n(:,i), JJ_b(:,:,i) )
          END DO

!!$#ifdef NAVIER_STOKES
!!$
!!$       ! >>> Paroi adherente adiabatique <<<
!!$       !#################################
!!$       CASE(lgc_paroi_adh_adiab_flux_nul)
!!$       !#################################
!!$
!!$          ALLOCATE(  ff_NS_n(N_var), &
!!$                    ff_NSw_n(N_var) )
!!$
!!$          DO i = 1, N_dofs
!!$
!!$             ! BC for the normal component of the velocity
!!$             CALL imp_BC_Slip_Wall( b_element(jf)%b_f%n_b(:,i), &
!!$                                    Vp(:,i), fb_n(:,i), JJ_b(:,:, i) )
!!$
!!$             ! NS flux function
!!$             CALL Diffusion_flux_n(N_dim, Vp(:,i), Var%D_Ua(:,:,NU(i)), &
!!$                                   b_element(jf)%b_f%n_b(:,i), ff_NS_n)
!!$
!!$             ! Solid Wall NS flux function
!!$             CALL BC_Wall_Diff_flux_n(b_element(jf)%b_f, i, Var%Ua, ff_NSw_n)
!!$             
!!$             fb_n(:, i) = fb_n(:, i) - (ff_NSw_n - ff_NS_n)
!!$
!!$             !Jacobian ?
!!$
!!$          ENDDO
!!$
!!$          DEALLOCATE( ff_NS_n, ff_NSw_n )
!!$          
!!$#endif

          ! >>> Sortie_subsonique <<<          
          ! >>> Sortie_mixt <<<
          ! >>> Flux de stege <<<  
          !##########################
       CASE(lgc_sortie_subsonique, &
            & lgc_sortie_mixte,      &
            & lgc_flux_de_steger) 
          !##########################

          Vp_ext = DATA%Fr(bc_idx)%Vp

          DO i = 1, N_dofs
             CALL imp_Steger_Warming_Flux( b_element(jf)%b_f%n_b(:,i), &
                  Vp(:,i), Vp_ext, fb_n(:,i), JJ_b(:,:, i) )
          ENDDO

          ! >>>  Steger-Warming <<<
          !      pression infini
          !##########################
       CASE(lgc_ns_steger_warming) ! Code 154
          !##########################

          DO i = 1, N_dofs

             Vp_ext = Vp_P( Vp(:, i), DATA%Fr(bc_idx)%Vp(i_pre_vp) )

             CALL imp_Steger_Warming_Flux( b_element(jf)%b_f%n_b(:,i), &
                  Vp(:,i), Vp_ext, fb_n(:,i), JJ_b(:,:, i) )

          ENDDO

          !###########################
       CASE(lgc_subsonic_inflow_Pt) ! Code 95
          !###########################

          Vp_ext = DATA%Fr(bc_idx)%Vp

          DO i = 1, N_dofs
             CALL imp_BC_subsonic_inflow_Pt( b_element(jf)%b_f%n_b(:,i), &
                  Vp_ext, Vp(:,i), fb_n(:,i), JJ_b(:,:, i) )
          END DO

          !>>>  Entrée Turbine <<<
          !########################
       CASE(lgc_entree_turbine_subsonique) ! Code 91
          !########################

          DO i = 1, N_dofs
             CALL imp_BC_entree_turbine( b_element(jf)%b_f%n_b(:,i), &
                  Vp(:,i), fb_n(:,i), JJ_b(:,:, i) )
          END DO


          !>>>  Sortie Turbine <<<
          !########################
       CASE(lgc_sortie_turbine_subsonique) ! Code 92
          !########################

          DO i = 1, N_dofs
             CALL imp_BC_sortie_turbine( b_element(jf)%b_f%n_b(:,i), &
                  Vp(:,i), fb_n(:,i), JJ_b(:,:, i) )
          END DO

          !##################################
       CASE(lgc_gradient_nul_exterieur,    &
            lgc_gradient_nul_interieur,    &
            lgc_dirichlet, lgc_paroi_misc, &
            lgc_paroi_adh_adiab_flux_nul,  &
            lgc_periodique                 )
          !###################################

          is_WeakBC = .FALSE.

       CASE DEFAULT

          WRITE(*,*) 'ERROR: Unknown boundary contidion in imp_Weak_BC'
          WRITE(*,*) 'Code of the boundary selected : ', bc_type
          STOP

       END SELECT

       IF(is_WeakBC) THEN

          !--------------------------------
          ! BC contribution to the residual
          !----------------------------------------------
          DO i = 1, N_dofs
             Var%Flux(:, NU(i)) = Var%Flux(:, NU(i)) + &
                  bInt_i(b_element(jf)%b_f, i, fb_n)
          ENDDO

          !-------------------
          ! Jacobian of the BC
          !-----------------------------------------------
          DO k = 1, N_dofs

             DO i = 1, N_dofs

                m_ik = 0.0

                DO iq = 1, b_element(jf)%b_f%N_quad

                   m_ik = m_ik + b_element(jf)%b_f%w_q(iq) * &
                        b_element(jf)%b_f%phi_q(i, iq) * b_element(jf)%b_f%phi_q(k, iq)

                ENDDO

                MM = JJ_b(:,:, k)*m_ik

                CALL Assemblage_add(MM, NU(i), NU(k), Mat)

             ENDDO

          ENDDO

       ENDIF

       DEALLOCATE( fb_n, Vc, Vp, NU, JJ_b )

    ENDDO

    DEALLOCATE( MM, Vp_ext )

  END SUBROUTINE imp_Weak_BC
  !=========================

  !//////////////////////////////////////////////////////////////////////////
  !//                                                                      //
  !//                     STRONG BOUNDARY CONDITIONS                       //
  !//                            (implicit)                                //
  !//                                                                      //
  !//////////////////////////////////////////////////////////////////////////

  !=================================
  SUBROUTINE imp_Strong_BC(Var, Mat)
    !=================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Mat
    !------------------------------------

!    TYPE(face_str) :: loc_ele

    INTEGER, DIMENSION(:),   ALLOCATABLE :: NU
    REAL,    DIMENSION(:,:), ALLOCATABLE :: fb_n
    REAL,    DIMENSION(:,:), ALLOCATABLE :: Vp

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: JJ_b

    REAL, DIMENSION(:,:),   ALLOCATABLE :: MM
    LOGICAL, DIMENSION(DATA%Nvar)       :: Mask

    REAL :: m_ik, Cv, T_w

    INTEGER :: bc_idx, bc_type
    INTEGER :: jf, N_dofs, N_var, i, j, k, iq, i_b
    !---------------------------------------------

    N_var = DATA%Nvar

    i_b = 0

    ALLOCATE( MM(N_var, N_var) )

    DO jf = 1, SIZE(b_element)

       !loc_ele = b_element(jf)%b_f

       bc_idx  = b_element(jf)%b_f%bc_type
       bc_type = DATA%FacMap(bc_idx)

       N_dofs = b_element(jf)%b_f%N_points

       ALLOCATE( fb_n(N_var, N_dofs) )

       ALLOCATE( NU(N_dofs) )
       NU = b_element(jf)%b_f%NU

       ALLOCATE( Vp(SIZE(Var%Vp,1), N_dofs) )
       Vp = 0.5*( Var%Vp(:, NU) + Var%Vp_0(:, NU) )

       SELECT CASE(bc_type)

          ! >>> dirichlet <<<
          !##################
       CASE(lgc_dirichlet)
          !##################

          DO i = 1, N_dofs

             Var%Vp(:, NU(i)) = DATA%Fr(bc_idx)%Vp

             CALL Prim_to_Cons( Var%Vp(:, NU(i)), Var%Ua(:, NU(i)) )

             Var%Flux(:, NU(i)) = 0.0

             ! JACOBIAN
             CALL assemblage_setidentblokline(NU(i), Mat)

          ENDDO

#ifdef NAVIER_STOKES

          ! >>> Paroi adherente adiabatique <<<
          !#################################
       CASE(lgc_paroi_adh_adiab_flux_nul)
          !#################################

          ALLOCATE( JJ_b(N_var, N_var, N_dofs) )

          DO i = 1, N_dofs

             ! Velocity on the wall             
             !---------------------
             Var%Vp(i_vi1_vp:i_vid_vp, NU(i)) = DATA%Fr(bc_idx)%Vp(i_vi1_vp:i_vid_vp)

             ! Turbulent bc
             !-------------
             IF(Turbulent) CALL Turb_wall_bc( Var%Vp(:, NU(i)), Var%Flux(:, NU(i)) )

             CALL Prim_to_Cons( Var%Vp(:, NU(i)), Var%Ua(:, NU(i)) )

             Var%Flux(i_rv1_ua:i_rvd_ua, NU(i)) = 0.d0

             ! Adiabatic wall
             !---------------
             CALL imp_BC_Adiabatic_Wall( b_element(jf)%b_f%n_b(:,i), &
                  Var%Vp(:, NU(i)), Var%D_Ua(:,:, NU(i)),     &
                  fb_n(:,i), JJ_b(:,:, i) )

          ENDDO

          !--------------------------------
          ! BC contribution to the residual
          !----------------------------------------------
          MM = Identity_Matrix(N_var)

          Mask           = .TRUE.
          Mask(i_rho_ua) = .FALSE.
          Mask(i_ren_ua) = .FALSE.

          DO i = 1, N_dofs

             Var%Flux(:, NU(i)) = Var%Flux(:, NU(i)) - &
                  bInt_i(b_element(jf)%b_f, i, fb_n)

             ! JACOBIAN
             CALL assemblage_nullifyExtAndReplaceDiagMask(MM, NU(i), Mask, Mat)

          ENDDO

          DO k = 1, N_dofs 

             DO i = 1, N_dofs

                m_ik = 0.d0

                DO iq = 1, b_element(jf)%b_f%N_quad

                   m_ik = m_ik + b_element(jf)%b_f%w_q(iq) * &
                        b_element(jf)%b_f%phi_q(i, iq) * b_element(jf)%b_f%phi_q(k, iq)

                ENDDO

                MM = JJ_b(:,:, k)*m_ik

                CALL Assemblage_add(MM, NU(k), NU(i), Mat)

             ENDDO

          ENDDO

          DEALLOCATE( JJ_b )

          !###################
       CASE(lgc_paroi_misc)
          !###################

          T_w = DATA%Fr(bc_idx)%Vp(i_tem_vp)            

          DO i = 1, N_dofs

             Cv = EOSCv__T_rho( Var%Ua(i_rho_ua, NU(i)), T_w )

             ! Velocity on the wall
             !----------------------
             Var%Vp(i_vi1_vp:i_vid_vp, NU(i)) = DATA%Fr(bc_idx)%Vp(i_vi1_vp:i_vid_vp)

             ! Temperature on the wall
             !------------------------
             Var%Vp(i_tem_vp, NU(i)) = T_w

             ! Turbulent bc
             !-------------
             IF(Turbulent) CALL Turb_wall_bc( Var%Vp(:, NU(i)), Var%Flux(:, NU(i)) )

             CALL Prim_to_Cons( Var%Vp(:, NU(i)), Var%Ua(:, NU(i)) )

             ! Nullify the RHS
             Var%Flux(i_rv1_ua:i_rvd_ua, NU(i)) = 0.d0
             Var%Flux(i_ren_ua,          NU(i)) = 0.d0

          ENDDO

          !----------
          ! Jacobian
          !----------------------------------------------
          Mask           = .TRUE.
          Mask(i_rho_ua) = .FALSE.

          DO i = 1, N_dofs

             Cv = EOSCv__T_rho( Var%Ua(i_rho_ua, NU(i)), T_w )

             ! JACOBIAN
             MM = Identity_Matrix(N_var)
             MM(i_ren_ua, i_rho_ua) = -T_w * Cv

             CALL assemblage_nullifyExtAndReplaceDiagMask(MM, NU(i), Mask, Mat)

          ENDDO

#endif

       END SELECT

       DEALLOCATE( fb_n, Vp, NU )

    ENDDO

    DEALLOCATE( MM )


  END SUBROUTINE imp_Strong_BC
  !===========================  

  !-------------------------------------------------------------
  !-------------------------------------------------------------
  !-------------------------------------------------------------

  !==============================================
  SUBROUTINE imp_BC_Slip_Wall(n_b, Vp, fb_n, Jac)
    !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    REAL, DIMENSION(:,:), INTENT(OUT) :: Jac
    !----------------------------------------

    REAL :: rho, u_n, e_k, h_t, pi_rho, pi_E
    INTEGER :: i
    !----------------------------------------

    !Energy
    e_k  = 0.5d0*SUM(Vp(i_vi1_vp:i_vid_vp)**2)

    h_t  = EOSenthalpy(Vp) + e_k

    !Pressure Derivatives
    pi_rho = EOS_d_PI_rho(Vp)  ! > Pressure Derivative // rho

    pi_E   = EOS_d_PI_E(Vp)    ! > Pressure Derivative // rho*E

    !rho
    rho  = Vp(i_rho_vp)

    !u.n
    u_n  = SUM( Vp(i_vi1_vp:i_vid_vp)*n_b )

    !----------------
    ! Correction flux
    !-------------------------------------
    fb_n(i_rho_ua)          = -rho*u_n
    fb_n(i_rv1_ua:i_rvd_ua) = -rho*u_n * Vp(i_vi1_vp:i_vid_vp)
    fb_n(i_ren_ua)          = -rho*u_n * h_t

    !---------
    ! Jacobian 
    !---------------------------------------
    Jac = 0.0

    Jac(1, i_rv1_ua:i_rvd_ua) = n_b

    DO i = i_vi1_vp, i_vid_vp

       Jac(i, 1) = -Vp(i)*u_n

       Jac(i, i_rv1_ua:i_rvd_ua) = Vp(i)*n_b

       Jac(i, i) = Jac(i, i) + u_n

    ENDDO

    Jac(i_ren_ua, 1)                 = -u_n*(h_t - pi_rho)
    Jac(i_ren_ua, i_rv1_ua:i_rvd_ua) =  h_t*n_b - pi_E*Vp(i_vi1_Vp:i_vid_vp)*u_n
    Jac(i_ren_ua, i_ren_ua)          =  u_n*(1.0d0 + pi_E)

    Jac = -Jac

  END SUBROUTINE imp_BC_Slip_Wall
  !==============================

  !==============================================
  SUBROUTINE imp_BC_Sym_Plane(n_b, Vp, fb_n, Jac)
    !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    REAL, DIMENSION(:,:), INTENT(OUT) :: Jac
    !-----------------------------------------

    CALL imp_BC_Slip_Wall(n_b, Vp, fb_n, Jac)

#ifdef NAVIER_STOKES
    IF(Turbulent) THEN
       CALL Turb_sym_plane_bc(n_b, Vp, fb_n)
       CALL Turb_Jac_sym_plane(Vp, n_b, Jac)
    ENDIF
#endif

  END SUBROUTINE imp_BC_Sym_Plane
  !==============================

  !===============================================================
  SUBROUTINE imp_Steger_Warming_Flux(n_b, Vp_i, Vp_out, fb_n, Jac)
    !===============================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_out
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    REAL, DIMENSION(:,:), INTENT(OUT) :: Jac
    !---------------------------------------------

    REAL, DIMENSION(SIZE(fb_n)) :: alpha
    REAL, DIMENSION(SIZE(fb_n)) :: lambda
    REAL, DIMENSION(SIZE(fb_n)) :: Vc_i, Vc_out
    REAL, DIMENSION(SIZE(fb_n), SIZE(fb_n)) :: RR, LL, Neg

    INTEGER :: j
    !---------------------------------------------

    CALL Advection_eigenvalues(Vp_i, n_b, lambda)
    CALL Advection_eigenvectors(Vp_i, n_b, RR, LL)

    CALL Prim_to_Cons(Vp_i,   Vc_i)
    CALL Prim_to_Cons(Vp_out, Vc_out)

    alpha = MATMUL(LL, Vc_out - Vc_i)

    Neg = 0.0
    DO j = 1, SIZE(lambda)
       Neg(j,j) = MIN(0.0, lambda(j))
    ENDDO

    fb_n = MATMUL(RR, MATMUL(Neg, alpha))

    !---------
    ! JAcobian
    !---------------------------------
    Jac = -MATMUL(RR, MATMUL(Neg, LL))

  END SUBROUTINE imp_Steger_Warming_Flux
  !=====================================

  !>>>  Entrée Turbine <<<  Code 91
  !==============================================
  SUBROUTINE imp_BC_entree_turbine(n_b, Vp_i, fb_n, Jac)
    !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    REAL, DIMENSION(:,:), INTENT(OUT) :: Jac
    !----------------------------------------

    REAL, DIMENSION(SIZE(Vp_i)) :: Vp_b 

    REAL  :: usdn, usdn2, am0, t_tent, t_tent1, ro_tent, &
         err, t_c, rhoc, p_c, ac, nor_velc, uvelc, vvelc

    REAL :: rhoi, uveli, vveli, ei, ppi, ai, &
         tempi, nor_veli, tan_veli_x, tan_veli_y 
    REAL :: nx, ny

    REAL :: P_oo, rho_oo, T_oo, e_oo, ht_oo, s_oo, alpha

    INTEGER :: id, icont, i
    !----------------------------------------

    REAL, DIMENSION(DATA%NVar) :: lambda
    REAL, DIMENSION(DATA%NVar) :: u_d, v_d, u_c, v_c, u_b
    REAL, DIMENSION(DATA%NVar, DATA%Nvar) :: RR, LL

    REAL, DIMENSION(DATA%Nvar, DATA%NDim) :: ff_b, ff_d, ff_i

    REAL, DIMENSION(DATA%NVar, DATA%Nvar, DATA%NDim) :: JJ_b, JJ_d
    REAL, DIMENSION(DATA%NVar, DATA%Nvar, DATA%NDim) :: AA_i, AA_b

    REAL, DIMENSION(DATA%NVarPhy) :: Vp_c

    INTEGER :: N_dim, j
    !----------------------------------------

    !==================
    !  Initialization
    !==================
    !Vectors - Matrices = 0
    Vp_b    = 0d0

    !Get Vp_i and n_b
    rhoi    = Vp_i(i_rho_vp)
    uveli   = Vp_i(i_vi1_vp)
    vveli   = Vp_i(i_vid_vp)
    ei      = Vp_i(i_eps_vp)
    ppi     = Vp_i(i_pre_vp)
    ai      = Vp_i(i_son_vp)
    tempi   = Vp_i(i_tem_vp)
    nx      = n_b(1)
    ny      = n_b(2)

    !Get Boundary data
    P_oo    = DATA%P_oo_turb
    rho_oo  = DATA%rho_oo_turb
    alpha   = DATA%alpha_oo_turb
    T_oo    = T__rho_P(rho_oo, P_oo)
    s_oo    = s__rho_T(rho_oo, T_oo)  
    e_oo    = e__rho_T(rho_oo, T_oo)
    ht_oo   = e_oo + P_oo/rho_oo


    !==================
    !  From Vp_i to Vp_b
    !==================
    !Calculation of Vp_b from Vp_i
    usdn  = 1.d0/(COS(alpha)*nx + SIN(alpha)*ny)
    usdn2 = usdn**2.

    nor_veli   = uveli * nx + vveli * ny
    tan_veli_x = uveli - nor_veli * nx
    tan_veli_y = vveli - nor_veli * ny

    am0        = ppi + rhoi*ai*nor_veli
    t_tent     = T_oo
    ro_tent    = rho_oo

    !BOUCLE
    DO i = 1, 50

       t_tent1 = t_tent
       ro_tent = rho__s_T(s_oo, t_tent1)
       ac      = c__rho_T(ro_tent, t_tent1)
       am0     = ppi + ro_tent*ac*nor_veli
       t_tent  = EOS_TEICALC(ht_oo,ro_tent,(usdn/(ro_tent*ac))**2.,am0) 

       err = ABS((t_tent1 - t_tent)/t_tent1)

       IF (err <= 1.0E-6) THEN

          icont = 1
          t_c   = t_tent
          rhoc  = rho__s_T(s_oo, t_tent)
          p_c   = P__rho_T(rhoc, t_c)
          ac    = c__rho_T(rhoc, t_c)

          nor_velc = (am0 - p_c)/(rhoc*ac)

          EXIT
       ENDIF

    END DO

    IF (icont/=1.) THEN

       WRITE(*,*) "La condition limite INLET TURBINE SUBSONIQUE n'a pas converge sur 50 iterations (erreur < 1e-5 "
       WRITE(*,*)  "subroutine imp_BC_entree_turbine"
       WRITE(*,*) "ERREUR = ", err
       WRITE(*,*) "rho = ", ro_tent
       WRITE(*,*) "T   = ", t_tent

       STOP

       rhoc     = ro_tent
       t_c      = t_tent
       p_c = P__rho_T(rhoc, t_c)
       ac = c__rho_T(rhoc, t_c)
       nor_velc = (am0 - p_c)/(rhoc*ac)

    END IF

    uvelc = (nor_velc*usdn)*COS(alpha)
    vvelc = (nor_velc*usdn)*SIN(alpha)

    !Getting Vp_b
    Vp_b(i_pre_vp) = p_c
    Vp_b(i_rho_vp) = rhoc
    Vp_b(i_tem_vp) = t_c
    Vp_b(i_eps_vp) = e__rho_T(rhoc, t_c)
    Vp_b(i_son_vp) = ac
    Vp_b(i_vi1_vp) = uvelc
    Vp_b(i_vid_vp) = vvelc

    CALL imp_Steger_Warming_Flux(n_b, Vp_i, Vp_b, fb_n, Jac)

    !---------------------------------------------------

!!$    N_dim = Data%NDim
!!$
!!$    CALL Advection_eigenvalues( Vp_i, n_b, lambda)
!!$    CALL Advection_eigenvectors(Vp_i, n_b, RR, LL)
!!$
!!$    ! Boundary INTERNAL state
!!$    CALL Prim_to_Cons(Vp_i, u_d);   v_d = MATMUL(LL, u_d)
!!$
!!$    ! Collected state
!!$    Vp_c = Vp_b
!!$
!!$    CALL Prim_to_Cons(Vp_c, u_c);   v_c = MATMUL(LL, u_c)
!!$
!!$    ! Boundary EXTERNAL STATE
!!$    DO j = 1, SIZE(lambda)
!!$
!!$       IF( lambda(j) < 0.d0 ) THEN
!!$          v_d(j) = 0.d0 ! NN
!!$       ELSE
!!$          v_c(j) = 0.d0 ! SN
!!$       ENDIF
!!$
!!$    ENDDO
!!$      
!!$    u_b = MATMUL( RR, v_c + v_d )
!!$
!!$    ! Flux correction
!!$    CALL Cons_to_Prim(u_b, Vp_b)
!!$
!!$    CALL advection_flux(N_dim, Vp_b, ff_b ) 
!!$    CALL advection_flux(N_dim, Vp_i, ff_d ) 
!!$
!!$    fb_n = 0.d0
!!$    DO id = 1, N_dim
!!$       fb_n =  fb_n + (ff_b(:, id) - ff_d(:, id)) * n_b(id)
!!$    ENDDO
!!$
!!$    ! Jacobian
!!$    CALL advection_Jacobian(N_dim, Vp_b, JJ_b)
!!$    CALL advection_Jacobian(N_dim, Vp_i, JJ_d)
!!$
!!$    Jac = 0.d0
!!$    DO id = 1, N_dim
!!$       Jac = Jac + (JJ_b(:,:, id) - JJ_d(:,:, id)) * n_b(id)
!!$    ENDDO    

    !-------------------------------------------

!!$    !==================
!!$    !  From Vp_b to flux and Jacobian
!!$    !==================
!!$    !Calculation of ff_i = EulerFlux(Vp_i)
!!$    CALL advection_flux(Data%Ndim, Vp_i, ff_i)
!!$
!!$    !Calculation of ff_b = EulerFlux(Vp_b)
!!$    CALL advection_flux(Data%Ndim, Vp_b, ff_b)
!!$
!!$    !Calculation of fb_n = (ff_b - ff_i).n_b
!!$    fb_n = MATMUL((ff_b - ff_i), n_b)
!!$
!!$    !Calculation of AA_i = Jac(Vp_i)
!!$    CALL advection_Jacobian(Data%Ndim, Vp_i, AA_i)
!!$
!!$    !Calculation of AA_b = Jac(Vp_b)
!!$    CALL advection_Jacobian(Data%Ndim, Vp_b, AA_b)
!!$
!!$    !Calculation of Jac  = (AA_b - AA_i).n_b
!!$    Jac = 0d0
!!$    DO id = 1, Data%Ndim
!!$       Jac = Jac + (AA_b(:,:,id) - AA_i(:,:,id))*n_b(id)
!!$    ENDDO

  END SUBROUTINE imp_BC_entree_turbine
  !==============================

  !>>>  Sortie Turbine <<<  Code 92
  !==============================================
  SUBROUTINE imp_BC_sortie_turbine(n_b, Vp_i, fb_n, Jac)
    !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    REAL, DIMENSION(:,:), INTENT(OUT) :: Jac
    !----------------------------------------

    REAL, DIMENSION(DATA%NVar) :: lambda
    REAL, DIMENSION(DATA%NVar) :: u_d, v_d, u_c, v_c, u_b
    REAL, DIMENSION(DATA%NVar, DATA%Nvar) :: RR, LL

    REAL, DIMENSION(DATA%Nvar, DATA%NDim) :: ff_b, ff_d, ff_i

    REAL, DIMENSION(DATA%NVar, DATA%Nvar, DATA%NDim) :: JJ_b, JJ_d
    REAL, DIMENSION(DATA%NVar, DATA%Nvar, DATA%NDim) :: AA_i, AA_b

    REAL, DIMENSION(DATA%NVarPhy) :: Vp_c, Vp_b

    INTEGER :: N_dim, id, j
    !----------------------------------------

    !    REAL, DIMENSION(SIZE(Vp_i)) :: Vp_b 

    REAL :: rhoi, uveli, vveli, ei, ppi, ai, tempi, si, nx, ny
    REAL :: P_e, r_turb, Pc, rhoc, Tc, T_tent, err

    INTEGER ::  icont, i
    !----------------------------------------

    !==================
    !  Initialization
    !==================
    !Vectors - Matrices = 0
    Vp_b    = 0.d0

    !Get Vp_i and n_b
    rhoi    = Vp_i(i_rho_vp)
    uveli   = Vp_i(i_vi1_vp)
    vveli   = Vp_i(i_vid_vp)
    ei      = Vp_i(i_eps_vp)
    ppi     = Vp_i(i_pre_vp)
    ai      = Vp_i(i_son_vp)
    tempi   = Vp_i(i_tem_vp)
    si      = s__rho_T(rhoi, tempi)
    nx      = n_b(1)
    ny      = n_b(2)

    !Get Boundary data
    r_turb  = DATA%r_turb
    P_e     = DATA%P_oo_turb
    Pc      = P_e / r_turb 

    !==================
    !  From Vp_i to Vp_b
    !==================
    !Calculation of Vp_b from Vp_i
    icont   = 0
    T_tent  = 1.01d0 !0.97d0 

    DO i = 1, 40

       Tc     = T_tent
       rhoc   = rho__P_T(Pc, Tc)
       T_tent = T__rho_s(rhoc, si)

       err    = ABS((T_tent - Tc)/T_tent)

       IF (err .LT. 1.0E-10 )  THEN

          icont = 1
          Tc   = T_tent
          EXIT
       END IF

    END DO

    !No convergence CASE
    IF (icont/=1.) THEN

       WRITE(*,*) "La condition limite OUTLET TURBINE SUBSUNIQUE n'a pas converge en 40 iterations (erreur < 1e-4)"
       WRITE(*,*)  "subroutine imp_BC_sortie_turbine" 
       WRITE(*,*) "ERREUR = ", err
       WRITE(*,*) "rho = ", rhoc
       WRITE(*,*) "T   = ", T_tent
       STOP

    END IF

    !Getting Vp_b
    Vp_b(i_pre_vp) = Pc                   !Pressure imposed
    Vp_b(i_vi1_vp) = uveli                 !Velocity from inside
    Vp_b(i_vid_vp) = vveli
    Vp_b(i_tem_vp) = Tc                    !Tc   = T(sc, Pc)
    Vp_b(i_rho_vp) = rho__P_T(Pc, Tc)      !rhoc = rho(sc, Tc) 
    Vp_b(i_eps_vp) = e__rho_T(rhoc, Tc)
    Vp_b(i_son_vp) = c__rho_T(rhoc, Tc)

    !-------------------------------------------
!!$    ! density extrapolation
!!$    Vp_b = Vp_P(Vp_i, Pc)
    CALL imp_Steger_Warming_Flux(n_b, Vp_i, Vp_b, fb_n, Jac)

!!$    N_dim = Data%NDim
!!$
!!$    CALL Advection_eigenvalues( Vp_i, n_b, lambda)
!!$    CALL Advection_eigenvectors(Vp_i, n_b, RR, LL)
!!$
!!$    ! Boundary INTERNAL state
!!$    CALL Prim_to_Cons(Vp_i, u_d);   v_d = MATMUL(LL, u_d)
!!$
!!$    ! Collected state
!!$!!    Vp_c(i_rho_vp)          = Vp_i(i_rho_vp)
!!$!!    Vp_c(i_vi1_vp:i_vid_vp) = Vp_i(i_vi1_vp:i_vid_vp)
!!$!!    Vp_c(i_pre_vp)          = Data%P_oo_turb / Data%r_turb 
!!$!!
!!$!!    Vp_c(i_tem_vp) = T__rho_P( Vp_c(i_rho_vp), &
!!$!!                               Vp_c(i_pre_vp)  )
!!$!!
!!$!!    Vp_c(i_eps_vp) = e__rho_T( Vp_c(i_rho_vp), &
!!$!!                               Vp_c(i_tem_vp)  )
!!$!!
!!$!!    Vp_c(i_son_vp) = c__rho_T( Vp_c(i_rho_vp), &
!!$!!                               Vp_c(i_tem_vp)  )
!!$
!!$    Vp_c = Vp_b
!!$
!!$    CALL Prim_to_Cons(Vp_c, u_c);   v_c = MATMUL(LL, u_c)
!!$
!!$    ! Boundary EXTERNAL STATE
!!$    DO j = 1, SIZE(lambda)
!!$
!!$       IF( lambda(j) < 0.d0 ) THEN
!!$          v_d(j) = 0.d0 ! NN
!!$       ELSE
!!$          v_c(j) = 0.d0 ! SN
!!$       ENDIF
!!$
!!$    ENDDO
!!$       
!!$    u_b = MATMUL( RR, v_c + v_d )
!!$
!!$    ! Flux correction
!!$    CALL Cons_to_Prim(u_b, Vp_b)
!!$
!!$    CALL advection_flux(N_dim, Vp_b, ff_b) 
!!$    CALL advection_flux(N_dim, Vp_i, ff_d)
!!$
!!$    fb_n = 0.d0
!!$    DO id = 1, N_dim
!!$       fb_n =  fb_n + (ff_b(:, id) - ff_d(:, id)) * n_b(id)
!!$    ENDDO
!!$
!!$    ! Jacobian
!!$    CALL advection_Jacobian(N_dim, Vp_b, JJ_b)
!!$    CALL advection_Jacobian(N_dim, Vp_i, JJ_d)
!!$
!!$    Jac = 0.d0
!!$    DO id = 1, N_dim
!!$       Jac = Jac + (JJ_b(:,:, id) - JJ_d(:,:, id)) * n_b(id)
!!$    ENDDO

    !-------------------------------------------

!!$    !Calculation of ff_i = EulerFlux(Vp_i)
!!$    CALL advection_flux(Data%Ndim, Vp_i, ff_i)
!!$
!!$    !Calculation of ff_b = EulerFlux(Vp_b)
!!$    CALL advection_flux(Data%Ndim, Vp_b, ff_b)
!!$
!!$    !Calculation of fb_n = (ff_b - ff_i).n_b
!!$    fb_n = MATMUL((ff_b - ff_i), n_b)
!!$
!!$    !Calculation of AA_i = Jac(Vp_i)
!!$    CALL advection_Jacobian(Data%Ndim, Vp_i, AA_i)
!!$
!!$    !Calculation of AA_b = Jac(Vp_b)
!!$    CALL advection_Jacobian(Data%Ndim, Vp_b, AA_b)
!!$
!!$    !Calculation of Jac  = (AA_b - AA_i).n_b
!!$    Jac = 0d0
!!$    DO id = 1, Data%Ndim
!!$       Jac = Jac + (AA_b(:,:,id) - AA_i(:,:,id))*n_b(id)
!!$    END DO

  END SUBROUTINE imp_BC_sortie_turbine
  !==============================

  !==============================================================
  SUBROUTINE imp_BC_subsonic_inflow_Pt(n_b, V_t, Vp_i, fb_n, Jac)
    !==============================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: V_t
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    REAL, DIMENSION(:,:), INTENT(OUT) :: Jac
    !----------------------------------------

    REAL, DIMENSION(SIZE(Vp_i)) :: Vp_b

    REAL, DIMENSION(SIZE(n_b)) :: vel, flow_dir

    REAL :: P_t, T_t, H_t
    REAL :: aa, bb, cc, dd, vel_mag, Mach2, Cp
    REAL :: g_1, g, R_p, c2_tot, tem, alpha
    REAL :: rho, vel2, ene, pre, c2
    !----------------------------------------

    ! Total Pressure
    P_t = V_t(i_pre_vp)

    ! Total Temperature
    T_t = V_t(i_tem_vp)

    flow_dir = V_t(i_vi1_vp:i_vid_vp)

    g_1 = EOSKappa()
    g   = g_1 + 1.d0

    Cp = 1.d0/g_1

    ! Total enthalpy
    H_t = Cp*T_t

    ! Solution at the bounday
    rho = Vp_i(i_rho_vp)
    vel = Vp_i(i_vi1_vp:i_vid_vp)
    vel2 = SQRT(SUM(vel**2))
    ene = Vp_i(i_eps_vp)
    pre = Vp_i(i_pre_vp)
    c2 = Vp_i(i_son_vp)**2

    ! Riemann invariant
    R_p = DOT_PRODUCT(vel, n_b) + 2.0*SQRT(c2)/g_1

    ! Total speed of sound 
    c2_tot = g_1*(H_t - (ene + Pre/rho)) + c2

    alpha = DOT_PRODUCT(n_b, flow_dir)

    ! Coefficients in the quadratic equation for the velocity
    aa = 1.0 + 0.5*g_1*alpha*alpha
    bb = -1.0*g_1*alpha*R_p
    cc = 0.5*g_1*R_p*R_p - 2.0*c2_tot/g_1

    ! Solve quadratic equation for velocity magnitude
    ! Value must be positive
    dd = bb*bb - 4.0*aa*cc
    dd = SQRT(MAX(0.0,dd))
    Vel_Mag   = (-bb + dd)/(2.0*aa)
    Vel_Mag   = MAX(0.0,Vel_Mag)
    Vel2      = Vel_Mag*Vel_Mag

    ! Speed of sound from total speed of sound
    c2 = c2_tot - 0.5*g_1*Vel2

    ! Mach squared (cut between 0-1), use to adapt velocity
    Mach2 = Vel2/c2
    Mach2 = MIN(1.0,Mach2)
    Vel2  = Mach2*c2
    Vel_Mag = SQRT(Vel2)
    c2 = c2_tot - 0.5*g_1*Vel2

    ! New velocity vector at the inlet
    Vel = Vel_Mag*Flow_dir

    ! Static temperature from the speed of sound 
    Tem = c2  ! T = c2/(gamma * R)

    ! Static pressure from isentropic relation
    Pre = P_t * (Tem/T_t)**(g/g_1)

    rho = rho__P_T(Pre, Tem)
    ene  = e__rho_T(rho, Tem)

    ! Solution state at the boundary
    Vp_b = V_t ![turb]

    Vp_b(i_rho_vp) = rho
    Vp_b(i_vi1_vp:i_vid_vp) = vel
    Vp_b(i_eps_vp) = ene
    Vp_b(i_pre_vp) = Pre
    Vp_b(i_tem_vp) = Tem
    Vp_b(i_son_vp) = c__rho_T(rho, Tem)

    CALL imp_Steger_Warming_Flux(n_b, Vp_i, Vp_b, fb_n, Jac)

  END SUBROUTINE imp_BC_subsonic_inflow_Pt
  !=======================================

#ifdef NAVIER_STOKES
  !=========================================================
  SUBROUTINE imp_BC_Adiabatic_Wall(n_b, Vp, D_Vc, fb_n, Jac)
    !=========================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:), INTENT(IN)  :: D_Vc
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    REAL, DIMENSION(:,:), INTENT(OUT) :: Jac
    !-----------------------------------------

    REAL, DIMENSION(SIZE(D_Vc,1), SIZE(D_Vc,2)) :: J_q

    REAL, DIMENSION(SIZE(n_b)) :: q

    INTEGER :: id
    !-----------------------------------------

    q = Thermal_flux(Vp, D_Vc)

    fb_n = 0.d0
    fb_n(i_ren_ua) = DOT_PRODUCT(q, n_b)

    !---------
    ! Jacobian
    !---------------------------------
    Jac = 0.d0

!!$    k_DT = Jacobian_thermal_flux(Vp, D_Vc)
!!$
!!$    DO id = 1, SIZE(q)
!!$
!!$       Jac(i_ren_ua, :) = K_DT * D_phi * n_b(id)
!!$
!!$    ENDDO

  END SUBROUTINE imp_BC_Adiabatic_Wall
  !===================================

#endif

  !//////////////////////////////////////////////////////////////////////////
  !//                                                                      //
  !//                    PERIODIC BOUNDARY CONDITIONS                      //
  !//                            (implicit)                                //
  !//                                                                      //
  !//////////////////////////////////////////////////////////////////////////

  !===================================
  SUBROUTINE imp_Periodic_BC(Var, Mat)
    !===================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Mat
    !--------------------------------------

    REAL, DIMENSION(DATA%NVar) :: Res_k, Res_i
    REAL, DIMENSION(DATA%NVar, DATA%NVar) :: MM_k, MM_i

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: temp

    INTEGER :: jb, NU_i, NU_k, j_i, j_k, n
    INTEGER :: idx_s_i, idx_e_i, idx_s_k, idx_e_k    
    !-------------------------------------- 

    LOGICAL, DIMENSION(DATA%Nvar) :: Mask

    IF( .NOT. ALLOCATED(per_conn) ) RETURN

    DO jb = 1, SIZE(per_conn, 1)

       NU_i = per_conn(jb, 1)
       NU_k = per_conn(jb, 2)

       Res_i = Var%Flux(:, NU_i)
       Res_k = Var%Flux(:, NU_k)

       Var%Flux(:, NU_i) = Var%Flux(:, NU_i) + Res_k

       Var%Flux(:, NU_k) = Var%Flux(:, NU_k) + Res_i !xxx
       !Var%Flux(:, NU_k) = 0.d0          !xxx
       !Var%Ua(:, NU_k) = Var%Ua(:, NU_i) !xxx
       !Var%Vp(:, NU_k) = Var%Vp(:, NU_i) !xxx

       ! Jacobian

       !!Mask = .TRUE. ! xxx
       !!MM_k = Identity_Matrix(Data%Nvar) ! xxx
       !!CALL assemblage_nullifyExtAndReplaceDiagMask(MM_k, NU_k, Mask, Mat) !xxx

       j_i = Mat%IDiag(NU_i) !xxx
       j_k = Mat%IDiag(NU_k) !xxx

       MM_i = Mat%Vals(:,:, j_i) !xxx 
       MM_k = Mat%Vals(:,:, j_k) !xxx

       Mat%Vals(:,:, j_i) = Mat%Vals(:,:, j_i) + MM_k
       Mat%Vals(:,:, j_k) = Mat%Vals(:,:, j_k) + MM_i

!!$       idx_s_i = Mat%Jposi(Nu_i)
!!$       idx_e_i = Mat%Jposi(Nu_i+1) - 1
!!$
!!$       idx_s_k = Mat%Jposi(Nu_k)
!!$       idx_e_k = Mat%Jposi(Nu_k+1) - 1
!!$
!!$       n = idx_e_i - idx_s_i + 1
!!$       
!!$       ALLOCATE( temp( SIZE(Mat%Vals, 1), &
!!$                       SIZE(Mat%Vals, 2), &
!!$                       n                ) )
!!$
!!$       temp = Mat%Vals(:,:, idx_s_i:idx_e_i)
!!$
!!$       Mat%Vals(:,:, idx_s_i:idx_e_i) = temp + &
!!$                                        Mat%Vals(:,:, idx_s_k:idx_e_k)
!!$
!!$
!!$       Mat%Vals(:,:, idx_s_k:idx_e_k) = Mat%Vals(:,:, idx_s_k:idx_e_k) + &
!!$                                        temp
!!$
!!$       DEALLOCATE(temp)
!!$       
!!$       Mat%Vals(:,:, idx_s_i:idx_e_i) = 0.5 * Mat%Vals(:,:, idx_s_i:idx_e_i)
!!$       Mat%Vals(:,:, idx_s_k:idx_e_k) = 0.5 * Mat%Vals(:,:, idx_s_k:idx_e_k)

       !CALL assemblage_setidentblokline(NU_i, Mat)
       !CALL assemblage_setidentblokline(NU_k, Mat)

    ENDDO

  END SUBROUTINE imp_Periodic_BC
  !=============================

  !//////////////////////////////////////////////////////////////////////////
  !//                                                                      //
  !//                     WEAK BOUNDARY CONDITIONS                         //
  !//                            (explicit)                                //
  !//                                                                      //
  !//////////////////////////////////////////////////////////////////////////

  !======================
  SUBROUTINE Weak_BC(Var)
    !======================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

!    TYPE(face_str) :: loc_ele

    INTEGER, DIMENSION(:),   ALLOCATABLE :: NU
    REAL,    DIMENSION(:,:), ALLOCATABLE :: fb_n
    REAL,    DIMENSION(:,:), ALLOCATABLE :: Vc
    REAL,    DIMENSION(:,:), ALLOCATABLE :: Vp
    REAL,    DIMENSION(:,:), ALLOCATABLE :: Rb_i
    !REAL,    DIMENSION(:),   ALLOCATABLE :: ff_NS_n
    !REAL,    DIMENSION(:),   ALLOCATABLE :: ff_NSw_n

    LOGICAL :: is_WeakBC

    INTEGER :: bc_idx, bc_type, N_dofs, N_var, N_dim
    INTEGER :: jf, i, j, k, iq
    !---------------------------------------------

    N_var = DATA%Nvar
    N_dim = DATA%Ndim

    DO jf = 1, SIZE(b_element)

       !loc_ele = b_element(jf)%b_f

       N_dofs = b_element(jf)%b_f%N_points

       ALLOCATE( fb_n(N_var, N_dofs) )

       ALLOCATE( NU(N_dofs) )
       NU = b_element(jf)%b_f%NU

       ALLOCATE( Vc(SIZE(Var%Ua,1), N_dofs) )
       Vc = Var%Ua(:,Nu)!0.5*( Var%Ua(:, NU)  + Var%Ua_0(:, NU) )

       ALLOCATE( Vp(SIZE(Var%Vp,1), N_dofs) )
       Vp = Var%Vp(:,Nu)!0.5*( Var%Vp(:, NU) + Var%Vp_0(:, NU) )

       ALLOCATE( Rb_i(N_var, N_dofs) )


#ifdef NAVIER_STOKES
       ! Boundary integral of the viscous flux
       IF(DATA%Iflux == 4) THEN
          DO i = 1, N_dofs
             CALL Diffusion_flux_n(N_dim, Vp(:, i), Var%D_Ua(:,:, NU(i)), &
                  b_element(jf)%b_f%n_b(:, i), fb_n(:, i))
          ENDDO

          DO i = 1, N_dofs
             Var%Flux(:, NU(i)) = Var%Flux(:, NU(i)) - &
                  bInt_i(b_element(jf)%b_f, i, fb_n)
          ENDDO
       ENDIF
#endif

       Rb_i = impose_WeakBC_exp(b_element(jf)%b_f, Vc, Vp)

       !--------------------------------
       ! BC contribution to the residual
       !----------------------------------------------
       DO i = 1, N_dofs

          Var%Flux(:, NU(i)) = Var%Flux(:, NU(i)) + Rb_i(:, i)

       ENDDO

       DEALLOCATE( fb_n, Vc, Vp, NU, Rb_i )

    ENDDO

  END SUBROUTINE Weak_BC
  !=====================

  !==================================
  SUBROUTINE Weak_BC_i(Nu_i, Vc, R_i)
    !==================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN)    :: Nu_i
    REAL, DIMENSION(:,:), INTENT(IN)    :: Vc
    REAL, DIMENSION(:),   INTENT(INOUT) :: R_i
    !--------------------------------------------

    !TYPE(face_str) :: ele_b

    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vc_l
    REAL, DIMENSION(:,:,:), ALLOCATABLE :: DVc_l
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vp_l
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Phi_i
    REAL, DIMENSION(:,:),   ALLOCATABLE :: fb_n

    INTEGER :: jf, i, k, l, N_var, N_dofs
    !--------------------------------------------

    N_var = DATA%NVar

    IF( ALLOCATED(Connect(Nu_i)%cn_bele) ) THEN

       DO l = 1, SIZE(Connect(Nu_i)%cn_bele)

          jf = Connect(Nu_i)%cn_bele(l)

          !ele_b = b_element(jf)%b_f

          N_dofs = b_element(jf)%b_f%N_points

          ALLOCATE( Vc_l(DATA%NVar,    N_dofs) )
          ALLOCATE( Vp_l(DATA%NVarPhy, N_dofs) )
          ALLOCATE(Phi_i(DATA%NVar,    N_dofs) )

#ifdef NAVIER_STOKES

          ALLOCATE( DVc_l(DATA%NDim, N_var, N_dofs) )
          ALLOCATE( fb_n(N_var, N_dofs) )
#endif

          Vc_l = Vc(:, b_element(jf)%b_f%Nu)

          DO k = 1, N_dofs            

             CALL Cons_to_Prim( Vc_l(:, k), Vp_l(:, k) )

#ifdef NAVIER_STOKES

             DVc_l(:, :, k) = Grad_i__tmp(b_element(jf)%b_f%Nu(k), Vc)
#endif

          ENDDO


#ifdef NAVIER_STOKES
          ! Boundary integral of the viscous flux
          IF(DATA%Iflux == 4) THEN
             DO i = 1, N_dofs
                CALL Diffusion_flux_n(DATA%Ndim, Vp_l(:, i), DVc_l(:,:, i), &
                     b_element(jf)%b_f%n_b(:, i), fb_n(:, i))
             ENDDO

             DO i = 1, N_dofs

                IF( b_element(jf)%b_f%Nu(i) == Nu_i ) THEN
                   R_i = R_i - bInt_i(b_element(jf)%b_f, i, fb_n)
                   EXIT
                ENDIF

             ENDDO
          ENDIF
#endif

          Phi_i = impose_WeakBC_exp(b_element(jf)%b_f, Vc_l, Vp_l)

          !--------------------------------
          ! BC contribution to the residual
          !----------------------------------------------
          DO i = 1, N_dofs

             IF( b_element(jf)%b_f%Nu(i) == Nu_i ) THEN

                R_i = R_i + Phi_i(:, i)
                EXIT

             ENDIF

          ENDDO

          DEALLOCATE( Vc_l, Vp_l, Phi_i )

#ifdef NAVIER_STOKES
          DEALLOCATE( DVc_l, fb_n )
#endif

       ENDDO

    ENDIF

  END SUBROUTINE Weak_BC_i
  !=======================  

  !=====================================================
  FUNCTION impose_WeakBC_exp(ele_b, Vc, Vp) RESULT(Rb_i)
    !=====================================================

    IMPLICIT NONE

    TYPE(face_str),       INTENT(IN) :: ele_b
    REAL, DIMENSION(:,:), INTENT(IN) :: Vc
    REAL, DIMENSION(:,:), INTENT(IN) :: Vp

    REAL, DIMENSION(SIZE(Vc,1), SIZE(Vc, 2)) :: Rb_i
    !-----------------------------------------------

    REAL, DIMENSION(SIZE(Vc,1), SIZE(Vc, 2)) :: fb_n

    REAL, DIMENSION(SIZE(Vp,1)) :: Vp_ext

    INTEGER :: i, N_dofs, bc_idx, bc_type

    LOGICAL :: is_WeakBC
    !-----------------------------------------------

    Rb_i = 0.d0

    is_WeakBC = .TRUE.

    bc_idx  = ele_b%bc_type
    bc_type = DATA%FacMap(bc_idx)

    N_dofs = ele_b%N_points

    SELECT CASE(bc_type)

       !>>>  Paroi glissante <<<
       !########################
    CASE(lgc_paroi_glissante) ! Code 21
       !########################

       DO i = 1, N_dofs
          CALL BC_Slip_Wall( ele_b%n_b(:,i), &
               Vp(:,i), fb_n(:,i) )
       END DO

       !>>> Symmetry plane <<<
       !####################### 
    CASE(lgc_symetrie_plane)
       !#######################

       DO i = 1, N_dofs
          CALL BC_Sym_Plane( ele_b%n_b(:,i), &
               Vp(:,i), fb_n(:,i) )
       END DO

!!$#ifdef NAVIER_STOKES
!!$
!!$       ! >>> Paroi adherente adiabatique <<<
!!$       !#################################
!!$       CASE(lgc_paroi_adh_adiab_flux_nul)
!!$       !#################################
!!$
!!$          ALLOCATE(  ff_NS_n(N_var), &
!!$                    ff_NSw_n(N_var) )
!!$     
!!$          DO i = 1, N_dofs
!!$
!!$             ! BC for the normal component of the velocity
!!$             CALL BC_Slip_Wall( b_element(jf)%b_f%n_b(:,i), &
!!$                                Vp(:,i), fb_n(:,i) )
!!$
!!$             ! NS flux function
!!$             CALL Diffusion_flux_n(N_dim, Vp(:,i), Var%D_Ua(:,:,NU(i)), &
!!$                                   b_element(jf)%b_f%n_b(:,i), ff_NS_n)
!!$
!!$             ! Solid Wall NS flux function
!!$             CALL BC_Wall_Diff_flux_n(b_element(jf)%b_f, i, Var%Ua, ff_NSw_n)
!!$             
!!$             fb_n(:,i) = fb_n(:,i) - (ff_NSw_n - ff_NS_n)
!!$ 
!!$          ENDDO
!!$
!!$          DEALLOCATE( ff_NS_n, ff_NSw_n )
!!$
!!$#endif

       ! >>> Sortie_subsonique <<<          
       ! >>> Sortie_mixt <<<
       ! >>> Flux de stege <<<  
       !##########################
    CASE(lgc_sortie_subsonique, &
          lgc_sortie_mixte, &
          lgc_flux_de_steger) 
       !##########################

       Vp_ext = DATA%Fr(bc_idx)%Vp

       DO i = 1, N_dofs
          CALL Steger_Warming_Flux( ele_b%n_b(:,i), &
               Vp(:,i), Vp_ext, fb_n(:,i) )
       ENDDO

       ! >>>  Steger-Warming <<<
       !      pression infini
       !##########################
    CASE(lgc_ns_steger_warming) ! Code 154
       !##########################

       DO i = 1, N_dofs

          Vp_ext = Vp_P( Vp(:, i), DATA%Fr(bc_idx)%Vp(i_pre_vp) )

          CALL Steger_Warming_Flux( ele_b%n_b(:,i), &
               Vp(:,i), Vp_ext, fb_n(:,i) )

       ENDDO


       !###########################
    CASE(lgc_subsonic_inflow_Pt) ! Code 95
       !###########################

       Vp_ext = DATA%Fr(bc_idx)%Vp

       DO i = 1, N_dofs
          CALL BC_subsonic_inflow_Pt( ele_b%n_b(:,i), &
               Vp_ext, Vp(:,i), fb_n(:,i) )
       END DO


       !>>>  Sortie Turbine <<<
       !########################
    CASE(lgc_sortie_turbine_subsonique) ! Code 92
       !########################

       DO i = 1, N_dofs
          CALL BC_sortie_turbine( ele_b%n_b(:,i), &
               Vp(:,i), fb_n(:,i))
       END DO

       !>>>  Entrée Turbine <<<
       !########################
    CASE(lgc_entree_turbine_subsonique) ! Code 91
       !########################

       DO i = 1, N_dofs
          CALL BC_entree_turbine( ele_b%n_b(:,i), &
               Vp(:,i), fb_n(:,i))
       END DO

       !##################################
    CASE(lgc_gradient_nul_exterieur,    &
         lgc_gradient_nul_interieur,    &
         lgc_dirichlet, lgc_paroi_misc, &
         lgc_paroi_adh_adiab_flux_nul,  &
         lgc_periodique                 )
       !###################################

       is_WeakBC = .FALSE.

    CASE DEFAULT

       WRITE(*,*) 'ERROR: Unknown boundary contidion in impose_WeakBC_exp'
       WRITE(*,*) 'Code of the boundary selected : ', bc_type
       STOP

    END SELECT

    IF( is_WeakBC ) THEN

       !--------------------------------
       ! BC contribution to the residual
       !----------------------------------------------
       DO i = 1, N_dofs

          Rb_i(:, i) = bInt_i(ele_b, i, fb_n)

       ENDDO

    ENDIF

  END FUNCTION impose_WeakBC_exp
  !=============================  


  !//////////////////////////////////////////////////////////////////////////
  !//                                                                      //
  !//                   STRONG BOUNDARY CONDITIONS                         //
  !//                          (explicit)                                  //
  !//                                                                      //
  !//////////////////////////////////////////////////////////////////////////

  !==================================
  SUBROUTINE Strong_BC(Var, Set_VcVp)
    !==================================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    LOGICAL,         INTENT(IN)    :: Set_VcVp
    !------------------------------------

!    TYPE(face_str) :: loc_ele

    INTEGER, DIMENSION(:), ALLOCATABLE :: NU

    REAL, DIMENSION(:,:), ALLOCATABLE :: Vc
    REAL, DIMENSION(:,:), ALLOCATABLE :: Vp
    REAL, DIMENSION(:,:), ALLOCATABLE :: Rb_i

    INTEGER :: jf, N_dofs
    !---------------------------------------------

    DO jf = 1, SIZE(b_element)

       !loc_ele = b_element(jf)%b_f

       N_dofs = b_element(jf)%b_f%N_points

       ALLOCATE( NU(N_dofs) )
       NU = b_element(jf)%b_f%NU

       ALLOCATE( Vc(SIZE(Var%Ua,1), N_dofs) )
       Vc = Var%Ua(:,Nu)!0.5*( Var%Ua(:, NU) + Var%Ua_0(:, NU) )

       ALLOCATE( Vp(SIZE(Var%Vp,1), N_dofs) )
       Vp = Var%Vp(:,Nu)!0.5*( Var%Vp(:, NU) + Var%Vp_0(:, NU) )

       ALLOCATE( Rb_i(SIZE(Var%Ua,1), N_dofs) )
       Rb_i = Var%Flux(:, NU)

#ifdef NAVIER_STOKES
       CALL impose_StrongBC_exp(b_element(jf)%b_f, Vc, Vp, Set_VcVp, Rb_i, Var%D_ua(:,:, NU) )
#else
       CALL impose_StrongBC_exp(b_element(jf)%b_f, Vc, Vp, Set_VcVp, Rb_i )
#endif       

       IF(Set_VcVp) THEN
          Var%Ua(:, NU) = Vc
          Var%Vp(:, NU) = Vp 
       ELSE
          Var%Flux(:, NU) = Rb_i
       ENDIF

       DEALLOCATE( Vp, Vc, Rb_i, NU )

    ENDDO

  END SUBROUTINE Strong_BC
  !=======================

  !====================================
  SUBROUTINE Strong_BC_i(Nu_i, Vc, R_i)
    !====================================

    IMPLICIT NONE
    INTEGER,              INTENT(IN)    :: Nu_i
    REAL, DIMENSION(:,:), INTENT(INOUT) :: Vc
    REAL, DIMENSION(:),   INTENT(INOUT) :: R_i
    !--------------------------------------------

    !TYPE(face_str) :: ele_b

    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vc_l
    REAL, DIMENSION(:,:,:), ALLOCATABLE :: DVc_l
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vp_l
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Phi_i

    INTEGER :: jf, i, k, l, N_var, N_dofs
    !--------------------------------------------

    N_var = DATA%NVar

    IF( ALLOCATED(Connect(Nu_i)%cn_bele) ) THEN

       DO l = 1, SIZE(Connect(Nu_i)%cn_bele)

          jf = Connect(Nu_i)%cn_bele(l)

          !ele_b = b_element(jf)%b_f

          N_dofs = b_element(jf)%b_f%N_points

          ALLOCATE( Vc_l(DATA%NVar,    N_dofs) )
          ALLOCATE( Vp_l(DATA%NVarPhy, N_dofs) )
          ALLOCATE(Phi_i(DATA%NVar,    N_dofs) )

#ifdef NAVIER_STOKES

          ALLOCATE( DVc_l(DATA%NDim, N_var, N_dofs) )

#endif

          Vc_l = Vc(:, b_element(jf)%b_f%Nu); Phi_i = 0.d0

          DO k = 1, N_dofs

             CALL Cons_to_Prim( Vc_l(:, k), Vp_l(:, k) )

             IF (b_element(jf)%b_f%Nu(k) == Nu_i) Phi_i(:, k) = R_i

#ifdef NAVIER_STOKES

             DVc_l(:, :, k) = Grad_i__tmp(b_element(jf)%b_f%Nu(k), Vc)

#endif

          ENDDO

#ifdef NAVIER_STOKES
          CALL impose_StrongBC_exp(b_element(jf)%b_f, Vc_l, Vp_l, .FALSE., Phi_i, DVc_l) 
#else
          CALL impose_StrongBC_exp(b_element(jf)%b_f, Vc_l, Vp_l, .FALSE., Phi_i)
#endif

          DO k = 1, N_dofs
             IF (b_element(jf)%b_f%Nu(k) == Nu_i) THEN
                R_i = Phi_i(:, k)
                EXIT
             ENDIF
          ENDDO

          DEALLOCATE( Vc_l, Vp_l, Phi_i )

#ifdef NAVIER_STOKES
          DEALLOCATE( DVc_l )
#endif

       ENDDO

    ENDIF

  END SUBROUTINE Strong_BC_i
  !=========================

  !==================================================================
  SUBROUTINE impose_StrongBC_exp(ele_b, Vc, Vp, Set_VcVp, Rb_i, D_Vc)
    !==================================================================

    IMPLICIT NONE

    TYPE(face_str),                   INTENT(IN)    :: ele_b
    REAL, DIMENSION(:,:),             INTENT(INOUT) :: Vc
    REAL, DIMENSION(:,:),             INTENT(INOUT) :: Vp
    LOGICAL,                          INTENT(IN)    :: Set_VcVp
    REAL, DIMENSION(:,:),             INTENT(INOUT) :: Rb_i
    REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: D_Vc
    !--------------------------------------------------------

    REAL, DIMENSION(SIZE(Vc,1), SIZE(Vc,2)) :: fb_n

    REAL :: T_w, Cv

    INTEGER :: bc_idx, bc_type, N_var, N_dofs, i
    !--------------------------------------------------------

    bc_idx  = ele_b%bc_type
    bc_type = DATA%FacMap(bc_idx)

    N_var = DATA%Nvar
    N_dofs = ele_b%N_points

    SELECT CASE(bc_type)

       ! >>> dirichlet <<<
       !##################
    CASE(lgc_dirichlet)
       !##################

       DO i = 1, N_dofs

          IF(Set_VcVp) THEN

             Vp(:, i) = DATA%Fr(bc_idx)%Vp

             CALL Prim_to_Cons( Vp(:, i), Vc(:, i) )

             Rb_i(:, i) = 0.d0

          ELSE

             Rb_i(:, i) = Vc(:, i)

          ENDIF

       ENDDO

#ifdef NAVIER_STOKES

       ! >>> Paroi adherente adiabatique <<<
       !#################################
    CASE(lgc_paroi_adh_adiab_flux_nul)
       !#################################

       fb_n = 0.d0

       DO i = 1, N_dofs

          IF(Set_VcVp) THEN

             ! Velocity on the wall
             !----------------------
             Vp(i_vi1_vp:i_vid_vp, i) = DATA%Fr(bc_idx)%Vp(i_vi1_vp:i_vid_vp)

             CALL Prim_to_Cons( Vp(:, i), Vc(:, i) )

          ENDIF

          ! Nullify the momentum reisdual
          !------------------------------
          Rb_i(i_rv1_ua:i_rvd_ua, i) = Vc(i_rv1_ua:i_rvd_ua, i)

          ! Turbulent bc
          !-------------
          IF(Turbulent) CALL Turb_wall_bc( Vp(:, i), Rb_i(:, i) )

          ! Adiabatic wall
          !---------------
          CALL BC_Adiabatic_Wall( ele_b%n_b(:,i), &
               Vp(:, i), D_Vc(:,:, i), fb_n(:,i) )

       ENDDO

       !--------------------------------
       ! BC contribution to the residual
       !----------------------------------------------
       DO i = 1, N_dofs 

          Rb_i(:, i) = Rb_i(:, i) - bInt_i(ele_b, i, fb_n)

       ENDDO

       !###################
    CASE(lgc_paroi_misc)
       !###################

       T_w = DATA%Fr(bc_idx)%Vp(i_tem_vp)

       DO i = 1, N_dofs

          Cv = EOSCv__T_rho( Vc(i_rho_ua, i), T_w )

          IF(Set_VcVp) THEN

             Vc(i_vi1_vp:i_vid_vp, i) = DATA%Fr(bc_idx)%Vp(i_vi1_vp:i_vid_vp)*Vc(i_ren_ua, i)
             Vc(i_ren_ua,          i) = T_w * Cv * Vc(i_rho_ua, i)

             CALL Cons_to_Prim( Vc(:, i), Vp(:, i) )

          ENDIF

          ! Nullify the momentum and the energy residuals
          Rb_i(i_rv1_ua:i_rvd_ua, i) = Vc(i_rv1_ua:i_rvd_ua, i)
          Rb_i(i_ren_ua,          i) = Vc(i_ren_ua, i) - T_w * Cv * Vc(i_rho_ua, i)

          ! Turbulent bc
          !-------------
          IF(Turbulent) CALL Turb_wall_bc( Vp(:, i), Rb_i(:, i) )

       ENDDO

#endif

    END SELECT

  END SUBROUTINE impose_StrongBC_exp
  !=================================

  !-------------------------------------------------------------
  !-------------------------------------------------------------
  !-------------------------------------------------------------

  !=====================================
  SUBROUTINE BC_Slip_Wall(n_b, Vp, fb_n)
    !=====================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    !----------------------------------------

    REAL :: rho, u_n, rho_un, e_k, h_t
    INTEGER :: i
    !----------------------------------------

    rho  = Vp(i_rho_vp)

    u_n = SUM( Vp(i_vi1_vp:i_vid_vp)*n_b )

    rho_un = rho*u_n

    e_k = 0.5*SUM(Vp(i_vi1_vp:i_vid_vp)**2)

    h_t = EOSenthalpy(Vp) + e_k

    !----------------
    ! Correction flux
    !-------------------------------------
    fb_n(i_rho_ua)          = -rho*u_n
    fb_n(i_rv1_ua:i_rvd_ua) = -rho*u_n * Vp(i_vi1_vp:i_vid_vp)
    fb_n(i_ren_ua)          = -rho*u_n * h_t

  END SUBROUTINE BC_Slip_Wall
  !==========================

  !======================================
  SUBROUTINE BC_Sym_Plane( n_b, Vp, fb_n)
    !======================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    !-----------------------------------------

    CALL BC_Slip_Wall(n_b, Vp, fb_n)

#ifdef NAVIER_STOKES
    IF(Turbulent) THEN
       CALL Turb_sym_plane_bc(n_b, Vp, fb_n)
    ENDIF
#endif

  END SUBROUTINE BC_Sym_Plane
  !==========================

#ifdef NAVIER_STOKES
  !================================================
  SUBROUTINE BC_Adiabatic_Wall(n_b, Vp, D_Vc, fb_n)
    !================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:), INTENT(IN)  :: D_Vc
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    !-----------------------------------------

    REAL, DIMENSION(SIZE(n_b)) :: q
    !-----------------------------------------

    q = Thermal_flux(Vp, D_Vc)

    fb_n = 0.d0
    fb_n(i_ren_ua) = DOT_PRODUCT(q, n_b)

  END SUBROUTINE BC_Adiabatic_Wall
  !===============================
#endif

  !======================================================
  SUBROUTINE Steger_Warming_Flux(n_b, Vp_i, Vp_out, fb_n)
    !======================================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_out
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    !-------------------------------------------

    REAL, DIMENSION(SIZE(fb_n)) :: alpha
    REAL, DIMENSION(SIZE(fb_n)) :: lambda
    REAL, DIMENSION(SIZE(fb_n)) :: Vc_i, Vc_out
    REAL, DIMENSION(SIZE(fb_n), SIZE(fb_n)) :: RR, LL, Neg 

    INTEGER :: j
    !---------------------------------------------

    CALL Advection_eigenvalues(Vp_i, n_b, lambda)
    CALL Advection_eigenvectors(Vp_i, n_b, RR, LL)

    CALL Prim_to_Cons(Vp_i,   Vc_i)
    CALL Prim_to_Cons(Vp_out, Vc_out)

    alpha = MATMUL(LL, Vc_out - Vc_i)

    Neg = 0.0
    DO j = 1, SIZE(lambda)
       Neg(j,j) = MIN(0.0, lambda(j))
    ENDDO

    fb_n = MATMUL(RR, MATMUL(Neg, alpha))

  END SUBROUTINE Steger_Warming_Flux
  !=================================

  !>>>  Entrée Turbine <<<  Code 91
  !==============================================
  SUBROUTINE BC_entree_turbine(n_b, Vp_i, fb_n)
    !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    !----------------------------------------

    REAL, DIMENSION(SIZE(Vp_i)) :: Vp_b 

    REAL :: rhoi, uveli, vveli, ei, ppi, ai, tempi, nor_veli, &
         tan_veli_x, tan_veli_y 
    REAL :: nx, ny
    REAL :: P_oo, rho_oo, T_oo, e_oo, ht_oo, s_oo, alpha
    INTEGER :: icont, i

    REAL :: usdn, usdn2, am0, t_tent, t_tent1, ro_tent, &
         err, t_c, rhoc, p_c, ac, nor_velc, uvelc, vvelc
    !----------------------------------------

    REAL, DIMENSION(DATA%NVar) :: lambda
    REAL, DIMENSION(DATA%NVar) :: u_d, v_d, u_c, v_c, u_b
    REAL, DIMENSION(DATA%NVar, DATA%Nvar) :: RR, LL

    REAL, DIMENSION(DATA%Nvar, DATA%NDim) :: ff_b, ff_d, ff_i

    REAL, DIMENSION(DATA%NVarPhy) :: Vp_c

    INTEGER :: N_dim, j, id
    !----------------------------------------


    !==================
    !  Initialization
    !==================
    !Vectors - Fluxes = 0
    Vp_b    = 0d0

    !Get Vp_i and n_b
    rhoi    = Vp_i(i_rho_vp)
    uveli   = Vp_i(i_vi1_vp)
    vveli   = Vp_i(i_vid_vp)
    ei      = Vp_i(i_eps_vp)
    ppi     = Vp_i(i_pre_vp)
    ai      = Vp_i(i_son_vp)
    tempi   = Vp_i(i_tem_vp)
    nx      = n_b(1)
    ny      = n_b(2)

    !Get Boundary data
    P_oo    = DATA%P_oo_turb
    rho_oo  = DATA%rho_oo_turb
    alpha   = DATA%alpha_oo_turb
    T_oo    = T__rho_P(rho_oo, P_oo)
    s_oo    = s__rho_T(rho_oo, T_oo)  
    e_oo    = e__rho_T(rho_oo, T_oo)
    ht_oo   = e_oo + P_oo/rho_oo


    !==================
    !  From Vp_i to Vp_b
    !==================
    !Calculation of Vp_b from Vp_i
    usdn  = 1.d0/(dcos(alpha)*nx + dsin(alpha)*ny)
    usdn2 = usdn**2.

    nor_veli   = uveli * nx + vveli * ny
    tan_veli_x = uveli - nor_veli * nx
    tan_veli_y = vveli - nor_veli * ny

    am0        = ppi + rhoi*ai*nor_veli
    t_tent     = T_oo
    ro_tent    = rho_oo

    !BOUCLE
    DO i = 1, 50

       t_tent1 = t_tent
       ro_tent = rho__s_T(s_oo, t_tent1)
       ac      = c__rho_T(ro_tent, t_tent1)
       am0     = ppi + ro_tent*ac*nor_veli
       t_tent  = EOS_TEICALC(ht_oo,ro_tent,(usdn/(ro_tent*ac))**2.,am0) 

       err = ABS((t_tent1 - t_tent)/t_tent1)

       IF (err <= 1.0E-6) THEN

          icont = 1
          t_c   = t_tent
          rhoc  = rho__s_T(s_oo, t_tent)
          p_c   = P__rho_T(rhoc, t_c)
          ac    = c__rho_T(rhoc, t_c)

          nor_velc = (am0 - p_c)/(rhoc*ac)

          EXIT
       ENDIF

    END DO

    IF (icont/=1.) THEN

       WRITE(*,*) "La condition limite INLET TURBINE SUBSONIQUE n'a pas converge sur 50 iterations (erreur < 1e-5 "
       WRITE(*,*)  "subroutine BC_entree_turbine"
       WRITE(*,*) "ERREUR = ", err
       WRITE(*,*) "rho = ", ro_tent
       WRITE(*,*) "T   = ", t_tent

       STOP

       rhoc     = ro_tent
       t_c      = t_tent
       p_c = P__rho_T(rhoc, t_c)
       ac = c__rho_T(rhoc, t_c)
       nor_velc = (am0 - p_c)/(rhoc*ac)

    END IF

    uvelc = (nor_velc*usdn)*COS(alpha)
    vvelc = (nor_velc*usdn)*SIN(alpha)

    !Getting Vp_b
    Vp_b(i_pre_vp) = p_c
    Vp_b(i_rho_vp) = rhoc
    Vp_b(i_tem_vp) = t_c
    Vp_b(i_eps_vp) = e__rho_T(rhoc, t_c)
    Vp_b(i_son_vp) = ac
    Vp_b(i_vi1_vp) = uvelc
    Vp_b(i_vid_vp) = vvelc

    CALL Steger_Warming_Flux(n_b, Vp_i, Vp_b, fb_n)

    !-------------------------------------------

!!$    N_dim = Data%NDim
!!$
!!$    CALL Advection_eigenvalues( Vp_i, n_b, lambda)
!!$    CALL Advection_eigenvectors(Vp_i, n_b, RR, LL)
!!$
!!$    ! Boundary INTERNAL state
!!$    CALL Prim_to_Cons(Vp_i, u_d);   v_d = MATMUL(LL, u_d)
!!$
!!$    ! Collected state
!!$    Vp_c = Vp_b
!!$
!!$    CALL Prim_to_Cons(Vp_c, u_c);   v_c = MATMUL(LL, u_c)
!!$
!!$    ! Boundary EXTERNAL STATE
!!$    DO j = 1, SIZE(lambda)
!!$
!!$       IF( lambda(j) < 0.d0 ) THEN
!!$          v_d(j) = 0.d0 ! NN
!!$       ELSE
!!$          v_c(j) = 0.d0 ! SN
!!$       ENDIF
!!$
!!$    ENDDO
!!$      
!!$    u_b = MATMUL( RR, v_c + v_d )
!!$
!!$    ! Flux correction
!!$    CALL Cons_to_Prim(u_b, Vp_b)
!!$
!!$    CALL advection_flux(N_dim, Vp_b, ff_b ) 
!!$    CALL advection_flux(N_dim, Vp_i, ff_d ) 
!!$
!!$    fb_n = 0.d0
!!$    DO id = 1, N_dim
!!$       fb_n =  fb_n + (ff_b(:, id) - ff_d(:, id)) * n_b(id)
!!$    ENDDO

    !-------------------------------------------

!!$    !==================
!!$    !  From Vp_b to flux and Jacobian
!!$    !==================
!!$    !Calculation of ff_i = EulerFlux(Vp_i)
!!$    CALL advection_flux(Data%Ndim, Vp_i, ff_i)
!!$
!!$    !Calculation of ff_b = EulerFlux(Vp_b)
!!$    CALL advection_flux(Data%Ndim, Vp_b, ff_b)
!!$
!!$    !Calculation of fb_n = (ff_b - ff_i).n_b
!!$    fb_n = MATMUL((ff_b - ff_i), n_b)

  END SUBROUTINE BC_entree_turbine
  !==============================

  !>>>  Sortie Turbine <<<  Code 92
  !==============================================
  SUBROUTINE BC_sortie_turbine(n_b, Vp_i, fb_n)
    !==============================================

    IMPLICIT NONE

    REAL, DIMENSION(:),   INTENT(IN)  :: n_b
    REAL, DIMENSION(:),   INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:),   INTENT(OUT) :: fb_n
    !----------------------------------------


    REAL, DIMENSION(DATA%NVar) :: alpha
    REAL, DIMENSION(DATA%NVar) :: lambda
    REAL, DIMENSION(DATA%NVar) :: u_d, v_d, u_c, v_c, u_b
    REAL, DIMENSION(DATA%NVar, DATA%Nvar) :: RR, LL

    REAL, DIMENSION(DATA%Nvar, DATA%NDim) :: ff_b, ff_d, ff_i

    REAL, DIMENSION(DATA%NVarPhy) :: Vp_c, Vp_b

    INTEGER :: id, j, N_dim
    !----------------------------------------

    !    REAL, DIMENSION(SIZE(Vp_i)) :: Vp_b 

    REAL :: rhoi, uveli, vveli, ei, ppi, ai, tempi, si, nx, ny
    REAL :: P_e, r_turb, Pc, rhoc, Tc, T_tent, err

    INTEGER  :: icont, i
    !----------------------------------------

    !==================
    !  Initialization
    !==================
    !Vectors - fluxes = 0
    Vp_b    = 0.d0

    !Get Vp_i and n_b
    rhoi    = Vp_i(i_rho_vp)
    uveli   = Vp_i(i_vi1_vp)
    vveli   = Vp_i(i_vid_vp)
    ei      = Vp_i(i_eps_vp)
    ppi     = Vp_i(i_pre_vp)
    ai      = Vp_i(i_son_vp)
    tempi   = Vp_i(i_tem_vp)
    si      = s__rho_T(rhoi, tempi)
    nx      = n_b(1)
    ny      = n_b(2)

    !Get Boundary data
    r_turb  = DATA%r_turb
    P_e     = DATA%P_oo_turb
    Pc      = P_e / r_turb 

    !==================
    !  From Vp_i to Vp_b
    !==================
    !Calculation of Vp_b from Vp_i
    icont   = 0
    T_tent  = 1.01d0

    DO i = 1, 40

       Tc     = T_tent
       rhoc   = rho__P_T(Pc, Tc)
       T_tent = T__rho_s(rhoc, si)

       err    = ABS((T_tent - Tc)/T_tent)

       IF (err .LT. 1.0E-10 )  THEN

          icont = 1
          Tc   = T_tent
          EXIT
       END IF

    END DO

    !No convergence CASE
    IF (icont/=1.) THEN

       WRITE(*,*) "La condition limite OUTLET TURBINE SUBSUNIQUE n'a pas converge en 40 iterations (erreur < 1e-4)"
       WRITE(*,*)  "subroutine BC_sortie_turbine" 
       WRITE(*,*) "ERREUR = ", err
       WRITE(*,*) "rho = ", rhoc
       WRITE(*,*) "T   = ", T_tent
       STOP

    END IF

    !Getting Vp_b
    Vp_b(i_pre_vp) = Pc                   !Pressure imposed
    Vp_b(i_vi1_vp) = uveli                 !Velocity from inside
    Vp_b(i_vid_vp) = vveli
    Vp_b(i_tem_vp) = Tc                    !Tc   = T(sc, Pc)
    Vp_b(i_rho_vp) = rho__P_T(Pc, Tc)      !rhoc = rho(sc, Tc) 
    Vp_b(i_eps_vp) = e__rho_T(rhoc, Tc)
    Vp_b(i_son_vp) = c__rho_T(rhoc, Tc)

    !-------------------------------------------
!!$    ! density extrapolation
!!$    Vp_b = Vp_P(Vp_i, Pc)
    CALL Steger_Warming_Flux(n_b, Vp_i, Vp_b, fb_n)

!!$    N_dim = Data%NDim
!!$
!!$    CALL Advection_eigenvalues( Vp_i, n_b, lambda)
!!$    CALL Advection_eigenvectors(Vp_i, n_b, RR, LL)
!!$
!!$    ! Boundary INTERNAL state
!!$    CALL Prim_to_Cons(Vp_i, u_d);   v_d = MATMUL(LL, u_d)
!!$
!!$    ! Collected state
!!$!!    Vp_c(i_rho_vp)          = Vp_i(i_rho_vp)
!!$!!    Vp_c(i_vi1_vp:i_vid_vp) = Vp_i(i_vi1_vp:i_vid_vp)
!!$!!    Vp_c(i_pre_vp)          = Data%P_oo_turb / Data%r_turb 
!!$!!
!!$!!    Vp_c(i_tem_vp) = T__rho_P( Vp_c(i_rho_vp), &
!!$!!                               Vp_c(i_pre_vp)  )
!!$!!
!!$!!    Vp_c(i_eps_vp) = e__rho_T( Vp_c(i_rho_vp), &
!!$!!                               Vp_c(i_tem_vp)  )
!!$!!
!!$!!    Vp_c(i_son_vp) = c__rho_T( Vp_c(i_rho_vp), &
!!$!!                               Vp_c(i_tem_vp)  )
!!$
!!$    Vp_c = Vp_b
!!$
!!$    CALL Prim_to_Cons(Vp_c, u_c);   v_c = MATMUL(LL, u_c)
!!$
!!$    ! Boundary EXTERNAL STATE
!!$    DO j = 1, SIZE(lambda)
!!$
!!$       IF( lambda(j) < 0.d0 ) THEN
!!$          v_d(j) = 0.d0 ! NN
!!$       ELSE
!!$          v_c(j) = 0.d0 ! SN
!!$       ENDIF
!!$
!!$    ENDDO
!!$      
!!$    u_b = MATMUL( RR, v_c + v_d )
!!$
!!$    ! Flux correction
!!$    CALL Cons_to_Prim(u_b, Vp_b)
!!$
!!$    CALL advection_flux(N_dim, Vp_b, ff_b ) 
!!$    CALL advection_flux(N_dim, Vp_i, ff_d ) 
!!$
!!$    fb_n = 0.d0
!!$    DO id = 1, N_dim
!!$       fb_n =  fb_n + (ff_b(:, id) - ff_d(:, id)) * n_b(id)
!!$    ENDDO
!!$
!!$!!  CALL Steger_Warming_Flux(n_b, Vp_i, Vp_b, fb_n)
    !-------------------------------------------

!!$    !Calculation of ff_i = EulerFlux(Vp_i)
!!$    CALL advection_flux(Data%Ndim, Vp_i, ff_i)
!!$
!!$    !Calculation of ff_b = EulerFlux(Vp_b)
!!$    CALL advection_flux(Data%Ndim, Vp_b, ff_b)
!!$
!!$    !Calculation of fb_n = (ff_b - ff_i).n_b
!!$    fb_n = MATMUL((ff_b - ff_i), n_b)

  END SUBROUTINE BC_sortie_turbine
  !==============================

  !=====================================================
  SUBROUTINE BC_subsonic_inflow_Pt(n_b, V_t, Vp_i, fb_n)
    !=====================================================

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN)  :: n_b
    REAL, DIMENSION(:), INTENT(IN)  :: V_t
    REAL, DIMENSION(:), INTENT(IN)  :: Vp_i
    REAL, DIMENSION(:), INTENT(OUT) :: fb_n
    !---------------------------------------

    REAL, DIMENSION(SIZE(Vp_i)) :: Vp_b

    REAL, DIMENSION(SIZE(n_b)) :: vel, flow_dir

    REAL :: P_t, T_t, H_t
    REAL :: aa, bb, cc, dd, vel_mag, Mach2, Cp
    REAL :: g_1, g, R_p, c2_tot, tem, alpha
    REAL :: rho, vel2, ene, pre, c2
    !----------------------------------------

    ! Total Pressure
    P_t = V_t(i_pre_vp)

    ! Total Temperature
    T_t = V_t(i_tem_vp)

    flow_dir = V_t(i_vi1_vp:i_vid_vp)

    g_1 = EOSKappa()
    g   = g_1 + 1.d0

    Cp = 1.d0/g_1

    ! Total enthalpy
    H_t = Cp*T_t

    ! Solution at the bounday
    rho = Vp_i(i_rho_vp)
    vel = Vp_i(i_vi1_vp:i_vid_vp)
    vel2 = SQRT(SUM(vel**2))
    ene = Vp_i(i_eps_vp)
    pre = Vp_i(i_pre_vp)
    c2 = Vp_i(i_son_vp)**2

    ! Riemann invariant
    R_p = DOT_PRODUCT(vel, n_b) + 2.0*SQRT(c2)/g_1

    ! Total speed of sound 
    c2_tot = g_1*(H_t - (ene + Pre/rho)) + c2

    alpha = DOT_PRODUCT(n_b, flow_dir)

    ! Coefficients in the quadratic equation for the velocity
    aa = 1.0 + 0.5*g_1*alpha*alpha
    bb = -1.0*g_1*alpha*R_p
    cc = 0.5*g_1*R_p*R_p - 2.0*c2_tot/g_1

    ! Solve quadratic equation for velocity magnitude
    ! Value must be positive
    dd = bb*bb - 4.0*aa*cc
    dd = SQRT(MAX(0.0,dd))
    Vel_Mag   = (-bb + dd)/(2.0*aa)
    Vel_Mag   = MAX(0.0,Vel_Mag)
    Vel2      = Vel_Mag*Vel_Mag

    ! Speed of sound from total speed of sound
    c2 = c2_tot - 0.5*g_1*Vel2

    ! Mach squared (cut between 0-1), use to adapt velocity
    Mach2 = Vel2/c2
    Mach2 = MIN(1.0,Mach2)
    Vel2  = Mach2*c2
    Vel_Mag = SQRT(Vel2)
    c2 = c2_tot - 0.5*g_1*Vel2

    ! New velocity vector at the inlet
    Vel = Vel_Mag*Flow_dir

    ! Static temperature from the speed of sound 
    Tem = c2 ! T = c2/(gamma * R)

    ! Static pressure from isentropic relation
    Pre = P_t * (Tem/T_t)**(g/g_1)

    rho = rho__P_T(Pre, Tem)
    ene  = e__rho_T(rho, Tem)

    ! Solution state at the boundary
    Vp_b = V_t ![turb]

    Vp_b(i_rho_vp) = rho
    Vp_b(i_vi1_vp:i_vid_vp) = vel
    Vp_b(i_eps_vp) = ene
    Vp_b(i_pre_vp) = Pre
    Vp_b(i_tem_vp) = Tem
    Vp_b(i_son_vp) = c__rho_T(rho, Tem)

    CALL Steger_Warming_Flux(n_b, Vp_i, Vp_b, fb_n)

  END SUBROUTINE BC_subsonic_inflow_Pt
  !===================================

#ifdef NAVIER_STOKES
  !=====================================================
  SUBROUTINE BC_Wall_Diff_flux_n(b_ele, k_idx, Vc, ff_n)
    !=====================================================

    IMPLICIT NONE

    TYPE(face_str),       INTENT(IN)  :: b_ele
    INTEGER,              INTENT(IN)  :: k_idx
    REAL, DIMENSION(:,:), INTENT(IN)  :: Vc
    REAL, DIMENSION(:),   INTENT(OUT) :: ff_n
    !-------------------------------------------

    REAL, DIMENSION(:),   ALLOCATABLE :: Vp_w
    REAL, DIMENSION(:),   ALLOCATABLE :: Vc_w
    REAL, DIMENSION(:,:), ALLOCATABLE :: G_Vc_w

    INTEGER, DIMENSION(:), ALLOCATABLE :: cn_ele

    REAL, DIMENSION(DATA%Ndim) :: D_U

    REAL :: S_vol

    INTEGER :: N_var, N_dim, b_NU, NU_e

    INTEGER :: n_e, je, jv, k_b, i, id
    !-------------------------------------------

    N_dim = DATA%Ndim
    N_var = DATA%NVar

    b_NU = b_ele%NU(k_idx)

    ALLOCATE( Vp_w(DATA%NvarPhy) )
    ALLOCATE( Vc_w(DATA%NVar) )

    !------------
    ! Wall state
    !-----------------------------
    CALL Cons_to_Prim(Vc(:, b_NU), Vp_w)

    Vp_w(i_vi1_vp:i_vid_vp) = DATA%Fr(b_ele%bc_type)%Vp(i_vi1_vp:i_vid_vp)

    CALL Prim_to_Cons(Vp_w, Vc_w)
    !-----------------------------

    n_e = SIZE( b_ele%b_e_con(k_idx)%cn_ele )

    ALLOCATE (cn_ele(n_e) )

    cn_ele = b_ele%b_e_con(k_idx)%cn_ele

    ALLOCATE( G_Vc_w(DATA%Ndim, DATA%NVar) )

    G_Vc_w = 0.d0
    S_vol = 0.d0

    ! Loop on all the elements which 
    ! share the boundary node b_NU
    DO je = 1, n_e

       ! Find the element's node which corresponds 
       ! to the boundary's ndoe
       DO k_b = 1, d_element(cn_ele(je))%e%N_points

          IF(d_element(cn_ele(je))%e%NU(k_b) == b_NU) EXIT

       ENDDO

       ! Reconstructed gradients of the conservative
       ! variables on the boundary. The wall state is
       ! forced for the boundary node.
       DO jv = 1, N_var

          D_U = 0.d0

          DO id = 1, N_dim

             DO i = 1, d_element(cn_ele(je))%e%N_points

                NU_e = d_element(cn_ele(je))%e%NU(i)

                IF(i /= k_b) THEN

                   D_U(id) = D_U(id) + Vc(jv, NU_e) * &
                        d_element(cn_ele(je))%e%D_phi_k(id, i, k_b)

                ELSE

                   ! Wall node
                   D_U(id) =  D_U(id) + Vc_w(jv) * &
                        d_element(cn_ele(je))%e%D_phi_k(id, i, k_b)

                ENDIF

             ENDDO ! loop element dofs -> i

          ENDDO ! loop n_dim -> id

          G_Vc_w(:, jv) = G_Vc_w(:, jv) + &
               D_U * d_element(cn_ele(je))%e%Volume

       ENDDO ! loop variables -> jv

       S_vol = S_vol + d_element(cn_ele(je))%e%Volume

    ENDDO ! loop elements -> je

    G_Vc_w = G_Vc_w / S_vol

    CALL Diffusion_flux_n(N_dim, Vp_w, G_Vc_w, &
         b_ele%n_b(:, k_idx), ff_n)

    !adiabatic wall
    ff_n(i_ren_ua) = 0.d0

    DEALLOCATE( Vp_w, Vc_w, cn_ele, G_Vc_w )

  END SUBROUTINE BC_Wall_Diff_flux_n
  !=================================

#endif

  !//////////////////////////////////////////////////////////////////////////
  !//                                                                      //
  !//                    PERIODIC BOUNDARY CONDITIONS                      //
  !//                            (explicit)                                //
  !//                                                                      //
  !//////////////////////////////////////////////////////////////////////////

  !==============================
  SUBROUTINE exp_Periodic_BC(Var)
    !==============================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    REAL, DIMENSION(DATA%NVar) :: Res_k, Res_i

    INTEGER :: jb, NU_i, NU_k
    !--------------------------------------

    IF( .NOT. ALLOCATED(per_conn) ) RETURN

    DO jb = 1, SIZE(per_conn, 1)

       NU_i = per_conn(jb, 1)
       NU_k = per_conn(jb, 2)

       Res_i = Var%Flux(:, NU_i)
       Res_k = Var%Flux(:, NU_k)

       Var%Flux(:, NU_i) = Var%Flux(:, NU_i) + Res_k
       Var%Flux(:, NU_k) = Var%Flux(:, NU_k) + Res_i !xxx

       !Var%Flux(:, NU_k) = 0.d0          !xxx
       !Var%Ua(:, NU_k) = Var%Ua(:, NU_i) !xxx
       !Var%Vp(:, NU_k) = Var%Vp(:, NU_i) !xxx

    ENDDO

  END SUBROUTINE exp_Periodic_BC
  !=============================

END MODULE Contrib_Boundary

