MODULE GMRES_LUSGS

  USE LesTypes
  USE LibComm
  USE ModelInterfaces
  USE LinAlg
  
  IMPLICIT NONE

  INTEGER, PARAMETER :: ite_max = 120
  
CONTAINS

  !====================================
  SUBROUTINE GMRES_solver(Com, Var, Pc)
  !====================================

    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Pc
    !--------------------------------------

    INTEGER :: NCells, Nvar, NvarP, NCellssnd, NCellsIn
    
    INTEGER :: m, km
    INTEGER :: j, i, is, ite, irelax, pc_type

    REAL :: hij, hjj, hbj, cosx, sinx
    REAL :: resid, resid_0, rel_toll
    
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vj, Du, Avj, z
    REAL, DIMENSION(:,:,:), ALLOCATABLE :: v
    REAL, DIMENSION(:,:),   ALLOCATABLE :: H
    REAL, DIMENSION(:),     ALLOCATABLE :: Hb
    REAL, DIMENSION(:),     ALLOCATABLE :: Pcos, Psin
    REAL, DIMENSION(:),     ALLOCATABLE :: y    
    !----------------------------------------------------

    m        = Data%MKrylov
    rel_toll = Data%Resdlin

    NVar   = SIZE(Var%Ua, 1)
    NCells = SIZE(Var%Ua, 2)
    NVarP  = SIZE(Var%Vp, 1)
    
    NCellsIn  = Var%NCellsIn
    NCellssnd = Var%NCellssnd

    ALLOCATE(  Du(Nvar, NCells) )
    ALLOCATE(  Vj(Nvar, NCells) )
    ALLOCATE(   z(Nvar, NCells) )
    ALLOCATE( AVj(Nvar, NCells) )
    ALLOCATE( V(Nvar, NCells, m+1) )

    ALLOCATE( h(m+1, m), Hb(m+1) )

    ALLOCATE( Pcos(m), Psin(m), y(m) )
    
    !******************************************************!
    !             START GMRES ALGORITHM                    !
    !******************************************************! 

#ifndef SEQUENTIEL
    CALL EchangeSol(Com, Var%Flux)
#endif

    ! Initial Guess: DU_0 = 0
    Du =  0.0
    Vj = -Var%Flux
    
    resid = Norme2(Vj, Com, NCellsIn)

    v(:, :, 1) = Vj / resid

    Hb(1) = resid
    Hb(2) = 0.0

    resid_0 = resid;   ite = 0
    
    DO ! Outer Loop
              
       DO j = 1, m ! Inner Loop

          ite = ite + 1

          Vj(:,:) = v(:,:,j)

          ! z <= Pc^-1 * Vj
          CALL PC_LUSGS(Com, Pc, Vj,  z)

          ! AVj <= Jac*z
          CALL MatrxDotVecteur(Pc, z, Com, AVj)

          !----------------
          ! Arnoldi Process
          !----------------------------------------------------
          DO i = 1, j

             Vj = v(:,:,i)

             h(i, j) = 0.0
             DO is = 1, NCellsIn
                h(i, j) = h(i, j) + SUM(AVj(:, is) * Vj(:, is))
             END DO

          END DO

#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_sum, h(1:j, j))
#endif

          ! Krylov vector          
          Vj = AVj
          DO i = 1, j
             Vj = Vj - h(i, j) * v(:, :, i)
          END DO

          h(j+1, j) = Norme2(Vj, Com, NCellsIn)

          resid = h(j+1, j)
         
          v(:, :, j+1) = Vj / h(j+1, j)
 
          ! Make the Hessenberg matrix upper triangular
          ! by means of Givens rotations
          DO  i = 1, j-1

             cosx = Pcos(i)
             sinx = Psin(i)

             hjj = h(i, j)
             hij = h(i+1, j)

             h(i,   j) =  cosx*hjj + sinx*hij
             h(i+1, j) = -sinx*hjj + cosx*hij

          END DO

          hjj = h(j,   j)
          hij = h(j+1, j)

          cosx =  hjj / SQRT(hjj**2 + hij**2)
          sinx =  hij / SQRT(hjj**2 + hij**2) 

          Pcos(j) = cosx
          Psin(j) = sinx

          h(j, j) = cosx*hjj + sinx*hij

          hbj = Hb(j)

          Hb(j)   =  cosx * Hb(j)
          Hb(j+1) = -sinx * hbj

          ! Residual
          resid = ABS(Hb(j+1)) ; !!write(*,*) ite, resid, '+'

          km = j

          IF (resid <= rel_toll*resid_0 .OR. ite >= ite_max) THEN
             IF( ite >= 5 ) EXIT
          ENDIF
          
       END DO

       y(km) = Hb(km) / h(km, km)

       DO  i = km-1, 1, -1

          hbj = Hb(i)
          DO  j = i + 1, km
             hbj  = hbj  - h(i, j) * y(j)
          END DO

          y(i) = hbj / h(i, i)

       END DO ! Inner Loop

       !---------------------
       ! Approximate solution
       !------------------------------------------------
       Vj = 0.0
 
#ifndef SEQUENTIEL

       DO  i = 1, km
          DO is = 1, NCellssnd
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO

       CALL EchangeSolPrepSend(Com, Vj)

#endif

       DO  i = 1, km
          DO is = NCellssnd+1, NCellsIn
             Vj(:, is) = Vj(:, is) + v(:, is, i) * y(i)
          END DO
       END DO

#ifndef SEQUENTIEL
       CALL EchangeSolRecv(Com, Vj)
#endif

       ! z <= Pc^-1 * Vj
       CALL PC_LUSGS(Com, Pc, Vj,  z)

#ifndef SEQUENTIEL

       Du(:, 1:NCellssnd) = Du(:, 1:NCellssnd) + z(:, 1:NCellssnd)

       CALL EchangeSolPrepSend(Com, Du)

       Du(:, NCellssnd+1:NCellsIn) = Du(:, NCellssnd+1:NCellsIn) + &
                                   &  z(:, NCellssnd+1:NCellsIn)

       CALL EchangeSolRecv(Com, Du)

#else

       Du = Du + z

#endif
      
       ! Vj <= Jac*DU
       CALL MatrxDotVecteur(Pc, Du, Com, Vj)

       Vj = -Var%Flux - Vj

       resid = Norme2(Vj, Com, NCellsIn) ; !!write(*,*) ite, resid, '*'

       IF (resid < rel_toll*resid_0 .OR. ite >= ite_max) THEN
          IF( ite >= 5 ) EXIT
       END IF
       
       v(:, :, 1) = Vj / resid

       Hb(1) = resid
       Hb(2) = 0.0

    ENDDO ! Outer Loop
    
    !******************************************************!
    !               END GMRES ALGORITHM                    !
    !******************************************************!

    IF(Com%Me == 0) THEN
       WRITE(*, 118) ite, resid_0, resid
    ENDIF

#ifndef SEQUENTIEL
    CALL EchangeSolPrepSend(Com, Du)
#endif

    ! Solution increment
    Var%Flux(:, 1:NCellsIn) = Du(:, 1:NCellsIn)

#ifndef SEQUENTIEL
    CALL EchangeSolRecv(Com, Var%Flux)
#endif
           
    DEALLOCATE(Du, Vj, AVj, v, h, Hb, Pcos, Psin, y, z)

118 FORMAT(' + Lin ITE = ', I3, '    RES_0 = ', E12.5, '   RES = ', E12.5, '   +' )
 
  END SUBROUTINE GMRES_solver
  !==========================  

  !=================================
  SUBROUTINE PC_LUSGS(Com, Pc, v, z)
  !=================================

    IMPLICIT NONE

    TYPE(MeshCom),        INTENT(INOUT) :: Com
    TYPE(Matrice),        INTENT(IN)    :: Pc
    REAL, DIMENSION(:,:), INTENT(IN)    :: v

    REAL, DIMENSION(:,:), INTENT(OUT) :: z
    !------------------------------------------

    REAL, DIMENSION(:,:), ALLOCATABLE :: z_old

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: inv_D
    !----------------------------------------------

    REAL :: res

    INTEGER :: i, j, jm, k, i_e, i_s, N_var, N_dofs, N_In, l

    REAL,    PARAMETER :: in_toll = 1.0E-6
    INTEGER, PARAMETER :: k_max = 15
    INTEGER, PARAMETER :: k_min = 3
    !-----------------------------------------------

    N_Var  = SIZE(v, 1)
    N_dofs = SIZE(V, 2)
    N_In   = Pc%NNin

    ALLOCATE( inv_D(N_var, N_var, N_dofs) )

    ALLOCATE( z_old(N_var, N_dofs) )

    ! Store the inverse of the diagonal
    DO i = 1, Pc%NNin

       l = Pc%IDiag(i)
       
       inv_D(:,:, i) = inverse( Pc%Vals(:,:, l) )

    ENDDO

    z = 0.d0
    DO k = 1, k_max
       
       z_old = z

       !--------------
       ! FORWARD SWEEP
       !-----------------------------------------------
#ifndef SEQUENTIEL

       DO i = 1, Pc%NNsnd

          z(:, i) = -v(:, i)
          
          i_s = Pc%JPosi(i)
          i_e = Pc%JPosi(i+1) - 1

          DO jm = i_s, i_e

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

             z(:, i) = z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO

       CALL EchangeSolPrepSend(Com, z)

#endif

       DO i = Pc%NNsnd+1, Pc%NNin

          z(:, i) = -v(:, i)

          i_s = Pc%JPosi(i)
          i_e = Pc%JPosi(i+1) - 1

          DO jm = i_s, i_e

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

              z(:, i) = z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO
       !----------------------------------------------------

#ifndef SEQUENTIEL
    CALL EchangeSolRecv(Com, z)
#endif

       !----------------
       ! BACKWARD SWEEP
       !----------------------------------------------------
       DO i = Pc%NNin, 1, -1

          z(:, i) = -v(:, i)

          i_s = Pc%JPosi(i+1) - 1
          i_e = Pc%JPosi(i)

          DO jm = i_s, i_e, -1

             j = Pc%JvCell(jm)

             IF (j == i) CYCLE

              z(:, i) =  z(:, i) - MATMUL(Pc%Vals(:,:, jm), z(:, j))

          ENDDO

          z(:, i) = MATMUL( inv_D(:,:, i), z(:, i) )

       ENDDO
       !----------------------------------------------------
    
#ifndef SEQUENTIEL
    CALL EchangeSol(Com, z)
#endif

       res = Norme2( z-z_old, Com, N_dofs )
       
       IF( k >= k_min  ) THEN
          IF( res <= in_toll ) EXIT
       ENDIF

    ENDDO

#ifndef SEQUENTIEL
    CALL EchangeSolPrepSend(Com, z)
#endif

    DEALLOCATE( inv_D, z_old )

  END SUBROUTINE PC_LUSGS
  !======================
  
END MODULE GMRES_LUSGS
