MODULE BoucleEnTemps

  USE LesTypes
  USE EOSLaws
  USE LibComm
  USE Outputs
  USE ModelInterfaces
  USE Assemblage

  USE MF_method
  USE LUSGS_method
  USE GMRES_LUSGS

  USE PETSc_driver
  USE LinAlg

  IMPLICIT NONE

  REAL, DIMENSION(2) :: Residus0

  REAL :: Resid0_oo   

CONTAINS
 
  !===========================================
  SUBROUTINE boucle_en_temps(Com, Var, Mat_fb)
    !===========================================

#ifdef PETSC
#include "finclude/petscsys.h"
#include "finclude/petscmat.h"
#include "finclude/petscksp.h"
#include "finclude/petscpc.h"
#endif

    CHARACTER(LEN = *), PARAMETER :: mod_name = "Boucle_en_temps"
    TYPE(MeshCom)  , INTENT(INOUT)           :: Com
    TYPE(Variables), INTENT(INOUT)           :: Var
    TYPE(Matrice)  , INTENT(INOUT), OPTIONAL :: Mat_fb
    !--------------------------------------------------

    INTEGER :: Ns, j, ii, is

    REAL :: CFL,  res, res0

    REAL :: Temps_start, Temps_ecr1, Temps_ecr2, &
         &  Temps_xxx, Prev_test

    REAL :: time_loop_start, time_loop_end

    REAL :: Temps_elapsed1, Temps_elapsed2

    INTEGER :: nb_periodes_initial, nb_periodes_final, nb_periodes

    ! For RK methods
    REAL, DIMENSION(1: 4) :: a, b
    INTEGER :: ia, rk, inewt
    REAL, DIMENSION(: ), ALLOCATABLE :: Utmpp

#ifdef PETSC
    Mat :: A_imp
    KSP :: ksp
#endif


    !-----------------------------------------------------------
    !  Diverses Initialisations
    !-----------------------------------------------------------
    !
    Var%time_loop = 0.0

    Temps_ecr1 = 0.0
    Temps_ecr2 = 0.0

    Temps_elapsed1 = 0.0
    Temps_elapsed2 = 0.0

    Prev_test = 0.0

    !
    Ns       = Var%NCells
    Var%Dt   = DATA%Dtmax
    Var%kt   = DATA%kt0
    DATA%kt0 = DATA%kt0 + 1

    IF (DATA%Impre > 0) THEN
       PRINT *, " Data%Methode ", DATA%Methode, DATA%sel_methode
    END IF

    !-----------------------------------------------------------
    !-----------------------------------------------------------
    ! Allocation memoire
    !-----------------------------------------------------------
    !-----------------------------------------------------------
    !
    !
    SELECT CASE(DATA%sel_methode)

    CASE(cs_methode_imp_rk1_rd)

#ifdef PETSC
       CALL init_PETSc(Var%NCells, Mat_fb, A_imp, ksp)
#endif

    CASE(cs_methode_exp_rk1)
       ALLOCATE(Utmpp(DATA%Nvar))
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "utmpp", SIZE(Utmpp))
       END IF
       a(1) = 1.0
       rk = 1
    CASE(cs_methode_exp_rk2)
       rk = 2
       ALLOCATE(Utmpp(DATA%Nvar))
       a(1: 2) = (/ 0.5, 1.0 /)
       IF (see_alloc) THEN
          CALL alloc_allocate(mod_name, "utmpp", SIZE(Utmpp))
       END IF

    CASE(cs_methode_exp_rk_Mario_rd) ! Explicite RD ordre 2

       SELECT CASE( DATA%nordre)
       CASE(1)
          rk = 1
          a(1:2)=1.0
       CASE default
          rk = 2
          a(1:4) = 1.0
       END SELECT
       ALLOCATE(Utmpp(SIZE(Var%Ua,dim=1)))

    END SELECT

    IF (MINVAL(Var%Vp(i_rho_vp, : )) <= 0.0) THEN
       PRINT *,  " :: ERREUR : rho <= 0.0, initialisation invalide"
       CALL stop_run()
    END IF

    !-----------------------------------------------------------
    !-----------------------------------------------------------
    !           LA BOUCLE EN TEMPS
    !-----------------------------------------------------------
    !-----------------------------------------------------------
    !
    Var%avec_h = .FALSE.
    ! Strategie de censure des chocs
!!$    IF (Var%avec_h) THEN 
!!$       Var%h(1,:) = 0.0
!!$       Var%h(2,:) = 1.0 
!!$    END IF
       !-------------------------------------------------------------------
       ! Copy of solutions: this usefull only for the Mario Exp scheme
       ! For the others, this is useful to avoid crash in Contrib_RD, only
       !--------------------------------------------------------------------


       Var%Vp_0 = Var%Vp
       Var%ua_0 = Var%Ua

#ifndef SEQUENTIEL
       CALL EchangeSol( Com, Var%Vp_0 )
       CALL EchangeSol( Com, Var%Ua_0 )
#endif

    !#######################################################################
    !                              -- Boucle_en_temps : boucle principale
    !#######################################################################

    CALL AfficheResu(DATA, Var, Com)

    DO  !-- boucle principale, en temps

       CALL CPU_TIME(time_loop_start)


       !-----------------------------------------------------------
       !  Init. des compteurs / Incrementation pas de temps
       !-----------------------------------------------------------
       !
       Var%kt = Var%kt + 1

       CALL Model_check_variables(Var)

       !-----------------------------------------------------------
       !  Calcul du Second Membre
       !-----------------------------------------------------------
       !
       SELECT CASE(DATA%sel_methode)

       CASE(cs_methode_imp_rk1_rd, cs_methode_exp_rk1)

          Var%Flux  = 0.d0

       CASE(cs_methode_exp_rk_Mario_rd)

       CASE DEFAULT

          WRITE(*,*) 'ERROR: unknown time integration method'
          STOP

       END SELECT


     !-----------------------------------------------------------
     !  Calcul du Pas de Temps
     !-----------------------------------------------------------
     !



       CALL CalCFl(Var, CFL)
       !! May 12 2014
       !! modification
       !! In order to easily integrate the RK methods, I keep all what is done in
       !! the internal routines to evaluate the Var%dtloc variable: this totaly
       !! useless for unsteady, only Var%dt is needed (global time step
       !!
       SELECT CASE(DATA%sel_methode)

       CASE( cs_methode_exp_rk1,cs_methode_exp_rk_Mario_rd )
          CALL Model_timeStep(DATA, Var, CFL)
 
#ifndef SEQUENTIEL
          CALL Reduce(Com, cs_parall_min,Var%dt_phys)
#endif
! RA: 01/02/2015
! To understand what follows, see  unsteady time stepping.
! in a nutshell: to have the same routines for steady and unsteady 
! problems, I have a dV which is 0 when we do steady and u(n+1)-u(n) (basicaly)
! for unsteady. However, Var%dt_phys, which the argument that is passed for that
! instruction, is not defined for steady problems, so we'll set it to +infty
! hence it is better to pass 1/Var%dt_phys (=0) and multiply instead of dividing
! Var%dt is used to compute the time  in both cases, so we have to set it to 
! Var%dt_phys
          Var%dt     = Var%dt_phys
          Var%dt_phys= 1./Var%dt_phys
       CASE DEFAULT
          Var%dt_phys=0.0 ! this value to kill the dVc in the explicit residual, see above
          Var%Dtloc = 0.d0
       END SELECT


       !#endif

       !-----------------------------------------------------------
       !-----------------------------------------------------------
       !  Switch en fonction de la Methode
       !-----------------------------------------------------------
       !-----------------------------------------------------------
       !
       SELECT CASE(DATA%sel_methode)

       CASE (cs_methode_imp_rk1_rd)

          CALL imp_rk1_rd() !***

          !-----------------------------------------------------------
          !-----------------------------------------------------------
          !  Mise a jour de la Solution
          !-----------------------------------------------------------
          !-----------------------------------------------------------
          !
          !  Par convention, ici, VAR%FLUX CONTIENT Un-Ua
          !  et les overlaps sont deja nickels
          !
          CALL MODELIncrement(Var, DATA)
          !^^^^^^^^^^^^^
       CASE (cs_methode_exp_rk1)

          CALL Explicite_Type_RK1()


       CASE(cs_methode_exp_rk_Mario_rd)

          CALL Explicite_Type_RK_Mario_Rd()

          !! La solution est mise a jour dans  Explicite_Type_RK_Mario_Rd

       CASE DEFAULT

          WRITE(*,*) 'ERROR: unknown time integration method'
          STOP

       END SELECT




       Var%temps = Var%temps + Var%Dt !?



       !*********************
       ! Fin de l'iteration *
       !*********************
       CALL CPU_TIME(time_loop_end)

       ! CPU time of the time loop
       Var%time_loop = Var%time_loop + &
            (time_loop_end - time_loop_start)

       !------------------------
       ! Ecritures des Resultats
       !--------------------------------------------------------
       IF (MOD(Var%kt, DATA%ifre) == 0) THEN
          CALL AfficheResu(DATA, Var, Com, CFL)
       END IF

       IF (MOD(Var%kt, DATA%ifres) == 0 ) THEN
          CALL My_timer(nb_periodes_initial)
          CALL CPU_TIME(Temps_start)

          CALL Ecriture(DATA, Var, Com)

          CALL CPU_TIME(Temps_xxx)
          CALL My_timer(nb_periodes_final)
          nb_periodes = nb_periodes_final - nb_periodes_initial
          temps_elapsed1 = temps_elapsed1 + REAL(nb_periodes) *0.001
          Temps_ecr1 = Temps_ecr1 + Temps_xxx - Temps_start
       END IF

       IF (Var%temps >= DATA%Tmax) THEN
          IF (Com%Me == 0) THEN
             PRINT *, " Temps de simulation depasse : Var%Temps >= Data%Tmax"
             PRINT*,  Var%temps,DATA%Tmax
          END IF
          EXIT
       END IF

       IF (Var%kt >= DATA%ktmax) THEN
          IF (Com%Me == 0) THEN
             PRINT *,  " Nombre maximal d'iterations depasse : Var%kt >= Data%ktmax"
          END IF
          EXIT
       END IF

       IF (MAXVAL(Var%Residus) <= DATA%resdsta) THEN
          IF (Com%Me == 0) THEN
             WRITE(*,*) 'Residual lower than the fixed tollerance', DATA%resdsta
          END IF
          EXIT
       END IF

    END DO !-- fin de la boucle principale en temps

    !#######################################################################
    !                       -- Boucle_en_temps : fin de la boucle principale
    !#######################################################################

    !-----------------------------------------------------------
    !-----------------------------------------------------------
    ! Fin de la Boucle en Temps
    !-----------------------------------------------------------
    !-----------------------------------------------------------

    CALL My_timer(nb_periodes_initial)
    CALL CPU_TIME(Temps_start)

    CALL AfficheResu(DATA, Var, Com)

    CALL EcritureFIn(DATA, Var, Com)


    CALL CPU_TIME(Temps_xxx)
    CALL My_Timer(nb_periodes_final)

    nb_periodes = nb_periodes_final - nb_periodes_initial
    temps_elapsed2 = temps_elapsed2 + REAL(nb_periodes) *0.001

    Temps_ecr2 = Temps_ecr2 + Temps_xxx - Temps_start

    WRITE(*,*)
    WRITE(*, '("   Time write-loop:              ", F10.4, "(s) on Proc " I4)') Temps_ecr1,     Com%Me
    WRITE(*, '("   Time write-loop (elapse):     ", F10.4, "(s) on Proc " I4)') Temps_elapsed1, Com%Me
    WRITE(*, '("   Time write-loop end:          ", F10.4, "(s) on Proc " I4)') Temps_ecr2,     Com%Me
    WRITE(*, '("   Time write-loop end (elapse): ", F10.4, "(s) on Proc " I4)') Temps_elapsed2, Com%Me
    WRITE(*,*)

    !******************************************************************************************
    !******************************************************************************************
    !******************************************************************************************
    !******************************************************************************************

  CONTAINS


    !-----------------------------------------------------------
    !-----------------------------------------------------------
    !!-- Contribution Implicite RK1
    !-----------------------------------------------------------
    !-----------------------------------------------------------
    !
    !======================
    SUBROUTINE imp_rk1_rd()
      !======================

      IF( DATA%sel_relax == cs_relax_nl_lusgs ) THEN

!!$         CALL Model_compute_RHS(Var, .TRUE.)

         CALL resetcoeftab(Mat_fb)

         CALL Model_compute_LHS_RHS(Com, Var, Mat_fb)

         CALL compute_residual(Com, Var)

#ifndef SEQUENTIEL

         CALL Reduce(Com, cs_parall_min, Var%Dt)
         CALL EchangeVec( Com, Var%DtLoc, DATA )
!         CALL EchangeVec( Com, Var%h(2,:), DATA )
#endif
         !------------------------------------
         CALL NonLin_BLU_SGS(Com, Var, Mat_fb)
         !------------------------------------

      ELSE
         CALL resetcoeftab(Mat_fb)

         CALL Model_compute_LHS_RHS(Com, Var, Mat_fb)

         CALL compute_residual(Com, Var)

#ifndef SEQUENTIEL

         CALL Reduce(Com, cs_parall_min, Var%Dt)
         CALL EchangeVec( Com, Var%DtLoc, DATA )
!         CALL EchangeVec( Com, Var%h(2,:), DATA )
#endif

#ifdef PETSC
         CALL solve_PETSc(Var, Mat_fb, A_imp, ksp)
#else

         IF( DATA%sel_relax == cs_relax_mf        .OR. & 
              DATA%sel_relax == cs_relax_mf_jacobi .OR. &
              DATA%sel_relax == cs_relax_mf_ilu    .OR. &
              DATA%sel_relax == cs_relax_mf_lusgs ) THEN

            !------------------------------
            CALL MF_GMRES(Com, Var, Mat_fb)
            !------------------------------

         ELSEIF( DATA%sel_relax == cs_relax_blusgs ) THEN

            !-----------------------------
            CALL BLU_SGS(Com, Var, Mat_fb)
            !-----------------------------

         ELSEIF( DATA%sel_relax == cs_relax_glusgs ) THEN

            !----------------------------------
            CALL GMRES_solver(Com, Var, Mat_fb)
            !----------------------------------

         ELSE

            CALL SolveSystem(Com, DATA, Var, Mat_fb)

         ENDIF

#endif

      ENDIF

    END SUBROUTINE imp_rk1_rd
    !========================
    SUBROUTINE Explicite_Type_RK1()

      CHARACTER(LEN = *), PARAMETER :: mod_name = "Explicite_Type_RK1"
      INTEGER :: ia, is
      ! ICI on a deja F0 et  dt

      CALL Model_compute_RHS(Com,  Var)

#ifndef SEQUENTIEL

      CALL EchangeSolRecv( Com, Var%flux)
      CALL Reduce(Com, cs_parall_min, Var%Dt)

#endif


      DO is = 1, Var%NCells
         Utmpp = Var%Ua(:, is) + Var%dt * Var%flux(:, is) 
         Var%Ua(:,is) = Utmpp
         CALL ModelU2VpPt(Utmpp, Var%Vp(:, is))
      END DO
#ifdef VERIFIER
      CALL tester_nan2(Var%vp, mod_name // " Var%vp en interne/A")
#endif






      !Var%Flux = Var%Flux*a(RK) ... car a(RK) == 1.0 !!!!!!
      ! Ici on sort avec l'incr�ment total
    END SUBROUTINE Explicite_Type_RK1
    !==============================================================
    SUBROUTINE Explicite_Type_RK_Mario_Rd()
      !
      ! Version explicite d'ordre 2 Par Mario Ricchiuto, RR6998, juillet 2009
      ! Code par R. Abgrall
      !
      CHARACTER(LEN = *), PARAMETER :: mod_name = "Explicite_Type_RK_Mario_Rd"
      INTEGER :: ia, is, jf, N_i, N_p
      REAL    :: dt

      !      Var%h(2,:)=0.0  !Ici, on dit qu'on n'a pas confiance dans
      ! la stabilisation au 1er
      ! sous pas de RK dans cette methode. Dans RR6998, juillet 2009,
      ! on filtre par rapport
      ! au residu en cours de calcul.
      ! en // on n'a pas besoin d'echanger h car h vaut partout 1.


      DO is=1, Var%Ncells
         Var%Vp_0(:,is) = Var%Vp(:,is)!
         Var%Ua_0(:,is) = Var%Ua(:,is)
      ENDDO

!!$      IF (Var%avec_h) THEN
!!$         Var%h(1,:)=1.0
!!$         Var%h(2,:)=0.0
!!$      ENDIF

      DO ia = 1, rk


         DO is=1,SIZE(Var%Flux,2)
            Var%Flux(:,is) =0.0
         ENDDO

         CALL Model_compute_RHS(Com, Var)

#ifndef SEQUENTIEL
         CALL EchangeSol( Com, Var%flux ) !probablement plus les solutions
#endif

         !
         !Conditions de periodicite


!!$         IF (SIZE(Var%LogPer)>0) THEN
!!$
!!$
!!$            DO is=1, Var%Ncells
!!$               IF (Var%LogPer(is).NE.is) THEN
!!$                  Var%flux(:,Var%LogPer(is))=Var%Flux(:,Var%LogPer(is))&
!!$                       +Var%Flux(:,is)
!!$               ENDIF
!!$            ENDDO
!!$
!!$            DO is=1, Var%Ncells
!!$               IF (Var%LogPer(is).NE.is) THEN
!!$                  Var%Flux(:,is) = Var%Flux(:,Var%logPer(is))
!!$               ENDIF
!!$            ENDDO
!!$         ENDIF

         ! source --> u^n on n'y touche pas.
         ! ua --> u^n puis u^1
         ! ua_0--> u^1 puis u^{n+1}
         !         call tester_nan2(Var%flux,"Var%flux")



         IF (Var%dt_phys.LE.0) THEN
            PRINT*, mod_name, "L=587"
            PRINT*, "Var%dt_phys is not correct", Var%dt_phys
         ELSE
            dt=1./Var%dt_phys
         ENDIF

         DO is = 1, Var%NCells


            Utmpp = Var%Ua(:, is) - Var%flux(:, is) * dt *a(ia)/Var%cellVol(is)

            Var%Ua(:,is)=Utmpp

            CALL ModelU2VpPt(Utmpp, Var%Vp(:, is))


         END DO

         !

         !Conditions de periodicite
!!$         IF (SIZE(Var%LogPer)>0) THEN
!!$            DO is=1, Var%Ncells
!!$
!!$               IF (Var%LogPer(is).NE.is) THEN
!!$                  Var%Vp(:,is) = Var%Vp(:,Var%LogPer(is))
!!$                  Var%Ua(:,is) = Var%Ua(:,Var%LogPer(is))
!!$               ENDIF
!!$            ENDDO
!!$         ENDIF
!!$         IF (Var%avec_h) THEN
!!$            Var%h(2,:) = Var%h(1,:)
!!$            Var%h(1,:) = 1.0
!!$         ENDIF

#ifndef SEQUENTIEL
         CALL EchangeSol( Com, Var%Ua ) 
         CALL EchangeSol( Com, Var%Vp ) 
!!$         IF (Var%avec_h) THEN
!!$            CALL EchangeSol( Com, Var%h ) 
!!$         ENDIF
#endif

      END DO

    END SUBROUTINE Explicite_Type_RK_Mario_Rd
    !==============================================
  END SUBROUTINE Boucle_en_temps
    
  !==========================
  SUBROUTINE CalCFl(Var, CFL)
  !==========================

    IMPLICIT NONE

    TYPE(Variables), INTENT(INOUT) :: Var
    REAL,            INTENT(INOUT) :: CFL
    !-------------------------------------

    REAL :: x_L2, x_Loo, x_res

    REAL, PARAMETER :: a_res   = 0.9d0
    REAL, PARAMETER :: CFL_0   = 1.0d0
    REAL, PARAMETER :: CFL_exp = 0.9d0
    !-------------------------------------

    SELECT CASE(DATA%LoiCfl)

    CASE(cs_loicfl_clfmax) !-- code 0

          CFL = MIN(5.0 * (1.5**Var%Kt), DATA%CflMax) 

    CASE(cs_loicfl_minmult) !-- code 2, pente

       IF(var%kt <= 1) THEN

          CFL = CFL_0

       ELSE

          IF (Var%Resid_2 > Var%Resid_1) THEN

             ! Favorable convergence  
             !CFL = MIN( 2.d0*Var%CFL, Var%CFL*(Var%Resid_2/Var%Resid_1) ) 
             CFL = MIN( 1.25d0*Var%CFL, Var%CFL*(Var%Resid_2/Var%Resid_1) )

          ELSE

             ! Unfavorable convergence 
             !CFL = MAX(Var%CFL*(Var%Resid_2/Var%Resid_1), 0.1d0*Var%CFL)
             CFL = MAX(Var%CFL*(Var%Resid_2/Var%Resid_1), 0.5d0*Var%CFL)

          ENDIF

!          IF( Var%Residus(i_ren_ua) > 1.0E-4 ) THEN
             CFL = MIN(CFL, DATA%CFLmax)
!          ELSE
!             CFL = MIN(CFL, 10.0*Data%CFLmax)
!          ENDIF

       ENDIF

    CASE(cs_loicfl_rapport_res) !-- code 3, bourrinissime

       x_L2  = MAXVAL(Var%Residus)
       x_Loo = MAXVAL(Var%Res_oo )
       
       IF(x_Loo <= 1.d0) THEN
          x_res = MIN(x_L2, 10.d0)
       ELSE
          x_res = x_Loo
       ENDIF
       
       IF( x_res <= 1.d0 ) THEN
          CFL = CFL_0 / ( x_res**a_res )
       ELSE
          CFL = CFL_exp + (CFL_0 - CFL_exp) * &
                EXP( a_res*(CFL_0/(CFL_0 - CFL_exp)) * (1.d0 - x_res) )
       ENDIF

       CFL = MIN(CFL, DATA%CFLmax) 

    CASE DEFAULT

       CFL = DATA%CflMax

    END SELECT

    Var%CFL = CFL

  END SUBROUTINE CalCFL
  !====================

  !====================================
  SUBROUTINE compute_residual(Com, Var)
  !====================================

    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    REAL, DIMENSION(DATA%Nvar) :: ff
    REAL :: R_oo
    INTEGER :: is, k
    !--------------------------------------

    Var%Residus = 0.0; Var%Res_oo = 0.d0; R_oo = 0.d0

    IF (Var%kt > 1) THEN

       DO is = 1, Var%NCellsIn

          ff = ABS(Var%Flux(:, is))

          Var%Residus = Var%Residus + ff*ff

          DO k = 1, DATA%Nvar
             Var%Res_oo(k) = MAX(Var%Res_oo(k), ff(k))
          ENDDO

          R_oo = MAX(R_oo, ff(i_rho_vp))

       END DO

#ifndef SEQUENTIEL
       CALL Reduce(Com, cs_parall_sum, Var%Residus)
       DO k = 1, DATA%Nvar
          CALL Reduce(Com, cs_parall_max, Var%Res_oo(k))
       ENDDO
       CALL Reduce(Com, cs_parall_max, R_oo)
#endif

       Var%Residus = SQRT(Var%Residus) 

!R_oo = Var%Residus(1)!???

       ! Normalizated residual
       Var%Residus = Var%Residus / DATA%Residus0
       Var%Res_oo  = Var%Res_oo  !/ Data%Res_oo0

    ELSE  !-- var%kt <= 1        

       DO is = 1, Var%NCellsIn

          ff = ABS(Var%Flux(:, is))

          Var%Residus = Var%Residus + ff*ff

          DO k = 1, DATA%Nvar
             Var%Res_oo(k) = MAX(Var%Res_oo(k), ff(k))
          ENDDO

          R_oo = MAX(R_oo, ff(i_rho_ua))

       END DO

#ifndef SEQUENTIEL
       CALL Reduce(Com, cs_parall_sum, Var%Residus)
       DO k = 1, DATA%Nvar
          CALL Reduce(Com, cs_parall_max, Var%Res_oo(k))
       ENDDO
       CALL Reduce(Com, cs_parall_max, R_oo)
#endif

       Var%Residus = SQRT(Var%Residus) 

       ! Initial residual
       DATA%Residus0 = Var%Residus
       DATA%Res_oo0 =  Var%Res_oo

       Var%Residus = Var%Residus / DATA%Residus0
       Var%Res_oo  = Var%Res_oo  !/ Data%Res_oo0

    END IF !-- kt <= 1

    ! For the CFL strategy in the MF
    Var%Resid_2 = Var%Resid_1
    Var%Resid_1 = R_oo

  END SUBROUTINE compute_residual
  !==============================
 
END MODULE BoucleEnTemps
