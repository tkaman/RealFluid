MODULE Scheme_LDA

  USE LesTypes
 
  USE Advection
  USE Diffusion 
  USE Turbulence_models,   ONLY: Turbulent, N_eqn_turb
  USE Source

  USE PLinAlg
  USE Assemblage

  IMPLICIT NONE

CONTAINS

  !=========================================================
  SUBROUTINE ResiduLDA_imp(ele, Vp, Vc, CFL, Phi_tot, JJ_tot, &
                                      Phi_i, inv_dt, Mat)
  !=========================================================
    
    IMPLICIT NONE
    
    TYPE(element_str),                INTENT(IN)    :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
    REAL,                             INTENT(IN)    :: CFL
    REAL, DIMENSION(:),               INTENT(IN)    :: Phi_tot
    REAL, DIMENSION(:,:,:),           INTENT(IN)    :: JJ_tot
    REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
    REAL,                             INTENT(OUT)   :: inv_dt
    TYPE(Matrice),                    INTENT(INOUT) :: Mat
    !---------------------------------------------------------

    REAL, DIMENSION(:),   ALLOCATABLE :: lambda
    REAL, DIMENSION(:,:), ALLOCATABLE :: RR, LL, Pos
    REAL, DIMENSION(:,:), ALLOCATABLE :: N_m, NN

    REAL, DIMENSION(Data%NVar, Data%Nvar) :: M_ik, Diag_i

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: Beta

    REAL, DIMENSION(ele%N_dim) :: v_nn

    REAL, DIMENSION(SIZE(Vp,1)) :: Vp_b
    
    REAL :: mod_n, r_N_dofs, p_max, u_max, c_max, S_ele, C_Dt
   
    INTEGER :: N_dim, N_dofs, N_var, i, j, k
    !--------------------------------------------------

    N_dim  = Data%Ndim
    N_var  = Data%NVar

    N_dofs = ele%N_Points; r_N_dofs = REAL(N_dofs, 8)
    
    ALLOCATE( lambda(N_var) )

    ALLOCATE(  RR(N_var, N_var), &
               LL(N_var, N_var), &
              Pos(N_var, N_var) )

    ALLOCATE( N_m(N_var, N_var), &
               NN(N_var, N_var) )

    ALLOCATE( Beta(N_var, N_var, N_dofs) )

    ! Mean state
    Vp_b = Mean_State_Vp(Vp)

    inv_dt = 0.d0;  N_m = 0.d0

    DO i = 1, N_dofs

       mod_n = SQRT( SUM(ele%rd_n(:, i)**2) )

       v_nn = ele%rd_n(:, i) / mod_n
       
       CALL Advection_eigenvalues ( Vp_b, v_nn, lambda )
       CALL Advection_eigenvectors( Vp_b, v_nn, RR, LL )

       Pos = 0.d0; p_max = 0.d0

       DO j = 1, SIZE(lambda)

          Pos(j,j) = MAX(0.d0, lambda(j)) * mod_n

          p_max = MAX( p_max, Pos(j,j) )     
              
       ENDDO

       inv_dt = inv_dt + p_max
      
       Beta(:,:, i) = 0.5d0 * MATMUL(RR, MATMUL(Pos, LL))

       N_m = N_m + Beta(:,:, i)

    ENDDO

    NN = InverseLU(N_m)

    DO i = 1, N_dofs

       Beta(:,:, i) = MATMUL( Beta(:,:, i), NN )

       Phi_i(:, i) = MATMUL( Beta(:,:, i), Phi_tot )

    ENDDO

    DEALLOCATE( lambda, RR, LL, Pos, N_m, NN )

    !-----------------------
    ! Time stepping ~ |V|/Dt
    !-------------------------------------------
!!$    u_max = 0.d0; c_max = 0.d0
!!$    DO i = 1, N_dofs
!!$       u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
!!$       c_max = MAX( c_max, Vp(i_son_vp, i) )
!!$    ENDDO
!!$
!!$    ! Element surface
!!$    S_ele = 0.d0
!!$    DO i = 1, ele%N_faces
!!$       S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
!!$    ENDDO
!!$
!!$    p_max = u_max + c_max
!!$
!!$    inv_dt = p_max*S_ele

#ifdef NAVIER_STOKES

    inv_dt = inv_dt + alpha_visc(ele, Vp)

#endif

    !///////////////////////////////////////////////////
    !//         Jacobian of the LDA scheme            //
    !///////////////////////////////////////////////////

    !---------------
    ! Diagonal term
    !----------------------------------------------------
    C_Dt = inv_dt / CFL

    DO i = 1, N_dofs

       Diag_i = MATMUL( Beta(:,:, i), JJ_tot(:,:, i) )

       DO k = 1, N_var
          Diag_i(k,k) = Diag_i(k,k) + C_Dt
       ENDDO

       CALL Assemblage_AddDiag(Diag_i, ele%NU(i), Mat)

    END DO

    !----------------------
    ! Extra-Diagonal terms 
    !---------------------------------------------------
    DO i = 1, N_dofs

       DO k = 1, N_dofs

          IF(k == i) CYCLE

          M_ik = MATMUL( Beta(:,:, i), JJ_tot(:,:, k) )

          CALL Assemblage_Add(M_ik, ele%NU(i), ele%NU(k), Mat)

       END DO

    END DO
    !///////////////////////////////////////////////////

    DEALLOCATE( Beta )

  END SUBROUTINE ResiduLDA_imp  
  !===========================

  !=================================================================
  SUBROUTINE ResiduLDA_exp(ele, Vp, Vc, CFL, Phi_tot, Phi_i, inv_dt)
  !=================================================================
    
    IMPLICIT NONE
    
    TYPE(element_str),                INTENT(IN)    :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
    REAL,                             INTENT(IN)    :: CFL
    REAL, DIMENSION(:),               INTENT(IN)    :: Phi_tot
    REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
    REAL,                             INTENT(OUT)   :: inv_dt
    !---------------------------------------------------------

    REAL, DIMENSION(:),   ALLOCATABLE :: lambda
    REAL, DIMENSION(:,:), ALLOCATABLE :: RR, LL, Pos
    REAL, DIMENSION(:,:), ALLOCATABLE :: N_m, NN

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: Beta

    REAL, DIMENSION(ele%N_dim) :: v_nn

    REAL, DIMENSION(SIZE(Vp,1)) :: Vp_b
    
    REAL :: mod_n, r_N_dofs, p_max, u_max, c_max, S_ele
   
    INTEGER :: N_dim, N_dofs, N_var, i, j, k
    !--------------------------------------------------

    N_dim  = Data%NDim
    N_var  = Data%NVar

    N_dofs = ele%N_Points; r_N_dofs = REAL(N_dofs, 8)
    
    ALLOCATE( lambda(N_var) )

    ALLOCATE(  RR(N_var, N_var), &
               LL(N_var, N_var), &
              Pos(N_var, N_var) )

    ALLOCATE( N_m(N_var, N_var), &
               NN(N_var, N_var) )

    ALLOCATE( Beta(N_var, N_var, N_dofs) )

    ! Mean state
    Vp_b = Mean_State_Vp(Vp)

    inv_dt = 0.d0;  N_m = 0.d0

    DO i = 1, N_dofs

       mod_n = SQRT( SUM(ele%rd_n(:, i)**2) )

       v_nn = ele%rd_n(:, i) / mod_n
       
       CALL Advection_eigenvalues ( Vp_b, v_nn, lambda )
       CALL Advection_eigenvectors( Vp_b, v_nn, RR, LL )

       Pos = 0.d0; p_max = 0.d0

       DO j = 1, SIZE(lambda)

          Pos(j,j) = MAX(0.d0, lambda(j)) * mod_n

          p_max = MAX( p_max, Pos(j,j) )
          
       ENDDO

       Beta(:,:, i) = 0.5d0 * MATMUL(RR, MATMUL(Pos, LL))

       N_m = N_m + Beta(:,:, i)

    ENDDO

    NN = InverseLU(N_m)

    DO i = 1, N_dofs

       Beta(:,:, i) = MATMUL( Beta(:,:, i), NN )

       Phi_i(:, i) = MATMUL( Beta(:,:, i), Phi_tot )

    ENDDO

    DEALLOCATE( lambda, RR, LL, Pos, N_m, NN, Beta )

    !-----------------------
    ! Time stepping ~ |V|/Dt
    !-------------------------------------------
    u_max = 0.d0; c_max = 0.d0
    DO i = 1, N_dofs
       u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
       c_max = MAX( c_max, Vp(i_son_vp, i) )
    ENDDO

    ! Element surface
    S_ele = 0.d0
    DO i = 1, ele%N_faces
       S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
    ENDDO

    p_max = u_max + c_max

    inv_dt = inv_dt*S_ele

#ifdef NAVIER_STOKES

    inv_dt = inv_dt + alpha_visc(ele, Vp)

#endif

  END SUBROUTINE ResiduLDA_exp
  !===========================

END MODULE Scheme_LDA
