Module Space_integration

  USE LesTypes
  USE Gradient_Recovery

  USE Advection
  USE Diffusion 
  USE Source

  USE Turbulence_models,   ONLY: Turbulent

  USE Num_schemes

  USE LibComm

  IMPLICIT NONE

CONTAINS
  
  !===================================
  SUBROUTINE imp_RD_fuq(Com, Var, Mat)
  !===================================

    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var
    TYPE(Matrice),   INTENT(INOUT) :: Mat
    !------------------------------------

    TYPE(element_str) :: ele

    REAL, DIMENSION(Data%NVar) :: Phi_tot

    REAL, DIMENSION(:), ALLOCATABLE :: SS

    REAL, DIMENSION(:,:), ALLOCATABLE :: Vp 
    REAL, DIMENSION(:,:), ALLOCATABLE :: Vc
    REAL, DIMENSION(:,:), ALLOCATABLE :: Phi_i

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: D_Vc

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: JJ_tot
    !--------------------------------------------

    REAL :: inv_dt

    INTEGER :: N_ele, N_var_C, N_var_P, N_dim
    INTEGER :: N_dofs, Nu_i
    INTEGER :: i, je, id
    !-----------------------------------------

    N_ele   = SIZE(d_element)
    N_var_C = Data%Nvar
    N_var_P = Data%NvarPhy
    N_dim   = Data%Ndim

#ifdef NAVIER_STOKES

    CALL Grad_Recovery(Com, Var)

#endif

    ALLOCATE( SS(N_ele) )

    call  shock_sensor(Com, Var, SS)

    DO je = 1, N_ele

       ele = d_element(je)%e

       N_dofs = ele%N_points

       ALLOCATE(    Vp(N_var_P, N_dofs) )
       ALLOCATE(    Vc(N_var_C, N_dofs) )
       ALLOCATE( Phi_i(N_var_C, N_dofs) )

       ALLOCATE( JJ_tot(N_var_C, N_var_C, N_dofs) )

#ifdef NAVIER_STOKES

       ALLOCATE( D_Vc(N_dim, N_var_C, N_dofs) )

#endif

       DO i = 1, N_dofs
          
          Nu_i = ele%NU(i)
          
          Vp(:, i) = Var%Vp(:, Nu_i)
          Vc(:, i) = Var%Ua(:, Nu_i)

#ifdef NAVIER_STOKES

          DO id = 1, N_dim
             D_Vc(id, :, i) = Var%D_Ua(id, :, Nu_i)
          ENDDO

#endif

       ENDDO

       !------------------------------
       ! Compute the total fluctuation
       !---------------------------------------
#ifndef NAVIER_STOKES      
       CALL Total_Residual_imp(ele, Vp, Vc, Phi_tot, JJ_tot)
#else
       CALL Total_Residual_imp(ele, Vp, Vc, Phi_tot, JJ_tot, D_Vc)
#endif

       !---------------------------
       ! Distribute the fluctuation
       !---------------------------------------------
#ifndef NAVIER_STOKES
       CALL Distribute_residual_imp(ele, Vp, Vc, Var%CFL, Phi_tot, JJ_tot, &
                                    SS(je), Phi_i, inv_dt, Mat)
#else
       CALL Distribute_residual_imp(ele, Vp, Vc, Var%CFL, Phi_tot, JJ_tot, &
                                    SS(je), Phi_i, inv_dt, Mat, D_Vc)
#endif

       !--------------------------
       ! Gather the nodal residual
       !-------------------------------------------
       DO i = 1, N_dofs

          Nu_i = ele%NU(i)

#ifndef SEQUENTIEL          
          IF(Nu_i > Var%NCellsIn) CYCLE
#endif

          Var%Flux(:, Nu_i) = Var%Flux(:, Nu_i) + Phi_i(:, i)

          Var%DTloc(Nu_i) = Var%DTloc(Nu_i) + inv_dt

       ENDDO

       DEALLOCATE( Vp, Vc, Phi_i, JJ_tot )

#ifdef NAVIER_STOKES
       DEALLOCATE( D_Vc )
#endif

    ENDDO

    DEALLOCATE( SS )

  END SUBROUTINE imp_RD_fuq  
  !========================
 
  !=======================================
  SUBROUTINE exp_RD_fuq(Com, Var, with_Dt)
    !=======================================

    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Space_integration/exp_RD_fuq"

    TYPE(MeshCom),     INTENT(INOUT) :: Com
    TYPE(Variables),   INTENT(INOUT) :: Var
    LOGICAL, OPTIONAL, INTENT(IN)    :: with_Dt
    !------------------------------------------

    TYPE(element_str) :: ele

    REAL, DIMENSION(Data%NVar) :: Phi_tot

    REAL, DIMENSION(:), ALLOCATABLE :: SS

    REAL, DIMENSION(:,:), ALLOCATABLE :: Vp 
    REAL, DIMENSION(:,:), ALLOCATABLE :: Vc, dVc
    REAL, DIMENSION(:,:), ALLOCATABLE :: Phi_i

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: D_Vc
    !--------------------------------------------

    REAL :: inv_dt

    INTEGER :: N_ele, N_var_C, N_var_P, N_dim
    INTEGER :: N_dofs, Nu_i
    INTEGER :: i, je, id

    LOGICAL :: cmp_Dt = .FALSE.
    !-----------------------------------------

    N_ele   = SIZE(d_element)
    N_var_C = Data%Nvar
    N_var_P = Data%NvarPhy
    N_dim   = Data%Ndim

    IF( PRESENT(with_Dt) ) THEN
       IF(with_Dt) cmp_Dt = .TRUE.
    ENDIF

#ifdef NAVIER_STOKES

    CALL Grad_Recovery(Com, Var)

#endif

    ALLOCATE( SS(N_ele) )

    call  shock_sensor(Com, Var, SS)



    DO je = 1, N_ele

       ele = d_element(je)%e

       N_dofs = ele%N_points

       ALLOCATE(    Vp(N_var_P, N_dofs) )
       ALLOCATE(    Vc(N_var_C, N_dofs) )
       ALLOCATE(    dVc(N_var_C, N_dofs) )
       ALLOCATE( Phi_i(N_var_C, N_dofs) )

#ifdef NAVIER_STOKES

       ALLOCATE( D_Vc(N_dim, N_var_C, N_dofs) )

#endif
       dVc=0.0
       DO i = 1, N_dofs

          Nu_i = ele%NU(i)

          Vp(:, i) = Var%Vp(:, Nu_i)
          Vc(:, i) = Var%Ua(:, Nu_i)

#ifdef NAVIER_STOKES

          DO id = 1, N_dim
             D_Vc(id, :, i) = Var%D_Ua(id, :, Nu_i)
          ENDDO

#endif

       ENDDO

       !-------------------------------
       ! Compute the total fluctuation
       !---------------------------------------
#ifndef NAVIER_STOKES      
       CALL Total_Residual_exp(ele, Vp, Vc, Phi_tot)
#else
       CALL Total_Residual_exp(ele, Vp, Vc, Phi_tot, D_Vc)
#endif       

       !---------------------------
       ! Distribute the fluctuation
       !---------------------------------------------
#ifndef NAVIER_STOKES
       CALL Distribute_residual_exp(ele, Vp, Vc, dVc, Var%CFL, Phi_tot, &
            SS(je), Phi_i, Var%dt_Phys, inv_dt)
#else
       CALL Distribute_residual_exp(ele, Vp, Vc, Var%CFL, Phi_tot, &
            SS(je), Phi_i, inv_dt, D_Vc)
#endif

       !--------------------------
       ! Gather the nodal residual
       !-------------------------------------------
       DO i = 1, N_dofs

          Nu_i = ele%NU(i)

#ifndef SEQUENTIEL          
          IF(Nu_i > Var%NCellsIn) CYCLE
#endif

          Var%Flux(:, Nu_i) = Var%Flux(:, Nu_i) + Phi_i(:, i)

          IF( cmp_Dt ) THEN

             Var%DTloc(Nu_i) = Var%DTloc(Nu_i) + inv_dt

          ENDIF

       ENDDO

       DEALLOCATE( Vp, Vc, Phi_i, dVc)

#ifdef NAVIER_STOKES
       DEALLOCATE( D_Vc )
#endif

    ENDDO

    DEALLOCATE( SS )

  END SUBROUTINE exp_RD_fuq
  !========================


  !=======================================
  SUBROUTINE exp_RD_fuq_exp(Com, Var, with_Dt)
    !=======================================

    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Space_integration/exp_RD_fuq_exp"


    TYPE(MeshCom),     INTENT(INOUT) :: Com
    TYPE(Variables),   INTENT(INOUT) :: Var
    LOGICAL, OPTIONAL, INTENT(IN)    :: with_Dt
    !------------------------------------------

    TYPE(element_str) :: ele

    REAL, DIMENSION(Data%NVar) :: Phi_tot
    REAL, DIMENSION(:), ALLOCATABLE :: SS

    REAL, DIMENSION(:,:), ALLOCATABLE :: h
    REAL, DIMENSION(:,:), ALLOCATABLE :: Vp , Vp_0
    REAL, DIMENSION(:,:), ALLOCATABLE :: Vc , Vc_0, dVc
    REAL, DIMENSION(:,:), ALLOCATABLE :: Phi_i

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: D_Vc
    !--------------------------------------------

    REAL :: inv_dt

    INTEGER :: N_ele, N_var_C, N_var_P, N_dim
    INTEGER :: N_dofs, Nu_i
    INTEGER :: i, je, id

    LOGICAL :: cmp_Dt = .FALSE.
    !-----------------------------------------

    N_ele   = SIZE(d_element)
    N_var_C = Data%Nvar
    N_var_P = Data%NvarPhy
    N_dim   = Data%Ndim

    IF( PRESENT(with_Dt) ) THEN
       IF(with_Dt) cmp_Dt = .TRUE.
    ENDIF

#ifdef NAVIER_STOKES

    CALL Grad_Recovery(Com, Var)

#endif
    ALLOCATE( SS(N_ele) )
    call shock_sensor(Com, Var, SS)

    DO je = 1, N_ele

       ele = d_element(je)%e

       N_dofs = ele%N_points

       ALLOCATE(    Vp(N_var_P, N_dofs)    )
       ALLOCATE(    Vc(N_var_C, N_dofs)    )
       ALLOCATE(    Vp_0(N_var_P, N_dofs)  )
       ALLOCATE(    Vc_0(N_var_C, N_dofs)  )
       ALLOCATE(    dVc(N_var_C, N_dofs)   )
       ALLOCATE(    Phi_i(N_var_C, N_dofs) )
!       ALLOCATE(    h    (2, N_dofs)       )

#ifdef NAVIER_STOKES

       ALLOCATE( D_Vc(N_dim, N_var_C, N_dofs) )

#endif

       DO i = 1, N_dofs

          Nu_i = ele%NU(i)

          Vp(:, i)   = Var%Vp(:, Nu_i)
          Vc(:, i)   = Var%Ua(:, Nu_i)
          Vp_0(:, i) = Var%Vp_0(:, Nu_i)
          Vc_0(:, i) = Var%Ua_0(:, Nu_i)
!          h   (:, i)  = Var%h   (:, Nu_i)

#ifdef NAVIER_STOKES

          DO id = 1, N_dim
             D_Vc(id, :, i) = Var%D_Ua(id, :, Nu_i)
          ENDDO

#endif

       ENDDO

       dVc= Vc -Vc_0

       Vp= 0.5*( Vp+Vp_0)  
       !  Vp =0.5*(Vp+Vp_0)
       DO i=1, N_dofs
          CALL Prim_to_Cons(Vp(:,i),Vc(:,i)) 
       ENDDO

       !-------------------------------
       ! Compute the total fluctuation
       !---------------------------------------
#ifndef NAVIER_STOKES      
       CALL Total_Residual_exp(ele, Vp, Vc, Phi_tot)
#else
       CALL Total_Residual_exp(ele, Vp, Vc, Phi_tot, D_Vc)
#endif       
!       CALL sensor ( Vc, Vp, h(1,:)) 

       !---------------------------
       ! Distribute the fluctuation
       !---------------------------------------------
#ifndef NAVIER_STOKES
       CALL Distribute_residual_exp(ele, Vp, Vc, dVc, Var%CFL, Phi_tot, SS(je),&
            Phi_i, inv_dt, Var%dt_phys)!, h(2,:))
#else
       CALL Distribute_residual_exp(ele, Vp, Vc, dVc, Var%CFL, Phi_tot, SS(je),&
            Phi_i, inv_dt, Var%dt_phys, D_Vc) !, h(2,:),D_Vc)
#endif


       !--------------------------
       ! Gather the nodal residual
       !-------------------------------------------
       DO i = 1, N_dofs

          Nu_i = ele%NU(i)

#ifndef SEQUENTIEL          
          IF(Nu_i > Var%NCellsIn) CYCLE
#endif

          Var%Flux(:, Nu_i) = Var%Flux(:, Nu_i) +  Phi_i(:, i)

!          Var%h(1,Nu_i)=MIN(Var%h(1,Nu_i),h(1,i))
          IF( cmp_Dt ) THEN

             !             Var%DTloc(Nu_i) = Var%DTloc(Nu_i) + inv_dt

             IF (Var%DTloc(Nu_i) == 0) THEN
                Var%DTloc(Nu_i) = inv_dt !?
             ELSE
                Var%DTloc(Nu_i) = MAX(Var%DTloc(Nu_i), inv_dt) 
             ENDIF

          ENDIF

       ENDDO

       DEALLOCATE( Vp, Vc, Phi_i, dVc , Vp_0, Vc_0)!, h)

#ifdef NAVIER_STOKES
       DEALLOCATE( D_Vc )
#endif
       !stop
    ENDDO
     
     deallocate (SS)

    IF( cmp_Dt ) THEN
       Var%DTloc = Var%DTloc/Var%CFL ! ~ Dx/(Dt*CLF)
    ENDIF

  END SUBROUTINE exp_RD_fuq_exp
  !========================

  !======================================
  FUNCTION exp_RD_i(Nu_i, Vc) RESULT(R_i)
  !======================================

    IMPLICIT NONE

    INTEGER,              INTENT(IN) :: Nu_i
    REAL, DIMENSION(:,:), INTENT(IN) :: Vc

    REAL, DIMENSION(Data%Nvar) :: R_i
    !-----------------------------------------

    TYPE(element_str) :: ele

!!$    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vp

    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vc_l, dVc
    REAL, DIMENSION(:,:,:), ALLOCATABLE :: DVc_l
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Vp_l
    REAL, DIMENSION(:),     ALLOCATABLE :: Phi_tot
    REAL, DIMENSION(:,:),   ALLOCATABLE :: Phi_i

    REAL :: dummy

    INTEGER :: je, i, k, l, N_var, N_dofs
    !-----------------------------------------

    N_var = Data%NVar

    R_i = 0.d0


!!$#ifdef NAVIER_STOKES
!!$
!!$    ALLOCATE( Vp(Data%NVarPhy, SIZE(Vc, 2)) )
!!$
!!$    DO k = 1, SIZE(Vc, 2)
!!$       CALL Cons_to_Prim( Vc(:, k), Vp(:, k) )
!!$    ENDDO
!!$
!!$#endif

    DO l = 1, SIZE(Connect(Nu_i)%cn_ele)

       je = Connect(Nu_i)%cn_ele(l)

       ele = d_element(je)%e

       N_dofs = ele%N_points

       ALLOCATE( Vc_l(Data%NVar,    N_dofs) )
       ALLOCATE( Vp_l(Data%NVarPhy, N_dofs) )
       allocate(  dVc(Data%NVar,    N_dofs) )
#ifdef NAVIER_STOKES

       ALLOCATE( DVc_l(Data%NDim, N_var, N_dofs) )

#endif

       ALLOCATE( Phi_tot(N_Var) )
       ALLOCATE(   Phi_i(N_Var, N_dofs) )

       Vc_l = Vc(:, ele%Nu)
       dVc=0.

       DO k = 1, N_dofs

          CALL Cons_to_Prim( Vc_l(:, k), Vp_l(:, k) )

#ifdef NAVIER_STOKES

!!$          DVc_l(:, :, k) = Grad_i__tmp(ele%Nu(k), Vp)
          DVc_l(:, :, k) = Grad_i__tmp(ele%Nu(k), Vc)

#endif

       ENDDO

       !-------------------------------
       ! Compute the total fluctuation
       !---------------------------------------
#ifndef NAVIER_STOKES
       CALL Total_Residual_exp(ele, Vp_l, Vc_l, Phi_tot)
#else
       CALL Total_Residual_exp(ele, Vp_l, Vc_l, Phi_tot, DVc_l)
#endif

       !---------------------------
       ! Distribute the fluctuation
       !---------------------------------------------
#ifndef NAVIER_STOKES
       CALL Distribute_residual_exp(ele, Vp_l, Vc_l, dVc,  dummy, &
                                     Phi_tot, dummy, Phi_i, dummy, dummy)
#else
       CALL Distribute_residual_exp(ele, Vp_l, Vc_l, dVc, dummy, &
                                     Phi_tot, dummy, Phi_i, dummy, dummy,  DVc_l)
#endif

       !--------------------------
       ! Gather the nodal residual
       !-------------------------------------------
       DO i = 1, N_dofs

          IF( ele%Nu(i) == Nu_i ) THEN

             R_i = R_i + Phi_i(:, i)
             EXIT

          ENDIF

       ENDDO

       DEALLOCATE( Vc_l, Vp_l, Phi_tot, Phi_i, dVc )

#ifdef NAVIER_STOKES
       DEALLOCATE( DVc_l )
#endif

    ENDDO ! ele \ni Nu_i

!!$#ifdef NAVIER_STOKES
!!$    DEALLOCATE( Vp )
!!$#endif

  END FUNCTION exp_RD_i  
  !====================

!------------------------------------------------------------------
!--------------------------- IMPLICIT -----------------------------
!------------------------------------------------------------------

  !================================================================
  SUBROUTINE Total_Residual_imp(ele, Vp, Vc, Phi_tot, JJ_tot, D_Vc)
  !================================================================

    IMPLICIT NONE

    TYPE(element_str),                INTENT(IN)  :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)  :: Vc
    REAL, DIMENSION(:),               INTENT(OUT) :: Phi_tot 
    REAL, DIMENSION(:,:,:),           INTENT(OUT) :: JJ_tot
    REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)  :: D_Vc
    !-----------------------------------------------------

    REAL, DIMENSION(:,:,:), POINTER :: D_phi_k

    INTEGER,                 POINTER :: N_quad_f
    INTEGER,                 POINTER :: N_dofs_f
    INTEGER, DIMENSION(:),   POINTER :: l_nu_f
    REAL,    DIMENSION(:,:), POINTER :: p_f
    REAL,    DIMENSION(:,:), POINTER :: n_f
    REAL,    DIMENSION(:),   POINTER :: w_f

    REAL, DIMENSION(:,:),   POINTER :: p
    REAL, DIMENSION(:),     POINTER :: w
    REAL, DIMENSION(:,:,:), POINTER :: D_p
    !---------------------------------------------------

    REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_q

    REAL, DIMENSION(Data%Ndim, Data%Nvar) :: D_Vc_q

    REAL, DIMENSION(Data%Nvar, Data%Ndim) :: ff_advec
    REAL, DIMENSION(Data%Nvar, Data%Ndim) :: ff_visc
    REAL, DIMENSION(Data%Nvar)            :: SS_q

    REAL, DIMENSION(Data%Nvar) :: Phi_b, Phi_s

    REAL :: d_min_q
    !---------------------------------------------------

    REAL, DIMENSION(:,:,:), ALLOCATABLE :: AAq
    REAL, DIMENSION(:,:),   ALLOCATABLE :: AAq_n

    REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: KK_q
    REAL, DIMENSION(:,:),     ALLOCATABLE :: D_phi_q
    !---------------------------------------------------
    
    INTEGER :: N_dim, N_var, N_dofs
    INTEGER :: jf, iq, k, id, jd, l_j, i
    !---------------------------------------------------

    ff_advec = 0.d0;  ff_visc = 0.d0
    
    N_dim  = Data%Ndim
    N_var  = Data%Nvar
    N_dofs = ele%N_Points

    Phi_b = 0.d0;  Phi_s = 0.d0

    IF(Data%Iflux == 4) RETURN

    !--------------------
    ! Conservative fluxes
    !----------------------------------------------
    DO jf = 1, ele%N_faces

       N_quad_f => ele%faces(jf)%f%N_quad
       N_dofs_f => ele%faces(jf)%f%N_points
       l_nu_f   => ele%faces(jf)%f%l_nu
       p_f      => ele%faces(jf)%f%phi_q
       w_f      => ele%faces(jf)%f%w_q
       n_f      => ele%faces(jf)%f%n_q

       DO iq = 1, N_quad_f
          
          Vp_q = Interp_Vp( Vp(:, l_nu_f), p_f(:, iq) )

          !---------------
          ! Advective flux
          !-------------------------------------------
          CALL advection_flux( N_dim, Vp_q, ff_advec )

#ifdef NAVIER_STOKES

          D_Vc_q = 0.d0
          DO k = 1, N_dofs_f

             DO id = 1, N_dim
                
                D_Vc_q(id, :) = D_Vc_q(id, :) + &
                                D_Vc(id, :, l_nu_f(k)) * p_f(k, iq)

             ENDDO

          ENDDO
          
          !--------------
          ! Viscous flux
          !-------------------------------------------------
          CALL diffusion_flux( N_dim, Vp_q, D_Vc_q, ff_visc )

#endif
          
          ! Face integral
          DO id = 1, N_dim

             Phi_b = Phi_b + w_f(iq) * &
                            (ff_advec(:, id) - ff_visc(:, id))*n_f(id, iq)

          ENDDO

       ENDDO ! iq <- N_quad

       NULLIFY( N_quad_f, N_dofs_f, l_nu_f, p_f, w_f, n_f )

    ENDDO ! j <- N_faces

#ifdef NAVIER_STOKES

    !-------------
    ! Source term
    !----------------------------------------------
    IF( Turbulent ) THEN  
    
       D_p => ele%D_phi_q
         p => ele%phi_q
         w => ele%w_q

       DO iq = 1, ele%N_quad
      
          d_min_q = SUM(ele%d_min * p(:, iq))
       
          Vp_q = Interp_Vp( Vp, p(:, iq) )

          D_Vc_q = 0.d0
          DO k = 1, N_dofs
             
             DO id = 1, N_dim

                D_Vc_q(id, :) = D_Vc_q(id, :) + &
                                Vc(:, k) * D_p(id, k, iq)

             ENDDO

          ENDDO

          CALL Source_term(Vp_q, D_Vc_q, d_min_q, SS_q)

          ! Domain integral
          Phi_s = Phi_s + w(iq) * SS_q

       ENDDO ! iq <- N_quad

       NULLIFY( D_p, p, w )

    ENDIF
 
#endif

    Phi_tot = Phi_b - Phi_s
    !----------------------------------------------

          
    !////////////////////////////////////////////////////////////
    !//             JACOBIAN OF THE TOTAL RESIDUAL             //
    !//                                                        //
    !//                   d Phi_tot(m)                         //
    !// JJ_tot(m, n, k) = ------------ ;  m,n = 1, ..., N_var  //
    !//                     d Vc(n)_i       i = 1, ..., N_dofs //
    !////////////////////////////////////////////////////////////
    JJ_tot = 0.d0

    ALLOCATE(   AAq(N_var, N_var, N_dim) )
    ALLOCATE( AAq_n(N_var, N_var) )

#ifdef NAVIER_STOKES

    ALLOCATE( KK_q(N_var, N_var, N_dim, N_dim) )

    ALLOCATE( D_phi_q(N_dim, N_dofs) )

    D_phi_k => ele%D_phi_k

#endif

    DO i = 1, N_dofs

       !--------------------
       ! Conservative fluxes
       !----------------------------------------------
       DO jf = 1, ele%N_faces

          N_quad_f => ele%faces(jf)%f%N_quad
          N_dofs_f => ele%faces(jf)%f%N_points
          l_nu_f   => ele%faces(jf)%f%l_nu
          p_f      => ele%faces(jf)%f%phi_q
          w_f      => ele%faces(jf)%f%w_q
          n_f      => ele%faces(jf)%f%n_q

          DO l_j = 1, N_dofs_f

             !-----------------------
             ! Advective contribution
             !----------------------------------------
             IF( l_nu_f(l_j) == i ) THEN

                DO iq = 1, N_quad_f
                   
                   Vp_q = Interp_Vp( Vp(:, l_nu_f), p_f(:, iq) )
                   
                   ! AA(Vp_q)
                   CALL Advection_Jacobian(N_dim, Vp_q, AAq)

                   ! AA(Vp_q) * n_q
                   AAq_n = 0.d0
                   DO id = 1, N_dim

                      AAq_n = AAq_n + AAq(:,:, id)*n_f(id, iq)

                   ENDDO
                   
                   JJ_tot(:,:, i) = JJ_tot(:,:, i) + &
                                    w_f(iq) * p_f(l_j, iq) * AAq_n

!!$#ifdef NAVIER_STOKES
!!$       
!!$                   !-------------------------------
!!$                   ! Diffusive contribution (exact)
!!$                   !----------------------------------------
!!$                   !
!!$                   ! d(KK_v)/dU * Grad(U)
!!$                   !
!!$                   D_phi_q = 0.d0 
!!$                   DO k = 1, N_dofs_f
!!$
!!$                      DO id = 1, N_dim
!!$                
!!$                         D_phi_q(id, :) = D_phi_q(id, :) + &
!!$                                          D_phi_k(id, :, l_nu_f(k))*p_f(k, iq)
!!$
!!$                      ENDDO
!!$
!!$                   ENDDO ! k <- N_dofs(jf)
!!$                   
!!$                   DO k = 1, N_var
!!$
!!$                      DO id = 1, N_dim
!!$
!!$                         D_Vc_q(id, k) = SUM(D_phi_q(id, :)*Vc(k, :))
!!$
!!$                      ENDDO
!!$
!!$                   ENDDO
!!$
!!$                   CALL Diffusion_flux_Jacobian(Vp_q, D_Vc_q, AAq)
!!$
!!$                   ! AA(Vp_q) * n_q
!!$                   AAq_n = 0.d0
!!$                   DO id = 1, N_dim
!!$
!!$                      AAq_n = AAq_n + AAq(:,:, id)*n_f(id, iq)
!!$
!!$                   ENDDO
!!$
!!$                   JJ_tot(:,:, i) = JJ_tot(:,:, i) + &
!!$                                    w_f(iq) * p_f(l_j, iq) * AAq_n
!!$
!!$#endif

                ENDDO ! iq <- N_quad(jf)

             ENDIF ! IF i \in face(jf)

          ENDDO ! l_j <- N_dofs(jf)

#ifdef NAVIER_STOKES

          !------------------------------------
          ! Viscous contribution (approximated)
          !----------------------------------------
          !
          ! KK_v * d(Grad U)/dU
          !
          ! TODO: should try to compute the Jacobian
          !       of the viscous part strarting from
          !       DiV( SUM_i KK_i phi_i Gradu_i ).
          !       It will possible also to retain the
          !       positive term only (if necessary)         
          DO iq = 1, N_quad_f

             Vp_q = Interp_Vp( Vp(:, l_nu_f), p_f(:, iq) )

             D_phi_q = 0.d0 
             DO k = 1, N_dofs_f

                DO id = 1, N_dim
                
                   D_phi_q(id, :) = D_phi_q(id, :) + &
                                    D_phi_k(id, :, l_nu_f(k))*p_f(k, iq)

                ENDDO

             ENDDO ! k <- N_dofs(jf)
             
             CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)

             AAq = 0.d0
             DO id = 1, N_dim

                DO jd = 1, N_dim
                   
                   AAq(:,:, id) = AAq(:,:, id) + &
                                  KK_q(:,:, id, jd) * D_phi_q(jd, i)

                ENDDO

             ENDDO

             AAq_n = 0.d0
             DO id = 1, N_dim

                AAq_n = AAq_n + AAq(:,:, id) * n_f(id, iq)

             ENDDO

             JJ_tot(:,:, i) = JJ_tot(:,:, i) - &
                              w_f(iq) * AAq_n

          ENDDO ! iq <- N_quad(jf)

        
#endif

       ENDDO ! jf <- N_faces 

       NULLIFY( N_quad_f, N_dofs_f, l_nu_f, p_f, w_f, n_f )

       
#ifdef NAVIER_STOKES

       !-------------
       ! Source term
       !----------------------------------------------
       IF( Turbulent ) THEN
          
          D_p => ele%D_phi_q
            p => ele%phi_q
            w => ele%w_q

          DO iq = 1, ele%N_quad

             d_min_q = SUM(ele%d_min * p(:, iq))

             Vp_q = Interp_Vp( Vp, p(:, iq) )

             D_Vc_q = 0.d0
             DO k = 1, N_dofs
             
                DO id = 1, N_dim

                   D_Vc_q(id, :) = D_Vc_q(id, :) + &
                                   Vc(:, k) * D_p(id, k, iq)

                ENDDO

             ENDDO

             JJ_tot(:,:, i) = JJ_tot(:,:, i) - &
                              w(iq)*p(i, iq) * & 
                              Jacobian_Source(Vp_q, D_Vc_q, d_min_q)

          ENDDO ! iq <- N_quad

          NULLIFY( D_p, p, w )

       ENDIF ! IF turbulent

#endif

    ENDDO ! i <- N_dofs

    DEALLOCATE( AAq, AAq_n ) 

#ifdef NAVIER_STOKES
    DEALLOCATE( KK_q, D_phi_q )
    NULLIFY( D_phi_k )    
#endif

    !///////////////////////////////////////////////////////////

  END SUBROUTINE Total_Residual_imp  
  !================================

!------------------------------------------------------------------
!--------------------------- EXPLICIT -----------------------------
!------------------------------------------------------------------

  !========================================================
  SUBROUTINE Total_Residual_exp(ele, Vp, Vc, Phi_tot, D_Vc)
  !========================================================

    IMPLICIT NONE

    TYPE(element_str),                INTENT(IN)  :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)  :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)  :: Vc
    REAL, DIMENSION(:),               INTENT(OUT) :: Phi_tot
    REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)  :: D_Vc
    !-----------------------------------------------------

    INTEGER,                 POINTER :: N_quad_f
    INTEGER,                 POINTER :: N_dofs_f
    INTEGER, DIMENSION(:),   POINTER :: l_nu_f
    REAL,    DIMENSION(:,:), POINTER :: p_f
    REAL,    DIMENSION(:,:), POINTER :: n_f
    REAL,    DIMENSION(:),   POINTER :: w_f

    REAL, DIMENSION(:,:),   POINTER :: p
    REAL, DIMENSION(:),     POINTER :: w
    REAL, DIMENSION(:,:,:), POINTER :: D_p
    !---------------------------------------------------

    REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_q

    REAL, DIMENSION(Data%Ndim, Data%Nvar) :: D_Vc_q

    REAL, DIMENSION(Data%Nvar, Data%Ndim):: ff_advec
    REAL, DIMENSION(Data%Nvar, Data%Ndim):: ff_visc
    REAL, DIMENSION(Data%Nvar)            :: SS_q

    REAL, DIMENSION(Data%Nvar) :: Phi_b, Phi_s

    REAL :: d_min_q
    !---------------------------------------------------

    INTEGER :: N_dim, N_var, N_dofs
    INTEGER :: jf, iq, k, id
    !---------------------------------------------------

    ff_advec = 0.d0;  ff_visc = 0.d0
    
    N_dim  = Data%Ndim
    N_var  = Data%Nvar
    N_dofs = ele%N_Points

    IF(Data%Iflux == 4) RETURN ! todo

    !------------------
    ! Boundary integral
    !----------------------------------------------
    Phi_b = 0.d0;  Phi_s = 0.d0

    DO jf = 1, ele%N_faces

       N_quad_f => ele%faces(jf)%f%N_quad
       N_dofs_f => ele%faces(jf)%f%N_points
       l_nu_f   => ele%faces(jf)%f%l_nu
       p_f      => ele%faces(jf)%f%phi_q
       w_f      => ele%faces(jf)%f%w_q
       n_f      => ele%faces(jf)%f%n_q

       DO iq = 1, N_quad_f
          
          Vp_q = Interp_Vp( Vp(:, l_nu_f), p_f(:, iq) )

          !---------------
          ! Advective flux
          !-------------------------------------------
          CALL advection_flux( N_dim, Vp_q, ff_advec )

#ifdef NAVIER_STOKES

          D_Vc_q = 0.d0
          DO k = 1, N_dofs_f

             DO id = 1, N_dim
                
                D_Vc_q(id, :) = D_Vc_q(id, :) + &
                                D_Vc(id, :, l_nu_f(k)) * p_f(k, iq)

             ENDDO

          ENDDO
          
          !--------------
          ! Viscous flux
          ! -------------------------------------------------
          CALL diffusion_flux( N_dim, Vp_q, D_Vc_q, ff_visc )

#endif
          
          ! Face integral
          DO id = 1, N_dim
             Phi_b = Phi_b + w_f(iq) * &
                            (ff_advec(:, id) - ff_visc(:, id))*n_f(id, iq)
          ENDDO

       ENDDO ! iq <- N_quad

       NULLIFY( N_quad_f, N_dofs_f, l_nu_f, p_f, w_f, n_f )

    ENDDO ! j <- N_faces


#ifdef NAVIER_STOKES

    !-------------
    ! Source term
    !----------------------------------------------
    IF( Turbulent ) THEN

       DO iq = 1, ele%N_quad

          D_p => ele%D_phi_q
            p => ele%phi_q
            w => ele%w_q

          d_min_q = SUM(ele%d_min * p(:, iq))

          Vp_q = Interp_Vp( Vp, p(:, iq) )

          D_Vc_q = 0.d0
          DO k = 1, N_dofs
             
             DO id = 1, N_dim

                D_Vc_q(id, :) = D_Vc_q(id, :) + &
                                Vc(:, k) * D_p(id, k, iq)

             ENDDO

          ENDDO

          CALL Source_term(Vp_q, D_Vc_q, d_min_q, SS_q)

          ! Domain integral
          Phi_s = Phi_s + w(iq) * SS_q

          NULLIFY( D_p, p, w )

       ENDDO ! iq <- N_quad

    ENDIF

#endif

    Phi_tot = Phi_b - Phi_s
    !----------------------------------------------

  END SUBROUTINE Total_Residual_exp
  !================================

!------------------------------------------------------------------------
!------------------------------------------------------------------------
!------------------------------------------------------------------------

  !=========================================
  subroutine  shock_sensor(Com, Var, ss)
  !=========================================
    
    IMPLICIT NONE

    TYPE(MeshCom),   INTENT(INOUT) :: Com
    TYPE(Variables), INTENT(INOUT) :: Var

    REAL, DIMENSION(:), intent(inout) :: ss
    !------------------------------------------

    TYPE(element_str) :: ele

    REAL, DIMENSION(Data%NDim) :: vv, int_Dp, Dp_q

    REAL :: P_max, P_min, P_Mn, nn, v2, d_Pv2, mod_v

    REAL :: L_ref, h, sc

    REAL, PARAMETER :: eps = 1.0E-12
    
    INTEGER :: Nu_i, l, i, je, id, iq
    !------------------------------------------

    P_max = MAXVAL(Var%Vp(i_pre_vp, :))
    P_min = MINVAL(Var%Vp(i_pre_vp, :))

#ifndef SEQUENTIEL
    CALL Reduce(Com, cs_parall_max, P_max)
    CALL Reduce(Com, cs_parall_min, P_min)
#endif

    P_Mn = P_max - P_min

    DO id = 1, Data%NDim
       vv(id) = SUM( Var%Vp(id+1, :) )
    ENDDO

    nn = REAL(Var%NCells, 8)

#ifndef SEQUENTIEL
    CALL Reduce(Com, cs_parall_sum, nn)

    DO id = 1, Data%NDim
       CALL Reduce(Com, cs_parall_sum, vv(id))
    ENDDO
#endif

    vv = vv / nn

    v2 = SUM( vv**2 )

    d_Pv2 = P_Mn * v2

!    ALLOCATE( SS(SIZE(d_element)) )

    DO je = 1, SIZE(d_element)
       
       ele = d_element(je)%e

       DO id = 1, Data%NDim
          vv(id) = SUM(Var%Vp(id+1, ele%NU)) / &
                   REAL(ele%N_points, 8)
       ENDDO

       mod_v = SQRT( SUM(vv**2) )

       int_Dp = 0.d0
       DO iq = 1, ele%N_quad

          Dp_q = 0.d0
          DO id = 1, Data%NDim
             Dp_q(id) = SUM( Var%Vp(i_pre_vp, ele%NU) * &
                              ele%D_phi_q(id, :, iq) )
          ENDDO

          int_Dp = int_Dp + ele%w_q(iq) * Dp_q

       ENDDO

       sc = DOT_PRODUCT( int_Dp, vv ) * mod_v / (d_Pv2 * ele%Volume + eps)

       L_ref = Data%LRef

       h = HUGE(1.d0)
       DO i = 1, ele%N_verts
          h = MIN( h, SQRT(SUM(ele%rd_n(:, i)**2)) )
       ENDDO

       SS(je) = 1.d0 - MIN(1.d0, L_ref * h * sc**2)

    ENDDO

  END subroutine shock_sensor  
  !========================

END Module Space_integration
