MODULE Scheme_SUPG

  USE LesTypes
  USE PLinAlg
  
  USE Advection
  USE Diffusion 
  USE Source
  USE EOSLaws
  USE Visc_Laws
  USE Turbulence_models,   ONLY: Turbulent

  USE PLinAlg
  USE Assemblage

  IMPLICIT NONE

CONTAINS

  !======================================================================
  SUBROUTINE SUPG_scheme_imp(ele, Vp, Vc, CFL, Phi_i, inv_dt, Mat, Dr_Vc)
  !======================================================================

    IMPLICIT NONE

    TYPE(element_str),                INTENT(IN)    :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
    REAL,                             INTENT(IN)    :: CFL
    REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
    REAL,                             INTENT(OUT)   :: inv_dt
    TYPE(Matrice),                    INTENT(INOUT) :: Mat
    REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: Dr_Vc
    !---------------------------------------------------------

    REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: Jac

    REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q, &
                                             Dr_Vc_q

    REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_b, &
                                    Vp_q

    REAL, DIMENSION(Data%NVar) :: S_q

    REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q

    REAL, DIMENSION(Data%NVar, Data%NDim) :: ff_v

    REAL, DIMENSION(Data%NVar, Data%Nvar, &
                    Data%NDim, Data%NDim) :: KK_q

    REAL, DIMENSION(:,:, :,:, :), ALLOCATABLE :: KK_i, &
                                                 KKq_i

    REAL, DIMENSION(Data%NVar) :: A_GU,   &
                                  Div_Fv, &
                                  Res

    REAL, DIMENSION(Data%NVar, Data%Nvar) :: A_Gphi_i, &
                                             A_Gphi_k, &
                                             KK_Gphi_i

    REAL, DIMENSION(Data%NVar, Data%Nvar) :: J_Res_k,  &
                                             J_Div_Fv, &
                                             J_Sq

    REAL, DIMENSION(Data%NVar, Data%Nvar) :: Tau
    REAL, DIMENSION(Data%NVar, Data%NDim) :: nu_shock
    !---------------------------------------------------

    REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
    REAL, DIMENSION(:,:),   POINTER :: D_phi_q
    REAL, DIMENSION(:),     POINTER :: phi_q
    REAL, DIMENSION(:),     POINTER :: w_q
    !---------------------------------------------------- 

    REAL, DIMENSION(Data%NVar) :: R_i

    REAL, DIMENSION(Data%NVar, Data%Nvar) :: M_ik, Diag_i

    REAL :: u_max, p_max, c_max, Dt, S_ele, h_e, d_min_q

    REAL :: r_N_dim, r_N_dofs

    INTEGER :: N_var, N_dim, N_dofs, N_quad

    INTEGER :: i, j, k, id, jd, iq
    !----------------------------------------------------
    
    N_var = Data%NVar
    N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

    N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

    N_quad = ele%N_quad

    D_phi_k => ele%D_phi_k 
        w_q => ele%w_q

    ALLOCATE( Jac(N_var, N_var, N_dofs, N_dofs) ) 

    Vp_b = Mean_state_Vp(Vp)

#ifdef NAVIER_STOKES

    ALLOCATE( KK_i (N_var,N_var, N_dim,N_dim, N_dofs) )
    ALLOCATE( KKq_i(N_var,N_var, N_dim,N_dim, N_dofs) )

    DO i = 1, N_dofs
       CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_i(:,:, :,:, i) )
    ENDDO

#endif

    Phi_i = 0.d0; Jac = 0.d0

    DO iq = 1, N_quad

       !-----------------------
       ! Stuff defined at the 
       ! quadrature points only
       !------------------------------------------------

       D_phi_q => ele%D_phi_q(:,:, iq)
         phi_q => ele%phi_q(:, iq)

       ! Interpolated solution
       Vp_q = Interp_Vp( Vp, phi_q )

       ! Scaling matrix
       Tau = Tau_matrix( D_phi_q, Vp_b )

       ! FEM Gradient
       D_Vc_q = 0.d0
       DO k = 1, N_dofs
          DO id = 1, N_dim
             D_Vc_q(id, :) = D_Vc_q(id, :) &
                           + D_phi_q(id, k) * Vc(:, k)
          ENDDO
       ENDDO

       ! Advective Jacobian 
       CALL advection_Jacobian(N_dim, Vp_q, AA_q)

       !****************************
       ! Strong form of the residual
       !************************************************

       A_GU = 0.d0
       DO id = 1, N_dim
          A_GU = A_GU &
               +  MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )
       ENDDO

       ! Inviscid residual
       Res = A_GU

#ifdef NAVIER_STOKES

       ! Reconstructed Gradient
       Dr_Vc_q = 0.d0
       DO i = 1, N_dofs
          DO id = 1, N_dim
             Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                              phi_q(i) * Dr_Vc(id, :, i)
          ENDDO
       ENDDO

       ! Viscous flux & Jacobian 
       CALL Diffusion_flux_function(Vp_q, D_Vc_q, ff_v)

       CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)

       DO i = 1, N_dofs
          DO id = 1, N_dim
             KKq_i(:,:, id, :, i) = KK_i(:,:, id, :, i) &
                                  * D_phi_q(id, i)
          ENDDO
       ENDDO

       Div_Fv = 0.d0
       DO id = 1, N_dim

          DO i = 1, N_dofs

             DO jd = 1, N_dim
                Div_Fv = Div_Fv &
                       +  MATMUL( KKq_i(:,:, id, jd, i), &
                                  Dr_Vc(jd, :, i) )
             ENDDO

          ENDDO

       ENDDO
       
       ! Diffusion residual
       Res = Res - Div_fv
       
       ! Turbulent source term
       IF( Turbulent ) THEN 

          d_min_q = SUM(ele%d_min * phi_q)
          CALL Source_term(Vp_q, D_Vc_q, d_min_q, S_q)

          J_Sq = Jacobian_Source(Vp_q, D_Vc_q, d_min_q)

          ! Source residual
          Res = Res - S_q
          
       ENDIF

#endif

       ! Artificial viscosity
       nu_shock = Shock_viscosity(ele, Vp_q, D_Vc_q, Res)
 
       !--------------------------------------------------
       !--------------------------------------------------
       !--------------------------------------------------

       ! For each DOF i...
       DO i = 1, N_dofs

          !*******************
          ! Galerkin Residual 
          !************************************************

          !Inviscid
          R_i = phi_q(i) * A_GU

#ifdef NAVIER_STOKES

          ! Viscous I.b.P.
          DO id = 1, N_dim
             R_i = R_i &
                 + D_phi_q(id, i) * ff_v(:, id)
          ENDDO

          ! FOS Stabilization
          DO id = 1, N_dim
             
             KK_Gphi_i = 0.d0
             DO jd = 1, N_dim
                KK_Gphi_i = KK_Gphi_i & 
                          + KK_q(:,:, id, jd) * D_phi_q(jd, i)
             ENDDO
             
             R_i = R_i &
                 + MATMUL( KK_Gphi_i, &
                           D_Vc_q(id, :) - Dr_Vc_q(id, :) )

          ENDDO

          ! Turbulent source term
          IF( Turbulent ) THEN 
             R_i = R_i - phi_q(i) * S_q
          ENDIF

#endif

          !*******************
          ! Stabilization term
          !************************************************

          A_Gphi_i = 0.d0
          DO id = 1, N_dim
             A_Gphi_i = A_Gphi_i &
                      + AA_q(:,:, id) * D_phi_q(id, i)
          ENDDO

          R_i = R_i &
              + MATMUL( A_Gphi_i, &
                        MATMUL(Tau, Res) )

          !*******************
          ! Shock capturing term
          !************************************************

          DO id = 1, N_dim
             R_i = R_i & 
                 + nu_shock(:, id) &
                   * D_Vc_q(id, :) * D_phi_q(id, i)
          ENDDO

          ! Perform the integration
          Phi_i(:, i) = Phi_i(:, i) & 
                      + R_i * w_q(iq)

          !-----------------------------
          ! Construction of the Jacobian
          !------------------------------------------------
          DO k = 1, N_dofs

             IF( k == i ) THEN

                A_Gphi_k = A_Gphi_i

             ELSE

                A_Gphi_k = 0.d0
                DO id = 1, N_dim
                   A_Gphi_k = A_Gphi_k &
                            + AA_q(:,:, id)*D_phi_q(id, k)
                ENDDO

             ENDIF

             ! Jacobian of the Galerkin term (inviscid)
             M_ik = phi_q(i) * A_Gphi_k

             ! Jacobian of inviscid strong residual
             J_Res_k = A_Gphi_k

#ifdef NAVIER_STOKES

             ! Jacobian of the Galerkin term (viscous)
             DO id = 1, N_dim
                DO jd = 1, N_dim
                   M_ik = M_ik &
                        + D_phi_q(id, i) &
                          * KK_q(:,:, id, jd) * D_phi_q(jd, k)
                ENDDO
             ENDDO

             ! Jacobian of viscous strong residual
             J_Div_Fv = 0.d0
             DO id = 1, N_dim
                
                DO j = 1, N_dofs

                   DO jd = 1, N_dim
                      J_Div_Fv = J_Div_Fv &
                               + KKq_i(:,:, id, jd, j) &
                                 * D_phi_k(jd, k, j) 
                   ENDDO

                ENDDO

             ENDDO
             
             J_Res_k = J_Res_k - J_Div_Fv

             IF( Turbulent ) THEN

                ! Jacobian of the Galerkin term (Source)
                M_ik = M_ik &
                     - phi_q(i) * J_Sq * phi_q(k)                
                
                ! Jacobian of source strong residual
                J_Res_k = J_Res_k &
                        - J_Sq * phi_q(k)                
                
             ENDIF

#endif

             ! Jacobian of the stabilization term
             M_ik = M_ik &
                  + MATMUL( A_Gphi_i, &
                            MATMUL(Tau, J_Res_k) ) 
             
             ! Jacobian of the shock capturing term
             DO j = 1, N_var
                DO id = 1, N_dim
                   M_ik(j,j) = M_ik(j,j) &
                             + nu_shock(j, id) &
                               * D_phi_q(id, i) * D_phi_q(id, k)
                ENDDO
             ENDDO
             
             ! Perform the integration
             Jac(:,:, i, k) = Jac(:, :, i, k) &
                            + M_ik * w_q(iq)
 
          ENDDO
          !------------------------------------------------

       ENDDO ! i = 1 ... N_dofs

       NULLIFY( D_phi_q, phi_q )

    ENDDO ! iq = 1 ... N_quad

    NULLIFY( D_phi_k, w_q )

#ifdef NAVIER_STOKES

    DEALLOCATE( KK_i, KKq_i )

#endif

    !-----------------------
    ! Time stepping ~ |V|/Dt
    !-------------------------------------------
    u_max = 0.d0; c_max = 0.d0
    DO i = 1, N_dofs
       u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
       c_max = MAX( c_max, Vp(i_son_vp, i) )
    ENDDO

    p_max = u_max + c_max

    ! Element surface
    S_ele = 0.d0
    DO i = 1, ele%N_faces
       S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
    ENDDO

    h_e = r_N_dim * ele%Volume/S_ele

#ifdef NAVIER_STOKES
    Dt = h_e / ( p_max + alpha_visc(ele, Vp) )
#else
    Dt = h_e /   p_max
#endif
    
    Dt = CFL * Dt

    inv_dt = (ele%Volume/r_N_dofs)/Dt
    !-------------------------------------------

    !//////////////////////////////////////////////////
    !//         Add the local contribution to        //
    !//          the global Jacobian matrix          //
    !//////////////////////////////////////////////////
    
    !---------------
    ! Diagonal term
    !----------------------------------------------------
    DO i = 1, N_dofs

       Diag_i = Jac(:,:, i, i)

       DO j = 1, N_var
          Diag_i(j,j) = Diag_i(j,j) + inv_dt
       ENDDO

       CALL Assemblage_AddDiag(Diag_i, ele%NU(i), Mat)

    ENDDO

    !----------------------
    ! Extra-Diagonal terms 
    !---------------------------------------------------
    DO i = 1, N_dofs
       
       DO k = 1, N_dofs

          IF(k == i) CYCLE

          M_ik = Jac(:,:, i, k)

          CALL Assemblage_Add(M_ik, ele%NU(i), ele%NU(k), Mat)

       ENDDO

    ENDDO

    DEALLOCATE(Jac)

  END SUBROUTINE SUPG_scheme_imp
  !=============================
  
  !=================================================================
  SUBROUTINE SUPG_scheme_exp(ele, Vp, Vc, CFL, Phi_i, inv_dt, Dr_Vc)
  !=================================================================

    IMPLICIT NONE

    TYPE(element_str),                INTENT(IN)    :: ele
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vp
    REAL, DIMENSION(:,:),             INTENT(IN)    :: Vc
    REAL,                             INTENT(IN)    :: CFL
    REAL, DIMENSION(:,:),             INTENT(OUT)   :: Phi_i
    REAL,                             INTENT(OUT)   :: inv_dt
    REAL, DIMENSION(:,:,:), OPTIONAL, INTENT(IN)    :: Dr_Vc
    !---------------------------------------------------------

    REAL, DIMENSION(Data%NDim, Data%NVar) :: D_Vc_q, &
                                             Dr_Vc_q

    REAL, DIMENSION(SIZE(Vp, 1)) :: Vp_b, &
                                    Vp_q

    REAL, DIMENSION(Data%NVar) :: S_q

    REAL, DIMENSION(Data%NVar, Data%Nvar, Data%NDim) :: AA_q

    REAL, DIMENSION(Data%NVar, Data%NDim) :: ff_v

    REAL, DIMENSION(Data%NVar, Data%Nvar, &
                    Data%NDim, Data%NDim) :: KK_q

    REAL, DIMENSION(:,:, :,:, :), ALLOCATABLE :: KK_i, &
                                                 KKq_i

    REAL, DIMENSION(Data%NVar) :: A_GU,   &
                                  Div_Fv, &
                                  Res

    REAL, DIMENSION(Data%NVar, Data%Nvar) :: A_Gphi_i, &
                                             KK_Gphi_i

    REAL, DIMENSION(Data%NVar, Data%Nvar) :: Tau
    REAL, DIMENSION(Data%NVar, Data%NDim) :: nu_shock
    !---------------------------------------------------

    REAL, DIMENSION(:,:,:), POINTER :: D_phi_k
    REAL, DIMENSION(:,:),   POINTER :: D_phi_q
    REAL, DIMENSION(:),     POINTER :: phi_q
    REAL, DIMENSION(:),     POINTER :: w_q
    !---------------------------------------------------- 

    REAL, DIMENSION(Data%NVar) :: R_i

    !REAL :: u_max, p_max, c_max, Dt, S_ele, h_e

    REAL :: r_N_dim, r_N_dofs, d_min_q

    INTEGER :: N_var, N_dim, N_dofs, N_quad

    INTEGER :: i, k, id, jd, iq
    !----------------------------------------------------
    
    N_var = Data%NVar
    N_dim = Data%NDim;     r_N_Dim = REAL(N_dim, 8)

    N_dofs = ele%N_points; r_N_dofs = REAL(N_dofs, 8)

    N_quad = ele%N_quad

    D_phi_k => ele%D_phi_k 
        w_q => ele%w_q

    Vp_b = Mean_state_Vp(Vp)

#ifdef NAVIER_STOKES

    ALLOCATE( KK_i (N_var,N_var, N_dim,N_dim, N_dofs) )
    ALLOCATE( KKq_i(N_var,N_var, N_dim,N_dim, N_dofs) )

    DO i = 1, N_dofs
       CALL Diffusion_Jacobian( N_dim, Vp(:,i), KK_i(:,:, :,:, i) )
    ENDDO

#endif

    Phi_i = 0.d0

    DO iq = 1, N_quad

       !-----------------------
       ! Stuff defined at the 
       ! quadrature points only
       !------------------------------------------------

       D_phi_q => ele%D_phi_q(:,:, iq)
         phi_q => ele%phi_q(:, iq)

       ! Interpolated solution
       Vp_q = Interp_Vp( Vp, phi_q )

       ! Scaling matrix
       Tau = Tau_matrix( D_phi_q, Vp_b )

       ! FEM Gradient
       D_Vc_q = 0.d0
       DO k = 1, N_dofs
          DO id = 1, N_dim
             D_Vc_q(id, :) = D_Vc_q(id, :) &
                           + D_phi_q(id, k) * Vc(:, k)
          ENDDO
       ENDDO
        
       ! Advective Jacobian 
       CALL advection_Jacobian(N_dim, Vp_q, AA_q)

       !****************************
       ! Strong form of the residual
       !************************************************

       A_GU = 0.d0
       DO id = 1, N_dim
          A_GU = A_GU &
               + MATMUL( AA_q(:,:, id), D_Vc_q(id, :) )
       ENDDO

       ! Inviscid residual
       Res = A_GU

#ifdef NAVIER_STOKES

       ! Reconstructed Gradient
       Dr_Vc_q = 0.d0
       DO i = 1, N_dofs
          DO id = 1, N_dim
             Dr_Vc_q(id, :) = Dr_Vc_q(id, :) + &
                              phi_q(i) * Dr_Vc(id, :, i)
          ENDDO
       ENDDO

       ! Viscous flux & Jacobian
       CALL Diffusion_flux_function(Vp_q, D_Vc_q, ff_v)

       CALL Diffusion_Jacobian(N_dim, Vp_q, KK_q)

       DO i = 1, N_dofs
          DO id = 1, N_dim
             KKq_i(:,:, id, :, i) = KK_i(:,:, id, :, i) &
                                  * D_phi_q(id, i)
          ENDDO
       ENDDO

       Div_Fv = 0.d0
       DO id = 1, N_dim

          DO i = 1, N_dofs

             DO jd = 1, N_dim
                Div_Fv = Div_Fv &
                       +  MATMUL( KKq_i(:,:, id, jd, i), &
                                  Dr_Vc(jd, :, i) )
             ENDDO

          ENDDO

       ENDDO
       
       ! Diffusion  residual 
       Res = Res - Div_fv
       
       ! Turbulent source term
       IF( Turbulent ) THEN 

          d_min_q = SUM(ele%d_min * phi_q)
          CALL Source_term(Vp_q, D_Vc_q, d_min_q, S_q)

          ! Source  residual
          Res = Res - S_q
          
       ENDIF

#endif

       ! Artuficial viscosity
       nu_shock = Shock_viscosity(ele, Vp_q, D_Vc_q, Res)

       !--------------------------------------------------
       !--------------------------------------------------
       !--------------------------------------------------

       ! For each DOF i...
       DO i = 1, N_dofs

          !*******************
          ! Galerkin Residual 
          !************************************************

          ! Inviscid
          R_i = phi_q(i) * A_GU

#ifdef NAVIER_STOKES

          ! Viscous I.b.P.
          DO id = 1, N_dim
             R_i = R_i &
                 + D_phi_q(id, i) * ff_v(:, id)
          ENDDO

          ! FOS Stabilization
          DO id = 1, N_dim
             
             KK_Gphi_i = 0.d0
             DO jd = 1, N_dim
                KK_Gphi_i = KK_Gphi_i & 
                          + KK_q(:,:, id, jd) * D_phi_q(jd, i)
             ENDDO
             
             R_i = R_i &
                 + MATMUL( KK_Gphi_i, &
                           D_Vc_q(id, :) - Dr_Vc_q(id, :) )

          ENDDO

          ! Turbulent source term
          IF( Turbulent ) THEN 
             R_i = R_i - phi_q(i) * S_q
          ENDIF

#endif

          !*******************
          ! Stabilization term
          !************************************************

          A_Gphi_i = 0.d0
          DO id = 1, N_dim
             A_Gphi_i = A_Gphi_i &
                      + AA_q(:,:, id) * D_phi_q(id, i)
          ENDDO

          
          R_i = R_i &
              + MATMUL( A_Gphi_i, &
                        MATMUL(Tau, Res) )

          !*******************
          ! Shock capturing term
          !************************************************

          DO id = 1, N_dim
             R_i = R_i & 
                 + nu_shock(:, id) &
                   * D_Vc_q(id, :) * D_phi_q(id, i)
          ENDDO

          ! Perform the integration
          Phi_i(:, i) = Phi_i(:, i) & 
                      + R_i * w_q(iq)

       ENDDO ! i = 1 ... N_dofs     

       NULLIFY( D_phi_q, phi_q )

    ENDDO ! i = 1 ... N_quad

    NULLIFY( D_phi_k, w_q )

!!$     !-----------------------
!!$     ! Time stepping ~ |V|/Dt
!!$     !-------------------------------------------
!!$     u_max = 0.d0; c_max = 0.d0
!!$     DO i = 1, N_dofs
!!$        u_max = MAX( u_max, SQRT(SUM(Vp(i_vi1_vp:i_vid_vp, i)**2)) )
!!$        c_max = MAX( c_max, Vp(i_son_vp, i) )
!!$     ENDDO
!!$
!!$     p_max = u_max + c_max
!!$
!!$     ! Element surface
!!$     S_ele = 0.d0
!!$     DO i = 1, ele%N_faces
!!$        S_ele = S_ele + SUM(ele%faces(i)%f%w_q)
!!$     ENDDO
!!$
!!$     h_e = r_N_dim * ele%Volume/S_ele
!!$
!!$#ifdef NAVIER_STOKES
!!$     Dt = h_e / ( p_max + alpha_visc(ele, Vp) )
!!$#else
!!$     Dt = h_e /   p_max
!!$#endif
!!$
!!$     Dt = CFL * Dt
!!$
!!$     inv_dt = (ele%Volume/r_N_dofs)/Dt
!!$     !-------------------------------------------

  END SUBROUTINE SUPG_scheme_exp
  !=============================



  !=============================================
  FUNCTION Tau_matrix(D_phi_q, Vp_q) RESULT(Tau)
  !=============================================

    IMPLICIT NONE
     
    REAL, DIMENSION(:,:), INTENT(IN) :: D_phi_q
    REAL, DIMENSION(:),   INTENT(IN) :: Vp_q

    REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau
    !-------------------------------------------

    REAL, DIMENSION(Data%NVar, Data%NVar, Data%NDim, Data%Ndim) :: KK

    REAL, DIMENSION(Data%NVar, Data%NVar) :: Tau_
    REAL, DIMENSION(Data%NVar, Data%NVar) :: RR, LL, Pos
    REAL, DIMENSION(Data%NVar) :: lambda
    
    REAL, DIMENSION(Data%NDim) :: v_nn

    REAL :: mod_n, p

    INTEGER :: i, j, id, jd, N_dofs, N_Var, N_dim
    !--------------------------------------------
    
    Tau_ = 0.d0

    N_dofs = SIZE(D_phi_q, 2)
    N_Var = Data%NVar
    N_dim = Data%NDim

    p = REAL(Data%NOrdre - 1.d0, 8)

    DO i = 1, N_dofs

       v_nn = D_Phi_q(:, i)
        
       mod_n = SQRT( SUM(v_nn**2) )

       v_nn = ( v_nn /  mod_n )

       CALL Advection_eigenvalues( Vp_q, v_nn, lambda)
       CALL Advection_eigenvectors(Vp_q, v_nn, RR, LL)

       Pos = 0.d0
       DO j = 1, N_var
          Pos(j,j) = ABS(lambda(j)) * mod_n

          ! Entropy fix-like correction.
          ! The matrix is ill conditioned
          ! when only 1 eigenval. is /= 0
          !Pos(j,j) = MAX( Pos(j,j), 0.01d0 )
       ENDDO
        
       Tau_ = Tau_ +  MATMUL(RR, MATMUL(Pos, LL))

       v_nn = v_nn * mod_n !* p

#ifdef NAVIER_STOKES
       CALL Diffusion_Jacobian(N_dim, Vp_q, KK)

       DO id = 1, N_dim
          DO jd = 1, N_dim
             !Tau_ = Tau_ + v_nn(id) * KK(:,:, id, jd) * v_nn(jd)
             Tau_ = Tau_ + v_nn(id) * KK(:,:, id, jd) * v_nn(jd) * p**2 
          ENDDO
       ENDDO
#endif
       
    ENDDO

    Tau = InverseLU(Tau_)

  END FUNCTION Tau_matrix
  !======================
  
  !=======================================================
  FUNCTION Shock_viscosity(ele, Vp, D_Vc, R) RESULT(nu_sc)
  !=======================================================

    IMPLICIT NONE

    TYPE(element_str),    INTENT(IN) :: ele
    REAL, DIMENSION(:),   INTENT(IN) :: Vp
    REAL, DIMENSION(:,:), INTENT(IN) :: D_Vc
    REAL, DIMENSION(:),   INTENT(IN) :: R

    REAL, DIMENSION(Data%NVar, Data%NDim) :: nu_sc
    !----------------------------------------------

    REAL, DIMENSION(Data%NVar) :: dP_dVc
    
    REAL, DIMENSION(Data%NDim) :: s, h, D_P 
     
    REAL :: dP_drho, dP_dEt, R_P, mod_DP, f_p
    REAL :: r_N_dim, den, P_s, nu
    
    INTEGER :: i, id, jf, N_var, N_dim

    REAL, PARAMETER :: C_e = 0.2d0
    !------------------------------------------------

    N_dim = Data%NDim; r_N_dim = REAL(N_dim, 8)
    N_var = Data%NVar
    
    dP_drho = EOS_d_PI_rho(Vp)
    dP_dEt  = EOS_d_PI_E(Vp) 

    dP_dVc = 0.d0
    dP_dVc(i_rho_ua)          =  dP_drho
    dP_dVc(i_rv1_ua:i_rvd_ua) = -dP_dEt * Vp(i_vi1_vp:i_vid_vp)
    dP_dVc(i_ren_ua)          =  dP_dEt
    
    R_p = DOT_PRODUCT(dP_dVc , R)/Vp(i_pre_vp)

    ! Element lenght
    P_s = 1.d0
    DO id = 1, N_dim

       den = 0.d0
       DO jf = 1, ele%N_faces
          den = den + SUM( ABS(ele%faces(jf)%f%n_q(id, :)) * &
                      ele%faces(jf)%f%w_q )
       ENDDO

       s(id) = 2.d0*ele%volume/den
        
       P_s = P_s * s(id)

    ENDDO

    DO id = 1, N_dim

       h(id) = s(id) * (ele%volume/P_s)**(1.d0/r_N_dim)
       h(id) = h(id) / Data%NOrdre
        
       D_P(id) = DOT_PRODUCT(dP_dVc, D_Vc(id, :))

    ENDDO
     
    mod_DP = SQRT( SUM(D_P**2) )

    nu_sc = 0.d0

    DO id = 1, N_dim

       f_p = h(id) * mod_DP / ( Vp(i_pre_vp) + 1.0E-12 )

       nu = C_e * (h(id)**2) * f_p * ABS(R_p)

       ! No contribution to the turbulent equation
       nu_sc(i_rho_ua:i_ren_ua, id) = nu
        
    ENDDO

  END FUNCTION Shock_viscosity
  !===========================
  
END MODULE Scheme_SUPG
