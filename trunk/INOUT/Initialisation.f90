MODULE Initialisation

      USE LesTypes
      USE EOSLaws
      USE Ringleb_flow
      USE MMS
      USE Reprise
      USE ModelInterfaces
      USE Gradient_Recovery
      USE LibComm
      
      IMPLICIT NONE

!      REAL(KIND=8), PARAMETER :: PI = DACOS(-1.d0)

   CONTAINS

     !====================================
     SUBROUTINE InitFields(Com, Data, Var)
     !====================================

       TYPE(donnees),   INTENT(INOUT) :: Data
       TYPE(MeshCom),   INTENT(INOUT) :: Com
       TYPE(Variables), INTENT(INOUT) :: Var
       !--------------------------------------

       INTEGER :: Icas, Nvar, Ns, Ndim
       INTEGER, DIMENSION(: ), POINTER :: logcell
       INTEGER ::j, is
       CHARACTER(LEN = 20) :: chaine
       !--------------------------------------
       REAL(KIND=8) :: PI

       PI = DACOS(-1.d0)

       Ns   = Var%NCells
       Nvar = data%Nvar
       Ndim = data%Ndim
       Icas = data%Icas

       CALL encoder_data_icas(icas, chaine)
       IF (Data%Impre > 0) THEN
          PRINT *, " icas==", TRIM(chaine), " reprise==", Data%Reprise, " iflux==", Data%iflux
       END IF

       IF (Data%Reprise) THEN

          CALL Reprise_in(Com, Data, Var, Data%PbName)
       ELSE

          Var%kt         = 0
          Data%kt0       = 0
          Var%Temps      = 0.0
          Var%Residus    = 1.0
          Data%Residus0  = 0.0
          Var%Res_oo     = 1.0
          Data%Res_oo0   = 0.0
          Var%CFL        = 0.0
          Var%Resid_1    = 1.0           
          Var%Resid_2    = 1.0         
          
         CALL Model_Init_Sol (icas, DATA, Var)  
!!$          SELECT CASE(Icas)
!!$
!!$          !---------------------
!!$          CASE(cs_icas_uniforme)
!!$          !---------------------
!!$ 
!!$             CALL CAS_Uniforme(Data, Var)
!!$             
!!$          !--------------------
!!$          CASE(cs_icas_ringleb)
!!$          !--------------------
!!$
!!$             CALL CAS_Ringleb(Data, Var)
!!$
!!$          !---------------
!!$          CASE(cs_icas_MS)
!!$          !---------------
!!$
!!$             CALL CAS_MS(Data, Var)
!!$
!!$          !--------------------
!!$          CASE(cs_icas_supvort)
!!$          !--------------------
!!$
!!$             CALL CAS_SupersonicVortex(Data, Var)
!!$
!!$          CASE DEFAULT
!!$
!!$             WRITE(*,*) 'ERROR: unknown initilization type'
!!$             CALL stop_run()
!!$
!!$          END SELECT

       END IF 

       CALL Model_Init_boundaries(Var, Data)

       DO j = 1, Var%Ncells
          CALL ModelVp2u( Var%Vp(:, j), Var%Ua(:, j) )
       ENDDO


#ifndef SEQUENTIEL

       CALL EchangeSol( com, Var%Ua )


       DO is = Var%NcellsIn + 1, Var%Ncells

          CALL ModelU2VpPt(Var%Ua(:, is), Var%Vp(:, is) )

       END DO

#endif

#ifdef NAVIER_STOKES
    CALL Grad_Recovery(Com, Var)
!CALL Grad_Recovery(Var)
#endif     

       Var%Flux = 0.0
       
       WRITE(*,*) 'Solution initialized...'

     END SUBROUTINE InitFields
     !========================

!--------------------------------------------------------------------------------------------
!--------------------------------------------------------------------------------------------
!--------------------------------------------------------------------------------------------
!--------------------------------------------------------------------------------------------

   END MODULE Initialisation
