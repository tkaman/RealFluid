!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   MODULE Inputs
      USE types_enumeres
      USE LesTypes
      IMPLICIT NONE

   CONTAINS

!> r�le : Lecture du d�but (4 premi�res sections) du fichier de donn�es "NumMeth_" // <rootname>.data
!!-- La structure qui est remplie est Data.
!!-- l'unit� de lecture 12 MyUnit est ouverte mais non ferm�e, il y a une suite de lecture dans ModelInterfaces.f90/ModelDonData
!!-- Apr�s la lecture, on initialise des valeurs d�riv�es, en fonction du num�ro de t�che en particulier
     SUBROUTINE DonNumMeth(com, DATA)
       CHARACTER(LEN = *), PARAMETER :: mod_name = "DonNumMeth"
       ! Type et vocations des Variables d'appel
       !----------------------------------------
       TYPE(Donnees), INTENT(INOUT)    :: DATA
       TYPE(MeshCom), INTENT(IN)       :: com
       ! Variables locales scalaires
       !----------------------------
       INTEGER             :: MyUnit, Len
       CHARACTER(LEN = 70)   :: FileName
       LOGICAL             :: existe
       INTEGER             :: istat, iligne, ios
       CHARACTER(LEN = 50)   :: chaine
       REAL                :: c__ResdLin, c__Tmax, c__dtMax !-- [KADNA-PROTECTED]
       REAL                :: c__CFLmax, c__Mult, c__CFL_phys, c__resdSta !-- [KADNA-PROTECTED]
       !--------------------------------------------------------------------
       !                Partie ex�cutable de la proc�dure
       !--------------------------------------------------------------------
       MyUnit = 12
       FileName(1: ) = "NumMeth_" // TRIM(data%rootname) // ".data"
       IF ( Data%Impre > 0) THEN
          WRITE(6, '(" * Fichier de donnees:")', ADVANCE = 'no')
          WRITE(6, *) TRIM(FileName)
       END IF
       INQUIRE(FILE = FileName, EXIST = existe)
       IF ( Data%Impre > 0) THEN
          CALL system("echo 'pwd'; pwd")
       END IF
       IF (.NOT. existe) THEN
          IF ( Data%Impre > 0) THEN
             CALL system("ls -l")
             CALL system("ls -l " // TRIM(FileName))
          END IF
          PRINT *, mod_name, " :: ERREUR : pas de fichier ", TRIM(FileName), Com%Me
          CALL delegate_stop()
       END IF
       OPEN(UNIT = MyUnit, FILE = FileName, FORM = "formatted", STATUS = "old", IOSTAT = ios )
       IF (ios /= 0) THEN
          IF ( Data%Impre > 0) THEN
             CALL system("ls -l")
             CALL system("ls -l " // TRIM(FileName))
             PRINT *, mod_name, " :: ERREUR : impossible d'ouvrir ", TRIM(FileName), Com%Me, ios
          END IF
          CALL delegate_stop()
       END IF

       iligne = 2 !-- ou 1, approximativement %%%%
       READ(MyUnit, *, ERR = 999, END = 999) data%Geotype, data%MeshType, data%MeshOrder
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " Data%MeshType  = ", TRIM(data%MeshType)
          PRINT *, mod_name, " Data%MeshOrder = ", data%MeshOrder
       END IF
       CALL decoder_data_geotype(data%Geotype, data%sel_GeoType)
       CALL decoder_data_meshtype(data%MeshType, data%sel_MeshType)

       !Lines to change for frozing the dimension

       IF (data%sel_GeoType == cs_geotype_1d) THEN
          data%Ndim = 1
       ELSE IF (data%sel_GeoType == cs_geotype_3d) THEN
          data%Ndim = 3
       ELSE
          data%Ndim = 2
       END IF
       SELECT CASE(Data%Ndim)
       CASE(1)
          IF (Data%sel_geoType.NE.cs_geotype_1d) THEN
             PRINT*, mod_name, "This is not the right mesh dimension"
             PRINT*, "here mesh is 1D and Data%Ndim= ", Data%Ndim
             PRINT*, "See LTYPES/LesTypes.f90, Ndim in Variables structure"
             STOP
          ENDIF
       CASE(3)
          IF (data%sel_GeoType .NE. cs_geotype_3d) THEN
             PRINT*, mod_name, "This is not the right mesh dimension"
             PRINT*, "here mesh is 3D and Data%Ndim= ", Data%Ndim
             PRINT*, "See LTYPES/LesTypes.f90, Ndim in Variables structure"
             STOP
          ENDIF
       CASE(2)
          IF (data%sel_GeoType .NE. cs_geotype_2dplan) THEN
             PRINT*, mod_name, "This is not the right mesh dimension"
             PRINT*, "here mesh is 2D and Data%Ndim= ", Data%Ndim
             PRINT*, "See LTYPES/LesTypes.f90, Ndim in Variables structure"
             STOP
          ENDIF
       CASE default
          PRINT*, mod_name, "This is not the right mesh dimension"
          PRINT*, "here mesh has unknown dimension= ", Data%Ndim
          PRINT*, "data%sel_GeoType =", data%sel_GeoType 
          PRINT*, "See LTYPES/LesTypes.f90, Ndim in Variables structure"
          STOP
       END SELECT

       iligne = 3
       READ(MyUnit, *, ERR = 999, END = 999)
       iligne = 4
#ifdef MHD_GP
#ifdef MHD
       READ(MyUnit, *, ERR = 998, END = 998) data%Stationnaire, data%Reprise, data%Deformable !-- [KADNA-PROTECTED]
#else
       READ(MyUnit, *, ERR = 998, END = 998) data%Stationnaire, data%Reprise, data%Deformable, data%Bz_en_2d, & !-- [KADNA-PROTECTED]
            & data%div_cleaning !-- [KADNA-PROTECTED]
       IF (data%Ndim /= 2) THEN
          data%Bz_en_2d = .FALSE.
       END IF
#endif
#else
       READ(MyUnit, *, ERR = 998, END = 998) data%Stationnaire, data%Reprise, data%Deformable !-- [KADNA-PROTECTED]
#endif
       iligne = 5
       READ(MyUnit, *, ERR = 998, END = 998) data%Modele(1: )!-- [KADNA-PROTECTED]
       iligne = 6
       READ(MyUnit, *, ERR = 998, END = 998)
       iligne = 7
       READ(MyUnit, *, ERR = 997, END = 997) data%Approche, data%Methode, data%Nnewton !-- [KADNA-PROTECTED]
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " Approche==", TRIM(data%Approche)
          PRINT *, mod_name, " Nnewton==", data%Nnewton
       END IF
       iligne = 8
       READ(MyUnit, *, ERR = 997, END = 997) data%Relax,   data%Nrelax,  c__ResdLin, data%Mkrylov !-- [KADNA-PROTECTED]
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " Relax==", data%Relax
       END IF
       data%ResdLin = c__ResdLin
       iligne = 9
       READ(MyUnit, *, ERR = 997, END = 997) data%Iflux,   data%Nordre,  data%Ipred,   data%RGrad !-- [KADNA-PROTECTED]
       PRINT *, mod_name, " Solution Order = ",  data%Nordre
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " Iflux==", data%Iflux
       END IF
!!$       IF (data%Nordre > 1 .AND. data%Ilimt == 0 ) THEN
!!$          data%Ilimt = 1 ! Minmod par d�faut !!!!!
!!$       END IF
       iligne = 10
       READ(MyUnit, *, ERR = 997, END = 997) c__Tmax,    c__dtMax,   data%ktMax !-- [KADNA-PROTECTED]
       data%Tmax = c__Tmax
       data%dtMax = c__dtMax
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " Tmax==", data%Tmax
          PRINT *, "ktmax==", data%ktMax
       END IF

       iligne = 11
       ! essaie de lire data%loc, data%CFL_phys
       READ(MyUnit, FMT = '(A)', IOSTAT = istat) chaine
       IF ( Data%Impre > 0) THEN
          PRINT *, "chaine/1==", TRIM(chaine), istat
       END IF
       IF (istat /= 0) THEN
          GO TO 9970
       END IF
       READ(chaine, FMT = *, IOSTAT = istat) c__CFLmax,  data%LoiCFl,  c__Mult, data%loc, c__CFL_phys !-- [KADNA-PROTECTED]
       IF (istat /= 0) THEN
          READ(chaine, FMT = *, IOSTAT = istat) c__CFLmax,  data%LoiCFl,  c__Mult !-- [KADNA-PROTECTED]
          IF (istat /= 0) THEN
             IF ( Data%Impre > 0) THEN
                PRINT *, "chaine==", TRIM(chaine)
             END IF
             GO TO 9970
          END IF
          data%CFLmax = c__CFLmax
          data%mult = c__Mult
          data%loc = .FALSE.
          data%CFL_Phys = 0.5
          WRITE(message_log, FMT = *) "ATTENTION : Valeurs par defaut pour Data%loc, Data%CFL_Phys", data%loc, data%CFL_Phys
          CALL log_record(message_log)
       ELSE
          data%CFLmax = c__CFLmax
          data%mult = c__Mult
          data%CFL_Phys = c__CFL_phys
       END IF
       IF ( Data%Impre > 0) THEN
          PRINT *, "CFLmax==", data%CFLmax
       END IF
       iligne = 12
       READ(MyUnit, *, ERR = 997, END = 997) c__resdSta !-- [KADNA-PROTECTED]
       data%resdSta = c__resdSta
       iligne = 13
       READ(MyUnit, *, ERR = 997, END = 997)
       iligne = 14
       READ(MyUnit, *, ERR = 995, END = 995) data%Impre,   data%Icas !-- [KADNA-PROTECTED]
       iligne = 15
       READ(MyUnit, *, ERR = 995, END = 995) data%Ifres,   data%Ifre,   data%Ifrel  !-- [KADNA-PROTECTED]
       iligne = 16
       !-- la suite de la lecture de NumMeth...data se poursuit dans ModelDonData

       IF (data%Impre > 1) THEN
          PRINT *, "Data%Modele ", data%Modele
          PRINT *, "Data%Relax ", data%Relax
       END IF

       !    CLOSE(MyUnit) ! sera ferm� dans DonDataModel().....

       data%PbName(1: ) = TRIM(data%rootname) // "-"
       Len = INDEX(data%PbName, " ")
       IF (com%Ntasks == 1) THEN
          WRITE(data%PbName(Len: Len + 2), "(i3.3)")0
       ELSE
          WRITE(data%PbName(Len: Len + 2), "(i3.3)") com%Me+ 1
       END IF

       data%OutputName = data%rootname

       IF (data%Methode       == "Implicite ") THEN !-- %%%%%%%%%%%%%%%%%%%%%%% Cela n'existe pas ou plus 2007/10/22
          data%Ipred = 0
       END IF
       IF (data%Methode       == "Explicite " .AND. data%Nordre == 2 ) THEN !-- %%%%%%%%%%%%%%%%%%%%%%% Cela n'existe pas ou plus 2007/10/22
          data%Ipred = 1
       END IF

       CALL decoder_data_methode(data%Methode, data%sel_Methode)
       CALL decoder_data_modele(data%Modele, data%sel_Modele)
       CALL decoder_data_approche(data%Approche, data%sel_Approche)
       CALL decoder_data_relax(data%Relax, data%sel_Relax)

#ifdef INSTAT_RD
#ifdef ORDRE_ELEVE
       IF (data%sel_Methode==cs_methode_exp_rk1_rd.AND.(data%Iflux /= cs_iflux_6 .AND. data%Iflux /= cs_iflux_7 .AND. data%Iflux /= cs_iflux_9) ) THEN
          PRINT *, mod_name, " Ordre eleve : ERREUR : Residus + Instat => Data%Iflux = 6, 7 ou 9 svp, pas ", &
               & data%Iflux, data%Methode,data%sel_Methode
          CALL delegate_stop()
       END IF
#else
       IF (data%sel_Methode==cs_methode_exp_rk1_rd.AND.(data%Iflux /= cs_iflux_6 .AND. data%Iflux /= cs_iflux_7 .AND. data%Iflux /= cs_iflux_3) ) THEN
          PRINT *, mod_name, " EulNSRD : ERREUR : Residus + Instat => Data%Iflux = 3, 6 ou 7 svp, pas ", data%Iflux, data%Methode,data%sel_Methode
          CALL delegate_stop()
       END IF
#endif
#endif

       RETURN !-- OK
999    CONTINUE       
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " ERREUR DANS ", TRIM(FileName),  ". LA SECTION 1 DOIT CONTENIR:"
          PRINT *, "Data%Geotype, Data%MeshType, Data%MeshOrder"
          PRINT *, " "
       END IF
       CALL delegate_stop()
998    CONTINUE
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " ERREUR DANS ", TRIM(FileName),  ". LA SECTION 2 DOIT CONTENIR:"
          PRINT *, "Data%Stationnaire, Data%Reprise,       Data%Deformable [Data%BZ_en_2D si MHD]"
          PRINT *, "Data%Modele "
          PRINT *, " "
       END IF
       CALL delegate_stop()
997    CONTINUE
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " ERREUR DANS ", TRIM(FileName),  ". LA SECTION 3 DOIT CONTENIR: iligne==", iligne
          PRINT *, "Data%Approche, Data%Methode"
          PRINT *, "Data%Relax,   Data%Nrelax,  Data%ResdLin, Data%Mkrylov"
          PRINT *, "Data%Iflux,   Data%Nordre,  Data%Ipred,   Data%RGrad"
          PRINT *, "Data%Tmax,    Data%dtMax,   Data%ktmax"
          PRINT *, "Data%CFLmax,  Data%LoiCFL,  Data%Mult"
          PRINT *, "Data%ResdSta"
          PRINT *, " "
       END IF
       CALL delegate_stop()
9970   CONTINUE
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " ERREUR DANS ", TRIM(FileName),  ". LA SECTION 3++ DOIT CONTENIR:"
          PRINT *, "Data%Approche, Data%Methode"
          PRINT *, "Data%Relax,   Data%Nrelax,  Data%ResdLin, Data%Mkrylov"
          PRINT *, "Data%Iflux,   Data%Nordre,  Data%Ipred,   Data%RGrad"
          PRINT *, "Data%Tmax,    Data%dtMax,   Data%ktmax"
          PRINT *, "Data%CFLmax,  Data%LoiCFL,  Data%Mult, Data%Loc, Data%CFL_phys"
          PRINT *, "Data%ResdSta"
          PRINT *, " "
       END IF
       CALL delegate_stop()
995    CONTINUE
       IF ( Data%Impre > 0) THEN
          PRINT *, mod_name, " ERREUR DANS ", TRIM(FileName),  ". LA SECTION 4 DOIT CONTENIR:"
          PRINT *, "Data%Impre,   Data%Icas"
          PRINT *, "Data%Ifres,   Data%Ifre,   Data%Ifrel"
          PRINT *, "PB"
       END IF
       CALL delegate_stop()
     END SUBROUTINE DonNumMeth

   END MODULE Inputs
