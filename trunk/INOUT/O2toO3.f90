MODULE O2toO3

  USE LesTypes
  USE LibMesh
  USE EOSLaws

!  USE Advection,         ONLY: Prim_to_Cons, Cons_to_Prim
!  USE Turbulence_models, ONLY: Turbulent

  IMPLICIT NONE

CONTAINS

  !==================================================
  SUBROUTINE interp_O2_to_O3(Unit, N_DOF, N_ELE, Var)
  !==================================================
  !
  ! Interpolate the P1 solution on the P2 mesh
  ! NOTE: the grid must be the same and 
  !       in parallel it is assumed that the 
  !       partioning it's the same
  !
    IMPLICIT NONE

    INTEGER,         INTENT(IN)    :: Unit
    INTEGER,         INTENT(IN)    :: N_DOF
    INTEGER,         INTENT(IN)    :: N_ELE
    TYPE(Variables), INTENT(INOUT) :: Var
    !--------------------------------------

    TYPE(element_str) :: loc_ele

    REAL, DIMENSION(:,:), ALLOCATABLE :: V0_Vp
    REAL, DIMENSION(:,:), ALLOCATABLE :: V0_Vc

    TYPE cnt
       INTEGER, DIMENSION(:), ALLOCATABLE :: Nu
    END TYPE cnt
    TYPE(cnt), DIMENSION(:), ALLOCATABLE :: Map_P1P2
    !-----------------------------------------------
    
    REAL :: dummy
    INTEGER :: i_dummy

    INTEGER :: ies, is, je, j_f, i, j, N_P
    INTEGER :: NU, NU_1, NU_2, NU_3, &
                   NU_4, NU_5, NU_6
    INTEGER :: NU_P1, NU_P2 
    
    INTEGER :: ios
    !------------------------------------

!!$    Var%kt         = 0
!!$    Data%kt0       = 0
!!$    Var%Temps      = 0.0
!!$    Var%Residus    = 1.0
!!$    Data%Residus0  = 0.0
    Var%Res_oo     = 1.0
    Data%Res_oo0   = 0.0
    Var%CFL        = 1.0
!!$    Var%Resid_1    = 1.0
!!$    Var%Resid_2    = 1.0

    Data%Residus0  = 1.0
    Var%kt = Var%kt + Data%Ifres
    Data%kt0 = Var%kt

    ALLOCATE( Map_P1P2(N_ELE) )

    DO je = 1, N_ELE

       READ(Unit) i_dummy, & ! Mesh%Maill(jt)%Ndegre
                  N_P,     & ! Mesh%Maill(jt)%Nsommets
                  i_dummy    ! Mesh%Maill(jt)%EltType

       READ(Unit) ( (dummy,             & ! Mesh%Maill(jt)%coor(i,j)
                     i = 1, Data%Ndim), &
                     j = 1, N_P )  

       ALLOCATE( Map_P1P2(je)%Nu(N_P) )

       READ(Unit) ( Map_P1P2(je)%Nu(i), & ! Mesh%Maill(jt)%Nu(i)
                    i = 1, N_P )

    ENDDO

    ALLOCATE( V0_Vp(Data%NvarPhy, N_DOF) )

    READ(Unit) ( (V0_Vp(ies, is),         &
                  ies = 1, Data%NvarPhy), &
                   is = 1, N_DOF          )
 
    DO je = 1, SIZE(d_element)

       !loc_ele = d_element(je)%e

       ALLOCATE( V0_Vc( Data%Nvar, d_element(je)%e%N_points) )

       V0_Vc = 0.d0

       DO i = 1, d_element(je)%e%N_verts

          Nu_P2 = d_element(je)%e%NU(i)
          Nu_P1 = Map_P1P2(je)%Nu(i)

          CALL Prim2Cons( V0_Vp(:, NU_P1), V0_Vc(:, i) )
          
       ENDDO

       SELECT CASE(d_element(je)%e%Type)

       !-------------------------
       CASE(TRI_P2)
       !-------------------------

          DO j_f = 1, d_element(je)%e%N_faces

             NU_1 = d_element(je)%e%faces(j_f)%f%l_nu(1)
             NU_2 = d_element(je)%e%faces(j_f)%f%l_nu(2)
             NU_3 = d_element(je)%e%faces(j_f)%f%l_nu(3)

             V0_Vc(:, NU_3) = 0.5d0*( V0_Vc(:, NU_1) + V0_Vc(:, NU_2) )

          ENDDO 
          
       !-------------------------
       CASE(QUA_Q2)
       !-------------------------

          DO j_f = 1, d_element(je)%e%N_faces

             NU_1 = d_element(je)%e%faces(j_f)%f%l_nu(1)
             NU_2 = d_element(je)%e%faces(j_f)%f%l_nu(2)
             NU_3 = d_element(je)%e%faces(j_f)%f%l_nu(3)

             V0_Vc(:, NU_3) = 0.5d0*( V0_Vc(:, NU_1) + V0_Vc(:, NU_2) )

          ENDDO

          DO i = 1, Data%NVar
             V0_Vc(i, 9) = SUM( V0_Vc(i, 1:4) ) / 4.d0
          ENDDO

       !-------------------------
       CASE(TET_P2)
       !-------------------------

          DO j_f = 1, d_element(je)%e%N_faces

             NU_1 = d_element(je)%e%faces(j_f)%f%l_nu(1)
             NU_2 = d_element(je)%e%faces(j_f)%f%l_nu(2)
             NU_3 = d_element(je)%e%faces(j_f)%f%l_nu(3)
             NU_4 = d_element(je)%e%faces(j_f)%f%l_nu(4)
             NU_5 = d_element(je)%e%faces(j_f)%f%l_nu(5)
             NU_6 = d_element(je)%e%faces(j_f)%f%l_nu(6)

             V0_Vc(:, NU_4) = 0.5d0*( V0_Vc(:, NU_1) +  V0_Vc(:, NU_2) )
             V0_Vc(:, NU_5) = 0.5d0*( V0_Vc(:, NU_2) +  V0_Vc(:, NU_3) )
             V0_Vc(:, NU_6) = 0.5d0*( V0_Vc(:, NU_3) +  V0_Vc(:, NU_1) )

          ENDDO 

       CASE DEFAULT

          WRITE(*,*) 'ERROR: wrong element type'
          STOP

       END SELECT

       DO i = 1, d_element(je)%e%N_points

!#ifdef NAVIER_STOKES
!    IF(turbulent) THEN
!       V0_Vc(i_muts_ua, i) = MAX(V0_Vc(i_muts_ua, i), 0.d0)
!    ENDIF
!#endif
          CALL Cons2Prim( V0_Vc(:, i), Var%Vp(:, d_element(je)%e%NU(i)) )

       ENDDO

       DEALLOCATE( V0_Vc )
              
    ENDDO
  
    DEALLOCATE(V0_Vp, Map_P1P2)

  END SUBROUTINE interp_O2_to_O3
  !=============================  
  
  !=============================
  SUBROUTINE Cons2Prim(Vc, Vp)
  !=============================

    IMPLICIT NONE

     REAL, DIMENSION(:), INTENT(IN)  :: Vc
     REAL, DIMENSION(:), INTENT(OUT) :: Vp
     !------------------------------------
     REAL :: rho
     !------------------------------------

     Vp = 0.d0

     rho = Vc(i_rho_ua)
  
     Vp(1:SIZE(Vc)) = Vc / rho

     Vp(i_rho_vp) = rho

     Vp(i_eps_vp) =  Vp(i_eps_vp) - 0.5d0*SUM(Vp(i_vi1_vp:i_vid_vp)**2)
     
     CALL EOSEos(Vp)
     
  END SUBROUTINE Cons2Prim
  !=======================
  
  !===========================
  SUBROUTINE Prim2Cons(Vp, Vc)
  !===========================

    IMPLICIT NONE

     REAL, DIMENSION(:), INTENT(IN)  :: Vp
     REAL, DIMENSION(:), INTENT(OUT) :: Vc
     !------------------------------------
     REAL :: rho
     !------------------------------------
     
     rho = Vp(i_rho_vp)

     Vc = Vp(1:SIZE(Vc))*rho

     Vc(i_rho_ua) = rho

     Vc(i_ren_ua) = Vc(i_ren_ua) + 0.5d0*rho*SUM(Vp(i_vi1_vp:i_vid_vp)**2)
          
   END SUBROUTINE Prim2Cons   
   !=======================

END MODULE O2toO3
