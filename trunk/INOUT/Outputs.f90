MODULE Outputs

  USE LesTypes
  USE Reprise
  USE Visu
  USE LibComm
  USE EOSLaws,    ONLY: EOSKappa
  USE Compute_forces
  USE PostPro_turbine

!use visc_laws
!use gradient_recovery
!use MMS

  IMPLICIT NONE
  
CONTAINS
   
  !==================================
  SUBROUTINE Ecriture(DATA, Var, Com)
  !==================================
    
    IMPLICIT NONE

     CHARACTER(LEN = *), PARAMETER  :: mod_name = "Ecriture"

     TYPE(Donnees),   INTENT(IN)    :: DATA
     TYPE(Variables), INTENT(INOUT) :: Var
     TYPE(MeshCom),   INTENT(INOUT) :: Com
     !--------------------------------------

     CHARACTER(LEN = 70) :: BackUpFile
     CHARACTER(LEN = 10) :: Num
     INTEGER :: lensd, Len, lennum
     !---------------------------------------

     Len = 6

     BackUpFile(1: ) = TRIM(ADJUSTL(data%PbName)) // "-"
     lensd = INDEX(BackUpFile, " ")

     IF (data%ifres == 0 .OR. data%ifres > 10000000) THEN
        PRINT *, mod_name, " ERREUR : data%ifres est nul ou invalide", data%ifres
        CALL delegate_stop()
     END IF

     WRITE(Num, "(I10)")Var%kt / data%ifres

     Num = ADJUSTL(Num)

     lennum = INDEX(Num, " ") -1
     IF (lennum > Len) THEN
        PRINT *, mod_name, " ERREUR : le nombre initial d'iteration est invalide, 1000000 maximum", Var%kt, Data%ifres
        CALL delegate_stop()
     END IF

     BackUpFile(lensd: ) = REPEAT("0", Len - lennum) // TRIM(Num)

     CALL Compute_Aerodynamic_coeff(Com, Var)

CALL Efficiency_turbine(Var)
!!CALL Efficiency_turbine_XL(Var)


!     CALL Compute_Grad_err(Var)

     CALL Reprise_out(DATA, Var, BackUpFile) !*

!     CALL Compute_S_error(Data, Var)

!     CALL Compute_test_err(Com, Var);stop

   END SUBROUTINE Ecriture
   !======================

   !=====================================
   SUBROUTINE EcritureFIn(DATA, Var, Com)
   !=====================================
   !
   ! écrit un fichier suffixé par "Fin", par appel
   ! de writesol (INTERFACE de writesolmat, entre autres)
   ! appelle l'écriture des fichiers pour medit
   !  
     CHARACTER(LEN = *), PARAMETER  :: mod_name = "EcritureFIn"

     TYPE(Donnees),   INTENT(IN)    :: DATA
     TYPE(Variables), INTENT(INOUT) :: Var
     TYPE(MeshCom),   INTENT(INOUT) :: Com
     !--------------------------------------

     CHARACTER(LEN = 200) :: BackUpFile
     CHARACTER(LEN = 10)  :: Num
     INTEGER  :: lensd, Len, lennum
     !---------------------------------------

     Len = 6

     BackUpFile(1: ) = TRIM(ADJUSTL(data%PbName)) // "-"

     lensd = INDEX(BackUpFile, " ")

     WRITE(Num, "(I10)") Var%kt

     Num = ADJUSTL(Num)

     lennum = INDEX(Num, " ") -1

     BackUpFile(lensd: ) = REPEAT("0", Len - lennum) // TRIM(Num)

     CALL Compute_Aerodynamic_coeff(Com, Var)

!!CALL Efficiency_turbine(Var)

     ! écriture du point de reprise, en binaire non portable
     CALL Reprise_out(DATA, Var, BackUpFile) 

!     CALL Compute_S_error(Data, Var)

!     CALL Compute_test_err(Com, Var)
     
   END SUBROUTINE EcritureFIn
   !=========================

   !===============================
   SUBROUTINE AffichageDeDebut(Com)
   !===============================

     IMPLICIT NONE

     CHARACTER(LEN = *), PARAMETER :: mod_name = "AffichageDeDebut"

     TYPE(MeshCom), INTENT(IN) :: Com
     !-------------------------------
     CHARACTER(LEN = 8)  :: dt2     
     CHARACTER(LEN = 10) :: time
     !-------------------------------
     
     CALL DATE_AND_TIME(dt2, time)
     
     IF (Com%Me == 0 ) THEN
        WRITE(6, 100)
        IF (Com%Ntasks == 1) THEN
           WRITE(6, '("*", 19X, A10, 19X, "*")') 'Sequential'
        ELSE
           WRITE(6, '(1X, A6, I4, A21)')'*     ', Com%Ntasks, ' threads            *'
        END IF
        WRITE(6, "(A, A, A, A, A) ") "* Le ", dt2, " a ", time, "                       *"
        WRITE(6, 100)
        WRITE(6, *)
        WRITE(6, 100)
        WRITE(6, '("*", 8x, A33, 7x, "*")') 'Lecture des fichiers de démarrage'
        WRITE(6, 101)
     END IF

100  FORMAT(50("*"))
101  FORMAT("*", 48X, "*")

   END SUBROUTINE AffichageDeDebut
   !==============================

   !================================
   SUBROUTINE AffichageDeDebut2(Com)
   !================================

     IMPLICIT NONE
     
     CHARACTER(LEN = *), PARAMETER :: mod_name = "AffichageDeDebut2"

     TYPE(MeshCom), INTENT(IN) :: Com
     !---------------------------------

     IF (Com%Me == 0 ) THEN        
        WRITE(6, 101)
        WRITE(6, "(A, A, A) ") "* Modele == ", DATA%Modele(1: 20), "                  *"
        WRITE(6, '("* Nom du cas         : ")', ADVANCE = 'no')
        WRITE(6, '(1X, A15, 10X, "*")')TRIM(data%rootname)
        WRITE(6, '("* Formulation        : ")', ADVANCE = 'no')
        WRITE(6, '(1X, A15, 10X, "*")')TRIM(DATA%Approche)
        WRITE(6, 100)
     END IF
     
100  FORMAT(50("*"))
101  FORMAT("*", 48X, "*")
     
   END SUBROUTINE AffichageDeDebut2
   !===============================
   
   !=============================
   SUBROUTINE AffichageDeFin(Com)
   !=============================

     IMPLICIT NONE
          
     CHARACTER(LEN = *), PARAMETER :: mod_name = "AffichageDeFin"

     TYPE(MeshCom), INTENT(IN) :: Com
     !--------------------------------

     CHARACTER(LEN = 8)  :: dt2
     CHARACTER(LEN = 10) :: time
     !--------------------------------

     CALL DATE_AND_TIME(dt2, time)

     IF (Com%Me == 0 ) THEN
        WRITE(6, "(A, A, A, A, A) ") "* FIN Le ", dt2, " a ", time, "                       *"
     END IF
      
   END SUBROUTINE AffichageDeFin
   !============================
   
   !==========================================
   SUBROUTINE AfficheResu(DATA, Var, Com, CFL)
   !==========================================

     IMPLICIT NONE

     CHARACTER(LEN = *), PARAMETER  :: mod_name = "AfficheResu"

     TYPE(Donnees),   INTENT(IN)    :: DATA
     TYPE(Variables), INTENT(INOUT) :: Var
     TYPE(MeshCom),   INTENT(INOUT) :: Com
     REAL, OPTIONAL,  INTENT(IN)    :: CFL
     !-------------------------------------

     CHARACTER(LEN=64), DIMENSION(Data%NVar) :: Var_Vc

     LOGICAL :: existe
     CHARACTER(LEN = 80) :: filename
     INTEGER :: ies, ios, Nvar_affiche
     REAL :: v_CFL
#ifndef SEQUENTIEL
     REAL,  DIMENSION(Data%Nvar) :: Umin, Umax
#endif
     !-----------------------------------------------
     Nvar_affiche=Data%Nvar
     filename = "Residus_" // TRIM(data%RootName) // ".dat"

#ifndef SEQUENTIEL
     DO ies = 1, Nvar_affiche
        Umin(ies) = MINVAL(Var%Vp(ies, : ))
        Umax(ies) = MAXVAL(Var%Vp(ies, : ))
     END DO

     CALL Reduce(Com, cs_parall_min, Umin(: ))
     CALL Reduce(Com, cs_parall_max, Umax(: ))
#endif

     Var_Vc(:) = 'rho'
     DO ies = 1, Nvar_affiche
        Var_Vc(ies) = TRIM(ADJUSTL(Data%VarNames(ies)))
     ENDDO

     IF (Com%Me == 0) THEN

        IF (PRESENT(CFL)) THEN
           v_CFL = CFL
        ELSE
           v_CFL = var%CFL
        ENDIF

        WRITE(6, *)
        WRITE(6, '(" +", 62("-"), "+")')
        WRITE(6, '(" | ite   = ", I10, 42X, " |")') Var%kt
        WRITE(6, '(" | Dt    = ", E13.6, 19X, " CFL =", E12.5, 2X, " |")') Var%Dt, v_CFL
        WRITE(6, '(" | Time  = ", E13.6, 39X, " |")') Var%temps
        WRITE(6, '(" +", 62("-"), "+")')
        WRITE(6, '(" | ", "           Min   ", "        Max  ",  &
                          & "          Res0 ", "       Res/Res0 ", "|" )')

        DO ies = 1, Nvar_affiche

#ifndef SEQUENTIEL
           WRITE(6, '(" ", A6": ", 2(E12.5, 2X), 1X, 2(E12.5, 2x))') & 
                & TRIM(ADJUSTL(Var_Vc(ies))), &
                  Umin(ies), Umax(ies), &
                  Data%Residus0(ies),  Var%Residus(ies)
#else
           WRITE(6, '(" ", A6": ", 2(E12.5, 2X), 1X, 2(E12.5, 2x))') & 
                & TRIM(ADJUSTL(Var_Vc(ies))),  &
                  MINVAL(Var%Vp(ies, : )), MAXVAL(Var%Vp(ies, : )), &
                  Data%Residus0(ies),  Var%Residus(ies)
#endif
        END DO

        WRITE(6, '(" +", 62("-"), "+")')

        IF (Var%kt < data%ifre) THEN

           OPEN(UNIT = 80, FILE = filename, FORM = 'formatted', STATUS = 'unknown', &
              & IOSTAT = ios, RECL = 200)

           IF (ios /= 0) THEN
              PRINT *, mod_name, " ERREUR : impossible d'ouvrir en REPLACE ", TRIM(filename), ios
              print*, Var%kt, data%ifre

              CALL delegate_stop()
           END IF

           WRITE(80, *) 'TITLE="Convergence history"'
           !WRITE(80, *) 'VARIABLES="ite" "CFL" "L1_RES" "L2_RES", "CPU_time"'

        ELSE

           INQUIRE(FILE = filename, EXIST = existe)
           IF (.NOT. existe) THEN
              PRINT *, mod_name, " :: ERREUR : le fichier ", TRIM(filename), " n'existe pas"
              OPEN(UNIT = 80, FILE = filename, FORM = 'formatted', STATUS = 'old', ACCESS = 'sequential', &
                 & IOSTAT = ios, RECL = 200)

              IF (ios /= 0) THEN
                 PRINT *, mod_name, " ERREUR : impossible d'ouvrir en OLD ", TRIM(filename), ios
                 CALL delegate_stop()
              END IF
           ELSE
              OPEN(UNIT = 80, FILE = filename, FORM = 'formatted', STATUS = 'old', ACCESS = 'sequential', &
                   & POSITION = 'APPEND', IOSTAT = ios, RECL = 200)
              IF (ios /= 0) THEN
                 PRINT *, mod_name, " ERREUR : impossible d'ouvrir en APPEND ", TRIM(filename), ios
                 CALL delegate_stop()
              END IF
           END IF
        END IF

     END IF

     IF (Com%Me == 0) THEN
!        WRITE(UNIT = 80, FMT = '(I8, 8(2X, ES16.8))') Var%kt, Var%CFL, Var%Residus, Var%time_loop

        WRITE(UNIT = 80, FMT='(I8, 2X,  E12.5)', ADVANCE='NO') Var%kt, Var%CFL
        DO ies = 1, SIZE(Var%Residus)
           WRITE(UNIT = 80, FMT='(1X,  E12.5)', ADVANCE='NO') Var%Residus(ies)
        ENDDO
        DO ies = 1, SIZE(Var%Res_oo)
           WRITE(UNIT = 80, FMT='(1X,  E12.5)', ADVANCE='NO') Var%Res_oo(ies)
        ENDDO
        WRITE(UNIT = 80, FMT='(2X,  E12.5)') Var%time_loop

     END IF

   END SUBROUTINE AfficheResu
   !=========================

  !====================================
  SUBROUTINE Compute_S_error(Data, Var)
  !====================================

    IMPLICIT NONE

    TYPE(Donnees),   INTENT(IN) :: Data
    TYPE(Variables), INTENT(IN) :: Var
    !-----------------------------------------

    INTEGER, DIMENSION(:), ALLOCATABLE :: NU
    REAL,    DIMENSION(:), ALLOCATABLE :: rho, P, SS

    REAL :: g, rho_oo, P_oo
    REAL :: err_L2, err_Loo, int_V
    INTEGER :: je, ierror
    !-----------------------------------------

    err_L2 = 0.d0; err_Loo = 0.d0; int_V = 0.d0

    rho_oo = Data%VarPhyInit(i_rho_vp, 1)
    P_oo   = Data%VarPhyInit(i_pre_vp, 1)

    DO je = 1, SIZE(d_element)

       ALLOCATE(  NU(d_element(je)%e%N_points), &
                 rho(d_element(je)%e%N_points), &
                   P(d_element(je)%e%N_points), &
                  SS(d_element(je)%e%N_points)  )

       NU = d_element(je)%e%NU

       rho = Var%Vp(i_rho_vp, NU)
       P   = Var%Vp(i_pre_vp, NU)

       g = EOSKappa() + 1

       SS = ( P/(rho**g) - P_oo/(rho_oo**g) ) / &
            ( P_oo/(rho_oo**g) )

       err_L2 = err_L2 + Int_d(d_element(je)%e, SS**2)

       int_V = int_V + d_element(je)%e%Volume

       err_Loo = MAX( err_Loo, MAXVAL(ABS(SS)) )

       DEALLOCATE( rho, P, SS, NU )

    ENDDO

    err_L2 = SQRT(err_L2) / SQRT(int_V)
    
    OPEN(9, FILE = 'error.'//TRIM(ADJUSTL(Data%PbName)), &
         ACTION= 'WRITE', POSITION = 'APPEND', IOSTAT = ierror)

    IF(ierror /= 0) THEN              
       WRITE(*,*) 'ERROR: Impossible to open the file converegence'
       WRITE(*,*) 'STOP'
       STOP       
    ENDIF

    WRITE(9, 22) Var%NCells, err_L2, err_Loo
    
    CLOSE(9)

22 FORMAT(I6, 2(1x,e24.16))

  END SUBROUTINE Compute_S_error
  !=============================  

!!$#ifdef NAVIER_STOKES
!!$    !====================================
!!$    SUBROUTINE Compute_test_err(Com, Var)
!!$    !====================================
!!$
!!$      IMPLICIT NONE
!!$
!!$      TYPE(MeshCom),   INTENT(INOUT) :: Com
!!$      TYPE(Variables), INTENT(INOUT) :: Var
!!$      !-------------------------------------
!!$
!!$      REAL(KIND=8), DIMENSION(:,:), POINTER :: p
!!$      REAL(KIND=8), DIMENSION(:),   POINTER :: w
!!$      REAL(KIND=8), DIMENSION(:,:), POINTER :: xy
!!$      !----------------------------------------------------
!!$    
!!$      INTEGER :: je, iq, N_quad
!!$
!!$      REAL, DIMENSION(Data%NVar) :: u_ex, u_q, err_L2, int_D2
!!$
!!$      REAL ::  mu, Gp, P0
!!$      !----------------------------------------------------
!!$      
!!$      err_L2 = 0.d0; int_D2 = 0.d0
!!$
!!$      DO je = 1, SIZE(d_element)
!!$   
!!$         N_quad =  d_element(je)%e%N_quad
!!$         p      => d_element(je)%e%phi_q
!!$         xy     => d_element(je)%e%xx_q
!!$         w      => D_element(je)%e%w_q
!!$      
!!$          DO iq = 1, N_quad
!!$
!!$             u_ex = MS_Csolution(xy(1, iq), xy(2, iq))
!!$
!!$             u_q(i_rho_ua) = SUM( Var%Ua(i_rho_ua, d_element(je)%e%NU) * p(:, iq) )
!!$             u_q(i_rv1_ua) = SUM( Var%Ua(i_rv1_ua, d_element(je)%e%NU) * p(:, iq) )
!!$             u_q(i_rv2_ua) = SUM( Var%Ua(i_rv2_ua, d_element(je)%e%NU) * p(:, iq) )
!!$             u_q(i_ren_ua) = SUM( Var%Ua(i_ren_ua, d_element(je)%e%NU) * p(:, iq) )
!!$
!!$             err_L2 = err_L2 + w(iq) * (u_q - u_ex)**2
!!$             int_D2 = int_D2 + w(iq) * u_ex**2
!!$
!!$          ENDDO
!!$
!!$          NULLIFY( p, w, xy )
!!$
!!$       ENDDO
!!$
!!$       err_L2 = SQRT(err_L2)/SQRT(int_D2)
!!$
!!$       WRITE(*,*) '*** SOLUTION L2 ERROR ***'
!!$       WRITE(*, '(I9, 4E24.16)') Var%NCells, err_L2
!!$       WRITE(*,*)
!!$
!!$       CALL Compute_Gtest_err(Com, Var)
!!$
!!$       WRITE(*,*)
!!$
!!$CONTAINS
!!$
!!$    !=====================================
!!$    SUBROUTINE Compute_Gtest_err(Com, Var)
!!$    !=====================================
!!$
!!$      IMPLICIT NONE
!!$
!!$      TYPE(MeshCom),   INTENT(INOUT) :: Com
!!$      TYPE(Variables), INTENT(INOUT) :: Var
!!$      !-------------------------------------
!!$
!!$      REAL(KIND=8), DIMENSION(:,:), POINTER :: p
!!$      REAL(KIND=8), DIMENSION(:),   POINTER :: w
!!$      REAL(KIND=8), DIMENSION(:,:), POINTER :: xy
!!$      !----------------------------------------------
!!$
!!$      REAL(KIND=8), DIMENSION(Data%NDim, Data%NVar) :: D_u_ex_q
!!$      REAL(KIND=8), DIMENSION(Data%NDim)            :: D_u_q
!!$
!!$      INTEGER :: je, k, i, iq, N_quad, id, l
!!$
!!$      REAL :: err_q, err_F_L2
!!$
!!$      REAL, DIMENSION(Data%NDim, Data%NVar) :: err_GG_L2, int_D2
!!$      !----------------------------------------------------
!!$
!!$      err_GG_L2 = 0.d0; int_D2 = 0.d0; err_F_L2 = 0.d0
!!$
!!$      CALL Grad_Recovery(Var)
!!$
!!$      DO je = 1, SIZE(d_element)
!!$
!!$         N_quad =  d_element(je)%e%N_quad
!!$         p      => d_element(je)%e%phi_q
!!$         xy     => d_element(je)%e%xx_q
!!$         w      => D_element(je)%e%w_q
!!$
!!$         DO iq = 1, N_quad
!!$          
!!$            D_u_ex_q = MS_GradSolution(xy(1, iq), xy(2, iq))
!!$
!!$            DO l = 1, Data%NVar
!!$
!!$               DO id = 1, Data%NDim
!!$                  D_u_q(id) = SUM( Var%D_Ua(id, l, d_element(je)%e%NU) * p(:, iq) )
!!$               ENDDO
!!$
!!$               DO id = 1, Data%NDim
!!$
!!$                  err_q = D_u_q(id) - D_u_ex_q(id, l)
!!$
!!$                  err_GG_L2(id, l) = err_GG_L2(id, l) + w(iq) * err_q**2
!!$
!!$                  int_D2(id, l) = int_D2(id, l) + w(iq) * D_u_ex_q(id, l)**2
!!$
!!$               ENDDO
!!$
!!$            ENDDO
!!$
!!$         ENDDO
!!$
!!$         NULLIFY( p, w, xy )
!!$
!!$      ENDDO
!!$    
!!$      DO id = 1, Data%NDim
!!$         DO l = 1, Data%NVar
!!$            err_GG_L2(id, l) = SQRT(err_GG_L2(id, l))/SQRT(int_D2(id, l))
!!$         ENDDO
!!$      ENDDO
!!$
!!$      WRITE(*,*) '*** GRADIENT L2 ERROR ***'
!!$      WRITE(*, '(I9, 4E24.16)') Var%NCells, err_GG_L2(1, :)
!!$      WRITE(*, '(9X, 4E24.16)')             err_GG_L2(2, :)
!!$
!!$      WRITE(*,*)
!!$
!!$    END SUBROUTINE Compute_Gtest_err
!!$    !===============================
!!$
!!$  END SUBROUTINE Compute_test_err
!!$  !===============================
!!$#endif

END MODULE Outputs
 
     

