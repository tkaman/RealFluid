MODULE element_class_seg

  USE element_class

  IMPLICIT NONE

  TYPE, PUBLIC, EXTENDS(element) :: element_seg

     INTEGER :: inum = 0 ! logique frontiere
     INTEGER :: jt = 0   ! element  qui contient ce segment comme arete
     INTEGER :: iseg = 0

CONTAINS

  END TYPE element_seg

END MODULE element_class_seg
