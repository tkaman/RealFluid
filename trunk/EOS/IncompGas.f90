!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   MODULE Incompgas
      USE LesTypes
      USE LibComm
      IMPLICIT NONE
      REAL, PRIVATE, SAVE :: cv
      REAL, PRIVATE, SAVE :: gama
      REAL, PRIVATE, SAVE :: gama1
      INTEGER, PRIVATE, SAVE :: ndim, nvar

   CONTAINS

!!-- Lit des variables globales priv�es gama, et cv de EOS sur EOS_<n0>.data (suite de lecture, ouvert par EOS.f90/EOSinit)
!!-- initialise par argument ndim et nvar
      SUBROUTINE ig_init(MyUnit, Dim, Var)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "ig_init"
         INTEGER, INTENT(IN) ::MyUnit, Dim, Var
         INTEGER :: statut

            READ(MyUnit, *, IOSTAT = statut)gama
            IF (statut /= 0) THEN
               PRINT *, mod_name, " ERREUR : statut==", statut, "pour la ligne gama"
               CALL stop_run()
            END IF
            READ(MyUnit, *, IOSTAT = statut)cv
            IF (statut /= 0) THEN
               PRINT *, mod_name, " ERREUR : statut==", statut, "pour la ligne cv"
               CALL stop_run()
            END IF

         gama1 = gama -1.0
         ndim = Dim
         nvar = Var
      END SUBROUTINE ig_init

      FUNCTION IG_Gradt(Vp) RESULT(gt)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Gradt"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL, DIMENSION(nvar)                :: gt
         REAL :: q, rho
         rho = Vp(i_rho_vp) * cv
         q  = 0.5 * Sum(Vp(i_vi1_vp: i_vid_vp) ** 2)

         gt(1)        = q            / rho
         gt(2: 1 + ndim) = - Vp(i_vi1_vp: i_vid_vp) / rho
         gt(2 + ndim)   = 1.0          / rho

         RETURN
      END FUNCTION IG_Gradt

      FUNCTION IG_Gradp(Vp) RESULT(gp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Gradp"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL, DIMENSION(nvar)          :: gp
         REAL :: q
         q  = 0.5 * Sum(Vp(i_vi1_vp: i_vid_vp) ** 2)
         gp(1)        =  gama1 * q
         gp(2: 1 + ndim) = - gama1 * Vp(i_vi1_vp: i_vid_vp)
         gp(2 + ndim)   =  gama1
      END FUNCTION IG_Gradp

      FUNCTION IG_Kappa() RESULT(k)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Kappa"
         REAL :: k
         k = gama1
      END FUNCTION IG_Kappa

      FUNCTION IG_Cp() RESULT(k)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Cp"
         REAL :: k
         k  = gama * cv
      END FUNCTION IG_Cp

      FUNCTION IG_Epsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Epsilon"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: re
    ! epsilon = p/(gama-1)/rho
         re  = Vp(i_pre_vp) / gama1 / Vp(i_rho_vp)
      END FUNCTION IG_Epsilon


      FUNCTION IG_RhoEpsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_RhoEpsilon"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: re
    ! rho*epsilon = p/(gama-1)
         re  = Vp(i_pre_vp) / gama1
      END FUNCTION IG_RhoEpsilon

      FUNCTION IG_EpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_EpsilonDet"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: are
    ! rho*epsilon = rho*CV*T
         are  = cv * Vp(i_tem_vp)
      END FUNCTION IG_EpsilonDet

      FUNCTION IG_RhoEpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_RhoEpsilonDet"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: are
    ! rho*epsilon = rho*CV*T
         are  = Vp(i_rho_vp) * cv * Vp(i_tem_vp)
         RETURN
      END FUNCTION IG_RhoEpsilonDet

      FUNCTION IG_Rhoh(Vp) RESULT(rh)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Rhoh"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: rh
    ! rho*epsilon = gama*p/(gama-1)
         rh  = Vp(i_pre_vp) * gama / gama1
         RETURN
      END FUNCTION IG_Rhoh


      FUNCTION IG_Temperature(Vp) RESULT(Temp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Temperature"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: Temp
    ! T =(epsilon)/cv
         REAL   :: Epsilon
    !           if (Ua(ifl, 1, is) /= 0 .and. Ua(ifl, 2, is)/=0) then
         Epsilon = Vp(i_eps_vp)    ! �nergie interne
         Temp    = Epsilon / cv
         RETURN
      END FUNCTION IG_Temperature


      FUNCTION IG_Pression(Vp) RESULT(p)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Pression"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: p
    ! P = (gama - 1)*rho*epsilon
         REAL   :: Epsilon, rho
    !           if (Ua(ifl, 1, is) /= 0 .and. Ua(ifl, 2, is)/=0) then
         rho  = Vp(i_rho_vp)
         Epsilon = Vp(i_eps_vp)    ! �nergie interne
         p = gama1 * rho * Epsilon
         RETURN
      END FUNCTION IG_Pression


      FUNCTION IG_Son(Vp) RESULT (c)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Son"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: c
         c = Sqrt(gama * Vp(i_pre_vp) / Vp(i_rho_vp))
         RETURN
      END FUNCTION IG_Son

      SUBROUTINE IG_Gas(Vp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "IG_Gas"
         REAL, DIMENSION(: ), INTENT(INOUT) :: Vp
    ! P = (gama - 1)*rho*epsilon - gama*Pc

      END SUBROUTINE IG_Gas

   END MODULE Incompgas
