!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MODULE VanDerWaals

  USE LesTypes
  USE LibComm

  IMPLICIT NONE

  REAL,    PRIVATE, SAVE :: gam_VDW
  REAL,    PRIVATE, SAVE :: a_VDW
  REAL,    PRIVATE, SAVE :: b_VDW
  REAL,    PRIVATE, SAVE :: Zc_VDW


  !*****************************************************
  !       FILE EOS_1.data ; Van Der Waals Gas Case     *
  !*****************************************************
  !"VDW"                  !EosLaw                      * 
  !1.4                    !gama_VDW, gas constant      *
  !*****************************************************

  !-----------------------------------------------------
  !       Other Values                                 -
  !-----------------------------------------------------
  !a_VDW  = 3             !Constant                    -
  !b_VDW  = 1/3           !Constant                    -
  !Zc_VDW = 3/8           !Constant                    -
  !Cv_inf = R/(gam_VDW - 1)                            -
  !-----------------------------------------------------

CONTAINS

  !===================================
  SUBROUTINE VDW_init(MyUnit, Dim, Var)
    !===================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "vdw_init"

    INTEGER, INTENT(IN) :: MyUnit, Dim, Var
    !---------------------------------------

    INTEGER :: statut
    !---------------------------------------

    !Gas constant
    READ(MyUnit, *, IOSTAT = statut) gam_VDW

    a_VDW  = 3.d0             !Constant                    

    b_VDW  = 1.d0/3.d0        !Constant                    

    Zc_VDW = 3.d0/8.d0        !Constant                    

  END SUBROUTINE VDW_init
  !=====================





  !============================================================================
  !============================================================================
  !BEGIN :           basic FUNCTIONS

  !Description
  !-------------------------------
  ! > VDW_P__rho_T(rho,T)          P = P(rho  , T       )  
  ! > VDW_T__rho_P(rho,P)          T = T(rho  , P       )
  ! > VDW_T__rho_rhoe(rho,rhoe)    T = T(rho  , rho*e   ),     e := internal energy
  ! > VDW_rho__P_T(P,T)            rho = rho(P  , T       )  
  ! > VDW_c__rho_T(rho,T)          c = c(rho  , T       )
  ! > VDW_e__rho_T(rho,T)          e = e(rho  , T       ),     e := internal energy
  ! > VDW_P__rho_rhoU_rhoE(rho,rhoU,rhoE)     P = P(rho, rho*|u|, rho*E)  E := total energy = e + |u|**2/2
  ! > VDW_dP_de__rho_T(rho,T)                (dP/de)|rho    = dP/de(rho, T)   e:= internal energy
  ! > VDW_dP_drho__rho_T(rho,T)              (dP/drho)|e    = dP/drho(rho, T)   e:= internal energy
  ! > VDW_pi_rho__rho_u_T(rho,u,T)           (dPI/drho)     = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > VDW_pi_rhoe__rho_T(rho,T)               dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > VDW_GAMMA__rho_T(rho,T)                 GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 
  ! > VDW_de_drho__rho_T(rho,T)              (de/drho)|T    = de/drho(rho,T)
  ! > VDW_dT_de__rho_T(rho, T)               (dT/de)_rho    = dT/de(rho, T)
  ! > VDW_dT_drho__rho_T(rho, T)             (dT/drho)_e    = dT/drho(rho, T)
  ! > VDW_dP__dT_dv(rho, T)                   dP/dT|_v & dP/dv|_T
  ! > VDW_dP2__dT_dv(rho, T)                  d^2P/dT^2|_v & d^2P/dv^2|_T & d^2P/dvdT|_T

  !===========================================
  REAL FUNCTION VDW_P__rho_T(rho, T) RESULT(P)
  !===========================================
  ! > P = P(rho,T)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    P = T/(Zc_VDW*((1./rho) - b_VDW)) - a_VDW*rho*rho

  END FUNCTION VDW_P__rho_T
  !========================

  !===========================================
  REAL FUNCTION VDW_T__rho_P(rho, P) RESULT(T)
  !===========================================
  ! > T = T(rho,P)

    REAL, INTENT(IN) :: rho, P
    !-------------------------

    T = (P + a_VDW*rho*rho)*Zc_VDW*((1./rho) - b_VDW)

  END FUNCTION VDW_T__rho_P
  !========================

  !==========================================
  REAL FUNCTION VDW_T__rho_rhoe(rho, rhoe) RESULT(T)
    !==========================================
    ! >  T = T(rho, rho*e), e := intern energy

    REAL, INTENT(IN) :: rho, rhoe
    !-------------------------

    T = (rhoe/rho + a_VDW*rho)*(gam_VDW - 1.d0)*Zc_VDW

  END FUNCTION VDW_T__rho_rhoe
  !=======================

  !==========================================
  REAL FUNCTION VDW_rho__P_T(P, T) RESULT(rho)
    !==========================================
    ! >  rho = rho(P,T)  

    REAL, INTENT(IN) :: P, T
    !-------------------------

    INTEGER          :: i, i_conv, itermax
    !-------------------------

    REAL             :: a, b, c, f, f_der, rho_1, TOL
    !-------------------------

    !...............
    !Newton m�thod
    !...............
    !f(rho) = rho**3 - rho**2/b_VDW + rho*(P/a_VDW + T/(a_VDW*b_VDW*Zc_VDW)) - P/(a_VDW*b_VDW) = 0


    !INITIALIZATION
    rho_1      = 0.01d0
    i_conv     = 0 
    itermax    = 400 
    TOL        = 5e-6

    a      = -1.0d0 / b_VDW
    b      = P/a_VDW + T/(a_VDW*b_VDW*Zc_VDW)
    c      = -P/(a_VDW*b_VDW)

    !LOOP
    DO i = 1, itermax

       rho   = rho_1 
       f     = rho**3       + a*rho**2    + b*rho + c

       IF( f .eq. 0.0d0) THEN
          i_conv = 1
          EXIT
       END IF

       f_der = 3.0d0*rho**2 + 2.0d0*a*rho + b

       rho_1 = rho - f_der/f

       IF( ABS(rho_1 - rho)/rho_1 .le. TOL) THEN
          rho    = rho_1
          i_conv = 1
          EXIT
       END IF

    END DO

    IF (i_conv .ne. 1) THEN
       print*, "ERROR in EOS - rho CALCULATION"
       print*, "VDW.f90 - VDW_rho__P_T(P,T)"
       print*, "P = ", P, "T = ", T
       print*, "No Convergence in ", itermax, "iterations"
       print*, "rho = ", rho, "rho_1 = ", rho_1
       print*, "Err = ", ABS(rho_1 - rho)/rho_1, "> TOL = ", TOL
       PAUSE
    END IF

  END FUNCTION VDW_rho__P_T
  !=======================


  !==========================================
  REAL FUNCTION VDW_c__rho_T(rho, T) RESULT(c)
    !==========================================
    !  > c = c(rho,T)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    c = SQRT(((1.d0/(1.d0 - rho*b_VDW))**2.d0)*T/Zc_VDW*gam_VDW - 2.d0*a_VDW*rho)

  END FUNCTION VDW_c__rho_T
  !=======================

  !==========================================
  REAL FUNCTION VDW_e__rho_T(rho, T) RESULT(e)
    !==========================================
    !  > e = e(rho,T), e := internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    e = T/(gam_VDW - 1.d0)/Zc_VDW - a_VDW*rho

  END FUNCTION VDW_e__rho_T
  !=======================

  !==========================================
  REAL FUNCTION VDW_P__rho_rhoU_rhoE(rho, rhoU, rhoE) RESULT(P)
    !==========================================
    !  >  P = P(rho, rho*|u|, rho*E) E total energy 

    REAL, INTENT(IN) :: rho, rhoU, rhoE
    !-------------------------

    REAL             :: e 
    !-------------------------

    e = rhoE/rho - 0.5*(rhoU/rho)**2
    P = (gam_VDW - 1.d0)*(e + a_VDW*rho)/(1.d0/rho - b_VDW) - a_VDW*rho**2.d0


  END FUNCTION VDW_P__rho_rhoU_rhoE
  !=======================


  !==========================================
  REAL FUNCTION VDW_dP_de__rho_T(rho, T) RESULT(d)
    !==========================================
    !  > (dP/de)|rho = dP/de(rho, T)   e:= internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    d = (gam_VDW - 1.d0)*rho/(1.d0 - b_VDW*rho)

  END FUNCTION VDW_dP_de__rho_T
  !=======================

  !==========================================
  REAL FUNCTION VDW_dP_drho__rho_T(rho, T) RESULT(d)
    !==========================================
    !  > (dP/drho)|e = dP/drho(rho, T)   e:= internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    d = (gam_VDW - 1.d0)*(VDW_e__rho_T(rho, T) - a_VDW*b_VDW*rho**2.d0 + 2.d0*a_VDW*rho)
    d = d/((1.d0 - b_VDW*rho)**2.d0) - 2.d0*a_VDW*rho

  END FUNCTION VDW_dP_drho__rho_T
  !=======================

  !==========================================
  REAL FUNCTION VDW_pi_rho__rho_u_T(rho, u, T) RESULT(pi)
    !==========================================
    !  > (dPI/drho) = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    REAL, INTENT(IN) :: rho, u, T
    !-------------------------

    REAL             :: e, dRho, dE
    !-------------------------

    e    = VDW_e__rho_T(rho, T)
    dRho = VDW_dP_drho__rho_T(rho, T)
    dE   = VDW_dP_de__rho_T(rho, T)

    pi = dRho + (0.5*u*u - e)*dE/rho 

  END FUNCTION VDW_pi_rho__rho_u_T
  !=======================

  !==========================================
  REAL FUNCTION VDW_pi_rhoe__rho_T(rho, T) RESULT(pi)
    !==========================================
    !  > dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    pi = VDW_dP_de__rho_T(rho, T) / rho

  END FUNCTION VDW_pi_rhoe__rho_T
  !=======================

  !==========================================
  REAL FUNCTION VDW_de_drho__rho_T(rho,T) RESULT(d)
    !==========================================
    ! > VDW_de_drho__rho_T(rho,T)                (de/drho)|rho    = de/drho(rho,T)
    ! > d = 0

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    d = -a_VDW

  END FUNCTION VDW_de_drho__rho_T
  !=======================


  !=================================================
  REAL FUNCTION VDW_dT_drho__rho_T(rho, T) RESULT(d)
  !=================================================
  !
  !                 -(de/drho)|_T      -(de/drho)|_T 
  ! (dT/drho)|_e = ---------------- = ----------------
  !                  (de/dT)|_rho          Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: de_drho, c_v
    !-------------------------
    
    de_drho = VDW_de_drho__rho_T(rho, T)

    c_v = VDW_Cv()

    d = -de_drho / c_v

  END FUNCTION VDW_dT_drho__rho_T
  !==============================

  !===============================================
  REAL FUNCTION VDW_dT_de__rho_T(rho, T) RESULT(d)
  !===============================================
  !
  !                         1          1
  ! (dT/de)|_rho = ---------------- = ----
  !                  (de/dT)|_rho      Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: c_v
    !-------------------------
    
    c_v = VDW_Cv()

    d = 1.d0 / c_v

  END FUNCTION VDW_dT_de__rho_T
  !============================

  !========================================
  FUNCTION VDW_dP__dT_dv(rho, T) RESULT(dP)
  !========================================
  ! 
  ! dP(1) = dP/dT|_v,   dP(2) = dP/dv|_T
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: dP
    !-------------------------

    REAL :: v
    !-------------------------

    v = 1.d0 / rho
    
    dP(1) = 1.d0/( Zc_VDW*(v - b_VDW) )

    dP(2) = -T/( Zc_VDW*(v - b_VDW)**2 ) + 2.d0*a_VDW/v**3

  END FUNCTION VDW_dP__dT_dv
  !=========================

  !==========================================
  FUNCTION VDW_dP2__dT_dv(rho, T) RESULT(dP2)
  !==========================================
  ! 
  ! dP(1) = d^2P / dT^2|_v  
  ! dP(2) = d^2P / dv^2|_T 
  ! dP(3) = d^2P / dvdT|_vT
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(3) :: dP2
    !-------------------------

    REAL :: v
    !-------------------------

    v = 1.d0 / rho
    
    dP2(1) = 0.d0

    dP2(2) = 2.d0*T/( Zc_VDW*(v - b_VDW)**3 ) - 6.d0*a_VDW/v**4

    dP2(3) = -1.d0/( Zc_VDW*(v - b_VDW)**2 )

  END FUNCTION VDW_dP2__dT_dv  
  !==========================


  !---------------END basic FUNCTIONS          -----------------------------!
  !============================================================================
  !============================================================================


  !============================================================================
  !============================================================================
  !BEGIN :           derivated FUNCTIONS

  !Description
  ! > VDW_c__rho_e(rho, e)          c  = c(rho,e)
  ! > VDW_Temperature(Vp)           T  = T(rho,e)
  ! > VDW_Pression(Vp)              P  = P(rho,e)
  ! > VDW_Epsilon(Vp)               e  = T(rho,P)
  ! > VDW_EpsilonDet(Vp)            e  = T(rho,T)
  ! > VDW_Son(Vp)                   c  = T(rho,P)
  ! > VDW_enthalpy(Vp)              h  = T(rho,P)
  ! > VDW_Gas(Vp)                   Vp = Vp(rho,e) "subroutine" computes all Vp components from (rho,e) <- Vp
  ! > VDW_Cv()                      Cv = constant
  ! > VDW_d_Cv()                    d_Cv = d_Cv

  !==========================================
  REAL FUNCTION VDW_c__rho_e(rho, e) RESULT(c)
    !==========================================
    ! > c = c(rho, e)

    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: e

    REAL :: T
    !-------------------------

    !INTERMEDIATE CALCULATION
    T = VDW_T__rho_rhoe(rho,rho*e)

    !RESULT
    c = VDW_c__rho_T(rho,T)

  END FUNCTION VDW_c__rho_e
  !=======================  


  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !============================================
  REAL FUNCTION VDW_Temperature(Vp) RESULT(T)
    !============================================
    ! > T = T(rho, e)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "VDW_Temperature"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, e
    !------------------------------------

    ! DATA
    e     = Vp(i_eps_vp) 
    rho   = Vp(i_rho_vp)

    ! RESULT
    T     = VDW_T__rho_rhoe(rho,rho*e)

  END FUNCTION VDW_Temperature
  !==========================


  !======================================
  REAL FUNCTION VDW_Pression(Vp) RESULT(P)
    !======================================
    ! P = P (e, rho)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "VDW_Pression"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: e, rho, T
    !-------------------------------------

    !DATA
    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)    ! �nergie interne

    !INTERMEDIATE CALCULATION
    T       = VDW_T__rho_rhoe(rho,rho*e)

    !RESULT
    P       = VDW_P__rho_T(rho,T)

  END FUNCTION VDW_Pression
  !=======================

  !======================================
  REAL FUNCTION VDW_Epsilon(Vp) RESULT(e)
    !======================================
    ! > e = e(rho,P)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "VDW_Epsilon"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T   = VDW_T__rho_P(rho,P)

    !RESULT
    e   = VDW_e__rho_T(rho,T)

  END FUNCTION VDW_Epsilon
  !======================

  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !==========================================
  REAL FUNCTION VDW_EpsilonDet(Vp) RESULT(e)
    !==========================================
    ! > e = e(rho, T)

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    T   = Vp(i_tem_vp)

    ! e = e(rho, T)
    e   = VDW_e__rho_T(rho,T)

  END FUNCTION VDW_EpsilonDet
  !=========================


  !==================================
  REAL FUNCTION VDW_Son(Vp) RESULT (c)
    !==================================
    ! c = c(P, rho)

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T, e
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T = VDW_T__rho_P(rho,P)

    !RESULT
    c = VDW_c__rho_T(rho,T)

  END FUNCTION VDW_Son
  !==================


  !======================================
  REAL FUNCTION VDW_enthalpy(Vp) RESULT(h)
    !======================================
    ! > h = h (P, rho)

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T, e
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !CALCULATION
    T = VDW_T__rho_P(rho,P)
    e = VDW_e__rho_T(rho,T)

    !RESULT
    h = e + P/rho

  END FUNCTION VDW_Enthalpy
  !=======================

  !===========================
  SUBROUTINE VDW_Gas(Vp)
    !===========================
    ! > (T,P,c) in function of (rho, e), e:= internal energy

    CHARACTER(LEN = *), PARAMETER :: mod_name = "VDW_PerfectGas"

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp
    !--------------------------------------

    REAL :: e, rho, P, T, c
    !---------------------------------------

    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)  ! �nergie interne i_eps_vp

    !CALCULATION
    T       = VDW_T__rho_rhoe(rho,rho*e)         !-- Temperature
    P       = VDW_P__rho_T(rho,T)                !-- Pressure
    c       = VDW_c__rho_T(rho,T)                !-- Sound Speed

    !RESULT
    Vp(i_pre_vp) = P 
    Vp(i_tem_vp) = T
    Vp(i_son_vp) = c

  END SUBROUTINE VDW_Gas
  !===========================

  !=====================================
  FUNCTION VDW_dCv__dT_dv() RESULT(d_Cv)
  !=====================================
  ! 
  ! d_Cv(1) = dCv/dT|_rho
  ! d_Cv(2) = dCv/drho|_T
  !
    IMPLICIT NONE

    REAL, DIMENSION(2) :: d_Cv
    !-------------------------

    d_Cv = 0.d0

  END FUNCTION VDW_dCv__dT_dv
  !==========================

  ! TO BE REMOVED XXX
  !===============================
  FUNCTION VDW_d_Cv() RESULT(d_cv)
  !===============================

    IMPLICIT NONE
    
    REAL :: d_cv
    !-----------

    d_cv = 0.d0

  END FUNCTION VDW_d_Cv
  !====================

  !===========================
  FUNCTION VDW_Cv() RESULT(cv)
  !===========================
  ! > Cv(rho,T) = (de/dT)|rho    = de/dT(rho,T)
  ! > d = Cv

    IMPLICIT NONE
    
    REAL :: cv
    !---------

    cv = 1.0d0/Zc_VDW/(gam_VDW - 1.d0)

  END FUNCTION VDW_Cv
  !==================



  !-------------ENDING derivated FONCTIONS     -----------------------------!
  !----------------------------------------------------------------------------
  !============================================================================


!!$  !==========================================
!!$  FUNCTION VDW_GAMMA__rho_T(rho, T) RESULT(G)
!!$  !==========================================
!!$  !  > GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 
!!$
!!$    REAL, INTENT(IN) :: rho, T
!!$    REAL             :: G
!!$    !-------------------------
!!$
!!$    G = (gam_VDW + 1)/2. 
!!$
!!$  END FUNCTION VDW_GAMMA__rho_T
!!$  !=======================


END MODULE VanDerWaals





