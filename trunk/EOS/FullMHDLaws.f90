
   MODULE FullMHDLaws

      USE LesTypes
      USE EOSLaws
      IMPLICIT NONE

      REAL, PRIVATE, SAVE :: d0, k0, Nu0, Eta0 !-- CCE R.B. 2008/11/04 : ils �taient recouverts par des variables locales (g95!)

   CONTAINS

      SUBROUTINE FullMHD_Init(Data)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "FullMHD_Init"
         TYPE(Donnees), INTENT(INOUT) :: Data
         LOGICAL :: existe
         INTEGER :: statut
         CHARACTER(LEN = *), PARAMETER :: fmhd = "FullMHD.data"

         INQUIRE(FILE = fmhd, EXIST = existe)
         IF (.NOT. existe) THEN
            PRINT *, mod_name, " ERREUR : le fichier ", fmhd, " n'existe pas"
            CALL stop_run()
         END IF

         OPEN(10, FILE = fmhd, IOSTAT = statut)
         IF (statut /= 0) THEN
            PRINT *, mod_name, " ERREUR :: ERREUR : statut == ", statut, " ouvrir ", fmhd
            CALL stop_run()
         END IF

         READ(10, *, IOSTAT = statut)  d0, k0, Nu0, Eta0
         IF (statut /= 0) THEN
            PRINT *, mod_name, " ERREUR :: ERREUR : statut == ", statut, " pour la 1ere ligne de ", fmhd
            CALL stop_run()
         END IF
!$!    READ( 10,*,IOSTAT=statut)  c__Phydro
!$!    Data%Phydro = c__Phydro
!$!    IF (statut /= 0) THEN
!$!       PRINT *, mod_name, " ERREUR :: ERREUR : statut == ", statut, " pour ligne phydro ", fvis
!$!       CALL stop_run()
!$!    END IF
         CLOSE(10)

      END SUBROUTINE FullMHD_Init

      REAL FUNCTION Diffusivity() RESULT(d)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "Diffusivity"
         d = d0
      END FUNCTION Diffusivity

      REAL FUNCTION Thermal_Conductivity() RESULT(k)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "Thermal_Conductivity"
         k = k0
      END FUNCTION Thermal_Conductivity

      REAL FUNCTION Viscosity() RESULT(Nu)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "Viscosity"
         Nu = Nu0
      END FUNCTION Viscosity

      REAL FUNCTION Resistivity() RESULT(Eta)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "Resistivity"
         Eta = Eta0
      END FUNCTION Resistivity

   END MODULE FullMHDLaws
