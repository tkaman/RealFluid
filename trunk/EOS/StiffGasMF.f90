!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   MODULE StiffGasMf
      USE LesTypes
      USE LibComm
      IMPLICIT NONE
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: gamma
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: pc
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: cv
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: gamma1
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: facteur !-- %%%%%%%%%%%%%%%%%% perte de pr�cision : 80 bits -> 64 bits
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: c__gamma !-- [KADNA-PROTECTED]
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: c__pc !-- [KADNA-PROTECTED]
      REAL, PRIVATE, SAVE, DIMENSION(: ), ALLOCATABLE :: c__cv !-- [KADNA-PROTECTED]
      INTEGER, PRIVATE, SAVE :: ndim, nvar, nfluid
      REAL, PRIVATE, SAVE :: AlpMin
      REAL, PRIVATE, SAVE :: AlpMax

  ! Vp = (/a1, a1*r1, a2*r2, u, v , ei , p , c, T1, T2 /)
  ! ----------------------------------------------------

      LOGICAL, PRIVATE, SAVE :: said = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said2 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said3 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said4 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said5 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said6 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said7 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said8 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: said9 = .FALSE.
      LOGICAL, PRIVATE, SAVE :: saida = .FALSE.
      LOGICAL, PRIVATE, SAVE :: saidb = .FALSE.
      LOGICAL, PRIVATE, SAVE :: saidc = .FALSE.
      CHARACTER(LEN = 1024), PRIVATE :: sayit

   CONTAINS
      SUBROUTINE SGMF_const(gam1, pinf1, cv1, gam2, pinf2, cv2)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_const"
         REAL, INTENT(OUT) :: gam1, pinf1, cv1, gam2, pinf2, cv2
         gam1 = gamma(1)
         pinf1 = pc(1)
         cv1 = cv(1)
         gam2 = gamma(2)
         pinf2 = pc(2)
         cv2 = cv(2)
      END SUBROUTINE SGMF_const

!!-- initialisations par lecture EOS_<n0>.data (suite de lecture, ouvert par EOS.f90/EOSinit)
!!-- allocations de tableaux (variables globales internes du module StiffGasMF); il y a Nfluid (===2) fluides diff�rents
      SUBROUTINE sgmf_init(MyUnit, Dim, Var)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "sgmf_init"
         INTEGER, INTENT(IN) ::MyUnit, Dim, Var
         INTEGER :: ie, statut
         LOGICAL, SAVE :: dit = .FALSE.

            READ(MyUnit, *, IOSTAT = statut) nfluid, AlpMin, AlpMax
            IF (statut /= 0) THEN
               PRINT *, mod_name, " ERREUR : statut==", statut, "pour la ligne nfluid"
               CALL stop_run()
            END IF
            IF (nfluid /= 2 .AND. nfluid /= 1 ) THEN
               PRINT *, mod_name, " ERREUR : : Uniquement du 2 fluids"
               CALL stop_run()
            END IF
            ALLOCATE( gamma(nfluid), gamma1(nfluid), pc(nfluid), cv(nfluid), facteur(nfluid) )
            ALLOCATE( c__gamma(nfluid), c__pc(nfluid), c__cv(nfluid))
            IF (see_alloc) THEN
               CALL alloc_allocate(mod_name, "gamma", Size(gamma))
               CALL alloc_allocate(mod_name, "gamma1", Size(gamma1))
               CALL alloc_allocate(mod_name, "pc", Size(pc))
               CALL alloc_allocate(mod_name, "cv", Size(cv))
               CALL alloc_allocate(mod_name, "facteur", Size(facteur))
            END IF
            DO ie = 1, nfluid
               READ(MyUnit, *) c__gamma(ie), c__pc(ie), c__cv(ie)
               IF (statut /= 0) THEN
                  PRINT *, mod_name, " ERREUR : statut==", statut, "pour la ligne gamma", ie
                  CALL stop_run()
               END IF
            END DO
            gamma = c__gamma
            pc = c__pc
            cv = c__cv

         facteur = 1.0 / gamma
         IF (.NOT. dit) THEN
            dit = .TRUE.
            WRITE(sayit, *) mod_name, " ATTENTION ATTENTION ATTENTION : stocker 1.0/gamma fait perdre en precision"
            CALL log_record(sayit)
         END IF

         gamma1 = gamma - 1.0
         ndim   = Dim
         nvar   = Var
      END SUBROUTINE sgmf_init


      FUNCTION SGMF_Gradt(Vp, ifluid) RESULT(gt)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Gradt"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL, DIMENSION(nvar)                :: gt
         INTEGER, INTENT(IN) :: ifluid
         IF (.NOT. saidc) THEN
            WRITE(message_log, FMT = *) mod_name, " :: ATTENTION : il faudrait calculer le gradient de T"
            CALL log_record(message_log)
            saidc = .TRUE.
         END IF
         gt = 0.0
      END FUNCTION SGMF_Gradt

      FUNCTION SGMF_Gradp(Vp) RESULT(gp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Gradp"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL, DIMENSION(nvar)          :: gp
         REAL :: q, gam1, RoKa, Roe, m

         RoKa   = KappaMel(Vp)
         Roe    = Vp(nvar) * (Vp(i_ar1_vp) + Vp(i_ar2_vp) )
         gam1   = 1.0 / RoKa
         q      = 0.5 * Sum(Vp(4: 3 + ndim) ** 2)

         m  =     ( Vp(i_pre_vp) + gamma(2) * pc(2) ) / gamma1(2) &
         & - ( Vp(i_pre_vp) + gamma(1) * pc(1) ) / gamma1(1)

         gp(1)        =  gam1 * m
         gp(2: 3)      =  gam1 * q * (/ 1.0, 1.0 /)
         gp(4: 3 + ndim) = - gam1 * Vp(4: 3 + ndim)
         gp(nvar)     =  gam1

      END FUNCTION SGMF_Gradp



      REAL FUNCTION SGMF_Kappa() RESULT(k)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Kappa"
         k  = gamma1(1)
      END FUNCTION SGMF_Kappa

      REAL FUNCTION SGMF_Cp() RESULT(cp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Cp"
    !    Cp = Gamma*Cv
         cp  = gamma(1) * cv(1)

      END FUNCTION SGMF_Cp


      REAL FUNCTION SGMF_RhoEpsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_RhoEpsilon"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp

         REAL :: RhoEpsRef, RoKa

    ! RhoEpsRefini de m�lange
         RhoEpsRef = RhoEpsRefMel(Vp)

    ! gamma de m�lange
         RoKa =  KappaMel(Vp)

         re  = RoKa * Vp(i_pre_vp)  + RhoEpsRef

      END FUNCTION SGMF_RhoEpsilon




      REAL FUNCTION SGMF_Epsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Epsilon"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
    ! rho*epsilon = (p +Gamma*Pc)/(Gamma-1)


         REAL :: RhoEpsRef, RoKa

    ! RhoEpsRefini de m�lange
         RhoEpsRef = RhoEpsRefMel(Vp)

    ! gamma de m�lange
         RoKa =  KappaMel(Vp)

         re   = RoKa * Vp(i_pre_vp)  + RhoEpsRef

         re   = re / ( Vp(i_ar1_vp) + Vp(i_ar2_vp) )

      END FUNCTION SGMF_Epsilon


      REAL FUNCTION SGMF_EpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_EpsilonDet"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp

         REAL ::z1

         z1 = filtremf(Vp(i_alp_vp))

    ! rho*epsilon = (p +Gamma*Pc)/(Gamma-1)
         are  =  z1 * ( Vp(i_tm1_vp) * cv(1) + pc(1)) + &
         & (1.0 - z1) * ( Vp(i_tm2_vp) * cv(2) + pc(2))

         PRINT *, mod_name, " ERREUR : STOP reste dans le code ************************** "
         CALL stop_run() !-- pourquoi ? %%%%%%%%%%%%%%%%%%%%
      END FUNCTION SGMF_EpsilonDet


      REAL FUNCTION SGMF_RhoEpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_RhoEpsilonDet"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
    ! rho*epsilon = (p +Gamma*Pc)/(Gamma-1)

         REAL ::z1

         z1 = filtremf(Vp(i_alp_vp))

         are  =  z1 * (Vp(i_tm1_vp) * cv(1) + pc(1)) + &
         & (1.0 - z1) * (Vp(i_tm2_vp) * cv(2) + pc(2))

         are  = are * (Vp(i_ar1_vp) + Vp(i_ar2_vp) )

         PRINT *, mod_name, " ERREUR : STOP reste dans le code ************************** "
         CALL stop_run() !-- pourquoi ? %%%%%%%%%%%%%%%%%

      END FUNCTION SGMF_RhoEpsilonDet


      REAL FUNCTION SGMF_Rhoh(Vp) RESULT(rh)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Rhoh"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
    ! rho*epsilon = (p +Gamma*Pc)/(Gamma-1)

         REAL :: RhoEpsRef, RoKa, re


    ! RhoEpsRefini de m�lange
         RhoEpsRef = RhoEpsRefMel(Vp)
    ! gamma de m�lange
         RoKa = KappaMel(Vp)


         re   = RoKa * Vp(i_pre_vp)  + RhoEpsRef

         rh   = Vp(i_pre_vp) + re

         PRINT *, mod_name, " ERREUR : STOP reste dans le code ************************** "
         CALL stop_run() !-- pourquoi ? %%%%%%%%%%%%%%%%%
      END FUNCTION SGMF_Rhoh


      REAL FUNCTION SGMF_Temperature(Vp) RESULT(Temp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Temperature"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL   :: t1, t2

         t1   = SGMF_Temperaturem(Vp, 1)
         t2   =  SGMF_Temperaturem(Vp, 1)
         Temp = 0.5 * (t1 + t2)

         PRINT *, mod_name, ' ERREUR : passage dans SGMF_Temperature'
         CALL stop_run()

      END FUNCTION SGMF_Temperature



      REAL FUNCTION SGMF_Rhom(p, s, m) RESULT(rho)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Rhom"
         INTEGER, INTENT(IN) :: m
         REAL, INTENT(IN) :: p, s

         IF (s == 0.0) THEN !-- CCE 2007/04/17
            IF (.NOT. said4) THEN
               said4 = .TRUE.
               PRINT *, mod_name, " ERREUR : s==0.0"
               CALL stop_run()
            END IF
            rho = 1.0
         ELSE IF (p + pc(m) < 0.0) THEN
            IF (.NOT. said5) THEN
               said5 = .TRUE.
               PRINT *, mod_name, " ERREUR : p+pc==0.0", p, m, pc(m)
               CALL stop_run()
            END IF
            rho = 1.0
         ELSE IF ((p + pc(m)) / s < 0.0) THEN
            IF (.NOT. saidb) THEN
               saidb = .TRUE.
               PRINT *, mod_name, " ERREUR : (p+pc)/s<=0.0", p, m, pc(m), s
               CALL stop_run()
            END IF
            rho = 1.0
         ELSE IF (gamma(m) <= 0.0) THEN
            IF (.NOT. said6) THEN
               said6 = .TRUE.
               PRINT *, mod_name, " ERREUR : gamma<=0.0", m, gamma(m)
               CALL stop_run()
            END IF
            rho = 1.0
         ELSE
!            rho   = ( ( p +  pc(m) )/s )**( 1.0/gamma(m) )
            rho   = ( ( p +  pc(m) ) / s ) ** facteur(m)
         END IF

      END FUNCTION SGMF_Rhom


      REAL FUNCTION SGMF_Entropiem(Vp, m) RESULT(Ent)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Entropiem"
         INTEGER, INTENT(IN) :: m
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL   :: rom

         IF (m < nfluid ) THEN
            IF (Vp(i_alp_vp) < 1.0e-08) THEN
               Ent = 0.0
               RETURN
            END IF
            rom = Vp(i_ar1_vp) / Vp(i_alp_vp)
         ELSE
            IF (1.0 - Vp(i_alp_vp) < 1.0e-08) THEN
               Ent = 0.0
               RETURN
            END IF
            rom = Vp(i_ar2_vp) / (1.0 - Vp(i_alp_vp))
         END IF

         IF (rom <= 0.0) THEN !-- CCE 2007/04/17
            IF (.NOT. said2) THEN
               PRINT *, mod_name, " :: ERREUR : rom=", rom
               PRINT *, "m=", m
               PRINT *, "gamma=", gamma(m)
               PRINT *, "pc=", pc(m)
               PRINT *, "vp=", Vp
               said2 = .TRUE.
               CALL stop_run()
            END IF
            Ent = 0.0
         ELSE IF (rom ** gamma(m) <= 1.0d-200) THEN !-- CCE 2007/04/17
            IF (.NOT. saida) THEN
               PRINT *, mod_name, " :: ERREUR : rom**gamma=0.0", rom
               PRINT *, "m=", m
               PRINT *, "gamma=", gamma(m)
               PRINT *, "pc=", pc(m)
               PRINT *, "vp=", Vp
               saida = .TRUE.
               CALL stop_run()
            END IF
            Ent = 0.0
         ELSE
            Ent = ( Vp( nvar + 1) + pc(m) ) / ( rom ** gamma(m) )
         END IF

      END FUNCTION SGMF_Entropiem



      REAL FUNCTION SGMF_Temperaturem(Vp, m) RESULT(Temp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Temperaturem"
         INTEGER, INTENT(IN) :: m
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL   :: rom, Eps

         Temp = 0.0

         IF (m < nfluid ) THEN
            IF (Vp(i_alp_vp) < 1.0e-08) THEN
               Temp = 0.0
               RETURN
            END IF
            rom = Vp(i_ar1_vp) / Vp(i_alp_vp)
         ELSE
            IF (1.0 - Vp(i_alp_vp) < 1.0e-08) THEN
               Temp = 0.0
               RETURN
            END IF
            rom = Vp(i_ar2_vp) / (1.0 - Vp(i_alp_vp))
         END IF

!         Eps  = 1.0/gamma1(m)
         Eps  = facteur(m)
         IF (rom == 0.0) THEN
            IF (.NOT. said9) THEN
               PRINT *, mod_name, " :: ERREUR rom==0.0", Vp
               said9 = .TRUE.
               CALL stop_run()
               Temp = 0.0
            END IF
         ELSE
            Temp = Eps * ( Vp(i_pre_vp) + gamma(m) * pc(m) ) / (rom * cv(m))
         END IF


      END FUNCTION SGMF_Temperaturem

!!-- Impedance moyenne
      REAL FUNCTION SGMF_Impm(Vp, m) RESULT(Imp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Impm"
         INTEGER, INTENT(IN) :: m
         REAL, DIMENSION(: ), INTENT(IN) :: Vp

         IF (Abs(pc(m) + Vp(i_pre_vp)) < 1.0d-200) THEN
            IF (.NOT. said) THEN
               said = .TRUE.
               PRINT *, "m=", m
               PRINT *, "GAMMA=", gamma(m)
               PRINT *, "PC=", pc(m)
               PRINT *, "Vp=", Vp(i_pre_vp)
               PRINT *, mod_name, " ERREUR : PC+Vp == 0.0"
               CALL stop_run()
            END IF
            Imp = 1.0
         ELSE
            Imp  = facteur(m) / ( Vp(i_pre_vp) + pc(m) ) !-- %%%%%%%% il faut absolument arriver � inliner cette fonction
         END IF

      END FUNCTION SGMF_Impm




      REAL FUNCTION SGMF_Pression(Vp) RESULT(p)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Pression"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
    ! P = (Gamma - 1)*rho*epsilon - Gamma*Pc
         REAL   :: Epsilon, rho
    !           if (Ua(ifl, 1, is) /= 0 .and. Ua(ifl, 2, is)/=0) then


         REAL :: RhoEpsRef, RoKa

    ! RhoEpsRefini de m�lange
         RhoEpsRef =  RhoEpsRefMel(Vp)

    ! gamma de m�lange
         RoKa =  KappaMel(Vp)

         rho  = Vp(i_ar1_vp) + Vp(i_ar2_vp)
         Epsilon = Vp(4 + ndim)

         p = (rho * Epsilon - RhoEpsRef ) / RoKa

      END FUNCTION SGMF_Pression


      REAL FUNCTION SGMF_Son(Vp) RESULT (c)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_Son"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp

         REAL :: Imp1, Imp2, z1, rho, Imp

         z1 = filtremf(Vp(i_alp_vp))

         Imp1  = SGMF_Impm(Vp, 1)
         Imp2  = SGMF_Impm(Vp, 2)

         Imp   = z1 * Imp1 + (1.0 - z1) * Imp2

         rho = Vp(i_ar1_vp) + Vp(i_ar2_vp)

         IF (rho == 0.0) THEN
            IF (.NOT. said3) THEN
               PRINT *, mod_name, " ERREUR : rho==0.0"
               said3 = .TRUE.
               CALL stop_run()
            END IF
            c = 1.0e+30
         ELSE IF (Imp == 0.0) THEN
            IF (.NOT. said7) THEN
               PRINT *, mod_name, " ERREUR : Imp==0.0"
               said7 = .TRUE.
               CALL stop_run()
            END IF
            c = 1.0e+30
         ELSE IF (rho * Imp < 0.0) THEN
            IF (.NOT. said8) THEN
               PRINT *, mod_name, " :: ERREUR : rho*Imp <0.0", rho, Imp
               PRINT *, "VP==", Vp
               said8 = .TRUE.
               CALL stop_run()
            END IF
            c = 1.0e+30
         ELSE
            c = 1.0 / Sqrt( rho * Imp )
         END IF

      END FUNCTION SGMF_Son


      SUBROUTINE SGMF_StiffenedGas(Vp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SGMF_StiffenedGas"
         REAL, DIMENSION(: ), INTENT(INOUT) :: Vp
    ! P = (Gamma - 1)*rho*epsilon - Gamma*Pc

         Vp(i_pre_vp) = SGMF_Pression(Vp)
         Vp(i_son_vp) = SGMF_Son(Vp)
         Vp(i_tm1_vp) = SGMF_Temperaturem(Vp, 1)
         Vp(i_tm2_vp) = SGMF_Temperaturem(Vp, 2)
      END SUBROUTINE SGMF_StiffenedGas


      REAL FUNCTION filtremf(alp) RESULT (z1)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "filtremf"
         REAL, INTENT(IN) :: alp
         z1 = alp

         IF (alp > AlpMax) THEN
            z1 = 1.0
         END IF
         IF (alp < AlpMin) THEN
            z1 = 0.0
         END IF

      END FUNCTION filtremf

      REAL FUNCTION RhoEpsRefMel(Vp) RESULT(RhoEpsRefm)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "RhoEpsRefMel"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: z1, z2
    ! RhoEpsRefini de m�lange
         z1 = filtremf(Vp(i_alp_vp))
         z2 = 1.0 - z1

         RhoEpsRefm = z1 * gamma(1) * pc(1) / gamma1(1) + z2 * gamma(2) * pc(2) / gamma1(2)

      END FUNCTION RhoEpsRefMel

      REAL FUNCTION KappaMel(Vp) RESULT(KappaMelm)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "KappaMel"
         REAL, DIMENSION(: ), INTENT(IN) :: Vp
         REAL :: z1, z2
    !
         z1 = filtremf(Vp(i_alp_vp))
         z2 = 1.0 - z1

         KappaMelm =  z1 / gamma1(1) + z2 / gamma1(2)
      END FUNCTION KappaMel

   END MODULE StiffGasMf

