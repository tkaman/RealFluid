!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
!*                                                                           */
!*                  This file is part of the FluidBox program                */
!*                                                                           */
!* Copyright (C) 2000-2008 B.Nkonga                                         */
!*                                                                           */
!*                  2000-2008 Institut de Math�matiques de Bordeaux          */
!*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
!*                                                                           */
!*  FluidBox is distributed under the terms of the Cecill-B License.         */
!*                                                                           */
!*  You can find a copy of the Cecill-B License at :                         */
!*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
!*                                                                           */
!* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

   MODULE stiffenedgas
      USE LesTypes
      USE LibComm
      IMPLICIT NONE
      REAL, PRIVATE, SAVE :: gama
      REAL, PRIVATE, SAVE :: pc
      REAL, PRIVATE, SAVE :: cv
      REAL, PRIVATE, SAVE :: gama1
      INTEGER, PRIVATE, SAVE :: ndim, nvar

   CONTAINS


      SUBROUTINE sg_init(MyUnit, Dim, Var)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "sg_init"
         INTEGER, INTENT( IN) ::MyUnit, Dim, Var
         READ(MyUnit, *)gama
         READ(MyUnit, *)pc
         READ(MyUnit, *)cv
         gama1 = gama -1.0
         ndim = Dim
         nvar = Var
      END SUBROUTINE sg_init



      REAL FUNCTION SG_Kappa() RESULT(k)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_Kappa"
         k  = gama1
         RETURN
      END FUNCTION SG_Kappa

      REAL FUNCTION SG_Cp() RESULT(cp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_Cp"
!    Cp = gama*Cv
         cp  = gama * cv
         RETURN
      END FUNCTION SG_Cp


      REAL FUNCTION SG_RhoEpsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_RhoEpsilon"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         re  = (Vp(nvar + 1) + gama * pc) / gama1
         RETURN
      END FUNCTION SG_RhoEpsilon

      REAL FUNCTION SG_Epsilon(Vp) RESULT(re)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_Epsilon"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         re  = (Vp(nvar + 1) + gama * pc) / gama1 / Vp(1)
         RETURN
      END FUNCTION SG_Epsilon


      REAL FUNCTION SG_EpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_EpsilonDet"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         are  = cv * Vp(nvar + 3) + pc
         RETURN
      END FUNCTION SG_EpsilonDet


      REAL FUNCTION SG_RhoEpsilonDet(Vp) RESULT(are)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_RhoEpsilonDet"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         are  = Vp(1) * (cv * Vp(nvar + 3) + pc)
         RETURN
      END FUNCTION SG_RhoEpsilonDet


      REAL FUNCTION SG_Rhoh(Vp) RESULT(rh)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_Rhoh"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! rho*epsilon = (p +gama*Pc)/(gama-1)
         rh  = (Vp(nvar + 1) + pc) * gama / gama1
         RETURN
      END FUNCTION SG_Rhoh


      REAL FUNCTION SG_Temperature(Vp) RESULT(Temp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_Temperature"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! T =(epsilon-Pc/rho)/cv
         REAL   :: Epsilon, rho
    !           if (Ua(ifl,1,is) /= 0 .and. Ua(ifl,2,is)/=0) then
         rho  = Vp(1)
         Epsilon = Vp(ndim + 2)
         Temp = ( (Epsilon - pc / rho) ) / cv
         RETURN
      END FUNCTION SG_Temperature


      REAL FUNCTION SG_Pression(Vp) RESULT(p)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_Pression"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
    ! P = (gama - 1)*rho*epsilon - gama*Pc
         REAL   :: Epsilon, rho
    !           if (Ua(ifl,1,is) /= 0 .and. Ua(ifl,2,is)/=0) then
         rho  = Vp(1)
         Epsilon = Vp(2 + ndim)   ! �nergie interne
         p = rho * (gama1 * Epsilon) - gama * pc
         RETURN
      END FUNCTION SG_Pression


      REAL FUNCTION SG_Son(Vp) RESULT (c)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_Son"
         REAL, DIMENSION(: ), INTENT( IN) :: Vp
         c = gama * (Vp(nvar + 1) + pc) / Vp(1) ! C^2
         c = Sqrt(c)
         RETURN
      END FUNCTION SG_Son

      SUBROUTINE SG_StiffenedGas(Vp)
         CHARACTER(LEN = *), PARAMETER :: mod_name = "SG_StiffenedGas"
         REAL, DIMENSION(: ), INTENT(INOUT) :: Vp
     ! P = (gama - 1)*rho*epsilon - gama*Pc
         REAL   :: Epsilon, rho
         rho  = Vp(1)
         Epsilon = Vp(2 + ndim)      ! �nergie interne
         Vp(nvar + 1) = rho * gama1 * Epsilon - gama * pc
         Vp(nvar + 2) = Sqrt( gama * (Vp(nvar + 1) + pc) / rho)
         Vp(nvar + 3) = (Epsilon - pc / rho) / cv
      END SUBROUTINE SG_StiffenedGas


   END MODULE stiffenedgas
