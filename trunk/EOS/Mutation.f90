MODULE MUTATION

  USE LesTypes
  USE LibComm

  IMPLICIT NONE

  ! Variables read
  REAL,    PRIVATE, SAVE :: omega_prsv, nexp, k1, cvinf

  ! Variables calculated
  REAL,    PRIVATE, SAVE :: apr, bpr, alfa_om, z_c

  REAL, PRIVATE, PARAMETER :: TOL = 1.0E-13;


  !*****************************************************
  !       FILE EOS_1.data ; PRSV Gas Case              *
  !*****************************************************
  !"PRSV"                 !EosLaw                      * 
  !0.4833                 !omega "acentric factor"     *
  !0.5255                 !nexp                        *
  !0.04285                !k1                          *
  !1                      !cvinf(Tc)/R_gas             * !Dimensionless
  !*****************************************************

  !-----------------------------------------------------
  !       Other Values                                 -
  !-----------------------------------------------------
  !z_c     = 0.3112      !Constant for PRSV Model      -
  !apr     = f(z_c)                                    - !Dimensionless
  !bpr     = f(z_c)                                    - !Dimensionless
  !alfa_om = f(omega)    !k0                           -
  !-----------------------------------------------------

CONTAINS

  !=====================================
  SUBROUTINE Mutation_init(MyUnit, Dim, Var)
  !=====================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "prsv_init"

    INTEGER, INTENT(IN) :: MyUnit, Dim, Var
    !---------------------------------------

    INTEGER :: statut
    !---------------------------------------

    !--------
    !Read Values
    !--------
    !omega "acentric factor"
    READ(MyUnit, *, IOSTAT = statut) omega_prsv
    !Data%AcentricFactor    = omega

    !nexp
    READ(MyUnit, *, IOSTAT = statut) nexp

    !k1
    READ(MyUnit, *, IOSTAT = statut) k1

    !cvinf(Tc)
    READ(MyUnit, *, IOSTAT = statut) cvinf


    !--------
    !Calculation of Values
    !--------
    !z_c
    z_c      = 0.311155425170673d0

    !apr = f(z_c)
    apr = 0.457235d0 / z_c**2

    !bpr = f(z_c)
    bpr = 0.077796 / z_c

    !alpha_om      -k0
    alfa_om = 0.378893d0 + 1.4897153*omega_prsv - 0.17131848*omega_prsv**2. &
         + 0.0196554*omega_prsv**3.

  END SUBROUTINE Mutation_init
  !========================

  !==============================
  FUNCTION Mutation_omega() RESULT(w)
  !==============================

    IMPLICIT NONE

    REAL :: w

    w = omega_prsv

  END FUNCTION Mutation_omega
  !======================  

  !============================================================================
  !============================================================================
  !BEGIN :           basic FUNCTIONS

  !-------------------------------
  ! > PRSV_P__rho_T(rho,T)          P = P(rho  , T       )  
  ! > PRSV_T__rho_P(rho,P)          T = T(rho  , P       )
  ! > PRSV_T__rho_rhoe(rho,rhoe)    T = T(rho  , rho*e   ),     e := internal energy
  ! > PRSV_rho__P_T(P,T)            rho = rho(P, T       )  
  ! > PRSV_c__rho_T(rho,T)          c = c(rho  , T       )
  ! > PRSV_e__rho_T(rho,T)          e = e(rho  , T       ),     e := internal energy
  ! > PRSV_s__rho_T(rho,T)          s = s(rho  , T       ),     s := entropy
  ! > PRSV_rho__s_T(s  ,T)        rho = rho(s  , T       ),     s := entropy
  ! > PRSV_T__rho_s(rho, s)         T = T(rho  , s       ),     s := entropy
  ! > PRSV_P__rho_rhoU_rhoE(rho,rhoU,rhoE)     P = P(rho, rho*|u|, rho*E)  E := total energy = e + |u|**2/2
  ! > PRSV_dP_de__rho_T(rho,T)                (dP/de)|rho    = dP/de(rho, T)   e:= internal energy
  ! > PRSV_dP_drho__rho_T(rho,T)              (dP/drho)|e    = dP/drho(rho, T)   e:= internal energy
  ! > PRSV_pi_rho__rho_u_T(rho,u,T)           (dPI/drho)     = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > PRSV_pi_rhoe__rho_T(rho,T)               dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > PRSV_GAMMA__rho_T(rho,T)                 GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 
  ! > PRSV_de_drho__rho_T(rho, T)              (de/drho)|T    = de/drho(rho,T)
  ! > PRSV_dT_de__rho_T(rho, T)                (dT/de)_rho    = dT/de(rho, T)
  ! > PRSV_dT_drho__rho_T(rho, T)              (dT/drho)_e    = dT/drho(rho, T)
  ! > PRSV_TEICALC(h, rho, c1, c2)             Calcul sp�cifique entr�e turbine (lgc = 91)
  ! > PRSV_dP__dT_dv(rho, T)                   dP/dT|_v & dP/dv|_T
  ! > PRSV_dP2__dT_dv(rho, T)                  d^2P/dT^2|_v & d^2P/dv^2|_T & d^2P/dvdT|_T

  !============================================
  REAL FUNCTION PRSV_P__rho_T(rho, T) RESULT(P)
  !============================================
  ! > P = P(rho,T)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: ka, alfa, v
    !-------------------------

    v = 1.d0/rho
    
    ka = alfa_om + k1*(1.0d0 + SQRT(T))*(0.7d0 - T)

    alfa = ( 1.d0 + ka*(1.d0 - SQRT(T)) )**2

    P = T/( Z_c*(v - bpr) ) - apr*alfa/( v**2 + 2*v*bpr - bpr**2 )

  END FUNCTION PRSV_P__rho_T
  !=========================

  !==========================================
  REAL FUNCTION PRSV_T__rho_P(rho, P) RESULT(T)
    !==========================================
    ! > T = T(rho,P)

    REAL, INTENT(IN) :: rho, P
    !-------------------------

    REAL             :: ka, alfa, T_1,  dkat, bco, dalfat, dprtr
    !-------------------------

    INTEGER          :: i, i_conv, itermax
    !-------------------------

    !...............
    !Newton m�thod
    !...............

    !INITIALIZATION
    T_1        = 0.9d0

    itermax    = 150 
    i_conv     = 0 

    DO  i = 1, itermax

       T     = T_1

       ka    = alfa_om + k1*(1.0d0 + T**0.5d0)*(0.7d0 - T)
       dkat  = k1* (-1.0d0 + 0.7d0*0.5d0/T**0.5 - 1.5d0*T**0.5)
       alfa  = (1.0d0 + ka*(1.0d0 - T**0.5))**2
       bco   = (dkat - T**0.5*dkat - 0.5*ka/T**0.5)
       dalfat= 2.0d0*alfa**0.5*bco

       dprtr = 1.0d0/(1.0d0/rho - bpr)/z_c - apr*dalfat/(1.0d0/rho**2. &
            + 2.0d0*bpr/rho - bpr**2.)

       T_1   = T - (P - T/(1.0d0/rho - bpr)/z_c + (apr*alfa)/(1.0d0/rho**2. &
            + 2.0d0*bpr/rho - bpr**2.))/(-dprtr)

       IF( ABS(T_1 - T)/T .le. TOL) THEN
          T      = T_1
          i_conv = 1
          EXIT
       END IF

    END DO

    IF (i_conv .ne. 1) THEN
       print*, "ERROR in T CALCULATION"
       print*, "PRSV.f90 - PRSV_T__rho_P(rho,P)"
       print*, "P = ", P, "rho = ", rho
       print*, "No Convergence in ", itermax, "iterations"
       print*, "T = ", T, "T_1 = ", T_1
       print*, "Err = ", ABS(T_1 - T)/T_1, "> TOL = ", TOL
       PAUSE
    END IF


  END FUNCTION PRSV_T__rho_P
  !=======================


  !==========================================
  REAL FUNCTION PRSV_T__rho_rhoe(rho, rhoe) RESULT(T)
    !==========================================
    ! >  T = T(rho, rho*e), e := intern energy

    REAL, INTENT(IN) :: rho, rhoe
    !-------------------------

    REAL             :: T_1, pic, ka, dkat, d2kat, alfa, bco, dalfat, dbcot, d2alfat, e, num, dem
    !-------------------------

    INTEGER          :: i, i_conv, itermax
    !-------------------------
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    !TEST
    !PRINT*,"T(rho,rhoe) : rho = ", rho, "e = ", rhoe / rho


    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




    !...............
    !Newton m�thod
    !...............

    !INITIALIZATION
    !c ho tolto il termine relativo a mu per l'a-dim cvinf/(nexp+1)/z_c
    pic = 1.0d0/bpr/(8.0d0**0.5)*LOG(ABS((2.0d0/rho + 2.0d0*bpr + &
         bpr*(8.0d0**0.5))/(2.0d0/rho + 2.0d0*bpr - bpr*(8.0d0**0.5))))

    T_1        = 1.0d0
    itermax    = 100
    i_conv     = 0 


    !LOOP
    do i = 1, itermax

       T      = T_1

       ka     = alfa_om + k1*(1.0d0 + T**0.5)*(0.7d0 - T)
       dkat   = k1*(-1.0d0 + 0.7d0*0.5d0/T**0.5 - 1.5d0*T**0.5)
       d2kat  = k1*(-0.7d0*0.25d0/T**1.5 - 1.5d0*0.5d0/T**0.5)

       alfa   = (1.0d0 + ka*(1.0d0 - T**0.5))**2.0d0
       bco    = (dkat - T**0.5*dkat - 0.5*ka/T**0.5)
       dalfat = 2.0d0*alfa**0.5*bco
       dbcot  = d2kat - T**0.5*d2kat - 1.0d0/T**0.5*dkat + 0.25d0*ka/T**1.5
       d2alfat= bco/alfa**0.5*dalfat + 2.0*alfa**0.5*dbcot

       e      = cvinf/(nexp + 1)/z_c*(abs(T))**(nexp + 1) + &
            pic*apr*(T*dalfat - alfa)

       num    = e - rhoe/rho
       dem    = cvinf/z_c*T**nexp + T*d2alfat*apr*pic

       T_1    = T - num/dem

       IF( ABS(T_1 - T)/T_1 .le. TOL) THEN
          T      = T_1
          i_conv = 1
          EXIT
       END IF

    END DO

    IF (i_conv .ne. 1) THEN
       print*, "ERROR in EOS - T CALCULATION"
       print*, "PRSV.f90 - PRSV_T__rho_rhoe(rho, rhoe)"
       print*, "rho = ", rho, "rhoe = ", rhoe
       print*, "No Convergence in ", itermax, "iterations"
       print*, "T = ", T, "T_1 = ", T_1
       print*, "Err = ", ABS(T_1 - T)/T_1, "> TOL = ", TOL
       PAUSE
    END IF

  END FUNCTION PRSV_T__rho_rhoe
  !=======================

  !==========================================
  REAL FUNCTION PRSV_rho__P_T(P, T) RESULT(rho)
    !==========================================
    ! >  rho = rho(P,T)  

    REAL, INTENT(IN) :: P, T
    !-------------------------

    INTEGER          :: i, i_conv, itermax
    !-------------------------

    REAL             :: rho_1, ka, alfa, f, f_der
    !-------------------------

    !...............
    !Newton m�thod
    !...............

    !INITIALIZATION
    rho_1      = 0.01d0
    i_conv     = 0 
    itermax    = 400 

    ka         = alfa_om + k1*(1. + T**0.5)*(0.7 - T)
    alfa       = (1. + ka*(1. - T**0.5))**2.

    !LOOP
    DO i = 1, itermax

       rho   = rho_1 
       f     = T/z_c/rho**2.0d0/(1./rho - bpr)**2.0d0 - 2.0d0*apr*alfa/ &
            (1./rho**2. + 2.*bpr/rho - bpr**2.)**2.0d0*(1./rho**3.0d0 + bpr/rho**2.0d0)

       IF( f .eq. 0.0d0) THEN
          i_conv = 1
          EXIT
       END IF

       f_der = T/(1./rho - bpr)/z_c - (apr*alfa)/(1./rho**2. + 2.*bpr/rho - bpr**2.) - P

       rho_1 = rho - f_der/f

       IF( ABS(rho_1 - rho)/rho_1 .le. TOL) THEN
          rho    = rho_1
          i_conv = 1
          EXIT
       END IF

    END DO

    IF (i_conv .ne. 1) THEN
       print*, "ERROR in EOS - rho CALCULATION"
       print*, "PRSV.f90 - PRSV_rho__P_T(P,T)"
       print*, "P = ", P, "T = ", T
       print*, "No Convergence in ", itermax, "iterations"
       print*, "rho = ", rho, "rho_1 = ", rho_1
       print*, "Err = ", ABS(rho_1 - rho)/rho_1, "> TOL = ", TOL
       PAUSE
    END IF

  END FUNCTION PRSV_rho__P_T
  !=======================


  !==========================================
  REAL FUNCTION PRSV_c__rho_T(rho, T) RESULT(c)
    !==========================================
    !  > c = c(rho,T)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, dkat, d2kat, d3kat, alfa, bco, dalfat, dbcot, d2alfat, d3alfat, cvr, & 
         dprvr, dprtr, d2prtr, d2prvr, d2prtrvr
    !-------------------------

    ka      = alfa_om + k1*(1.0d0 + T**0.5)*(0.7d0 - T) 
    dkat    = k1* (-1.0d0 + 0.7d0*0.5d0/T**0.5d0 - 1.5d0*T**0.5) 
    d2kat   = k1* (-0.7d0*0.25d0/T**1.5 - 1.5d0*0.5d0/T**0.5 )
    d3kat   = k1* (0.7d0*0.25d0*1.5d0/T**2.5 + 1.5d0*0.25d0/T**1.5 ) 

    alfa    = (1.0d0 + ka*(1.0d0 - T**0.5))**2.0d0
    bco     = (dkat - T**0.5*dkat - 0.5d0*ka/T**0.5)    
    dalfat  = 2.0d0*alfa**0.5*bco

    dbcot   = d2kat - T**0.5*d2kat - 1.0d0/T**0.5*dkat + 0.25*ka/T**1.5
    d2alfat = bco/alfa**0.5*dalfat + 2.0d0*alfa**0.5*dbcot 

    d3alfat = 2.0d0*dbcot/alfa**0.5*dalfat - 0.5*bco*(dalfat**2.0d0)/alfa**1.5    &
         + bco/alfa**0.5*d2alfat + 2.0d0*alfa**0.5*(d3kat - T**0.5*d3kat -     &
         1.5d0/T**0.5*d2kat + 0.75d0/T**1.5*dkat - 0.25d0*1.5d0*ka/T**2.5  ) 


    cvr     = cvinf/z_c*T**nexp + T*d2alfat*apr/bpr/(8.0d0**0.5)*    &
         log(abs((2.0d0/rho+2.0d0*bpr + bpr*(8.0d0**0.5)) / (2.0d0/rho +     &
         2.0d0*bpr - bpr*(8.0d0**0.5))))


    dprvr   = (-T/z_c/(1.0d0/rho - bpr)**2.0d0 + alfa*apr*(2.0d0/rho + 2.0d0*bpr)/    &
         (1.0d0/rho**2.0d0+2.0d0/rho*bpr-bpr**2.0d0)**2.0d0)


    dprtr   = 1.0d0/(1.0d0/rho - bpr)/z_c - apr*dalfat/(1.0d0/rho**2.0d0     &
         + 2.0d0/rho*bpr - bpr**2.0d0)


    d2prtr  = -apr*d2alfat/(1.0d0/rho**2.0d0 + 2.0d0/rho*bpr - bpr**2.0d0)


    d2prvr  = 2.0d0*T/z_c/(1.0d0/rho - bpr)**3.0d0 + alfa*apr*2.0d0*    &
         (-3.0d0/rho**2.0d0 - 6.0d0*bpr/rho - 5.0d0*bpr**2.0d0)/    &
         ((1.0d0/rho**2.0d0+2.0d0/rho*bpr-bpr**2.0d0)**3.0d0)


    d2prtrvr = -1.0d0/(1/rho - bpr)**2.0d0/z_c + apr*(2.0d0/rho + 2.0d0*bpr)*    &
         dalfat/((1.0d0/rho**2.0d0 + 2.0d0/rho*bpr - bpr**2.0d0)**2.0d0)


    c        = SQRT(1/rho**2*(T/cvr*dprtr**2 - dprvr))	

  END FUNCTION PRSV_c__rho_T
  !=======================

  !==========================================
  REAL FUNCTION PRSV_e__rho_T(rho, T) RESULT(e)
    !==========================================
    !  > e = e(rho,T), e := internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, dkat, d2kat, alfa, bco, dalfat, dbcot, d2alfat
    !-------------------------

    ka      = alfa_om + k1*(1.0d0 + T**0.5)*(0.7d0 - T)
    dkat    = k1* (-1.0d0 + 0.7d0*0.5d0/T**0.5 - 1.5d0*T**0.5 )
    d2kat   = k1* (-0.7d0*0.25d0/T**1.5 - 1.5d0*0.5d0/T**0.5 )

    alfa    = (1.0d0 + ka*(1.0d0 - T**0.5))**2.
    bco     = (dkat - T**0.5*dkat - 0.5d0*ka/T**0.5  )
    dalfat  = 2.0d0*alfa**0.5*bco
    dbcot   = d2kat - T**0.5*d2kat - 1.0d0/T**0.5*dkat + 0.25d0*ka/T**1.5
    d2alfat = bco/alfa**0.5*dalfat + 2.0d0*alfa**0.5d0*dbcot


    e       = cvinf/(nexp + 1)/z_c*(abs(T))**(nexp + 1.0d0) + 1.0d0/bpr/    &
         (8.0d0**0.5)*log(abs((2.0d0/rho + 2.0d0*bpr + bpr*(8.0d0**0.5)) /    &
         (2.0d0/rho + 2.0d0*bpr - bpr*(8.d0**0.5))))*apr*(T*dalfat - alfa)

  END FUNCTION PRSV_e__rho_T
  !=======================

  !==========================================
  REAL FUNCTION PRSV_s__rho_T(rho, T) RESULT(s)
    !==========================================
    !  > s = s(rho,T), s := entropy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: alfa, dalfat
    !-------------------------

    alfa   = ( 1d0 + alfa_om*(1d0 - T**0.5) )**2.
    dalfat = -alfa_om*( 1d0/T**0.5 + alfa_om*(1d0/T**0.5 - 1d0) )

    s      = cvinf/nexp/z_c*(ABS(T)**nexp - 1d0) + &
         1d0/z_c*(  LOG( (1d0/rho - bpr)/(1d0 - bpr) )  ) + &
         apr/(8d0)**0.5/bpr*dalfat*(  LOG( (1d0/rho + bpr + bpr*(2d0)**0.5)/ &
         (1d0/rho + bpr - bpr*(2d0)**0.5) )  )


  END FUNCTION PRSV_s__rho_T
  !=======================

  !==========================================
  REAL FUNCTION PRSV_rho__s_T(s, T) RESULT(rho)
    !==========================================
    !  > rho = rho(s,T), s := entropy

    REAL, INTENT(IN) :: s, T
    !-------------------------

    REAL             :: alfa, dalfat, ro, func, der_func, ro1, err
    INTEGER          :: i, icont
    !-------------------------

    alfa     = (1. + alfa_om*(1. - (T**0.5)))**2.
    dalfat   = -alfa_om*(1./T**0.5 + alfa_om*(1./T**0.5 - 1.))

    ro=1.
    icont = 0

    do i=1,20

       func = (cvinf/nexp/z_c*(abs(T)**nexp-1.)+&
            1./z_c*(log((1./ro-bpr)/(1.-bpr))) + &
            apr/(8.)**0.5/bpr*dalfat* (log((1./ro+bpr+bpr*(2.)**0.5)/(1./ro+bpr-bpr*(2.)**0.5))))-s

       der_func = -1./z_c/(ro*(1-bpr*ro))+apr*dalfat/((1+ro*bpr)**2-2*ro**2*bpr**2)

       ro1=MAX(0.01,ro-func/der_func)
       err=abs(ro1-ro)/ro1


       if (err<=TOL) THEN
          ro=ro1
          icont=1.
          EXIT
       endif

       ro=ro1

    end do

    if (icont/=1.) then
       print*,"Calcul : PRSV_rho__s_T(s, T) -> NON Convergence en 20 it�rations"
       print*,"s      =",s
       print*,"T      =",T
       print*,"Erreur =",err
       print*,"rho    =",ro
       STOP
    endif

    rho = ro

  END FUNCTION PRSV_rho__s_T
  !=======================

  !==========================================
  REAL FUNCTION PRSV_T__rho_s(rho, s) RESULT(T)
    !==========================================
    !  > T = T(rho, s), s := entropy

    REAL, INTENT(IN) :: rho, s
    !-------------------------

    REAL             :: tr, tr1, err, alfa, dalfat, d2alfat, func, der_func
    INTEGER          :: i, icont
    !-------------------------

    tr    = 1d0
    icont = 0

    DO i = 1, 40


       alfa     = (1.+alfa_om*(1.-(tr**0.5)))**2.
       dalfat   =  -alfa_om*(1./tr**0.5+alfa_om*(1./tr**0.5-1.))
       d2alfat  = alfa_om/2./tr**(1.5)*(1.+alfa_om)

       func     = cvinf/nexp/z_c*(abs(tr)**nexp-1.)+ &
            1./z_c*(log((1./rho-bpr)/(1.-bpr))) +   &
            apr/(8.)**0.5/bpr*dalfat*             &
            (log((1./rho+bpr+bpr*(2.)**0.5)/(1./rho+bpr-bpr*(2.)**0.5)))-s

       der_func = cvinf/z_c*(abs(tr)**(nexp-1.))+  &
            +  apr/(8.)**0.5/bpr*d2alfat*           &
            (log((1./rho+bpr+bpr*(2.)**0.5)/(1./rho+bpr-bpr*(2.)**0.5)))

       tr1      = tr - (func)/der_func

       err      = abs(tr1-tr)/tr1

       if (err .le. TOL) then
          tr=tr1
          icont=1
          EXIT
       endif

       tr=tr1

    end do

    if (icont/=1.) then
       print*,"Calcul : PRSV_T__rho_s(rho, s) -> NON Convergence en 20 it�rations"
       print*,"rho    =", rho
       print*,"s      =", s
       print*,"Erreur =", err
       print*,"T      =",tr
       print*,"T+     =",tr1
       STOP
    endif
    
    T   = tr

  END FUNCTION PRSV_T__rho_s
  !=======================

  !==========================================
  REAL FUNCTION PRSV_P__rho_rhoU_rhoE(rho, rhoU, rhoE) RESULT(P)
    !==========================================
    !  >  P = P(rho, rho*|u|, rho*E) E total energy 

    REAL, INTENT(IN) :: rho, rhoU, rhoE
    !-------------------------

    REAL             :: e, T
    !-------------------------

    e = rhoE/rho - 0.5*(rhoU/rho)**2

    T = PRSV_T__rho_rhoe(rho, rho*e)
    P = PRSV_P__rho_T(rho,T)


  END FUNCTION PRSV_P__rho_rhoU_rhoE
  !=======================


  !==========================================
  REAL FUNCTION PRSV_dP_de__rho_T(rho, T) RESULT(d)
    !==========================================
    !  > (dP/de)|rho = dP/de(rho, T)   e:= internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, dkat, d2kat, d3kat, alfa, bco, dalfat, dbcot, d2alfat, d3alfat, cvr, dprtr
    !-------------------------


    ka      = alfa_om + k1*(1.0d0 + T**0.5)*(0.7d0 - T)
    dkat    = k1* (-1.0d0 + 0.7d0*0.5d0/T**0.5 - 1.5d0*T**0.5 )
    d2kat   = k1* (-0.7d0*0.25d0/T**1.5 - 1.5d0*0.5d0/T**0.5 )
    d3kat   = k1* (0.7d0*0.25d0*1.5d0/T**2.5 + 1.5d0*0.25d0/T**1.5 )

    alfa    = (1.0d0 + ka*(1.0d0 - T**0.5))**2.
    bco     = (dkat - T**0.5*dkat - 0.5d0*ka/T**0.5  )
    dalfat  = 2.0d0*alfa**0.5*bco
    dbcot   = d2kat - T**0.5*d2kat - 1.0d0/T**0.5*dkat + 0.25d0*ka/T**1.5
    d2alfat = bco/alfa**0.5*dalfat + 2.0d0*alfa**0.5*dbcot
    d3alfat = 2.0d0*dbcot/alfa**0.5*dalfat - 0.5d0*bco*(dalfat**2.0)/    &
         alfa**1.5 + bco/alfa**0.5*d2alfat + 2.0d0*alfa**0.5*(d3kat - T**0.5*d3kat -    &
         1.5d0/T**0.5*d2kat + 0.75d0/T**1.5*dkat - 0.25d0*1.5d0*ka/T**2.5)

    cvr    = cvinf/z_c*T**nexp + T*d2alfat*apr/bpr/(8.0d0**0.5)*log(abs(    &
         (2.0d0/rho+2.*bpr+bpr*(8.**0.5))/(2.0d0/rho + 2.0d0*bpr - bpr*(8.0d0**0.5))))

    dprtr  = 1.0d0/(1.0d0/rho - bpr)/z_c - (apr*dalfat)/(1.0d0/rho**2. + 2.0d0*bpr/rho - bpr**2.)

    d      = dprtr/cvr

  END FUNCTION PRSV_dP_de__rho_T
  !=======================

  !==========================================
  REAL FUNCTION PRSV_dP_drho__rho_T(rho, T) RESULT(d)
    !==========================================
    !  > (dP/drho)|e = dP/drho(rho, T)   e:= internal energy

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, dkat, d2kat, d3kat, alfa, bco, dalfat, dbcot, d2alfat, d3alfat, cvr, dprtr, dprvr, dervr
    !-------------------------

    ka      = alfa_om + k1*(1. + T**0.5)*(0.7 - T)
    dkat    = k1*( -1.0 + 0.7*0.5/T**0.5 - 1.5*T**0.5 )
    d2kat   = k1*(-0.7*0.25/T**1.5 - 1.5*0.5/T**0.5 )
    d3kat   = k1*( 0.7*0.25*1.5/T**2.5 + 1.5*0.25/T**1.5 )

    alfa    = ( 1. + ka*(1. - T**0.5) )**2.
    bco     = ( dkat - T**0.5*dkat - 0.5*ka/T**0.5 )
    dalfat  = 2.*alfa**0.5*bco
    dbcot   = d2kat - T**0.5*d2kat - 1./T**0.5*dkat + 0.25*ka/T**1.5
    d2alfat = bco/alfa**0.5*dalfat + 2.0*alfa**0.5*dbcot
    d3alfat = 2.*dbcot/alfa**0.5*dalfat - 0.5*bco*(dalfat**2.0)/alfa**1.5 + &
         bco/alfa**0.5*d2alfat + 2.0*alfa**0.5*(d3kat - T**0.5*d3kat - &
         1.5/T**0.5*d2kat + 0.75/T**1.5*dkat - 0.25*1.5*ka/T**2.5 )

    cvr     = cvinf/z_c*T**nexp + T*d2alfat*apr/bpr/(8.**0.5)* &
         log(abs((2./rho + 2.*bpr + bpr*(8.**0.5))/(2./rho + 2.*bpr - bpr*(8.**0.5))))

    dprtr  = 1./(1./rho - bpr)/z_c - (apr*dalfat)/(1./rho**2. + 2.*bpr/rho - bpr**2.)

    dprvr  = (-T/z_c/(1./rho - bpr)**2. + alfa*apr*(2./rho + 2.*bpr)/ &
         (1./rho**2. + 2./rho*bpr - bpr**2.)**2.) 

    dervr  = -1./bpr/(8.**0.5)*apr*(T*dalfat - alfa)*8.*2.**0.5*bpr/(2./rho &
         + 2.*bpr + bpr*(8.**0.5))/(2./rho + 2.*bpr - bpr*(8.**0.5)) 

    d      = -(dprvr - dprtr*dervr/cvr)/rho**2


  END FUNCTION PRSV_dP_drho__rho_T
  !=======================

  !==========================================
  REAL FUNCTION PRSV_pi_rho__rho_u_T(rho, u, T) RESULT(pi)
    !==========================================
    !  > (dPI/drho) = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    REAL, INTENT(IN) :: rho, u, T
    !-------------------------

    REAL             :: e, dRho, dE
    !-------------------------

    e    = PRSV_e__rho_T(rho, T)
    dRho = PRSV_dP_drho__rho_T(rho, T)
    dE   = PRSV_dP_de__rho_T(rho, T)

    pi = dRho + (0.5*u*u - e)*dE/rho 

  END FUNCTION PRSV_pi_rho__rho_u_T
  !=======================

  !==========================================
  REAL FUNCTION PRSV_pi_rhoe__rho_T(rho, T) RESULT(pi)
    !==========================================
    !  > dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    pi = PRSV_dP_de__rho_T(rho, T) / rho

  END FUNCTION PRSV_pi_rhoe__rho_T
  !=======================


  !==========================================
  REAL FUNCTION PRSV_GAMMA__rho_T(rho, T) RESULT(G)
    !==========================================
    !  > GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, dkat, d2kat, d3kat, alfa, bco, dalfat, dbcot, d2alfat, d3alfat, cvr, &
         dprvr, dprtr, d2prtr, d2prvr, dcvrtr, ar2, d2prtrvr
    !-------------------------

    ka      = alfa_om + k1*(1. + T**0.5)*(0.7 - T)
    dkat    = k1* (-1.0 + 0.7*0.5/T**0.5 - 1.5*T**0.5 )
    d2kat   = k1* (-0.7*0.25/T**1.5 - 1.5*0.5/T**0.5 )
    d3kat   = k1* (0.7*0.25*1.5/T**2.5 + 1.5*0.25/T**1.5 )

    alfa    = (1. + ka*(1. - T**0.5))**2.
    bco     = (dkat - T**0.5*dkat - 0.5*ka/T**0.5  )
    dalfat  = 2*alfa**0.5*bco
    dbcot   = d2kat - T**0.5*d2kat - 1./T**0.5*dkat + 0.25*ka/T**1.5
    d2alfat = bco/alfa**0.5*dalfat + 2.0*alfa**0.5*dbcot
    d3alfat = 2.*dbcot/alfa**0.5*dalfat - 0.5*bco*(dalfat**2.0)/ &
         alfa**1.5 + bco/alfa**0.5*d2alfat + 2.0*alfa**0.5*(d3kat - &
         T**0.5*d3kat - 1.5/T**0.5*d2kat + 0.75/T**1.5*dkat - 0.25*1.5*ka/T**2.5  )

    cvr     = cvinf/z_c*T**nexp + T*d2alfat*apr/bpr/(8.**0.5)* &
         log((2./rho + 2.*bpr + bpr*(8.**0.5))/(2./rho + 2.*bpr - bpr*(8.**0.5)))

    dprvr   = (-T/z_c/(1./rho - bpr)**2. + alfa*apr*(2./rho + 2.*bpr)/ &
         (1./rho**2. + 2./rho*bpr - bpr**2.)**2.)

    dprtr   = 1./(1./rho - bpr)/z_c - apr*dalfat/(1./rho**2. + 2./rho*bpr - bpr**2.)

    d2prtr  = -apr*d2alfat/(1./rho**2. + 2./rho*bpr - bpr**2.)

    d2prvr  = 2.*T/z_c/(1./rho - bpr)**3. + alfa*apr*2.*(-3./rho**2. - &
         6.*bpr/rho - 5.*bpr**2.)/((1./rho**2. + 2./rho*bpr - bpr**2.)**3.)

    d2prtrvr= -1./(1./rho - bpr)**2./z_c + apr*(2./rho + 2.*bpr)*dalfat/ &
         ((1./rho**2. + 2./rho*bpr - bpr**2.)**2.)

    dcvrtr  = cvinf*nexp/z_c*T**(nexp - 1) + apr/bpr/(8.**0.5)*log((2./rho &
         +  2.*bpr + bpr*(8.**0.5))/(2./rho + 2.*bpr - bpr*(8.**0.5)))*(d2alfat + T*d3alfat)

    ar2     = 1./rho**2*(T/cvr*dprtr**2 - dprvr)

    G       = (1./rho)**3/2./ar2*(d2prvr - 3.*T/cvr*dprtr*d2prtrvr + &
         ((T/cvr*dprtr)**2)*(3.*d2prtr + 1./T*dprtr*(1. - T/cvr*dcvrtr)))


  END FUNCTION PRSV_GAMMA__rho_T
  !=======================


  !==========================================
  REAL FUNCTION PRSV_de_drho__rho_T(rho, T) RESULT(d)
    !==========================================
    ! > P = P(rho,T)

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, alfa, dkat, bco, dalfat, dervr
    !-------------------------

    ka      = alfa_om + k1*(1.0d0 + T**0.5)*(0.7d0 - T)
    alfa    = ( 1.d0 + ka*(1.d0 - T**0.5) )**2.

    dkat    = k1*( -1.0 + 0.7*0.5/T**0.5 - 1.5*T**0.5 )
    bco     = ( dkat - T**0.5*dkat - 0.5*ka/T**0.5 )
    dalfat  = 2.*alfa**0.5*bco

    dervr  = -1./bpr/(8.**0.5)*apr*(T*dalfat - alfa)*8.*2.**0.5*bpr/(2./rho &
         + 2.*bpr + bpr*(8.**0.5))/(2./rho + 2.*bpr - bpr*(8.**0.5)) 

    d    = -dervr / rho**2

  END FUNCTION PRSV_de_drho__rho_T
  !===============================
  
  !==================================================
  REAL FUNCTION PRSV_dT_drho__rho_T(rho, T) RESULT(d)
  !==================================================
  !
  !                 -(de/drho)|_T      -(de/drho)|_T 
  ! (dT/drho)|_e = ---------------- = ----------------
  !                  (de/dT)|_rho          Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: de_drho, c_v
    !-------------------------
    
    de_drho = PRSV_de_drho__rho_T(rho, T)

    c_v = PRSV_Cv__rho_T(rho,T)

    d = -de_drho / c_v

  END FUNCTION PRSV_dT_drho__rho_T
  !===============================

  !================================================
  REAL FUNCTION PRSV_dT_de__rho_T(rho, T) RESULT(d)
  !================================================
  !
  !                         1          1
  ! (dT/de)|_rho = ---------------- = ----
  !                  (de/dT)|_rho      Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: c_v
    !-------------------------
    
    c_v = PRSV_Cv__rho_T(rho,T)

    d = 1.d0 / c_v

  END FUNCTION PRSV_dT_de__rho_T
  !=============================

  !=========================================
  FUNCTION PRSV_dP__dT_dv(rho, T) RESULT(dP)
  !=========================================
  ! 
  ! dP(1) = dP/dT|_v,   dP(2) = dP/dv|_T
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: dP
    !-------------------------

    REAL :: v, ka, d_ka, alpha, d_alpha
    !-------------------------

    v = 1.d0 / rho
    
    ka = alfa_om + k1*(1.0d0 + SQRT(T))*(0.7d0 - T)

    d_ka = (0.5d0*k1/SQRT(T))*(0.7d0 - T) - k1*(1.d0 + SQRT(T))

    alpha = ( 1.d0 + ka*(1.d0 - SQRT(T)) )**2

    d_alpha = 2.d0*( 1.d0 + ka*(1.d0 - SQRT(T)) ) &
            * ( d_ka*(1.d0 - SQRT(T)) - 0.5d0*ka/SQRT(T) )


    dP(1) = 1.d0/( Z_c*(v - bpr) )  &
          - apr*d_alpha/( v**2 + 2*v*bpr - bpr**2 )

    dP(2) = -T/( Z_c*(v - bpr)**2 ) &
          + 2.d0*apr*alpha*(v + bpr)/( v**2 + 2*v*bpr - bpr**2 )**2

  END FUNCTION PRSV_dP__dT_dv  
  !==========================

  !===========================================
  FUNCTION PRSV_dP2__dT_dv(rho, T) RESULT(dP2)
  !===========================================
  ! 
  ! dP(1) = d^2P / dT^2|_v  
  ! dP(2) = d^2P / dv^2|_T 
  ! dP(3) = d^2P / dvdT|_vT
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(3) :: dP2
    !-------------------------

    REAL :: v, ka, d_ka, alpha, d_alpha
    REAL :: d2_ka, d2_alpha, cg
    !-------------------------
    v = 1.d0 / rho
    
    ka = alfa_om + k1*(1.0d0 + SQRT(T))*(0.7d0 - T)

    d_ka = (0.5d0*k1/SQRT(T))*(0.7d0 - T) - k1*(1.d0 + SQRT(T))

    d2_ka = -k1*( (0.7 - T)/(4.d0*T*SQRT(T)) ) &
          - k1/SQRT(T)

    alpha = ( 1.d0 + ka*(1.d0 - SQRT(T)) )**2

    d_alpha = 2.d0*( 1.d0 + ka*(1.d0 - SQRT(T)) ) &
            * ( d_ka*(1.d0 - SQRT(T)) - 0.5d0*ka/SQRT(T) )

    d2_alpha = 2.d0*(d_ka - d_ka*SQRT(T) - 0.5d0*ka/SQRT(T))**2 &
             + 2.d0*(1.d0 + ka - ka*SQRT(T))*&
              ( d2_ka - d2_ka*SQRT(T) - d_ka/SQRT(T) + ka/(4.d0*T*SQRT(T)) )
    
    dP2(1) = -apr*d2_alpha/( v**2 + 2*v*bpr - bpr**2 )

    dP2(2) = 2.d0*T/( Z_c*(v - bpr)**3 ) &
           - 8.d0*apr*alpha*((v + bpr)**2)/( v**2 + 2*v*bpr - bpr**2 )**3 &
           + 2.d0*apr*alpha               /( v**2 + 2*v*bpr - bpr**2 )**2

    dP2(3) = -1.d0/( Z_c*(v - bpr)**2 ) &
           + 2.d0*apr*d_alpha*(v + bpr)/( v**2 + 2*v*bpr - bpr**2 )**2

  END FUNCTION PRSV_dP2__dT_dv  
  !===========================


  !---------------END basic FUNCTIONS          -----------------------------!
  !============================================================================
  !============================================================================

  !============================================================================
  !============================================================================
  !BEGIN :           derivated FUNCTIONS

  !Description
  ! > PRSV_c__rho_e(rho, e)          c  = c(rho,e)
  ! > PRSV_Temperature(Vp)           T  = T(rho,e)
  ! > PRSV_Pression(Vp)              P  = P(rho,e)
  ! > PRSV_Epsilon(Vp)               e  = T(rho,P)
  ! > PRSV_EpsilonDet(Vp)            e  = T(rho,T)
  ! > PRSV_Son(Vp)                   c  = T(rho,P)
  ! > PRSV_enthalpy(Vp)              h  = T(rho,P)
  ! > PRSV_Gas(Vp)            Vp = Vp(rho,e) "subroutine" computes all Vp components from (rho,e) <- Vp
  ! > PRSV_Cv__rho_T(rho,T)          Cv = Cv(rho,T)
  ! > PRSV_Cv_oo(T)                  Cv_oo = Cv_ideal(T)
  ! > PRSV_d_Cv_oo(T)                d_Cv_oo = d_Cv_ideal(T)
  ! > PRSV_d_Cv_rho(rho,T)           dCv/drho|T
  ! > PRSV_d_Cv_T(rho,T)             dCv/dT|rho

  !==========================================
  REAL FUNCTION PRSV_c__rho_e(rho, e) RESULT(c)
    !==========================================
    ! > c = c(rho, e)

    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: e

    REAL :: T
    !-------------------------

    !INTERMEDIATE CALCULATION
    T = PRSV_T__rho_rhoe(rho,rho*e)

    !RESULT
    c = PRSV_c__rho_T(rho,T)

  END FUNCTION PRSV_c__rho_e
  !=======================  


  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !============================================
  REAL FUNCTION PRSV_Temperature(Vp) RESULT(T)
    !============================================
    ! > T = T(rho, e)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "PRSV_Temperature"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, e
    !------------------------------------

    ! DATA
    e     = Vp(i_eps_vp) 
    rho   = Vp(i_rho_vp)

    ! RESULT
    T     = PRSV_T__rho_rhoe(rho,rho*e)

  END FUNCTION PRSV_Temperature
  !==========================


  !======================================
  REAL FUNCTION PRSV_Pression(Vp) RESULT(P)
    !======================================
    ! P = P (e, rho)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "PRSV_Pression"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: e, rho, T
    !-------------------------------------

    !DATA
    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)    ! �nergie interne

    !INTERMEDIATE CALCULATION
    T       = PRSV_T__rho_rhoe(rho,rho*e)

    !RESULT
    P       = PRSV_P__rho_T(rho,T)

  END FUNCTION PRSV_Pression
  !=======================

  !======================================
  REAL FUNCTION PRSV_Epsilon(Vp) RESULT(e)
    !======================================
    ! > e = e(rho,P)

    CHARACTER(LEN = *), PARAMETER :: mod_name = "PRSV_Epsilon"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T   = PRSV_T__rho_P(rho,P)

    !RESULT
    e   = PRSV_e__rho_T(rho,T)

  END FUNCTION PRSV_Epsilon
  !======================

  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !==========================================
  REAL FUNCTION PRSV_EpsilonDet(Vp) RESULT(e)
    !==========================================
    ! > e = e(rho, T)

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    T   = Vp(i_tem_vp)

    ! e = e(rho, T)
    e   = PRSV_e__rho_T(rho,T)

  END FUNCTION PRSV_EpsilonDet
  !=========================


  !==================================
  REAL FUNCTION PRSV_Son(Vp) RESULT (c)
    !==================================
    ! c = c(P, rho)

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T = PRSV_T__rho_P(rho,P)

    !RESULT
    c = PRSV_c__rho_T(rho,T)

  END FUNCTION PRSV_Son
  !==================


  !======================================
  REAL FUNCTION PRSV_enthalpy(Vp) RESULT(h)
    !======================================
    ! > h = h (P, rho)

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T, e
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !CALCULATION
    T = PRSV_T__rho_P(rho,P)
    e = PRSV_e__rho_T(rho,T)

    !RESULT
    h = e + P/rho

  END FUNCTION PRSV_Enthalpy
  !=======================

  !===========================
  SUBROUTINE PRSV_Gas(Vp)
    !===========================
    ! > (T,P,c) in function of (rho, e), e:= internal energy

    CHARACTER(LEN = *), PARAMETER :: mod_name = "PRSV_PerfectGas"

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp
    !--------------------------------------

    REAL :: e, rho, P, T, c
    !---------------------------------------

    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)  ! �nergie interne i_eps_vp

    !CALCULATION
    T       = PRSV_T__rho_rhoe(rho,rho*e)         !-- Temperature
    P       = PRSV_P__rho_T(rho,T)                !-- Pressure
    c       = PRSV_c__rho_T(rho,T)                !-- Sound Speed

    !RESULT
    Vp(i_pre_vp) = P 
    Vp(i_tem_vp) = T
    Vp(i_son_vp) = c

  END SUBROUTINE PRSV_Gas
  !===========================


  !-------------ENDING derivated FONCTIONS     -----------------------------!
  !----------------------------------------------------------------------------
  !============================================================================


  !============================================================================
  !----------------------------------------------------------------------------
  !-------------BEGINNING useful specific FONCTIONS                  -------!

  !===================================
  FUNCTION PRSV_cv_oo(T) RESULT(cv_oo)
  !===================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: T
    REAL             :: cv_oo
    !--------------------

    cv_oo = (cvinf/z_c)*T**nexp

  END FUNCTION PRSV_cv_oo  
  !======================

  !=======================================
  FUNCTION PRSV_d_cv_oo(T) RESULT(d_cv_oo)
  !=======================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: T
    REAL             :: d_cv_oo
    !--------------------

    d_cv_oo = cvinf*(nexp/z_c)*T**(nexp - 1)

  END FUNCTION PRSV_d_cv_oo  
  !========================

  !==============================================
  FUNCTION PRSV_Cv__rho_T(rho, T) RESULT(Cv)
  !==============================================
  !  > Cv = Cv(rho, T)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    REAL             :: Cv
    !-------------------------

    REAL :: v, ka, d_ka, d2_ka, &
            alpha, d_alpha, d2_alpha
    !-------------------------------

    v = 1.d0 / rho

    ka    = alfa_om + k1*(1.0d0 + SQRT(T))*(0.7d0 - T)
    d_ka  = (0.5d0*k1/SQRT(T))*(0.7d0 - T) - k1*(1.d0 + SQRT(T))
    d2_ka = -k1*( (0.7 - T)/(4.d0*T*SQRT(T)) ) &
          - k1/SQRT(T)

    alpha    = ( 1.d0 + ka*(1.d0 - SQRT(T)) )**2
    d_alpha  = 2.d0*( 1.d0 + ka*(1.d0 - SQRT(T)) ) &
             * ( d_ka*(1.d0 - SQRT(T)) - 0.5d0*ka/SQRT(T) )

    d2_alpha = 2.d0*(d_ka - d_ka*SQRT(T) - 0.5d0*ka/SQRT(T))**2 &
             + 2.d0*(1.d0 + ka - ka*SQRT(T))*&
              ( d2_ka - d2_ka*SQRT(T) - d_ka/SQRT(T) + ka/(4.d0*T*SQRT(T)) )

    Cv = (cvinf/Z_c)*T**nexp + T*apr*d2_alpha &
         *LOG( (v + bpr + bpr*SQRT(2.d0))/(v + bpr - bpr*SQRT(2.d0)) ) / (2.d0*bpr*SQRT(2.d0))

  END FUNCTION PRSV_Cv__rho_T
  !==========================

  !============================================
  FUNCTION PRSV_dCv__dT_dv(rho, T) RESULT(d_Cv)
  !============================================
  ! 
  ! d_Cv(1) = dCv/dT|_rho
  ! d_Cv(2) = dCv/drho|_T
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: d_Cv
    !-------------------------

    REAL :: v, ka, d_ka, d2_ka, d3_ka, Q0, &
            alpha, d_alpha, d2_alpha, d3_alpha

    REAL :: d_cv__v, d_cv__T
    !-------------------------------

    v = 1.d0 / rho

    ka    = alfa_om + k1*(1.0d0 + SQRT(T))*(0.7d0 - T)

    d_ka  = (0.5d0*k1/SQRT(T))*(0.7d0 - T) - k1*(1.d0 + SQRT(T))

    d2_ka = -k1*( (0.7d0 - T)/(4.d0*T*SQRT(T)) ) &
          - k1/SQRT(T)

    d3_ka = (3.d0/8.d0)*k1*(0.7d0 - T)/(T*T*SQRT(T)) &
          + (3.d0/4.d0)*k1/(T*SQRT(T))

    alpha    = ( 1.d0 + ka*(1.d0 - SQRT(T)) )**2

    d_alpha  = 2.d0*( 1.d0 + ka*(1.d0 - SQRT(T)) ) &
             * ( d_ka*(1.d0 - SQRT(T)) - 0.5d0*ka/SQRT(T) )

    d2_alpha = 2.d0*(d_ka - d_ka*SQRT(T) - 0.5d0*ka/SQRT(T))**2 &
             + 2.d0*(1.d0 + ka - ka*SQRT(T))*&
              ( d2_ka - d2_ka*SQRT(T) - d_ka/SQRT(T) + ka/(4.d0*T*SQRT(T)) )

    d3_alpha = 6.d0*(  ( d_ka*(1.d0 - SQRT(T)) - 0.5*ka/SQRT(T) ) &
                     *( d2_ka*(1.d0 - SQRT(T)) - d_ka /SQRT(T) + ka/(4.d0*T*SQRT(T)) )  ) &
             + 2.d0*( 1.d0 + ka*(1.d0 - SQRT(T)) )  &
                   *( d3_ka*(1.d0 - SQRT(T)) &
                      - (3.d0/2.d0)*d2_ka / (SQRT(T))  &
                      + (3.d0/4.d0)* d_ka / (T*SQRT(T)) &
                      - (3.d0/8.d0)*   ka / (T*T*SQRT(T)) )

    Q0 = LOG( (v + bpr + bpr*SQRT(2.d0))/(v + bpr - bpr*SQRT(2.d0)) ) / (2.d0*bpr*SQRT(2.d0))


    d_cv__T = (nexp*cvinf/Z_c)*T**(nexp-1) &
            + apr*d2_alpha*Q0 & 
            + apr*d3_alpha*T*Q0

    d_cv__v = T*apr*d2_alpha*( 1.d0/(v + bpr + bpr*SQRT(2.d0)) &
            - 1.d0/(v + bpr - bpr*SQRT(2.d0)) ) / (2.d0*bpr*SQRT(2.d0))

    d_Cv(1) = d_cv__T
    d_Cv(2) = d_cv__V

  END FUNCTION PRSV_dCv__dT_dv
  !===========================

!!$  !================================================
!!$  REAL FUNCTION PRSV_d_Cv__rho(rho, T) RESULT(d_Cv)
!!$  !================================================
!!$  !
!!$  !   d Cv  |
!!$  ! --------|
!!$  !  d rho  |T
!!$  !
!!$    REAL, INTENT(IN) :: rho, T
!!$    !-------------------------
!!$
!!$    REAL :: v, ka, d_ka, d2_ka, &
!!$            d2_alpha
!!$    !-------------------------------
!!$
!!$    v = 1.d0 / rho
!!$
!!$    ka    = alfa_om + k1*(1.0d0 + SQRT(T))*(0.7d0 - T)
!!$
!!$    d_ka  = (0.5d0*k1/SQRT(T))*(0.7d0 - T) - k1*(1.d0 + SQRT(T))
!!$
!!$    d2_ka = -k1*( (0.7d0 - T)/(4.d0*T*SQRT(T)) ) &
!!$          - k1/SQRT(T)
!!$
!!$    d2_alpha = 2.d0*(d_ka - d_ka*SQRT(T) - 0.5d0*ka/SQRT(T))**2 &
!!$             + 2.d0*(1.d0 + ka - ka*SQRT(T))*&
!!$              ( d2_ka - d2_ka*SQRT(T) - d_ka/SQRT(T) + ka/(4.d0*T*SQRT(T)) )
!!$
!!$    d_cv = -(v**2) * T*apr*d2_alpha*( 1.d0/(v + bpr + bpr*SQRT(2.d0)) &
!!$                   - 1.d0/(v + bpr - bpr*SQRT(2.d0)) ) / (2.d0*bpr*SQRT(2.d0))
!!$
!!$  END FUNCTION PRSV_d_Cv__rho
!!$  !==========================




  !===============================
  REAL FUNCTION PRSV_d_Cv_rho(rho,T) RESULT(dCv)
    !===============================
    ! > dCv/drho|T

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, dkat, d2kat, alfa, bco, dalfat, dbcot, d2alfat, dprvr, d2prtr
    !-------------------------


    ka      = alfa_om + k1*(1.0d0 + T**0.5)*(0.7d0 - T) 
    dkat    = k1* (-1.0d0 + 0.7d0*0.5d0/T**0.5d0 - 1.5d0*T**0.5) 
    d2kat   = k1* (-0.7d0*0.25d0/T**1.5 - 1.5d0*0.5d0/T**0.5 )

    alfa    = (1.0d0 + ka*(1.0d0 - T**0.5))**2.0d0
    bco     = (dkat - T**0.5*dkat - 0.5d0*ka/T**0.5)    

    dalfat  = 2.0d0*alfa**0.5*bco
    dbcot   = d2kat - T**0.5*d2kat - 1.0d0/T**0.5*dkat + 0.25*ka/T**1.5

    d2alfat = bco/alfa**0.5*dalfat + 2.0d0*alfa**0.5*dbcot 
    d2prtr  = -apr*d2alfat/(1.0d0/rho**2.0d0 + 2.0d0/rho*bpr - bpr**2.0d0)

    dCv     = -T*d2prtr/rho**2



  END FUNCTION PRSV_d_Cv_rho
  !=================


  !===============================
  REAL FUNCTION PRSV_d_Cv_T(rho,T) RESULT(dCv)
    !===============================
    ! > dCv/dT|rho

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: ka, dkat, d2kat, d3kat, alfa, bco, dalfat, dbcot, d2alfat, d3alfat
    !-------------------------

    ka      = alfa_om + k1*(1. + T**0.5)*(0.7 - T)
    dkat    = k1* (-1.0 + 0.7*0.5/T**0.5 - 1.5*T**0.5 )
    d2kat   = k1* (-0.7*0.25/T**1.5 - 1.5*0.5/T**0.5 )
    d3kat   = k1* (0.7*0.25*1.5/T**2.5 + 1.5*0.25/T**1.5 )

    alfa    = (1. + ka*(1. - T**0.5))**2.
    bco     = (dkat - T**0.5*dkat - 0.5*ka/T**0.5  )

    dalfat  = 2*alfa**0.5*bco
    dbcot   = d2kat - T**0.5*d2kat - 1./T**0.5*dkat + 0.25*ka/T**1.5

    d2alfat = bco/alfa**0.5*dalfat + 2.0*alfa**0.5*dbcot
    d3alfat = 2.*dbcot/alfa**0.5*dalfat - 0.5*bco*(dalfat**2.0)/ &
         alfa**1.5 + bco/alfa**0.5*d2alfat + 2.0*alfa**0.5*(d3kat - &
         T**0.5*d3kat - 1.5/T**0.5*d2kat + 0.75/T**1.5*dkat - 0.25*1.5*ka/T**2.5  )

    dCv  = cvinf*nexp/z_c*T**(nexp - 1) + apr/bpr/(8.**0.5)*log((2./rho &
         +  2.*bpr + bpr*(8.**0.5))/(2./rho + 2.*bpr - bpr*(8.**0.5)))*(d2alfat + T*d3alfat)


  END FUNCTION PRSV_d_Cv_T
  !=================


  !==========================================
  REAL FUNCTION PRSV_TEICALC(h, rho, c1, c2) RESULT(T)
    !==========================================
    ! > fonction sp�cifique entr�e turbine
    
    REAL, INTENT(IN) :: h, rho, c1, c2
    !-------------------------

    REAL             :: tr, alfa, dalfat, d2alfat, eicalc, pecalc, deicalc, dprtr, func, der_func, tr1, err
    INTEGER          :: icont, i
    !-------------------------


    tr=1.
    icont = 0

    do i=1,20

       alfa= (1.+alfa_om*(1.-(tr**0.5)))**2.
       dalfat= -alfa_om*(1./tr**0.5+alfa_om*(1./tr**0.5-1.))
       d2alfat= alfa_om/2./tr**(1.5)*(1.+alfa_om)

       eicalc = cvinf/(nexp+1)/z_c*(abs(tr))**(nexp+1.)+&
            1./bpr/(8.**0.5)*log(abs((2./rho+2.*bpr+bpr*(8.**0.5))&
            /(2./rho+2.*bpr-bpr*(8.**0.5))))*apr*&
            (tr*dalfat-alfa)

       pecalc = tr/(1./rho-bpr)/z_c-(apr*alfa)/&
            (1./rho**2.+2.*bpr/rho-bpr**2.)

       deicalc = cvinf/z_c*(abs(tr))**nexp+1./bpr/8.**0.5*apr*tr*d2alfat*&
            log(abs((2./rho+2.*bpr+bpr*(8.**0.5))/(2./rho+2.*bpr-bpr*(8.**0.5))))

       dprtr = 1./(1./rho-bpr)/z_c-apr/(1./rho**2.+2.*bpr/rho+bpr**2.)*dalfat


       func = eicalc+pecalc/rho+0.5*c1*(c2-pecalc)**2.-h
       der_func = deicalc+dprtr/rho-c1*(c2-pecalc)*dprtr

       tr1=tr-func/der_func
       err = abs(tr-tr1)/tr1

       if (err<=TOL) THEN
          tr=tr1
          icont=1.
          EXIT
       endif

       tr=tr1

    end do

    if (icont/=1.) then
       print*,"Calcul : PRSV_TEICALC(h, rho, c1, c2) -> NON Convergence en 20 it�rations"
       print*,"h      =",h
       print*,"rho    =",rho
       print*,"c1     =",c1
       print*,"c2     =",c2
       print*,"Erreur =",err
       print*,"T      =",tr1
       STOP
    endif

    T = tr1


  END FUNCTION PRSV_TEICALC
  !=======================


  !-------------ENDING useful specific FONCTIONS                  ----------!
  !============================================================================
  !----------------------------------------------------------------------------



END MODULE PRSV









