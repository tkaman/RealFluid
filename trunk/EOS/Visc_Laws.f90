MODULE Visc_Laws
  
  USE LesTypes
  USE EOSLaws

  IMPLICIT NONE

  !==================================
  REAL, PUBLIC, SAVE :: prandtl
  REAL, PUBLIC, SAVE :: muref
  REAL, PUBLIC, SAVE :: lbdaref
  !----------------------------------
  REAL, PUBLIC,  SAVE :: Re_mu
  REAL, PRIVATE, SAVE :: T_ref ! [k]
  REAL, PRIVATE, SAVE :: T_oo_adim
  !----------------------------------
  INTEGER, PRIVATE, SAVE :: visc_law, therm_law 
  !==================================

CONTAINS
         
  !========================================================
  SUBROUTINE EOSIviscInit(Data, M, Re, Re_adim, mu_oo_adim)
  !========================================================

    IMPLICIT NONE

    TYPE(Donnees), INTENT(INOUT):: Data    
    REAL,          INTENT(IN)   :: M
    REAL,          INTENT(IN)   :: Re
    REAL,          INTENT(OUT)  :: Re_adim
    REAL,          INTENT(OUT)  :: mu_oo_adim
    !------------------------------------------

    CHARACTER(LEN = 20) :: EosLaw
    CHARACTER(LEN = 20) :: Visclaw
    CHARACTER(LEN = 20) :: Thermlaw

    REAL :: Re_oo, M_oo
    REAL :: rho_oo_adim, v_oo_adim
         
    LOGICAL :: existe
    INTEGER :: statut
    !------------------------------------------
         
    INQUIRE(FILE = "ViscLaw.data", EXIST = existe)
    IF (.NOT. existe) THEN
       WRITE(*,*)  'ERROR: file ViscLaw.data not found'
       STOP
    END IF

    OPEN(10, FILE = "ViscLaw.data", IOSTAT = statut)

    READ(10, *, IOSTAT = statut)  muref, prandtl, lbdaref
                      
   CLOSE(10)

   M_oo = M
   Re_oo = Re

   visc_law  = Data%sel_loivisq
   therm_law = Data%sel_LoiTh

   T_ref = Data%TRef

   rho_oo_adim = Data%VarPhyInit(i_rho_vp, 1)
     v_oo_adim = SQRT( SUM(Data%VarPhyInit(i_vi1_vp:i_vid_vp, 1)**2) )

     T_oo_adim = Data%VarPhyInit(i_tem_vp, 1)
     
   Re_mu = Re_oo / (rho_oo_adim * v_oo_adim)

   IF( ( (visc_law  == cs_loivisq_chungdilute) .AND. &
         (therm_law /= cs_loith_chungdilute) ) .OR.  &
       ( (visc_law  == cs_loivisq_chungdense)   .AND. &
         (therm_law /= cs_loith_chungdense) )    ) THEN

      WRITE(*,*) 'Viscosity and thermal conductivity models'
      WRITE(*,*) 'do not match. STOP'
      STOP

   END IF

   IF ( sel_EosLaw == cs_loietat_gp ) THEN

      IF ( visc_law == cs_loivisq_chungdilute .OR. &
           visc_law == cs_loivisq_chungdense) THEN

         CALL encoder_data_loietat(sel_EosLaw, EosLaw)
         CALL encoder_data_loivisq(visc_Law,   ViscLaw)

         WRITE(*,*) 'The thermodynamic model ', TRIM(ADJUSTL(EosLaw)), ' does not support'
         WRITE(*,*) 'the Viscosity laws ', TRIM(ADJUSTL(ViscLaw)), ' STOP'
         STOP

      ENDIF

   ENDIF
   
   Re_adim = Re_mu
   mu_oo_adim = EOSvisco(rho_oo_adim, T_oo_adim)

   WRITE(*, '("   mu_oo = ", E13.6)') mu_oo_adim
   WRITE(*,*) 

  END SUBROUTINE EOSIviscInit
  !==========================

  !======================================
  FUNCTION EOSvisco(rho, T) RESULT(visco)
  !======================================
  !
  ! rho, T : dimensionless
  !
 
    IMPLICIT NONE
    
    REAL, INTENT(IN) :: rho, T

    REAL :: visco
    !---------------------------------

    SELECT CASE (visc_law)
       
    CASE(cs_loivisq_constant)
                   
       visco = 1.d0
       
    CASE(cs_loivisq_sutherland)
       
       visco = visco_sutherland(T_ref, T)

    CASE(cs_loivisq_chungdilute)

       visco = visco_chung_dilute(T)
       
    CASE DEFAULT
                    
       WRITE(*,*)  'ERROR: unknown viscous law'
       STOP

    END SELECT

    ! Dimensionles dynamic viscosity
    visco = visco / Re_mu
 
  END FUNCTION EOSvisco
  !====================

  !========================================  
  FUNCTION EOSCondTh(rho, T) RESULT(CondTh)
  !========================================
  !
  ! rho, T : dimensionless
  !
 
    IMPLICIT NONE
    
    REAL, INTENT(IN) :: rho, T
    
    REAL             :: CondTh
    !------------------------------------
    
    SELECT CASE (therm_law)

    CASE(cs_loith_prandtl)

       CondTh = condth_prandtl(rho, T)

    CASE(cs_loith_chungdilute)

       CondTh = condth_chung_dilute(rho, T)

    CASE DEFAULT

       WRITE(*,*)  'ERROR: unknown thermal law'
       STOP
            
    END SELECT
    
  END FUNCTION EOSCondTh
  !=====================

!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
  
  !================================================
  FUNCTION visco_sutherland(T_ref, T) RESULT(visco)
  !================================================

    IMPLICIT NONE

    ! T: dimensionless temperature    
    ! T_ref: reference temperature (dimensional)
    
    REAL, INTENT(IN) :: T_ref, T
    REAL :: visco
    !----------------------------

    REAL :: CS_t
    REAL, PARAMETER :: CS = 110.4 ! [K]
    !----------------------------

    CS_t = CS/T_ref
             
    visco = T*SQRT(T) * (1.d0 + CS_t)/(T + CS_t)
    
  END FUNCTION visco_sutherland
  !============================

  !===========================================
  FUNCTION visco_chung_dilute(T) RESULT(visco)
  !===========================================
  !
  ! T : dimensionless temperature
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: T    ! T_adim = T_dim/T_c
    REAL             :: visco
    !--------------------------------------------

    REAL :: Ts, Ts_oo, Omegas, Omegas_oo

    REAL, PARAMETER :: beta = 1.2593d0

    REAL, DIMENSION(10), PARAMETER  ::  CC_O = & 
         (/  1.16145d0,  0.14874d0, 0.52487d0,  0.77320d0,  2.16178d0,  &
             2.43787d0, -6.4350E-4, 7.27371d0,  18.0323d0, -0.76830d0  /)
    !--------------------------------------------

    Ts    = T         * beta
    Ts_oo = T_oo_adim * beta

    Omegas    =  (CC_O(1) / Ts**CC_O(2))     &
              +   CC_O(3) / EXP(CC_O(4)*Ts)  &
              +   CC_O(5) / EXP(CC_O(6)*Ts)  &
              +   CC_O(7) * (Ts**CC_O(2))    &
              * SIN( CC_O(9)*Ts**CC_O(10) - CC_O(8) )

    Omegas_oo =  (CC_O(1) / Ts_oo**CC_O(2))     &
              +   CC_O(3) / EXP(CC_O(4)*Ts_oo)  &
              +   CC_O(5) / EXP(CC_O(6)*Ts_oo)  &
              +   CC_O(7) * (Ts_oo**CC_O(2))    &
              * SIN( CC_O(9)*Ts_oo**CC_O(10) - CC_O(8) )

    visco = SQRT(T/T_oo_adim) * (Omegas_oo/Omegas)

  END FUNCTION visco_chung_dilute
  !==============================

  !===========================================
  FUNCTION condth_prandtl(rho, T) RESULT(k_th)
  !===========================================
  !
  ! rho, T : dimensionless
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    REAL             :: k_th
    !----------------------------

    REAL :: mu_adim, Cp
    !----------------------------

    mu_adim = EOSvisco(rho, T)
             
    Cp = EOSCp__T_rho(rho, T)
             
    ! Dimensionles thermal conductivity
    k_th = Cp * mu_adim / Prandtl

  END FUNCTION condth_prandtl
  !==========================
  
  !================================================
  FUNCTION condth_chung_dilute(rho, T) RESULT(k_th)
  !================================================
  !
  ! T : dimensionless temperature
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    REAL             :: k_th
    !----------------------------

    REAL :: mu_adim, gamma, omega, Z_c 
    REAL :: cv_oo, alpha, beta, H, Psi
    !----------------------------
    
    gamma = (7.452d0 * 4.1868d0) / 8.3144d0
           !  ^^^^^^^^^^^^^^^^^    ^^^^^^^^
           !    J / (mol * K)     R_gas univ

    Z_c = EOS_Zc()
    
    omega = EOS_omega()

    cv_oo = EOSCv_oo(T)

    alpha = cv_oo*Z_c - 1.5d0 

    beta  = 0.7862d0 - 0.7109d0*omega + 1.3168d0*omega**2

    H     = 2.d0 + 10.5d0*(T)**2

    Psi  = 1.d0 + alpha &
         * ( (0.215d0 + 0.28288d0*alpha - 1.061d0*beta + 0.26665d0*H) &
           / (0.6366d0 + beta*H + 1.061d0*alpha*beta)                 )

    mu_adim = EOSvisco(rho, T)
  
    ! Dimensionles thermal conductivity
    k_th = (gamma/Z_c) * mu_adim * Psi

  END FUNCTION condth_chung_dilute
  !===============================
 
END MODULE Visc_Laws
     
