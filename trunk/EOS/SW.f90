MODULE SW

  USE LesTypes
  USE LibComm

  IMPLICIT NONE

  REAL,    PRIVATE, SAVE :: n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12
  REAL,    PRIVATE, SAVE :: zc_sw, tc, r_boltz
  REAL,    PRIVATE, SAVE :: eta1, eta2, eta3, eta4

  REAL, PRIVATE, SAVE :: omega_sw

  REAL, PRIVATE, PARAMETER :: TOL = 1.0E-12

  !*****************************************************
  !       FILE EOS_1.data ; SW Gas Case  17 parameters *
  !*****************************************************
  !"SW"                   !EosLaw                      * 
  !0.4833                 !n1                          *
  !.                                                   *
  !.                                                   *
  !.                                                   *
  !0.04285                !n12                         *
  !0.4833                 !eta1/r_boltz                *
  !.                                                   *
  !.                                                   *
  !.                                                   *
  !0.04285                !eta4/r_boltz                *
  !0.45                   !zc_sw                       *
  !*****************************************************

CONTAINS

  !===================================
  SUBROUTINE SW_init(MyUnit, Dim, Var)
  !===================================

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "sw_init"

    INTEGER, INTENT(IN) :: MyUnit, Dim, Var
    !---------------------------------------

    INTEGER :: statut
    !---------------------------------------

    !--------
    !Read Values
    !--------
    !omega "acentric factor"
    READ(MyUnit, *, IOSTAT = statut) omega_sw !Data%AcentricFactor

    !n1,..., n12
    READ(MyUnit, *, IOSTAT = statut) n1
    READ(MyUnit, *, IOSTAT = statut) n2
    READ(MyUnit, *, IOSTAT = statut) n3
    READ(MyUnit, *, IOSTAT = statut) n4

    READ(MyUnit, *, IOSTAT = statut) n5
    READ(MyUnit, *, IOSTAT = statut) n6
    READ(MyUnit, *, IOSTAT = statut) n7
    READ(MyUnit, *, IOSTAT = statut) n8

    READ(MyUnit, *, IOSTAT = statut) n9
    READ(MyUnit, *, IOSTAT = statut) n10
    READ(MyUnit, *, IOSTAT = statut) n11
    READ(MyUnit, *, IOSTAT = statut) n12

    !eta1,...,eta4
    READ(MyUnit, *, IOSTAT = statut) eta1
    READ(MyUnit, *, IOSTAT = statut) eta2
    READ(MyUnit, *, IOSTAT = statut) eta3
    READ(MyUnit, *, IOSTAT = statut) eta4

    !zc_sw
    READ(MyUnit, *, IOSTAT = statut) zc_sw

    !"eta" Norm
    r_boltz = 8.314472d0
    eta1    = eta1 / r_boltz
    eta2    = eta2 / r_boltz
    eta3    = eta3 / r_boltz
    eta4    = eta4 / r_boltz

    !Tc : critical Temperature
    Tc = Data%Tcrit

    WRITE(*,*) "  Tc (SW)   = ",  Data%Tcrit

  END SUBROUTINE SW_init
  !=====================

  
  !============================
  FUNCTION SW_omega() RESULT(w)
  !============================

    IMPLICIT NONE

    REAL :: w

    w = omega_sw

  END FUNCTION SW_omega
  !====================

  !============================================================================
  !============================================================================
  !BEGIN :           basic FUNCTIONS

  !-------------------------------
  ! > SW_P__rho_T(rho,T)          P = P(rho  , T       )  
  ! > SW_T__rho_P(rho,P)          T = T(rho  , P       )
  ! > SW_T__rho_rhoe(rho,rhoe)    T = T(rho  , rho*e   ),     e := internal energy
  ! > SW_rho__P_T(P,T)            rho = rho(P  , T       )  
  ! > SW_c__rho_T(rho,T)          c = c(rho  , T       )
  ! > SW_e__rho_T(rho,T)          e = e(rho  , T       ),     e := internal energy
  ! > SW_s__rho_T(rho,T)          s = s(rho  , T       ),     s := entropy
  ! > SW_rho__s_T(s  ,T)        rho = rho(s  , T       ),     s := entropy
  ! > SW_T__rho_s(rho, s)         T = T(rho  , s       ),     s := entropy
  ! > SW_P__rho_rhoU_rhoE(rho,rhoU,rhoE)     P = P(rho, rho*|u|, rho*E)  E := total energy = e + |u|**2/2
  ! > SW_dP_de__rho_T(rho,T)                (dP/de)|rho    = dP/de(rho, T)   e:= internal energy
  ! > SW_dP_drho__rho_T(rho,T)              (dP/drho)|e    = dP/drho(rho, T)   e:= internal energy
  ! > SW_pi_rho__rho_u_T(rho,u,T)           (dPI/drho)     = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > SW_pi_rhoe__rho_T(rho,T)               dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)
  ! > SW_GAMMA__rho_T(rho,T)                 GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 
  ! > SW_de_drho__rho_T(rho, T)              (de/drho)|T    = de/drho(rho,T)
  ! > SW_dT_de__rho_T(rho, T)                (dT/de)_rho    = dT/de(rho, T)
  ! > SW_dT_drho__rho_T(rho, T)              (dT/drho)_e    = dT/drho(rho, T)
  ! > SW_TEICALC(h, rho, c1, c2)             Calcul sp�cifique entr�e turbine (lgc = 91)

  !==========================================
  REAL FUNCTION SW_P__rho_T(rho, T) RESULT(P)
  !==========================================
  ! > P = P(rho,T)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: delta, tau, der_psi_delta
    !-------------------------


    delta         = rho
    tau           = 1.d0/T

    der_psi_delta = n1*tau**(0.25d0)+n2*tau**(1.125d0)+n3*tau**(1.5d0)      &
         +n4*2.*delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*     &
         
         delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*                &
         (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)       &
         -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*                &
         delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*                   &
         delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*         &
         3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*         &
         (4.*delta**(3.)-3.*delta**(6.))


    P           = delta/zc_sw/tau*(1. + delta*der_psi_delta)

  END FUNCTION SW_P__rho_T
  !=======================

  !==========================================
  REAL FUNCTION SW_T__rho_P(rho, P) RESULT(T)
  !==========================================
  ! > T = T(rho,P)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, P
    !-------------------------

    REAL  :: T_1, delta, tau, der_psi_delta, &
             der2_psi_tau_delta, der_p_t_cal, pecal, effe, effep
    !-------------------------

    INTEGER          :: i, i_conv, itermax
    !-------------------------

    !...............
    !Newton m�thod
    !...............

    !INITIALIZATION
    T_1        = 0.9d0

    itermax    = 150 
    i_conv     = 0 

    delta      = rho

    DO  i = 1, itermax


       T     = T_1
       tau   = 1./T

       der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)         &
            +n4*2.*delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.* &
            delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*          &
            (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.) &
            -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*          &
            delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*             &
            delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*   &
            3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*   &
            (4.*delta**(3.)-3.*delta**(6.))

       der2_psi_tau_delta= 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)    &
            +1.5                                                         &
            *n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                &
            +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*   &
            tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*     &
            tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*               &
            (5.*delta**(4.)                                              &
            -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*         &
            (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*   &
            (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*        &
            exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))              &
            +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)          &
            -3.*delta**(6.))

       der_p_t_cal = delta/zc_sw*(1.+delta*der_psi_delta-delta*tau*        &
            der2_psi_tau_delta)     

       pecal       = delta/zc_sw/tau*(1.+delta*der_psi_delta)

       effe        = P - pecal
       effep       =-der_p_t_cal

       T_1         = T - effe/effep

       IF( ABS(T_1 - T)/T .le. TOL) THEN
          T      = T_1
          i_conv = 1
          EXIT
       END IF

    END DO

    IF (i_conv .ne. 1) THEN
       print*, "ERROR in EOS - T CALCULATION"
       print*, "SW.f90 - SW_T__rho_P(rho,P)"
       print*, "P = ", P, "rho = ", rho
       print*, "No Convergence in ", itermax, "iterations"
       print*, "T = ", T, "T_1 = ", T_1
       print*, "Err = ", ABS(T_1 - T)/T_1, "> TOL = ", TOL
       PAUSE
    END IF


  END FUNCTION SW_T__rho_P
  !=======================

  !==========================================
  REAL FUNCTION SW_T__rho_rhoe(rho, rhoe) RESULT(T)
  !==========================================
  ! >  T = T(rho, rho*e), e := intern energy

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, rhoe
    !-------------------------

    REAL :: T_1, delta, tau, der_psi0_tau, der2_psi0_tau, &
            eical, dedt, effe, effep, der_psi_tau, der2_psi_tau
    !-------------------------

    INTEGER :: i, i_conv, itermax
    !-------------------------

    !...............
    !Newton m�thod
    !...............

    !INITIALIZATION
    T_1        = 1.0d0
    itermax    = 150
    i_conv     = 0 

    delta      = rho

    !LOOP
    do i = 1, itermax

       T      = T_1

       tau    = 1./T

       der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*          &
            tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*      &
            tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*         &
            delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*     &
            tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)       &
            +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                    &
            +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*       &
            n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                     &
            +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

       der2_psi_tau= -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*  &
            delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*       &
            1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)   &
            *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)        &
            -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)         &
            +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*     &
            3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                  &
            2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)      &
            +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)        &
            +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)

       der_psi0_tau= (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.*   &
            tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)

       der2_psi0_tau=	-(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*             &
            tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)

       eical        = 100.+1./zc_sw*(der_psi_tau+der_psi0_tau)

       dedt         = 1./zc_sw*(der2_psi_tau+der2_psi0_tau)*(-1./T**2.)

       effe         = rhoe/rho - eical
       effep        = -dedt

       T_1          = T - effe/effep

       IF( ABS(T_1 - T)/T_1 .le. TOL) THEN
          T      = T_1
          i_conv = 1
          EXIT
       END IF

    END DO

    IF (i_conv .ne. 1) THEN
       print*, "ERROR in EOS - T CALCULATION"
       print*, "SW.f90 - SW_T__rho_rhoe(rho, rhoe)"
       print*, "rho = ", rho, "rhoe = ", rhoe
       print*, "No Convergence in ", itermax, "iterations"
       print*, "T = ", T, "T_1 = ", T_1
       print*, "Err = ", ABS(T_1 - T)/T_1, "> TOL = ", TOL
       PAUSE
    END IF

  END FUNCTION SW_T__rho_rhoe
  !=======================

  !==========================================
  REAL FUNCTION SW_rho__P_T(P, T) RESULT(rho)
  !==========================================
  ! >  rho = rho(P,T)  

    IMPLICIT NONE

    REAL, INTENT(IN) :: P, T
    !-------------------------

    INTEGER          :: i, i_conv, itermax
    !-------------------------

    REAL             :: f, f_der, delta, delta_1, tau
    REAL             :: der_psi_delta, der2_psi_delta, pecal
    !-------------------------

    !...............
    !Newton m�thod
    !...............

    !INITIALIZATION  (delta = rho)
    delta_1    = 0.01d0
    i_conv     = 0 
    itermax    = 100

    tau        = 1.d0/T

    !LOOP
    DO i = 1, itermax

       delta = delta_1

       der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)           &
            +n4*2.                                                                &
            *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*              &
            delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*                 &
            (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)        &
            -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*                &
            delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*                   &
            delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*         &
            3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*         &
            (4.*delta**(3.)-3.*delta**(6.))


       der2_psi_delta= n4*2.*tau**(1.375)                                    &
            +n5*6.*delta*tau**(0.25)+n6*42.*delta**(5.)*tau**(0.875)+           &
            n7*tau**(0.625)*exp(-delta)*(2.-4.*delta+delta**2.)                 &
            +n8*tau**(1.75)*exp(-delta)*(20.*delta**(3.)-10.*                   &
            delta**(4.)+delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*           &
            (-6.*delta+4.*delta**(3.))+                                         &
            n10*tau**(3.625)*exp(-delta**2.)*(12.*delta**(2.)-18.*              &
            delta**(4.)+4.*delta**(6.))                                         &
            +n11*tau**(14.5)*exp(-delta**3.)*(6.*delta-24*delta**(4.)           &
            +9.*delta**(7.))+n12*tau**(12.)*exp(-delta**3.)*                    &
            (12.*delta**(2.)-30.*delta**(5.)+9.*delta**(8.))


       pecal=delta/zc_sw/tau*(1.+delta*der_psi_delta)



       f     = -1./zc_sw/tau*(1.+delta*der_psi_delta + delta*      &
            (der_psi_delta+delta*der2_psi_delta))

       IF( f .eq. 0.0d0) THEN
          i_conv = 1
          EXIT
       END IF

       f_der = P - pecal

       delta_1 = delta - f_der/f

       IF( ABS(delta_1 - delta)/delta_1 .le. TOL) THEN
          delta  = delta_1
          i_conv = 1
          EXIT
       END IF

    END DO

    IF (i_conv .ne. 1) THEN
       print*, "ERROR in EOS - rho CALCULATION"
       print*, "SW.f90 - SW_rho__P_T(P,T)"
       print*, "P = ", P, "T = ", T
       print*, "No Convergence in ", itermax, "iterations"
       print*, "rho = ", delta, "rho_1 = ", delta_1
       print*, "Err = ", ABS(delta_1 - delta)/delta_1, "> TOL = ", TOL
       PAUSE
    END IF

    rho = delta

  END FUNCTION SW_rho__P_T
  !=======================

  !==========================================
  REAL FUNCTION SW_c__rho_T(rho, T) RESULT(c)
  !==========================================
  !  > c = c(rho,T)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: tau, delta, der_psi_delta, der2_psi_delta, &
            der_psi_tau, der2_psi_tau, der2_psi_tau_delta, &
            der_psi0_tau, der2_psi0_tau, cvr, dprvr, dprtr, ar2
    !-------------------------

    tau=1./T
    delta=rho

    der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)+n4*2.   &
         *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*          &
         delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*             &
         (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)    &
         -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*            &
         delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*               &
         delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*     &
         3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*     &
         (4.*delta**(3.)-3.*delta**(6.))

    der2_psi_delta= n4*2.*tau**(1.375)                                &
         +n5*6.*delta*tau**(0.25)+n6*42.*delta**(5.)*tau**(0.875)+      &
         n7*tau**(0.625)*exp(-delta)*(2.-4.*delta+delta**2.)            &
         +n8*tau**(1.75)*exp(-delta)*(20.*delta**(3.)-10.*              &
         delta**(4.)+delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*      &
         (-6.*delta+4.*delta**(3.))+                                    &
         n10*tau**(3.625)*exp(-delta**2.)*(12.*delta**(2.)-18.*         &
         delta**(4.)+4.*delta**(6.))                                    &
         +n11*tau**(14.5)*exp(-delta**3.)*(6.*delta-24*delta**(4.)      &
         +9.*delta**(7.))+n12*tau**(12.)*exp(-delta**3.)*               &
         (12.*delta**(2.)-30.*delta**(5.)+9.*delta**(8.))


    der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*           &
         tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*     &
         tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*        &
         delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*    &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)      &
         +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                   &
         +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*      &
         n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                    &
         +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

    der2_psi_tau= -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*   &
         delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*       &
         1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)   &
         *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)        &
         -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)         &
         +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*     &
         3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                  &
         2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)      &
         +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)        &
         +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)


    der2_psi_tau_delta= 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)  &
         +1.5                                                         &
         *n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                &
         +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*   &
         tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*     &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*               &
         (5.*delta**(4.)                                              &
         -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*         &
         (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*   &
         (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*        &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))              &
         +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)         &
         -3.*delta**(6.))


    der_psi0_tau = (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.* &
         tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)

    der2_psi0_tau =	-(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*   &
         tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)


    cvr           = -tau**2./zc_sw*(der2_psi0_tau+der2_psi_tau)

    dprvr         = -delta**2./tau/zc_sw*(1.   &
         +2.*delta*der_psi_delta+delta**2.*der2_psi_delta)

    dprtr         = delta/zc_sw*(1.+delta*der_psi_delta-delta*tau*  &
         der2_psi_tau_delta)


    ar2           = 1./rho**2*(T/cvr*dprtr**2-dprvr)

    c             = SQRT(ar2)

  END FUNCTION SW_c__rho_T
  !=======================

  !==========================================
  REAL FUNCTION SW_e__rho_T(rho, T) RESULT(e)
  !==========================================
  !  > e = e(rho,T), e := internal energy

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: delta, tau, der_psi_tau, der_psi0_tau
    !-------------------------

    delta = rho
    tau   = 1./T

    der_psi_tau   = 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*          &
         tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*    &
         tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*       &
         delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*   &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)     &
         +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                  &
         +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*     &
         n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                   &
         +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)


    der_psi0_tau = (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.*     &
         tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)

    e            =100.+1./zc_sw*(der_psi_tau+der_psi0_tau)


  END FUNCTION SW_e__rho_T
  !=======================

  !==========================================
  REAL FUNCTION SW_s__rho_T(rho, T) RESULT(s)
  !==========================================
  !  > s = e(rho,T), s := entropy

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL             :: delta, tau, psi, psi0, der_psi_tau, der_psi0_tau
    !-------------------------

    delta         = rho
    tau           = 1d0/T  	


    psi           = n1*delta*tau**(0.25) + n2*delta*tau**(1.125)              &
         + n3*delta*tau**(1.5) + n4*delta**(2.)*tau**(1.375)                &
         + n5*delta**(3.)*tau**(0.25) + n6*delta**(7.)*tau**(0.875) + n7*   &
         delta**(2.)*tau**(0.625)*exp(-delta)                               &
         + n8*delta**(5.)*tau**(1.75)*exp(-delta) + n9*delta*               &
         tau**(3.625)*exp(-delta**2.) +                                     &
          n10*delta**(4.)*tau**(3.625)*exp(-delta**2.) + n11*               &
         delta**(3.)*tau**(14.5)*exp(-delta**3.)                            &
         + n12*delta**(4.)*tau**(12.)*exp(-delta**3.)

    psi0         =  (eta1 - 1d0)*(-tau - log(1./tau)) + eta2*tc*(-1./(2.*tau) &
         - tau/2.) + eta3*tc**2.*(-1./(6.*tau**2.) - tau/3d0) +             &
         eta4*tc**3.*(-1./(12.*tau**3.) - tau/4.) + log(delta)            

    der_psi_tau  = 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*            &
         tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*      &
         tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*         &
         delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*     &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)       &
         +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                    &
         +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*       &
         n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                     &
         +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

    der_psi0_tau = (eta1-1.)*(-1. + 1./tau) + eta2*tc/2.*(1./tau**2. -   &
         1.) + eta3/3.*tc**2.*(1./tau**3. - 1.) + eta4*tc**3./4.*        &
         (1./tau**4. - 1.)


    s            = 1./zc_sw*(tau*(der_psi_tau + der_psi0_tau) - psi - psi0)

  END FUNCTION SW_s__rho_T
  !=======================

  !==========================================
  REAL FUNCTION SW_rho__s_T(s, T) RESULT(rho)
  !==========================================
  !  > rho = rho(s,T), s := entropy

    IMPLICIT NONE

    REAL, INTENT(IN) :: s, T
    !-------------------------

    REAL    :: ro, delta, tau, psi, psi0, der_psi_tau, der_psi0_tau
    REAL    :: scal, func, der_psi_delta, der_psi0_delta, &
               der2_psi_tau_delta, der_func, delta1, err
    INTEGER :: i, icont
    !-------------------------


    ro=1.
    delta=ro
    tau=1./T

    icont = 0

    do i=1,40

       psi= n1*delta*tau**(0.25)+n2*delta*tau**(1.125)                  &
            +n3*delta*tau**(1.5)+n4*delta**(2.)*tau**(1.375)                &
            +n5*delta**(3.)*tau**(0.25)+n6*delta**(7.)*tau**(0.875)+n7*   &
            delta**(2.)*tau**(0.625)*exp(-delta)                          &
            +n8*delta**(5.)*tau**(1.75)*exp(-delta)+n9*delta*             &
            tau**(3.625)*exp(-delta**2.)+                                 &
            n10*delta**(4.)*tau**(3.625)*exp(-delta**2.)+n11*             &
            delta**(3.)*tau**(14.5)*exp(-delta**3.)                       &
            +n12*delta**(4.)*tau**(12.)*exp(-delta**3.)

       psi0=                                                                 &
            (eta1-1.)*(-tau-log(1./tau)) + eta2*tc*(-1./(2.*tau)-tau/2.)      &
            + eta3*tc**2.*(-1./(6.*tau**2.)-tau/3.) +                         &
            eta4*tc**3.*(-1./(12.*tau**3.)-tau/4.) + log(delta)

       der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*            &
            tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*      &
            tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*         &
            delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*     &
            tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)       &
            +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                    &
            +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*       &
            n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                     &
            +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

       der_psi0_tau= (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.*      &
            tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)


       scal=1./zc_sw*(tau*(der_psi_tau+der_psi0_tau)-psi-psi0)

       func= scal - s         

       der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)       &
            +n4*2.                                                            &
            *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*          &
            delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*             &
            (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)    &
            -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*            &
            delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*               &
            delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*     &
            3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*     &
            (4.*delta**(3.)-3.*delta**(6.))

       der_psi0_delta=1./delta  

       der2_psi_tau_delta= 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)       &
            +1.5*n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                  &
            +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*         &
            tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*           &
            tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*(5.*delta**(4.)      &
            -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*               &
            (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*         &
            (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*              &
            exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                    &
            +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)-3.*           &
            delta**(6.))         


       der_func = 1./zc_sw*(tau*der2_psi_tau_delta-der_psi_delta-der_psi0_delta)

       delta1=MAX(0.01,delta-func/der_func)
       err=abs(delta1-delta)/delta1


       if (err<=1.e-8) THEN
          delta=delta1
          icont=1.
          EXIT
       endif

       delta=delta1

    end do

    if (icont/=1.) then
       print*,"Calcul : SW_rho__s_T(s, T) -> NON Convergence en 40 it�rations"
       print*,"s      =",s
       print*,"T      =",T
       print*,"Erreur =",err
       print*,"rho    =",delta
       STOP
    endif

    rho = delta


  END FUNCTION SW_rho__s_T
  !=======================

  !==========================================
  REAL FUNCTION SW_T__rho_s(rho, s) RESULT(T)
  !==========================================
  !  > T = T(rho, s), s := entropy

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, s
    !-------------------------

    REAL :: delta, tau, psi, psi0, der_psi_tau, der_psi0_tau, &
            der2_psi_tau, der2_psi0_tau, scal, func, der_func, tau1, err
    INTEGER :: i, icont
    !-------------------------
    
    delta = rho
    tau   = 1d0

    icont = 0

    DO i  = 1, 40

       psi= n1*delta*tau**(0.25)+n2*delta*tau**(1.125)                      &
            +n3*delta*tau**(1.5)+n4*delta**(2.)*tau**(1.375)                     &
            +n5*delta**(3.)*tau**(0.25)+n6*delta**(7.)*tau**(0.875)+n7*        &
            delta**(2.)*tau**(0.625)*exp(-delta)                               &
            +n8*delta**(5.)*tau**(1.75)*exp(-delta)+n9*delta*                  &
            tau**(3.625)*exp(-delta**2.)+                                      &
            n10*delta**(4.)*tau**(3.625)*exp(-delta**2.)+n11*                  &
            delta**(3.)*tau**(14.5)*exp(-delta**3.)                            &
            +n12*delta**(4.)*tau**(12.)*exp(-delta**3.)

       psi0=                                                                &
            (eta1-1.)*(-tau-log(1./tau)) + eta2*tc*(-1./(2.*tau)-tau/2.)      &
            + eta3*tc**2.*(-1./(6.*tau**2.)-tau/3.) +                         &
            eta4*tc**3.*(-1./(12.*tau**3.)-tau/4.) + log(delta)

       der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*               &
            tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*          &
            tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*             &
            delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*         &
            tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)           &
            +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                        &
            +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*           &
            n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                         &
            +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

       der_psi0_tau= (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.* &
            tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)

       der2_psi_tau= -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*         &
            delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*              &
            1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)          &
            *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)               &
            -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)                &
            +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*            &
            3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                         &
            2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)             &
            +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)               &
            +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)


       der2_psi0_tau=	-(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*    &
            tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)


       scal=1./zc_sw*(tau*(der_psi_tau+der_psi0_tau)-psi-psi0)


       func= scal - s

       der_func= 1./zc_sw*(der_psi_tau+der_psi0_tau        &
            + tau*(der2_psi_tau+der2_psi0_tau)      &
            - der_psi_tau -der_psi0_tau   ) 

       tau1 = tau - (func)/der_func

       err  = abs(tau1-tau)/tau1

       if (err .le. 1e-5) then

          tau   = tau1
          icont = 1
          EXIT

       endif

       tau=tau1

    end do

    if (icont/=1.) then
       print*,"Calcul : SW_T__rho_s(rho, s) -> NON Convergence en 40 it�rations"
       print*,"rho    =", rho
       print*,"s      =", s
       print*,"Erreur =", err
       print*,"T      =", 1d0/tau
       print*,"T+     =", 1d0/tau1
       STOP
    endif
    
    T   = 1d0/tau

  END FUNCTION SW_T__rho_s
  !=======================

  !==========================================
  REAL FUNCTION SW_P__rho_rhoU_rhoE(rho, rhoU, rhoE) RESULT(P)
  !==========================================
  !  >  P = P(rho, rho*|u|, rho*E) E total energy 

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, rhoU, rhoE
    !-------------------------

    REAL             :: e, T
    !-------------------------

    e = rhoE/rho - 0.5*(rhoU/rho)**2

    T = SW_T__rho_rhoe(rho, rho*e)
    P = SW_P__rho_T(rho,T)


  END FUNCTION SW_P__rho_rhoU_rhoE
  !=======================


  !==========================================
  REAL FUNCTION SW_dP_de__rho_T(rho, T) RESULT(d)
  !==========================================
  !  > (dP/de)|rho = dP/de(rho, T)   e:= internal energy

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: tau, delta, der_psi_delta, der_psi_tau, &
            der2_psi_tau, der2_psi_tau_delta, der_psi0_tau, &
            der2_psi0_tau, cvr, dprtr
    !-------------------------


    tau   = 1./T
    delta = rho

    der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)+n4*2.     &
         *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*            &
         delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*               &
         (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*              &
         delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*                 &
         delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*       &
         3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*       &
         (4.*delta**(3.)-3.*delta**(6.))


    der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*             &
         tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*        &
         tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*           &
         delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*       &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)         &
         +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                      &
         +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*         &
         n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                       &
         +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

    der2_psi_tau= -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*      &
         delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*           &
         1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)       &
         *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)            &
         -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)             &
         +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*         &
         3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                      &
         2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)          &
         +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)            &
         +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)

    der2_psi_tau_delta= 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)       &
         +1.5*n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                  &
         +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*         &
         tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*           &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*               &
         (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*         &
         (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*              &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                    &
         +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)-3.*           &
         delta**(6.))


    der_psi0_tau = (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.*    &
         tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)

    der2_psi0_tau = -(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*       &
         tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)     

    cvr           = -tau**2./zc_sw*(der2_psi0_tau+der2_psi_tau)

    dprtr         = delta/zc_sw*(1.+delta*der_psi_delta-delta*tau* &
         der2_psi_tau_delta)

    d             = dprtr / cvr


  END FUNCTION SW_dP_de__rho_T
  !=======================

  !==========================================
  REAL FUNCTION SW_dP_drho__rho_T(rho, T) RESULT(d)
  !==========================================
  !  > (dP/drho)|e = dP/drho(rho, T)   e:= internal energy

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: tau, delta, der_psi_delta, der2_psi_delta, &
            der_psi_tau, der2_psi_tau, der2_psi_tau_delta, &
            der_psi0_tau, der2_psi0_tau, cvr, dprtr, dprvr, dervr
    !-------------------------

    tau    = 1./T
    delta  = rho

    der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)+n4*2.     &
         *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*            &
         delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*               &
         (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*              &
         delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*                 &
         delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*       &
         3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*       &
         (4.*delta**(3.)-3.*delta**(6.))

    der2_psi_delta= n4*2.*tau**(1.375)                                  &
         +n5*6.*delta*tau**(0.25)+n6*42.*delta**(5.)*tau**(0.875)+        &
         n7*tau**(0.625)*exp(-delta)*(2.-4.*delta+delta**2.)              &
         +n8*tau**(1.75)*exp(-delta)*(20.*delta**(3.)-10.*                &
         delta**(4.)+delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*        &
         (-6.*delta+4.*delta**(3.))+                                      &
         n10*tau**(3.625)*exp(-delta**2.)*(12.*delta**(2.)-18.*           &
         delta**(4.)+4.*delta**(6.))                                      &
         +n11*tau**(14.5)*exp(-delta**3.)*(6.*delta-24*delta**(4.)        &
         +9.*delta**(7.))+n12*tau**(12.)*exp(-delta**3.)*                 &
         (12.*delta**(2.)-30.*delta**(5.)+9.*delta**(8.))


    der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*             &
         tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*        &
         tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*           &
         delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*       &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)         &
         +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                      &
         +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*         &
         n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                       &
         +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

    der2_psi_tau= -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*      &
         delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*           &
         1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)       &
         *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)            &
         -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)             &
         +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*         &
         3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                      &
         2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)          &
         +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)            &
         +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)     

    der2_psi_tau_delta= 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)       &
         +1.5*n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                  &
         +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*         &
         tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*           &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*               &
         (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*         &
         (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*              &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                    &
         +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)-3.*           &
         delta**(6.))     

    der_psi0_tau  = (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.*    &
         tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)

    der2_psi0_tau = -(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*       &
         tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)     

    cvr           = -tau**2./zc_sw*(der2_psi0_tau+der2_psi_tau)

    dprvr         = -delta**2./tau/zc_sw*(1.                     &
         +2.*delta*der_psi_delta+delta**2.*der2_psi_delta)

    dprtr         = delta/zc_sw*(1.+delta*der_psi_delta-delta*tau*      &
         der2_psi_tau_delta)

    dervr         = -delta**2.*(1./zc_sw*(der2_psi_tau_delta))

    d             = -(dprvr-dprtr*dervr/cvr)/rho**2


  END FUNCTION SW_dP_drho__rho_T
  !=======================

  !==========================================
  REAL FUNCTION SW_pi_rho__rho_u_T(rho, u, T) RESULT(pi)
  !==========================================
  !  > (dPI/drho) = dPI/drho(rho, |u|, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, u, T
    !-------------------------

    REAL             :: e, dRho, dE
    !-------------------------

    e    = SW_e__rho_T(rho, T)
    dRho = SW_dP_drho__rho_T(rho, T)
    dE   = SW_dP_de__rho_T(rho, T)

    pi = dRho + (0.5*u*u - e)*dE/rho 

  END FUNCTION SW_pi_rho__rho_u_T
  !=======================

  !==========================================
  REAL FUNCTION SW_pi_rhoe__rho_T(rho, T) RESULT(pi)
  !==========================================
  !  > dPI/d(rho*e)  = dPI/drhoe(rho, T)   PI(rho, rho*u, rho*E) = P(rho, e)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    pi = SW_dP_de__rho_T(rho, T) / rho

  END FUNCTION SW_pi_rhoe__rho_T
  !=======================


  !==========================================
  REAL FUNCTION SW_GAMMA__rho_T(rho, T) RESULT(G)
  !==========================================
  !  > GAMMA         = d(rho*c(s,rho))/drho * (1/rho)  (GAMMA < 0 => BZT Gas) 

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL        ::der_psi_delta,der2_psi_delta,der3_psi_delta,der4_psi_delta
    REAL        ::der_psi_tau,der2_psi_tau,der3_psi_tau                     
    REAL        ::der2_psi_tau_delta,der3_psi_tau2_delta,der3_psi_tau_delta2
    REAL        ::der_psi0_tau,der2_psi0_tau,der3_psi0_tau                  
    REAL        ::delta,tau   
    REAL        ::der_p_ro,der2_p_ro
    REAL        :: cvr,dprvr,dprtr,d2prtr,d2prvr,d2prtrvr,dcvrtr,ar2
    !-------------------------

    tau      = 1./T
    delta    = rho

    der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)+n4*2.     &
         *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*            &
         delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*               &
         (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*              &
         delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*                 &
         delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*       &
         3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*       &
         (4.*delta**(3.)-3.*delta**(6.))

    der2_psi_delta= n4*2.*tau**(1.375)                                  &
         +n5*6.*delta*tau**(0.25)+n6*42.*delta**(5.)*tau**(0.875)+        &
         n7*tau**(0.625)*exp(-delta)*(2.-4.*delta+delta**2.)              &
         +n8*tau**(1.75)*exp(-delta)*(20.*delta**(3.)-10.*                &
         delta**(4.)+delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*        &
         (-6.*delta+4.*delta**(3.))+                                      &
         n10*tau**(3.625)*exp(-delta**2.)*(12.*delta**(2.)-18.*           &
         delta**(4.)+4.*delta**(6.))                                      &
         +n11*tau**(14.5)*exp(-delta**3.)*(6.*delta-24*delta**(4.)        &
         +9.*delta**(7.))+n12*tau**(12.)*exp(-delta**3.)*                 &
         (12.*delta**(2.)-30.*delta**(5.)+9.*delta**(8.))

    der3_psi_delta=                                                    &
         n5*6.*tau**(0.25)+n6*210.*delta**(4.)*tau**(0.875)+n7*           &
         tau**(0.625)*exp(-delta)*(6.*delta-6.-delta**(2.))               &
         +n8*tau**(1.75)*exp(-delta)*(60*delta**(2.)-60.*                 &
         delta**(3.)+15.*delta**(4.)-delta**(5.))                         &
         +n9*tau**(3.625)*exp(-delta**2.)*(-6.+24.*delta**(2.)           &
         -8.*delta**(4.))+n10*tau**(3.625)*exp(-delta**2.)*               &
         (24.*delta-96.*delta**(3.)+60.*delta**(5.)-8.*                   &
         delta**(7.))+n11*tau**(14.5)*exp(-delta**3.)*(6.-114.            &
         *delta**(3.)+135*delta**(6.)-27.*delta**(9.))                    &
         +n12*tau**(12.)*exp(-delta**3.)*(24.*delta-186.*                &
         delta**(4.)+162.*delta**(7.)-27.*delta**(10.))

    der4_psi_delta=                                                    &
         n6*840.*delta**(3.)*tau**(0.875)+n7*tau**(0.625)*                &
         exp(-delta)*(12.-8.*delta+delta**(2.))                           &
         +n8*tau**(1.75)*exp(-delta)*(120.*delta-240*delta**(2.)          &
         +120.*delta**(3.)-20.*delta**(4.)+delta**(5.))                   &
         +n9*tau**(3.625)*exp(-delta**2.)*(60.*delta-80.*delta**(3.)     &
         +16.*delta**(5.))+n10*tau**(3.625)*exp(-delta**2.)*(24.          &
         -336.*delta**(2.)+492.*delta**(4.)-176.*delta**(6.)+16.*         &
         delta**(8.))+n11*tau**(14.5)*exp(-delta**3.)*(-360.*             &
         delta**(2.)+1152.*delta**(5.)-648.*delta**(8.)+81.*              &
         delta**(11.))+n12*tau**(12.)*exp(-delta**3.)*(24.-816.           &
         *delta**(3.)+1692.*delta**(6.)-756.*delta**(9.)                  &
         +81.*delta**(12.))

    der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*             &
         tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*        &
         tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*           &
         delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*       &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)         &
         +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                      &
         +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*         &
         n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                       &
         +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)

    der2_psi_tau= -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*      &
         delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*           &
         1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)       &
         *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)            &
         -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)             &
         +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*         &
         3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                      &
         2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)          &
         +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)            &
         +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)

    der3_psi_tau= 0.75*0.25*1.75*n1*delta*tau**(-2.75)-0.875*            &
         0.125*1.125*n2*                                         &
         delta*tau**(-1.875)-0.5*0.5*1.5*n3*delta*tau**(-1.5)-              &
         0.625*0.375*1.375*n4*delta**(2.)*tau**(-1.625)+1.75*0.75*          &
         0.25*n5*delta**(3.)                                               &
         *tau**(-2.75)+1.125*0.125*0.875*n6*delta**(7.)*tau**(-2.125)       &
         +1.375*0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-2.375)        &
         -0.25*0.75*1.75*n8*tau**(-1.25)*exp(-delta)*delta**(5.)+           &
         1.625*2.625*3.625*n9*tau**(0.625)*exp(-delta**2.)*delta+           &
         1.625*2.625*3.625*n10*tau**(0.625)*exp(-delta**2.)*                &
         delta**(4.)+12.5*13.5*14.5*n11*tau**(11.5)*exp(-delta**3.)*        &
         delta**(3.)                                                        &
         +10.*11.*12.*n12*tau**(9.)*exp(-delta**3.)*delta**(4.)

    der2_psi_tau_delta= 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)       &
         +1.5*n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                  &
         +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*         &
         tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*           &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*               &
         (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*         &
         (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*              &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                    &
         +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)-3.*           &
         delta**(6.))

    der3_psi_tau2_delta= -0.75*0.25*n1*tau**(-1.75)                       &
         +0.125*1.125*n2*tau**(-0.875)+0.5*1.5                               &
         *n3*tau**(-0.5)+0.375*2.*1.375*n4*delta*tau**(-0.625)               &
         -0.75*0.75*n5*delta**(2.)*tau**(-1.75)                              &
         -0.125*7.*0.875*n6*delta**(6.)*                                     &
         tau**(-1.125)-0.375*0.625*n7*exp(-delta)*                           &
         (2.*delta-delta**2.)                                                &
         *tau**(-1.375)+0.75*1.75*n8*tau**(-0.25)*                           &
         exp(-delta)*(5.*delta**(4.)                                         &
         -delta**(5.))+2.625*3.625*n9*tau**(1.625)*exp(-delta**2.)           &
         *(1-2.*delta**(2.))+2.625*3.625*n10*tau**(1.625)*                   &
         exp(-delta**2.)*                                                    &
         (4.*delta**(3.)-2.*delta**(5.))+13.5*14.5*n11*tau**(12.5)*          &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                     &
         +11.*12.*n12*tau**(10.)*exp(-delta**3.)*                           &
         (4.*delta**(3.)-3.*delta**(6.))

    der3_psi_tau_delta2 =  1.375*n4*2.*tau**(0.375)                       &
         +0.25*n5*6.*delta*tau**(-0.75)+0.875*n6*42.*delta**(5.)*            &
         tau**(-0.125)+                                                      &
         0.625*n7*tau**(-0.375)*exp(-delta)*(2.-4.*delta+delta**2.)          &
         +1.75*n8*tau**(0.75)*exp(-delta)*(20.*delta**(3.)-10.*              &
         delta**(4.)+delta**(5.))+3.625*n9*tau**(2.625)*                     &
         exp(-delta**2.)*                                                    &
         (-6.*delta+4.*delta**(3.))+                                         &
         3.625*n10*tau**(2.625)*exp(-delta**2.)*(12.*delta**(2.)-            &
         18.*delta**(4.)+4.*delta**(6.))                                     &
         +14.5*n11*tau**(13.5)*exp(-delta**3.)*(6.*delta-24*                &
         delta**(4.)                                                         &
         +9.*delta**(7.))+12.*n12*tau**(11.)*exp(-delta**3.)*                &
         (12.*delta**(2.)-30.*delta**(5.)+9.*delta**(8.))

    der_psi0_tau= (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.*    &
         tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)

    der2_psi0_tau=	-(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*       &
         tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)

    der3_psi0_tau=	2.*(eta1-1.)/tau**3.+3.*eta2*tc*(1./tau**4.)+    &
         4.*eta3*tc**2.*(1./tau**5.)+5.*eta4*tc**3.*(1./tau**6.)

    cvr= -tau**2./zc_sw*(der2_psi0_tau+der2_psi_tau)

    dprvr= -delta**2./tau/zc_sw*(1.                                      &
         +2.*delta*der_psi_delta+delta**2.*der2_psi_delta)

    dprtr= delta/zc_sw*(1.+delta*der_psi_delta-delta*tau*                &
         der2_psi_tau_delta)

    d2prtr= delta**2.*tau**3./zc_sw*der3_psi_tau2_delta

    der_p_ro=1./tau/zc_sw*(1.+2.*delta*der_psi_delta+delta**2.           &
         *der2_psi_delta)
    der2_p_ro=1./tau/zc_sw*(4.*delta*der2_psi_delta+2.*der_psi_delta+    &
         delta**2.*der3_psi_delta)
    d2prvr=2.*delta**3.*der_p_ro+delta**4.*der2_p_ro

    d2prtrvr=-delta**2./zc_sw*(1.+2.*delta*der_psi_delta+                &
         delta**2.*                                                     &
         der2_psi_delta-delta**2.*tau*der3_psi_tau_delta2-              &
         2.*delta*tau*der2_psi_tau_delta)

    dcvrtr=tau**3./zc_sw*(2.*(der2_psi0_tau+der2_psi_tau)+               &
         tau*(der3_psi0_tau+der3_psi_tau))                          

    ar2=1./rho**2.*(T/cvr*dprtr**2.-dprvr)

    G  = (1./rho)**3./2./ar2*(d2prvr-3.*T/cvr*dprtr*d2prtrvr+               &
         ((T/cvr*dprtr)**2.)*(3.*d2prtr+1./T*dprtr*(1.-T/cvr*            &
         dcvrtr))) 


  END FUNCTION SW_GAMMA__rho_T
  !=======================


  !==========================================
  REAL FUNCTION SW_de_drho__rho_T(rho, T) RESULT(d)
  !==========================================
  ! > P = P(rho,T)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: dervr, der2_psi_tau_delta, delta, tau
    !-------------------------

    tau     = 1.d0/T
    delta   = rho

    der2_psi_tau_delta = 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)       &
         +1.5*n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                  &
         +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*         &
         tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*           &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*               &
         (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*         &
         (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*              &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                    &
         +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)-3.*           &
         delta**(6.))     


    dervr  = -rho**2.*(1./zc_sw*(der2_psi_tau_delta))

    d      = -dervr / rho**2

  END FUNCTION SW_de_drho__rho_T
  !=======================

  !================================================
  REAL FUNCTION SW_dT_drho__rho_T(rho, T) RESULT(d)
  !================================================
  !
  !                 -(de/drho)|_T      -(de/drho)|_T 
  ! (dT/drho)|_e = ---------------- = ----------------
  !                  (de/dT)|_rho          Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: de_drho, c_v
    !-------------------------
    
    de_drho = SW_de_drho__rho_T(rho, T)

    c_v = SW_Cv__rho_T(rho,T)

    d = -de_drho / c_v

  END FUNCTION SW_dT_drho__rho_T
  !=============================

  !==============================================
  REAL FUNCTION SW_dT_de__rho_T(rho, T) RESULT(d)
  !==============================================
  !
  !                         1          1
  ! (dT/de)|_rho = ---------------- = ----
  !                  (de/dT)|_rho      Cv
  ! 
  ! e = e(rho, T)
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: c_v
    !-------------------------
    
    c_v = SW_Cv__rho_T(rho,T)

    d = 1.d0 / c_v

  END FUNCTION SW_dT_de__rho_T
  !===========================

  !=======================================
  FUNCTION SW_dP__dT_dv(rho, T) RESULT(dP)
  !=======================================
  ! 
  ! dP(1) = dP/dT|_v,   dP(2) = dP/dv|_T
  !
    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T

    REAL, DIMENSION(2) :: dP
    !-------------------------

    REAL :: delta, tau
    REAL :: der_psi_delta, der2_psi_delta, der2_psi_tau_delta
    !-------------------------

    tau   = 1.d0/T
    delta = rho

    der_psi_delta = n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)+n4*2.      &
         *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*           &
         delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*              &
         (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)     &
         -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*              &
         delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*                 &
         delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*       &
         3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*       &
         (4.*delta**(3.)-3.*delta**(6.))

    der2_psi_delta = n4*2.*tau**(1.375)                                   &
         +n5*6.*delta*tau**(0.25)+n6*42.*delta**(5.)*tau**(0.875)+        &
         n7*tau**(0.625)*exp(-delta)*(2.-4.*delta+delta**2.)              &
         +n8*tau**(1.75)*exp(-delta)*(20.*delta**(3.)-10.*                &
         delta**(4.)+delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*        &
         (-6.*delta+4.*delta**(3.))+                                      &
         n10*tau**(3.625)*exp(-delta**2.)*(12.*delta**(2.)-18.*           &
         delta**(4.)+4.*delta**(6.))                                      &
         +n11*tau**(14.5)*exp(-delta**3.)*(6.*delta-24*delta**(4.)        &
         +9.*delta**(7.))+n12*tau**(12.)*exp(-delta**3.)*                 &
         (12.*delta**(2.)-30.*delta**(5.)+9.*delta**(8.))

    der2_psi_tau_delta = 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)         &
         +1.5*n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                  &
         +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*         &
         tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*           &
         tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*(5.*delta**(4.)      &
         -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*               &
         (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*         &
         (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*              &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                    &
         +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)-3.*            &
         delta**(6.))
    
    dP(1) = (delta/Zc_sw) &
          * ( 1.d0 + delta*der_psi_delta &
            - delta*tau*der2_psi_tau_delta )

    dP(2) = (-delta**2 / (Zc_sw * tau) ) &
          * ( 1.d0 + 2.d0*delta*der_psi_delta &
                   + delta*delta*der2_psi_delta )

  END FUNCTION SW_dP__dT_dv  
  !========================

  !---------------END basic FUNCTIONS          -----------------------------!
  !============================================================================
  !============================================================================

  !============================================================================
  !============================================================================
  !BEGIN :           derivated FUNCTIONS

  !Description
  ! > SW_c__rho_e(rho, e)          c  = c(rho,e)
  ! > SW_Temperature(Vp)           T  = T(rho,e)
  ! > SW_Pression(Vp)              P  = P(rho,e)
  ! > SW_Epsilon(Vp)               e  = T(rho,P)
  ! > SW_EpsilonDet(Vp)            e  = T(rho,T)
  ! > SW_Son(Vp)                   c  = T(rho,P)
  ! > SW_enthalpy(Vp)              h  = T(rho,P)
  ! > SW_Gas(Vp)            Vp = Vp(rho,e) "subroutine" computes all Vp components from (rho,e) <- Vp
  ! > SW_Cv__rho_T(rho,T)          Cv = Cv(rho,T)
  ! > SW_Cv_oo(T)                  Cv_oo = Cv_ideal(T)
  ! > SW_d_Cv_oo(T)                d_Cv_oo = d_Cv_ideal(T)
  ! > SW_d_Cv_rho(rho,T)           dCv/drho|T
  ! > SW_d_Cv_T(rho,T)             dCv/dT|rho
  ! > SW_Zc()                      zc_sw

  !==========================================
  REAL FUNCTION SW_c__rho_e(rho, e) RESULT(c)
  !==========================================
    ! > c = c(rho, e)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho
    REAL, INTENT(IN) :: e

    REAL :: T
    !-------------------------

    !INTERMEDIATE CALCULATION
    T = SW_T__rho_rhoe(rho,rho*e)

    !RESULT
    c = SW_c__rho_T(rho,T)

  END FUNCTION SW_c__rho_e
  !=======================  


  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !============================================
  REAL FUNCTION SW_Temperature(Vp) RESULT(T)
  !============================================
  ! > T = T(rho, e)

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "SW_Temperature"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, e
    !------------------------------------

    ! DATA
    e     = Vp(i_eps_vp) 
    rho   = Vp(i_rho_vp)

    ! RESULT
    T     = SW_T__rho_rhoe(rho,rho*e)

  END FUNCTION SW_Temperature
  !==========================


  !======================================
  REAL FUNCTION SW_Pression(Vp) RESULT(P)
  !======================================
  ! P = P (e, rho)

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "SW_Pression"

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: e, rho, T
    !-------------------------------------

    !DATA
    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)    ! �nergie interne

    !INTERMEDIATE CALCULATION
    T       = SW_T__rho_rhoe(rho,rho*e)

    !RESULT
    P       = SW_P__rho_T(rho,T)

  END FUNCTION SW_Pression
  !=======================

  !======================================
  REAL FUNCTION SW_Epsilon(Vp) RESULT(e)
  !======================================
  ! > e = e(rho,P)

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "SW_Epsilon"

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T   = SW_T__rho_P(rho,P)

    !RESULT
    e   = SW_e__rho_T(rho,T)

  END FUNCTION SW_Epsilon
  !======================

  !CAUTION : CHECK THAT rho HAS BEEN CALCULATED!!
  !==========================================
  REAL FUNCTION SW_EpsilonDet(Vp) RESULT(e)
  !==========================================
  ! > e = e(rho, T)

    IMPLICIT NONE

    REAL, DIMENSION(:), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    T   = Vp(i_tem_vp)

    ! e = e(rho, T)
    e   = SW_e__rho_T(rho,T)

  END FUNCTION SW_EpsilonDet
  !=========================


  !==================================
  REAL FUNCTION SW_Son(Vp) RESULT (c)
  !==================================
  ! c = c(P, rho)

    IMPLICIT NONE

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !INTERMEDIATE CALCULATION
    T = SW_T__rho_P(rho,P)

    !RESULT
    c = SW_c__rho_T(rho,T)

  END FUNCTION SW_Son
  !==================


  !======================================
  REAL FUNCTION SW_enthalpy(Vp) RESULT(h)
  !======================================
  ! > h = h (P, rho)

    IMPLICIT NONE

    REAL, DIMENSION(: ), INTENT(IN) :: Vp
    !------------------------------------

    REAL :: rho, P, T, e
    !-------------------------------------

    !DATA
    rho = Vp(i_rho_vp)
    P   = Vp(i_pre_vp)

    !CALCULATION
    T = SW_T__rho_P(rho,P)
    e = SW_e__rho_T(rho,T)

    !RESULT
    h = e + P/rho

  END FUNCTION SW_Enthalpy
  !=======================

  !===========================
  SUBROUTINE SW_Gas(Vp)
  !===========================
  ! > (T,P,c) in function of (rho, e), e:= internal energy

    IMPLICIT NONE

    CHARACTER(LEN = *), PARAMETER :: mod_name = "SW_PerfectGas"

    REAL, DIMENSION(:), INTENT(INOUT) :: Vp
    !--------------------------------------

    REAL :: e, rho, P, T, c
    !---------------------------------------

    rho     = Vp(i_rho_vp)
    e       = Vp(i_eps_vp)  ! �nergie interne i_eps_vp

    !CALCULATION
    T       = SW_T__rho_rhoe(rho,rho*e)         !-- Temperature
    P       = SW_P__rho_T(rho,T)                !-- Pressure
    c       = SW_c__rho_T(rho,T)                !-- Sound Speed

    !RESULT
    Vp(i_pre_vp) = P 
    Vp(i_tem_vp) = T
    Vp(i_son_vp) = c

  END SUBROUTINE SW_Gas
  !===========================


  !-------------ENDING derivated FONCTIONS     -----------------------------!
  !----------------------------------------------------------------------------
  !============================================================================

  !============================================================================
  !----------------------------------------------------------------------------
  !-------------BEGINNING useful specific FONCTIONS                  -------!

  
  !=================================
  FUNCTION SW_cv_oo(T) RESULT(cv_oo)
  !=================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: T
    REAL             :: cv_oo
    !-------------------------

    REAL :: der2_psi0_tau, tau
    !-------------------------

    tau = 1.d0 / T

    der2_psi0_tau = -(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*   &
                     tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)

    cv_oo = -tau**2./zc_sw*(der2_psi0_tau)

  END FUNCTION SW_cv_oo  
  !====================

  !=====================================
  FUNCTION SW_d_cv_oo(T) RESULT(d_cv_oo)
  !=====================================

    IMPLICIT NONE

    REAL, INTENT(IN) :: T
    REAL             :: d_cv_oo
    !---------------------------

    REAL :: der2_psi0_tau, der3_psi0_tau, tau
    !-------------------------

    tau = 1.d0 / T

    der2_psi0_tau = -(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*   &
                     tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)

    der3_psi0_tau =	2.*(eta1-1.)/tau**3.+3.*eta2*tc*(1./tau**4.)+    &
         4.*eta3*tc**2.*(1./tau**5.)+5.*eta4*tc**3.*(1./tau**6.)
    
    d_cv_oo = (tau**3./zc_sw) *(2.d0 * der2_psi0_tau +  &
                                 tau * der3_psi0_tau)

  END FUNCTION SW_d_cv_oo  
  !======================

  !===========================================
  REAL FUNCTION SW_Cv__rho_T(rho,T) RESULT(Cv)
  !===========================================
  !  > Cv = Cv(rho,T)

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL :: tau, delta, der2_psi_tau, der2_psi0_tau, cvr
    !-------------------------

    tau = 1.d0 / T

    delta = rho

    der2_psi_tau = -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*               &
                    delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*       &
                    1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)   &
                    *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)        &
                    -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)         &
                    +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*     &
                    3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                  &
                    2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)      &
                    +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)        &
                    +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)

    der2_psi0_tau = -(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*   &
                     tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)

    cvr           = -tau**2./zc_sw*(der2_psi0_tau+der2_psi_tau)

    Cv            = cvr


  END FUNCTION SW_Cv__rho_T
  !========================

  !===============================
  REAL FUNCTION SW_d_Cv_rho(rho,T) RESULT(dCv)
  !===============================
  ! > dCv/drho|T

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL        :: tau, delta, der3_psi_tau2_delta, d2prtr
    !-------------------------

    tau      = 1./T
    delta    = rho

    der3_psi_tau2_delta= -0.75*0.25*n1*tau**(-1.75)                       &
         +0.125*1.125*n2*tau**(-0.875)+0.5*1.5                               &
         *n3*tau**(-0.5)+0.375*2.*1.375*n4*delta*tau**(-0.625)               &
         -0.75*0.75*n5*delta**(2.)*tau**(-1.75)                              &
         -0.125*7.*0.875*n6*delta**(6.)*                                     &
         tau**(-1.125)-0.375*0.625*n7*exp(-delta)*                           &
         (2.*delta-delta**2.)                                                &
         *tau**(-1.375)+0.75*1.75*n8*tau**(-0.25)*                           &
         exp(-delta)*(5.*delta**(4.)                                         &
         -delta**(5.))+2.625*3.625*n9*tau**(1.625)*exp(-delta**2.)           &
         *(1-2.*delta**(2.))+2.625*3.625*n10*tau**(1.625)*                   &
         exp(-delta**2.)*                                                    &
         (4.*delta**(3.)-2.*delta**(5.))+13.5*14.5*n11*tau**(12.5)*          &
         exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                     &
         +11.*12.*n12*tau**(10.)*exp(-delta**3.)*                           &
         (4.*delta**(3.)-3.*delta**(6.))

    d2prtr = delta**2.*tau**3./zc_sw*der3_psi_tau2_delta

    dCv    = -T*d2prtr/rho**2

  END FUNCTION SW_d_Cv_rho
  !=================


  !===============================
  REAL FUNCTION SW_d_Cv_T(rho,T) RESULT(dCv)
  !===============================
  ! > dCv/dT|rho

    IMPLICIT NONE

    REAL, INTENT(IN) :: rho, T
    !-------------------------

    REAL        :: tau, delta, der2_psi0_tau, der2_psi_tau, der3_psi0_tau, der3_psi_tau
    !-------------------------

    tau          = 1./T
    delta        = rho


    der2_psi_tau = -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*      &
         delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*           &
         1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)       &
         *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)            &
         -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)             &
         +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*         &
         3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                      &
         2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)          &
         +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)            &
         +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)

    der3_psi_tau = 0.75*0.25*1.75*n1*delta*tau**(-2.75)-0.875*            &
         0.125*1.125*n2*                                         &
         delta*tau**(-1.875)-0.5*0.5*1.5*n3*delta*tau**(-1.5)-              &
         0.625*0.375*1.375*n4*delta**(2.)*tau**(-1.625)+1.75*0.75*          &
         0.25*n5*delta**(3.)                                               &
         *tau**(-2.75)+1.125*0.125*0.875*n6*delta**(7.)*tau**(-2.125)       &
         +1.375*0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-2.375)        &
         -0.25*0.75*1.75*n8*tau**(-1.25)*exp(-delta)*delta**(5.)+           &
         1.625*2.625*3.625*n9*tau**(0.625)*exp(-delta**2.)*delta+           &
         1.625*2.625*3.625*n10*tau**(0.625)*exp(-delta**2.)*                &
         delta**(4.)+12.5*13.5*14.5*n11*tau**(11.5)*exp(-delta**3.)*        &
         delta**(3.)                                                        &
         +10.*11.*12.*n12*tau**(9.)*exp(-delta**3.)*delta**(4.)




    der2_psi0_tau =	-(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*       &
         tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)

    der3_psi0_tau =	2.*(eta1-1.)/tau**3.+3.*eta2*tc*(1./tau**4.)+    &
         4.*eta3*tc**2.*(1./tau**5.)+5.*eta4*tc**3.*(1./tau**6.)

    dCv           = tau**3./zc_sw*(2.*(der2_psi0_tau+der2_psi_tau)+               &
         tau*(der3_psi0_tau+der3_psi_tau))                          

  END FUNCTION SW_d_Cv_T
  !=================

  !==========================================
  REAL FUNCTION SW_Zc() RESULT(Zc)
  !==========================================
  ! > zc_sw

    IMPLICIT NONE
    !-------------------------

    Zc = zc_sw

  END FUNCTION SW_Zc
  !=======================  

  !==========================================
  REAL FUNCTION SW_TEICALC(h, rho, c1, c2) RESULT(T)
  !==========================================
  ! > fonction sp�cifique entr�e turbine
    
    IMPLICIT NONE

    REAL, INTENT(IN) :: h, rho, c1, c2
    !-------------------------

    REAL :: tr, tau, delta, der_psi_tau, der_psi0_tau, &
            der_psi_delta, der2_psi_tau_delta, der2_psi_tau, &
            der2_psi0_tau, pecalc, eicalc, deicalc, dprtr, func, der_func, tr1, err
    INTEGER          :: icont, i
    !-------------------------

    
    tr=1.
    icont = 0
    tau=1./tr
    delta=rho

    do i=1,20

       der_psi_tau= 0.25*n1*delta*tau**(-0.75)+1.125*n2*delta*          &
            tau**(0.125)+1.5*n3*delta*tau**(0.5)+1.375*n4*delta**(2.)*    &
            tau**(0.375)+0.25*n5*delta**(3.)*tau**(-0.75)+0.875*n6*       &
            delta**(7.)*tau**(-0.125)+0.625*n7*exp(-delta)*(delta**2.)*   &
            tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*delta**(5.)     &
            +3.625*n9*tau**(2.625)*exp(-delta**2.)*delta                  &
            +3.625*n10*tau**(2.625)*exp(-delta**2.)*delta**(4.)+14.5*     &
            n11*tau**(13.5)*exp(-delta**3.)*delta**(3.)                   &
            +12.*n12*tau**(11.)*exp(-delta**3.)*delta**(4.)


       der_psi0_tau= (eta1-1.)*(-1.+1./tau)+eta2*tc/2.*(1./tau**2.-1.)+eta3/3.*     &
            tc**2.*(1./tau**3.-1.)+eta4*tc**3./4.*(1./tau**4.-1.)


       der_psi_delta= n1*tau**(0.25)+n2*tau**(1.125)+n3*tau**(1.5)            &
            +n4*2.                                                                &
            *delta*tau**(1.375)+n5*3.*delta**(2.)*tau**(0.25)+n6*7.*              &
            delta**(6.)*tau**(0.875)+n7*tau**(0.625)*exp(-delta)*                 &
            (2*delta-delta**2.)+n8*tau**(1.75)*exp(-delta)*(5.*delta**(4.)        &
            -delta**(5.))+n9*tau**(3.625)*exp(-delta**2.)*(1.-2.*                &
            delta**(2.))+n10*tau**(3.625)*exp(-delta**2.)*(4.*                   &
            delta**(3.)-2.*delta**(5.))+n11*tau**(14.5)*exp(-delta**3.)*         &
            3.*(delta**(2.)-delta**(5.))+n12*tau**(12.)*exp(-delta**3.)*         &
            (4.*delta**(3.)-3.*delta**(6.))

       der2_psi_tau_delta= 0.25*n1*tau**(-0.75)+1.125*n2*tau**(0.125)       &
            +1.5*n3*tau**(0.5)+2.*1.375*n4*delta*tau**(0.375)                  &
            +0.75*n5*delta**(2.)*tau**(-0.75)+7.*0.875*n6*delta**(6.)*         &
            tau**(-0.125)+0.625*n7*exp(-delta)*(2.*delta-delta**2.)*           &
            tau**(-0.375)+1.75*n8*tau**(0.75)*exp(-delta)*(5.*delta**(4.)      &
            -delta**(5.))+3.625*n9*tau**(2.625)*exp(-delta**2.)*               &
            (1-2.*delta**(2.))+3.625*n10*tau**(2.625)*exp(-delta**2.)*         &
            (4.*delta**(3.)-2.*delta**(5.))+14.5*n11*tau**(13.5)*              &
            exp(-delta**3.)*(3.*delta**(2.)-3.*delta**(5.))                    &
            +12.*n12*tau**(11.)*exp(-delta**3.)*(4.*delta**(3.)-3.*           &
            delta**(6.))

       der2_psi_tau= -0.75*0.25*n1*delta*tau**(-1.75)+0.125*1.125*n2*      &
            delta*tau**(-0.875)+0.5*1.5*n3*delta*tau**(-0.5)+0.375*           &
            1.375*n4*delta**(2.)*tau**(-0.625)-0.75*0.25*n5*delta**(3.)       &
            *tau**(-1.75)-0.125*0.875*n6*delta**(7.)*tau**(-1.125)            &
            -0.375*0.625*n7*exp(-delta)*(delta**2.)*tau**(-1.375)             &
            +0.75*1.75*n8*tau**(-0.25)*exp(-delta)*delta**(5.)+2.625*         &
            3.625*n9*tau**(1.625)*exp(-delta**2.)*delta+                      &
            2.625*3.625*n10*tau**(1.625)*exp(-delta**2.)*delta**(4.)          &
            +13.5*14.5*n11*tau**(12.5)*exp(-delta**3.)*delta**(3.)            &
            +11.*12.*n12*tau**(10.)*exp(-delta**3.)*delta**(4.)

       der2_psi0_tau=	-(eta1-1.)/tau**2.+eta2*tc*(-1./tau**3.)+eta3*       &
            tc**2.*(-1./tau**4.)+eta4*tc**3.*(-1./tau**5.)

       pecalc=delta/zc_sw/tau*(1.+delta*der_psi_delta)

       eicalc=100.+1./zc_sw*(der_psi_tau+der_psi0_tau)


       deicalc = -tau**2./zc_sw*(der2_psi_tau+der2_psi0_tau)

       dprtr= delta/zc_sw*(1.+delta*der_psi_delta-delta*tau*                &
            der2_psi_tau_delta)


       func = eicalc+pecalc/rho+0.5*c1*(c2-pecalc)**2.-h
       der_func = deicalc+dprtr/rho-c1*(c2-pecalc)*dprtr

       tr1=tr-func/der_func
       err = abs(tr-tr1)/tr1

       if (err<=1.e-8) THEN
          tr=tr1
          icont=1.
          EXIT
       endif

       tr=tr1
       tau=1./tr

    end do

    if (icont/=1.) then
       print*,"Calcul : SW_TEICALC(h, rho, c1, c2) -> NON Convergence en 20 it�rations"
       print*,"h      =",h
       print*,"rho    =",rho
       print*,"c1     =",c1
       print*,"c2     =",c2
       print*,"Erreur =",err
       print*,"T      =",tr1
       STOP
    endif

    T = tr1


  END FUNCTION SW_TEICALC
  !=======================


  !-------------ENDING useful specific FONCTIONS                  ----------!
  !============================================================================
  !----------------------------------------------------------------------------




END MODULE SW


