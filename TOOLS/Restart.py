#!/usr/bin/python
import os, sys, re

# Try to create restart's files
def main(args):
  argc = len(args)
  # Check if the number of arguments is valid
  if argc < 1 or argc > 2:
    chaine = "No"
    if argc > 2:
      chaine = "Too many"
    print "ERROR : %s parameters specified - 2 needed" % chaine
    print "Usage : Restart.py casename iteration"
    print "  casename being the case you want to restart"
    print "  iteration the iteration you want to restart from"
    return
  else:
    # Try to read the parameters of the command line
    try:
      casename  = args[0]
      iteration = int(args[1])
    except:
      print "ERROR : wrong parameters specified - need a string and an integer"
      return
    
    # The restart files are like: casename-000-iteration.save
    #  casename being a string containing the name of the case
    #  iteration the iteration number, an integer of length 6 
    #  and 000 the processor number an integer of length 3
    #  the regexp python follows - parenthesis means we want to save 
    #  the processor number if the filename string matches this string
    matchstring = "%s-([0-9]{3})-%6.6d\.save$" % (casename, iteration)

    # List the files in the current directory
    curdir = os.getcwd()
    files  = os.listdir(curdir)
    atLeast1Found = False
    for fichier in files:
      # Check if fichier is one of the files needed for the restart 
      isvalid = re.search(matchstring, fichier)
      if isvalid:
        atLeast1Found = True
        procNumber = isvalid.group(1)
        # We generate the name of the restart file
        link = "%s-%s.save" % (casename, procNumber)
        print "linking %s with %s" % (fichier, link)
        # If the link already exists we overwrite it
        if os.path.lexists(link):
          os.remove(link)
        # Create the link
        os.symlink(fichier, link)

    if not atLeast1Found:
      print "We found no files matching %s-XXX-%6.6d.save" % (casename, iteration)


if __name__ == "__main__":
  main(sys.argv[1:])
  
