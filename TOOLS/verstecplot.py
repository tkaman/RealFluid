#!/usr/bin/python
import sys, os,subprocess, re


def main (args):
 print "aim: create multiple tecplot files in case of many domains"
 argc = len(args)
  # Check if the number of arguments is valid
 if argc < 1 or argc > 2:
    chaine = "No"
    if argc > 2:
      chaine = "Too many"
    print "ERROR : %s parameters specified - 2 needed" % chaine
    print "Usage : Restart.py casename iteration"
    print "  casename being the case you want to 'tecplot'"
    print "  iteration the iteration you want to 'tecplot' from"
    return
 else:
    # Try to read the parameters of the command line
    try:
      casename  = args[0]
      iteration = int(args[1])
    except:
      print "ERROR : wrong parameters specified - need a string and an integer"
      return
 # The plotted files are like: casename-000-iteration.save
 #  casename being a string containing the name of the case
 #  iteration the iteration number, an integer of length 6 
 #  and 000 the processor number an integer of length 3
 #  the regexp python follows - parenthesis means we want to save 
 #  the processor number if the filename string matches this string
 matchstring = "%s-([0-9]{3})-%6.6d\.save$" % (casename, iteration)
 curdir = os.getcwd()
 files  = os.listdir(curdir)
 atLeast1Found = False
 for fichier in files:
   # Check if fichier is one of the files needed for the restart 
   isvalid = re.search(matchstring, fichier)
   if isvalid:
      atLeast1Found = True
      print "processing ", fichier
      subprocess.call(['/mnt/hgfs/abgrall/Project/Rob_UQ/BIN/save2Visu',' tecplot',fichier])


if __name__ == "__main__":
  main(sys.argv[1:])
  
