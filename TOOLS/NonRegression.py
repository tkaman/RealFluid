#!/usr/bin/python
import os, re, datetime, subprocess
from optparse import OptionParser

import threading
import Construire

readLock = threading.RLock() 
writeLock = threading.RLock() 
commands = []
recap = []
successfull = []

# Colors
txtred='\033[91m' # Red
txtgrn='\033[92m' # Green
txtrst='\033[0m'  # Text Reset

# Thread function generating the executables
def genExe(noregdir):
  while True:
    readLock.acquire()
    try:
      program = commands.pop()
    except:
      readLock.release()
      return

    # Generates the command line to compile the program
    try:
      fbxname, cmd, envir = Construire.choix_modele( program, noreg = True, rep = noregdir, nodisplay = True)
    except:
      # Nothing correspond to "program" (for example there is no fast option implemented with gfortran)
      print "It's impossible to build %s" % program
      continue
    finally:
      readLock.release()
    print "-- Building [%s]" % fbxname

    # launches a subprocess - we are interested by the output
    output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
    cmakeok   = False
    programok = False
    generatetext = "[100%%] Built target %s" % fbxname
    saveoutput = []
    line = output.readline()
    warning = 0
    # Reads the output and check some infos
    while line:
      saveoutput.append(line)
      # cmake output line meaning the makefile is ok
      if line[:-1] == "-- Generating done":
        cmakeok = True
      # cmake output line meaning the executable was successfully built
      if line[:-1] == generatetext:
        programok = True
      # Count the number of warnings : If some warnings are not detected add your search here
      if re.search("warning", line, re.I) or re.search("remark #7712", line, re.I):
        warning += 1
      line = output.readline()

    # Save the logfile for the generation of this program
    logfile = open( os.path.join(noregdir,"make_%s_log" % fbxname), "w")
    for line in saveoutput:
      logfile.write(line)
    logfile.close()
        
    # Save some recap
    data = (program, fbxname, cmakeok, programok, len(saveoutput), warning) 
    info = "-- Program [%s] cmake:%s exe:%s output length:%d lines Nb Warnings:%d" % data[1:]

    # Display the info in colors 
    if (cmakeok and programok):
      info = "%s%s%s" % (txtgrn, info, txtrst)
    else:
      info = "%s%s%s" % (txtred, info, txtrst)
      
    writeLock.acquire() 
    print info
    recap.append( data )

    if programok:
      successfull.append( (program, fbxname, envir) )

    writeLock.release()



# Function for launching the regression testing
# 1: Checks the validity of the command line
# 2: Launches the validation procedure
def main(options, argv, noregid):
  """
Regression Testing of FluidBox (addecco):

Usage: ./NonRegression.py -n 2 gfortran ordre_eleve interf opt_maximale

    list of compilers, models, options and modes (full list available with ./Help.py)
    you can type NonRegression.py --help for the complete list of options
"""
  global commands
  print "Regression Testing of FluidBox (addecco) id=[%s]" % noregid

  import check_args, list_tests_cases, submit, environment
  if options.essential:
    argv += check_args.Essential()

  nbproc = options.nbproc
  # Creates a new directory
  noregdir = os.path.join(options.noregdir, "NR%s" % noregid)
  testdir  = options.testdir
  os.makedirs( noregdir )
  os.chdir( noregdir )

  # generate the commands for the Regression testing
  commands, unknown = check_args.genCommands(argv)

  if len(unknown) != 0:
    print "%sWe didn't understood : %s (try option --help)\nAborting! (Have you seen it???)%s" % (txtred, unknown, txtrst)
    return

  nbprog = len(commands)
  if nbprog == 0:
    print "It's weird we don't have anything to build."
    dico = check_args.sort_args(argv)
    print "You specified %d Compilers and %d Models, check your command line (try option --help)" % (
      len(dico["Compiler"]) , len(dico["Model"]) )
    print "%sAborting! (Have you seen it???)%s" % (txtred, txtrst)
    return
  else:
    print "We are going to generate %d programs with %d threads" % (nbprog, nbproc)
      
  # We launch nbproc threads calling : genExe(noregdir)
  lthreads = []
  for i in range(nbproc):
    thread = threading.Thread(None, genExe, None, (noregdir,))
    thread.start()
    lthreads.append( thread )

  # We wait for all threads to finish
  for thread in lthreads:
    thread.join()
     
  print "------------"
  print "--  Done  --"
  print "------------"

  # We write a recap of the generation of the executables in the file "recap_log"
  logfile = open( os.path.join(noregdir,"recap_log"), "w")
  logfile.write( '<noreg id="%s">\n' % noregid )
  logfile.write( '\t<args>%s</args>\n\t<nproc>%d</nproc>\n' % (argv, nbproc))
  try:
    machine = check_args.sort_args(argv)["Machine"][0]
  except:
    machine = os.uname()[1]
  logfile.write( '\t<machine>%s</machine>\n' % environment.genericMachineName(machine) )
  logfile.write( '\t<user>%s</user>\n' %  os.getlogin() )
  logfile.write( "<programs>\n")
  makefile_generated = 0
  builds_successfull = 0

  nbprog = len(recap)
  for line in recap:
    xml = '<program name="%s">\n\t<cmake>%s</cmake>\n\t<exe>%s</exe>\n\t<len>%d</len>\n\t<warnings>%d</warnings>\n\t'% line[1:]
    xml += '<commands>%s</commands>\n' % line[0]
    xml += '<log><![CDATA['
    makelog = open( os.path.join(noregdir,"make_%s_log" % line[1]), "r")
    for l in makelog.readlines():
      xml += l
    makelog.close()
    xml += ']]></log>\n</program>\n' % line[0]
    logfile.write(xml)
    if line[2]:
      makefile_generated += 1
    if line[3]:
      builds_successfull += 1
                  
  logfile.write("</programs>\n</noreg>\n")
  logfile.close()

  print "%d/%d Makefiles generated" % ( makefile_generated, nbprog )
  print "%d/%d Builds Successfull"  % ( builds_successfull, nbprog )
  
  # now we will run the tests cases that we can launch with the exe we just built
  # Checking what test case we can run with the executables generated
  if not options.compileOnly:
    list_tests_cases.list_and_launch(successfull, nbproc, noregdir, testdir)

  # send the regression to the database
  submit.createTarFile(noregdir)
  submit.submitTarFile(noregdir)
  
  # Regression or not?
  print "%s%s%s" % (txtred, submit.getRegression(noregid), txtrst)

#####################################################
# This is the entry point of the Regression testing #
#####################################################
if __name__ == "__main__":
  # generate an id (unique) for the tests : 14 numbers based on the time
  # (we assume we don't launch 2 tests in less than 1sec)
  noregid = "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d"% datetime.datetime.now().timetuple()[0:6]

  # Set the path to include all the Python files we need
  Construire.expandPath()

  # Get the path of this file
  thisfile = os.path.abspath( __file__ )
  thisrep =  os.path.split(thisfile)[0]
  # Set the default paths for the noreg directory and the test directory
  noregdir = os.path.join(thisrep, "..", "NOREG")
  testdir  = os.path.join(thisrep, "..", "TESTS")

  # Defining the options of the program
  usage="""usage: %prog [Options] args

  args : list of compilers, models, options and modes (full list available with ./Help.py)
    type -h or --help to show the valid options"""
  # Creating the parser (http://docs.python.org/library/optparse.html) 
  parser = OptionParser(usage=usage)
  # Adding the options
  parser.add_option("-n", "--nbproc", dest="nbproc",
                    help="Set the number of processors",
                    metavar="NBPROC", type=int, default=1)
  parser.add_option("-e", "--essential", dest="essential",
                    help="Launch the \"essential\" of the tests",
                    action="store_true", default=False)
  parser.add_option("-c", "--compile-only", dest="compileOnly",
                    help="Just compile the executables don't run the tests",
                    action="store_true", default=False)
  parser.add_option("-m", "--mail", dest="mailto",
                    help="Send a recap of the tests by email to EMAIL@DRESS. You can use this option more than once",
                    action="append",metavar="EMAIL@DRESS", type=str, default=[])
  parser.add_option("-t", "--test-directory", dest="testdir",
                    help="Set the directory containing the tests cases to TESTDIR, pour tester le script, utiliser les tests de TESTSCRIPT",
                    default= testdir)
  parser.add_option("-d", "--noreg-directory", dest="noregdir",
                    help="Set the root directory of the non regression to NOREGDIR",
                    default= noregdir)
  # Parse the command line
  (options, args) = parser.parse_args()
  options.noregdir = os.path.abspath(options.noregdir)
  options.testdir  = os.path.abspath(options.testdir)

  # Go go go
  main(options, args, noregid)

