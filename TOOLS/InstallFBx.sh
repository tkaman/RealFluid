#!/bin/bash

# This file defines 4 bash functions

# Display the FBx environment variables
function displayfbxvar {
    echo "FBx sources directory RFB is set to $RFB";
    echo "FBx TOOLS directory TFB is set to $TFB";
    echo "FBx BIN directory BFB is set to $BFB";
}

# Set RFB to the current directory - usage: cd mytrunk; rfb
function rfb {
    export RFB=$(pwd);
}

# Set TFB to the current directory and BFB to $TFB/../BIN - usage: cd TOOLS; tfb
function tfb {
    export TFB=$(pwd);
    export BFB=`readlink -f $(pwd)/../BIN`;
    export PATH=$TFB:$BFB:$PATH;
}

# Set RFB, TFB and BFB assuming we work in the trunk - usage: cd TOOLS; fbxclassic 
#                                                        or : cd mytrunk;fbxclassic (in this case TOOLS must be in ../TOOLS and BIN in ../BIN) 
function fbxclassic {
  curdir1=${PWD};
  base=`basename ${PWD}`;
  if [[ $base == "TOOLS" ]];
  then
		export RFB=`readlink -f ${PWD}/../trunk`;
		tfb
	else
		if [[ $base == "BIN" ]];
		then
			export RFB=`readlink -f ${PWD}/../trunk`;
			cd ../TOOLS;
			tfb	    
		else
			export BFB=`readlink -f ${PWD}/../BIN`;
			export TFB=`readlink -f ${PWD}/../TOOLS`; 
			export PATH=$BFB:$TFB:$PATH;
			rfb
		fi
	fi;
	cd ${curdir1}
}




# This is the command executed when we source this file
case "${1:-}" in
  '') 
	fbxclassic
	;;
  *)
	curdir=${PWD};
	cd $1;
	fbxclassic;
	cd ${curdir};
	;;
esac
