#!/usr/bin/python
import os, sys, subprocess
import Construire

# Try to rebuild a version of FluidBox
def main(args):
  import check_args
  argc = len(args)
  # Check if the number of arguments is valid
  if argc < 1 or argc > 3:
    chaine = "No"
    if argc > 3:
      chaine = "Too many"
    print "ERROR : %s parameters specified" % chaine
    print "Usage : ./Redo.py exename [nbproc] [!]"
    print "  exename being the exe you want to rebuild"
    print "  nbproc the number of processors to build it (optional)"
    print "  ! means you want to rebuild it from scratch (optional)"
    return
  else:
    # Parse the command line
    nbproc = 1
    from_scratch = False
    # If we find "!" we set from_scratch to true and suppress "!" from the command line
    while "!" in args:
      args.remove("!")
      from_scratch = True
      
    # Here we check if we specified a number of processor (integer) and suppress it from the command line
    arg2 = []
    for elt in args:
      arg2.append(elt)
      try:
         nb = int(elt)
         nbproc = max(nb, 1)
         arg2.remove(elt)
      except :
        pass

    # Here we should only have one element in the command line : the exename
    argc = len(arg2)
    if argc > 1:
      print "Error : Can't parse the command line"
      print "  We still have to parse :" , arg2
      return
    elif argc == 0: 
      print "Error : Can't parse the command line"
      print "  No exename specified"
      return
    else:
      exename = arg2[0]

    # Get all the options possible
    total = check_args.Total()
    # generate the list of all the different model we can generate with this version of FluidBox
    commands, unknown = check_args.genCommands(total, display=False)


    # Try to load a file in order to run faster
    try:
      import lastbuild
      commands = [lastbuild.program] + commands
    except:
      pass
 
    # If the name correspond with the name specified on the command line we generate the exe
    for program in commands:
      try:
        fbxname, envir = Construire.choix_modele(program, nodisplay = True, computeNameOnly = True)
        if fbxname == exename:
          # Get the path of this file
          thisfile = os.path.abspath( __file__ )
          thisrep =  os.path.split(thisfile)[0]
          exepath = os.path.join(thisrep, "../OBJECTS/O%s" % fbxname)

          # save a file in order to run faster next time
          try:
            f = open(os.path.join(thisrep,"lastbuild.py"), "w")
            f.write("program=%s" % repr(program))
            f.close()
          except:
            print "Error writing lastrun"

          # Command to run if the Makefile exists and if we dont want to build from scratch
          if os.path.exists(os.path.join(exepath, "Makefile")) and not from_scratch:
            subprocess.Popen("bash -c '%s cd %s && make -j%d'" % ( envir, exepath, nbproc),
                      shell=True).wait()                             
          else:
            # Command to run if the Makefile doesn't exists or if we wan't to rebuild from scratch
            Construire.choix_modele(program, nbprocs = nbproc, nodisplay = True)
          return
      except:
        pass
    # Fail : the exename specified can not be generated
    print "%s : Not a valid name" % exename


if __name__ == "__main__":
  # Set the path to include all the python files we need
  Construire.expandPath()

  main(sys.argv[1:])
  
