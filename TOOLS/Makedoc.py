#!/usr/bin/python
import os, sys, shutil, subprocess

# display some helping stuff
def help(docrep):
  try:
    os.chdir(docrep)
    fhelp = open("README", "r")
    lines = fhelp.readlines()
    for line in  lines:
      print line[:-1]
    fhelp.close()
  except:
    print "Error : Can't open the README file"
    print "  we tried to help, but the help"
    print "  file is unavailable :'("


# Try to generate the documentation
def main(docrep, doxyrep, args):
  # Check if the number of arguments is valid
  if "help" in args:
    help(doxyrep)

  elif "clean" in args:
    print "\nCleaning FluidBox's Documentation\n"
    if os.path.exists(docrep):
      shutil.rmtree(docrep)
    try:
      os.remove('myDox')
      os.remove('myDox1')
      os.remove('out')
    except:
      pass    

  else:
    rfb = os.getenv("RFB")
    if not os.path.exists(rfb):
      print "Error : you have to define the environment variable RFB\n"
      help(doxyrep)
    else:
      print "\nGenerating FluidBox Documentation"
      print "for more informations type : Makedoc.py help\n"
      if not os.path.exists(docrep):
        print "Creating DOC directory"
        os.mkdir(docrep)


      os.chdir(doxyrep)
      print "Looking for files in $RFB=%s" % rfb

      # Set the input and output directories of the doc to docrep and rfb
      subprocess.Popen(
        'sed -e "s@^OUTPUT_DIRECTORY .*@OUTPUT_DIRECTORY = %s@" Doxyfile > myDox1' % docrep,
        shell=True).wait()
      subprocess.Popen(
        'sed -e "s@^INPUT .*@INPUT = %s@" myDox1 > myDox' % rfb, shell=True).wait()

      print "Generating html Documentation"
      print "This can take a long time, open the 'out' file in FBx/TOOLS/DOXYGEN if there is a problem"
      
      # Generate the documentation using Doxygen
      process = subprocess.Popen("doxygen myDox", shell=True,
          stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      # Save the output in the file out
      fout = open("out", "w")
      foutput, ferr = process.communicate()
      fout.write(foutput)
      fout.close()

      # Generate an html file : the entry point to the documentation
      os.chdir(docrep)
      fichier = open("FluidBox_Doc.html", "w")
      fichier.write('<HTML><HEAD><META HTTP-EQUIV="refresh" CONTENT="0;URL=html/index.html"></HEAD><BODY></BODY></HTML>')
      fichier.close()

      print "Generating the pdf output in FBx/DOC/FluidBox_Doc.pdf"
      print "Open the 'FBx/DOC/latex/out' file if there is a problem"
      os.chdir("latex")
      subprocess.Popen('make pdf > out && mv refman.pdf ../FluidBox_Doc.pdf',
                      shell=True).wait()
      print "Documentation successfully generated"



if __name__ == "__main__":  
  # Get the path of this file
  thisfile = os.path.abspath( __file__ )
  thisrep  = os.path.split(thisfile)[0]
  # Get the path of the doxygen files and of the output directory
  doxyrep  = os.path.join(thisrep, "DOXYGEN")
  docrep   = os.path.realpath( os.path.join(thisrep, "..", "DOC") )

  main(docrep, doxyrep, sys.argv[1:])
  

