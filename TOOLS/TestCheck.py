#!/usr/bin/python
import sys, os
import Construire
# Colors
txtred='\033[91m' # Red
txtrst='\033[0m'  # Text Reset

def main(argv): 
  import list_tests_cases
  argc = len(argv)
  if argc == 0:
    if os.path.exists("testcase.py"):
      print "This test can run"
      list_tests_cases.listTests(".", verbose = True)
    else:
      print "%sERROR:" % txtred
      print "  You need to run this script in a test case directory"
      print "   Or you can specify 1 or more test cases's directory"
      print "   on the command line%s" % txtrst
  else:
    for path in argv:
      globPath = os.path.join(path,"testcase.py")
      globPath = os.path.abspath(globPath)
      if os.path.exists(globPath):
        if not list_tests_cases.isValidCasePath(path):
          print "%sERROR:" % txtred
          print "  The test in %s contains an invalid testcase.py file%s" % (path, txtrst)
        else:
          print "The test in %s defines" % globPath
          list_tests_cases.listTests(path, verbose = True)
      else:
        print "%sERROR:" % txtred
        print "  %s is an invalid test case path" % globPath
        print "   To be valid a test needs a testcase.py file%s" % txtrst

if __name__ == "__main__":
  Construire.expandPath()
  main(sys.argv[1:])
