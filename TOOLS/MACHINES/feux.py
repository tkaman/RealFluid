# Specify the generic name of this environment
name = "feux"

# Specify the name of the machines on which we use this environement (linux command: uname -n)
# this names are a regexp see http://docs.python.org/library/re.html
machine_names = ["agni", "vulcain", "hephaistos", "loki"]

# Specify the command to set the environment
data = {
'gfortran&mpi': 'source /opt/mpich2/env.sh',
'ifort&mpi': 'source /opt/env/env_64.sh && source /opt/intel/mpich2/env.sh',
'ifort&scotch': 'source /opt/env/env_64.sh && source /opt/scotch/int32/env.sh',
'gfortran': '',
'gfortran&scotch': 'source /opt/scotch/int32/env.sh',
'ifort': "source /opt/env/env_64.sh"
}
