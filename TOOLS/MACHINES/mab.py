# Specify the generic name of this environement
name = "mab"

# Specify the name of the machines on which we use this environement (linux command: uname -n)
# this names are a regexp see http://docs.python.org/library/re.html
machine_names = ["epeire", "dolomede"]

# Specify the command to set the environment
data = {
'gfortran&mpi': 'module purge && module add gnu-openmpi',
'ifort&mpi': 'module purge && module add intel-mvapich2',
'ifort&scotch': 'module purge && module add intel-compiler && export SCOTCH_HOME=/home/jacq/projets/scotch_5.1',
'gfortran': 'module purge && module add gnu-compiler',
'gfortran&scotch': 'module purge && module add gnu-compiler && export SCOTCH_HOME=/home/jacq/projets/scotch_5.1',
'ifort': 'module purge && module add intel-compiler'
}
