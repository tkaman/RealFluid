# Specify the generic name of this environment
name = "mygale"

# Specify the name of the machines on which we use this environement (linux command: uname -n)
# this names are a regexp see http://docs.python.org/library/re.html
machine_names = ["devel[0-9]+", "fourmi[0-9]+"]

# Specify the command to set the environment
data = {
'gfortran&mpi': 'module purge && module add meta/gnu-mvapich2',
#'ifort&mpi': 'module purge && module add compiler/intel/12.0.5.220 && module add mpi/mvapich2/latest && module add mpi/mpiexec/0.84',
'ifort&mpi': 'module purge && module add compiler/mkl/11.1.3 && module add compiler/intel/stable && module add mpi/openmpi/1.8.1',
#'ifort&scotch': 'module purge && module add compiler/intel-11.1-072 mpi/mvapich2-1.5 scotch-5.1.9_32',
'ifort&scotch': 'module purge && module load compiler/intel/12.0.5.220 && module load mpi/openmpi/1.6.2 && module load scotch/int32/5.1.12b',
'gfortran': 'module purge && module add compiler/gcc-4.5.1',
'gfortran&scotch': 'module purge && module add compiler/gcc-4.5.0 mpi/intel-4.0.0.028 scotch-5.1.9_32',
'ifort': 'module purge && module add compiler/mkl/11.1.3 && module add compiler/intel/stable'
}
