import os, subprocess

# Return the revision number and the date of the commit or (-1,"") on error
def getSubversion(rep):
  rev = -1
  date = ""
  foutput = subprocess.Popen("cd %s && svn info" % rep, shell=True,
                       stdout=subprocess.PIPE).stdout
  ol = foutput.readlines()
  i = 0
  for line in ol:
    i += 1
    subline = line[:-1].split(":",1)
    if i == 5:
      rev = int(subline[1])
    if i == 10:
      date = subline[1]
  return rev, date

# Create a file containing some svn informations which are displayed by FBx
def makeVersionModule(rep, repobject):
  curdir = os.getcwd()
  try:
    os.chdir(repobject)
    fichier = open("svnversion.f90","w")
    fichier.write("MODULE svnversion\n")
    version, date = getSubversion(rep)
    fichier.write("  INTEGER, PARAMETER, PUBLIC :: svnver=%d\n" % version)
    fichier.write("  CHARACTER(LEN=*), PARAMETER, PUBLIC :: svndate=\"%s\"\n" % date)
    fichier.write("END MODULE svnversion\n")
    fichier.close()
  except:
    print "We where unable to create the file svnversion.f90 in %s" % repobject
  finally:
    os.chdir(curdir)
