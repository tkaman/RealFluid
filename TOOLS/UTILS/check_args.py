import compilers_options, mode, models, environment

# Check if an argument on command line matches one of the following
# FBx compilation directive
def isCompiler(value):
    compiler_list = compilers_options.data
    return value in [compiler.compilo for compiler in compiler_list]

def isMode(value):
    mode_list = mode.data
    return value in [modes.name for modes in mode_list]

def isOption(value):
    option_list = compilers_options.explain.keys()
    return value in option_list

def isModel(value):
    model_list = [model[1] for model in models.data]
    return value in [model.name for model in model_list]

def isMachine(value):
    return environment.isMachine(value)

# Check if a model has the attribute seqonly (sequential mode only)
def isModelOnlySeq(value):
    model_list = [model[1] for model in models.data]
    for model in model_list:
        if value == model.name:
            return model.seqonly
    # An error occured weird because we checked before (only if called from NonRegression.py)
    return False
                
# The following part is just for user-nearly-friendly display, not important
class InvalidKeyword:
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "%s is defined in more than one place check in files : compilers_options.py mode.py and models.py" % repr(self.value)


def Category(value):
    comp  = isCompiler(value)
    mode  = isMode(value)
    opt   = isOption(value)
    model = isModel(value)
    compu = isMachine(value)

    #Checking keyword validity: a keyword must have only one meaning!
    count = 0
    if comp:
        count += 1
    if mode:
        count += 1
    if opt:
        count += 1
    if model:
        count += 1
    if compu:
        count += 1

    if count > 1:
        raise InvalidKeyword(value)
    
    # Returning the keyword's meaning
    if comp:
        return "Compiler"
    elif mode:
        return "Mode"
    elif opt:
        return "Option"
    elif model:
        return "Model"
    elif compu:
        return "Machine"
    
    return "Unknown"


def sort_args(argv, display = False):
  # dico will contain all the informations
  dico = {"Compiler":[], "Mode":[], "Model":[] ,"Option" :[], "Unknown":[], "Machine":[] }

  # For every argument we check if it means something and moves
  # it where it belongs
  if display:
    print "Checking command line arguments"
    print "-------------------------------"

  for val in argv:
    key = Category(val)
    dico[key].append(val)
    if display:
      print "%s is %s" % (val, key)

  if display:
    print "-------------------------------"

  return dico





def showCompiler():
    print "-------------------------------"
    print " Compiler List"
    print "-------------------------------"
    compiler_list = compilers_options.data
    for compiler in compiler_list:
      print compiler.compilo

def showMode():
    print "-------------------------------"
    print " Mode List"
    print "-------------------------------"
    mode_list = mode.data
    for modes in mode_list:
      print modes.name
    

def showOption():
    print "-------------------------------"
    print " Option List"
    print "-------------------------------"
    option_list = compilers_options.explain.keys()
    for opt in option_list:
      print opt


def showModel():
    print "-------------------------------"
    print " Model List"
    print "-------------------------------"
    model_list = [model[1] for model in models.data]
    for model in model_list:
      print model.name



def Essential():
    allarg = []
    allarg += [model[1].name for model in models.data]
    allarg += ["seq", "mpi"]
    allarg += [comp.compilo for comp in compilers_options.data]
    return allarg

def Total():
    allarg = []
    allarg += [model[1].name for model in models.data]
    allarg += compilers_options.explain.keys()
    allarg += [modes.name for modes in mode.data]
    allarg += [comp.compilo for comp in compilers_options.data]
    return allarg


def genCommands(argv, display=True):
  dico = sort_args(argv, display=display)

  # if Mode is empty we assume we just want to test sequential mode
  if len(dico["Mode"]) == 0:
    dico["Mode"] = ["seq"]
    print "Assuming sequential mode"
  # if Option is empty we assume we just want to test default
  if len(dico["Option"]) == 0:
    dico["Option"] = ["default"]
    print "Assuming default option"
    
  commands = []
  # generate the list of the programs
  for compiler in dico["Compiler"]:
    for option in dico["Option"]:
      for model in dico["Model"]:
        if isModelOnlySeq(model):
          commands.append( [compiler, model, option] )
        else:
          for mode in dico["Mode"]:
            commands.append( [compiler, model, option, mode] )

  return commands, dico["Unknown"]
