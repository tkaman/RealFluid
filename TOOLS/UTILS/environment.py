import os, re

def isMachine(machinename):
  # Get the path of this file
  thisfile = os.path.abspath( __file__ )
  thisrep  = os.path.split(thisfile)[0]
  machines = os.path.join(thisrep, "..", "MACHINES") 
  
  # List the files in TOOLS/MACHINES
  files = os.listdir(machines)
  for fichier in files:
    # Get only the python files
    if re.search("\.py$", fichier):
      dico = {}
      execfile(os.path.join(machines, fichier), dico)
      try:
        # If machinename matches with a name in machine_names this is a valid machine name!
        for name in dico["machine_names"]:
          if re.match(name, machinename):
            return True
        if re.match(dico["name"], machinename):
          return True
      except:
        pass
  return False


def getMachineEnv(machinename):
  # Get the path of this file
  thisfile = os.path.abspath( __file__ )
  thisrep  = os.path.split(thisfile)[0]
  machines = os.path.join(thisrep, "..", "MACHINES") 

  # List the files in TOOLS/MACHINES
  files = os.listdir(machines)
  for fichier in files:
    if re.search("\.py$", fichier):
      dico = {}
      # Get only the python files
      execfile(os.path.join(machines, fichier), dico)
      # Check the validity of the file
      valid = "machine_names" in dico.keys() and "name" in dico.keys() and "data" in dico.keys()
      if valid:
        valid = type(dico["machine_names"]) == type([]) and type(dico["name"]) == type("") and type(dico["data"]) == type( {} )

      # Print an error message if it is not valid
      if not valid:
        print "ERROR:"
        print "  '%s' is not a valid machine file" % os.path.join(machines, fichier)
        print "this file should contains:"
        print "    a string called name"
        print "    a list of string called machine_names"
        print "    a dictionary called data"
        print "look in '%s' for a working example" % os.path.join(machines, "machines_feux.py")
      else:
        for machine in dico["machine_names"]:
          if re.match(machine, machinename):
            return dico["name"], dico["data"]
        if re.match(dico["name"], machinename):
          return dico["name"], dico["data"]

  return 


def setenvir(model, compilo, envir, machine = None, display = True):
  if model == "INTERF":
    cle = "%s&scotch" % compilo
  elif envir ==  "seq":
    cle = compilo
  else:
    cle = "%s&%s" % (compilo , envir)

  if machine == None:
    machinename = os.uname()[1]
  else:
    machinename = machine

  # Get the path of this file
  thisfile = os.path.abspath( __file__ )
  thisrep =  os.path.split(thisfile)[0]
  
  error = False
  # If "localenvir.py" exists it owerwrites this file!
  if os.path.isfile( os.path.join(thisrep, "..", "localenvir.py") ):
    if display:
      print "-- Local Environment file present - using it"
    import localenvir
    try:
      vals = localenvir.data[cle]
    except:
      error = True
  else:
    try:
      name, values = getMachineEnv(machinename)
      if display:
        print "-- Using values defined for %s" % name     
    except:
      if display:
        print "-- No values found for %s" % machinename

    try:
      vals = values[cle]
    except:
      error = True

  if error:
    if display:
      print "-- Commands for setting environment Not found for [%s] - Hoping environment is already set" % cle
    return ''
  else:
    return vals


def genericMachineName(machine = None):
  if machine == None:
    machinename = os.uname()[1]
  else:
    machinename = machine

  try:
    name, values = getMachineEnv(machinename)
    return name
  except:
    return machinename
