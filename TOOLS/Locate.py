#!/usr/bin/env python
# -*- coding: latin-1 -*-
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#*                                                                           */
#*                  This file is part of the IDE for FluidBox (FBEDI)        */
#* BEWARE : The licence for this file is not the licence for FluidBox itself */
#*                                                                           */
#*    Copyright (C) 2007-2008 R�mi Butel                                     */
#*                  2007-2008 Institut de Math�matiques de Bordeaux          */
#*                            UMR 5251 C.N.R.S. Universit� Bordeaux I        */
#*                                                                           */
#*  FBEDI is distributed under the terms of the Cecill-B License.            */
#*                                                                           */
#*  You can find a copy of the Cecill-B License at :                         */
#*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                 */
#*                                                                           */
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#-- ************************************************
#-- *         R�mi BUTEL                           *
#-- *                                              *
#-- *      INSTITUT DE MATHEMATIQUES DE BORDEAUX   *
#-- *      351, cours de la Lib�ration             *
#-- *      33405 TALENCE CEDEX                     *
#-- *      FRANCE                                  *
#-- *                                              *
#-- * t�l:     (33)  5.40.00.60.55                 *
#-- * fax:     (33)  5.40.00.21.23                 *
#-- * e-mail:  Remi.Butel@math.u-bordeaux1.fr      *
#-- ************************************************
""" Resume : trouver un item dans les sources de FluidBox"""
#-- 2008/06/03

SH_AUTEUR = "R.Butel, IMB"
SH_VERSION = "1.1"
SH_VERSION = "2.0" #-- passage en Python
SH_VERSION = "2.1" #-- ajout du mode =repertoire apres la cl�
SH_VERSION = "3.0" #-- restructuration; introduction du mode "S"
SH_DATE = "2009/02/23"
SH_GROUPE = "[SRC/gest]"
SH_ACTIONS = "@RF_display"

import sys
import os, re
import datetime

debug = 2
debug = 0
RFB = os.getenv("RFB")

date = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

class Exit(Exception):
  """Exit"""
  pass

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#                                                       -- Locate
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

class Locate:
  """la classe principale"""

  def __init__(self):
    self.marg = None
    self.cdsrc = None
    self.head = None

  #-- __init__

#***********************************************************************
#                                                        -- usage
#***********************************************************************

  def usage(self, anglais): #-- pylint: disable-msg=R0201
    """mode d'emploi"""
    mod_name = "Locate.usage" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- usage.1
#-----------------------------------------------------------------------

    print  "Purpose: a tool for localisation of various patterns in Fortran 90 files of a given directory tree"
    print 
    print  "Uses: 2 arguments [or more]:"
    print  """
          [directory] -A variable:       affectation to a variable
          [directory] -C subroutine:     calls
          [directory] -D flag:           #if[n]def flag
          [directory] -F function:       definition of a FUNCTION
          [directory] -I subroutine:     genericity (INTERFACE)
          [directory] -K comment:        comment
          [directory] -L subroutine:     definition of a SUBROUTINE
          [directory] -M module:         definition of a module
          [directory] -N f90|py|sh|xml:  number of lines
          [directory] -T type:           use(s) of a type
          [directory] -U module:         use of a module
          [directory] -V var./expr.:     use(s) of a variable, more generaly an expression or instruction
          [directory] -W variable:       use(s) of a variable (in word mode)
          [directory] -Z dummy|utf:      finds non ISO and non UTF files

Regular expressions can be searched

Search keys can be catened with =directory, without any space, thus allowind search in another version of FluidBox

Quoting (with simple quotes) these regular expressions is advisable
"""
    print  "for example, \"" + __file__ + " -V 'allocate.*var%vp'\" gives all occurences of allocation/deallocation of Var%Vp"
    print  
    print  "Search is done without lowercase/uppercase distinction"
    print  
    print  "Version==" + SH_VERSION
    print  
    print  "Message==ERROR: " + anglais

#-----------------------------------------------------------------------
#                                                       -- usage.4
#-----------------------------------------------------------------------

  #-- usage

#***********************************************************************
#                                                       -- execute
#***********************************************************************

  def execute(self, cmd): #-- pylint: disable-msg=R0201
    """ex�cution d'une commande"""
    mod_name = "Locate.execute" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- execute.1
#-----------------------------------------------------------------------

    os.system(cmd)

  #-- execute

#***********************************************************************
#                                                       -- append
#***********************************************************************

  def append(self, list_out, list_in): #-- pylint: disable-msg=R0201
    """ajout � une liste"""
    mod_name = "Locate.append" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- append.1
#-----------------------------------------------------------------------

    for item in list_in:
      list_out.append(item)

  #-- append

#***********************************************************************
#                                                       -- __decoder_args
#***********************************************************************

  def __decoder_args(self): #--  pylint: disable-msg=R0201
    """decoder les arguments"""
    mod_name = "Locate.__decoder_args" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __decoder_args.1
#-----------------------------------------------------------------------

    argc = len(sys.argv) - 1

    if argc < 2:
      self.usage("Invalid number of argument(s) " + str(argc))

    ofs = 0

    if sys.argv[1][0] == '-':
      repertoire = RFB
    else:
      if argc < 3:
        self.usage("Invalid number of argument(s) " + str(argc))
        
      repertoire = sys.argv[1]
      ofs = 1

#-----------------------------------------------------------------------
#                                                        -- __decoder_args.2
#------------------------------------------------ d�codage des arguments

    selecteur = sys.argv[1+ofs]
    if selecteur[0] == "-":
      selecteur = selecteur[1:]

    if selecteur.find("=") == 1:
      repertoire = selecteur[2:]
      selecteur = selecteur[0]

    if not selecteur in ["A", "C", "D", "F", "I", "K", "L", "M", "N", "S", "T", "U", "V", "W", "Z"]:
      self.usage("invalid selector argument " + selecteur)

    ofs += 1

    methode = sys.argv[1+ofs:]
    self.marg = " ".join(methode)

#-----------------------------------------------------------------------
#                                                        -- __decoder_args.3
#----------------------------------------------- r�pertoire de recherche

    if not os.path.isdir(repertoire):
      self.usage(repertoire + " is not a directory")

    if repertoire.endswith(os.sep):
      pass
    elif repertoire.endswith(os.sep + os.sep):
      pass
    else:
      pass
    #repertoire = os.path.join(repertoire, "SRC")

    if not os.path.isdir(repertoire):
      self.usage(repertoire + " is not a directory/2")

    return repertoire, methode, selecteur

  #-- __decoder_args

#***********************************************************************
#                                                       -- __nombre_de_lignes
#***********************************************************************

  def __nombre_de_lignes(self, lang, repertoire, methode): #--  pylint: disable-msg=R0201
    """nombres de lignes"""
    mod_name = "Locate.__nombre_de_lignes" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                               -- __nombre_de_lignes.1
#-----------------------------------------------------------------------

    if methode[0] == "f90":

      cmd = self.cdsrc
      s = os.popen(cmd + "find . -type f -name \"*." + methode[0] +"\" -print | xargs wc -l | " + lang + " grep -v ."+ methode[0]+ " | sed -e 's/total//' || echo 0", "r").readlines()[0] #-- SRC/*.f90
      somme = int(s)

#-----------------------------------------------------------------------
#                                               -- __nombre_de_lignes.2
#-----------------------------------------------------------------------

    elif methode[0] in ["py", "sh", "xml"]:

      cmd = "cd " + os.path.join(repertoire, "..", "TOOLS") + ";"
      s = os.popen(cmd + "find . -type f -name \"*." + methode[0] +"\" -print | xargs wc -l | " + lang + " grep -v ."+ methode[0]+ " | sed -e 's/total//' || echo 0", "r").readlines()[0] #-- SRC/*.f90
      somme = int(s)

#-----------------------------------------------------------------------
#                                               -- __nombre_de_lignes.5
#-----------------------------------------------------------------------

    else:
      self.usage("invalid sub-selector argument " + methode[0])

    print >> sys.stdout
    print >> sys.stdout, "#lignes(" + methode[0] + ")=", somme

  #-- __nombre_de_lignes

#***********************************************************************
#                                                       -- __def_module
#***********************************************************************

  def __def_module(self): #--  pylint: disable-msg=R0201
    """recherche d'une d�finition de module"""
    mod_name = "Locate.__def_module" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __def_module.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"module  *" + self.marg + "\" || >/dev/null")
  #-- __def_module

#***********************************************************************
#                                                       -- __use_module
#***********************************************************************

  def __use_module(self): #--  pylint: disable-msg=R0201
    """recherche d'un emploi de module"""
    mod_name = "Locate.__use_module" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __use_module.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"^ *use  *" + self.marg + "\" || >/dev/null")
  #-- __use_module

#***********************************************************************
#                                                       -- __def_method
#***********************************************************************

  def __def_method(self): #--  pylint: disable-msg=R0201
    """recherche d'une d�finition de subroutine"""
    mod_name = "Locate.__def_method" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __def_method.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"subroutine  *" + self.marg + "[^a-z0-9_]\" || >/dev/null")
  #-- __def_method

#***********************************************************************
#                                                       -- __def_call
#***********************************************************************

  def __def_call(self): #--  pylint: disable-msg=R0201
    """recherche d'un appel de subroutine"""
    mod_name = "Locate.__def_call" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __def_call.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"call  *" + self.marg + "\" || >/dev/null")
  #-- __def_call

#***********************************************************************
#                                                       -- __sub_call
#***********************************************************************

  def __sub_call(self, repertoire): #-- R�cursive; pylint: disable-msg=R0201
    """recherche des appels d'UNE subroutine, avec nom des m�thodes appelantes"""
    mod_name = "Locate.__sub_call" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __sub_call.1
#-----------------------------------------------------------------------

    wanted = re.compile("^.*call  *" + self.marg + ".*$")

    for nom in os.listdir(repertoire):

      full = os.path.join(repertoire, nom)

      if os.path.isdir(full):

        self.__sub_call(full)

      elif nom.endswith(".f90"):

        inside = ""
        printed = False
        ln0 = 0
        ln1 = 0

#-----------------------------------------------------------------------
#                                                       -- __sub_call.2
#-----------------------------------------------------------------------

        for line in open(full, "r").readlines():

          ln1 += 1
          test = line.lower()
#          print >> sys.stderr, mod_name, test, wanted.match(test)

          if wanted.match(test):
            if not printed:
              printed = True
              print >> sys.stdout, full.replace(self.head, "") + ":" + str(ln0), inside,
            print >> sys.stdout, full.replace(self.head, "") + ":" + str(ln1), line,
          elif test.find("  subroutine ") >= 0:
            printed = False
            ln0 = ln1
            inside = line
          elif test.find("  function ") >= 0:
            printed = False
            ln0 = ln1
            inside = line
          elif test.find(" end function ") >= 0:
            printed = False
            ln0 = ln1
            inside = "?"
          elif test.find(" end subroutine ") >= 0:
            printed = False
            ln0 = ln1
            inside = "?"

  #-- __sub_call

#***********************************************************************
#                                                       -- __interface
#***********************************************************************

  def __interface(self): #--  pylint: disable-msg=R0201
    """recherche d'une interface"""
    mod_name = "Locate.__interface" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __interface.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"module *procedure.*" + self.marg + "\" || >/dev/null")

  #-- __interface

#***********************************************************************
#                                                       -- __def_function
#***********************************************************************

  def __def_function(self): #--  pylint: disable-msg=R0201
    """recherche d'une d�finition de fonction"""
    mod_name = "Locate.__def_function" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __def_function.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"function  *" + self.marg + "\" || >/dev/null")
  #-- __def_function

#***********************************************************************
#                                                       -- __emplois
#***********************************************************************

  def __emplois(self): #--  pylint: disable-msg=R0201
    """recherche des emplois d'une entit�"""
    mod_name = "Locate.__emplois" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __emplois.1
#-----------------------------------------------------------------------

    if self.marg.startswith("^"):
      self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins '" + self.marg + "' || >/dev/null")
    elif self.marg.find('"') >= 0:
      self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins '^ .*" + self.marg + "' || >/dev/null")
    else:
      self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"^ .*" + self.marg + "\" || >/dev/null")

  #-- __emplois

#***********************************************************************
#                                                       -- __w_emplois
#***********************************************************************

  def __w_emplois(self): #--  pylint: disable-msg=R0201
    """recherche des emplois d'une entit� en mode word"""
    mod_name = "Locate.__w_emplois" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __w_emplois.1
#-----------------------------------------------------------------------

    if self.marg.find('"') >= 0:
      self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -insw '^ .*" + self.marg + "' || >/dev/null")
    else:
      self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -insw \"^ .*" + self.marg + "\" || >/dev/null")

  #-- __w_emplois

#***********************************************************************
#                                                       -- __init
#***********************************************************************

  def __init(self): #--  pylint: disable-msg=R0201
    """recherche des initialisations d'une entit�"""
    mod_name = "Locate.__init" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __init.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"^ *" + self.marg + ".*=\" | grep -i \"  " + self.marg + "\" || >/dev/null")
    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"^ read.*" + self.marg + "\" | grep -i \"  " + self.marg + "\" || >/dev/null")

  #-- __init

#***********************************************************************
#                                                       -- __comment
#***********************************************************************

  def __comment(self, lang): #--  pylint: disable-msg=R0201
    """recherche d'un commentaire"""
    mod_name = "Locate.__comment" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __comment.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + lang + " find . -type f -name \"*.f90\" -print | xargs grep -ins \"!.*" + self.marg + "\" || >/dev/null")

  #-- __comment

#***********************************************************************
#                                                       -- __use_type
#***********************************************************************

  def __use_type(self): #--  pylint: disable-msg=R0201
    """recherche des emplois d'un type"""
    mod_name = "Locate.__use_type" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __use_type.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -ins \"^ *type *( *" + self.marg + " *)\" || >/dev/null")
  #-- __use_type

#***********************************************************************
#                                                       -- __ifdef
#***********************************************************************

  def __ifdef(self): #--  pylint: disable-msg=R0201
    """rechercher des if[n]def d'un drapeau"""
    mod_name = "Locate.__ifdef" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __ifdef.1
#-----------------------------------------------------------------------

    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -insw \"^#if\(n\)*def *" + self.marg + "\" || >/dev/null")
    self.execute(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs grep -insw \"^#elif *" + self.marg + "\" || >/dev/null")
  #-- __ifdef

#***********************************************************************
#                                                       -- __non_standard
#***********************************************************************

  def __non_standard(self, methode): #--  pylint: disable-msg=R0201
    """fichiers qui ne sont pas ISO ou ASCII"""
    mod_name = "Locate.__non_standard" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                       -- __non_standard.1
#-----------------------------------------------------------------------

    liste = []
    self.append(liste, os.popen(self.cdsrc + "find . -type f -name \"*.f90\" -print | xargs file -i || >/dev/null", "r").readlines())

    if methode[0].lower() == "utf":
      for line in liste:
        if line.lower().find("utf") >= 0:
          print >> sys.stdout, line,
    else:
      for line in liste:
        if line.find(" ASCII") >= 0 or line.find(" ISO") >= 0 or line.find("symbolic link") >= 0 \
        or line.find("Assembler source") >= 0 or line.find("Bennet Yee") >= 0 or line.find("cannot open") >= 0:
          pass
        else:
          print >> sys.stdout, line,


  #-- __non_standard

#***********************************************************************
#                                                       -- main
#***********************************************************************

  def main(self):
    """programme principal"""
    mod_name = "Locate.main" #-- pylint: disable-msg=W0612

#-----------------------------------------------------------------------
#                                                        -- main.1
#------------------------------------------------------- initialisations

    print >> sys.stderr, "<!>LOCATE-LOCATE-LOCATE-LOCATE-LOCATE-LOCATE-LOCATE-LOCATE-LOCATE-LOCATE-" + date + " " + " ".join(sys.argv)

#-----------------------------------------------------------------------
#                                                        -- main.2
#------------------------------------------------ d�codage des arguments

    repertoire, methode, selecteur = self.__decoder_args() #--  d�coder les arguments

#-----------------------------------------------------------------------
#                                                        -- main.3
#----------------------------------------------------- initialisations/2

    print >> sys.stdout #-- une ligne de mise en page des r�sultats

    self.head = repertoire
    self.cdsrc = "cd " + repertoire + ";"

    lang = "LC_ALL=ISO-8859-1"

#-----------------------------------------------------------------------
#                                                        -- main.4
#--------------------------------------------------- nombre(s) de lignes

    if selecteur == "N":

      self.__nombre_de_lignes(lang, repertoire, methode) #--  nombres de lignes

#-----------------------------------------------------------------------
#                                                        -- main.5
#---------------------------------- recherche d'une d�finition de module

    elif selecteur == "M":

      self.__def_module() #--  recherche d'une d�finition de module

#-----------------------------------------------------------------------
#                                                        -- main.6
#--------------------------------------- recherche d'un emploi de module

    elif selecteur == "U":

      self.__use_module() #--  recherche d'un emploi de module

#-----------------------------------------------------------------------
#                                                        -- main.7
#------------------------------ recherche d'une d�finition de subroutine

    elif selecteur == "L":

      self.__def_method() #--  recherche d'une d�finition de subroutine

#-----------------------------------------------------------------------
#                                                        -- main.8
#------------------------------------ recherche d'un appel de subroutine

    elif selecteur == "C":

      self.__def_call() #--  recherche d'un appel de subroutine

#-----------------------------------------------------------------------
#                                                        -- main.9
#----- recherche des appels d'une m�thode, avec nom des m�thodes d'appel

    elif selecteur == "S":

      self.__sub_call(repertoire)

#-----------------------------------------------------------------------
#                                                        -- main.10
#--------------------------------------------- recherche d'une interface

    elif selecteur == "I":

      self.__interface() #--  recherche d'une interface

#-----------------------------------------------------------------------
#                                                        -- main.11
#-------------------------------- recherche d'une d�finition de fonction

    elif selecteur == "F":

      self.__def_function() #--  recherche d'une d�finition de fonction

#-----------------------------------------------------------------------
#                                                        -- main.12
#------------------------------------ recherche des emplois d'une entit�

    elif selecteur == "V":

      self.__emplois() #--  recherche des emplois d'une entit�

#-----------------------------------------------------------------------
#                                                        -- main.13
#----------------------- recherche des emplois d'une entit� en mode word

    elif selecteur == "W":

      self.__w_emplois() #--  recherche des emplois d'une entit� en mode word

#-----------------------------------------------------------------------
#                                                        -- main.14
#---------------------------- recherche des initialisations d'une entit�

    elif selecteur == "A":

      self.__init() #--  recherche des initialisations d'une entit�

#-----------------------------------------------------------------------
#                                                        -- main.15
#------- recherche d'un commentaire (ON SUPPOSE LES FICHIERS ISO ou UTF)

    elif selecteur == "K":

      self.__comment(lang) #--  recherche d'un commentaire

#-----------------------------------------------------------------------
#                                                        -- main.16
#--------------------------------------- recherche des emplois d'un type

    elif selecteur == "T":

      self.__use_type() #--  recherche des emplois d'un type

#-----------------------------------------------------------------------
#                                                        -- main.17
#--------------------------------------------- recherche d'un ifdef flag

    elif selecteur == "D":

      self.__ifdef() #--  rechercher des if[n]def d'un drapeau

#-----------------------------------------------------------------------
#                                                        -- main.18
#--------------------------------- fichiers qui ne sont pas ISO ou ASCII

    elif selecteur == "Z":

      self.__non_standard(methode) #--  fichiers qui ne sont pas ISO ou ASCII

#-----------------------------------------------------------------------
#                                                        -- main.19
#------------------------------------------------------------------- NIP

    else:

      self.usage("NIP " + selecteur)

#-- main

#***********************************************************************
#                                                       -- appel direct
#***********************************************************************

if __name__ == "__main__":
  zzzz = Locate()
  zzzz.main()

