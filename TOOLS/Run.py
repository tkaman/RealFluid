#!/usr/bin/python
import os, sys, subprocess
import Construire

# Try to rebuild a version of FluidBox
def main(args, nbproc, exename_like, batchmode):
  import check_args
  argc = len(args)
  
  # Check if the number of arguments is valid
  if argc < 1:
    print "Error : Can't parse the command line"
    print "  You have to specify a program name"
    return
  
  exename = args[0]
  testname = " ".join( args[1:] )
  # Get all the options possible
  total = check_args.Total()
  # generate the list of all the different model we can generate with this version of FluidBox
  commands, unknown = check_args.genCommands(total, display=False)
  
  if exename_like == "":
    exe = exename
  else:
    exe = exename_like

  # Try to load a file in order to run faster
  try:
    import lastrun
    commands = [lastrun.program] + commands
  except:
    pass

  # If the name correspond with the name specified on the command line we generate the exe
  for program in commands:
    try:
      fbxname, envir = Construire.choix_modele(program, nodisplay = True, computeNameOnly = True)
      if fbxname == exe:
        # Get the path of this file
        thisfile = os.path.abspath( __file__ )
        thisrep =  os.path.split(thisfile)[0]
        exepath = os.path.abspath( os.path.join(thisrep, "../BIN/%s" % exename) )    

        # save a file in order to run faster next time
        try:
          f = open(os.path.join(thisrep,"lastrun.py"), "w")
          f.write("program=%s" % repr(program))
          f.close()
        except:
          print "Error writing lastrun"

        # Command to run if the Makefile exists and if we dont want to build from scratch
        if nbproc > 1:
          runexe = "mpd --daemon && mpirun -n %d %s" % (nbproc, exepath)
        else:
          runexe = exepath
        if batchmode:
          runexe = "mpiexec %s" % exepath

        command = "%s %s %s" % ( envir, runexe, testname)
        print "Launching : %s" % command
        try:
          subprocess.Popen("bash -c '%s'" % command, shell=True).wait()
        except:
          print "Problem running : %s" % command
        finally:
          return
    except:
      pass
    # Fail : the exename specified can not be generated
  print "%s : Not a valid name" % exe


if __name__ == "__main__":
  # Set the path to include all the python files we need
  Construire.expandPath()

  from optparse import OptionParser
  # Defining the options of the program
  usage="""usage: %prog [Options] exename args

  exename : name of the executable to launch
  args    : the arg
    type -h or --help to show the valid options"""
  # Creating the parser (http://docs.python.org/library/optparse.html) 
  parser = OptionParser(usage=usage)
  # Adding the options
  parser.add_option("-n", "--nbproc", dest="nbproc",
                    help="Set the number of processors",
                    metavar="NBPROC", type=int, default=1)
  parser.add_option("-l", "--like",   dest="exename",
                    help="Source the environnement as if we wanted to run EXENAME",
                    metavar="EXENAME", type=str, default="")
  parser.add_option("-b", "--batch-mode", dest="batch",
                    help="Launch in batch mode : ie with mpiexec",
                    action="store_true", default=False)
  # Parse the command line
  (options, args) = parser.parse_args()
  main(args, options.nbproc, options.exename, options.batch)
  
