#!/usr/bin/python
import sys, os, re, subprocess

max_try = 4

def display_list(title, liste, nb_try = 0, values = None, nodisplay = False):
  """Display a list of options"""
  if nb_try >= max_try:
    raise(ValueError("Too many failures\nExiting"))
  if values == None or values == []:
    if nodisplay:
      raise(ValueError("Nothing was specified on command line, and no input is allowed"))
    print(title)
    nb = 1
    for elt in liste:
      print( "%d : %s" % (nb, elt) )
      nb += 1
  
    choice = raw_input()
    ichoice = -1
    try:
      ichoice = int(choice) - 1
    except:
      print( "Type a number" )
      return display_list(title, liste, nb_try + 1)
  
    if ichoice >= 0 and ichoice < len(liste):
      return liste[ichoice]
    else:
      print("Invalid range : try again")
      return display_list(title, liste, nb_try + 1)
  else:
    for val in values:
      for elt in liste:
        try:
          if elt.name == val:
            if not nodisplay:
              print(title)
              print( "Choice [%s] : %s" % ( val, elt ) )
            return elt
        except:
          if elt == val:
            if not nodisplay:
              print(title)
              print( "Choice [%s] : %s" % ( val, elt ) )
            return elt
           
    if nodisplay:
      raise(ValueError("Nothing on command line is matching what we expected, and no input is allowed") )
    # If we are here we found no matches on the command line: We ask the user
    return display_list(title, liste)


def display_dict(title, liste, nb_try = 0, values = None, nodisplay = False):
  """Display a dictionnary of options (key: value)"""
  if nb_try >= max_try:
    raise(ValueError("Too many failures\nExiting"))
    
  if values == None or values == []:
    if nodisplay:
      raise(ValueError("Nothing was specified on command line, and no input is allowed"))
    print(title)
    for elt in liste:
      print( "%s : %s" % (elt[0], elt[1]) )
  
    choice = raw_input()
    if choice in [elt[0] for elt in liste]:
      for elt in liste:
        if elt[0] == choice:
          return elt[1]
    else:
      print("Wrong value : try again")
      return display_dict(title, liste, nb_try + 1)
  else:
    for val in values:
      for elt in [elem[1] for elem in liste]:
        if elt.name == val:
          if not nodisplay:
            print(title)
            print( "Choice [%s] : %s" % ( val, elt ) )
          return elt
    if nodisplay:
      raise(ValueError("Nothing on command line is matching what we expected, and no input is allowed") )
    # If we are here we found no matches on the command line: We ask the user
    return display_dict(title, liste)
    
    


def choix_modele(command, noreg = False, rep = "" , nbprocs = 1, nodisplay = False, computeNameOnly = False):
  """This function parses the command line or asks the user 
to enter values in order to generate a version of FluidBox

If noreg is set to True the function only generate the command line
to generate the program
"""
  import compilers_options
  import models
  import mode
  #  get infos for the program directory, model, macros, environement, ...
  mod = display_dict("Choose your model", models.data, values = command , nodisplay = nodisplay)
  # here we just want the compiler name (str gives us that)
  compilo = str( display_list("Choose your compiler", [compiler.compilo for compiler in compilers_options.data] , values = command, nodisplay = nodisplay) )

  commands = [nbprocs,mod.name,compilo]
  # Not asking the question if we can execute only in sequential mode
  if not mod.seqonly:
    envir = display_list("Choose your environnement", mode.data, values = command, nodisplay = nodisplay)
    commands.append(envir.name)
  else:
    envir = mode.data[0]

  for compiler in compilers_options.data:
    if compiler.compilo == compilo:
      opts_compilo = compiler
      opts_compilo.display_list("Choose your compiler's options", values = command, nodisplay = nodisplay)
      opts = "%s %s" %(opts_compilo.opts, opts_compilo.always)
      commands.append(opts_compilo.name)

  # Check if we build FluidBox or an extra tool
  if mod.directory == "":
    if not nodisplay:
      print "Not compiling FBx : Bulding a tool instead"
    if mod.exename[:6] == "interf":
      # Special treat for interf (with or without openmp? : not maintained)
      fbxmodel = "INTERF"
    elif mod.exename[:9] == "save2Visu":
      fbxmodel = "VISU"
    elif mod.exename[:5] == "wextr":
      fbxmodel = "EXTR"
    else:
      fbxmodel = "TOOL"      
  else:
    fbxmodel = "FBX"

  import check_args
  dico = check_args.sort_args(command)
  machinename = None
  if len(dico["Machine"])>0:
    machinename = dico["Machine"][0]
    commands.append(machinename)

  import environment
  prepenvir = environment.setenvir(fbxmodel, compilo, envir.name, machine = machinename, display = not nodisplay)

  # Command to load the environment 
  if prepenvir != "":
    prepenvir = "%s && " % prepenvir


  if "--verbose" in command:
    print("--------Recap--------")
    print compilo
    print mod.directory
    print mod.main
    print mod.macros
    print envir.values()
    print opts
    print opts_compilo.suffix
    print opts_compilo.prefix
    print("--------Recap--------")

  # Compute the executable name
  fbxname = "%s%s%s%s" % (envir.prefix, opts_compilo.prefix, mod.exename, opts_compilo.suffix)
  if computeNameOnly:
    return fbxname, prepenvir
  # workaround to pass '=' in the argument list (gfortran option)
  opts = re.sub('=',"@@@",opts)

  # Get the path of this file
  thisfile = os.path.abspath( __file__ )
  thisrep =  os.path.split(thisfile)[0]

  # if rep is not given we build the program in the FBx/OBJECTS directory
  if rep == "":
    rep = "%s/.." % thisrep

  rep = "%s/OBJECTS/O%s" % (rep,fbxname)
  # Generate or clean the directory
  try:
    os.makedirs(rep)
  except:
    if not nodisplay:
      print "Directory exists"
    subprocess.Popen('rm -fr %s/*' % rep, shell=True).wait()

  if mod.needscotch:
    # If scotch is needed we set the variable scotch to the directory containing
    # FindSCOTCH.cmake (for now it is ${TFB}/UTILS )
    with_scotch= "-DSCOTCH=%s" % os.path.join(thisrep,"UTILS")
  else:
    with_scotch=""
  
  # BEGIN dante
  # Set the variable petsc to the directory containing
  # FindPETSC.cmake (for now it is ${TFB}/UTILS )
  # NOTA: FindPackageMultipass.cmake and 
  #       ResolveCompilerPaths.cmake must be present in petsc
  if mod.needpetsc:
     with_petsc= "-DPETSC=%s" % os.path.join(thisrep,"UTILS")
  else:
	  with_petsc= ""
  # END dante
  
  rfb = os.getenv("RFB")

  # Create the file svnversion.f90
  import getsvn
  getsvn.makeVersionModule(rfb, rep)
  
  # Create the commandline to compile FBx through cmake
  # added with_petsc !!!
  commandline = 'bash -c \'%scd "%s" && cmake "%s" -DCOMPILO="%s" -DMODEL="%s" -DFortran_FLAGS="%s" -DFBXFLAGS="%s" -DMAIN="%s" -DFORTRAN_PREPROCESS_FLAG="%s" -DFBXMODEL="%s" -DFBXNAME="%s" -DENVIR="%s" -DENVIRFILES="%s" %s %s && make clean && make -j%d\'' % (prepenvir, rep, rfb, compilo, mod.directory, opts, mod.macros, mod.main, opts_compilo.preprocess, fbxmodel, fbxname, envir.name, envir.addfiles[0], with_scotch, with_petsc, nbprocs)
  # only 1 file can be put in addfiles

  if not noreg:
    print "***************************************************************"
    print "fin2"
    print "fbxname", fbxname
    print "commandline", commandline
    print "prepenvir", prepenvir
    # Pass informations to cmake (not really pretty)
    subprocess.Popen(commandline, shell=True).wait()
  else:
    print "***************************************************************"
    print "***************************************************************"
    print "fin"
    print "fbxname", fbxname
    print "commandline", commandline
    print "prepenvir", prepenvir
    return fbxname, commandline, prepenvir

  

def main(argv): 
  print( "Compiling sources in $RFB=%s" % os.getenv("RFB") )
  try:
    args = argv[1:]
  except:
    args = []

  try:
    nbproc = int(args[0])
    args = args[1:]
  except:
    nbproc = 1

  choix_modele(args, nbprocs = nbproc)
    

# Add UTILS and CONFIG in the path
def expandPath():
  thisfile = os.path.abspath( __file__ )
  thisrep  = os.path.split(thisfile)[0]
  utilrep  = os.path.join(thisrep, "UTILS")
  sys.path.append(utilrep)
  confrep  = os.path.join(thisrep, "CONFIG")
  sys.path.append(confrep)
  noregrep = os.path.join(thisrep, "REGRESSION")
  sys.path.append(noregrep)


if __name__ == "__main__":
  expandPath()
  main(sys.argv)
