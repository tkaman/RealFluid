from models import model

#Defining ordre_eleve_petsc
ordre_eleve_petsc = model("ordre_eleve_petsc")
ordre_eleve_petsc.description = "High Order Steady Residual Distribution Model + PETSc"
ordre_eleve_petsc.main        = "Main.f90"
ordre_eleve_petsc.directory   = "OrdreEleve"
ordre_eleve_petsc.exename     = "FBxOE"
ordre_eleve_petsc.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE -DPETSC"
ordre_eleve_petsc.needpetsc   = True


#Defining ordre_eleve_ns
ordre_eleve_ns = model("ordre_eleve_ns")
ordre_eleve_ns.description = "High Order Steady Residual Distribution Model for viscous flows"
ordre_eleve_ns.main        = "Main.f90"
ordre_eleve_ns.directory   = "OrdreEleve"
ordre_eleve_ns.exename     = "FBxOENS"
ordre_eleve_ns.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE -DNAVIER_STOKES "
ordre_eleve_ns.needpetsc   = False

#Defining ordre_eleve_muliphase
ordre_eleve_multiphase = model("ordre_eleve_multiphase")
ordre_eleve_multiphase.description = "High Order Residual Distribution Model for multiphase flows (5eqs)"
ordre_eleve_multiphase.main        = "Main.f90"
ordre_eleve_multiphase.directory   = "Multiphase"
ordre_eleve_multiphase.exename     = "FBxMphase"
ordre_eleve_multiphase.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE  "
ordre_eleve_multiphase.needpetsc   = False


#Defining extract_wall
extract_wall = model("extract_wall")
extract_wall.description = "Extract wall points from a mesh file"
extract_wall.main        = "extract_wall.f90"
extract_wall.directory   = ""
extract_wall.exename     = "wextr"
extract_wall.macros      = "-DRESIDUAL -DORDRE_ELEVE -DNAVIER_STOKES "
extract_wall.needpetsc   = False

#Defining ordre_eleve_ns
man_sol = model("man_sol")
man_sol.description = "High Order Steady Residual Distribution Model for viscous manufactured solution"
man_sol.main        = "Main.f90"
man_sol.directory   = "Man_sol"
man_sol.exename     = "FBxOEX"
man_sol.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE -DNAVIER_STOKES"
man_sol.needpetsc   = False

#Defining ordre_eleve_ns
supg = model("supg")
supg.description = "High Order Steady SUPG method"
supg.main        = "Main.f90"
supg.directory   = "Supg"
supg.exename     = "FBxOEG"
supg.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE -DNAVIER_STOKES"
supg.needpetsc   = False

#Defining ordre_eleve_ns
old_ordre_eleve_ns = model("old_ordre_eleve_ns")
old_ordre_eleve_ns.description = "High Order Steady RD stable viscous"
old_ordre_eleve_ns.main        = "Main.f90"
old_ordre_eleve_ns.directory   = "old_OrdreEleve"
old_ordre_eleve_ns.exename     = "FBxOEO"
old_ordre_eleve_ns.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE -DNAVIER_STOKES "
old_ordre_eleve_ns.needpetsc   = False

#Defining ordre_eleve
old_ordre_eleve = model("old_ordre_eleve")
old_ordre_eleve.description = "High Order Steady RD stable viscous"
old_ordre_eleve.main        = "Main.f90"
old_ordre_eleve.directory   = "old_OrdreEleve"
old_ordre_eleve.exename     = "FBxOEOe"
old_ordre_eleve.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE"
old_ordre_eleve.needpetsc   = False


data = [
["M", ordre_eleve_multiphase],
["N", ordre_eleve_petsc],
["4", ordre_eleve_ns],
["D", extract_wall],
["X", man_sol],
["S", supg],
["O", old_ordre_eleve_ns],
["E", old_ordre_eleve],
]
