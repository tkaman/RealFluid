#Class containing model definitions
class model:
  def __init__(self, name = ""):
    self.addfiles    = []
    self.name        = name
    self.description = name
    self.exename     = name
    self.directory   = ""
    self.main        = ""
    self.macros      = ""
    self.needpetsc   = False
    # (for pre or post treatment)
    self.seqonly     = False
    self.needscotch  = False

  def __repr__(self):
    return "%s  ( %s )" % ( self.description , self.name)

  def values(self):
    return "%s %s %s %s" % (self.name, self.directory, self.main, self.macros)



#Defining rdstat
rdstat = model("rdstat")
rdstat.description = "Remi Abgrall's model, steady"
rdstat.main        = "Main.f90"
rdstat.directory   = "EulNSRD"
rdstat.exename     = "FBxRDsNS"
rdstat.macros      = "-DPERFECTGAS -DRESIDUAL"

#Defining rdinstat
rdinstat = model("rdinstat")
rdinstat.description = "Remi Abgrall's model, unsteady"
rdinstat.main        = "Main.f90"
rdinstat.directory   = "EulNSRD"
rdinstat.exename     = "FBxRDiNS"
rdinstat.macros      = "-DPERFECTGAS -DRESIDUAL -DINSTAT_RD"

#Defining ordre_eleve
ordre_eleve = model("ordre_eleve")
ordre_eleve.description = "High Order Steady Residual Distribution Model"
ordre_eleve.main        = "Main.f90"
ordre_eleve.directory   = "OrdreEleve"
ordre_eleve.exename     = "FBxOE"
ordre_eleve.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE"

#Defining ordre_eleve_p2p2
ordre_eleve_p2p2 = model("ordre_eleve_p2p2")
ordre_eleve_p2p2.description = "High Order Steady Residual Distribution Model - P2P2"
ordre_eleve_p2p2.main        = "Main.f90"
ordre_eleve_p2p2.directory   = "OrdreEleve"
ordre_eleve_p2p2.exename     = "FBxOE_P2P2"
ordre_eleve_p2p2.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE -DP2P2"

#Defining residus
residus = model("residus")
residus.description = "Euler/Navier-Stokes RDS Steady (Merge)"
residus.main        = "Main.f90"
residus.directory   = "Residus"
residus.exename     = "FBxResidus"
residus.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE"

#Defining residus_instat
residus_instat = model("residus_instat")
residus_instat.description = "Euler/Navier-Stokes RDS Unsteady (Merge)"
residus_instat.main        = "Main.f90"
residus_instat.directory   = "Residus"
residus_instat.exename     = "FBxResidusi"
residus_instat.macros      = "-DPERFECTGAS -DRESIDUAL -DINSTAT_RD -DORDRE_ELEVE"

#Defining interf
interf = model("interf")
interf.description = "Interf (grid partitioner for parallel executions)"
interf.main        = "Interf.f90"
interf.exename     = "interf"
interf.macros      = "-DORDRE_ELEVE"
interf.seqonly     = True
interf.needscotch  = True

#Defining interf p2p2
interfp2p2 = model("interfp2p2")
interfp2p2.description = "Interf (grid partitioner for parallel executions) P2P2"
interfp2p2.main        = "Interf.f90"
interfp2p2.exename     = "interfp2p2"
interfp2p2.macros      = "-DORDRE_ELEVE -DP2P2"
interfp2p2.seqonly     = True
interfp2p2.needscotch  = True

#Defining save2visu
save2visu = model("save2visu")
save2visu.description = "Save2visu (format translator for visualization)"
save2visu.main        = "Save2Visu.f90"
save2visu.exename     = "save2Visu"
save2visu.macros      = "-DMOULINETTE -DORDRE_ELEVE"
save2visu.seqonly     = True


# This is the list of the models in the trunk
data=[
["1",    rdstat],
["2",    rdinstat],
["3",    ordre_eleve],
#["4",    residus],
#["5",    residus_instat],
#["P2P2", ordre_eleve_p2p2],

["A",    interf],
["AP2",  interfp2p2],
["B",    save2visu]
]


# You can add your own models in the file mymodels.py
try:
  import mymodels
  newdata = mymodels.data
except ImportError:
  newdata = []
except:
  print "Error in the file mymodels.py"
  raise

# If you redefine a model we show a warning and suppress the old definition
for keyname in [key[1].name for key in newdata]:
  if keyname in [key[1].name for key in data]:
    print "Warning mymodels.py overwrite the definition of the model : %s" %  keyname
    for key in data:
      if key[1].name == keyname:
        data.remove(key)

# If there is a conflict with the key we raise an error
for keyname in [key[0] for key in newdata]:
  if keyname in [key[0] for key in data]:
    raise( KeyError("Conflict in model keys between your local model and basic models : %s" %  keyname))

data += newdata
data.sort()

#####################################################
## Here is an example of a file mymodels.py        ##
## The first line import the definition of "model" ##
## The data list contains your own models          ##
#####################################################
#from models import model
#
##Defining ordre_eleve_nurbs
#ordre_eleve_nurbs = model("ordre_eleve_nurbs")
#ordre_eleve_nurbs.description = "High Order Steady Residual Distribution Model + NURBS"
#ordre_eleve_nurbs.main        = "Main.f90"
#ordre_eleve_nurbs.directory   = "OrdreEleve"
#ordre_eleve_nurbs.exename     = "FBxOEn"
#ordre_eleve_nurbs.macros      = "-DPERFECTGAS -DRESIDUAL -DORDRE_ELEVE -DNURBS"
#
#data = [
#["N", ordre_eleve_nurbs]
#]
