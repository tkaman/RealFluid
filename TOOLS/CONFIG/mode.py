class mode:
  def __init__(self, name = ""):
    self.addfiles    = []
    self.prefix      = ""
    self.name        = name
    self.description = name

  def __repr__(self):
    return ("%s  ( %s )" % ( self.description, self.name) )

  def values(self):
    return "%s %s" % (self.name, repr(self.addfiles))


#Defining sequential mode
seq             = mode("seq")
seq.description = "Sequential"
seq.addfiles    = ["COMM/SeqMPPComm.f90"]

#Defining mpi mode
mpi             = mode("mpi")
mpi.description = "MPI"
mpi.addfiles    = ["COMM/MpiMPPComm.f90"]
mpi.prefix      = "m"




#Defining murge mode with hips
murgeh             = mode("hips")
murgeh.description = "MURGE"
murgeh.addfiles    = ["COMM/MpiMPPComm.f90"]
murgeh.prefix      = "h"

#Defining murge mode with pastix
murgep             = mode("pastix")
murgep.description = "MURGE"
murgep.addfiles    = ["COMM/MpiMPPComm.f90"]
murgep.prefix      = "p"

# We group all the definitions in a list
data = [
    seq,
    mpi
]
#,
#    murgeh,
#    murgep
#]

