###############################################################
#  Define the class containing all compilers options
###############################################################
class options:
  def __init__(self, name = ""):
    self.compilo     = name
    self.max_try     = 4
    self.prefix      = ""
    self.always      = ""
    self.all_opts    = []
    self.preprocess  = ""
    self.suffix      = ""
    self.opts        = ""
    self.name        = ""

  def values(self):
    return "%s %s" % (self.name, repr(self.addfiles))

  def display_list(self, title, nb_try = 0, values = None, nodisplay = False):
      if nb_try >= self.max_try:
          raise(ValueError("Too many failures\nExiting"))
      if values == None or values == []:
        if nodisplay:
          raise(ValueError("Nothing was specified on command line, and no input is allowed"))
        print(title)
        for elt in self.all_opts:
          expl = explain[ elt[0] ]
          print( "%s : %s  ( %s )" % (expl[0], expl[1], elt[0]))
  
        choice = raw_input()
        for elt in self.all_opts:
          expl = explain[ elt[0] ]
          if choice == expl[0]:
            self.suffix = expl[2] 
            self.opts = elt[1]
            self.name = elt[0]
            return self
  
        print("Wrong value : try again")
        return self.display_list(title, nb_try + 1)
      else:
        for val in values:
          for elt in self.all_opts:
            if elt[0] == val:
              expl = explain[ elt[0] ]
              if not nodisplay:
                print(title)
                print( "Choice [%s] : %s" % ( val, expl[1]) )
              self.suffix = expl[2] 
              self.opts = elt[1]
              self.name = val
              return self
        if nodisplay:
          raise(ValueError("Nothing on command line is matching what we expected, and no input is allowed") )

        # If we are here we found no matches on the command line: We ask the user
        return self.display_list(title)
        

      
###############################################################
# Defining compilers options
###############################################################

#Defining ifort properties
ifort = options("ifort")
ifort.always = "-r8 -g -prec-div -prec-sqrt -pc80 -free -implicitnone -traceback -I/usr/local/include -warn all"
ifort.openmp = "-openmp"
ifort.preprocess = "-fpp"
ifort.all_opts = [ 
    ("default"      ,""),       
    ("opt_moderee"  ,"-O3"),
    ("profil"       ,"-pg -O5"),
    ("bornes"       ,"-C"),
    ("nan"          ,"-fpe0 -check uninit -ftrapuv"),
    ("bornes+nan"   ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created"),
    ("opt_maximale" ,"-O5 "),
    ("fast"         ,"-fast"),
    ("sans_opt"     ,"-O0"),
    ("indef"        ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created -pg")
    ]

#Defining ftn properties (Pitz Dora @CSCS
import os
ftn = options("ftn")
if os.environ['PE_ENV']=="INTEL":
    ftn.always = "-r8 -g -prec-div -prec-sqrt -pc80 -free -implicitnone -traceback -I/usr/local/include -warn all -warn nounused -nofor_reentrancy"
    ftn.openmp = ""
    ftn.preprocess = "-fpp"
    ftn.all_opts = [ 
        ("default"      ,"-qno-openmp"),       
        ("opt_moderee"  ,"-O3"),
        ("profil"       ,"-pg -O5"),
        ("bornes"       ,"-C"),
        ("nan"          ,"-fpe0 -check uninit -ftrapuv"),
        ("bornes+nan"   ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created"),
        ("opt_maximale" ,"-O5 "),
        ("fast"         ,"-fast"),
        ("sans_opt"     ,"-O0"),
        ("indef"        ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created -pg"),
        ("omp"          ,"-qopenmp") 
        ]
elif os.environ['PE_ENV']=="CRAY":
       ftn.always = "-s default64 -prec-div -prec-sqrt -eI -e0"
       ftn.openmp = ""
       ftn.preprocess = "-eF"
       ftn.all_opts = [ 
           ("default"      ,"-craype-verbose"),       
           ("opt_moderee"  ,"-O1"),
           ("profil"       ,"-pg -O2"),
           ("bornes"       ,"-C"),
           ("nan"          ,"-fpe0 -check uninit -ftrapuv"),
           ("bornes+nan"   ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created"),
           ("opt_maximale" ,""),
           ("fast"         ,"-ipo -no-prec-div"),
           ("sans_opt"     ,"-O0"), 
           ("indef"        ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created -pg"),
           ("acc"          ,"-hlist=a"),       
           ("omp"          ,"-hlist=a") #hautothread
       ]   
elif os.environ['PE_ENV']=="PGI":
       ftn.always = "-r8 -g -pc 80 -Mfpapprox=div -Mfpapprox=sqrt -Mdclchk"
       ftn.openmp = ""
       ftn.preprocess = "-Mpreprocess"
       ftn.all_opts = [ 
           ("default"      ,"-Minfo=all"),       
           ("opt_moderee"  ,"-O1"),
           ("profil"       ,"-pg -O2"),
           ("bornes"       ,"-C"),
           ("nan"          ,"-fpe0 -check uninit -ftrapuv"),
           ("bornes+nan"   ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created"),
           ("opt_maximale" ,""),
           ("fast"         ,"-fast"),
           ("sans_opt"     ,"-O0"), 
           ("indef"        ,"-C -check uninit -ftrapuv -fpe0 -check noarg_temp_created -pg"),
           ("acc"          ,"-acc -fast -Minfo=accel -ta=tesla:cc35 -Mallocatable=95 -Mnoidiom"),
           ("omp"          ,"-mp -Minfo=loop"), #Mconcur automatic parallelization
           ("multicore"    ,"-acc -fast -Minfo=accel -ta=multicore -Mallocatable=95 -Mnoidiom") 
           ]


#Defining gfortran properties
gfortran = options("gfortran")
gfortran.prefix = "g"
gfortran.always = "-fdefault-real-8 -fdefault-double-8 -g -ffree-line-length-none -ffree-form -fimplicit-none -fbacktrace -I/usr/include -I/usr/local/include -Wall"
gfortran.openmp = "-fopenmp"
gfortran.preprocess = "-x f95-cpp-input"
gfortran.all_opts = [
    ("default"      ,""),
    ("opt_moderee"  ,"-O2"),
    ("profil"       ,"-pg -O3"),
    ("bornes"       ,"-fbounds-check"),
    ("nan"          ,"-ffpe-trap=invalid,zero,overflow -Wuninitialized -O"),
    ("bornes+nan"   ,"-fbounds-check -ffpe-trap=invalid,zero,overflow -Wuninitialized -O"),
    ("opt_maximale" ,"-O3"),
    ("sans_opt"     ,"-O0"),
    ("indef"        ,"-fbounds-check -ffpe-trap=invalid,zero,overflow -Wuninitialized -O -pg")
    ]

data=[
ifort,
gfortran,
ftn
]








###############################################################
# Data displaying the options, and suffixe to executables name
###############################################################
explain={
#number, description, suffix
"default":        ["-",  "default optimisation",   ""],
"opt_moderee":    ["0",  "moderate optimisation",  "m"],
"profil":         ["1",  "time profiling",        "t"],
"bornes":         ["2",  "array bound checking",  "b"],
"nan":            ["3",  "NaN forbidden",         "n"],
"bornes+nan":     ["4",  "array bound checking + NaN forbidden",    "s"],
"opt_maximale":   ["5",  "HEAVY OPTIMISATION (except for IBM)",     "r"],
"sans_opt":       ["6",  "no optimisation",                         "z"],
"indef":          ["7",  "pre-initialisation of variables with `undefined' values","i"],
"fast":           ["8",  "-fast when possible for compiler/system", "f"],
"acc":            ["9",  "OpenACC extension","a"],
"omp":            ["10", "OpenMP extension","o"],
"multicore":      ["11", "multicore OpenACC extension", "mc"]
}
