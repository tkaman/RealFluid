#!/usr/bin/python
import urllib, urllib2
import tarfile, os

databasePath = "http://vulcain.local/fluidbox"

# Send the compilation recap to the regression database
def submitCompil(filename):
  submitFile(filename, "%s/send_compil.php" % databasePath)

# Send the execution recap of a test case to the regression database
def submitExec(filename):
  submitFile(filename, "%s/send_exec.php" % databasePath)

# Open a file, concatenate the lines in a string and send the string to an url
def submitFile(filename, url):
  try:
    txt = "".join( filename.readlines() )
    filename.close()
    data   = urllib.urlencode( {"file":txt} ) 
    submit = urllib2.urlopen(url, data)
    l = submit.readlines()
    if l != []:
      print l
    submit.close()
  except:
    print "Error in submiting file '%s' to the url '%s'" % (filename, url)


# Get the informations of regression for test id regid
def getRegression(regid):
  try:
    submit = urllib2.urlopen( "%s/regression.php?id=%s" % (databasePath, regid))
    txt = "".join(submit.readlines())
    submit.close()
    return txt
  except:
    print "Error in getting regression %d" % regid
    return ""


def parentFolder(path):
  return os.path.split( os.path.split(path)[0])[1]


def submitTarFile(filepath):
  try:
    tar = tarfile.open( os.path.join(filepath, "recap.tar.bz2"), "r:bz2")
    for tarinfo in tar:
      f = tar.extractfile(tarinfo)
      if parentFolder(tarinfo.name) == os.path.split(filepath)[1]:
        submitCompil(f)
      else:
        submitExec(f)
    tar.close()
  except:
    print "Error in submiting tar file : %s" % os.path.join(filepath, "recap.tar.bz2")

def createTarFile(noregdir):
  try:
    tar = tarfile.open( os.path.join(noregdir, "recap.tar.bz2") , "w:bz2")
    if os.path.exists( os.path.join(noregdir,"recap_log") ):
      tar.add( os.path.join(noregdir, "recap_log") )  
    # List the regression directory
    try:
      dirs = os.listdir( os.path.join(noregdir, "TESTS") )
    except:
      dirs = []
    for directory in dirs:
      # launch the parser if the file named "recap_log" exists in this directory
      filename = os.path.join( noregdir, "TESTS", directory, "recap_log")
      if os.path.exists( filename ):
        tar.add(filename)
    tar.close()
  except:
    print "Error in saving tar file for directory: %s" % noregdir
