#!/usr/bin/python
import os, sys, subprocess
import threading, time
import shutil, re
import check_args

freeproc = 0
problems = 0

lock = threading.RLock()
# Colors
txtred='\033[91m' # Red
txtgrn='\033[92m' # Green
txtrst='\033[0m'  # Text Reset

# Return True if the directory contains a valid test case description
# -> contains a file "testcase.py"
# -> contains a name (unique?)
# -> contains a list named tests : [model,"command line", nbprocs, pre-treatment]
def isValidCasePath(path):
  testpath = os.path.join(path,"testcase.py")
  if os.path.isfile(testpath):
    dico = {}
    execfile(testpath, dico)
    if "name" in dico.keys() and "tests" in dico.keys():
      return type(dico["tests"])== type([])
  return False
  

# Group the binairies by compiler and optimization
#  in  : list of binairies
#  out : list of list of binairies
def factorize(models):
  dico = {}
  for elt in models:
    args = check_args.sort_args(elt[0])
    comp = args["Compiler"][0]
    opts = args["Option"][0]
    # Here we add a new element to identify the version of the exe (used in prepareTest)
    # newelt contains the list of options, the name of the exe, the command to
    # load the environment, and the version of the exe
    if len(args["Mode"]) == 0 or "seq" in args["Mode"]:
      model = args["Model"][0]
    else:
      model = "%s&%s" % (args["Model"][0], args["Mode"][0])
    newelt = list(elt)
    newelt.append(model)

    if dico.has_key(comp):
      if dico[comp].has_key(opts):
        dico[comp][opts].append(newelt)
      else:
        dico[comp][opts] = [newelt]
    else:
      dico[comp] = {}
      dico[comp][opts] = [newelt]
    
  binaries = []
  for compilo, compilo_l in dico.items():
    for opts, opts_l in compilo_l.items():
      binaries.append(opts_l)
      
  return binaries


# For post-treatment we may want to get the number of the last iteration 
# of the computation, this number is contained in the noreg file:
#  in  : a path to a test case (already run)
#  out : the last iteration of the run (or 0 if not found)
def getLastIter(path):
  lastiter = 0
  try:
    # open the file
    noregfile = open( os.path.join(path, "noreg.xml") , "r")
    l = noregfile.readline()
    while l:
      # Search for the good line
      if l.find("<iter>") >= 0:
        # Complicated query returning the iteration number
        lastiter = int(l.replace("<iter>","").replace("</iter>",""))
      l = noregfile.readline()
    noregfile.close()
  except :
    pass
  return lastiter


# Prepare a test case
# in  : a group of executables
#       a description of the tests
#       the path to the non-regression directory
#       the number of processors we are allowed to use
# Generates the name of the test, copies the test's
#  files in the directory and generates the 
#  commands to launch the test
def prepareTest(exe, test, noregdir, maxproc):
  dico = {}
  for elt in exe:
    dico[ elt[-1] ] = elt

  testname = test[0]
  basename = os.path.split(testname)[1]
  commands = []
  for t in test[1]:
    try:
      fbxname = ""
      environ = []
      exename = []
      nproc = 1
      for sub_t in t:
        sub = ""
        try:
          if sub_t[2] != 1:
            sub= "&mpi"
            nproc = sub_t[2]
          key = "%s%s" %(sub_t[0],sub)
        except:
          key = sub_t[0]

        fbxname = "%s-%s" % (fbxname, dico[key][1])
      casename  = "%s%s" % (basename, fbxname)
      if nproc != 1:
        casename  = "%s_%d" % (casename, nproc)
        # This test uses more processors that the
        # number of processors we allocated
        if nproc > maxproc:
          raise ValueError

      print "-- Preparing %s" % casename

      os.chdir( os.path.join(noregdir, 'TESTS') )
      shutil.copytree(testname, casename)

      command = [ nproc, casename ]
      for sub_t in t:
        nproc = 1
        sub = ""
        try:
          if sub_t[2] != 1:
            sub= "&mpi"
            nproc = sub_t[2]
          key = "%s%s" %(sub_t[0],sub)
        except:
          key = sub_t[0]

        mpilauncher = ""
        if nproc != 1:
          mpilauncher = "mpd --daemon && mpirun -n %d " % nproc

        params =  sub_t[1]
        launch = '%s%s%s/BIN/%s' % (dico[key][2], mpilauncher, noregdir, dico[key][1])

        # append the command and the logfile associated
        command.append( [launch, params, "output_%s_log" % dico[key][1] ])
      commands.append(command)
    except:
      pass
  
  return commands



def md5(fileName):
  """Return the md5 checksum of a file"""
  import hashlib
  m = hashlib.md5()
  try:
    fd = open(fileName,"rb")
  except IOError:
    print "Unable to open the file in readmode:", filename
    return
  content = fd.readlines()
  fd.close()
  for eachLine in content:
    m.update(eachLine)
  return m.hexdigest()

def interestingFiles(path):
  files = os.listdir(path)
  iFiles = []
  for f in files:
    if f.find("Residus_") == 0 or f.find(".save") > 0 or f.find(".plt") > 0 or f.find(".pseg") > 0 or f.find(".pmsh") > 0:
      iFiles.append([f,md5(os.path.join(path,f))])
  iFiles.sort()
  return iFiles

def getTime(path):
  fd = open(os.path.join(path,"noreg.xml"),"r")
  for line in fd.readlines():
    if line.find("overall") > 0 :
      time = int(line.replace("<time step=\"overall\">","").replace("</time>",""))
  fd.close() 
  return time

# Launch a test:
# in : path the path to the test
#      commands the list of commands
#      nproc the maximum number of proc needed for the test
#      notif is a threading.Condition object used for... threading purposes
def runATest(path, commands, nproc, notif, regid):
  global freeproc, problems
  short = os.path.split(path)[1]
  lock.acquire()
  print '-- Launching test : %s' % short
  lock.release()
  lastiter = 0
  recap =  open( os.path.join(path, "recap_log"), "w")
  recap.write("<tests testname=\"%s\" id=\"%s\">\n" % (short,regid) ) 
  for test in commands:
    # get the command line and the ouput filename
    command = test[0]
    params  = test[1] 
    if params.find("$lastiter$") >= 0:
      lastiter = getLastIter( path )
      params = params.replace("$lastiter$", "%6.6d" % lastiter)

    exename = command.split("/")[-1]
    output  = os.path.join(path, test[2])

    recap.write("<test name=\"%s\">\n" % (exename))

    t1 = time.time()
    process = subprocess.Popen('bash -c \'cd %s && %s %s\'' % (path, command, params), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    foutput = process.stdout
    line = foutput.readline()
    # Save the logfile for the generation of this program
    recap.write("<log><![CDATA[")
    logfile = open( output, "w")
    while line:
      logfile.write(line)
      recap.write(line)    
      line = foutput.readline()
    recap.write("]]></log>\n")
    logfile.close()
    foutput.close()
    
    # If the process terminates correctly it returns 0
    success = (process.wait() == 0) 
    t = time.time() - t1
    recap.write("<success>%s</success>\n<time>%.3f</time>\n</test>\n" % (success,t))    
    txt = "-- Command %s in %s" % (exename, short)
    if success:
      txt = "%s%s was successfull%s " % (txtgrn, txt, txtrst)
    else:
      txt = "%s%s had a problem%s " % (txtred, txt, txtrst)
      lock.acquire()
      problems += 1
      lock.release()

    print txt

  recap.write("<checksum>")
  for f in interestingFiles(path):
    recap.write( "%s:%s\n" % (f[0],f[1]) )
  recap.write("</checksum>")

  recap.write("</tests>\n")
  recap.close()

  lock.acquire()
  print '-- Test %s is finished' % short

  # The nproc allocated for this test are now available
  freeproc += nproc
  lock.release()
  # We notify the main thread that the work is done
  notif.acquire()
  notif.notify()
  notif.release()


# Comparison function which sorts the tests cases
# we run the tests using the maximum number of processor first
# if the number of processors needed is bigger than
# the number of available processors we put that test
# at the end of the list
def comp(a,b):
  # -1 means that a<b , 0 means a==b, and 1 means a>b
  # "<" in the I wan't to order my threads meaning
  global freeproc
  x = a[0]
  y = b[0]

  if (x < y):
    if x > freeproc:
      return 0
    elif y > freeproc:
      return 1
    else:
      return -1
  elif (x == y):
    return 0
  else:
    if y > freeproc:
      return 0
    elif x > freeproc:
      return -1
    else:  
      return 1



# Main thread function that launch all the sub-threads
# in : totalproc the number of processors allocated
#      commands the list of tests cases
#      noregdir the path to the root of the NoReg directory
def launchTests(totalproc, commands, noregdir):
  global freeproc

  # cv is a lock which can be realesed when cv.notify() is called
  cv = threading.Condition()
  cv.acquire()

  # We will store the list of threads in this variable so we can
  # check if they are all finished before we exit this function
  threads = []

  # Set the number of available processors to the total amount of processors
  freeproc = totalproc
  
  # get the noreg id
  noregid = os.path.split(noregdir)[1][2:]

  # sort the threads (basic ordering)
  commands.sort(cmp = comp)
  while len(commands) > 0:
    tests = commands.pop()
    nproc    = tests[0]
    casename = tests[1]
    # Check if we can launch this test right now
    if nproc <= freeproc:
      # Launch the test in a thread
      thr = threading.Thread(None, runATest, 
                         None, (os.path.join(noregdir, 'TESTS', casename), tests[2:], nproc, cv, noregid))
      threads.append( thr )
      lock.acquire()
      # update the number of available processors
      freeproc -= nproc
      lock.release()
      thr.start()
      # If there is no more processors available we wait for a thread to finish (they will notify us)
      if freeproc == 0:
        cv.wait()
        
      # Reorder the tests because the number of available processors has changed 
      # ( the lock is needed because the sort is based on freeproc which can change at any time)
      lock.acquire()
      commands.sort(cmp = comp) 
      lock.release()      
    # the test needs too much processors
    elif nproc <= totalproc:
      # put the test back in the list
      commands.append(tests)
      # wait for a thread to finish
      cv.wait()        
      # Reorder the tests
      lock.acquire()
      commands.sort(cmp = comp) 
      lock.release()
    # This case should not be possible : the test needs more porcessors than the total available
    # theses case are suppressed in the function prepareTest
    else:
      print "%s-- It is not possible to launch %s" % (txtred, casename)
      print "-- This test needs %d processors - %d allocated for the regression testing%s" % (nproc, totalproc, txtrst)

  cv.release()

  print "-- All The tests are launched - Waiting for them to finish"
  # Make sure everything is finished
  for thr in threads:
    thr.join()
  


# Generate the list of the tests we can launch with the models we provide
# in : model the list of models generated in NonRegression.py
#      nbproc the total number of processors we can use to launch the tests
#      noregdir the path to the Non-regression root directory 
#      testdir the path to the root directory containing the tests
def list_and_launch(model, nbproc, noregdir, testdir):
  import shutil
  import subprocess
  global problems
  print
  print "-- Starting the validation of the Programs generated"
  print
  # Return to the root of the NoReg directory
  os.chdir(noregdir)
  # Create a directory to store the tests
  os.makedirs('TESTS')
  shutil.copyfile(os.path.join(testdir, 
                               u'script_visit.sh'),
                  os.path.join(noregdir, 
                               u'script_visit.sh'))
  # Get the list of the potential tests
  dirs = os.listdir(testdir)
  # Get the list of the valid tests
  validtests = []
  for directory in dirs:
    testpath = os.path.join(testdir, directory)
    if isValidCasePath(testpath):
      tests = listTests(testpath)
      # If the test case is not empty
      if len(tests[1]) > 0:
        validtests.append( tests )

  # sort the exes in a specific fashion
  binaries = factorize(model)
    
  list2Test = []
  # Prepare the tests (copies)
  for bin in binaries:
    for test in validtests:
      list2Test += prepareTest(bin, test, noregdir, nbproc)

  print 

  # Launch the tests
  launchTests(nbproc, list2Test, noregdir)

  print 
  print "-- No Reg is Finished"

  if problems > 1:
    print "There were %d execution problems" % problems
  elif problems == 1:
    print "There was %d execution problem" % problems
  else:
    print "There was no execution problem"
  subprocess.Popen([u'bash',u'script_visit.sh'],
                   cwd=noregdir).wait()

# associe pretraitement avec traitement
def tri(ok, needPre):
  size = len(needPre)
  
  noEvol = True
  associate = []
  used = []
  for elt_pre_ in ok:
    elt_pre = elt_pre_[-1]
    prename = elt_pre[0]
    
    wasTreated = False
    
    for elt_need_ in needPre:
      isPreprocess = False
      elt_need = elt_need_[-1]
      if elt_need[-1] == prename: 
        elt_loc = []
        for elt_p in elt_pre:
          elt_loc.append(elt_p)
        elt_loc2 = []
        for elt_p in elt_need:
          elt_loc2.append(elt_p)

        pretreatment = []
        for pre in elt_pre_[:-1]:
          pretreatment.append(pre)
          
        if (elt_pre[1].find("$nbproc$") >= 0):
          if (elt_need[2] > 1):
            elt_loc[1] = elt_pre[1].replace("$nbproc$", "%d" % elt_need[2])
            pretreatment.append(elt_loc)
            pretreatment.append(elt_need)
            isPreprocess = True
        else:
          if (elt_need[1].find("$nbproc$") >= 0):
            try:
              nbproc = elt_pre[2]
            except:
              nbproc = 1

            if (nbproc > 1):
              elt_loc2[1] = elt_need[1].replace("$nbproc$", "%d" % nbproc)
              pretreatment.append( elt_loc)
              pretreatment.append(elt_loc2)
              isPreprocess = True
          else:  
            try:
              nbproc = elt_pre[2]
            except:
              nbproc = 1
            if nbproc == 1:
              pretreatment.append(elt_loc)
              pretreatment.append(elt_need)
              isPreprocess = True
           
        if isPreprocess:
          associate.append( pretreatment )
          wasTreated = True
          used.append(elt_need_)

    if not wasTreated:
      associate.append(elt_pre_)

  for elt in used:
    try:
      needPre.remove(elt)
    except:
      pass
  
  l = len(needPre)
  if l == 0:
    return associate, needPre
  else:
    if l < size:
      return tri(associate, needPre)
    else:
      return associate, needPre


# Check if a test is valid (the models exists)
def isValidCase(cases, dico):
  for cas in cases:
    name = cas[0]
    if not check_args.isModel(name):
      return False

  return True
 

# Display a test case on the screen
def prettyPrint(cases, dico):
  for cas in cases:
    name = cas[0]
    args = cas[1]
    chaine = ""
    try:
      proc = cas[2]
      if proc > 1:
        chaine = "mpi %d processors : " % proc
    except:
      pass

    chaine = "\t%s%s %s" % (chaine, name, args)
    # Check if the model exist or not
    if not check_args.isModel(name):
      chaine = "%s[INVALID MODEL]%s%s" % (txtred, chaine, txtrst)

    print chaine


# Return the list of tests cases that can be launched with the
# current model
def listTests(path, verbose = False):
  launchables = []
  dico = {}
  execfile(os.path.join(path,"testcase.py") , dico)
  liste = dico["tests"]
  needPre = []
  canBeLaunched = []
  for elt in liste:
    l = len(elt)
    if l > 1:
      if l < 4:
        canBeLaunched.append([elt])
      else:
        needPre.append([elt])
    else:
      if verbose:
        print "%sInvalid definition [%s] :" % (txtred, elt)
        print "Need at least 2 parameters [model, arguments]%s" % txtrst

  ass, pre = tri(canBeLaunched, needPre)
  
  if verbose:
    id = 1
    for test in ass:
      if isValidCase(test, dico):
        print "  Case %d:" % id
      else:
        print "%s  Invalid case %d:%s" % (txtred, id, txtrst)
      id += 1
      prettyPrint(test, dico)
      print

    if len(pre) != 0 :     
      print "%sWarning it is impossible to run :%s" % (txtred, txtrst)
      for test in pre:
        prettyPrint(test, dico)

  valid = []
  # put the valid tests cases in the list "valid"
  for test in ass:
    if isValidCase(test, dico):
      valid.append(test)
  return path, valid, dico["description"], dico["name"]
