#!/usr/bin/python
import sys, os
import Construire

if __name__ == "__main__":
  Construire.expandPath()
  import check_args

  print "Displaying command line options"
  check_args.showCompiler()
  check_args.showModel()
  check_args.showMode()
  check_args.showOption()
  
