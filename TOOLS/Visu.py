#!/usr/bin/python
import os
import sys
from subprocess import Popen, PIPE

import Construire

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

# Try to rebuild a version of FluidBox
def main(args):
    argc = len(args)

    # Check if the number of arguments is valid
    if argc < 1 or argc > 3:
        chaine = "No"
        if argc > 3:
            chaine = "Too many"
        print("ERROR : %s parameters specified" % chaine)
        print("Usage : ./Visu.py [vistype] [name]")
        print("  [vistype] is the type of the visualization")
        print("  [name] is the root name to convert")
        return
    else:
        name = args[1]

        wd = os.getcwd()

        for f in os.listdir(wd):
            if f.startswith(name + "-") and f.endswith(".save"):
                f_plt = f.replace(".save", ".plt")

                if os.path.isfile(f_plt):
                    print(f + "\t" + bcolors.WARNING + "SKIP"+ bcolors.ENDC)
                else:
                    command = "$TFB/Run.py save2Visu " + args[0] + " " + f
                    #print(command)
                    process = Popen([command], shell=True, stdout=PIPE, stderr=PIPE)
                    (output, err) = process.communicate()
                    exit_code = process.wait()

                    if exit_code == 0:
                        print(f + "\t" + bcolors.OKGREEN + "DONE" + bcolors.ENDC)
                        print(output)
                    else:
                        print(f + "\t" + bcolors.FAIL + "ERROR" + bcolors.ENDC)
                        print(err)
                        break


if __name__ == "__main__":
    # Set the path to include all the python files we need
    Construire.expandPath()

    main(sys.argv[1:])

